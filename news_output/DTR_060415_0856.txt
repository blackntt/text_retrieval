﻿ " Sao " thể_thao làm_đẹp
Mộc_mạc và giản_dị , đó là nét thường thấy ở các nữ VĐV nhưng như_thế không có nghĩa_là họ không quan_tâm tới chuyện làm_đẹp , bởi mỗi khi ra sàn đấu , họ vẫn có cách làm_đẹp riêng .
Mỹ_Đức , Mai_Phương : Đẹp là phải hợp với mình
Đặc_trưng của Taolu là môn thể_thao biểu_diễn , vì_vậy các VĐV này đặc_biệt quan_tâm đến trang_phục thi_đấu . Chất_liệu mà các võ sỹ Wushu_Taolu ưa_thích là lụa tơ_tằm . Màu_sắc cũng rất nhẹ_nhàng . Mỹ_Đức thường chọn màu vàng cam , khi thì_là màu đỏ cho các bài thi_đấu trường quyền . Còn_Mai_Phương lại thích hồng nhạt và xanh da_trời vì những màu này hợp với các bài thi Thái_cực của cô hơn .
Ở giải vô_địch Wushu thế_giới vừa_qua , một_số VĐV các đoàn Mỹ , Nga , Italia đã trình_diễn một_số trang_phục lạ_mắt , nhất_là kiểu áo cách_tân sát_nách . Nhưng_Phương và Đức có cùng quan_điểm " không hợp với mình " .
Yếu_tố trang_phục hài_hòa cộng với gương_mặt xinh_xắn , đặc_biệt là thần_thái khi thi_đấu nhiều lần đã giúp họ chiến_thắng đối_phương , chinh_phục được các vị giám_khảo khó_tính , đăng_quang ngôi vô_địch .
Kình_ngư Võ_Thị_Thanh_Vy : Mê trang_điểm
Bơi_lội là môn thể_thao mang lại vóc_dáng khá lý_tưởng , ngặt nỗi , VĐV khi bơi đương_nhiên yếu_tố kỹ_thuật sẽ được coi_trọng hơn yếu_tố hình_thức và ai là người bơi nhanh_nhất sẽ thắng .
Nhưng không vì_thế mà các VĐV bơi_lội quên mất làm_đẹp cho mình . Lúc này , họ lại tập_trung vào sưu_tầm những bộ trang_phục bơi thật độc_đáo ; từ màu_sắc của mỹ tới những họa_tiết trên quần_áo bơi ... sao cho thật lạ_mắt .
Với_Thanh_Vy , kình_ngư xuất_sắc của bơi_lội TP.HCM và là thành_viên của ĐTQG thì cô lại rất mê trang_điểm . Vy cho_biết : " Con_gái ai chẳng thích làm_đẹp , cho_dù VĐV bơi_lội luôn phải ngâm mình dưới nước . Trang_điểm giúp tôi tự_tin hơn khi thi dấu và quyến_rũ mỗi khi ra đường .
Hoa_khôi Phạm_Thị_Kim_Huệ : Thích vẻ đẹp mộc_mạc
Hoa_khôi bóng_chuyền Kim_Huệ cao trên 1m80 dáng người cũng mảnh_khảnh như người_mẫu . Huệ tiết_lộ : cô rất ít trang_điểm đặc_biệt trước khi vào trận . Bởi vào sân_khấu vận_động nhiều mồ_hôi ra nếu dùng kem phấn không cẩn_thận sẽ phản_tác_dụng .
Tuy_nhiên đã là con_gái thi_thoảng cũng_nên trang_điểm đôi_chút . Có_thể là một_chút son môi chẳng_hạn . Được biết Kim_Huệ còn đang thử mình trong cả lĩnh_vực người_mẫu .
VĐV Dance_Sport_Hồng_Thi : Tự làm_đẹp cho mình
Hồng_Thi rất quan_tâm tới một_số phương_pháp làm_đẹp da bằng cách đơn_giản từ mật_ong , lòng_trắng trứng , cà_chua ... nhưng cũng ít khi thực_hiện được vì không có thời_gian . Ngoài_ra , trang_phục thi_đấu cũng được cô để_ý chăm_chút .
Còn nhớ trong thời_gian tập_luyện ở nước_ngoài có lúc Thi cùng người bạn nhảy là Hải_Anh đã phải cất_công nghiên_cứu , tìm ra kiểu_dáng và đặt may riêng trang_phục biểu_diễn . Thậm_chí còn phải sưu_tầm vải rồi may lấy cho vừa_ý mình .
VĐV nhảy_cao Ngọc_Tâm : Không_thể tiếc tiền
Ngọc_Tâm được trời phú cho dáng_dấp mảnh_mai và chiều_cao như một người_mẫu , hai má lúm_đồng_tiền làn da khá mịn_màng . Ngọc_Tâm từng tiết_lộ : cô thường_xuyên dùng kem chống nắng mỗi khi ra sân để giữ_gìn làn da mỏng_manh . Dù_vậy cũng không tránh được những thời_điểm da bị sạm đi vì nắng . Nhiều khi cũng phải đến thẩm_mỹ_viện để chăm_sóc da mặt . Tốn_kém lắm nhưng phụ_nữ mà không_thể tiết_kiệm quá . Hơn thế , những lúc rảnh_rỗi cô và đồng_đội thường tranh_thủ ... massage cho nhau . Theo An_Ninh_Thế_Giới

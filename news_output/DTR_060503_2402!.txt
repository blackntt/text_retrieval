﻿ Cặp đôi so_le : chồng 33 tuổi , vợ 104 tuổi
Người đàn_ông Malaysia 33 tuổi rạng ngời hạnh_phúc sánh_vai cùng_với người vợ đã bước sang tuổi 104 trước sự chứng_kiến thành_thân của bạn_bè , người_thân . Từ tôn_trọng lẫn nhau , hai người_ở hai thế_hệ này đã yêu nhau và sau đó là quyết_định kết_hôn .
Chú_rể trong lễ cưới có “ một không hai ” đó là anh Muhamad_Noor_Che_Musa . Đây là cuộc hôn_nhân đầu_tiên của anh nhưng lại là lần thứ 21 bước lên xe_hoa của bà Wook_Kundor , theo tờ The_Star .
Anh Muhamad là một cựu_chiến_binh . Anh này nói anh cảm_thấy thoải_mái và có cảm_giác không_thể sống thiếu bà Wook_Kundor ngay từ khi gặp bà . Muhamad nói rằng anh thông_cảm mới hoàn_cảnh của bà Wook vì bà không có con , già_yếu và sống cô_đơn một_mình .
“ Tôi lấy cô ấy không phải vì tiền , cô ấy rất nghèo ” , Muhamad nói với báo_chí . “ Trước khi gặp Wook , tôi chưa bao_giờ ở lâu một chỗ bao_giờ ” .
Tờ_The_Star không cho_biết 20 đời chồng trước của bà Wook có còn sống không .
N . H_Theo AP

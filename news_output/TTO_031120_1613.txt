﻿ Michael_Kamen đã ra_đi
Kamen đã ngã qụy tại nhà vì bệnh đau tim ngày 18-11 . Ông hợp_tác rộng_rãi với rất nhiều nghệ_sĩ , trong số những album nổi_tiếng nhất có album The_Wall năm 1979 mà ông hòa_âm cho ban_nhạc Pink_Floyd .
Giải_Grammy gần đây nhất ông đoạt được vào năm 2001 , cùng_với ban_nhạc rock Metallica cho hòa_âm của bản The_Call of Ktulu . Trước đó , ông cũng đã đoạt giải Grammy vào năm 1996 với phần hòa_âm hay nhất cho tác_phẩm An_American_Symphony trong phim Mr . Holland ' s Opus . ( Sau phim này , Kamen đã lập quỹ mang tên Holland_Opus để trợ_giúp nhạc_cụ cho trẻ_em trong nước ) . Và giải Grammy đầu_tiên của ông là vào năm 1992 với âm_nhạc trong phim Robin_Hood : Prince of Thieves .
Với những ca_khúc hợp_tác cùng Bryan_Adams như bài hát chính ( Everything I_Do ) I_Do_It_For_You trong phim Robin_Hood và Have_You_Ever_Really_Loved a Woman trong Don_Juan_De Marco , ông đã được đề_cử giải Oscar .
Kamen cũng từng cộng_tác với những ngôi_sao nhạc pop , jazz và rock như Sting , Rod_Stewart , David_Bowie và Eric_Clapton .
Bản_nhạc viết_tay của John_Lennon được bán với giá 455.000 USD
Bản_nhạc viết_tay của John_Lennon - Nowhere_Man - đã được bán với giá 455.000 USD , nhiều gấp 4 lần so với dự_đoán ban_đầu tại nhà bán_đấu_giá Christie hôm 18-11 .
Một kỷ_vật quan_trọng khác đã được bán là tượng Oscar dành cho đạo_diễn xuất_sắc nhất của Michael_Curtiz - đạo_diễn phim Casablanca , đã bán cho nhà ảo_thuật lừng_danh David_Copperfield với giá 231,500 USD .
L . T . ( Theo AP , Reuters )

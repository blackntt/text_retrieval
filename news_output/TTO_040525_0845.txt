﻿ Quảng_Nam thử_nghiệm công_nghệ ray hàn dài của Nhật_Bản
Dự_kiến chương_trình sẽ hoàn_thành và đưa vào sử_dụng ngày 30-6 . Nếu chạy thử_nghiệm thành_công sau một năm , sẽ tiếp_tục đưa công_nghệ mới này đầu_tư vào trên các tuyến đường_sắt đã và đang xuống_cấp , hạn_chế tốc_độ chạy_tàu .
Cũng trong dự_án công_nghệ mới này còn đầu_tư xây_dựng 4 đường ngầm dân_sinh băng qua đường_tàu trên đoạn đường_sắt này nhằm hạn_chế tai_nạn và đảm_bảo an_toàn chạy_tàu tại khu_vực thường_xuyên bị lũ_lụt đe_doạ gây ách_tắc chạy_tàu trong mùa mưa_lũ .
HOÀI_NHÂN

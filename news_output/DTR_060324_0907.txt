﻿ ĐTDĐ thế_hệ kế_tiếp với màn_hình cuộn ?
Tiếp_nối những thành_công trong việc khám_phá ra “ giấy điện_tử ” , Polymer_Vision vừa cho ra_mắt một sản_phẩm mang tính cách_mạng trong lĩnh_vực truyền_thông đa_phương_tiện , một màn_hình cuộn dành cho ĐTDĐ , thiết_bị định_vị GPS hay các thiết_bị di_động giải_trí khác .
Với kích_thước 4.8 inch , được Polymer_Vision phát_triển từ công_nghệ màn_hình linh_hoạt mới giúp màn_hình điện_thoại có_thể vừa_vặn với hầu_hết các thiết_bị có một vòng cuộn là 0.75cm . Độ_phân_giải là 240 x 320 pixel , màn_hình cuộn có tốc_độ làm tươi là 50Hz cùng thời_gian chuyển_đổi là 0.5 - 1 ms .
Loại màn_hình mới này khá vừa_vặn và linh_hoạt trong việc hiển_thị nội_dung nên chắc_chắn nó cũng sẽ giúp những chiếc ĐTDĐ tiện dung hơn như_là một công_cụ cho công_việc kinh_doanh .
Polymer_Vision đang nhắm tới mục_tiêu phát_triển hoàn_thiện sản_phẩm này để cung_cấp cho các thị_trường ĐTDĐ , máy định_vị GPS đang rất màu_mỡ . Hơn_nữa , loại màn_hình này lại tiêu_thụ rất ít nguồn điện_năng nên nó sẽ rất hữu_dụng khi kết_hợp với các thiết_bị nhỏ .
Những công_nghệ mà chỉ mới vài năm trước chúng_ta khó_lòng nghĩ ra thì nay đã là một sản_phẩm có thực . Hãy cùng chờ_đợi những điều kỳ_diệu tiếp_nối , những thiết_bị giải_trí di_động sẽ có một bước_tiến dài nữa trong công_nghệ .
Theo Tuyết_Phấn_Tuổi trẻ

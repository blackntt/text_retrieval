﻿ Không có tài_khoản ngân_hàng , có được đi Mỹ thăm người_thân ?
- Trả_lời của phòng Thị_thực , Tổng_lãnh_sự_quán Hoa_Kỳ tại TP.HCM :
Nếu muốn đi thăm thân_nhân tại Hoa_Kỳ , bạn phải xin thị_thực không di_dân .
Hướng_dẫn và đơn xin cấp thị_thực có sẵn trên mạng của chúng_tôi www . hochiminh.usconsulate.gov .
Việc không có tài_khoản trong ngân_hàng cũng không làm ảnh_hưởng đến việc xin cấp thị_thực của bạn . Khi xem_xét đơn xin thị_thực , viên_chức sẽ xem tổng_thể những ràng_buộc của đương đơn với Việt_Nam .
Trong những trường_hợp giống như trường_hợp của bạn ( không có tài_khoản ngân_hàng , những ràng_buộc khác như học_vấn , điểm , tình_trạng gia_đình , và những kế_hoạch của bạn ở Việt_Nam ) sẽ được xem_xét đến .
TTO

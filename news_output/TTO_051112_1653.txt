﻿ Các thông_tin về ngành Kiến_trúc tại Pháp
Thông_thường , các trường ĐH tại Pháp yêu_cầu trình_độ tiếng Pháp đầu_vào của thí_sinh là TCF trên 400 và điểm bài viết phải trên 12/20 ( tiêu_chuẩn này cũng có_thể cao hơn tùy theo yêu_cầu của mỗi trường ) .
Nếu sinh_viên tốt_nghiệp loại khá , giỏi tại VN , sinh_viên sẽ có cơ_hội học thẳng lên Master tại Pháp . Nếu sinh_viên tốt_nghiệp loại trung_bình , sinh_viên có_thể sẽ phải học lại năm cuối ĐH tại Pháp .
Thời_hạn nộp hồ_sơ cao_học vào tháng 3 , 4 hàng năm . Hội_đồng xét tuyển của nhà_trường sẽ họp và đưa ra kết_quả vào tháng 7 , 8 . Em của em có_thể tìm trường về ngành xây_dựng hoặc kiến_trúc tại trang_web : http : / / www.onisep.fr/national/atlas/html/atlas _ sup/dom/cadredom.htm
Kết_quả visa không phụ_thuộc vào thành_phố nơi em đăng_ký học mà phụ_thuộc vào tính logic của chương_trình học_tập của em , quá_trình học_tập của em tại VN . Tuy_nhiên , khi làm hồ_sơ đăng_ký , em nên chọn nhiều thành_phố khác_nhau để tăng khả_năng được nhận ( các trường ở Paris bao_giờ cũng có rất nhiều hồ_sơ đăng_ký , vì_vậy khả_năng được tiếp_nhận sẽ khó hơn ) .
Em có_thể tham_khảo thêm thông_tin về ngành Kiến_trúc - cảnh_quan dưới đây :
Jean_Nouvel , Christian de Porzamparc , Domonique_Perrault , Jean-Marie Charpentier ở Thượng_Hải hay Paul_Andreu ở Bắc_Kinh ... đều là những kiến_trúc_sư Pháp nổi_tiếng trên thế_giới . Nhưng bên_cạnh đó , Riccardo_Bofill ( Catalan ) , Renzo_Piano , Massimiliano_Fuksas ( Ý ) , Santiago_Calatrava ( Tây_Ban_Nha ) và rất nhiều kiến_trúc_sư khác cũng đã trở_nên nổi_tiếng tại Pháp nơi mà họ đã có_thể bắt_đầu xây_dựng các công_trình và rất nhiều người trong số họ đã thành_lập công_ty riêng .
Từ lâu , nước Pháp đã rất quan_tâm đến các sáng_tạo kiến_trúc và mời các kiến_trúc_sư nổi_tiếng đến xây_dựng ý_tưởng của họ tại Pháp : Rem_Koolas ( Lille … ) , Norman_Foster ( Millau , Nimes … ) , Andréa_Bruno ( Paris , Corte … ) , Ieoh_Ming_Pei ( kim_tự_tháp tại bảo_tàng Louvre ) , Oscar_Niemeyer ( Paris ... ) , Richard_Meier hay Frank_Gehry , danh_sách này còn rất dài và chưa kết_thúc …
Cách_thức đăng_ký dành cho sinh_viên nước_ngoài
Sinh_viên nước_ngoài có_thể được nhận vào mọi cấp_độ học , từ văn_bằng của riêng các trường hay bằng cao_học định_hướng nghiên_cứu ( DEA ) , bằng cao_học chuyên_môn ( DESS ) .
Một_số trường chấp_nhận sinh_viên nước_ngoài vào học trực_tiếp giai_đoạn 3 với thời_gian đào_tạo ngắn_hạn ( ví_dụ 3 tháng ) cho một lĩnh_vực đặc_biệt nếu sinh_viên đó đã học xong hoặc có kinh_nghiệm làm_việc trong ngành kiến_trúc tại đất_nước của sinh_viên .
Thí_sinh đăng_ký vào một trong ba giai_đoạn đào_tạo kiến_trúc phải nộp hồ_sơ theo mẫu có sẵn của Bộ_trưởng đặc_trách về kiến_trúc tại trang_web : http : / / www.archi.fr/ECOLES/index.html # formulaires
Thí_sinh có_thể lấy bộ hồ_sơ này tại Cơ_quan Hợp_tác và Hoạt_động Văn_hóa - Đại_sứ_quán Pháp tại nước_ngoài hoặc tại các trường kiến_trúc tại Pháp . Hồ_sơ phải được gửi đến trường kiến_trúc theo thời_hạn qui_định của hồ_sơ . Thí_sinh có quyền lựa_chọn theo nguyện_vọng 2 trường .
Hồ_sơ sẽ được gửi đến trường thứ nhất theo nguyện_vọng của thí_sinh . Nếu nguyện_vọng này bị từ_chối , hồ_sơ sẽ được chuyển_tiếp sang trường nguyện_vọng 2 . Các trường sẽ gửi thông_báo quyết_định đến các thí_sinh . Hồ_sơ sẽ không hợp_lệ và không được xét nếu_không đầy_đủ hoặc gửi đến sau thời_gian qui_định .
Bằng_cấp
Sinh_viên theo học toàn_phần hoặc một phần các mô đun của giai_đoạn 3 có_thể nhận được chứng_chỉ chuyên_sâu về kiến_trúc ( CEAA ) cho_phép sinh_viên học tiếp chuyên_ngành bổ_sung . Mặt_khác , các trường kiến_trúc cũng có_thể cấp các văn_bằng quốc_gia ở giai_đoạn 3 như DPEA , DESS , DEA ( tự đào_tạo và cấp bằng hoặc hợp_tác với các trường khác ) .
DPEA ( bằng dành riêng cho các trường kiến_trúc ) xác_nhận các chương_trình đào_tạo trong những lĩnh_vực chuyên_ngành ( vẽ phối_cảnh , xây_dựng hệ_thống chống địa_chấn tại Marseille , đóng tàu tại Nantes ... ) .
DESS ( bằng cao_học định_hướng chuyên_môn ) đi vào hướng chuyên_môn_hóa . Các trường ĐH cung_cấp những chương_trình đào_tạo DESS ở nhiều lĩnh_vực khác_nhau và hợp_tác cấp bằng với các trường kiến_trúc : văn_bằng này bổ_sung chương_trình học kiến_trúc và cho_phép sinh_viên tốt_nghiệp có_thể làm_việc với các nhà qui_hoạch_đô_thị và cảnh_quan , xung_quanh việc làm_chủ một công_trình .
Ví_dụ một_số chương_trình như : DESS công_trình di_sản khảo_cổ của các trường kiến_trúc Paris-Belleville , Strasbourg và Nancy với Trường ĐH Strasbourg II ; DESS quy_hoạch_đô_thị , dân_cư và di_dời của Trường_Kiến trúc Languedoc-Roussillon với Trường ĐH Perpignan .
DEA ( bằng cao_học định_hướng nghiên_cứu ) chuẩn_bị cho sinh_viên bước vào con đường nghiên_cứu , đó là năm đầu_tiên của chuơng trình tiến_sĩ . Ví_dụ một_số chương_trình như : DEA lịch_sử kiến_trúc hiện_đại và đương_thời giữa Trường ĐH Paris I với các trường kiến_trúc Versailles , Nancy , Lille-Région-Nord ; DEA dự_án kiến_trúc và đô_thị : lý_thuyết và thiết_bị , chương_trình hợp_tác giữa Trường ĐH Paris VIII với trường kiến_trúc Paris-Belleville , Paris-la-Vilette và Versailles .
Cảnh_quan
Văn_bằng DPLG ( văn_bằng đào_tạo các chuyên_gia về cảnh_quan ) , thời_gian đào_tạo 3,5 năm , thi_tuyển đầu_vào dành cho các thí_sinh đã có bằng tú_tài + 2 . Phần_lớn thí_sinh trúng_tuyển xuất_thân từ các ngành khoa_học ( địa_lý , khoa_học ) , nghệ_thuật tạo_hình ( trường kiến_trúc , trường nghệ_thuật ) . Có 2 trường đào_tạo văn_bằng này :
- Trường_Kiến trúc Bordeaux ( xem trong phần danh_sách các trường )
- Trường cao_cấp về cảnh_quan quốc_gia Versailles : http : / / www.versailles.ecole-paysage.fr
Ngoài_ra , còn có 3 trường khác đào_tạo kỹ_sư về cảnh_quan :
- Viện_Quốc gia về Hoa tại Angers , http : / / www.inh.fr , thời_gian đào_tạo từ 3 đến 5 năm .
- Trường quốc_gia cao_cấp về Thiên_nhiên và Cảnh_quan , http : / / www.ensnp.fr , thi_tuyển từ năm thứ nhất hoặc năm thứ 3 ( điều_kiện phải có bằng tú_tài + 2 năm ) .
- Trường cao_cấp về kiến_trúc vườn , trường tư_thục ( học_phí 6000 euros/năm ) , đào_tạo 5 năm cho bằng thạc_sĩ châu Âu về thiết_kế cảnh_quan , http : / / www.esaj.asso.fr
Di_sản
Trung_tâm Cao_học Chaillot tuyển_sinh ( trên hồ_sơ và phỏng_vấn ) 2 năm một lần ( vào các năm lẻ ) khoảng 80 kiến_trúc_sư nước_ngoài và Pháp cho khóa đào_tạo 2 năm chuyên_ngành phục_chế , bảo_tồn , gìn_giữ các công_trình cổ và bảo_vệ các thành_phố lịch_sử và cảnh_quan , http : / / www.archi.fr/CHEC ( xem thêm trong phiếu thông_tin ngành Di_sản ) .
Tổ_chức ngành học kiến_trúc
Giảng_dạy kiến_trúc được chia làm 2 loại_hình : giảng_dạy thuần_túy cho lĩnh_vực kiến_trúc ( dự_án kiến_trúc và đô_thị , lý_thuyết kiến_trúc và thành_phố , thể_hiện không_gian ) và có khả_năng làm_việc tốt trong các lĩnh_vực khác ( ghệ thuật , xã_hội_học , lịch_sử , khoa_học và kỹ_thuật , ngôn_ngữ , tin_học ... ) .
Chương_trình học được tổ_chức theo mô - đun học_kỳ hoặc năm với khá nhiều thời_lượng giờ học , tùy theo 3 giai_đoạn giảng_dạy và đang trong quá_trình chuẩn_hóa với tiêu_chuẩn châu Âu . Tuy_nhiên , chương_trình vẫn còn giữ lại theo giai_đoạn tối_thiểu là 2 năm/giai đoạn :
- Giai_đoạn 1 : Phương_pháp làm_việc và đào_tạo cơ_bản ( 1.600 giờ và văn_bằng giai_đoạn 1 ) .
- Giai_đoạn 2 : Nắm vững các khái_niệm chủ_yếu về dự_án kiến_trúc và đô_thị ( 1.500 giờ , văn_bằng giai_đoạn 2 , đồ_án và luận_văn ) .
- Giai_đoạn 3 : Giai_đoạn chuyên_môn_hóa để cấp bằng DPLG ( do 20 trường kiến_trúc công_lập cấp ) , DSEA ( do trường chuyên_ngành kiến_trúc , trường tư_thục cấp ) , DENSAI ( Viện khoa_học_ứng_dụng quốc_gia Strasbourg cấp , trường công_lập ) . Những văn_bằng trên cho_phép sinh_viên tốt_nghiệp hành_nghề tại Pháp , sau khi đã đăng_ký vào Hội_đồng các Hiệp_đoàn .
Các trường công_lập
- Trường kiến_trúc và cảnh_quan Bordeaux : http : / / www.bordeaux.archi.fr : cảnh_quan , âm_học , thiết_kế .
- Trường kiến_trúc Bretagne , http : / / www.rennes.archi.fr : di_sản , quy_hoạch_đô_thị .
- Trường kiến_trúc Clermont-Ferrand , http : / / www.clermont-fd.archi.fr : kiến_trúc và triết_học , vẽ phối_cảnh .
- Trường kiến_trúc Grenoble , http : / / www.grenoble.archi.fr : kiến_trúc đất_đai , môi_trường kiến_trúc và đô_thị , quy_hoạch_đô_thị .
- Trường kiến_trúc Languedoc-Roussillon , http : / / www.montpellier.archi.fr : không_gian đô_thị và phát_triển bền_vững , xây_dựng - cấu_trúc .
- Trường kiến_trúc Lille và Région_Nord , http : / / www.lille.archi.fr : lịch_sử và di_sản hiện_đại , cảnh_quan và dự_án đô_thị .
- Trường kiến_trúc Lyon , http : / / www.lyon.archi.fr : thành_phố và lịch_sử , phân_tích hình_dạng , công_nghệ số , đào_tạo kỹ_sư - kiến_trúc_sư .
- Trường kiến_trúc Marseille-Luminy , http : / / www.marseille.archi.fr : quy_hoạch_đô_thị / cảnh_quan địa trung hải , xây_dựng khí_hậu sinh_học / hệ_thống chống địa_chấn .
- Trường kiến_trúc Nancy , http : / / www.nancy.archi.fr : thiết_kế , thủy_tinh , rừng , hình_ảnh kỹ_thuật_số , mô_hình .
- Trường kiến_trúc Nantes , http : / / www.nantes.archi.fr : kiến_trúc đóng tàu , vẽ phối_cảnh , thành_phố , đất_đai .
- Trường kiến_trúc Normandie , http : / / www.rouen.archi.fr : thẩm_định lại đô_thị và kiến_trúc , vẽ phối_cảnh .
- Trường kiến_trúc Saint-Etienne , http : / / www.st-etienne.archi.fr : cư_xử trong đô_thị / dự_án đô_thị / lịch_sử , mô_hình , tin_học .
- Trường kiến_trúc Strasbourg , http : / / www.strasbourg.archi.fr : quy_hoạch_đô_thị , khảo_cổ , hình_ảnh tổng_hợp .
- Trường kiến_trúc Toulouse , http : / / www.toulouse.archi.fr : môi_trường / cảnh_quan , di_sản công_nghiệp và đô_thị , phát_triển bền_vững .
- Trường kiến_trúc Paris-Bellville , http : / / www.paris-belleville.archi.fr : kiến_trúc - di_sản - xã_hội , qui_hoạch_đô_thị , các đô_thị ở châu Á - Thái_Bình_Dương .
- Trường kiến_trúc Paris-Malaquais , http : / / www.paris-malaquais.archi.fr : kiến_trúc - văn_hóa - xã_hội , quy_hoạch_đô_thị , lập chương_trình .
- Trường kiến_trúc Paris - Val - de - Seine , http : / / www.paris-valdeseine.archi.fr : lịch_sử nghệ_thuật và khảo_cổ_học , quy_hoạch_đô_thị , quy_hoạch .
- Trường kiến_trúc Paris - La_Villette , http : / / wwwparis-lavillette.archi.fr : kiến_trúc đóng tàu , các đô_thị châu Á - Thái_Bình_Dương , cảnh_quan vườn , kiến_trúc và ứng_xử xã_hội , môi_trường .
- Trường kiến_trúc Marne-la-Vallée , http : / / www.marnelavallée.archi.fr : đất_đai , kiến_trúc , xây_dựng và phát_triển bền_vững .
- Trường kiến_trúc Versailles , http : / / www.versailles.archi.fr : cảnh_quan , lịch_sử kiến_trúc và đô_thị , kiến_trúc / cơ_sở_hạ_tầng / đất_đai .
Các trường khác
- ESA , trường chuyên_ngành kiến_trúc Paris . Đây là trường tư_thục ( 6.500 euros/năm ) thi_tuyển đầu_vào ngay từ đầu khóa học . Thời_gian đào_tạo 5 năm với văn_bằng của trường ( đào_tạo định_hướng kỹ_thuật hơn là nghệ_thuật ) , http : / / www.esa-paris.fr
- INSA ( Viện khoa_học_ứng_dụng quốc_gia Strasbourg , tên gọi cũ là ENSAIS ) là một trường kỹ_sư được Nhà_nước công_nhận . Thời_gian đào_tạo 3 năm sau 1 năm dự_bị khoa_học với văn_bằng tương_đương kiến_trúc , http : / / www.insa-strasbourg.fr
Đào_tạo liên_kết ngành Kiến_trúc
Trong số 80 trường ĐH Pháp , gần phân_nửa trong số đó đào_tạo và cấp văn_bằng liên_kết với các trường kiến_trúc . Danh_sách các trường ĐH hiện có tại trang_web của Bộ Giáo dục quốc_gia : http : / / www.education.gouv.fr/sup/univ.html . Có khoảng 30 đến 40 chương_trình DESS và một con_số tương_tự như_vậy đối_với chương_trình DEA liên_quan đến kiến_trúc , cảnh_quan hoặc đô_thị_hóa ; khoảng 80 phòng_thí_nghiệm và nhóm nghiên_cứu làm_việc trong lĩnh_vực này .
Các nghề kiến_trúc
Kiến_trúc_sư làm_việc trên nhiều giai_đoạn khác_nhau của dự_án : thiết_kế , kỹ_thuật thể_hiện , thiết_kế , kiến_trúc nội_thất , chiếu sáng , vẽ phối_cảnh , bảo_tồn và trùng_tu , cảnh_quan , đô_thị_hóa ... Ngoài việc làm_chủ công_trình , kiến_trúc_sư còn tham_gia giảng_dạy và nghiên_cứu , lên chương_trình , trợ_lý cho chủ công_trình ...
Các địa_chỉ hữu_ích
- http : / / www.culture.fr : trang_web của Bộ Văn hóa .
- http : / / www.archi.fr : Bộ Văn hóa - Kiến_trúc .
- http : / / www.enpc.fr : Trường_Quốc gia Cầu_đường .
- http : / / www.entpe.fr : Trường_Quốc gia các công_trình công_cộng .
- http : / / www.cstb.fr : Trung_tâm Khoa_học và kỹ_thuật xây_dựng .
- http : / / www.les-grands-ateliers.archi.fr : Các công_trường lớn ở l Isle d Abeau .
- http : / / www.ramau.archi.fr : Các hoạt_động và nghề_nghiệp kiến_trúc và đô_thị_hóa .
Q . DŨNG thực_hiện

﻿ Ai tiết_lộ vụ nhà_tù bí_mật của CIA ?
“ Mary_Mc Carthy không phải là người gây ra vụ đó ” , Rand_Beers , đồng_sự cũ của Mary_Mc Carthy , một nhà_phân_tích tình_báo kỳ_cựu của CIA , nói .
Beers là người đứng đầu các chương_trình tình_báo tại Hội_đồng an_ninh quốc_gia dưới thời tổng_thống Bill_Clinton . Beers nói McCarthy đã ủy_quyền cho ông đưa ra tuyên_bố này , nhưng từ_chối trao_đổi cụ_thể hơn .
Hôm thứ_năm vừa_qua , giám_đốc CIA Porter_Goss bày_tỏ mối lo_ngại sâu_sắc của mình về việc CIA sẽ “ chịu những thiệt_hại khó lường ” do thông_tin từ CIA bị rò_rỉ cho báo_giới và nói CIA đã sa_thải một nhân_viên không được tiết_lộ tên .
Thông_tin CIA lén xây_dựng các nhà_tù bí_mật ở nước_ngoài , chủ_yếu ở Đông_Âu , để giam_giữ và tra_tấn nghi can khủng_bố đã làm dấy lên làn_sóng chỉ_trích và phản_đối từ nhiều nước . Quốc_hội châu Âu đã thành_lập một ủy_ban điều_tra về vụ này và khẳng_định bất_cứ quốc_gia thành_viên EU nào bị phát_hiện có liên_quan có_thể sẽ bị trừng_phạt và mất quyền bỏ_phiếu trong liên_minh .
T.VY ( Theo AP )

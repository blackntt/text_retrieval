﻿ Những câu không nên nói trong công_sở
Để hòa_hợp và phát_triển tốt ở công_sở , bạn chẳng_những cần năng_lực làm_việc mà_còn cần cả năng_lực giao_tiếp . Nó giúp bạn thể_hiện khả_năng kinh_doanh , kinh_nghiệm , sự chuyên_nghiệp và chứng_tỏ bạn là ai với sếp cũng_như đồng_nghiệp . Sau đây là những câu không nên nói :
" Tôi không_thể làm được ... "
Thay_vì bạn nói " tôi không biết làm , tôi không_thể làm được " , hãy tìm cách trau_dồi thêm hay tìm ra phương_hướng , cách giải_quyết . Đứng trước tình_huống được giao một việc khó , bạn hãy nhận_lời và nhẹ_nhàng nói : " Tôi rất vui và tự_hào được đảm_nhận công_việc này . Tôi cần thêm thời_gian ( hay nhân_sự , tiền_bạc , huấn_luyện ) để thực_hiện dự_án " .
" Anh nghi_ngờ ( hay chất_vấn , phán_xét ) cách làm của tôi ư ? "
Bạn đừng cố_thủ , phòng_ngự khi bị sếp hay đồng_nghiệp chất_vấn , thử_thách . Hãy chuyển thành một câu nói khác tích_cực hơn như : " Tôi đánh_giá cao suy_nghĩ của bạn , nhưng tôi mạnh_dạn tin_tưởng vào cách làm của tôi " .
" Tôi sẽ thử làm theo anh nhưng đừng trách nếu_không hiệu_quả "
Có_thể bạn không cần làm theo cách của đồng_nghiệp nhưng cũng_nên tìm ra những điểm tương_đồng hay một_vài điểm nhất_trí . Hãy nói rằng : " Tôi đồng_ý chúng_ta cần thay_đổi chiến_lược , nhưng tôi còn thắc_mắc hay cảm_thấy không ổn về việc này , chúng_ta hãy cùng thảo_luận " .
" Anh_chị chẳng bao_giờ ... "
Tránh nói những câu đại_loại như " anh_chị chẳng bao_giờ chịu trả_lời email " vì nó thể_hiện sự tiêu_cực hay khẳng_định sự bất_lực của đối_phương . Với cách này , bạn khó mà giữ được hòa_khí khi làm_việc .
Theo Phụ_Nữ TP.HCM

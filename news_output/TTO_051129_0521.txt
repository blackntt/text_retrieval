﻿ Lễ cưới : khuyến_khích báo_hỉ , tổ_chức tiệc_trà
Qui_chế qui_định : việc cưới cần phải được tổ_chức trang_trọng , lành_mạnh ; các thủ_tục có tính phong_tục , tập_quán như chạm_ngõ , lễ hỏi , xin dâu cần được tổ_chức đơn_giản , gọn_nhẹ ... Khuyến_khích các hình_thức như báo_hỉ ; tổ_chức tiệc_trà thay cho tiệc_mặn tại gia_đình , hội_trường ; cơ_quan , tổ_chức hoặc đoàn_thể đứng ra tổ_chức lễ cưới ...
Đối_với việc tang , cần được tổ_chức chu_đáo , gọn_nhẹ , tiết_kiệm ; khi đưa_tang phải tuân_thủ các qui_định của pháp_luật về an_toàn giao_thông và trật_tự an_toàn công_cộng ; hạn_chế tối_đa việc rắc vàng_mã , tiền_âm_phủ trên đường .
Qui_chế qui_định khi tổ_chức lễ_hội phải thực_hiện đúng qui_định của pháp_luật về di_sản văn_hóa và qui_chế tổ_chức lễ_hội do bộ_trưởng Bộ Văn hóa - thông_tin ban_hành .
TTXVN

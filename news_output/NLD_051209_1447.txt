﻿ Hồng_Ánh sống thật nhất khi yêu
Diễn_viên Hồng_Ánh_Nữ diễn_viên này tự nhận mình là người tham_công_tiếc_việc , nhưng phải công_nhận chị làm việc_gì cũng ra_trò . Trên gương_mặt của Hồng_Ánh không_bao_giờ thấy nét mệt_mỏi , bởi chị luôn sống hết_mình cả trong công_việc lẫn tình_yêu .
. Sau thời_gian dài , chị mới trở_lại với phim_truyền_hình qua bộ phim " Nghề báo " , vai_diễn cô nhà_báo hấp_dẫn chị ở điểm nào ?
- Đó là vai_diễn hoàn_toàn mới_lạ . Thúy_Bình là một nhà_báo mạnh_mẽ , tự_tin , năng_động và đầy quyết_đoán . Nhà_văn Nguyễn_Mạnh_Tuấn đã xây_dựng cho tôi một hình_tượng rất hấp_dẫn , vừa gần_gũi với con_người tôi nhưng cũng rất lạ_lẫm và buộc tôi phải khám_phá .
. Để diễn ngọt vai , chị lấy kinh_nghiệm làm báo ở đâu ?
- Tôi có may_mắn quen nhiều phóng_viên , một trong hai người bạn_thân nhất của tôi cũng là nhà_báo , vì_vậy tôi có khá nhiều vốn_sống cho vai_diễn này .
. Chị từng phát_biểu muốn được tham_gia những bộ phim thị_trường , nhưng thất_bại của hàng_loạt hãng phim tư_nhân vừa_qua có làm chị thay_đổi ?
- Được tham_gia vào bộ phim hay và có đông khán_giả là mong_muốn chưa bao_giờ thay_đổi trong tôi . Lúc này , tôi nghĩ điện_ảnh vẫn đang đi tìm khán_giả , con đường còn khó_khăn . Vì_thế không nên nóng_vội hay sớm nản_lòng . Xác_định nghiệp diễn là con đường dài nên tôi luôn kiên_nhẫn chờ_đợi .
. Chị nghĩ gì trước sự xuất_hiện ngày_càng nhiều dàn diễn_viên trẻ đẹp ?
- Đó là điều_kiện cần_thiết cho một nền điện_ảnh đang trên đà đi lên . Tôi không hề sợ sự đào_thải , gây ấn_tượng ở vai_diễn đầu là niềm_vui nhưng ở vai thứ_hai , thứ_ba mới là điều đáng_giá hơn cả .
. Đảm_nhận nhiều vai_trò : MC truyền_hình , đóng_kịch , đóng phim , biên_kịch , chị yêu_thích lĩnh_vực nào nhất ?
- Thú_thật , tôi không phải là người giải_quyết công_việc một_cách hoàn_hảo về mặt thời_gian . Hiện_nay tôi còn làm bán thời_gian cho một công_ty truyền_thông nữa nhưng dù làm_gì , tôi vẫn yêu nhất việc diễn_xuất , tôi cảm_thấy tự_tin trong vai_trò này .
. Bận_rộn như_vậy , chị dành thời_gian nào cho tình_yêu ? - Tôi ít có dịp vui_chơi , giải_trí ở những nơi đông người . Lúc căng_thẳng , tôi thường đi tâm_sự với bạn_thân là diễn_viên Mai_Hoa . Riêng về tình_yêu , lúc này xin cho tôi được giữ bí_mật . Với tôi , tình_yêu là điều gì đó thật quý_giá . Ở đó , tôi được sống thật nhất với chính mình dù buồn_vui hay đau_khổ .
Theo Văn_Hóa_Thông_Tin

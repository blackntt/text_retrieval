﻿ Người_dân mua nhà trả_chậm được tính lãi_suất 0,25%
Theo đó , chính_sách này được áp_dụng ngay đối_với các trường_hợp mua nhà tái_định_cư trả_chậm đồng_thời thay_thế quy_định bán nhà trả_chậm theo phương_thức quy_đổi ra vàng tại văn_bản số 1256 / QĐ - UB .
Cũng theo quy_định mới , tất_cả các trường_hợp đã mua nhà tái_định_cư theo phương_thức trả_chậm từ trước tới nay sẽ không quy_đổi theo giá vàng để trả nợ và sẽ được tính theo lãi_suất ngân_hàng ở mức 0,25% / tháng . Được biết , trên địa_bàn Hà_Nội hiện có khoảng 300 trường_hợp mua nhà tái_định_cư trả_chậm quy_đổi số tiền nợ ra vàng 98% .
TUẤN_CƯỜNG

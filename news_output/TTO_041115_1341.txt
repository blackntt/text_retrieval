﻿ Ai_Cập hy_vọng tìm ra bí_ẩn cái chết của vua Tutankhamun
Pharaoh_Tutankhamun đã trị_vì Ai_Cập 3.000 năm về trước và đã chết khi vẫn còn ở trong độ tuổi vị_thành_niên .
Zahi_Hawass , trưởng nhóm cho_biết : " Chúng_tôi sẽ biết được bất_cứ bệnh_tật và vết_thương nào ông_ta mắc phải cũng_như là tuổi thật của vị vua này . Chúng_tôi sẽ trả_lời được câu_hỏi liệu vị vua này chết bình_thường hay đã bị giết ? " .
Các nhà khảo_cổ_học sẽ đem xác của vị vua này ra khỏi ngôi mộ và thực_hiện kỹ_thuật Scan , công_việc này sẽ kết_thúc vào cuối năm . Kỹ_thuật này giúp tạo ra một hình_ảnh X-Quang 3 chiều về xác của Tutankhamun .
Những nghiên_cứu trước_đây cho_biết Tutankhamun đã chết vì bị đánh vào đầu . Bí_ẩn về vị vua này càng được nâng cao sau khi một trong những người đầu_tiên đi vào mộ của ông đã chết sau khi bị muỗi cắn .
Người_ta cho rằng những người này đã phạm phải lời nguyền của những vị Pharaon . Tuy_nhiên các nhà_khoa_học tin rằng một loại bệnh nằm sẵn trong ngôi mộ chính là nguyên_nhân giết chết nhà khảo_cổ này .
KINH_LUÂN ( Theo Reuters )

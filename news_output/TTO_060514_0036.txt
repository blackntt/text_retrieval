﻿ Trừ ruồi đục trái bằng ... bã bia
Đây là kết_quả hợp_tác nghiên_cứu giữa ACIAR , Công_ty bia Foster ’ s , các nhà_khoa_học thuộc Trường ĐH Griffith ( Úc ) , Viện_Nghiên cứu cây_ăn_quả miền Nam ( SOFRI ) , Viện_Bảo vệ thực_vật và Công_ty_cổ_phần Thuốc_sát_trùng Cần_Thơ .
Năm 2001 , từ đề_nghị của Chính_phủ VN , các nhà_nghiên_cứu Úc và VN đã phối_hợp với Foster ’ s Tiền_Giang chuyển bã bia từ qui_trình sản_xuất bia thành một sản_phẩm protein . Bã protein sau đó được Công_ty_cổ_phần Thuốc_sát_trùng Cần_Thơ trộn với một lượng rất nhỏ thuốc_trừ_sâu tạo thành chất dẫn_dụ và tiêu_diệt ruồi hiệu_quả ( sản_phẩm có tên thương_mại là SOFRI Protein ) .
V.TR .

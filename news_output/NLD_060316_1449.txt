﻿ Án cao nhất 5 năm 6 tháng tù_giam
Chiều 15-3 , Tòa_án nhân_dân thành_phố HN tuyên_án vụ mua bán_dâm tại khách_sạn Lake_Side . Với tội_danh chứa mại_dâm , Lê_Minh_Tuấn , phó_giám_đốc , bị tuyên_phạt 5 năm 6 tháng tù .
Cùng tội_danh này , Nguyễn_Thị_Thanh_Thuỷ ( nhân_viên thu_ngân ) bị tuyên_phạt 3 năm tù , Lê_Thị_Thuỷ 3 năm .
Với tội môi_giới mại_dâm , 4 " má_mì " Ngô_Thị_Thuỷ bị tuyên_phạt 4 năm , Nguyễn_Thị_Thu_Hương , Tăng_Ngọc_Dung và Đặng_Huyền_Anh cùng bị tuyên mức 3 năm . Riêng_Ngô_Tùng_Thiện , HĐXX cho rằng có vai_trò thứ_yếu trong vụ_án nên chỉ bị phạt 2 năm tù_treo
Tại toà , Lê_Minh_Tuấn phủ_nhận sự liên_quan tới hoạt_động mại_dâm diễn ra tại Lake_Side . Tuấn khai , là phó_giám_đốc phụ_trách nhân_sự , nên chỉ quản_lý đội_ngũ phục_vụ bưng_bê đồ uống . Còn các tiếp_viên do các quản_lý phụ_trách .
Tuy_nhiên , với các chứng_cứ và lời khai trùng khớp của các tiếp_viên ở Lake_Side , HĐXX nhận_định , Lê_Minh_Tuấn phải chịu trách_nhiệm về hoạt_động mại_dâm diễn ra tại CLB , sau Huang_Bo , giám_đốc CLB . Hiện_tại , Huang_Bo và Chu_Ching_Jung , chủ_tịch Hội_đồng_quản_trị của liên_doanh vẫn đang bỏ trốn .
Theo VNExpress

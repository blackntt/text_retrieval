﻿ Hoạt_động thể_lực giúp giảm huyết_áp
Qua theo_dõi huyết_áp của 168 người bằng thiết_bị chuyên_dụng 24/24h trong 6 tuần , một nghiên_cứu mới_đây đã xác_nhận rằng , hoạt_động thể_lực giúp giảm huyết_áp ở những bệnh_nhân bị huyết_áp_cao , đặc_biệt là những trường_hợp đang phải sử_dụng thuốc .
Trước khi công_bố báo_cáo này trên tạp_chí chuyên_đề Sports_Medicine , bác_sĩ Domenico_Di_Raimondo , từ ĐH Universita degli Studi di Palermo ( Italia ) và các cộng_sự đã có những đánh_giá được sự ủng_hộ của giới nghiên_cứu về hiệu_quả giảm huyết_áp từ chương_trình đi bộ 6 tuần trên 168 bệnh_nhân bị huyết_áp_cao .
Chương_trình đi bộ nhanh được thực_hiện 3 lần/tuần , dưới sự hướng_dẫn của một nhà_vật_lý trị_liệu giàu kinh_nghiệm .
Đối_tượng tham_gia nghiên_cứu khi bắt_đầu bài_tập đi bộ nhanh có huyết_áp dao_động trong khoảng : 140 – 159 cho huyết_áp tối_đa và 90 – 99 cho huyết_áp tối_thiểu . Thêm vào đó tất_cả các đối_tượng này đều đang sử_dụng thuốc chống huyết_áp và không bị béo phì .
Sau chương_trình luyện_tập , huyết_áp tối_đa của những người tham_gia nghiên_cứu đã giảm xuống , chỉ còn từ 143,1 - 135.5mmHg , trong khi huyết_áp tối_thiểu cũng giảm xuống , chỉ còn từ 91,1 – 84,8 mmHg .
Ngược với một_số báo_cáo trước_đây cho rằng việc luyện_tập sẽ làm giảm huyết_áp ở nữ nhiều hơn ở nam , nghiên_cứu này cho thấy không có sự khác_biệt nào về hiệu_quả đi bộ nhanh với việc giảm huyết_áp ở cả 2 giới .
Kết_quả nghiên_cứu này đã góp_phần vào xu_thế coi việc rèn_luyện thân_thể là một phần quan_trọng của quá_trình điều_trị đối_với những người bị huyết_áp , tất_nhiên phải có sự hỗ_trợ của thuốc .
Phương_Uyên_Theo_Reuters

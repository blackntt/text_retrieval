﻿ Bàng_hoàng ngày trở_về !
Anh đã trực_tiếp tham_gia cứu những người còn sống_sót , cũng_như tiếp_nhận các tử_thi đã nhiều ngày phơi dưới mưa_nắng . Những nạn_nhân trở_về đều kể lại với nét_mặt còn kinh_hoàng . Tàu đánh_cá VN bị nạn tại vị_trí một đảo nhỏ lánh_nạn gần Đài_Loan bởi phía họ không cho các tàu VN vào đảo lớn , do_đó phải neo lại ở một rạn san_hô cách đảo Đài_Loan từ 4 - 5 hải_lý .
Cơn bão đã hoành_hành tại vị_trí lánh_nạn này trong 13 tiếng đồng_hồ , gió và mưa cực lớn , thời_tiết lạnh . Mọi liên_lạc trên tàu gần_như vô_vọng . Các tàu bị chìm phần_lớn là bị đứt neo va phải rạn san_hô . Theo bác Phạm_Văn_Thắng ( thuyền_trưởng tàu ĐNA - 90151 ) , neo chung với tàu của bác là 40 chiếc tàu khác . Trong đó có tám tàu ĐN bị chìm và sáu tàu Quảng_Ngãi bị chìm theo , trung_bình mỗi tàu có 22 người .
Chuyện của 4 ngư_dân Quảng_Ngãi trở_về từ cõi chết
Phóng_viên Thế_Anh ( giữa ) trên tàu cứu nạn
Bốn anh Võ_Văn_Nam ( thôn Tân_Mỹ , xã Nghĩa_An , Quảng_Ngãi ) , Trần_Ngọc_Hoành ( Nghĩa_An , Quảng_Ngãi ) , Trương_Văn_Lên ( Nghĩa_Hòa , Quảng_Ngãi ) và Lê_Thanh_Vân ( Nghĩa_An , Quảng_Ngãi ) thuộc tàu Quảng_Ngãi 2726 may_mắn thoát chết , trong khi năm người đồng_hương còn lại mãi_mãi ở biển_khơi .
Đó là anh Hồng ( 30 tuổi ) , anh Xinh ( 22 tuổi ) , anh Cường ( 23 tuổi ) , Thật ( 16 tuổi ) và thuyền_trưởng Thanh ( 39 tuổi ) . Theo lời kể của anh Hoành , tàu 2726 chìm vào_khoảng 11g20 ngày 17-5 . Khi tàu chìm , chín người trên tàu dạt thành hai nhóm .
Nhóm của anh Hoành gồm bốn người bám trên hai chiếc can nhựa , nhóm năm người còn lại bám trên hơn 10 can nhựa . Nhóm anh Hoành bám được mấy khúc gỗ , trong khi nhóm năm người còn lại đã trôi hoặc chết gần nơi tàu chìm . Trước khi bị chìm hẳn , nhóm anh Hoành hét to với nhau cố_gắng giữ can tìm cách bơi lại vị_trí tàu chìm . Nếu có chết thì cùng nhau chết .
May_mắn là nhóm bốn người anh Hoành vượt sóng trở_lại tàu thì phần mũi vẫn còn nổi . Bốn người bám vào phần mũi đó chờ_đợi cứu sống trong ba ngày hai đêm , ăn rong_biển và cá sống cầm_hơi . Sau thời_gian sống trên mũi con tàu nửa chìm nửa nổi , họ được tàu ĐNA - 90351 cứu và sau đó chuyển sang tàu ĐNA - 90189 . Đến chiều 22-5 , bốn người này đã được đưa qua tàu cứu_hộ SAR 412 .
Cam_go cứu nạn
Suốt đêm 21-5 , liên_lạc giữa đất_liền và các tàu bị nạn với lực_lượng cứu nạn Trung_tâm 2 , hải_quân , biên_phòng vang lên liên_tục trên máy_bộ_đàm . Trên tàu SAR 412 , thuyền_trưởng , chỉ_huy_trưởng hiện_trường Phan_Xuân_Sơn đã quyết_định cho một_nửa số anh_em được nghỉ để giữ sức cho ngày 22-5 tập_trung cứu nạn . Số còn lại phải thức trắng_mắt theo_dõi và điều_khiển tàu đúng hướng đến hiện_trường .
Sau 23g ngày 21-5 , sóng_gió bắt_đầu mạnh so với lúc khởi_hành . Gió tây nam cấp 4 và sóng cao khoảng 2m khiến anh_em trên tàu đều hết_sức mệt_mỏi . Lúc 0g ngày 22-5 , tàu SAR 412 ở vào vị_trí 17 độ_vĩ bắc , 109 độ 55 phút kinh_độ_đông , tốc_độ tàu 14 hải_lý / giờ . Như_vậy tàu đã rời cảng Đà_Nẵng được 122 hải_lý . 1g30 ngày 22-5 , sóng và gió bắt_đầu mạnh hơn , sóng cấp 4 , cấp 5 và lên cao đến 3m .
Lúc 6g hôm_qua ( 22-5 ) , qua bộ_đàm tàu SAR 412 nhận được tín_hiệu của tàu bị nạn ngoài khơi . Đó là tàu cá 99 và tàu 45 của Đà_Nẵng . Lúc 7g30 , tàu SAR 412 đã đi được 215 hải_lý , vượt qua đảo Hoàng_Sa 33 hải_lý . Lúc 8g30 , theo tính_toán của thuyền_trưởng SAR 412 , nếu đi với vận_tốc 18 hải_lý / giờ , tàu đang cách các tàu bị nạn 100 hải_lý . Thuyền_trưởng Sơn quyết_định tăng_tốc để kịp công_việc trước khi trời tối .
Đến 11g , qua liên_lạc với các tàu cá , số người chết và mất_tích trên các tàu hơn 200 người . Hơn 12g , tàu ĐNA - 90189 báo đã nhìn thấy tàu SAR 412 . Lúc 12g15 , tàu SAR 412 cặp được với tàu 90189 để nhận thi_thể và đưa số nạn_nhân còn sống_sót lên tàu . Gió vẫn cấp 4 , cấp 5 nhưng sóng mạnh do tồn_lưu dòng không_khí sau bão nên hai tàu tròng_trành . Việc đưa xác nạn_nhân , người bị nạn qua tàu cứu_hộ rất khó_khăn .
Theo ghi_nhận của phóng_viên , mọi người trên tàu bị nạn đều ổn_định , xác của nạn_nhân bắt_đầu phân_hủy , mùi hôi_thối tỏa ra khắp_nơi . Mọi nặng_nhọc đều đè trên vai các thuyền_viên hai tàu SAR 411 và 412 . Nước_biển một màu đen sẫm . Trên trời , hải_âu kêu thảm_thiết . Tàu ĐNA - 90189 khác có 21 thuyền_viên và thêm bốn người sống_sót từ tàu Quảng_Ngãi . Tàu SAR 412 đã tiếp_nhận tám thi_thể và mười thuyền_viên . Lúc 13g30 tàu SAR 411 tiếp_cận được với hai tàu Đà_Nẵng số_hiệu 90345 và 90299 có bảy thi_thể và 23 thuyền_viên .
Sau hơn 5 tiếng đồng_hồ quần_quật đưa thi_thể và người cùng hàng cứu_trợ từ các đơn_vị đất_liền gửi ra ở trên hai tàu cứu_hộ , công_tác cứu nạn ở ba tàu kết_thúc vào lúc 17g30 . Anh Nguyễn_Đức_Toàn - thuyền_trưởng tàu ĐNA - 90345 - bị tụt canxi và suy_kiệt , được các bác_sĩ trên tàu cấp_cứu .
Dự_kiến sau 20 tiếng nữa , tức 13g30 ngày 23-5 , hai tàu SAR mới đưa được thi_thể người chết và người bị nạn về cảng Đà_Nẵng . Qua bộ_đàm liên_lạc , tàu hải_quân 628 cũng đã cứu_hộ và tiếp_nhận ba thi_thể người chết cùng 25 người bị nạn . Hiện tàu hải_quân này vẫn đang còn ở lại hiện_trường bị nạn để cứu nạn , cứu_hộ và tiếp nhiên_liệu cho các tàu bị nạn khác .
Danh_sách tàu chìm và mất_tích của Đà_Nẵng :
7 tàu bị chìm với 140 lao_động :
1 . Tàu ĐNA 90079 TS của ông Ngô_Tấn_Nhất - sinh năm 1958 , trú tổ 30 Thanh_Khê_Đông , có 22 người .
2 . Tàu ĐNA 90190 TS của ông Trương_Văn_Minh - SN 1972 , trú tổ 9 Xuân_Hà , có 20 người .
3 . Tàu ĐNA 90053 TS của bà Lê_Thị_Huệ - SN 1965 , trú tổ 15b Thanh_Khê_Đông , có 21 người .
4 . Tàu ĐNA 90199 TS của ông Phạm_Văn_Xinh - SN 1966 , trú tổ 36 Thanh_Khê_Đông , có 21 người .
5 . Tàu ĐNA 90093 TS của ông Ngô_Văn_Chiếu - SN 1931 , trú tại tổ 4 Xuân_Hà , có 17 người .
6 . Tàu ĐNA 90321 TS của ông Trần_Văn_Yá - SN 1943 , trú tổ 32 Thanh_Khê_Đông , có 21 người .
7 . Tàu ĐNA 90154 TS của bà Nguyễn_Thị_Phượng - SN 1965 , trú tổ 36 Thanh_Khê_Đông , có 18 người .
3 tàu vẫn còn mất liên_lạc với 58 lao_động :
1 . Tàu ĐNA 6126 TS của ông Đỗ_Văn_Đường - SN 1960 , trú tổ 35 Thanh_Khê_Đông , có 19 người .
2 . Tàu ĐNA 6018 TS của ông Bùi_Văn_Vịnh - SN 1956 , trú tổ 8 Xuân_Hà , có 20 người .
3 . Tàu ĐNA 90247 TS của ông Nguyễn_Văn_Ánh - SN 1957 , trú tổ 39 Xuân_Hà , có 19 người .
THẾ_ANH

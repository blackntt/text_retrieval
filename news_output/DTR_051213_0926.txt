﻿ Mourinho : HLV xuất_sắc nhất nước Anh 2005
HLV Jose_Mourinho ( Chelsea ) đã vượt qua hàng_loạt tên_tuổi khác để nhận danh_hiệu “ HLV xuất_sắc nhất nước Anh năm 2005 ” . Bên_cạnh đó , “ Vua bóng_đá ” Pelé đã được kênh truyền_thông nổi_tiếng BBC của Anh trao giải thành_tựu trọn đời .
“ Ông vua tại Stamford_Bridge ” Mourinho đượ c b ầ u là HLV gi ỏ i nh ấ t n ướ c Anh n ă m 2005 do đài_truyền_hình BBC bầu_chọn .
Ông đã v ượ t qua HLV độ i tuy ể n cricket Anh Duncan_Fletcher , HLV Liverpool_Rafael_Benitez và HLV độ i rugby x ứ Wales_Mike_Ruddock để giành giải cao_quý tại Anh hàng năm này .
Mourinho phát bi ể u : “ Đ ây là ph ầ n th ưở ng v ĩ đạ i mà tôi đ ã đượ c nh ậ n , vì 3 ứ ng viên x ế p sau tôi đề u r ấ t x ứ ng đ á ng . Tôi cho r ằ ng thành_công này là nh ờ vào hai gia đ ì nh mà tôi đ ang s ố ng . Đ ó là nhà tôi và sân Stamford_Bridge ” .
Bên_cạnh giải HLV xuất_sắc nhất dành cho Mourinho , “ Vua bóng đ á ” Pele đã được trao gi ả i Thành t ự u tr ọ n đờ i . Pele r ấ t b ấ t ng ờ khi bi ế t k ế t qu ả này , ông nói : “ Tôi có c ả m giác nh ư đ ang say . Tôi r ấ t c ả m độ ng , th ậ m chí có th ể khóc ngay lúc này nh ư m ộ t đứ a tr ẻ ” .
Theo L . G . Sài gòn giải_phóng

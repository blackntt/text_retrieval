﻿ TP.HCM : cần có chiến_lược để giảm tỉ_lệ thanh_thiếu_niên nạo phá_thai
Các ý_kiến đều cho rằng TP.HCM cần có một chiến_lược mang tính đồng_bộ , có người “ nhạc_trưởng ” để điều_phối về vấn_đề này . Ngoài trách_nhiệm của các ban_ngành liên_quan , tọa_đàm nhấn_mạnh đến việc cấp_bách đưa việc giáo_dục giới_tính vào trường_học và vai_trò của gia_đình , nhất_là giai_đoạn con bước vào tuổi dậy_thì .
Theo thống_kê từ năm 2003 đến nay , trung_bình trong hệ_thống các bệnh_viện công của TP.HCM có 100.000 ca nạo phá_thai / năm . Trong đó khoảng 1.000 ca phải sử_dụng phương_pháp sinh non Kovac ( thai quá lớn ) .
Ngoài những hậu_quả về mặt sinh_sản về sau như thai ngoài tử_cung , vô_sinh ... , các bạn trẻ này còn phải chịu những dư_chấn về mặt tâm_lý , ảnh_hưởng nặng_nề đến học_tập và lao_động .
TỐ_OANH - YẾN_TRINH

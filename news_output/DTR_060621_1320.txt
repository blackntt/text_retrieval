﻿ Orlando_Bloom : “ Keira_Knightley có nụ_hôn tuyệt_vời ! ”
Ngôi_sao trẻ Orlando_Bloom không ngần_ngại khi tiết_lộ , được hôn nữ diễn_viên xinh_đẹp Keira_Knightley thật_là một điều tuyệt_vời .
Orlando_Bloom và Keira_Knightley vừa hoàn_thành phần hai của bộ phim Cướp_biển vùng Caribe và chàng_trai hào_hoa này đã thổ_lộ rằng : “ Tôi rất thích những cảnh được hôn Keira , cô ấy có nụ_hôn thật tuyệt_vời , không_những thế cô ấy còn vô_cùng xinh_đẹp , dễ_thương và vui_vẻ . Những cảnh quay đó luôn làm tôi cảm_thấy có gì đó rất đặc_biệt , như_là đang dạo chơi trong đó vậy . ”
Keira_Knightley cũng vừa trả_lời phỏng_vấn trên tạp_chí Instyle số tháng 7/2006 rằng : Hiện_giờ cô chỉ thích hôn một người duy_nhất mà thôi , đó là bạn_trai cô , nam diễn_viên người Anh Rupert_Friend , 24 tuổi , người đã xuất_hiện bên_cạnh cô trong bộ phim Kiêu_hãnh và định_kiến . Và dù_rằng , Johnny_Deep , bạn diễn cùng cô trong Cướp_biển vùng Caribe có hôn_thú vị đến thế_nào thì cô vẫn thích những chàng_trai người Anh hơn .
Bên_cạnh đó , nữ diễn_viên 21 tuổi này còn cho_biết , cô rất ngại mỗi khi phải mặc đồ dạ_tiệc và trang_điểm thật đẹp để đi đâu_đó , nó làm cô cảm_thấy rất mất tự_nhiên và khi soi gương cô thấy mình thật ngốc_nghếch . Những bộ_đồ đơn_giản và thoải_mái đời_thường là trang_phục mà cô yêu_thích nhất .
Phần hai của bộ phim Cướp_biển vùng Caribe sẽ bắt_đầu được trình_chiếu trên toàn thế_giới vào ngày 7/7 này .
Keira trên bìa tạp_chí Instyle tháng 7/06 .
Vĩnh_Ngọc_Theo_Fametastic / Topix

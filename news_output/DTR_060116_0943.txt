﻿ Các CLB Italia đòi tẩy_chay Serie A
14 CLB nhỏ thuộc giải Serie A đang đe_doạ sẽ tẩy_chay các trận đấu với giữa họ với Juventus , AC Milan và Inter_Milan , nếu họ không được trả tiền bản_quyền truyền_hình xứng_đáng .
Các CLB này , dẫn_đầu là ông chủ của Fiorentina , ông Diego_Della_Valle , đã kiên_quyết đòi thương_lượng lại với các kênh_truyền_hình về bản_quyền truyền_hình các trận đấu .
Các đội bóng nhỏ_bé này đã ra yêu_sách rằng các kênh_truyền_hình cần phân_chia quyền_lợi từ bản_quyền truyền_hình cân_bằng hơn cho cả 20 đội bóng tham_dự Serie A .
" Hoặc là không thi_đấu các trận đấu với các đội bóng lớn , hoặc chúng_tôi sẽ đưa đội_hình trẻ thi_đấu với họ để ' ' giết chết ' ' bất_kỳ sự quan_tâm tới các trận này có tính quốc_tế này " , Chủ_tịch của Sampdoria , ông Riccardo_Garrone mới_đây đã mạnh_bạo tuyên_bố trên La_Gazzetta dello Sport .
Sự tranh_cãi nổ ra hôm thứ 4 tuần trước khi Thủ_tướng Italia_Silvio_Berlusconi đã trình Chính_phủ một văn_bản pháp_luật liên_quan đến truyền_hình mà có lợi cho các ông lớn như AC Milan , Juventus , Inter_Milan .
Gia_đình ông Berlusconi đang nắm giữ công_ty Fininvest , là chủ_sở_hữu AC Milan , một trong những đội bóng thu lợi_nhuận lớn nhất từ các hợp_đồng quảng_cáo với các kênh_truyền_hình .
Chính quy_định cho_phép các CLB tự thương_thảo hợp_đồng bản_quyền truyền_hình của họ từ năm 1999 - đã vô_hình chung góp_phần cho_phép các đội bóng lớn như Juventus , AC Milan hay Inter thu được khoản lợi_nhuận cao gấp 10 lần so với các đổi thủ yếu khác từ bản_quyền truyền_hình .
Theo Hà_Ngọc_Tiền phong/Reuters

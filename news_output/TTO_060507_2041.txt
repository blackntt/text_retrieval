﻿ Côn_Đảo - Bản anh_hùng_ca bất_khuất
Tham_dự buổi giao_lưu có gần 100 cựu tù chính_trị Côn_Đảo , nhóm thanh_niên sinh ngày 30-4 và các bạn sinh_viên Khoa sử Trường ĐH_KHXH& amp ; NV .
Thật xúc_động khi biết có những cụ ở tận Hà_Nội , Phú_Yên hay vùng_đất mũi Cà_Mau đã không ngại đường xa cùng tụ_họp về đây . Tất_cả họ - những con_người lập nên những kỳ_tích lịch_sử tại nhà_tù Côn_Đảo - địa_ngục Côn_Lôn đã làm không_khí buổi giao_lưu đầy ấm_áp với những cái bắt_tay nồng_ấm tình đồng_đội và những giọt nước_mắt hội_ngộ .
Tại buổi giao_lưu , cái tên Bùi_Văn_Toản càng được nhiều người biết đến như một người chuyên nghiên_cứu về nhà_tù Côn_Đảo . Trong những năm qua , ông đã sưu_tầm , thống_kê thông_tin của hơn 3.200 người tù hy_sinh ở Côn_Đảo , trong khi đó Ban quản_lý di_tích Côn_Đảo chỉ biết khoảng 700 ngôi mộ còn tên . Gần đây nhất , những ngày trung_tuần tháng tư , Nhà_xuất_bản Thanh_niên đã ra_mắt tập sách Côn_Đảo - Bản anh_hùng_ca bất_khuất của tác_giả Bùi_Văn_Toản .
HOÀI_PHƯƠNG

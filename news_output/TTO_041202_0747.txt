﻿ Thụy_Điển sẽ thử vaccin HIV vào năm 2005
Vaccin này do Viện quản_lý bệnh truyền_nhiễm phát_triển . Nó sẽ tập_trung vào dòng virus HIV - 1 tiền_thân của dạng virus phụ A , đây là loại virus làm 25 triệu người tại Châu_Phi nhiễm_bệnh .
Vaccin này ra_đời không nhằm mục_đích tăng khả_năng miễn_dịch đối_với HIV mà làm hạn_chế sự phát_triển của virus đối_với những người đã nhiễm_bệnh .
Trong chăng đầu của cuộc thử_nghiệm sẽ có 40 người tại Thụy_Điển tham_gia . Sang đến chặng thứ_hai được thử_nghiệm tại Dar es Salaam , Tazania sẽ có thêm 60 người tại quốc_gia này tham_gia .
Toàn_bộ cuộc thử_nghiệm này nhằm mục_đích kiểm_tra xem loại vaccin này có gây ra các tác_dụng phụ và liệu nó có phát_triển lập_tức sức đề_kháng của cơ_thể .
Ông Eric_Sandstroem , bác_sĩ trưởng tại bệnh_viện Soeder , Stockhom cho_biết : " Trong trường_hợp tốt nhất chúng_tôi sẽ bắt_đầu tiêm vaccin tại Dar es Salaam vào năm 2005 " .
Các nhà_khoa_học cho_biết một loại vaccin giúp có_thể miễn_dịch được virus HIV sẽ chỉ ra_đời vào sau năm 2011 .
KINH_LUÂN ( Theo AFP )

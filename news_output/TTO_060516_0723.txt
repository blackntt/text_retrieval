﻿ Lầu_Năm_Góc bi_quan về cuộc không_kích Iran
Trong khi đó , Iran có_thể trả_đũa bằng các cuộc tấn_công vào Mỹ ở nhiều nơi như Iraq , Afghanistan , vùng Vịnh hay tấn_công Israel ở miền nam Lebanon , hoặc bằng những cuộc khủng_bố nhắm các nước phương Tây .
Tờ này cho_biết Bộ_trưởng Quốc_phòng Donald_Rumsfeld đã gọi cho chủ_tịch Ủy_ban quân_sự của Thượng_viện John_Warner , kêu_gọi ông này đừng mở điều_trần về các phương_án hành_động quân_sự của Mỹ ở Iran .
Newsweek không loại_trừ sự dao_động , thiếu cương_quyết từ phía Lầu_Năm_Góc có_thể khiến Tổng_thống Bush khước_từ đường_lối cứng_rắn hiện_nay và có_thể chấp_nhận đàm_phán trực_tiếp với Iran .
TRẦN_ĐỨC_THÀNH ( Theo Newsweek )

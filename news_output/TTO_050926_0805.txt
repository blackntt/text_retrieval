﻿ Thi_trắc_nghiệm như_thế_nào ?
- Trong kỳ thi tốt_nghiệp THPT và tuyển_sinh ĐH năm 2006 , đề thi môn ngoại_ngữ - gồm các tiếng Anh , Nga , Pháp , Trung - sẽ được ra hoàn_toàn dưới hình_thức TNKQ cho cả HS học theo chương_trình THPT phân_ban và HS học theo chương_trình không phân_ban .
Các đề thi ngoại_ngữ năm 2006 sẽ sử_dụng loại câu_hỏi trắc_nghiệm có bốn phương_án lựa_chọn . Thời_gian làm bài sẽ là 45 phút đối_với kỳ thi tốt_nghiệp THPT và 90 phút đối_với bài thi_tuyển sinh ĐH , CĐ .
Dự_kiến số_lượng câu_hỏi trong mỗi đề thi là khoảng 50 câu_đối với đề thi tốt_nghiệp THPT và khoảng 70-100 câu_đối với đề thi_tuyển sinh ĐH , CĐ . Đề thi do Bộ GD - ĐT tổ_chức biên_soạn và có nhiều phiên_bản do máy_tính tự_động xáo_trộn thứ_tự câu cũng_như các phương_án trả_lời .
Thưa ông , việc_làm bài và chấm thi TNKQ môn ngoại_ngữ có gì khác so với hình_thức thi hiện_nay ? Khi nào Bộ GD - ĐT tổ_chức hướng_dẫn cho HS lớp 12 làm_quen với phương_thức thi này ?
- Thí_sinh sẽ làm bài thi trên “ Phiếu trả_lời trắc_nghiệm ” được in sẵn theo mẫu thống_nhất do Cục Khảo thí và kiểm_định chất_lượng giáo_dục biên_soạn . Bài thi được chấm bằng máy_quét chuyên_dụng cài_đặt phần_mềm chấm thi . Sau đó kết_quả chấm sẽ được gửi về các hội_đồng thi và tuyển_sinh nơi thí_sinh dự thi .
Trong tháng mười , chúng_tôi sẽ gửi tài_liệu hướng_dẫn cho thí_sinh và giáo_viên đến các sở GD - ĐT và các trường ĐH , CĐ có tuyển_sinh bằng khối D . Từ tháng mười_một , chúng_tôi sẽ bắt_đầu tổ_chức thí_điểm thi thử cho HS lớp 12 tại một_số vùng_miền để rút kinh_nghiệm triển_khai thi thử rộng trong cả nước từ tháng 1-2006 . Việc tập_huấn kỹ_thuật liên_quan đến tất_cả các khâu trong quá_trình thi TNKQ sẽ được tiến_hành từ tháng mười_hai .
Xin cảm_ơn ông .
THANH_HÀ thực_hiện

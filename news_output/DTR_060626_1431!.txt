﻿ 6 bí_quyết thành_công của doanh_nhân thế_giới
Mỗi doanh_nhân đi lên bằng những con đường khác_nhau , song tựu chung lại , ở những con_người thành_đạt ấy đều có những bí_quyết sau .
Làm những việc quan_trọng trước khi việc kinh_doanh bắt_đầu
Điều đầu_tiên mà một doanh_nhân thành_đạt phải làm trước khi mở công_ty là nghiên_cứu sâu_rộng để đảm_bảo hiểu rõ về trị trường , đối_thủ , nhu_cầu của khách_hàng , cơ_hội cũng_như thách_thức …
Cũng_như trước khi lên_lớp phải làm bài_tập ở nhà , doanh_nhân giỏi phải hoàn_thành “ bài_tập ” trước khi quyết_định hành_động .
Kiếm tiền từ những hoạt_động phi lợi_nhuận
Nhiều doanh_nhân khởi_nghiệp bằng cách tình_nguyện tham_gia các dự_án phi lợi_nhuận . Điều này cho họ cơ_hội bổ_sung kiến_thức , kinh nghiệp và tạo_dựng những mối quan_hệ quý_báu . Không_những thế , hình_ảnh của họ cũng được cải_thiện rất nhiều trong mắt các đối_tác tương_lai .
Susan_Keuhnhold , chủ_sở_hữu một hãng thiết_kế đồ_họa lớn tại Indianapolis , đã khởi_nghiệp bằng cách tham_gia dự_án thiết_kế tình_nguyện cho trường_học của con mình . “ Tôi thường tham_dự các cuộc họp tại trường , nghe_ngóng các nhu_cầu của các bậc phụ_huynh cũng_như của thầy và trò trong trường , đề_xuất những gì tôi có_thể làm . Nhờ đó , nhiều người biết đến công_việc của tôi . Tôi nhanh_chóng thiết_lập được một mạng_lưới khách_hàng mà rất nhiều trong đó vẫn duy_trì đến hôm_nay ” .
Thời_cơ có_thể đến bất_cứ lúc_nào và bất_cứ đâu
Đôi_khi , cơ_hội bỗng hiện ra_vào lúc người_ta ít mong_đợi nhất . Những doanh_nhân thành_công luôn học cách mở_rộng mắt và lắng_tai để tìm_kiếm mọi cơ_hội , dù là nhỏ nhất .
Bạn sẽ không biết được công_việc bắt_đầu_từ đâu . Chìa_khóa cho thành_công là luôn phải gieo hạt_giống . Bạn không biết nó sẽ mọc lên loại cây nào , nhưng khi bạn gieo càng nhiều , cơ_hội để có một loại cây ưng_ý sẽ tăng lên . Điều này đồng_nghĩa với việc bạn có_thể luận_bàn về công_việc kinh_doanh ở bất_cứ đâu , vào bất_cứ lúc_nào . Đảm_bảo mình luôn trong tình_trạng lên dây_cót trước mọi cơ_hội tiềm_ẩn , chứ không tự đặt mình vào thế bị_động khi nó xuất_hiện .
Kinh_doanh nhỏ có_thể dẫn tới một doanh_nghiệp lớn
Đôi_khi nhiều chủ doanh_nghiệp dành cả thời_gian và tiền_bạc để đánh_bắt những con cá lớn mà bỏ_qua các cơ_hội nhỏ .
Robyn_Frankel , chủ hãng quan_hệ cộng_đồng ở St . Louis , đã học được rằng những khách_hàng với dự_án nhỏ nhất cũng có_thể biến thành một cơ_hội lớn . “ Sẽ dễ_dàng hơn để phát_triển các mối quan_hệ sẵn có hơn là thiết_lập một cái gì đó mới_mẻ ” , bà nói . Vì_thế , bà chào_đón cả những dự_án dù nhỏ nhất , hoàn_thành chúng một_cách khôn_khéo và chủ_động khai_thác những cơ_hội lớn hơn từ đó .
Niềm đam_mê đóng vai_trò rất quan_trọng
Ai cũng biết để trở_thành một doanh_nhân thành_đạt là rất vất_vả . Các chủ doanh_nghiệp thường phải mất rất nhiều thời_gian chuẩn_bị cho việc thành_lập công_ty , và thường_xuyên lo_lắng , bận_rộn với công_việc của mình . Chính vì_vậy , điều thiết_yếu là phải yêu_thích công_việc mình đang làm .
Richardson cho_biết bà dành thời_gian cho việc kinh_doanh của mình nhiều hơn việc ở công_ty , nhưng bà không ngần_ngại bởi công_việc đang làm mang lại cho bà niềm đam_mê . Những doanh_nhân khác cũng chia_sẻ quan_điểm này . Mở công_ty riêng là một thách_thức , nhưng nó rất đáng_giá khi bạn có cơ_hội để thực_hiện niềm đam_mê của mình .
Mạng_lưới cá_nhân là hình_thức quảng_cáo tốt nhất
Các doanh_nhân đều đồng_ý rằng phương_pháp marketing hiệu_quả nhất không phải là quảng_cáo trên các phương_tiện_thông_tin_đại_chúng , mà là thông_qua mạng thông_tin cá_nhân .
“ Bạn có_thể tiêu tiền cho các hình_thức quảng_cáo , nhưng bạn sẽ thu lợi được nhiều hơn từ việc thành_lập mạng_lưới thông_tin cá_nhân ” , Keuhnhold nói . Điều này đồng_nghĩa với sự tham_gia vào các tổ_chức tại cộng_đồng , gia_nhập các nhóm doanh_nhân địa_phương , tham_dự nhiều sự_kiện để gặp_gỡ mọi người . Theo Jobvn / MSN

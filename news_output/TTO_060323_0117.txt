﻿ 5 bộ ngành kiểm_tra tình_hình nhập lậu gia_cầm
Cùng ngày , Ban chỉ_đạo đã có công_điện khẩn số 09 yêu_cầu các tỉnh_thành Hà_Nội , Bắc_Ninh , Bắc_Giang , Hải_Phòng ... tăng_cường kiểm_soát việc vận_chuyển gia_cầm vào địa_phương .
Theo Ban chỉ_đạo quốc_gia phòng_chống dịch cúm_gia_cầm , hiện các tỉnh biên_giới phía Bắc tiếp_tục có nhiều gà nhập lậu vào VN . Chủ_yếu trong số này là gà sống giá nhập rất rẻ , có_khi chỉ 5.000đ/kg .
Chẳng_hạn , tại Lạng_Sơn ngày 22-3 , Đội quản_lý thị_trường số 1 phối_hợp với đội kiểm_soát gia_cầm đã thu_giữ 1,2 tấn gà nhập lậu . Lượng gà nhập lậu thường được đưa đến gần trại chăn_nuôi gà để “ hợp_thức_hóa ” thành gà nuôi trong nội_địa .
L.ANH - HOÀNG_VĂN

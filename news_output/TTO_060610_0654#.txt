﻿ Đơn thư chờ giải_quyết
- Tập_thể xã_viên hai tuyến số 9 và 47 thuộc HTX_VT Quyết_Thắng kiến_nghị xem_lại việc mở thêm tuyến xe_buýt chạy xa_cảng miền Tây , Bình_Điền , quốc_lộ 1 , Nguyễn_Văn_Linh , Bình_Hưng , quận 8 , Nhà_Bè ...
Vì đây là những tuyến có trùng xe rất nhiều , gây ra hiện_tượng tranh_giành khách , bỏ trạm , vượt trạm cúp đầu xe , đánh nhau rất phức_tạp . Thư đã chuyển đến BGĐ Sở GTCC_TP . HCM .
- Một_số người_dân ( đường Lê_Văn_Huân , phường 13 , quận Tân_Bình , TP.HCM ) phản_ảnh hai cơ_sở thêu may_công_nghiệp trên địa_bàn gây tiếng ồn quá mức , sản_xuất suốt 24/24 , không có thời_gian nghỉ .
Phòng_Tài nguyên - môi_trường quận Tân_Bình đã kiểm_tra và có công_văn nhắc_nhở ... nhưng đã hai tháng qua tình_trạng vẫn như cũ . Thư đã chuyển đến UBND quận Tân_Bình , TP.HCM .
- Bà_con hẻm 248 Nơ_Trang_Long ( phường 12 , quận Bình_Thạnh , TP.HCM ) đề_nghị chính_quyền địa_phương xem_xét và giải_quyết tình_trạng ngập nước đã nhiều năm nay ở hẻm này . Thư đã chuyển đến UBND phường 12 , quận Bình_Thạnh , TP.HCM .
Trong tuần qua , báo Tuổi_Trẻ đã nhận được 57 công_văn của các cơ_quan_chức_năng trả_lời thư khiếu_nại của bạn_đọc .
BAN_CÔNG_TÁC_BẠN_ĐỌC

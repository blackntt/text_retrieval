﻿ Festival_Huế 2006 : Hứa_hẹn nhiều điều mới_lạ
Đến hẹn lại lên , Festival_Huế năm nay sẽ chính_thức khai_mạc vào 19h ngày_mai 3/6 . Festival lần này diễn ra_vào dịp kỷ_niệm 700 năm Thuận_Hóa - Phú_Xuân - Thừa_Thiên_Huế , nên các hoạt_động văn_hoá sẽ được tổ_chức một_cách quy_mô nhất từ trước tới nay .
Tại_Festival_Huế năm nay sẽ tập_trung vào những chương_trình như : Làm nổi_bật kiến_trúc di_sản đặc_biệt ( Đại_Nội , Hoàng_thành , sông Hương , thuyền_rồng , nhà_vườn , lăng_tẩm , chùa Thiên_Mụ , điều_kiện cư_trú truyền_thống , nghệ_nhân thủ_công ... ) và nghệ_thuật ẩm_thực truyền_thống nổi_tiếng ở Đông_Nam Á .
Các chương_trình biểu_diễn nghệ_thuật truyền_thống Việt_Nam của các đoàn đến từ nhiều tỉnh_thành trong cả nước ( Nhã_nhạc cung_đình , Múa cung_đình , Múa_rối_nước , Nhạc_viện Hà_Nội ) và các chương_trình của các đoàn nghệ_thuật đến từ Trung_Quốc và các nước láng_giềng , cũng_như các vở kịch đương_đại mang tính nghệ_thuật cao như chương_trình nghệ_thuật sân_khấu đường_phố ( nghệ_thuật múa đương_đại , xiếc , âm_nhạc ... ) . Ngoài_ra còn có chương_trình của các nghệ_sĩ tạo_hình , video chủ_yếu đến từ nước Pháp , Hàn_Quốc và Nhật_Bản ...
Đặc_biệt , ở kỳ Festival lần này , người xem sẽ được thưởng_thức Lễ_hội truyền lô , vinh_quy bái_tổ lần đầu_tiên được tái_hiện lại trên sân_khấu . Cũng trong dịp này , đêm Hoàng_cung với Dạ_tiệc cung_đình cùng các chương_trình nghệ_thuật , ẩm_thực và sinh_hoạt cung_đình gắn với không_gian Đại_nội huyền_ảo sẽ được tổ_chức rất hoành_tráng .
Các buổi biểu_diễn thời_trang áo_dài truyền_thống với chủ_đề Lễ_hội áo_dài của các nhà_thiết_kế Minh_Hạnh , Võ_Việt_Chung … với cách nhìn đương_đại , độc_đáo trong cách sử_dụng vải và màu_sắc sẽ tạo nên khoảnh_khắc êm_dịu và đầy ấn_tượng cho người xem .
Bên_cạnh đó , tại các địa_điểm khác_nhau trong thành_phố , du_khách cũng sẽ có dịp thưởng_thức và tham_gia vào các chương_trình ca_nhạc Sử_thi Việt_Nam , Nhã_nhạc múa_hát cung_đình , ca Huế , dân_ca , dân vũ , kịch_nói , biểu_diễn nhạc_điện_tử quốc_tế ... trong_suốt thời_gian diễn ra Festival .
Festival_Huế 2006 sẽ kéo_dài đến hết ngày 11/6/2006 .
Đại_nội đang được chuẩn_bị " làm_đẹp " .
Lực_lượng công_an được bổ_sung nhằm đảm_bảo công_tác an_ninh tại Festival .
Du_khách nước_ngoài cũng háo_hức với Festival .
Quang_cảnh sẽ diễn ra dạ_tiệc trong đêm Hoàng_cung .
Tin : Hương_Đinh Ảnh : Việt_Hưng

﻿ Trao 3 triệu đồng ủng_hộ Hội KH Đoan_Hùng ( Phú_Thọ )
Ngày 05/01/2006 , Đại_diện Quỹ_Nhân ái báo Khuyến_học & Dân_trí đã trao 3 triệu đồng ủng_hộ Hội_Khuyến học huyện miền núi Đoan_Hùng ( Phú_Thọ ) .
Ông Nguyễn_Trọng_Dương , Chủ_tịch Hội KH Đoan_Hùng nhận số tiền ủng_hộ trên và hứa sẽ sử_dụng số tiền này đạt hiệu_quả cao , thiết_thực đẩy_mạnh phong_trào khuyến_học , khuyến_tài , xóa đói thông_tin , nâng cao dân_trí , đưa Đoan_Hùng trở thanh xã_hội học_tập .
Nhân_dịp này , Hội KH Đoan_Hùng đã mua dài_hạn 180 tờ báo Khuyến_học & amp ; Dân_trí / kỳ , cấp cho các Chi_hội KH cơ_sở , các khu hành_chính , trường_học trong toàn huyện . Đ . H

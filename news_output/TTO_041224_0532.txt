﻿ Thu_hồi mặt_bằng sử_dụng không đúng mục_đích
Ngoài_ra , các sở phải phối_hợp UBND quận 8 rà_soát , kiểm_tra hiện_trạng kho_bãi trên địa_bàn hiện sử_dụng không đúng mục_đích , không hết diện_tích , qua đó đề_xuất TP hoặc Bộ Tài chính xử_lý .
Đối_với khu dân_cư bị hỏa_hoạn tại phường 1 ( quận 8 ) , UBND_TP thống_nhất cho bồi_thường , thu_hồi toàn_bộ khu_vực bị cháy để xây_dựng chung_cư tái_định_cư tại_chỗ với qui_mô khoảng 2,6 ha . Trước_mắt triển_khai xây_dựng một phần dự_án diện_tích 8.100m 2 bao_gồm khu_vực bị cháy và khoảng 7.000m 2 thu_hồi thêm .
PHÚC_HUY

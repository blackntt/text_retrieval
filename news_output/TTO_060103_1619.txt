﻿ 40 học_bổng chào năm mới
Khoảng 10.000 lượt SV tham_gia các hoạt_động chào năm mới : tìm_hiểu công_nghệ mới , thi_đấu games trên máy_tính , thể_thao , ẩm_thực cùng văn_nghệ trẻ_trung , hào_hứng . Chương_trình do ban quản_lý ký_túc_xá ĐH Quốc_Gia TP.HCM phối_hợp cùng Nhà_văn_hóa Sinh_viên và Nguyễn_Hoàng_Informatics tổ_chức với tổng giá_trị trên 150 triệu đồng .
Đây là năm thứ_hai Nguyễn_Hoàng_Informatics thực_hiện chương_trình ký_kết hỗ_trợ lâu_dài đối_với các hoạt_động học_tập , vui_chơi giải_trí và trao_tặng học_bổng cho sinh_viên KTX Đại_học Quốc_gia TP.HCM .
Đ . T

﻿ World_Cup và các Vua_phá_lưới
Giành danh_hiệu Vua_phá_lưới tại World_Cup luôn là khao_khát cháy_bỏng của bất_kỳ tiền_đạo nào trên thế_giới . Khi mọi ánh mắt đang hướng về nước Đức , hãy cùng nhìn lại những chân_sút đã đặt dấu_ấn của mình trong lịch_sử các vòng chung_kết .
Không_thể phủ_nhận rằng , mục_tiêu cao nhất của mỗi đội bóng nói_chung và từng cầu_thủ nói_riêng tham_dự World_Cup là được bước lên bục cao nhất giành cho người chiến_thắng , tay nâng cao chiếc Cúp_Vàng mơ_ước .
Điều đó cũng đơn_giản và dễ hiểu như khát_khao lớn nhất của mỗi tiền_đạo là ghi_bàn và càng nhiều bàn thắng thì ... càng tốt . Và nếu những bàn thắng đó giúp đội nhà lên_ngôi thì càng tuyệt_vời hơn_nữa .
Dù_vậy , trong lịch_sử các vòng chung_kết , chỉ có 6 người vừa giành danh_hiệu Vua_phá_lưới vừa đoạt chức vô_địch thế_giới ở một vòng chung_kết , mà cái tên cuối_cùng không ai khác là “ người_ngoài hành_tinh ” Ronaldo .
Người đầu_tiên trong lịch_sử World_Cup được hưởng niềm_vui “ kép ” đó là tiền_đạo người Italia , Angelo_Schiavio khi giải đấu được tổ_chức trên đất_nước “ hình chiếc ủng ” năm 1934 . Với 4 bàn thắng ghi được , Schiavio cùng chia_sẻ danh_hiệu này với Oldrich_Nejedly ( Tiệp_Khắc cũ ) , và Edmund_Conen ( Đức ) .
Just_Fontaine tại Thuỵ_Điển 1958 .
Tiền_đạo hiện đang giữ kỷ_lục ghi nhiều bàn thắng nhất ở một kỳ World_Cup là Just_Fontaine ( Pháp ) với 13 bàn thắng tại Thuỵ_Điển năm 1958 . Cho_đến nay , vẫn chưa có chân_sút nào vượt qua được con_số 13 này và không hiểu những_ai duy_tâm có “ liên_hệ ” đến_điều gì chăng ?
Tuy_nhiên , với số lần “ nã pháo ” kỷ_lục như_vậy nhưng Fontaine lại không_thể giúp “ những chú gà trống Gôloa ” vô_địch bởi sự xuất_hiện của một “ vì tinh_tú ” trên bầu_trời bóng_đá năm đó : Pele .
Bốn năm sau , tại Chile , người Brazil bảo_vệ thành công_chức vô_địch nhưng điều khác_biệt là họ đóng_góp tới 2 trong tổng_số ... 6 vua_phá_lưới ( cũng lại là một con_số kỷ_lục ! ) ở giải đấu đó .
Garrincha và Vava là những người có tên trong “ bảng Vàng ” , bên_cạnh Valentin_Ivanov ( Liên_Xô cũ ) , Leonal_Sanchez ( Chile ) , Florian_Albert ( Hungari ) và Drazen_Jerkovic ( Nam_Tư cũ ) .
Gerd_Muller ( áo xanh ) , người đang giữ kỷ_lục ghi_bàn
nhiều nhất trong lịch_sử World_Cup .
Phải mãi tới World_Cup năm 1978 mới lại có một cầu_thủ vừa giành danh_hiệu Vua_phá_lưới vừa đoạt chức vô_địch , đó là tiền_đạo Mario_Kempes ( Argentina ) .
Với tổng_cộng 6 bàn thắng , trong đó có 2 bàn ghi trong trận chung_kết với Hà_Lan , chân_sút có mái_tóc dài “ nghệ_sĩ ” này đã đưa Argentina lần đầu_tiên tới ngôi_vị cao nhất .
World_Cup 1982 tại Tây_Ban_Nha là một dấu_ấn không_thể_nào quên đối_với Paolo_Rossi khi anh toả sáng mang về cho người Italia chức vô_địch thế_giới lần thứ 2 trong lịch_sử .
Chơi chẳng mấy ấn_tượng , nếu_không muốn nói là tệ_hại , trong 3 trận ở vòng đấu bảng nhưng Rossi lại “ thăng_hoa ” trong các trận đấu quan_trọng sau đó .
Paolo_Rossi toả sáng tại Espana 1982 .
Mở_đầu là cú hat - trick trong trận gặp ứng_cử_viên Brazil ở vòng 2 , tiếp đến là 2 bàn giúp Italia vượt qua Ba_Lan ở bán_kết và kết_thúc bằng bàn mở tỷ_số trong trận chung_kết gặp Tây_Đức .
Người cuối_cùng có được “ niềm_vui nhân đôi ” như_thế là Ronaldo , với màn trình_diễn siêu_hạng tại Nhật_Bản và Hàn_Quốc năm 2002 .
Chân_sút người Brazil chỉ không “ phát_hoả ” duy_nhất trong một trận và có tới 8 lần khiến các thủ_môn đối_phương phải vào lưới nhặt bóng . Anh cũng là tiền_đạo đầu_tiên ghi được hơn 6 bàn thắng tại một kỳ World_Cup kể từ sau huyền_thoại Grzegor_Lato ( Ba_Lan ) năm 1974 .
Ronaldo đang có cơ_hội vượt qua kỷ_lục của Gerd_Muller .
Các số_liệu thống_kê cho thấy , chỉ có 3 người ghi được số bàn thắng từ 2 con_số trở_lên tại một kỳ World_Cup , đó là Just_Fontaine ( Pháp , 13 bàn ) , Sandor_Kocsis ( Hungary , 11 bàn ) và Gerd_Muller ( Tây_Đức , 10 bàn ) . Nhưng một điều trùng_hợp và đáng buồn là cả 3 tên_tuổi này đều không có vinh_dự nâng cao chiếc cúp Vàng trong năm đó .
Tuy_nhiên , tiền_đạo người Đức may_mắn hơn 2 người còn lại là 4 năm sau , anh vẫn được hưởng hương_vị chiến_thắng ngọt_ngào trên quê_hương sau chiến_thắng 2-1 trong trận chung_kết trước Hà_Lan , trong đó có 1 bàn của Muller .
Với 14 bàn ghi được trong 2 kỳ World_Cup đó , Gerd_Muller hiện vẫn đang là chân_sút xuất_sắc nhất trong lịch_sử các vòng chung_kết . Người có tiềm_năng nhất phá vỡ kỷ_lục này chính là Ronaldo , với chỉ 2 bàn kém hơn .
Liệu “ gã béo ” có thành_công với mục_tiêu lịch_sử của mình tại Đức lần này ? Mạnh_Hải

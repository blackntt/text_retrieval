﻿ Tiền_Giang : tạm dừng xét_xử vụ đại_lý thức_ăn kiện hai nông_dân
Quyết_định trên được đưa ra , theo ông Trí , là do huyện_ủy - UBND huyện có yêu_cầu tòa_án huyện “ chậm ” lại chờ chủ_trương của tỉnh và trung_ương trong việc xử_lý những vụ_việc như_thế_này . Ông Trí cũng cho_biết quan_điểm cá_nhân của ông về vụ_kiện này là hai bên nên tiếp_tục hòa_giải để tìm tiếng_nói chung , thông_cảm lẫn nhau . Trường_hợp hòa_giải không thành buộc phải ra tòa xét_xử thì hội_đồng xét_xử vẫn sẽ tiếp_tục hòa_giải , nếu vẫn không được thì mới xử theo đúng qui_định của pháp_luật .
VÂN_TRƯỜNG

﻿ Bộ Ngoại giao ủng_hộ 51 triệu xây_dựng bệnh_xá Đặng_Thùy_Trâm
Thứ_trưởng Nguyễn_Phú_Bình cho_biết : Đây là số tiền thu được từ đợt quyên_góp ủng_hộ xây_dựng bệnh_xá Đặng_Thùy_Trâm do Đảng_ủy và Đoàn_Thanh niên Bộ Ngoại giao phát_động từ tháng 10 đến tháng 12-2005 .
Hoạt_động này đã được cán_bộ , đảng_viên , đoàn_viên ... trong toàn Bộ Ngoại giao hưởng_ứng nhiệt_tình .
Theo Thứ trưởng Nguyễn_Phú_Bình , “ Các thế_hệ cán_bộ trong ngành ngoại_giao đánh_giá cao hoạt_động đầy ý_nghĩa , tạo tiếng_vang lớn trong nước và với bạn_bè quốc_tế của Báo_Tuổi_Trẻ . Vì_vậy chúng_tôi mong_muốn được góp_phần để dự_án bệnh_xá Đặng_Thùy_Trâm sớm trở_thành hiện_thực ” .
Ngoài việc ủng_hộ xây_dựng bệnh_xá , Đoàn thanh_niên Bộ Ngoại giao cũng đã tổ_chức các buổi tọa_đàm cho các đoàn_viên học_tập theo gương liệt_sĩ Đặng_Thùy_Trâm và các thế_hệ cha_anh đi trước nhằm giáo_dục , bồi_dưỡng tinh_thần yêu nước cho thế_hệ trẻ .
Tin , ảnh : THANH_VINH

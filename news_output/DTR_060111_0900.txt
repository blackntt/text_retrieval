﻿ Del_Piero lập kỷ_lục , Juve vào tứ_kết Cúp_Italia
Một hat - trick đã giúp cá_nhân Del_Piero trở_thành chân_sút xuất_sắc nhất trong lịch_sử " Bà đầm_già " , và góp công lớn giúp Juve giành vé vào tứ_kết Cup quốc_gia bằng chiến_thắng 4-1 trước đội khách Fiorentina ở lượt_về vòng 4 .
Đêm qua chính là đêm của Del_Piero - chân_sút ghi_bàn nhiều nhất trong lịch_sử Juventus . Với một hat - trick vào lưới Fiorentina , tiền_đạo 31 tuổi đã nâng thành_tích của mình lên con_số 185 bàn trên tất_cả các đấu_trường , vượt qua kỷ_lục 182 bàn lập bởi chính Del_Piero và huyền_thoại Boniperti ( giai_đoạn 1947-1961 ) .
HLV Capello phát_biểu sau khi Juve vượt qua Fio với tổng tỷ_số 6-3 sau hai lượt : " Đêm nay , chúng_tôi đã có 90 phút thi_đấu tuyệt_vời , đặc_biệt là trong hiệp một . Del_Piero , cũng_như những cầu_thủ khác , đều có phong_độ rất tốt .
Juve cần những con_người như_thế nếu muốn duy_trì phong_độ đỉnh_cao đến thời_điểm kết_thúc mùa giải " .
Bước vào trận lượt_về vòng 4 trên sân Delle_Alpi , " Lão bà " có chút_ít lợi_thế nhờ đã cầm_chân Fiorentina ở lượt_đi với tỷ_số 2-2 . Và lợi_thế đó nhanh_chóng được khẳng_định bằng bàn mở tỷ_số ở phút thứ 9 sau một pha phối_hợp như đá tập .
Nhận quả tạt của hậu_vệ trái Balzaretti , Mutu đánh_đầu cho Del_Piero . Anh này che bóng khéo_léo trước khi xoay người hạ_thủ thành Frey bằng một cú sút chân phải , đưa chủ nhà vượt lên dẫn 1 - 0 .
Hưng_phấn với bàn thắng thứ 183 - đồng_nghĩa với việc trở_thành chân_sút hiệu_quả nhất của Juve , Del_Piero tiếp_tục có bàn thứ_hai . Lần này là một quả sút phạt trực_tiếp tuyệt đẹp đưa bóng vào lưới trong sự ngỡ_ngàng của Frey . 2 - 0 cho Juve ở phút 17 .
Del_Piero đã trở_thành một phần lịch_sử tại SVĐ Delle_Alpi
Không muốn dừng lại ở đó , các học_trò của Capello lần thứ_ba xé toang mành lưới đối_phương . Mutu nhanh chân băng vào sút bồi sau khi bóng chạm người Del_Piero bật ra ở phút thứ 21 , nâng tỷ_số lên 3 - 0 .
Khác với những gì đang thể_hiện tại Serie A , Fiorentina chống_trả rất yếu_ớt trước các đợt tấn_công dồn_dập của đội chủ nhà . Mãi tới gần cuối hiệp 1 , họ mới có pha bóng nguy_hiểm đầu_tiên bằng pha đá phạt trực_tiếp của Bojinov .
Sau giờ nghỉ giữa hiệp , HLV Prandelli mới tung hai trụ_cột Pasqual và Toni vào sân để cải_thiện tình_thế . Nhưng cục_diện trên sân vẫn hoàn_toàn nằm trong tầm kiểm_soát của Juve , thậm_chí Toni suýt phải nhận thẻ_đỏ vì vào bóng thô_bạo với Vieira .
Del_Piero hoàn_tất cú hat - trick lịch_sử của anh bằng pha sút phạt_đền thành_công ở phút thứ 56 , sau khi Zalayeta bị đốn ngã trong vòng cấm .
Thời_gian còn lại chỉ đủ để Fiorentina có bàn gỡ danh_dự nhờ công của Bojinov , sau quả phạt trực_tiếp của Pasqual vào phút thứ 64 .
Đội_hình thi_đấu :
Juventus : Buffon , Zambrotta , Thuram , Cannavaro , Balzaretti ( Gladstone ) , Mutu , Blasi , Vieira , Nedved , Del_Piero , Zalayeta ( Olivera )
Fiorentina : Frey , Ujfalusi , Gamberini , Dainelli , Pancaro , Brivio ( Pasqual ) , Pazienza , Montolivo ( Brocchi ) , Maggio , Bojinov , Pazzini ( Toni )
Cũng trong đêm qua , Palermo vượt qua AS Bari thuộc Serie B với tỷ_số 5-4 để cùng Juve giành hai chiếc vé đầu_tiên vào tứ_kết Cúp_Italia .
Theo Minh_Kha_Vnexpress

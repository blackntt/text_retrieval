﻿ Thanh_Hoá : " Sốt " giá cói nguyên_liệu
Vì_vậy đến thời_điểm này , giá cói nguyên_liệu khô trên địa_bàn huyện đã tăng lên tới mức kỷ_lục : 10.000 đồng/kg , tăng gấp đôi so với năm_ngoái .
Hiện các cơ_sở sản_xuất , kinh_doanh mặt_hàng cói xuất_khẩu trên địa_bàn huyện Nga_Sơn luôn trong tình_trạng thiếu nguyên_liệu sản_xuất trầm_trọng . Chỉ trong ba tuần đầu tháng 5 vừa_qua , các cơ_sở này phải nhập gần 1.000 tấn cói nguyên_liệu ở các huyện Quảng_Xương ( Thanh_Hoá ) , Kim_Sơn ( tỉnh Ninh_Bình ) ... để sản_xuất , kịp giao sản_phẩm cho bạn_hàng trong và ngoài nước .
Được biết , Nga_Sơn là huyện có vùng sản_xuất cói nguyên_liệu lớn nhất tỉnh Thanh_Hoá . Đây cũng là huyện có nghề truyền_thống dệt chiếu cói nổi_tiếng , hiện đang phát_triển thêm nghề sản_xuất các mặt_hàng cói mỹ_nghệ xuất_khẩu . Nghề này mỗi năm đem lại thu_nhập hàng chục tỉ đồng cho bà_con nông_dân nơi đây .
Tin , ảnh : HÀ_ĐỒNG

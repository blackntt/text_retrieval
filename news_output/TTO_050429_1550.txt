﻿ ĐBSCL : Tôm nguyên_liệu rớt_giá
Đến chiều ngày 28-4-2005 , tôm loại 20 con/kg tại Cà_Mau chỉ còn 127.000 đồng/kg , tại Trà_Vinh là 120.000 đồng đến 125.000 đồng/kg , tại Kiên_Giang là 120.000 đồng/kg . Loại 30 con/kg giá dao_động từ 87.000 đồng đến 92.000 đồng/kg . Loại 40 con/kg chỉ còn 65.000 đồng đến 70.000 đồng/kg ...
Dự_báo trong vài ngày tới , giá tôm nguyên_liệu ở ĐBSCL sẽ tiếp_tục giảm làm cho hàng chục_ngàn nông_dân nuôi tôm đang lo_lắng .
Trong khi đó , các doanh_nghiệp chế_biến xuất_khẩu tôm cũng gặp rất nhiều khó_khăn vì hải_quan Mỹ áp_dụng biện_pháp đặc cọc trước một khoảng thuế rất lớn , tương_đương giá_trị xuất_khẩu trong vòng một năm với mức thuế tương_ứng nếu muốn xuất_khẩu qua thị_trường này . Từ đó việc xuất_khẩu tôm sang Mỹ bị đình_đốn .
Đây là nguyên_nhân chính dẫn đến tình_trạng tôm nguyên_liệu rớt_giá ngay trong mùa khan_hiếm nguyên_liệu . Ngoài_ra còn một nguyên_nhân khác là do chất_lượng tôm trong mùa nghịch này chưa cao , không đồng_đều .
Trước tình_trạng này các doanh_nghiệp chế_biến xuất_khẩu tôm ở ĐBSCL quay sang chú_trọng đầu_tư khai_thác thị_trường EU và Nhật_Bản . Nhiều doanh_nghiệp đã ký thêm nhiều hợp_đồng xuất_khẩu sang hai thị_trường này ngay vào đầu tháng 5 .
Theo Báo_Cần_Thơ

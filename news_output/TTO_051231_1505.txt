﻿ 55 Câu_chuyện giáo_dục : Câu_chuyện thứ 12
Tương_tự , khi chuyện_trò với người khác , việc trả_lời thành câu đầy_đủ là rất quan_trọng để tỏ lòng tôn_trọng người hỏi .
Chẳng_hạn , nếu có ai hỏi “ Bạn có khỏe không ? ” , thay_vì chỉ trả_lời cụt_lủn là “ Khỏe ” , em nên nói “ Mình rất khỏe , cảm_ơn . Còn bạn thì_sao ? ” .
Qui_tắc này giúp học_sinh phát_triển một yêu_cầu trong ngôn_ngữ viết . Nó giúp trẻ học cách phát_triển và sắp_xếp các ý_tưởng của mình , nhất_là khi các câu_hỏi đòi_hỏi một câu trả_lời ngắn_gọn nhưng phải đầy_đủ ý_nghĩa . Chẳng_hạn , câu_hỏi “ Bạn có cho rằng đề_nghị thêm 45 phút nữa vào một buổi học sẽ được thông_qua ? ” , có_thể được trả_lời “ không ” mà không cần kèm theo lời giải_thích nào trừ khi học_sinh được yêu_cầu trả_lời sâu câu_hỏi với đầy_đủ lý_lẽ .
Một đồng_nghiệp nữ của thầy dạy toán và khoa_học , nhưng cô đã làm được một việc tuyệt_vời là đưa kỹ_năng viêt vào các chủ_đề dạy của mình . Cô yêu_cầu học_sinh tổ_chức một ngày toán_học , và trong ngày đó các em học_sinh viết bài trình_bày cách mình đã giải các vấn_đề khác_nhau ra_sao .
Cô luôn yêu_cầu các học_sinh bắt_đầu câu trả_lời bằng cách lặp lại câu_hỏi và sử_dụng câu đầy_đủ . Đây là cách làm xuất_sắc để hiểu rõ các chủ_đề , và với tư_cách là thầy dạy viết cho học_sinh , thầy đánh_giá cao nỗ_lực ngoại_khóa này của cô đối_với học_sinh của mình nhằm giúp chúng phát_triển kỹ_năng viết .
Mỗi năm ở trường thầy , các học_sinh phải viết một bài luận . Chúng được yêu_cầu đọc một đoạn văn và sau đó trả_lời những câu_hỏi ngắn về đoạn văn ấy . Vào lúc ấy , lớp của thầy lại có nhiều học_sinh yếu về môn luận_văn này . Tuy_nhiên , khi tập viết bài luận_văn như_thế , thầy luôn hướng_dẫn các bạn ấy trả_lời các câu_hỏi theo cách dưới đây :
Thí_dụ :
Trong số các vận_động_viên bóng_rổ , em nghĩ ai là người giỏi nhất , L . hay V . ?
1 . Lặp lại câu_hỏi và thêm phần trả_lời :
Trong số các vận_động_viên bóng_rổ , em nghĩ L . giỏi nhất .
2 . Đưa ra một lý_do vì_sao bạn lại cho là như_thế :
Em nghĩ là L . giỏi nhất bởi_vì bạn ấy ném trái bóng quyết_định chiến_thắng .
3 . Hỗ_trợ câu trả_lời của bạn :
Sở_dĩ bạn ấy ném được trái bóng quyết_định chiến_thắng là vì bạn ấy rất bình_tĩnh trước các áp_lực và có quyết_tâm cao để giành chiến_thắng .
4 . Lặp lại câu_hỏi và đi đến kết_luận :
Bởi_vậy , em nghĩ L . là vận_động_viên bóng_rổ giỏi hơn V .
Dựa trên cái sườn chính này , các học_sinh của thầy đã nhanh_chóng biết viết những câu trả_lời đủ ý và đầy_đủ cho bất_kỳ câu_hỏi nào . Sau khi đã làm_chủ được kỹ_thuật viết này , nhiều học_sinh còn có_thể sử_dụng cách viết cơ_bản này để viết những câu trả_lời sáng_tạo và nhiều ý hơn , nhưng những câu trả_lời của chúng vẫn giữ được một cấu_trúc cân_đối cần_thiết để được đánh_giá là những bài viêt xuât sắc . Nhiều học_sinh lớp khác cũng lấy cách viết này để học_tập . Các bạn ấy đã thử vận_dụng và cũng đạt được những thành_quả tương_tự .
Trong năm đầu_tiên đi dạy , thầy chỉ mới nhận lớp ba tuần_lễ trước khi học_sinh phải làm bài luận kiểm_tra , và rất nhiều học_sinh năm ấy chỉ còn biết ngồi chống bút mà chẳng viết được gì . Hình_ảnh này khiến trái_tim thầy như thắt lại , nhưng mọi việc thầy có_thể làm là mỉm cười động_viên các em ấy hãy cố_gắng . Thầy đã dạy hai lớp 5 của trường , lớp thứ nhất đã đội_sổ trong toàn trường về bài kiểm_tra môn luận vào năm đầu_tiên thầy nhận lớp .
Năm kế_tiếp , thầy quyết_định phải thăng_hạng . Thầy triển_khai cách viết cơ_bản này và thầy_trò cùng miệt_mài thực_tập suốt năm trên mọi loại đề_tài . Đến ngày kiểm_tra môn luận , toàn_bộ học_sinh lớp 5 của thầy đều làm được bài và lớp của thầy được xếp_hạng đầu trong toàn trường . Thậm_chí thầy còn có những học_sinh được xếp_hạng cao bởi các em đã biết lặp lại phần câu_hỏi , rồi đưa ra phần trả_lời , đưa thêm phần hỗ_trợ câu trả_lời của mình , cuối_cùng nhắc lại phần câu_hỏi và đi đến kết_luận .
K . T .

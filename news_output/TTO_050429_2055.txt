﻿ Đà_Nẵng : Phát_hiện đường_dây làm giấy chứng_nhận giả vào đại_học
Theo kết_quả điều_tra , vào tháng 11.2004 , Nguyễn_Thị_Ngọc_Thịnh ( 1950 , trú tại Hưng_Lộc , TP Vinh , Nghệ_An ) đã cấu_kết với Nguyễn_Đình_Thi ( 1940 , trú tại Hà_Tĩnh ) nhận_lời " giúp " 8 học_sinh quê Nghệ_An , Thanh_Hoá vào nhập_học tại trường ĐH Duy_Tân - Đà_Nẵng với giá từ 2 triệu đến 20 triệu đồng / người .
Để thực_hiện hành_vi này , Thịnh cùng đồng_bọn đã làm giả giấy chứng_nhận nguyện_vọng 2 cùng con_dấu của các trường ĐH Sư_phạm Vinh , Học_viện Ngân_hàng Hà_Nội và đã thực_hiện trót_lọt . Tuy_nhiên vụ_việc đã bị công_an TP Đà_Nẵng phát_hiện , 8 sinh_viên trên lập_tức bị đình_chỉ học ngay sau đó .
Được biết đây là một trong số đường_dây sử_dụng giấy chứng_nhận kết_quả tuyển_sinh giả để xét tuyển vào các trường ĐH . Trước đó tại TP Vinh , Công_an Nghệ_An cũng đã phát_hiện trên 100 trường_hợp nhập_học tương_tự . Hiện hồ_sơ vụ_án đã được chuyển cho VKS Đà_Nẵng thụ_lý .
ĐĂNG_NAM

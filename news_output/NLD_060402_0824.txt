﻿ Vũ_điệu mặt_trời
Là chủ_đề của chương_trình biểu_diễn thời_trang , diễn ra_vào đêm 1-4-2006 , tại Nhà_hát TPHCM do Zen_Plaza và Công_ty YKK Việt_Nam phối_hợp tổ_chức .
Chương_trình giới_thiệu 125 mẫu thiết_kế thời_trang mới nhất dành cho mùa_hè của 5 nhà_thiết_kế ( NTK ) . Đêm diễn quy_tụ 25 người_mẫu tên_tuổi .
1 . NTK Kiều_Việt_Liên : Bộ sưu_tập bao_gồm quần_soóc ngắn , soóc lửng , váy ngắn , váy_xòe rộng ... Sử_dụng các gam màu nóng , sáng rực_rỡ như cam , vàng , hồng , trắng với các họa_tiết thêu hoa hải_đường trên nền vải chiffon , satin , nhung sọc mỏng . Bộ sưu_tập là một bản hòa ca màu của nắng hè .
2 . NTK Thanh_Huyền với bộ sưu_tập “ Điều kỳ_diệu của ước_mơ ” : Được thực_hiện trên nền chất_liệu nhẹ với gam màu hồng tím , đỏ_rực của hoa , của nắng , của mặt_trời lúc bình_minh , đem lại cho bạn trẻ cảm_giác lạc_quan và yêu_đời .
3 . NTK Lệ_Hằng : Lại sử_dụng các họa_tiết trang_trí hình hoa_lá , đan_xen những hình mảng mạnh_mẽ , trên nền chất_liệu dày như như cotton , jean ... NTK Lệ_Hằng muốn khắc_họa hình_ảnh dịu_dàng , đằm_thắm nhưng cũng thật mạnh_mẽ và độc_lập của người phụ_nữ .
4 . NTK Quốc_Thành với “ Giai_điệu của những thiên_thần ” . Chủ_đạo của giai_điệu này là những kiểu đầm ngắn . Sử_dụng tông màu trắng chủ_đạo , điểm_xuyết một_chút đỏ cam , trên chất_liệu mềm_mỏng như voan dập ly , ren ... tạo nên những kiểu_dáng lãng_mạn pha chút cổ_điển cho bạn_gái trẻ yêu_thích vẻ nhẹ_nhàng , thanh_lịch .
5 . NTK Nhật_Huy với “ Mặt_trời và trái_đất ” : Chất_liệu chính là cotton , lưới , chiffon in hoa trên hai gam màu nóng - lạnh , tạo nên nét hiện_đại và đầy cá_tính . Áo_khoác nhẹ , mỏng là điểm nhấn tạo nên hình_ảnh những cô_gái tinh_nghịch , quyết_đoán và yêu_thích tự_do . Người_mẫu : Minh_Thư , Tuyết_Trinh , Yến_Nhi , Thanh_Mai , Bảo_Trâm
B . H

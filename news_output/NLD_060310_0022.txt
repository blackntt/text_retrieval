﻿ Cả khu_phố tấn_công “ sa tặc ”
( NLĐ ) - Sáng 9-3 , gần trăm người_dân phường Đông_Giang đã dùng thuyền và huy_động nhiều phương_tiện khác ra sông Hiếu , đoạn chảy ngay trung_tâm thị_xã tỉnh_lỵ Đông_Hà , tấn_công “ sa tặc ” đang ngày_đêm khai_thác cát , có nguy_cơ sạt_lở bờ Bắc sông Hiếu .
Tình_trạng khai_thác cát ở đoạn sông này diễn ra từ trước Tết_Bính_Tuất . Nhiều lần người_dân kiến_nghị nhưng chính_quyền phường Đông_Giang không giải_quyết được . Trả_lời Báo_Người_Lao_Động , ông Võ_Trực_Linh - Giám_đốc Sở Tài nguyên - Môi_trường - cho_biết : “ Chúng_tôi không cấp phép cho khai_thác cát sạn ở sông Hiếu , đoạn qua thị_xã Đông_Hà . Khi biết được thông_tin này , tôi đã cho cán_bộ về kiểm_tra tình_hình để có biện_pháp giải_quyết thỏa_đáng ” .
L.An

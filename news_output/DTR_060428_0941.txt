﻿ Scolari sẽ làm HLV trưởng ĐT Anh ?
( Dân_trí ) - Có_vẻ như LĐBĐ Anh ( FA ) đã tìm ra được người sẽ ngồi vào chiếc ghế mà HLV Sven_Goran_Eriksson bỏ lại sau World_Cup năm nay khi GĐ điều_hành FA , Brian_Barwick đã bay sang Lisbon để thuyết_phục HLV Felipe_Scolari nhận_lời dẫn_dắt đội bóng “ sư_tử ” .
Tại sân_bay Heathrow sau chuyến công_du sang Bồ_Đào_Nha , Barwick phát_biểu với báo_giới : “ Đúng là chúng_tôi đã tới Bồ_Đào_Nha để nói_chuyện với Luiz_Felipe_Scolari và đó là một phần trong chương_trình tìm_kiếm HLV mới cho đội_tuyển ” .
Ông này cho_biết thêm FA sẽ xúc_tiến công_việc một_cách “ nhanh_chóng ” .
Sau khi Guus_Hiddink nhận_lời làm HLV trưởng đội_tuyển Nga , danh_sách ứng_cử_viên thay_thế HLV Eriksson chỉ còn lại Scolari và những nhà cầm_quân người Anh như Alardyce , Curbishley hay McLaren nhưng Big_Phil , biệt_danh của nhà cầm_quân người Brasil là sáng_giá hơn cả .
Với một bảng thành_tích lẫy_lừng và kinh_nghiệm dẫn_dắt nhiều đội bóng lớn , Scolari được coi là một trong những HLV xuất_sắc nhất thế_giới hiện_nay .
Đặc_biệt ở cấp_độ ĐTQG , ông đã từng cùng Brasil VĐTG năm 2002 , dẫn_dắt Bồ_Đào_Nha tới trận chung_kết EURO 2004 và hiện đang cùng đội bóng này lọt vào VCK World_Cup 2006 với thành_tích cực_kỳ ấn_tượng .
Theo nhiều nguồn tin , Brian_Barwick đã được sự đồng_ý của LĐBĐ Bồ_Đào_Nha để mang tới cho Big_Phil một bản hợp_đồng rất hấp_dẫn .
Nếu chấp_thuận về cầm_quân tại Anh sau World_Cup , HLV người Brasil sẽ bỏ_túi mỗi năm 3 triệu bảng ( tương_đương 5,35 triệu USD ) và trở_thành một trong những HLV được trả lương cao nhất thế_giới .
Cho_dù vẫn có những tiếng_nói kêu_gọi FA tuyển_mộ một ứng_cử_viên người Anh cho chức_danh HLV trưởng ĐTQG nhưng xét về thành_tích và khả_năng cầm_quân , không một HLV bản_địa nào có_thể vượt được Big_Phil .
Thêm nữa , khác với Big_Sam hay Mc_Claren , Scolari đã quen với việc dạy_bảo kèm_cặp các ngôi_sao , mà ở ĐT Anh thì lại có quá nhiều cầu_thủ nổi_tiếng . Những_Beck , Rooney hay Lampard chắc_chắn sẽ nể phục Big_Phil hơn bất_cứ một ông thầy người Anh nào .
Chiến_lược gia người Brazil gần đây cũng tuyên_bố mở về khả_năng sẽ chia_tay đội_tuyển Bồ_Đào_Nha sau World_Cup năm nay . Ông nói : “ Tôi là một HLV giỏi và tôi đang làm rất tốt công_việc của mình tại Bồ_Đào_Nha .
Tôi cũng là một con_người thích dịch_chuyển , tôi đã từng làm_việc tại Kuwait , Arab_Saudi , Brasil và tôi có_thể cảm_thấy hạnh_phúc ở bất_kỳ quốc_gia nào ” .
Về cơ_hội trở_thành HLV trưởng đội_tuyển Anh , ông cũng cho_biết mình rất hứng_thú : “ Tôi thích công_việc đó . Thế_nhưng trước_tiên tôi cần phải học tiếng Anh và tìm_hiểu kỹ hơn về tình_hình đội_tuyển đã . ”
Trước những thông_tin trên , hiện các nhà_cái đã đưa ra tỷ_lệ cá_cược mới cho việc Scolari về dẫn_dắt đội bóng đảo_quốc sương_mù từ 16-1 xuống còn 5-2 . Một cuộc thăm_dò mới_đây do BBC thực_hiện với 73000 người cũng cho thấy kết_quả có tới 65% ý_kiến đồng_ý với việc FA chọn Big_Phil .
Văn_Khánh_Theo_Soccernet / Sky_Sports

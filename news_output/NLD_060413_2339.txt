﻿ Gỏi tôm nhúng giấm
Vật_liệu
300 g tôm đất ( hoặc tôm_sú ) ; 1 củ hành_tây ; 100 g phi_lê cá chẽm ; 100g gừng chua ; 200g mực tươi ; 100g kiệu chua ; 100g thịt bò mềm ; 4 trái ớt ; 1 ít rau_răm ; 50g đậu_phộng ; bánh_phồng_tôm ; 1 chén giấm ; 3 trái chanh ; 50g hành phi .
Cách làm
1 . Chuẩn_bị : Tôm lột vỏ ( để đuôi ) , rút chỉ đen , ướp 1/2 muỗng cà_phê muối + 1 muỗng cà_phê tiêu ; cá chẽm rửa muối , thái lát mỏng , ướp chút muối_tiêu ; mực làm sạch , khía ca_rô , thái vừa ăn , ướp 1/2 muỗng cà_phê muối + 1/2 muỗng cà_phê tiêu ; thịt bò thái lát mỏng , ướp 1 muỗng xúp nước_tương + 1/2 muỗng xúp đường . Rau_răm thái sợi ; bánh_phồng chiên nở vàng ; hành_tây thái mỏng , ngâm nước_đá ( khi trộn vắt ráo ) ; gừng thái sợi ; kiệu chẻ mỏng ; ớt thái sợi ; đậu_phộng rang vàng , bóc vỏ , giã nhuyễn .
2 . Chế_biến : Nấu sôi giấm , nêm 2 muỗng xúp đường +1 / 2 muỗng xúp muối + 2 muỗng xúp dầu . Lần_lượt để tôm , cá , mực , thịt vào nhúng cho chín , vớt ra rổ để ráo nước . Trộn chung tôm , cá , mực , thịt , hành_tây , gừng , kiệu , ớt , rau_răm . Nêm thêm nước_cốt chanh , chút muối , đường cho có vị chua_ngọt .
Pha nước_mắm : 4 muỗng xúp nước chanh + 6 muỗng xúp đường + 4 muỗng xúp nước_mắm ngon . Khuấy tan + tỏi ớt bằm nhuyễn . 3 . Trình_bày : Cho gỏi ra dĩa , điểm cà , ớt tỉa hoa cho đẹp , trên rắc đậu_phộng , hành phi . Dùng kèm nước_mắm chua_ngọt , bánh_phồng_tôm .
Chuyên_gia nấu_ăn NẤU_ĂN_HỒNG_NHO ( GIẢNG_VIÊN_NHÀ_VH_PHỤ_NỮ_TPHCM )

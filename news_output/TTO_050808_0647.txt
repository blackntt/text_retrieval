﻿ Colombia : Carnaval hồi_sinh sau 74 năm
Khoảng 2.000 nghệ_sĩ múa đại_diện cho 30 trường_phái văn_hóa dân_gian khác_nhau , trong đó có hai nghệ_sĩ múa salsa nổi_tiếng nhất hiện_nay là Bobby_Cruz ( người Mỹ ) và Richie_Rey ( Puerto_Rico ) tham_dự Carnaval .
Ngày hội năm nay mang tên " Carnaval của sự đa_dạng " , thể_hiện mối giao_lưu văn_hóa giữa các dân_tộc cùng chung ngôn_ngữ và cội_nguồn , sẽ biểu_diễn nhiều điệu múa truyền_thống của Colombia và các dân_tộc Mỹ_Latin như el fandango , currulao , pasillo , vallenato và bambuco .
Carnaval sẽ diễn ra_vào các ngày cuối tuần trong tháng tám hằng năm . Ước_tính khoảng trên 2 triệu lượt người trong 7 triệu dân của Bogota tham_gia Carnaval này .
N . T . ĐA ( Theo AP , El_Diario )

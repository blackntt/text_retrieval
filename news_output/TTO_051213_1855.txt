﻿ Lập bản_đồ toàn_cầu các loài có nguy_cơ tuyệt_chủng
Danh_sách này do một nhóm liên_minh các nhà bảo_tồn thiên_nhiên thực_hiện , đã khoanh_vùng gần 800 loài mà họ cho là sẽ biến_mất sớm nếu_không có các biện_pháp bảo_vệ khẩn_cấp . Hầu_hết trong số 800 loài này hiện được phát_hiện chỉ tại một khu_vực , chủ_yếu là vùng nhiệt_đới .
Trên tạp_chí Khoa_học , các nhà_nghiên_cứu cho_biết việc bảo_vệ các vị_trí này sẽ tốn một khoản chi_phí gần 1.000 USD / 1 năm .
“ Đây là một tập_hợp đầy_đủ các loài đang bị đe_dọa tuyệt_chủng ” , Stuart_Butchart , điều_phối_viên chương_trình các loài toàn_cầu thuộc tổ_chức BirdLife , một trong các tổ_chức tham_gia lập bản_đồ nói trên cho_biết . “ Hầu_hết chúng đang sống ở một vị_trí và do_đó có nguy_cơ bị tổn_hại cao bởi các tác_động của con_người . Nếu chúng_ta không bảo_vệ chúng , chắc_chắn chúng sẽ tuyệt_chủng ” , ông nói .
Các nhà_khoa_học đã lập bản_đồ nói trên_dưới sự chỉ_đạo của một tổ_chức khá mới - Liên_minh vì mục_tiêu không tuyệt_chủng ( AZE ) . Từ các cơ_sở_dữ_liệu thu_thập được , họ đã lập nên một danh_sách gồm 595 khu_vực , mỗi khu_vực có ít_nhất một loài được xếp vào diện “ gặp nguy_hiểm ” hoặc “ cực_kỳ nguy_hiểm ” , theo tiêu_chuẩn sách Đỏ về các sinh_vật bị đe_dọa .
Cơ_hội cuối_cùng nhìn thấy chúng ...
Mỗi khu_vực hoặc là địa_điểm sinh_sống duy_nhất của sinh_vật đó , hoặc chứa ít_nhất 95% quần_thể được biết đến của nó . Một_số khu_vực có nhiều hơn 1 loài gặp nguy_hiểm .
Do không phải mọi sinh_vật trên trái_đất đều đã được nghiên_cứu hay được xác_định , nên danh_sách 794 loài chỉ bao_gồm các loài chim , động_vật_có_vú , loài lưỡng_cư , cây có quả hình_nón và một_số nhóm bò_sát . Phần_lớn các vị_trí quan_trọng thuộc những vùng nhiệt_đới , và hầu_hết là tại các nước_đang_phát_triển .
Theo John_Fa , giám_đốc khoa_học bảo_tồn tại Durrell_Wildlife , làm_việc với các cộng_đồng là chìa_khóa cho các chiến_lược bảo_tồn ở những vùng này .
“ Chiến_lược của chúng_tôi là không_chỉ tập_trung vào động_vật mà_còn làm_việc bên_cạnh các cộng_đồng địa_phương , làm điều gì đó để hỗ_trợ đời_sống của họ ” .
Nhóm của AZE đã tính_toán chi_phí cho việc bảo_tồn mỗi điểm trong số 595 khu_vực nhạy_cảm , và kết_luận chi_phí hàng năm này rất khác_nhau , từ 470 USD đến 3,5 triệu USD .
Có 13 nhóm đứng đằng sau bản báo_cáo trên , trong đó có Hiệp_hội động_vật_học London , Tổ_chức bảo_tồn quốc_tế , Nhóm bảo_tồn chim nước Mỹ …
T.VY ( Theo BBC )

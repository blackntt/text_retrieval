﻿ Giã_từ một ngôi làng …
Làm_sao không đau_lòng khi nhìn những ngôi nhà bề_thế - cơ_ngơi cóp nhóp mồ_hôi_nước_mắt cả đời người như_thế_này đã phải vứt bỏ lại .
Trên mảnh vườn nhà chị Trần_Thị_Gái , em Nguyễn_Văn_Hóa , 8 tuổi học lớp 2 ngồi trên đống cột kèo vừa dỡ ra , sau lưng em là nền nhà trơ những lỗ cột vừa được tháo lên . Bố của em bị mất cách nay ba năm vì cuốc phải bom khi đi đào phế_liệu về bán . Chị Gái một_mình tật_bệnh nuôi hai đứa con , Hóa là con_thứ hai . Lần dời nhà này không biết rồi cuộc_sống của ba mẹ_con đến_nơi ở mới sẽ thế_nào ?
Tại khu lều bạt được dựng vội cho người_dân ở tạm có những em bé vừa sinh chưa tròn tháng
Từ trên mái đồi này nhìn về làng không ai_ngờ ngôi làng ngày hôm_qua , hôm_kia đông vui thế giờ đã hoang_tàn . Huyện Cam_Lộ đã nhanh_chóng quyết_định di_dời tất_cả làng ra vùng Động_Mối cách làng cũ chừng hơn 2 Km . Những hộ bị thiệt_hại nặng sẽ đến trước , được chia lô ( 750-1.000m 2 / hộ ) để dựng nhà , sau đó sẽ đến lượt các hộ còn lại .
Làng_Tân_Hiệp ngày hôm_qua sẽ không còn nữa , và người_dân trong làng lại nhen_nhóm gầy_dựng lại một ngôi làng khác từ vùng_đất hoang Động_Mối này …
LÊ_ĐỨC_DỤC

﻿ Tuyết tan ở Bắc_Cực có_thể mở_đường cho tàu_thuyền đi_lại
Các năng_lượng hóa_thạch là nguyên_nhân gây nên khí hơi nóng trong không_khí , đồng_thời trong vài thập_kỷ tới Bắc_Cực được cảnh_báo sẽ ấm hơn gấp hai lần so với những nơi khác trên thế_giới .
Cuộc khảo_sát ước_lượng rằng khi tan chảy sông băng thì tàu_bè có_thể đi_lại khoảng 120 ngày/năm vào năm 2100 , so với 30 ngày như năm 2000 trên tuyến đường Biển_Bắc dọc theo bờ biển của Nga .
Tuy_nhiên , sự nguy_hiểm của tảng băng trôi và chi_phí cao của tàu_thuyền sẽ ngăn_chặn tàu qua các vùng địa_cực , các nhà_khoa_học từ Mỹ , Nga , Canada , Phần_Lan , Thụy_Điển , Na_Uy , Iceland và Đan_Mạch đang họp hội_nghị tại Iceland cho_biết .
QUỐC_DŨNG ( Theo Tân_Hoa_Xã )

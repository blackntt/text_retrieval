﻿ Triệu_tập cầu_thủ Bùi_Sỹ_Thành
Bùi_Sỹ_Thành , cựu tiền_vệ CLB_CA_TPHCM thừa_nhận việc nhận 65 triệu đồng từ tay Nguyễn_Phi_Hùng , cựu cầu_thủ P.SLNA ...
Trong diễn_biến nghi_án " mua " chức vô_địch của CLB P . SLNA trong mùa giải 2000/01 , ngày 10/1 , Bùi_Sỹ_Thành đến văn_phòng phía Nam của Cơ_quan CSĐT , Bộ Công an theo lệnh triệu_tập để làm rõ hành_vi nhận 65 triệu đồng từ tay Nguyễn_Phi_Hùng .
Ban_đầu , Bùi_Sỹ_Thành đã thừa_nhận hành_vi nhận tiền từ Nguyễn_Phi_Hùng vào đêm 27/5/2001 , sau trận cầu giữa hai đội SLNA - Công_an TPHCM kết_thúc vào chiều cùng ngày , với tỉ_số 4-3 nghiêng về SLNA . Số tiền này được trích ra 5 triệu đồng để chi cho bữa ăn_chơi của cầu_thủ 2 đội tại một vũ_trường ở thành_phố Vinh , Nghệ_An .
Số tiền còn lại được Bùi_Sỹ_Thành giao lại cho ông Hoàng_Trọng_Thanh , Trưởng_đoàn bóng_đá CLB_CA_TPHCM vào thời_điểm đó . Khi vụ_việc bị " đổ_bể " , ông Thanh nhận được số tiền trên và tra_hỏi nguồn_gốc , ông Thanh đã trả lại số tiền trên cho CLB P.SLNA .
Tại CQĐT vào ngày 10/1 , Bùi_Sỹ_Thành đã không cung_cấp thêm cho điều_tra_viên biết động_cơ của hành_vi đưa và nhận số tiền 65 triệu đồng của 2 cầu_thủ này .
Cùng ngày ( 10/1 ) , Cơ_quan CSĐT đã triệu_tập Trần_Thị_Phương , Kế_toán_trưởng của CLB P . SLNA đến CQĐT để làm rõ các khoản thu_chi của CLB này trong thời_điểm diễn ra mùa giải vô_địch quốc_gia 2000/01 .
Về diễn_biến vụ_án bán_độ của một_số cầu_thủ U23 Việt_Nam tại SEA Games 23 , cho_đến cuối ngày 10/1 , VKSND tối cáo vẫn chưa phê_chuẩn lệnh khởi_tố bị_can đối_với Phước_Vĩnh , Bật_Hiếu , Văn_Trương . Trước đó , Cơ_quan CSĐT , Bộ Công an đã ra_lệnh khởi_tố bị_can và đề_nghị VKSNDTC phê_chuẩn .
Tuy_nhiên , VKSNDTC yêu_cầu CQĐT cần làm rõ và cung_cấp thêm một_số chứng_cứ thuyết_phục , để VKSNDTC phê_chuẩn lệnh khởi_tố bị_can .
Theo Phan_Trần_Vietnamnet

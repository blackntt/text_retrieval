﻿ Bệnh đau nửa đầu ở phụ_nữ
Theo ước_tính , cứ 100 người thì_có gần 11 người bị chứng đau nửa đầu hành_hạ và 3 trong 4 người bị đau nửa đầu là phụ_nữ , đặc_biệt là phụ_nữ trong nhóm tuổi 35-45 .
Cơn đau nửa đầu có_thể kéo_dài 3-72 giờ , và những cơn đau tái_phát có_thể gây mệt_mỏi và làm hạn_chế nghiêm_trọng các hoạt_động hàng ngày của bạn .
Để giải_tỏa cơn đau nửa đầu , bạn có_thể áp_dụng nhiều cách , từ dùng thuốc cho_đến thay_đổi lối sống của mình .
Bệnh đau nửa đầu
Không như chứng đau_đầu thông_thường hay đau_đầu do lạnh , bệnh đau nửa đầu thường rất khó_chịu và hay tái đi tái lại và thường đi kèm với buồn_nôn .
Bệnh bắt_đầu bằng cơn đau một bên đầu và có_thể lan sang cả hai bên và có_thể cản_trở nghiêm_trọng đến sinh_hoạt hàng ngày của bệnh_nhân . Một_số bệnh_nhân có một_vài dấu_hiệu báo trước trước khi bị cơn đau nửa đầu tấn_công như mạch đập nhanh , hoa mắt …
Người_ta cho rằng cơn đau nửa đầu là do sự thay_đổi trong dòng máu chảy về não . Bệnh cũng có_thể do stress hay các yếu_tố khác .
Triệu_chứng bệnh
Các triệu_chứng của bệnh đau nửa đầu có_thể rất khác_nhau và với mức dữ_dội khác_nhau tuỳ từng người . Bệnh cũng có_thể do di_truyền và vì_vậy có_thể gặp ở nhiều gia_đình .
Các triệu_chứng thường gặp nhất của bệnh :
- Đau mạnh ở một hay cả hai bên đầu .
- Buồn_nôn và nôn .
- Cứng cơ cổ .
- Nhạy_cảm với âm_thanh và ánh_sáng .
- Ảo_giác như thấy các đường ziczac và ánh_sáng loá .
- Mệt_mỏi , dễ cáu_kỉnh và lầm_lẫn .
- Đổi tính .
- Tăng số lần đi_tiểu và tiêu_chảy .
Nguyên_nhân gây bệnh
Những thay_đổi thông_thường của thời_tiết hay độ cao có_thể gây đau nửa đầu . Giấc_ngủ thất_thường ( quá nhiều hay quá ít ) , sự gián_đoạn của giấc_ngủ thường_ngày cũng có_thể là nguyên_nhân gây bệnh .
Mùi hương quá đậm , đèn quá sáng hay tiếng ồn quá mức cũng được cho là gây đau nửa đầu .
Phụ_nữ thường bị đau nửa đầu nhiều hơn vào chu_kỳ kinh_nguyệt , do viên tránh thai hay do hormone .
Giảm nhẹ cơn đau nửa đầu
Thuốc : thuốc để trị chứng đau nửa đầu có_thể là ergotamine , sumatriptan và rizatriptan , isometheptenes .
Ngoài_ra có_thể dùng các loại thuốc ngăn_ngừa để giảm tần_số cơn đau nửa đầu . Thuốc giảm đau thường được dùng khi bắt_đầu cơn đau gồm có aspirin , acetaminophen và ibuprofen .
Đối_với phụ_nữ , liệu_pháp hormone có_thể giúp giảm cơn đau nửa đầu có liên_quan đến chu_kỳ kinh_nguyệt của họ .
Thay_đổi lối sống : ăn chế_độ ăn lành_mạnh và giữ cho cân nặng của bạn dưới tầm kiểm_soát .
Những người béo phì thường có khuynh_hướng dễ bị đau nửa đầu hơn . Hút thuốc và uống rượu có_thể dẫn đến cơn đau nửa đầu nghiêm_trọng , vì_vậy hãy tránh xa chúng .
Nếu trong cuộc_sống không_thể tránh khỏi stress , bạn hãy học cách khắc_phục nó tốt hơn . Hãy học cách thư_giãn và ngủ tốt hơn .
Tập_thể_dục thường_xuyên , đi bộ hay bơi_lội có_thể giúp giảm cơn đau nửa đầu . Một_số món ăn cũng có_thể gây đau nửa đầu , hãy hạn_chế chúng ra khỏi khẩu_phần của bạn . Khi bị đau , bạn hãy cố nghỉ_ngơi trong một căn phòng tối và mát hay đi tắm dưới vòi_sen . Đắp khăn lạnh lên trán cũng có_thể giúp giảm nhẹ sự khó_chịu . Một ly nước bổ_dưỡng , mátxa cổ , sau gáy và cơ … cũng có_thể có_ích cho bạn .
Theo Tuổi_Trẻ

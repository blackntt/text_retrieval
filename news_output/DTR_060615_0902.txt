﻿ NSƯT Vũ_Dậu : Mừng ca_sĩ Khánh_Linh sinh con_trai
Chưa kịp cho ra_mắt album riêng của gia_đình sau 40 năm hoạt_động ca_hát , NSƯT Vũ_Dậu đã tất_bật “ lên chức ” bà_ngoại . Cô út Khánh_Linh mới sinh con_trai đầu_lòng , đối_với bà đây là tin_mừng lớn nhất .
Cùng_với niềm_vui rộn_ràng , niềm hạnh_phúc lớn_lao , bà_ngoại Vũ_Dậu lại vất_vả hơn trong công_việc . Ngày nào bà cũng chuẩn_bị sữa , thuốc mang sang cho cháu ngoại . Được tắm và cho cháu ăn là việc bà vui nhất , làm không thấy mệt .
Bé_Anh_Khoa sinh nặng 2,8 kg , hiện_nay được hơn 4 kg . Mới được một tháng nhưng trông bé rất lanh_lợi , đã biết hóng chuyện . “ Cháu_Khoa ngủ suốt ngày , tỉnh dậy là đòi ăn , trông nó đáng yêu lắm ” , nghệ_sĩ Vũ_Dậu tâm_sự .
Không_chỉ riêng bà_ngoại Vũ_Dậu , ông ngoại Ngọc_Hướng và bác Ngọc_Châu cũng rất cưng cháu . Nhạc_sĩ Ngọc_Châu nhận thấy “ thằng nhóc ” rất nhạy_cảm với âm_nhạc , anh tiên_đoán sau_này Khoa sẽ đi theo con đường ca_hát . Cả ông_bà cũng hi_vọng Khoa sẽ theo con đường nghệ_thuật như truyền_thống của gia_đình .
Về_Khánh_Linh , nghệ_sĩ Vũ_Dậu cho_biết dù mới sinh con nhưng “ hoạ_mi ” đã muốn “ hót ” lắm rồi . Cô rất nhớ sân_khấu và sẽ gặp lại khán_giả vào tháng 7 tới . Hàn_Nguyệt

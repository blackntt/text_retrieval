﻿ Một cuộc chat đặc_biệt !
Những câu_hỏi liên_tiếp được đưa ra để cùng nhau trao_đổi . Trên 50 thành_viên ( 30 SV khoa địa_lý của Trường ĐH_KHXH & amp ; NV - ĐHQG_TP . HCM , 16 thành_viên nhóm VnGG ở Ireland , Pháp , Đức , Mỹ … cùng năm khách mời là các thầy_cô Trường ĐH_KHXH & amp ; NV_TP . HCM ) và những người quan_tâm về lĩnh_vực này đã cùng tham_gia trao_đổi trực_tuyến trong bốn phòng chat của Yahoo_Messenger .
Hàng_loạt thắc_mắc xoay quanh các nội_dung như : các câu_hỏi tổng_quan về KHTĐ , học_tập và nghiên_cứu KHTĐ trong và ngoài nước , những thông_tin về nhóm VnGG ... Không_khí thật sôi_nổi trong tất_cả phòng chat của buổi giao_lưu .
Bạn_Trần_Thị_Đoan_Trinh , SV năm 2 khoa Địa_lý của Trường ĐH_KHXH & amp ; NV , tươi_cười : “ Trinh đến với buổi giao_lưu này phần_nhiều vì tò_mò ngành mình đang học . Trước giờ Trinh rất ít chat , hôm_nay lại chat hơn hai giờ đồng_hồ để lắng_nghe nhiều người nói cũng_như được bày_tỏ suy_nghĩ của mình về Trái_đất … ” .
Cuộc trò_chuyện trực_tuyến kết_thúc trễ hơn 30 phút . Dự_kiến toàn_bộ nội_dung buổi giao_lưu trực_tuyến sẽ được tường_thuật lại trên website của nhóm VnGG ở địa_chỉ www . vngg.net cuối tuần này .
BÍCH_DẬU

﻿ Bom ở Baghdad giết chết chủ_tịch Hội_đồng điều_hành Iraq
Ông Mohammad , một người theo Hồi_giáo Shiite , ngồi trong chiếc xe cuối_cùng của đoàn xe năm chiếc dừng chờ tại chốt kiểm_soát để tiến vào " khu_vực xanh " của khu nhà thì quả bom phát_nổ khiến hơn một chục chiếc xe bị hư_hại . Trong số_nhiều người bị_thương , có hai lính Mỹ .
Vụ tấn_công này đã làm suy_yếu chính_quyền do Mỹ hậu_thuẫn khi chỉ còn sáu tuần nữa_là người Iraq được chuyển_giao quyền_lực . Ông Mohammad là một trong chín thành_viên Hội_đồng điều_hành giữ chức chủ_tịch luân_phiên ( một tháng ) và cũng là chủ_tịch Đảng_Hồi giáo Dawa ở Basra .
Lính_Mỹ tại hiện_trường vụ đánh bom Trước_đây , tháng 9-2003 , một trong ba thành_viên nữ của hội_đồng 25 thành_viên này là bà Aqila al - Hashemi đã bị bắn .
Trong khi đó tại Mỹ , vụ bê_bối ngược_đãi tù_nhân Iraq đã được chuyển sang nghi_vấn liệu có phải chính Nhà_Trắng đã tạo ra một nền_tảng pháp_lý mở_đường cho việc này hay không .
Vài tháng sau vụ tấn_công 11-9 , cố_vấn Nhà_Trắng_Alberto_Gonzales có gửi Tổng_thống Bush một giác_thư về vấn_đề chống khủng_bố và cho rằng những qui_định của Công_ước Genève không còn phù_hợp với tình_hình tra_khảo tù_nhân đối_phương .
Theo tạp_chí Newsweek , Ngoại_trưởng Colin_Powell đã vô_cùng tức_giận khi đọc giác_thư trên và cảnh_báo Tổng_thống Bush rằng kiểu tra_khảo tù_nhân mới sẽ " thay_đổi hoàn_toàn chính_sách của Mỹ trong một thập_kỷ " và " trả_giá đắt vì phản_ứng của quốc_tế " .
Trên_Đài NBC , trong một chuyển_biến chính_sách quan_trọng , Ngoại_trưởng Powell nói Mỹ có_thể chấp_nhận một nhà_nước Hồi_giáo tại Iraq nếu đó là ý_muốn của người_dân tại đây .
Thời_gian qua Mỹ luôn tìm cách ngăn_cản các giáo_sĩ Hồi_giáo Shiite của Iraq đưa vào hiến_pháp lâm_thời các điều_khoản tạo điều_kiện để Hồi_giáo trở_thành nền_tảng cho các hoạt_động lập_pháp .
S . N .

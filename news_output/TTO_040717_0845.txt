﻿ Viettel sẽ tính cước theo block 6 giây
Ngoài_ra , Viettel cũng đề_nghị ban_hành các gói cước cho sáu đối_tượng khách_hàng : gia_đình , cơ_quan , tổ_chức , người có thu_nhập thấp ... được hưởng mức giá ưu_đãi giảm khoảng 15% so với mức thông_thường . Riêng gói dành cho những người thu_nhập thấp ( gồm học_sinh , sinh_viên ) , mức cước sẽ là 60.000 đồng/tháng . Khách_hàng sẽ gọi được 60 block 6 giây và một lượng tin nhắn trị_giá 60.000 đồng ( 400 đồng/tin ) . Cước thuê_bao của Viettel hiện_tại là 45.000 đồng/tháng .
Đại_diện Viettel cho_biết dịch_vụ này vẫn trong thời_gian thử_nghiệm có thu phí , nên chỉ có nhân_viên , người_thân , bạn_bè quân_nhân mới có_thể đăng_ký sử_dụng . Hiện_Viettel đã phủ_sóng 43 tỉnh_thành , dự_kiến sẽ tăng lên 64 tỉnh_thành vào cuối năm 2004 .
ĐÔNG_HẢI

﻿ Thái_Lan đề_nghị ASEAN áp_dụng mức thuế chung cho ôtô
Dự_kiến , Tổng_cục Hải_quan Thái_Lan sẽ cùng_với các đối_tác ở các nước thành_viên ASEAN nghiên_cứu và đề_xuất mức thuế nhập_khẩu ôtô chung áp_dụng trong khu_vực .
Về phía Thái_Lan , ông Sathit_Limpongpan cho_biết những thỏa_thuận khu_vực thương_mại tự_do mà chính_phủ nước này đã và sẽ ký với các đối_tác thương_mại cũng sẽ hướng tới việc giảm thuế nhập_khẩu ôtô .
Theo ông Sathit_Limpongpan , thuế nhập_khẩu ôtô của các quốc_gia thành_viên ASEAN có_thể sẽ giảm còn 0% vào năm 2010-2011 . Động_thái này phù_hợp với Thỏa_thuận Khu_vực Thương_mại Tự_do ASEAN ( AFTA ) .
Theo TTXVN

﻿ Tào_Dĩnh - người_đẹp đa_tài
Qua hàng_loạt bộ phim như " Văn_Thành_Công_Chúa " , " Huyện thái gia 9 tuổi " , " Trung_Hoa nhi_nữ " ... nữ diễn_viên kiêm ca_sĩ , người_mẫu và MC Tào_Dĩnh đã tạo được nhiều ấn_tượng sâu_đậm với khán_giả .
Gần đây nhất , cô tiếp_tục xuất_hiện trong bộ phim Nữ luật_sư xinh_đẹp , đang phát_sóng trên kênh BTV . Cuộc_sống của người_đẹp đa_tài này có rất nhiều điều thú_vị , hãy nghe cô bật_mí .
Thu_hoạch lớn nhất của bạn từ khi vào nghệ_thuật là gì ?
Tôi đã lăn_lộn trong nghề này hơn 10 năm . Thu_hoạch lớn nhất của tôi là có_thể tìm được một ngôn_ngữ để giải_phóng mình , để người khác hiểu được tâm_tư tình_cảm của mình và cùng chia_sẻ mọi niềm_vui .
Bạn dự_định làm_gì 10 năm sau ? Có bỏ nghề không ?
Hiện_giờ rất khó đoán , nhưng 10 năm sau nhất_định tôi vẫn tràn_trề sức_sống . Tôi cho rằng với con_người , chỉ cần trái_tim còn tươi_trẻ , tuyệt_đối không nên để mình quá nhàn_nhã , không làm_gì , cũng không nghĩ_ngợi gì . Khả_năng tiếp_thu của con_người không_thể dừng lại vì tuổi_tác .
Bạn có_thể tiết_lộ bí_quyết để gìn_giữ vóc_dáng và làn da đẹp của mình không ?
Tôi thường_xuyên tập_thể_dục nhưng không có nhiều thời_gian để chăm_sóc da . Thân_hình tôi mảnh_khảnh như_vậy là may_mắn được thừa_hưởng từ mẹ . Thật_tình là tôi không theo chế_độ kiêng_khem gì , thậm_chí còn rất thích ăn đồ cay .
Nói về ngoại_hình của mình , bạn ưng_ý nhất điểm nào và có mong_muốn thay_đổi gì không ?
Tôi thích nhất ... cái tai vì đường_nét rõ_ràng , không chê vào đâu được . Còn các điểm khác ít_nhiều đều ... có khiếm_khuyết . Chán nhất_là cặp đùi quá gầy , khiến tôi rất khó_khăn khi đi mua quần jeans . Thế mới biết gầy quá cũng không đẹp . Tôi cũng ước mình có đựơc một_đôi vai rộng hơn .
Ngoài công_việc , bạn thường làm_gì khi rảnh_rỗi ?
Thực_ra , đối_với tôi , công_việc cũng_như nghỉ_ngơi thư_giãn . Tôi rất vui vì chúng đều là cuộc_sống của tôi . Tôi vẫn còn trẻ , không cần phải lo_lắng về tương_lai . Tôi rất thích họp_mặt các bạn , đi hát karaoke , đi shopping ... Tôi không chịu được nếu mình quá nhàn_rỗi . Ở nhà , tôi cũng có rất nhiều việc để làm như đọc sách , xem phim , nấu_ăn , ngủ lúc_nào tôi cũng bận !
Bạn có thói_quen đi du_lịch không ? Bạn thích đến những đâu ?
Tôi thường đi du_lịch cùng bạn_bè . Thực_ra mỗi khi đi làm_việc ở nơi xa , tôi đều coi đó như đi du_lịch . Tôi khao_khát nhất_là một chuyến du_hành đến ... các vì_sao , đương_nhiên cơ_hội rất hiếm_hoi . Tuy_nhiên , từ sau khi Trung_Quốc lên được vũ_trụ , tôi càng mong_muốn mình đến được những miền đất lạ .
Trong cuộc_sống hiện_nay , điều gì khiến bạn vui_sướng nhất ?
Nếu phát_hành được CD mới là vui_sướng nhất , vì đến giờ tôi vẫn chưa_thể dốc hết_sức để làm album riêng và đạt được như_ý . Tôi hy_vọng có một gia_đình ổn_định , quan_trọng nhất_là phải có một đứa con . Tôi không có yêu_cầu quá cao về bạn_đời . Sau một_số tan_vỡ tình_cảm , tâm_trạng tôi giờ_đây rất bình_thản , không đau_khổ , cũng không tránh_né . Hãy cứ để duyên_phận đến tự_nhiên . Theo Mỹ_Phẩm

﻿ Không đại_học , ai tuyển chúng_tôi ? !
TT - Đó là vấn_đề mà không ít bạn trẻ đặt ra đầy bức_xúc trong hơn 600 ý_kiến tham_gia diễn_đàn sau tám ngày mở_cửa .
Đặc_biệt , sau ý_kiến của một giám_đốc nhân_sự về tuyển người trên diễn_đàn hôm_qua , có bạn trẻ đã thẳng_thắn đặt câu_hỏi : “ Có bao_nhiêu nhà tuyển_dụng của chúng_ta có suy_nghĩ và hành_xử như_thế ? ” .
ĐH : tiêu_chuẩn đầu_tiên
Theo tôi , với tình_hình hiện_tại ở VN , con đường đậu ĐH là duy_nhất . Vì_sao ? Có nhiều lý_do , trước_hết nếu chúng_ta thử nhìn sơ qua các thông_báo tuyển_dụng trên các báo , đa_số vị_trí tuyển_dụng đều đòi_hỏi phải có bằng ĐH trở_lên . Điều này có_thể được giải_thích là vì các nhà tuyển_dụng , đặc_biệt là các nhân_viên nhân_sự của các công_ty , không nắm rõ vị_trí họ tuyển_dụng cần những điều_kiện nào thích_hợp .
Theo họ , yêu_cầu trình_độ ĐH trở_lên là thượng_sách , thừa còn hơn thiếu ( ví_dụ công_ty họ cần tuyển nhân_viên quản_trị mạng - theo tôi vị_trí này không cần tốt_nghiệp ĐH , chỉ cần có bằng kỹ_thuật_viên về mạng là đủ ) .
Hơn_nữa , có_thể vì các bằng cao_đẳng hay trung_học nghề không có được một “ thương_hiệu ” riêng để đảm_bảo độ tin_cậy về chất_lượng của các ứng_viên cho các nhà tuyển_dụng . Với các trường_hợp này , các bạn không có bằng ĐH sẽ không có cơ_hội được chứng_tỏ năng_lực của mình đối_với những vị_trí như_thế .
TRƯƠNG_ANH_TUẤN ( irvingtruong @ ... )

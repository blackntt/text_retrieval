﻿ Khai_mạc lễ_hội 5 năm di_sản Hội_An - Mỹ_Sơn
Sân_khấu dựng trên sông , thiết_kế bằng những chiếc đèn_lồng theo mô_hình chùa Cầu và đền tháp Mỹ_Sơn . Nhiều du_khách cho_biết họ bị thu_hút với hình_ảnh những chiếc thuyền lướt bên nhau rợp cờ hoa . Âm_nhạc cũng là một trong những điểm nhấn ấn tựơng với những điệu hò , những làn_điệu Chămpa dìu_dặt , âm khúc Sakura ( Nhật_Bản ) da_diết và cả âm_thanh rộn_ràng của dòng nhạc phương Tây .
Tuy_nhiên , màu_sắc chủ_đạo của sân_khấu ( vàng , đỏ , xanh ) đã gây thất_vọng cho khán_giả . Dù_vậy , chương_trình khai_mạc đã nhấn_mạnh được sự đóng_góp lớn_lao của nhân_dân Quảng_Nam trong việc bảo_tồn hai di_sản .
Trước đó , vào lúc 16g , bên bờ sông Hoài cũng diễn ra triển_lãm ảnh về phố cổ Hội_An ( CLB nữ Hải_Âu TPHCM và CLB nhiếp_ảnh Hội_An ) và triển_lãm gốm nghệ_thuật thu_hút nhiều du_khách .
Những màn pháo_hoa rực_rỡ đã khép lại , đêm khai_mạc lễ_hội còn nhiều hứa_hẹn thú_vị .
PVTTO

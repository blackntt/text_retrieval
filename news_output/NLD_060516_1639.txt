﻿ Kewell bị loại vì chấn_thương
Kewell trong màu áo Liverpool_Song rất may cho các chú Kanguru là tiền_vệ năng_động này chỉ bị gạch tên khỏi danh_sách tuyển_thủ tham_dự trận giao_hữu với ĐT Hy_Lạp tại Melbourne vào cuối tháng này .
Tuyển_thủ Australia 27 tuổi này đã phải khập_khiễng rời sân ngay đầu hiệp 2 trận chung_kết Cúp FA cuối tuần qua do giãn dây_chằng háng .
Lập_tức , bác sỹ ĐT Australia_Les_Gelis đã khám kỹ_lưỡng cho anh và thông_báo Kewell phải dưỡng_thương trong 3 tuần .
Thay_vì trở_về Australia tập_trung cùng đồng_đội , Kewell được phép ở lại châu Âu để điều_trị dưới sự chăm_sóc của cả bác sỹ CLB Liverpool lẫn ĐT Australia .
Theo lịch_trình , đội quân của HLV Guus_Hiddink sẽ có trận giao_hữu với ĐKVĐ châu Âu_Hy_Lạp tại Melbourne , ngày 25-5 , bay sang châu Âu đá tiếp trận thứ 2 cùng ĐT Hà_Lan ngày 4-6 tại Rotterdam và cuối_cùng gặp Liechtenstein tại Ulm , Đức ngày 7-6 . Dù chấn_thương mới xảy_ra với Kewell song LĐBĐ Australia cho_biết họ không có kế_hoạch gọi bất_kỳ cầu_thủ dự_bị nào bổ_sung cho danh_sách 23 tuyển_thủ được HLV Guus_Hiddink công_bố cuối tuần trước .
Theo Tiền_Phong

﻿ Nakata bất_ngờ giải_nghệ
Tiền_vệ quốc_tế người Nhật_Bản_Hidetoshi_Nakata , một trong những cầu_thủ châu Á nổi_tiếng nhất thế_kỷ XX , đã bất_ngờ tuyên_bố giải_nghệ dù mới bước sang tuổi 29 .
Người dẫn_dắt lối chơi của đương_kim vô_địch châu Á_Nhật_Bản đã ra sân ngay từ đầu trong đội_hình của Zico trong cả 3 trận đấu của đội_tuyển xứ mặt_trời mọc tại World_Cup 2006 . Anh vẫn thể_hiện được tầm ảnh_hưởng tới lối đá và tinh_thần của các đồng_đội .
Hai lần được tôn_vinh là Cầu_thủ Châu Á xuất_sắc nhất năm , Nakata luôn chứng_tỏ mình là biểu_tượng cho sự vươn lên mạnh_mẽ của bóng_đá châu_lục lớn nhất thế_giới . Anh là một ngôi_sao ở thế_hệ của mình , người gắn_bó phần_lớn sự_nghiệp cầu_thủ tại châu Âu xa_xôi và đặc_biệt là tại giải đấu khắc_nghiệt nhất : Serie A .
Nakata phát_biểu trên website của mình : " Tôi đã quyết_định nửa năm trước rằng mình sẽ chấm_dứt sự_nghiệp cầu_thủ chuyên_nghiệp sau World_Cup 2006 . Tôi sẽ không_bao_giờ ra sân nữa , nhưng cũng sẽ không khi nào từ_bỏ bóng_đá " .
Sau khi toả sáng trong màu áo CLB trong nước Bellmare_Hiratsuka và 2 lần giành danh_hiệu Quả bóng vàng châu Á năm 1997 và 1998 , Nakata có được một chiếc vé sang chơi bóng tại châu Âu .
Nakata trong màu áo Fiorentina . ( AP )
Cùng_với Ali_Daei , Nakata được coi là đại_diện châu Á tại cựu lục_địa . Anh thi_đấu rực_rỡ trong màu áo Perugia và được " người khổng_lồ " Roma mua về năm 2000 . Tại sân Olimpico , Nakata trở_thành 1 trong những tiền_vệ công hay nhất Serie A , những ngày_tháng cống_hiến không mệt_mỏi của anh đã đem lại cho Giallorossi chức vô_địch Scudetto năm 2001 .
Sau đó , Nakata đã chơi cho Parma , Bologna và Fiorentina trước khi được cho mượn tại Bolton_Wanderers mùa bóng trước .
Hidetoshi_Nakata nổi_tiếng với sự nhiệt_tình và tâm_huyết trong lối chơi của mình . Anh là một cầu_thủ thông_minh , có khả_năng chuyền bóng và sút xa rất tốt . Với vai_trò nhạc_trưởng đội_tuyển Nhật_Bản trong nhiều năm , anh đã thi_đấu 77 trận cho đội bóng xứ_sở hoa anh_đào , ghi được 11 bàn thắng .
Năm 2004 , Nakata chính_thức trở_thành đại_sứ bóng_đá của Nhật_Bản , cũng trong năm này anh được Pele đưa vào danh_sách 100 cầu_thủ vĩ_đại nhất còn sống .
Năm 2005 , nước Italia trao_tặng danh_hiệu cao_quý nhất cho Nakata : Hiệp_sĩ Ngôi_sao đất_nước Italia như một sự tưởng_thưởng cho việc những gì anh đã đóng_góp cho nước này .
Trong_suốt những ngày_tháng thi_đấu của mình , Nakata luôn tâm_niệm một điều : Mọi việc anh làm đều là để làm_đẹp hơn cho hình_ảnh nước Nhật trong mắt thế_giới .
Và khi anh quyết đinh gọi sự_nghiệp của mình là chỉ " một ngày " , có_thể khẳng_định Samurai tại trời Âu đã thực_sự để lại ấn_tượng sâu_sắc về con_người Nhật_Bản mạnh_mẽ và nhiệt_thành . Sa_Tri

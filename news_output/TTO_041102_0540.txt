﻿ Xây_dựng mới khu khám bệnh và cấp_cứu
Khi hoàn_thành , công_trình có tổng diện_tích sử_dụng hơn 10.000m 2 , gồm năm tầng với nhiều khu khám chữa bệnh . Dự_kiến sau một năm công_trình sẽ hoàn_thành và đưa vào sử_dụng .
Hiện_nay BV Nhân_Dân_Gia_Định cũng đang trong tình_trạng quá_tải , mỗi ngày tiếp_nhận khám chữa bệnh 2.500 - 3.000 bệnh_nhân . Việc mở_rộng khu khám và cấp_cứu là một phần của dự_án cải_tạo , nâng_cấp và mở_rộng BV . Sau đó sẽ nâng_cấp và mở_rộng khu điều_trị nội_trú có 1.000 giường_bệnh .
Tin , ảnh : L.TH . H .

﻿ Tái_ngộ Lưu_Hiểu_Khánh trong Cuộc tình chạy trốn
Thái_Ngạc , tức Thái_Tùng_Pha ( Ngũ_Vệ_Quốc thủ vai ) , một trong những danh_tướng chiến_thắng trong cuộc cách_mạng Tân_Hợi , quyết_tâm phá âm_mưu của gia_đình họ Viên . Tùng_Pha đã cùng cô kỹ_nữ Tiểu_Phụng_Tiên ( Lưu_Hiểu_Khánh ) dựng nên một vở kịch để tránh tai_mắt của Viên_Thế_Khải ( Lâm_Liên_Côn ) .
Còn_Phụng_Tiên vì quá yêu Tùng_Pha nên chấp_nhận hi_sinh tất_cả và đã nhiều lần giúp Tùng_Pha cùng vợ_con anh thoát nạn . Nhưng rồi anh cũng rời xa cô để về cõi vĩnh_hằng trong cơn bệnh nguy_cấp . Cuộc_đời cô kỹ_nữ tội_nghiệp này rồi sẽ đi_về đâu ?
Phim dài 32 tập , phát_sóng lúc 12g20 các ngày thứ_năm , sáu , bảy , chủ_nhật ( mỗi ngày_một tập ) từ 20-3 trên kênh HTV9 .
H . Q .

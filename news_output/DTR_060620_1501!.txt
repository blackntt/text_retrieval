﻿ “ Bói ” xem bạn là ai nào !
Có bao_giờ , bạn tự hỏi mình là ai ? Trắc_nghiệm nhỏ này sẽ giúp bạn đấy !
1 . Nếu được nuôi một trong ba loại động_vật dưới đây , bạn sẽ chọn loại nào ? a . Mèo . b . Chim . c . Chó .
2 . Trong 3 màu sau , bạn thích nhất màu nào ? a . Hồng . b . Trắng . c . Đen .
3 . Hãy ghi tên một người " cùng phe " ( giới_tính ) mà bạn nghĩ ngay đến lúc này .
4 . Hãy ghi tên một người " khác phe " mà bạn nghĩ ngay đến lúc này .
5 . Nếu được chọn một nơi để du_lịch , bạn sẽ chọn nơi nào ? a . Một bãi_biển nhiều sóng . b . Một vùng núi mát_mẻ .
6 . Bạn thích ngắm cảnh bình_minh hơn hay cảnh hoàng_hôn hơn ?
7 . Trong các con_số từ 1 đến 100 , bạn thích số nào nhất ?
8 . Trong 3 loại hoa sau , bạn thích loại hoa nào nhất : a . Hoa_hồng . b . Hoa hướng_dương . c . Hoa giả .
9 . Trong 3 mùa sau , bạn thích mùa nào nhất : a . Mùa_đông . b . Mùa xuân . c . Mùa_hè .
Giải_đáp :
1 . Loài vật_nuôi ưa_thích nói lên mối quan_hệ của bạn với những người xung_quanh :
a . Bạn thích nuôi chó : Luôn quan_tâm đến người khác là điểm mạnh của bạn . Bạn thường nghĩ đến cảm_giác của người khác trước mình nên luôn được coi là một người bạn trung_thành . Tuy_nhiên , cũng đôi_khi vì_thế mà bạn cảm_thấy khá mệt_mỏi đấy .
b . Chim : Bạn thích khuyên_nhủ mọi người , nói khá nhiều . Hầu_hết mọi người thấy bạn rất dễ_thương nhưng một_số người coi việc khuyên_nhủ của bạn là phiền_phức . Hãy tinh_ý nhé .
c . Mèo : Bạn yêu bản_thân mình nhất .
2 . Màu_sắc ưa_thích thể_hiện tính_cách của bạn : a . Hồng : hướng_ngoại . b . Trắng : cổ_điển . c . Đen : ưa phiêu_lưu , chấp_nhận rủi_ro .
3 . Đây là người bạn quân_sư của bạn , rất đáng tin_cậy , luôn sẵn_sàng để bạn tìm chỗ dựa những lúc khó_khăn . Tên này giống như " thiên_thần hộ_mệnh " của bạn vậy .
4 . Đây là tên bạn_thân của bạn , luôn có_mặt để chia_sẻ từ một cái kẹo mút đến chuyện tình_cảm . Hắn chỉ nghe bạn tâm_sự là bạn đã thấy niềm_vui nhân đôi , nỗi_buồn chia nửa rồi . Tên này như " ngôi_sao may_mắn " của bạn vậy .
5 . Địa_điểm bạn muốn tới thăm thể_hiện quan_niệm của bạn về tình_yêu : a . Bãi_tắm : Bạn tin rằng mình sẽ gặp người ấy ở một nơi đông_đúc , nhộn_nhịp nhưng bạn muốn có thời_gian tìm_hiểu kỹ_càng . b . Núi : Bạn mong_muốn sẽ gặp được một người mạnh_mẽ , luôn bình_tĩnh .
6 . Cảnh mà bạn thích thể_hiện tính_cách của bạn trong tình_yêu : a . Bình_minh : Bạn là người nhút_nhát , nhạy_cảm và rất quan_tâm tới nửa kia của mình . b . Hoàng_hôn : Bạn lãng_mạn khủng_khiếp , là người đa_cảm và nồng_nhiệt .
7 . Đây là con_số may_mắn của bạn , đơn_giản vì nó làm_bạn tự_tin hơn nhiều trong mỗi việc bạn làm .
8 . Loài hoa bạn ưa_thích thể_hiện mong_muốn của bạn trong tình_cảm : a . Hoa_hồng đỏ : Một tình_yêu lãng_mạn và nhiều_chuyện bất_ngờ . b . Hoa hướng_dương : Một tình_yêu ổn_định , bình_yên . c . Hoa giả : Một tình_yêu độc_đáo với những điều ngạc_nhiên hàng ngày .
9 . Mùa ưa_thích trong năm : a . Mùa_đông : Bạn là người_mẫu thiên về cảm_xúc , dễ xúc_động . b . Mùa xuân : Bạn là mẫu người lãng_mạn , ham hoạt_động . c . Mùa_hè : Bạn là mẫu người thẳng_thắn , mạnh_mẽ . Theo Hoa_Học_Trò

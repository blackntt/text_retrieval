﻿ Paris sẽ yêu một chàng đồng_tính ?
Mới_đây Paris_Hilton thú_nhận rằng cô rất muốn có một chàng người_yêu là dân đồng_tính . Có_vẻ như sau khi chia_tay với “ cậu em ” Stavros_Niarchos , Paris đã thay_đổi sở_thích .
Cô không còn hứng_thú với những cậu_ấm giàu_sụ nữa mà chuyển_hướng sang các chàng_trai đồng_tính vì trông họ hấp_dẫn và gợi_cảm hơn . " Bạn không thấy là các chàng_trai đồng_tính đều vui_tính và ăn_mặc rất sành_điệu à ? " , cô nàng đỏng_đảnh này đã thổ_lộ như_vậy .
“ Tất_cả những anh_chàng quyến_rũ nhất bây_giờ đều là dân đồng_tính ” , Paris nói thêm .
Có_vẻ đây là lý_do vì_sao Paris thường_xuyên quan_hệ với giới đồng_tính và dạo gần đây người_ta thấy Paris thường cặp_kè với một gã đồng_tính ở London . Biết_đâu đây sẽ là bồ mới của cô nàng giàu_sụ này ? BM Theo Celeb Wonder

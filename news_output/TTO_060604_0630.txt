﻿ Chủ hàng có quyền khiếu_nại
- Khi tham_vấn giá xe nhập_khẩu đã qua sử_dụng , nếu cơ_quan HQ xét thấy trị_giá khai_báo của chủ hàng phù_hợp với giá thực_tế đã thanh_toán cho người bán thì sẽ yêu_cầu chủ hàng nộp thuế và thông_quan hàng_hóa ngay .
Trong quá_trình làm thủ_tục hải_quan , nếu chủ hàng có điểm nào chưa rõ thì có_thể liên_hệ với đường_dây_nóng số 9144835 ( bà Vũ_Thúy_Hòa , trưởng_phòng trị_giá tính thuế ) ; 9141329 ( ông Nguyễn_Anh_Tuấn , phó_phòng trị_giá tính thuế ) và 8213750 ( ông Nguyễn_Quốc_Toản , phó_phòng trị_giá tính thuế ) .
Trong bài " Hải_quan làm_khó doanh_nghiệp " ( Tuổi_Trẻ , 2-6 ) có chi_tiết chưa chính_xác , xin đọc lại như sau : " Nếu_không xác_định chính_xác , Nhà_nước sẽ thất_thu thuế tiêu_thụ đặc_biệt và giá_trị gia_tăng " . Trong trường_hợp cơ_quan HQ tham_vấn thông_tin có sẵn trong hệ_thống dữ_liệu của Tổng_cục HQ về lô hàng khai_báo không đúng với trị_giá giao_dịch , cơ_quan HQ có quyền bác_bỏ trị_giá khai_báo và xác_định giá tính thuế tuần_tự theo các phương_pháp theo qui_định . Chủ hàng có trách_nhiệm nộp thuế theo thông_báo của cơ_quan HQ trước khi thông_quan hàng_hóa . Nếu chủ hàng_không đồng_ý với mức giá do HQ xác_định thì_có quyền khiếu_nại theo Luật khiếu_nại , tố_cáo .
Về bốn chiếc xe cũ đầu_tiên do Công_ty Tradoco nhập , sau khi xác_định lại giá , doanh_nghiệp đã đồng_ý với mức giá do HQ đưa ra .
HQ xác_định lại giá cao hơn từ 2-3 lần , công_ty có đồng_ý không ?
- Doanh_nghiệp nói rằng cơ_quan HQ xác_định lại giá phải trên cơ_sở khoa_học , hợp_lý để doanh_nghiệp chấp_nhận được . Điều đó có_nghĩa việc xác_định giá của HQ là phù_hợp .
Chấp_nhận giá của HQ cũng tức_là doanh_nghiệp thừa_nhận hành_vi gian_lận trong khai_báo , việc xử_lý thế_nào ?
- Theo qui_định , nếu thấy doanh_nghiệp khai_báo thấp mà doanh_nghiệp không chứng_minh được_giá khai_báo đúng thực_tế thì thuộc thẩm_quyền của HQ xác_định lại giá . Còn lại phải căn_cứ vào vụ_việc , tính_chất mà có đề_nghị với Tổng_cục HQ , chứ hiện_nay chưa đặt ra vấn_đề này .
Có thông_tin doanh_nghiệp đã làm đơn đề_nghị Cục HQ thành_phố xem_xét lại giá_mà HQ đã xác_định lại ?
- Chúng_tôi chưa nhận được đề_nghị này .
V . H . Q .

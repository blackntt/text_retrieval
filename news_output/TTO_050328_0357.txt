﻿ Hà_Nội : Ngày hội HSSV với Internet
Ảnh : Nguyệt_Ánh
TT ( Hà_Nội ) - " Diễn_đàn Internet cho HSSV” ( diễn ra từ 25 đến 27-3 ) thu_hút khá nhiều lượt HSSV tới tham_dự .
Tại phòng họp lớn , một màn_hình rộng hướng_dẫn cách truy_cập ở giữa , hai bên là hai dãy Internet truy_cập miễn_phí ( ảnh ) .
Thanh_Thảo ( SV năm 1 ĐH Mở ) cho_biết : “ Đến với diễn_đàn mới thấy việc mình không biết truy_cập thông_tin quả_là thiệt_thòi , mình đã bỏ_qua mất nhiều cơ_hội ” .
Ông Mai_Anh ( giám_đốc Trung_tâm Tin_học , Bộ Khoa học - công_nghệ ) và anh Nguyễn_Mạnh_Dũng ( Trung_ương Đoàn ) cùng cho_biết : “ Ngày hội và diễn_đàn nhằm đẩy_mạnh việc sử_dụng Internet lành_mạnh trong SVHS lẫn các bạn trẻ , đồng_thời đào_tạo một đội_ngũ tuyên_truyền_viên về công_nghệ_thông_tin cho thanh_niên nông_thôn , thiết_thực hưởng_ứng Tháng thanh_niên 2005 ” .
H.MAI - Đ . LỆ - N . ÁNH

﻿ Giá thép giảm 200.000 đồng/tấn
Đặc_biệt là thép tại khu_vực miền Nam sau thời_gian dài đứng ở mức cao_giá cũng đã bắt_đầu giảm bình_quân 200.000 đồng/tấn so với tuần trước , phổ_biến 7,2 triệu đồng / tấn thép cuộn , 7,7 triệu đồng / tấn thép cây .
Hiệp_hội Thép_Việt_Nam cho rằng , giá thép ở miền Nam có khả_năng sẽ còn giảm nhẹ . Tình_trạng suy_giảm tiêu_thụ thép trong thời_gian qua đã được chặn_đứng . Riêng tháng 5 , sản_lượng thép tiêu_thụ trong cả nước đã đạt 150.000 tấn . Dự_kiến trong tháng 6 , lượng thép tiêu_thụ đạt trên 170.000 tấn .
Theo SGGP

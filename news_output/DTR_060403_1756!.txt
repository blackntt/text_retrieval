﻿ Bia_mộ thời kỹ_thuật_số !
Một nhà_phát_minh Hà_Lan vừa phát_triển một loại bia_mộ kỹ_thuật_số với màn_hình LCD để chiếu hình_ảnh , phim về người đã khuất .
Theo tờ De_Morgen , bia_mộ có một bộ cảm_ứng hồng_ngoại để kích_hoạt màn_hình khi có bất cứu ai đứng trước mộ . Màn_hình sẽ sáng lên với những thông_điệp của người quá_cố , đồng_thời xuất_hiện những video và hình_ảnh của người đó .
Nhà_khoa_học Henk_Rozema , 65 tuổi , sống ở thành_phố Zwolle , Hà_Lan đã nảy ra ý_tưởng này khi ông làm đĩa DVD về cuộc_đời ông để chiếu trong bữa tiệc sinh_nhật .
Khi rất nhiều bạn_bè hỏi xin copy chiếc đĩa DVD , ông Rozema nhận thấy ông có_thể làm được một điều kỳ_lạ và cuối_cùng ông quyết_định xây bia_mộ kỹ_thuật_số .
“ Tôi là một kỹ_sư và tôi nghĩ điều này là hoàn_toàn có_thể ” , ông Rozema nõi . “ bởi_vì chúng_ta không sống trong thời_kỳ đồ đá nữa mà là trong thời_kỳ kỹ_thuật_số ” .
N . H_Theo_Ananova

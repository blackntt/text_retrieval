﻿ Ô chữ kỳ 68
1 . Tác_giả các lời hát : “ Ôi quê ta bánh_đa bánh_đúc ” trong bài Về quê .
7 . Nỗi_niềm riêng_tư , sầu kín .
8 . Các địa_danh ở tỉnh này : chùa Yên_Tử , đền Cửa_Ông , di_tích lịch_sử Bạch_Đằng ...
9 . Nữ_sĩ tên thật_là Vương_Kiều_Ân , tác_giả bài_thơ Bức tranh quê .
10 . Phần làm chỗ dựa bên trong cho những phần khác , tạo nên sự vững_chắc của toàn khối .
13 . Nói với giọng_điệu , thái_độ thiếu bình_tĩnh , thiếu ôn_hòa .
15 . Nhân_vật chính trong bộ phim Ngọn nến hoàng_cung .
18 . Tác_giả bài hát Tiếng còi trong sương đêm .
20 . Chim gần với khướu , lông màu nâu vàng , trên mí mắt có vành lông trắng , hót hay .
21 . Nhạc_sĩ sáng_tác bài Mùa xuân từ những giếng dầu .
Hàng dọc
1 . Nhạc_sĩ sáng_tác bài Mơ về nơi xa lắm , Em ơi Hà_Nội phố .
2 . Tỉnh thuộc khu_vực Tây_nguyên , tách ra từ tỉnh Đắc_Lắc .
3 . Giữ chặt lại , đè xuống , không cho tự_do di_động .
4 . Thế mạnh hơn .
5 . Chim_cánh_cụt sống nhiều ở vùng_đất này .
6 . Nhân_vật trong phim Con_nhà nghèo , do diễn_viên Bảo_Châu đóng .
11 . Nhạc_sĩ sáng_tác bài Tóc gió thôi bay .
12 . Quốc_hiệu của nước ta từ thời vua Lý_Thánh_Tông .
14 . NSND đóng vai ông Nam trong phim Dốc tình .
16 . Lúa gieo_cấy ở miền Bắc nước ta , vào đầu mùa lạnh và thu_hoạch vào đầu mùa nóng , mưa nhiều .
17 . Người có quyền_sở_hữu về tài_sản nào_đó .
19 . Có phần đầu nhỏ dần lại như hình mũi kim , dễ đâm thủng vật khác .

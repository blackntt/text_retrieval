﻿ Nam_Định đúc tượng 14 vị Hoàng_đế nhà Trần
Mỗi bức tượng gồm phần thân , bệ đúc liền và phần ngai cùng các họa_tiết phụ có trọng_lượng thành_phẩm là 9 tạ , được đúc bằng đồng nguyên_chất , mẫu do Công_ty Xây_dựng Mỹ_thuật Hà_Nội thiết_kế .
Phần ngai được thống_nhất một kiểu ngai_vàng mang phong_cách độc_đáo nghệ_thuật đời Trần , hai đầu rồng ngậm ngọc ở tay ngai lấy biểu_tượng từ đầu rồng đất_nung được khai_quật tại khu di_tích Đền_Trần - Chùa_Tháp .
Trang_phục là áo Long_cổn có rồng to chầu trước ngực và sau lưng , rồng ở hai ống tay và tà áo , gấu áo trang_trí bằng sóng thủy ba , thể_hiện phong_cách uy_nghi , trang_trọng , tôn_nghiêm của các vị Hoàng_đế . Phần vương_miện được sử_dụng những họa_tiết hoa_văn như rồng , chim , mây , mặt_trời , ngọc ...
14 tượng vua Trần có dung_nhan khác_nhau thể_hiện theo tính_cách , độ tuổi ...
Theo kế_hoạch , 14 pho tượng sẽ được bàn_giao cho Sở Văn hoá-Thông tin tỉnh Nam_Định trong tháng 12 âm_lịch để kịp lễ_hội khai ấn Đền_Trần 14 tháng Giêng .
Thời_Trần kéo_dài 175 năm ( 1225-1400 ) trải qua 14 đời vua , được đánh_giá là đỉnh_cao của văn_minh Đại_Việt , 3 lần đánh_bại ý_chí xâm_lược của đế_quốc Nguyên-Mông thế_kỷ XIII .
VN dự Hội_nghị Bộ_trưởng về công_nghiệp giải_trí văn_hoá
Nhận_lời mời của Bộ_trưởng Kinh_tế và Công_nghiệp Nhật_Bản , đoàn đại_biểu Bộ Văn hóa-Thông tin Việt_Nam do Thứ_trưởng Trần_Chiến_Thắng làm trưởng_đoàn , đã tham_dự Hội_nghị Bộ_trưởng các nước châu Á về công_nghiệp giải_trí văn_hóa lần thứ nhất trong hai ngày 27 và 28-10 tại thủ_đô Tôkyô .
Hội_nghị có sự tham_dự của đại_biểu các nước Nhật_Bản , Trung_Quốc , Hàn_Quốc , Ấn Độ và 10 nước ASEAN , trao_đổi về tình_hình sản_xuất phim của các nước , các hoạt_động văn_hóa trong lĩnh_vực sản_xuất các sản_phẩm nghe_nhìn , vui_chơi giải_trí .
Các đại_biểu nhấn_mạnh vai_trò quan_trọng của ngành này trong đời_sống kinh_tế , xã_hội và tác_động đối_với cộng_đồng . Một_số ý_kiến cũng nêu lên những mặt_trái của Internet và trò_chơi điện_tử đối_với thanh_thiếu_niên và nhi_đồng ; sự cần_thiết phải liên_kết giữa các nước khu_vực trong lĩnh_vực điện_ảnh ; đào_tạo nguồn_lực cho ngành công_nghiệp giải_trí hiện đang phát_triển mạnh trong khu_vực và trên toàn thế_giới .
Đại_diện các nước đã nhất_trí về việc trao_đổi thông_tin , kinh_nghiệm quản_lý và thúc_đẩy các hoạt_động chuyên_môn như tổ_chức hội_thảo , hội_chợ , triển_lãm , liên_hoan điện_ảnh , âm_nhạc , múa_hát , truyền_thông , hợp_tác làm phim , mở_rộng thị_trường , cải_tạo môi_trường đầu_tư và huy_động các thành_phần xã_hội nhằm tạo ra nhiều sản_phẩm cho ngành công_nghiệp giải_trí ở các nước châu Á .
Hội_nghị Bộ_trưởng châu Á về công_nghiệp giải_trí văn_hóa lần thứ 2 sẽ được tổ_chức tại Philipines trong năm tới .
Sóc_Trăng sưu_tập gần 4.000 hiện_vật có giá_trị
Gần 4.000 hiện_vật có giá_trị liên_quan đến nền văn_hóa dân_gian độc_đáo của địa_phương cũng_như lịch_sử hai cuộc kháng_chiến chống thực_dân Pháp và đế_quốc Mỹ vừa được Bảo_tàng tỉnh Sóc_Trăng sưu_tập tại 4 huyện Mỹ_Xuyên , Mỹ_Tú , Thạnh_Trị và Vĩnh_Châu .
Nhằm làm phong_phú thêm kho_tàng văn_hóa địa_phương để phục_vụ khách tham_quan , Bảo_tàng tỉnh cũng đang triển_khai đề_tài nghiên_cứu văn_hóa phi vật_thể mang tên " Hoa_văn truyền_thống tiêu_biểu trong trang_trí chùa Khơ me Sóc_Trăng " .
Triển_lãm chuyên_đề về Lào và tình đoàn_kết Việt-Lào
Ngày 28-10 , tại Bảo_tàng Cách_mạng Việt_Nam , hơn 200 hình_ảnh , hiện_vật phản_ánh về đất_nước , con_người các bộ_tộc Lào , sự phát_triển của nước CHDCND Lào trong 30 năm và mối quan_hệ đặc_biệt giữa Lào và VN đến từ Bảo_tàng Quốc_gia Lào đã được giới_thiệu tại một triển_lãm chuyên_đề có tên gọi " 30 nước CHDCND Lào ( 1975-2005 ) và tình đoàn_kết Việt_Lào " .
Đây là lần đầu_tiên các hiện_vật của nước CHDCND Lào được mang đi nước_ngoài triển_lãm nhân_sự kiện trọng_đại của đất_nước , khẳng_định mối quan_hệ bền_vững , tình_cảm đặc_biệt của các bạn Lào đối_với người anh_em VN .
Theo TTXVN

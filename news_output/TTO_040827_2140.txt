﻿ Rộn_ràng lễ_hội Mỹ_Sơn
Thật hiếm_hoi để có dịp nhìn thấy khu thánh_địa này trong một không_khí rộn_rã lễ_hội như hôm_nay . Ngoài triển_lãm mỹ_thuật đá gốm sứ , các hoạt_động của thanh_niên tình_nguyện hành_trình di_sản tại khu cắm trại bay , các tiết_mục biểu_diễn nghệ_thuật truyền_thống Chăm , chương_trình ấn_tượng nhất_là lễ rước nước tắm tượng tái_hiện một nghi_lễ truyền_thống của dân_tộc Chăm do dân_làng Thu_Bồn xã Duy_Xuyên thực_hiện .
Đoàn rước nước rực_rỡ sắc_màu kiệu hoa cồng_chiêng cờ lọng , đi từ cổng vào Mỹ_Sơn đến khu tháp D , biểu_diễn trích_đoạn lễ_hội Katê , tiến_hành lễ xin mở_cửa tháp , múa dâng lễ và tắm tượng tại đây .
Ngay giữa quần_thể tháp cổ , trên nền di_sản , mỗi hình_ảnh tái_hiện dường_như đều chuyển_tải được_cái hồn của lễ_hội . Có_thể nói từ trước đến nay Mỹ_Sơn mới có được một lễ_hội thu_hút đến như_vậy . Cô Monica_Palmero , một trong những du_khách trẻ đến từ Tây_Ban_Nha cho_biết : “ Tôi cảm_thấy thật_sự thú_vị với những hình_ảnh vừa được chứng_kiến , mặc_dù tôi không hiểu hết được ý_nghĩa của lễ hộị , Mỹ_Sơn là di_sản thật độc_đáo , đất_nước của các bạn có thật nhiều điều để khám_phá ” .
Trao_đổi bên lề lễ_hội , ông Nguyễn_Xuân_Phúc chủ_tịch UBND tỉnh Quảng_Nam cho_biết Quảng_Nam đã làm hết_sức mình để bảo_tồn di_sản văn_hoá thế_giới Mỹ_Sơn , đặc_biệt ở vấn_đề nâng cao ý_thức cộng_đồng .
Biểu_diễn nghệ_thuật chăm
Toàn_cảnh lễ_hội
PV_TTO

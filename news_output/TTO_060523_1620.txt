﻿ Giá cà_phê xuất_khẩu giảm
Hiện_tại , giá cà_phê Robusta loại 2,5% đen vỡ của VN được chào_bán ở mức 1.131-1.141 USD/tấn , FOB tại TP.HCM , giảm tới 40-45 USD/tấn so với tuần trước đó . Giá hầu_hết các loại cà_phê của Indonesia và Ấn Độ cũng trong xu_hướng giảm .
Các nhà_phân_tích giàu kinh_nghiệm trên thế_giới dự_báo , giá cà_phê thế_giới có_thể tăng trong thời_gian tới do nguồn cung khan_hiếm và cùng_với xu_hướng tăng_giá của một_số mặt_hàng chủ_chốt .
Tuy_nhiên , cà_phê vẫn là mặt_hàng có tính không ổn_định , chịu ảnh_hưởng từ nhiều yếu_tố , đặc_biệt là thời_tiết , vì_vậy các chuyên_gia khuyến_cáo các doanh_nghiệp xuất_khẩu theo_dõi chặt_chẽ diễn_biến thị_trường để có những quyết_định kinh_doanh có lợi .
Theo TTXVN

﻿ Các nước được yêu_cầu siết chặt an_toàn hàng_không
ICAO cho_biết tháng tám năm nay đã trở_thành tháng xảy_ra nhiều tai_nạn hàng_không nhất từ trước tới nay với năm vụ nghiêm_trọng ở bốn nước , làm 319 hành_khách thiệt_mạng .
Chủ_tịch hội_đồng ICAO Assad_Kotaite cho_biết sắp tới ICAO sẽ xem_xét thông_qua các tiêu_chuẩn về chế_độ quản_lý an_toàn hàng_không nhằm bảo_đảm sự phát_triển bền_vững của vận_tải hàng_không trong các năm tới , trong đó nhấn_mạnh việc tăng_cường hợp_tác giữa các nước thành_viên ICAO nhằm giảm_thiểu các rủi_ro có_thể xảy_ra .
Tăng_cường các chế_độ giám_sát an_toàn và sự minh_bạch trong quá_trình chia_sẻ thông_tin là những biện_pháp khẩn_cấp cần thực_hiện để đạt được mục_tiêu an_toàn hàng_không cao .
Các hãng hàng_không và các cơ_quan quản_lý hàng_không phải bảo_đảm hệ_thống quản_lý an_toàn hàng_không được thông_tin nhanh_nhất các thông_số an_toàn cần_thiết để kịp_thời hành_động trước khi xảy_ra tai_nạn .
ICAO yêu_cầu các nước thiết_lập các chế_độ giám_sát an_toàn hàng_không quốc_gia trong mọi thành_phần của kết_cấu_hạ_tầng hàng_không như sân_bay , các hãng hàng_không quốc_gia , hệ_thống không_lưu , luật hàng_không và cơ_quan quản_lý hàng_không .
Ngày 29-8 , Bỉ đã cho công_bố danh_sách chín hãng hàng_không bị cấm hoạt_động ở Bỉ vì lý_do không đáp_ứng đủ tiêu_chuẩn về an_toàn bay .
Các hãng bị cấm hầu_hết của châu Phi , gồm : Africa_Lines ( Cộng_hòa Trung_Phi ) , Air_Memphis ( Ai_Cập ) , Central_Air_Express ( CHDC Congo ) , ICTTPW ( Libya ) , International_Air_Tours_Limited ( Nigeria ) , Johnsons_Air_Limited ( Ghana ) , Silverback_Cargo_Freighters ( Rwanda ) , South_Airlines ( Ukraine ) và Air_Van_Airlines ( Armenia ) .
TÚ_ANH ( Theo AFP , Reuters )

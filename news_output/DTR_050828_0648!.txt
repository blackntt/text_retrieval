﻿ Chị thủ_khoa , em cũng thủ_khoa !
Đó là Nguyễn_Thị_Minh_Phương - thủ_khoa ĐH Xây_dựng Hà_Nội và Đỗ_Tuấn_Anh - thủ_khoa trường ĐH Thương_mại Hà_Nội . Cả hai là chị_em họ của nhau ( mẹ Phương là chị_gái của mẹ Tuấn_Anh ) .
Không gì có_thể tả hết niềm_vui của bố_mẹ Minh_Phương và Tuấn_Anh trong đêm hội_ngộ kỳ_thú tại sảnh_đường Văn_Miếu - Quốc_Tử_Giám trong lễ tuyên_dương thủ_khoa xuất_sắc các trường đại_học ở Hà_Nội năm 2005 .
Chị giỏi …
Phương sinh ngày 28/3/1982 , học chuyên_ngành kinh_tế xây_dựng , trường ĐH Xây_dựng Hà_Nội . Bố là đại_tá - tiến_sĩ làm_việc trong quân_đội . Cô có một anh_trai tốt_nghiệp trường ĐH Thương_mại , đang theo học văn_bằng hai ngành xây_dựng .
Là con_gái “ lạc ” vào “ trường của con_trai ” nhưng Minh_Phương lại là người phá kỷ_lục về điểm_số . Điểm trung_bình toàn khóa học của Phương là 9,19 . Bốn lần liên_tiếp nhận học_bổng WUS của Tổ_chức Hỗ_trợ Đại_học Việt_Nam - Bộ GD - ĐT và đạt hai giải B nghiên_cứu khoa_học của trường .
Cô_gái bé_nhỏ còn là một lớp trưởng oai_phong , chủ_nhiệm câu_lạc_bộ chuyên_ngành kinh_tế xây_dựng trong môi_trường toàn con_trai . Cô còn là cán_bộ đoàn tham_gia tích_cực phong_trào thanh_niên và được Trung_ương Đoàn tặng bằng_khen . Phương là đại_diện sinh_viên thành_phố Hà_Nội dự Đại_hội Hội_Liên hiệp Thanh_niên Việt_Nam - thành_phố Hà_Nội lần thứ IV .
Cô_gái có khuôn_mặt xinh_xắn và cách nói_chuyện rất duyên này người mê đọc sách , thích nấu_ăn , cắm hoa và tham_gia các hoạt_động văn_nghệ . Cô còn có “ công ” trong việc đặt tên tờ báo cho trường là “ Hoa của đá ” . Thi TOFFEL nội_địa được 550 điểm , cô sinh_viên mê chơi piano và chơi rất giỏi vẫn tiếp_tục thi TOFFEL quốc_tế đúng vào ngày các bạn thủ_khoa chia_tay nhau ( 20/8/2005 ) .
Trò_chuyện , Phương toàn nói lời tự_hào về cậu em họ Đỗ_Tuấn_Anh cũng là thủ_khoa . “ Mấy ngày theo đoàn đi chơi , hai chị_em có khối chuyện vui ” , và cô tiết_lộ : “ Tuấn_Anh là một chàng_trai rất giàu tình_cảm . Hai chị_em chơi thân với nhau từ bé , thường giúp_đỡ nhau trong học_tập ” .
Cô cũng tự_hào về trường cũ , trường THPT Kim_Liên , Hà_Nội , góp_mặt tới 8 vị thủ_khoa trên tổng_số 96 thủ_khoa của các trường đại_học . Bạn_bè lâu ngày có dịp gặp_gỡ , hàn_huyên , vi_vu với nhau tới mấy ngày .
Hiện_Phương đang làm_việc tại Công_ty Cổ_phần Đầu_tư Phát_triển nhà và Xây_dựng Tây_Hồ . Bố_mẹ Phương luôn tự_hào , khi cô_gái nhỏ_bé của mình lập nên một kỳ_tích đáng nể . “ Phương hầu_như tự_lập trong cuộc_sống . Vừa đi học , hoạt_động đoàn , Phương vẫn dành thời_gian dạy gia_sư , theo_đuổi ước_mơ nối_nghiệp mẹ ” - mẹ Phương tự_hào .
Đỗ_Tuấn_Anh , thủ_khoa trường ĐH Thương_mại Hà_Nội
Nguyễn_Thị_Minh_Phương , thủ_khoa trường ĐH Xây_dựng Hà_Nội
… em cũng “ siêu ” !
Tuấn_Anh sinh ngày 26/11/1983 , học khoa quản_trị doanh_nghiệp , trường ĐH Thương_mại Hà_Nội . Ngày 19/9 tới , cậu lên_đường sang Pháp học cao_học , tại ĐH Masseille .
Kiệm_lời hơn bà chị họ nhưng , khi quen rồi , Tuấn_Anh lại hết_sức cởi_mở . Giải_thưởng Sao tháng Giêng của Trung_ương Hội_Sinh viên Việt_Nam , học_bổng FUYO ( Nhật_Bản ) , học_bổng Dragon_Capital , bằng_khen của Thành_đoàn Hà_Nội về công_tác đoàn và phong_trào thanh_niên . Điểm trung_bình toàn khóa học là 8,89 … là những dòng trích về thành_tích của “ cậu em tình_cảm ” này .
Cũng “ tham ” chức như bà chị , ba năm liền làm lớp trưởng ( năm đầu_tiên làm lớp phó học_tập ) , Tuấn_Anh vẫn không bỏ một lô_lốc sở_thích , từ tin_học , điện_tử , đến đọc sách , thể_thao , âm_nhạc , đặc_biệt là nhạc giao_hưởng .
“ Mỗi khi gặp khó_khăn , mình luôn phải tìm cách vượt qua . Mình có một cậu em_trai học lớp 12 chuyên lý - Amstecdam nên cả hai anh_em cùng đua nhau học thôi ” . Nói về dự_định , Tuấn bộc_bạch : “ Mình muốn làm giảng_viên đại_học ” .
Về chuyện tình_yêu , Tuấn_Anh giấu tiệt , chỉ cười_trừ . Nhưng em_trai của cậu tiết_lộ : “ Bạn_gái anh_ấy học cùng trường Thương_mại đấy . Em chỉ biết tên chị ấy thôi … ”
Theo Hồng_Thái_Net Nam

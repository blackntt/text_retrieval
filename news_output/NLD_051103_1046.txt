﻿ Lời khuyên cho da nhờn
Ảnh minh_họa Đừng nghĩ rằng da nhờn thì_phải rửa mặt càng nhiều càng tốt , vì việc này không tốt cho da , thậm_chí làm da nhờn hơn . Chỉ cần rửa 2 lần/ngày là đủ .
Dùng loại sữa rửa mặt dành riêng cho da nhờn để tẩy bỏ lớp tế_bào chết và tuyến bã nhờn nằm sâu dưới lỗ_chân_lông .
Đừng bỏ kem chống nắng . Nhiều phụ_nữ da nhờn thường bỏ không dùng kem chống nắng . Tuy_nhiên , kem chống nắng vẫn rất cần cho da bạn khi ra ngoài nắng .
Tránh dùng kem dưỡng da và phấn có chất giữ ẩm . Kem dưỡng da cho những làn da tuổi 30 trở_lên thường chứa dầu và chất giữ ẩm . Tuy_nhiên , nếu da bạn nhờn thì điều này không cần_thiết . Hãy chọn loại kem không chứa dầu .
Dùng loại phấn bột trong quét nhẹ lên_mặt sau khi trang_điểm để thấm hút chất dầu còn dư trên da , giúp da bạn tự_nhiên sau mỗi lần trang_điểm , không bị hiện_tượng bóng_loáng .
Đắp mặt_nạ đất_sét 1 lần/tuần giúp giảm tuyến bã nhờn trên da . Sử_dụng phấn mắt , phấn má thay_vì dùng kem hoặc bút_chì khi trang_điểm .
Theo Thanh niên

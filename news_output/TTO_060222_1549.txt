﻿ Hà_Nội : Giải_phóng mặt_bằng cầu Vĩnh_Tuy chậm tiến_độ
Theo UBND quận Long_Biên , tiến_độ GPMB chậm là do còn một_số vướng_mắc liên_quan đến chính_sách bồi_thường , hỗ_trợ , tái_định_cư , nhất_là đối_với những hộ sử_dụng đất giao trái thẩm_quyền trước và sau ngày 15-10-1993 . Chủ_tịch UBND quận Vũ_Đức_Bảo cho_biết quận cũng đang kiến_nghị lên UBND_TP để tìm phương_án giải_quyết . Theo đó , có_thể sẽ giao đất tái_định_cư cho những hộ nói trên .
Ban quản_lý dự_án hạ_tầng Tả_Ngạn đánh_giá , nếu các kiến_nghị của quận được thành_phố chấp_thuận , công_tác GPMB cầu Vĩnh_Tuy sẽ hoàn_thành trước 30-4-2006 . Theo kế_hoạch , công_trình trọng_điểm cầu Vĩnh_Tuy sẽ phải hoàn_thành và thông tuyến kỹ_thuật vào cuối tháng 12-2006 .
TUẤN_CƯỜNG

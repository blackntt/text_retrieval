﻿ Thông_điệp yêu_thương : Cám_ơn đời vì em đã có anh
Em buồn nhiều khi nghĩ đến anh . Không biết sau_này chuyện sẽ như_thế_nào nhưng hiện_tại bây_giờ đây , em không còn niềm_vui nào khác là được nhìn thấy anh mỗi ngày , được nói_chuyện cùng anh và được anh yêu_thương , chăm_sóc .
Em phải làm_sao đây hả anh ? Anh hãy giúp em giống loài hoa xương_rồng anh nhé ! Em sẽ cố_gắng làm tốt công_việc tại nơi này và cũng sẽ cố vui_vẻ để anh cũng vui như lời anh nói .
Bé nhè
Gửi tới 091 . . 1705 ,
Em rất buồn và rất giận anh , tại_sao anh không đến gặp em để giải_thích dù em đã hứa sẽ tha_thứ cho anh mà . Em biết anh cũng đang rất buồn về công_việc và cả về bản_thân mình . Hãy cho em xin_lỗi nếu đã nói_gì làm anh tự_ái nhưng tất_cả chỉ vì em yêu anh , em luôn chờ_mong ngày mau_chóng tới ngày cưới của chúng_mình . Vậy_mà … Em biết anh chẳng bao_giờ đọc được những dòng này nhưng em sẽ mãi yêu anh và chờ_đợi ngày anh đến đón em về nhà anh như lời mình đã hẹn .
Gửi từ 091 . . 7313
Bé ơi , anh nhớ mãi nụ_hôn đầu_tiên mà em dành cho anh cách đây hai năm . Em đã yêu anh nhưng rồi tại_sao nhỉ ? Tới khi nào em thấy mình đau_khổ thì hãy tìm đến anh , anh luôn đón chờ em , chúc em hạnh_phúc .
011 gửi tới 010 ... 0245 .
Honey của H ,
C ơi , chia_tay 2 năm nhưng không_thể_nào xóa được nỗi nhớ trong H . Gặp lại nhau , tình_cảm cũ dâng trào , H không_thể chạy lại bên C để được C lo_lắng , chăm_sóc . Có một khoảng_cách quá xa giữa hai ta , C đã có người khác , đã thay_đổi , riêng H thì chưa_thể . Ngồi bên nhau , hai tâm_hồn không thuộc về nhau , xót_xa quá ...
H sẽ cố_gắng vượt qua nỗi đau này ...
Anh yêu , chuyện tình bao ngày_tháng chúng_ta cố_gắng vun_đắp bây_giờ đã kết_thúc rồi . Em vẫn còn yêu anh nhiều lắm nhưng em không_thể chấp_nhận cuộc_sống hiện_tại của anh . Em cảm_thấy rất đau_lòng khi trông thấy anh ốm_yếu gầy_nhom , em đau_lòng lắm khi thấy anh mặc những bộ quần_áo cũ_mèm và càng đau_lòng hơn khi thấy mẹ và em_gái của anh xài hoang_phí tiền mà anh không dám sử_dụng . Nhưng một điều em tiếc hơn cả là em chẳng_thể_nào làm điều gì hơn cho anh .
Lonelyprincess _ 27
Cá_mập ơi [ ^ _ ^ ] , đi học vui lắm phải không em ? Chỉ còn một 1 năm nữa thôi đối_với cả 2 đứa mình mà_sao anh thấy thời_gian trôi qua chậm quá . Có một " thằng nhóc " những lúc buồn , những lúc khó_khăn , những lúc bị " mê_hoặc " đều nghĩ về em , luôn tự hứa với con tim mình phải học_tập và làm_việc thật tốt vì Cá_mập . Mình cùng cố_gắng em nhé . Thằng Nhóc yêu Cá_mập nhiều nhiều .
Hãy mơ về anh để mỗi khi màn đêm buông xuống , công_chúa của anh sẽ chìm vào những giấc_mơ ngọt_ngào , hạnh_phúc ! ! ! Anh nhớ em nhiều lắm .
NGUYEN_PHU_VINH
Lovely … 1201 yêu_dấu !
Giờ_đây giữa thành_phố xa_lạ , anh mới thấy giây_phút bên em mới hạnh_phúc như_thế_nào . Thế_mà đã 2 năm từ ngày anh lặng_lẽ ra_đi , chưa bao_giờ anh nghĩ đến người con_gái nào khác ngoài em , anh biết em trách anh nhiều lắm , nhưng anh không làm khác được . Dù ở nơi đâu hay làm_gì anh vẫn chỉ canh_cánh trong lòng và cầu_chúc cho em đến được bến_bờ hạnh_phúc . Và anh vẫn nói rằng anh yêu em nhiều lắm …
Green ... ... 2000
Bạn có_thể gửi về Tuổi_Trẻ_Online nội_dung thông_điệp bạn muốn gửi đến cho " một_nửa " của mình , nội_dung không quá 50 chữ . Ngoài_ra , bạn có_thể đính kèm một tấm hình bạn muốn chuyển . Bài viết tham_gia xin gửi về email online@tuoitre.com.vn , chủ_đề ghi rõ là tham_gia chuyên_mục Thông_điệp yêu_thương .
TTO

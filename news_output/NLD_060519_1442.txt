﻿ Quán café nâu ở Amsterdam
Phía bên ngoài quán cafe nâu Amsterdam - Hà_Lan không nổi_tiếng về café , chắc_chắn thế_nhưng người_ta lại quan_tâm rất nhiều đến hai loại cửa_hàng café : café trắng và café nâu .
Gọi_là café trắng vì các cửa_hàng này đều hiện_đại , có_lẽ vì_thế nên tường nhà còn sáng_sủa và người_ta đặt cái tên như_vậy chứ cũng không hẳn những quán café này có màu trắng_toát .
Nếu chỉ nhìn bên ngoài thật khó biết được đâu là café nâu , đâu là café trắng . Chính vì_thế khi thấy một quán café sơn màu trắng và trông có_vẻ mới tôi nghĩ : sẽ thử ăn trưa ở đây . Nhưng khi bước chân vào , bên trong lại hoàn_toàn ngược hẳn , tôi đã chọn đúng quán café nâu .
Khung_cảnh ấm_cúng và thân_thiện với những người phục_vụ trong quán rất mến khách và như mọi người_dân Hà_Lan khác , nói tiếng Anh rất thành_thạo . Cả một không_gian toàn màu nâu của những tấm gỗ phai màu theo thời_gian ánh lên rất dễ_chịu .
Chủ_khách cùng trò_chuyện vui_vẻ
Không_gian màu nâu ấm_cúng
Một_vài chiếc bàn và ghế gỗ ở tầng một , đi qua một cầu_thang gỗ nhỏ là tới tầng hai cũng toàn bàn_ghế gỗ màu nâu . Trên quầy bán hàng người_ta để một hàng ghế dành cho những_ai chỉ ghé qua làm vài cốc bia . Ở những cái giá cao là các thùng bia cũng bằng gỗ màu nâu , vừa như để trang_trí , vừa để đựng bia .
Cả khách và chủ đều có_thể ngồi cạnh quầy và nói_chuyện vui_vẻ . Bước chân vào những quán café nâu cổ_kính của Amsterdam đem đến cho người tham_quan những giây_phút thư_thái thật_sự . Bỏ_qua tất_cả lịch_sử của quán café nâu có từ những năm của thế_kỉ 17 , bỏ_qua tất_cả kiến_trúc và không_khí ấm_cúng , bỏ_qua cả vị_trí thơ_mộng của các quán bên_cạnh những dòng kênh đẹp nhất thì những quán café này đều có những món ăn rất ngon và giá_cả phải_chăng . Đến_Amsterdam đừng lỡ dịp vào những quán café nâu nhé .
Theo Tuổi_Trẻ

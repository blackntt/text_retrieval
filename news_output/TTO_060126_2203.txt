﻿ Vườn đào của bố
Bố bảo , Tết hạnh_phúc nhất của bố là cái Tết được đánh gốc đào xấu nhất của vườn để chưng trong nhà , vì nghĩa_là năm ấy , đào đã được bán hết . Mẹ và con có thêm tấm áo mới , bố có thêm bình rượu thơm , và nồi bánh_chưng đêm 30 sẽ dầy lên hạnh_phúc .
365 ngày , bố quanh_quẩn với vườn đào . Bố tỉa lá , phun sâu , cắt chiết , uốn cành . Những cây đào đủ mọi tư_thế , đủ mọi tầng , mọi cành , mọi màu … Mỗi cây đào bố đều gửi một ý_nghĩa , một lời nguyện_ước may_mắn và thành_công để rồi những cành đào ấy khoe sắc trong bao nhà những ngày Tết đến xuân về .
Năm nay , bố cứ trông_chừng trời_đất , thỉnh_thoảng lại thở_dài , thỉnh_thoảng lại đưa_mắt nhìn vườn đào âu_lo . Thời_tiết thất_thường làm những tính_toán và kinh_nghiệm lâu năm của bố sai_lệch … Vườn đào đôi cành đã khoe hoa , bật nụ , những vẩy rồng đã sần lên đầy mong_ước sẵn_sàng cho những ngày Tết . Nhưng hầu_hết , các cành đào đều còi_cọc , mầm lá chưa xanh , những cành đào gầy và khẳng_khiu như hàng_rào .
Nhìn mắt bố , biết bố đang lo những nụ đào câm khi từng tờ lịch đang vơi cạn dần .
Bố ơi , đừng lo nữa bố ! Bố biết không , đã bao lần con muốn nói với bố rằng vườn đào chỉ làm cho ngày Tết đẹp hơn , chứ nó không làm_nên ngày Tết !
NGUYỄN_TRUNG_THU ( Hà_Nội )

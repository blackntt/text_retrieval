﻿ Serbia công_nhận nhà_nước Montenegro
Serbia hôm_qua tuyên_bố họ thừa_nhận Montenegro là một quốc_gia độc_lập và thiết_lập quan_hệ ngoại_giao với nước này .
" Đã có đủ điều_kiện để chính_phủ Serbia thừa_nhận nước Cộng_hòa Montenegro và thiết_lập quan_hệ ngoại_giao " , một tuyên_bố của chính_phủ Serbia viết .
Montenegro ra tuyên_ngôn độc_lập hôm 3/6 , đánh_dấu sự ra_đời của nhà_nước có chủ_quyền còn non_trẻ nhất thế_giới . Nó cũng đặt dấu_chấm hết cho mối liên_kết còn lại của các thành_viên Liên_bang Nam_Tư cũ .
Rất nhiều quốc_gia châu Âu cùng_với Mỹ và Trung_Quốc đã thừa_nhận độc_lập của Montenegro . Phía châu Âu bao_gồm những nước ở Balkan như Albania , Bosnia-Hercegovina , Bulgaria , Croatia , Hy_Lạp và Romania .
Iceland và người_dân nước này được người_dân Montenegro hết_sức ngưỡng_mộ sau khi họ trở_thành những người đầu_tiên công_nhận độc_lập của Montenegro .
Báo_chí ở đây còn đưa tin người_dân Montenegro mời dân Iceland sang nghỉ_ngơi ở nhà họ miễn_phí . Nhiều tờ báo còn đăng trên trang nhất bản_đồ của Iceland và mô_tả lịch_sử cũng_như kinh_tế của nước này .
Theo hiến_chương của Serbia-Mongtenegro cũ , Serbia được thừa_hưởng ghế thành_viên của Liên_Hợp_Quốc và các cơ_quan quốc_tế khác . Montenegro phải tự xin gia_nhập các tổ_chức này .
Liên_bang lỏng_lẻo giữa Serbia và Montenegro ra_đời năm 2003 dưới sức_ép của EU . Liên_bang chỉ có một_số bộ chung như quốc_phòng , ngoại_giao và nhân_quyền , còn lại Serbia và Montenegro vẫn có hệ_thống tiền_tệ , hải_quan và thuế riêng_biệt .
Theo Hải_Ninh_Vnexpress / BBC

﻿ Nga tuyên_án nghi can vụ Beslan
Phiên_tòa xét_xử Nur-Pashi Kulayev , sinh năm 1980 , nghi can duy_nhất còn lại trong vụ bắt_cóc , đang đến phần tuyên_án , một quy_trình được cho là phải mất nhiều ngày .
Kulayev đang đối_mặt với các tội_danh giết người và khủng_bố trong vụ tấn_công tháng 9-2004 khiến hơn 300 người thiệt_mạng .
“ Phiên_tòa đã xác_minh rõ rằng Kulayev đã tham_gia vào_cuộc tấn_công có vũ_trang và một con_tin bị bắt_giữ đã xác_nhận anh_ta đã giết người không có khả_năng tự_vệ ” , hãng tin AFP dẫn lời Tamerlan_Aguzarov , quan_tòa tại tòa_án tối_cao ở Bắc_Ossetia cho_biết .
“ Anh_ta đã thực_hiện hành_vi khủng_bố nhằm mục_đích gây ảnh_hưởng đến các quyết_định của nhà_chức_trách ” , Tamerlan_Aguzarov nói .
Kulayev thừa_nhận có tham_gia vụ tấn_công nhưng cho_biết mình không giết người nào cả .
Các công_tố_viên đã đề_nghị mức án tử_hình đối_với Kulayev .
TƯỜNG_VY ( Theo BBC , Reuters )

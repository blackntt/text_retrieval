﻿ Thủ_tướng Lý_Hiển_Long tái đắc_cử
Tại một điểm bỏ_phiếu ở Singapore . Đảng hành_động nhân_dân cầm_quyền ( PAP ) của Thủ_tướng Singapore_Lý_Hiển_Long đã giành thắng_lợi trong cuộc tổng_tuyển_cử diễn ra ngày hôm_qua 6-5 .
Theo kết_quả kiểm phiếu , Đảng hành_động nhân_dân của Thủ_tướng Lý_Hiển_Long đã giành được 66,6 % số phiếu ủng_hộ .
Mặc_dù kết_quả này có giảm 9% so với cuộc bầu_cử năm 2001 , nhưng đảng Đảng hành_động nhân_dân vẫn chiếm 82 trong tổng_số 84 ghế trong quốc_hội . Kết_quả bầu_cử đã khẳng_định uy_tín của thủ_tướng Lý_Hiển_Long , bởi ông Lý_Hiển_Long được bầu làm thủ_tướng 2 năm trước mà không qua bầu_cử . Phát_biểu sau khi kết_quả bầu_cử được công_bố , Thủ_tướng Lý_Hiển_Long đã cảm_ơn người_dân Singapore tiếp_tục ủng_hộ ông , và cam_kết sẽ nỗ_lực để giữ_gìn hình_ảnh tốt_đẹp của đất_nước Singapore .
Theo VTV

﻿ 12 nghề lạ_lùng nhất quả_đất
Cuộc_sống sản_sinh ra không biết_bao_nhiêu ngành_nghề cũ , mới . Thế_nhưng , thời hiện_đại lại có những ngành_nghề mới và lạ đến độ chỉ mới nghe thôi đã thấy ... kỳ quá rồi !
1 . Nghề buôn ... ước_mơ
Một nhân_viên bưu_điện mơ chỉ sau một đêm , mình sẽ trở_thành nghệ sỹ lớn . Một doanh_nhân bình_thường mơ được điều_hành tổng_công_ty cỡ bự . Bác sỹ tâm_lý mơ được khám và chữa bệnh cho 20 cô_gái đẹp trên khắp hành_tinh ... Nhưng , làm thế_nào để biến những ước_mơ đó trở_thành hiện_thực ?
Có một công_ty ở Chicago ( Mỹ ) đảm_nhiệm dịch_vụ biến những ước_mơ và suy_nghĩ viển_vông trong cuộc_sống của các khách_hàng trở_thành ... hiện_thực ! Chỉ cần bạn đến công_ty và kể cho các chuyên_gia về những ước_mơ đó là đủ . Thế_nhưng , bạn hãy thận_trọng trước khi quyết_định tham_gia dịch_vụ này . Bởi_vì giá_cả là do toàn_quyền của các chuyên_gia công_ty đưa ra và thấp nhấp mà cũng không dưới ... 150.000 USD !
2 . Nghề chọn bóng
Đó là nghề chỉ có trong các câu_lạc_bộ đánh golf sang_trọng lớn . Người hành_nghề này có nghĩa_vụ sao cho bóng của sân mình và bóng của sân khác không lẫn với nhau .
3 . Nghề săn kiến
Người săn kiến phải chọn được những con kiến tốt nhất trong tổ kiến đông hàng vạn con để khi chuyển những chú kiến này đến trang_trại chuyên nuôi kiến , chúng sẽ tiếp_tục nhân_giống và phát_triển tốt .
4 . Lấy óc vật bị_thịt
Vị_trí làm_việc của những người_làm nghề này là ở các lò_sát_sinh . Họ có nhiệm_vụ chờ khi thợ lò_mổ bửa đầu con vật bị giết_thịt ra thì lấy óc tươi của nó để đưa ngay đến nhà_bếp các khách_sạn chế_biến những món ăn cao_cấp .
5 . Nghề khử mùi hôi_thối
Nơi làm_việc là ở một_số công_ty , xí_nghiệp sản_xuất những mặt_hàng có mùi . Họ đeo thiết_bị chuyên_dụng và có trách_nhiệm phát_hiện , khử ngay các mùi hôi_thối .
6 . Quan_sát , phát_hiện hoả_hoạn
Những người này ngồi trực trên các chòi cao trong các công_viên ở Mỹ . Họ có nhiệm_vụ phát_hiện và thông_báo nhanh_chóng , kịp_thời các đám cháy .
7 . Bảo_trợ ong_chúa
Người bảo_trợ ong_chúa phải có những hiểu_biết chuyên_môn nhất_định về đời_sống của loài ong , đặc_biệt là của ong_chúa . Họ có trách_nhiệm bảo_đảm an_toàn , theo_dõi toàn_bộ quá_trình sinh_sản , phát_triển và các hoạt_động khác của tổ ong .
8 . Thợ đập trứng
Làm nhiệm_vụ tách lòng_trắng và lòng_đỏ trứng gà . Họ sẽ chọn những quả trứng tốt , còn nguyên_vẹn đưa vào một máy chuyên_dụng và lấy những sản_phẩm là lòng_trắng và lòng_đỏ đã được tách riêng_biệt ra .
9 . Chuyên_gia nhận_biết giống gà con
Người này phải có chuyên_môn và kinh_nghiệm để nhận_biết giống của những chú gà con mới có ... 1 ngày tuổi . Việc xác_định phải tuyệt_đối chính_xác bởi_vì căn_cứ vào đó , người_ta sẽ áp_dụng những biện_pháp thích_hợp để nuôi_dưỡng và chăm_sóc đàn gà .
10 . Nghề làm phẳng những nếp nhăn
Trong các hiệu giày cao_cấp có chuyên_gia làm phẳng các nếp nhăn trên những đôi giày đắt tiền .
11 . Ngửi trứng
Trong các nhà_máy bánh_kẹo đều có những người ngửi trứng . Nhiệm_vụ của họ là dùng mũi của mình kiểm_tra tất_cả các quả trứng đầu_vào và không được để một quả trứng ung , thối nào rơi vào dây_chuyền sản_xuất bánh , kẹo .
12 . Thợ thông ống_khói
Nghề này đã bị mai_một ở nhiều nơi . Thế_nhưng tại nam California thì thợ thông ống_khói lại có việc suốt ngày . Đây là một nghề nguy_hiểm và độc_hại . Các thợ thông ống_khói phải trải qua một khoá huấn_luyện chuyên_môn ít_nhất là 2 năm . Tiền_công : 20 USD / một lần thông ống_khói . Thành_Nam_Theo_Pravda

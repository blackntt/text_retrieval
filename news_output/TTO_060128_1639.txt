﻿ Trung_Quốc đón năm mới với Lễ_hội lồng đèn
Một chiếc lồng đèn_hình rồng trên hồ nước tại công_viên tỉnh Sơn_Đông hôm 24-1-2006
TTO - Sau_Lễ hội mùa xuân , Lễ_hội lồng đèn là sự_kiện vui nhất và sôi_nổi nhất trong năm nay của người Trung_Quốc . Năm nay , Lễ_hội lồng đèn kéo_dài đến tháng 2 .
& gt ; & gt ; Chuẩn_bị đón năm mới tại châu Á & gt ; & gt ; Châu Á rộn_ràng đón Tết_Bính_Tuất & gt ; & gt ; Làn_sóng đi_lại tại Trung_Quốc trong năm mới & gt ; & gt ; Tiếng pháo lại trở_về cùng không_khí Tết của người_dân Bắc_Kinh & gt ; & gt ; Ảnh ngộ_nghĩnh năm con chó & gt ; & gt ; Sắc_màu năm Bính_Tuất & gt ; & gt ; Tết cổ_truyền của người châu Á
Người_Trung_Quốc xưa tin rằng lồng đèn xua_đuổi ma_quỷ và mang lại bình_yên cũng_như hạnh_phúc . Họ thắp rất nhiều lồng đèn để chào_đón năm mới . Dần_dần nghi_lễ này phát_triển thành một hội_chợ lớn , là nơi trưng_bày các kiểu đèn_lồng trang_trí và ngày_càng được chế_tạo công_phu .
Lễ_hội lồng đèn năm nay được tổ_chức tại công_viên Chaoyang ở phía đông Bắc_Kinh . Lễ_hội sẽ thắp sáng bầu_trời đêm Bắc_Kinh từ 5 giờ 30 cho_đến 9 giờ 30 tối mỗi ngày , bắt_đầu_từ đêm hội mùa xuân ( 28-1 ) đến ngày 14-2 , ngày Lễ tình_nhân .
Hơn 1.000 chiếc lồng đèn sẽ được thắp sáng trong Lễ_hội , tạo thành một " bữa tiệc nghệ_thuật " hoành_tráng cho người_dân địa_phương và du_khách .
Ngoài các lồng đèn_hình thuyền_rồng , lồng đèn mô_phỏng tàu_vũ_trụ Thần_Châu VI vừa hoàn_thành chuyến bay vào vũ_trụ hồi tháng 10-2005 ... còn có nhiều chiếc lồng đèn chắc_chắn sẽ thu_hút các khách tham_quan nhí : những chiếc đèn_lồng hình động_vật và hình các nhân_vật hoạt_hình , những nhân_vật của Disney ...
Các cổng vào công_viên được trang_trí bằng những chiếc lồng đèn_đỏ ấm_áp Và , quan_trọng nhất và không_thể thiếu , đó là chiếc đèn_lồng hình chú cẩu - con thú biểu_trưng của năm Bính_Tuất .
Với nhiều người Trung_Quốc , còn gì thích bằng khi được ngắm những chiếc đèn_lồng đỏ đang tỏa ánh_sáng ấm_áp giữa tiết trời lành_lạnh của mùa xuân ...
T.VY ( Theo China_Daily , Chinabroadcast )

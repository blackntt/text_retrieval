﻿ Trung_Quốc : Con tự_tử , bố_mẹ kiện hãng game
Phụ_huynh của một thiếu_niên 13 tuổi đã đòi hãng game bồi_thường 100.000 NDT do cậu_bé này tự_tử sau khi chơi game suốt 36 tiếng liên_tục .
Năm 2004 , Trương_Hạo_Chí đã nhảy từ cửa_sổ tầng 24 xuống đất sau khi chơi Warcraft suốt một ngày rưỡi trời ở quán cafe Internet bên_cạnh .
Trong đơn_kiện của mình , bố_mẹ Chí cáo_buộc China_Cyber_Port , hãng phát_hành Warcraft tại Trung_Quốc đã không " cảnh_báo trước " người chơi về những " nguy_cơ tiềm_ẩn " trong Warcraft , và do_đó , phải chịu trách_nhiệm trước cái chết của con_trai họ .
" Tại_Mỹ , Warcraft được xếp_hạng T , tức_là chỉ thích_hợp với trẻ từ 13 tuổi trở_lên . Nhưng_China_Cyber đã cố_tình lờ đi chuyện này và không thông_báo gì với người dùng .
Chúng_tôi không hề hay_biết chuyện này " , họ trả_lời trên tờ Thời báo Bắc_Kinh . " Dĩ_nhiên là họ biết rõ nội_dung Warcraft rất bạo_lực và đẫm máu , không phù_hợp chút nào với trẻ nhỏ " .
" Lẽ_ra , họ phải cảnh_báo trước về nguy_cơ nghiện game này và thực_hiện các biện_pháp ngăn_chặn người chơi quá_đà " .
Ngoài việc đòi bồi_thường , bố_mẹ của Chí còn yêu_cầu tất_cả các gói dịch_vụ và văn_hóa_phẩm liên_quan đến trò_chơi Warcraft phát_hành tại Trung_Quốc phải đề rõ cảnh_báo sức_khỏe và mức_độ bạo_lực .
Game vi_tính và game online đã bùng_nổ tại Trung_Quốc trong vài năm trở_lại đây , với ước_tính 13.8 triệu người chơi . Trước nỗi quan_ngại rằng giới trẻ đang bị nghiện game và đắm_chìm trong thế_giới ảo , Trung_Quốc đã ban_bố một loạt các quy_định về hạn_chế thời_gian chơi game ở các quán cafe Internet và phạt rất nặng những quán cho_phép trẻ nhỏ chơi game .
Theo VNN

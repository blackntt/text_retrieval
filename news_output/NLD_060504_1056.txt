﻿ Moussaoui thoát án tử_hình
Moussaoui ta ̣ i phiên xe ́ t xư ̉ Hôm 3-5 , Bô ̀ i thâ ̉ m đoa ̀ n ta ̣ i Hoa_Ky ̀ quyê ́ t đi ̣ nh : Zacarias_Moussaoui , ngươ ̀ i duy nhâ ́ t bi ̣ kê ́ t tô ̣ i liên_quan đê ́ n ca ́ c vu ̣ tâ ́ n công va ̀ o nga ̀ y 11-9-2001 ta ̣ i New_York va ̀ Washington , se ̃ không pha ̉ i chi ̣ u a ́ n tư ̉ hi ̀ nh .
Ngươ ̀ i phu ̣ tra ́ ch thông_tin cu ̉ a toa ̀ a ́ n , ông Edwards_Adams , thông ba ́ o Moussaoui se ̃ chi ̣ u a ́ n tu ̀ chung_thân .
Ca ́ c tha ̀ nh viên bô ̀ i thâ ̉ m đoa ̀ n đưa ra quyê ́ t đi ̣ nh cu ̉ a ho ̣ sau ba ̉ y nga ̀ y nghi ̣ a ́ n . Nếu quyê ́ t đi ̣ nh của họ là thô ́ ng nhâ ́ t thi ̀ Moussaoui mơ ́ i chi ̣ u a ́ n tư ̉ hi ̀ nh .
Tuy_nhiên , thay va ̀ o đo ́ , Moussaoui chỉ bi ̣ pha ̣ t a ́ n tu ̀ chung_thân ma ̀ không co ́ kha ̉ năng đươ ̣ c ta ̣ i ngoa ̣ i .
Zacarias_Moussaoui la ̀ ngươ ̀ i Pha ́ p gô ́ c Marốc , năm nay 37 tuô ̉ i , đa ̃ thư ̀ a nhâ ̣ n mi ̀ nh liên_quan tơ ́ i viê ̣ c chuâ ̉ n bi ̣ cho ca ́ c kê ́ hoa ̣ ch tâ ́ n công toa ̀ tha ́ p đôi cu ̉ a Trung_tâm Thương ma ̣ i thê ́ giơ ́ i va ̀ Lâ ̀ u Năm_Go ́ c , mă ̣ c du ̀ khi ca ́ c vu ̣ tâ ́ n công xa ̉ y ra , y đang bi ̣ giam giư ̃ .
Trong sa ́ u tuâ ̀ n xe ́ t xư ̉ ta ̣ i Virginia , ca ́ c công tô ́ viên đa ̃ lâ ̣ p luâ ̣ n ră ̀ ng Moussaoui đa ̃ giâ ́ u ki ́ n thông_tin - ma ̀ nê ́ u đươ ̣ c tiê ́ t lô ̣ thi ̀ đa ̃ co ́ thê ̉ ngăn chă ̣ n đươ ̣ c ca ́ c vu ̣ tâ ́ n công ta ̣ i New_York va ̀ Washington .
Khi ba ̉ n a ́ n đươ ̣ c tuyên , Moussaoui đa ̃ hô vang : " Hoa_Ky ̀ , mi đa ̃ thua cuô ̣ c ! " . Ba ̉ n a ́ n chung_thân thư ̣ c ra cu ̃ ng không co ́ sư ̣ nhâ ́ t tri ́ cu ̉ a toa ̀ n bô ̣ bô ̀ i thâ ̉ m đoa ̀ n , tuy_nhiên no ́ vâ ̃ n đươ ̣ c đưa ra thay vi ̀ a ́ n tư ̉ hi ̀ nh .
Theo TP/BBC

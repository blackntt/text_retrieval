﻿ Dacourt sẽ tới Juventus ?
( Dân_trí ) - Hiện_Juventus đang tiến_hành tiếp_xúc với tiền_vệ của ASRoma Olivier_Dacourt trong chiến_dịch củng_cố lực_lượng vào mùa giải tới .
Dacourt đã bắt_đầu gặp_gỡ với đội Bianconeri và có_vẻ như tình_hình tiến_triển rất khả_quan mặc_dù còn một_số bất_đồng vẫn chưa được giải_quyết .
Theo ông Giorgio_De_Giorgis , đại_diện của chàng cầu_thủ người Pháp này cho_biết : " Đã có những cuộc tiếp_xúc diễn ra với Juventus , nhưng vẫn chưa đi đến kết_quả cuối_cùng . Chưa có gi được quyết_định và hiện Fiorentina cũng đang đưa ra lời mời hấp_dẫn "
Dacourt thi_đấu khá thành_công dưới màu áo của Roma và anh sẽ rời SVĐ Olimpico theo diện cầu_thủ tự_do sau khi CLB từ_chối gia_hạn hợp_đồng mới .
Thực_ra trước_đây có nhiều tin_đồn về tương_lai của cầu_thủ này , rằng anh sẽ trở_lại chơi tại Premiership , nơi anh đã từng khoác_áo Everton và Leed_United , hoặc trở_về quê_hương thi_đấu .
Còn tổng_giám_đốc Juve , ông Luciano_Moggi luôn tỏ ra thích_thú săn_tìm những cầu_thủ chuyển_nhượng tự_do với “ chiêu_thức ” ký hợp_đồng 3 năm với mỗi mùa giải dưới 1 triệu bảng !
Hiện " Bà đầm_già " đã chắc có được Marco_Marchionni vào mùa giải tới và đang hướng tới Cristiano_Zanetti sắp_sửa hết hạn hợp_đồng với Inter_Milan .
Một cầu_thủ Italia khác là Cassano cũng rất muốn chuyển về chơi cho Juventus sau khi_không thành_công dưới màu áo của Real_Madrid . Tuy_nhiên mới_đây giám_đốc kỹ_thuật của Real , ông Benito_Floro cho_biết CLB không hề có ý_định bán Cassano .
Hiện_Juventus vẫn vững_vàng ở ngôi đầu_bảng với 81 điểm , hơn đội đứng thứ 2 Milan 5 điểm khi Serie A chỉ còn 4 vòng đấu nữa . Tại vòng đấu tới , Juventus sẽ có một trận đấu khó_khăn khi đón Lazio trên sân_nhà Delle_Alpi . Thu_Hương

﻿ Làm rõ việc chung chi giữa cán_bộ Yteco và nhân_viên hải_quan
Một cán_bộ Cục Cảnh sát điều_tra tội_phạm về trật_tự quản_lý kinh_tế và chức_vụ cho_biết ngoài hành_vi buôn_lậu của các bị_can , cơ_quan điều_tra cũng đang tiến_hành làm rõ việc có hay không việc “ đi_lại ” , chung chi giữa cán_bộ Yteco và hải_quan qua các phi_vụ buôn_lậu tân_dược .
Theo nguồn tin riêng của Tuổi_Trẻ , cơ_quan điều_tra cũng đã triệu_tập một_số cán_bộ hải_quan để làm rõ vấn_đề này . Ngoài_ra , một_số cán_bộ hải_quan khác có mua cổ_phiếu của Yteco cũng sẽ được cơ_quan điều_tra triệu_tập , lấy lời khai trong thời_gian tới .
HOÀNG_KHƯƠNG

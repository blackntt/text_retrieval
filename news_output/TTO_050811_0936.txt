﻿ Khai_quật khảo_cổ lớn ở Quảng_Ninh và Hà_Tây
Tại di_chỉ Ba_Vũng , thôn 2 xã Hạ_Long , huyện Vân_Đồn , tỉnh Quảng_Ninh , từ ngày 5-8 đến 31-8 , cuộc khai_quật thứ nhất được thực_hiện trên diện_tích 100m 2 .
Cuộc khai_quật thứ_hai có sự tham_gia của chuyên_gia Nhật_Bản được thực_hiện tại 5 địa_điểm khu_vực xã Đường_Lâm , thị_xã Sơn_Tây ( Hà_Tây ) từ 14-8 đến 5-9 .
Bộ tem kỷ_niệm 60 năm thành_lập Công_an Nhân_dân
Ngày 10-8 , tại Bưu_điện Thành_phố Hà_Nội , bộ tem đặc_biệt kỷ_niệm 60 năm ngày thành_lập Công_an Nhân_dân 19-8-1945 đến 19-8-2005 đã chính_thức được phát_hành .
Bộ tem gồm hai mẫu , phác_họa một_cách khái_quát về lực_lượng Công_an Nhân_dân xưa và nay thông_qua hình_ảnh lực_lượng công_an trong thời_kỳ kháng_chiến bảo_vệ Tổ_quốc chống quân xâm_lược và đội_ngũ Công_an Nhân_dân trong thời_kỳ đổi_mới với bản_chất truyền_thống từ nhân_dân mà ra , vì nhân_dân mà chiến_đấu phục_vụ , gắn_bó mật_thiết với nhân_dân .
Bộ Bưu chính Viễn_thông VN , Bộ Công an và Công_ty Tem thuộc Tổng_công_ty Bưu_chính Viễn_thông VN đã phối_hợp phát_hành bộ tem trên .
Nhạc dân_tộc VN trên đất Italia
Nhóm nghệ_thuật dân_tộc VN do Nghệ_sĩ_ưu_tú Vũ_Bá_Phổ dẫn_đầu đã tham_gia Liên_hoan ca_nhạc tại thành_phố Susa ở vùng Piemôntê - khu du_lịch nổi_tiếng của Italia , nơi sẽ diễn ra Đại_hội thể_thao Olympic mùa Đông tháng 2-2006 .
Trong đêm biểu_diễn đầu_tiên , ngày 7-8 tại Quảng_trường Trung_tâm thành_phố , nghệ_sĩ Bá_Phổ và các thành_viên của đoàn , trong đó có nữ nghệ_sĩ Mai_Liên cùng nghệ_sĩ Vũ_Bá_Nha , đã đem đến cho khán_giả và du_khách nước_ngoài một đêm nhạc dân_tộc phong_phú và hấp_dẫn mang đậm_nét văn_hóa truyền_thống dân_gian .
Trong tháng 8 này , đoàn sẽ tiếp_tục biểu_diễn tại tại nhiều địa_phương khác của Italia , sau đó sẽ quay lại Roma biểu_diễn tại trụ_sở mới của Đại_sứ_quán Việt_Nam vào đúng ngày Quốc_khánh 2-9 .
Theo TTXVN

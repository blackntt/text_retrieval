﻿ Colombia : Phá tan âm_mưu tấn_công chuyên_cơ tổng_thống
Nhờ vào các thông_tin tình_báo , cảnh_sát đã phát_hiện 110 kg chất_nổ có sức công_phá rất mạnh cùng các thiết_bị hiện_đại được sử_dụng làm bệ_phóng tên_lửa tự_tạo trong một khu trại gần sân_bay Cartagena , vùng_ven biển Caribbean , nơi chuyên_cơ của tổng_thống Colombia cất_cánh . Hai người đã bị bắt khi đang sở_hữu các kế_hoạch bay của chuyên_cơ tổng_thống cùng những tài_liệu về những chuyến đi gần đây của tổng_thống Alvaro_Uribe
Theo đại_tá Mauricio_Agudelo , cảnh_sát trưởng tỉnh Bolivar , một vụ nổ xảy_ra trong thời_điểm chuyên_cơ đang đỗ sẽ là một thảm_kịch thật_sự .
NG.DAO ( Theo Le_Monde )

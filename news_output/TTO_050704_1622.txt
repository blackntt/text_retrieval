﻿ Qi_Gong - bậc thầy thư_pháp Trung_Quốc đã ra_đi
Bút_tích của vị chủ_tịch danh_dự Hiệp_hội những nhà thư_pháp Trung_Quốc có_thể bắt_gặp ở rất nhiều tòa nhà_nhà nổi_tiếng , các điểm danh_thắng Trung_Quốc và trên nhiều tựa sách .
Vì là một học_giả uyên_thâm về các di_tích văn_hóa Trung_Quốc , ông đã được bổ_nhiệm làm giám_đốc Học_viện nghiên_cứu trung_ương về văn_hóa và lịch_sử 1999 .
Qi_Gong có họ là Aisin_Giorro , ông còn sử_dụng tên Yuan_Bai để làm bút_danh .
Trong_suốt quãng đời , bậc thầy thư_pháp đã viết nhiều sách về lý_luận - thực_hành thư_pháp và các chữ_viết cổ Trung_Quốc đầy phong_cách . Sự ra_đi của ông là một tổn_thất lớn cho Trung_Quốc , cũng_như thế_giới nghệ_thuật truyền_thống và thư_pháp
A.NG . ( Theo Xinhua , AFP )

﻿ Câu_lạc_bộ các ông chồng : Mô-đen vợ chúa chồng tôi !
Có mấy môđen vợ chúa sau đây mà nhóm chồng lép tụi tui mới tổng_kết được , bày ra để bà_con tham_khảo :
Mô-đen thứ nhất : " Ta là số 1 ! " :
Các nường theo mô - đen này " đa_phần " là con_gái rượu , được cưng như trứng mỏng từ nhỏ đến lớn , lấy chồng thì chồng bị bắt rể , phận ở_đậu nên không dám hó_hé cục cựa gì .
Chỉ mới góp vợ tí_ti thì nường đã la toáng lên như bị bóp_cổ , như bị đấm_đá dã_man , thế_là nhạc_phụ nhạc_mẫu đại_nhân le_te chạy vào bênh con_gái , chửi con_rể . Vậy_là xong phim !
Mô-đen thứ_hai : " Ta là cổ_thụ ! " :
Các nường này khỏi cần nương_náu nhà cha_mẹ để dựa hơi lên_mặt với chồng . Vì bản_thân các nường đã có nội_lực thâm hậu , tức_là sức_mạnh tài_chính dồi_dào ! Vợ kiếm nhiều tiền mà chồng kiếm èo_uột thì đương_nhiên cán_cân nghiêng về phía kim_tiền ngay . Trong nhà , nường tự xem mình là cổ_thụ , chồng_con chỉ là phận dây_leo tầm_gửi , nên bảo gì là phải nghe đấy !
Môđen thứ_ba : " Ta là đại_nhân ! " :
Là đại_nhân không phải là người bự hay làm chức gì bự , chỉ đơn_giản " đại_nhân " chính là " đại_lượng " . Có_nghĩa là trong quá_khứ , chồng lỡ dại đã phạm phải một tội_ác tày_trời gì đấy , nhưng nường đã mở lượng hải_hà tha_thứ cho . Kể từ đó , nường trở_thành " đại_nhân " còn chồng thành " tội_đồ " bị án_treo , phải cư_xử sao đấy để mà được hưởng khoan_hồng của nường , nếu_không thì nường lu_loa khắp thiên_hạ thì chết !
Mô-đen thứ_tư : " Ta là thần hộ_mệnh ! " :
Chẳng may lấy nhầm một nường thuộc dạng " con_ông_cháu_cha " , có bộ rễ cây_đa_cây_đề , thì dù_cho nường ở nhà chồng , nường chẳng kiếm ra được xu nào , chồng chẳng phạm tội_ác nào … thì chồng vẫn bị bắt " việt_vị " như_thường . Vì gốc nường quá bự , rễ nường vươn quá xa , nên ráng ngo_ngoe cỡ nào cũng bị nường thộp được !
NHÓM_CHỒNG_LÉP st

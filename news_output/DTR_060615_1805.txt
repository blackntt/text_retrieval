﻿ Tổng_thống Bush chế_nhạo phóng_viên dị_tật
Tổng_thống Mỹ_Bush hôm_qua 14/6 đã phải lên_tiếng xin_lỗi vì đã có những lời_nói không được lịch_sự cho lắm đối_với một phóng_viên đeo kính_râm khi phỏng_vấn ông .
Vụ_việc xảy_ra tại một cuộc họp_báo sau khi ông Bush trở_về từ chuyến thăm Iraq . Ông Bush đã nói với phóng_viên Peter_Wallsten của tờ báo Los_Angeles_Times rằng , anh muốn hỏi gì trong lúc vẫn đeo kính_râm như_vậy .
“ Ở đây hoàn_toàn không có nắng , ” ông Bush vừa nói với vẻ mỉa_mai vừa cười . Rất nhiều tiếng cười từ dưới cũng rộ lên .
Điều đáng nói ở đây là Wallsten phải đeo kính_râm vì ông mắc bệnh Stargardt , một chứng_bệnh gây khó_khăn trong việc tiếp_nhận hình_ảnh của mắt . Do_đó mắt của Wallsten rất nhạy_cảm với ánh_sáng , thậm_chí vào ngày không có nắng .
Wallsten cho_biết Tổng_thống Bush sau đó đã gọi điện xin_lỗi ông và nói rằng không biết ông gặp vấn_đề về mắt . Wallsten lịch_sự trả_lời rằng việc xin_lỗi là không cần_thiết bởi ông không cảm_thấy bị xúc_phạm . Ông cũng không nói cho ai biết là ông có dị_tật về mắt .
Wallsten chỉ nói rằng ông không cảm_thấy hài_lòng khi Tổng_thống không trả_lời câu_hỏi của ông tại cuộc họp_báo .
Trước đó , không biết là vô_tình hay cố_ý , Bush cũng đã có những lời_nói không được đẹp cho lắm . Cụ_thể là tháng trước , tại bang Florida , Bush cũng đã nói với một người đang đi xe_lăn rằng “ Trông ông có_vẻ rất thoải_mái ” .
Nguyên_Hưng_Theo AP , CNN

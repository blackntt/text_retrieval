﻿ Thạc sỹ Thái trở_lại giảng_dạy tại ĐH Khoa_học Tự_nhiên
Sau quyết_định của Giám_đốc ĐH Quốc_gia Hà_Nội hồi đầu tháng 5 vừa_qua về việc khôi_phục quyền_lợi cho thạc sỹ Nguyễn_Thị_Thái , cô Thái đã nhận được quyết_định tiếp_nhận trở_lại và bố_trí công_tác vào chiều_hôm qua , 29/5 .
Thạc sỹ Nguyễn_Thị_Thái sẽ được tạm xếp lương bậc 6 , hệ_số 3,99 của ngạch Giảng_viên , mã_số 15.111 . Đây là mức lương tương_ứng với ở thời_điểm cô Thái rời trường ĐH Khoa_học Tự_nhiên 7 năm trước .
Được biết , ngày 5/5 vừa_qua , cô Thái cũng có đơn đề_nghị với nguyện_vọng được về lại làm công_tác giảng_dạy tại khoa Hóa . Cô Thái cũng cho_biết thêm , hôm_qua , cô đã viết đơn xin được nghỉ công_tác trong tháng 6 , 7 vì lý_do sức_khỏe chưa thật hồi_phục sau sự_việc vừa_qua .
Đồng_thời , cô cũng cần có thời_gian để chuẩn_bị giáo_án , cập_nhật thêm kiến_thức sau 7 năm không đứng trên bục giảng . Và đầu tháng 9 , sau kỳ nghỉ_hè , khi năm_học mới bắt_đầu , cô sẽ sẵn_sàng cho công_việc bị gián_đoạn lâu_nay .
Thạc sỹ Nguyễn_Thị_Thái xác_định : cô sẽ làm công_việc giảng_dạy ít_nhất là 5 năm nữa , cho_đến tuổi về hưu . Phương_Thảo - Trần_Đức

﻿ Ông Vũ_Hưng_Bình bị bắt tại Nội_Bài
Trung_tuần tháng năm , cơ_quan điều_tra đã khởi_tố bị_can , đồng_thời ra_lệnh bắt tạm giam ông Vũ_Hưng_Bình về hành_vi trốn_thuế trong thương_vụ nhập_khẩu ôtô . Tuy_nhiên , tại thời_điểm này bị_can không có_mặt ở nơi cư_trú nên lệnh tạm giam chưa được tống_đạt và thực_thi . Xác_định bị_can Vũ_Hưng_Bình có dấu_hiệu bỏ trốn khỏi nơi cư_trú , cơ_quan điều_tra đã phát lệnh truy_nã đối_với ông Bình .
Quá_trình điều_tra cho thấy Vũ_Hưng_Bình đã cấu_kết với Mai_Quý_Cường , Nguyễn_Thanh_Hải , Nguyễn_Phát_Đạt nhập_khẩu 93 xe ôtô các loại , chiếm_đoạt 13,11 tỉ đồng_tiền thuế_giá_trị_gia_tăng . Cho_đến nay , người_nhà bị_can Vũ_Hưng_Bình và một_số cá_nhân khác đã đến cơ_quan điều_tra nộp lại 2 tỉ đồng .
M . Q .

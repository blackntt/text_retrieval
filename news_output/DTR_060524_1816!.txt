﻿ Mua vé máy_bay cho … người chết
Nhân_viên bán vé máy_bay của hãng hàng_không quốc_tế Baiyun , Trung_Quốc thực_sự sốc khi một người khách đến đặt vé cho ... người cha quá_cố của mình .
Tháng trước , ông Zhong , người Trung_Quốc sống ở Mỹ , cùng_với gia_đình về quê ở thành_phố Meizhou , tỉnh Quảng_Đông để thăm họ_hàng . Nhưng , bố của ông Zhong không may lại bị chết ngay trên quê_hương .
Để tỏ lòng hiếu_thảo với bố , Zhong vẫn mua một chiếc vé máy_bay cho bố mặc_dù Zhong biết chắc_chắn rằng bố ông không_thể lên máy_bay để quay trở_về Mỹ được .
N . H_Theo_China Daily

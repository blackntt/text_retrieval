﻿ Sinh_viên dùng ảnh sex tống_tiền
Chiều ngày 15/9 , gia_đình bà T ở phường Phan_Đình_Phùng , thành_phố Thái_Nguyên liên_tiếp nhận được nhiều cú điện_thoại của hai đối_tượng nam_giới . Chúng yêu_cầu bà nộp 50 triệu đồng , nếu_không chúng sẽ giết bà và đem phát_tán hình_ảnh con_gái bà quan_hệ nam_nữ không lành_mạnh .
Ngay_lập_tức , bà T trình_báo sự_việc với Công_an thành_phố Thái_Nguyên .
Sáng 16/9 , khi các đối_tượng ra khu_vực đường_tròn trung_tâm thành_phố để nhận tiền_của bà T thì bị công_an bắt quả_tang .
Hai đối_tượng là Chu_Anh_Tuấn ( SN 1982 , ở tổ 17 phường Đức_Giang , quận Long_Biên , Hà_Nội ) và Nguyễn_Tử_Đại ( SN 1982 , sinh_viên Học_viện Kỹ_thuật Quân_sự ) hiện đang bị CA tạm giam chờ làm rõ . Theo Lao_Động

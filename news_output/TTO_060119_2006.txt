﻿ Ban xã_hội sẽ có sức hút hơn
- Năm_học 2005 - 2006 , theo quy_định của Luật_Giáo dục 2005 , sẽ bỏ kỳ thi tốt_nghiệp THCS . Bộ GD - ĐT sẽ ban_hành quy_định xét công_nhận tốt_nghiệp THCS trong quý I-2006 .
Đồng_thời bộ cũng đang tiếp_tục lấy ý_kiến từ cơ_sở và đưa ra thảo_luận lấy ý_kiến trong hội_nghị giao_ban giám_đốc sở sắp tới để chuẩn_bị cho việc ban_hành Quy_chế tuyển_sinh lớp 6 và lớp 10 sửa_đổi .
Phương_thức tuyển_sinh lớp 10 dự_kiến sẽ là xét tuyển và thi_tuyển cho từng trường phổ_thông , một trường không có đồng_thời hai hình_thức này , để đảm_bảo sự công_bằng . Phương_án tuyển_sinh sẽ do sở trình UBND tỉnh , thành quyết_định , bộ không can_thiệp .
Như_vậy là trong một địa_phương có_thể thực_hiện cả hình_thức thi hoặc xét đối_với mỗi trường , thưa ông ?
- Đúng vậy . Cũng có_thể một địa_phương chỉ thực_hiện toàn_bộ là thi hoặc xét . Trước_đây , nhiều địa_phương cho rằng nếu thi thì thi 3 môn là Toán , Ngữ_văn và Ngoại_ngữ . Tuy_nhiên , các ý_kiến gần đây đã thiên về kết_luận chỉ thi 2 môn Toán và Ngữ_văn để cho kỳ thi được nhẹ_nhàng .
Dự_thảo của bộ có nói về việc " bổ_sung điểm khuyến_khích đối_với kết_quả rèn_luyện đạo_đức và học_tập của HS trong 4 năm_học ở THCS " . Điểm khuyến_khích ưu_tiên chiếm tỉ_lệ tối_đa là bao_nhiêu trong tổng điểm xét tuyển , thưa ông ?
- Chúng_tôi đang tính_toán về điều này . Có_thể " học_lực giỏi + hạnh_kiểm tốt " ở lớp 6 ( lớp 7 , lớp 8 , lớp 9 ) được cộng 1 điểm . Như_vậy HS sẽ được cộng tối_đa là 4 điểm để vừa tôn_trọng kết_quả của các em trong các kỳ thi , vừa động_viên kết_quả học_tập .
Hoặc cũng có_thể quy_định tổng điểm khuyến_khích ưu_tiên chiếm tỉ_lệ không quá 30% tổng điểm xét tuyển . Hiện_nay chưa có quy_định chính_thức nhưng chắc_chắn là bộ sẽ không chốt con_số cụ_thể mà chỉ có khung để các địa_phương tự thực_hiện .
Các ông có đưa ra dự_báo của một nhà chuyên_môn : Sẽ có khoảng 50% HS chọn ban cơ_bản , 30% chọn ban KHTN và 20% chọn ban KHXH-NV . Ông có_thể đánh_giá tính chính_xác của dự_báo ?
- Chúng_tôi có nhắc đến dự_báo trên , và cũng muốn chờ xem dự_báo này có đúng không . Tuy_nhiên , tôi có_thể nói rằng , với tình_hình hiện_nay , ban KHXH-NV sẽ có sức hút hơn vì đã được bổ_sung thêm môn Ngoại_ngữ nâng cao .
Bộ đang soạn tài_liệu hướng_dẫn HS lớp 9 tìm_hiểu về các ban , xa hơn là định_hướng nghề_nghiệp sau khi tốt_nghiệp THPT . Trong năm_học này , HS lớp 9 ở THCS sẽ được hướng_dẫn lựa_chọn ban trước khi xin dự_tuyển vào trường THPT năm tới .
Có đảm_bảo tất_cả các trường THPT trên toàn_quốc có_thể dạy theo cả 3 ban được không ? Trong trường_hợp một ban có quá ít HS đăng_ký thì nhà_trường sẽ phải xử_lý như_thế_nào , thưa ông ?
- Dù thế_nào đi_nữa thì việc học ban nào trước_hết là theo nguyện_vọng , năng_lực của HS , do khả_năng tổ_chức và điều_kiện cơ_sở vật_chất của các trường . Trường_hợp có quá ít HS cho một ban , không đủ để tổ_chức thành một lớp theo quy_định , hiệu_trưởng sẽ phải điều_chỉnh . Trước_mắt là vận_động HS đăng_ký lại ban , và căn_cứ vào năng_lực để sắp_xếp lại .
Tuyệt_đại_bộ_phận HS sẽ được thoả_mãn theo đăng_ký . Và cũng không loại_trừ một trường chỉ có một ban nếu điều_kiện cơ_sở vật_chất và giáo_viên cho_phép .
Xin cảm_ơn ông .
Theo Lao_Động

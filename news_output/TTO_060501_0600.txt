﻿ Ai cũng khổ
Cứ đến đợt chấm vở sạch chữ đẹp là cả học_sinh và thầy_cô ... nhốn_nháo cả lên . Khổ_nỗi , không phải học_sinh nào cũng viết đẹp , giữ_gìn vở cẩn_thận . Do_đó , khi chấm vở , các em được thông_báo trước một tuần .
Chả_là để ... đẹp hóa , Trường T đã có một chiêu rất triệt_để : thay toàn_bộ vở xấu bằng một loạt vở mới ! Các bài_học được sao lại bằng những em học_sinh viết đẹp . Những em viết xấu thì_phải năn_nỉ gia_đình cho tiền để thay toàn_bộ vở , trả công cho những bạn viết đẹp chép hộ bài . Thầy_cô nhiều khi cũng phải mệt người , mất_công ... đe_nẹt những em chữ xấu ý_thức được " tội " của mình sẽ ảnh_hưởng đến thành_tích chung của lớp ; hay vận_động các em viết đẹp tham_gia phong_trào . Phụ_huynh cũng không_thể đứng ngoài cuộc .
Ai cũng khổ , mà chung_qui_lại chỉ vì bệnh thành_tích .
ĐƯỜNG_LOAN ( Hà_Nội )

﻿ Vụ 10 lao_động tại Malaysia về nước trước thời_hạn : Enlexco phải đảm_bảo quyền_lợi
Mới_đây Cục Quản lý lao_động ngoài nước ( Bộ Lao động - thương_binh & amp ; xã_hội ) đã có công_văn yêu_cầu Enlexco phải kiểm_tra , xác_minh lý_do về nước trước hạn của mười lao_động này . Đồng_thời thực_hiện thanh_lý hợp_đồng cho người lao_động trên cơ_sở bảo_đảm quyền_lợi chính_đáng cho họ ...
Những người lao_động bị về nước trước thời_hạn đều là những nông_dân nghèo từ Bến_Tre , Bình_Định và phải vay tiền ngân_hàng nộp cho Enlexco để đi làm_việc tại Malaysia với hợp_đồng ba năm .
Tuy_nhiên , những lao_động này đã bị chủ lao_động “ cho ngưng việc , thu thẻ vào xưởng với nhiều lý_do cá_nhân ” , thậm_chí 9/10 người này đã bị cảnh_sát nước sở_tại bắt_giữ ... Đến_Malaysia ngày 26-5 thì ngày 22-11-2005 họ bị đưa về nước mà không rõ lý_do .
Đ . BÌNH

﻿ # # D : \ Corpus \ HTML\ DTR\ www2.dantri.com.vn \ chuyenla \ 2006 \ 4 \ 112589.vip.htm
Con nhện hình mặt người
Những đường sọc màu xanh sáng trên lưng con nhện tạo thành đường_nét giống mặt người một_cách kỳ_lạ .
Điểm thu_hút đầu_tiên chính là màu sáng xanh đặc_biệt của con nhện , khác hẳn với màu vốn có cùng loài . Tuy_nhiên , hình mặt người tạo bởi những đường vằn_vện sau lưng mới là điều … dị_thường hơn cả .
Chú nhện này được tình_cờ phát_hiện tại tỉnh Sơn_Đông , Trung_Quốc vào ngày 18/4 vừa_qua , khi đang giăng tổ bắt mồi .
Thùy_Vân_Theo_Newsphoto

﻿ Học_bổng toàn_phần khóa Thạc_sĩ tại Anh
Trung_tâm Hợp_tác Trách_nhiệm Xã_hội quốc_tế - Trường ĐH Nottingham , Anh thông_báo cấp 13 suất học_bổng cho những sinh_viên đăng_ký nhập_học tại trường năm_học 2006 .
Học_bổng được phân_bổ cụ_thể :
- 1 suất học_bổng toàn_phần bao_gồm học_phí và chi_phí sinh_hoạt trị_giá khoảng 12.306 bảng Anh / năm .
- 2 học_bổng 100% học_phí .
- 5 học_bổng , mỗi suất trị_giá 2.000 bảng Anh ( ngành Xã_hội )
- 5 học_bổng , mỗi suất trị_giá 2.000 bảng Anh ( ngành Marketing và quản_lý Du_lịch )
Điều_kiện tham_gia :
- Tốt_nghiệp đại_học chuyên_ngành loại giỏi .
- Điểm TOEFL hoặc IELTS .
- Ưu_tiên sinh_viên đến từ các nước_đang_phát_triển .
- Đăng_ký học toàn thời_gian của khóa học .
- Đặt_cọc 300 bảng Anh .
Thời_hạn nộp đơn xin học_bổng : 30/6/2006 .
Để biết thêm thông_tin có_thể xem trang_web :
http : / / www.nottingham.ac.uk
Hồ_sơ và đơn xin học_bổng gửi đến địa_chỉ :
Scholarship_Section , International_Office
University of Nottingham
University_Park
Nottingham NG7 2RD
United_Kingdom
ĐT : +44 ( 0 ) 115 8466631
Email : MA-MScBusiness@nottingham.ac.uk Thu_Hoài

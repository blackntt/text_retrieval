﻿ Khai_mạc Hội_nghị Viết văn trẻ toàn_quốc lần 7
Tối_qua , 11-5 , hội_nghị những người viết văn trẻ toàn_quốc lần thứ 7 đã khai_mạc bằng cuộc giao_lưu “ Thơ_trẻ trong phố cổ ” .
Dự_kiến , ban_đầu chỉ mời 50 nhà_văn trẻ , song đến phút chót đã tăng lên hơn 100 . Đây là lần đầu_tiên hội_nghị này tổ_chức tại Hội_An , 6 lần trước đều ở Hà_Nội .
Sau 4 năm chờ_đợi , các nhà_văn trẻ lại được dịp hội_ngộ cùng nhau . Nhìn quanh_quẩn vẫn là các gương_mặt cũ , mới chăng là một_số nhà_văn ít xuất_hiện trên các diễn_đàn báo_chí và thường thì họ ở các tỉnh_lẻ . Nhưng dù gì đây cũng là dịp gặp_mặt hiếm_hoi như nhà_văn Nguyễn_Ngọc_Tư ( Cà_Mau ) nói : “ Tư muốn ra Trung , bây_giờ ra Trung rồi , vui lắm ” .
Sáng nay , 12-5 , là ngày làm_việc chính của hội_nghị . Đoàn nhà_văn trẻ sẽ tham_quan một_số di_tích lịch_sử và danh_lam của Quảng_Nam . Chiều cùng ngày sẽ tọa_đàm “ Văn tôi và phê_bình tôi nói_gì ? ” , buổi tối sẽ tọa_đàm “ Thơ tôi nói_gì ? ” và “ Hai phút cho một ý_tưởng sáng_tạo ” . Hội_nghị dự_kiến kết_thúc vào 12 giờ trưa 14-5 .
Tr . H . Nhân

﻿ Hạ mức khởi_điểm đóng thuế_thu_nhập : Sẽ mất_lòng dân
Bà Phạm_Chi_Lan nói : " Tôi không tán_thành cách làm này của Bộ Tài chính . Bởi_vì , ngay cả mức khởi_điểm thu thuế_thu_nhập cá_nhân là 5 triệu đồng / tháng cũng không hợp_lý trong điều_kiện nước ta . Vì mức thu_nhập này bây_giờ không cao .
Hạ mức khởi_điểm đóng thuế_thu_nhập cá_nhân xuống đến 3 triệu đồng / tháng sẽ làm hạn_chế mức tiêu_dùng , mua_sắm của người_dân . Hơn_nữa , mức thu_nhập càng thêm thấp do phải chịu thuế sẽ không kích_thích tinh_thần sản_xuất , kinh_doanh . Những tác_nhân trên làm hạn_chế phát_triển kinh_tế .
Từ chỗ phải đóng thuế trong khi thu_nhập còn thấp , việc đầu_tư nuôi_dạy trẻ_em sẽ bị hạn_chế , ảnh_hưởng không tốt đến tương_lai gia_đình và quốc_gia .
Một điều nữa , mở_rộng diện thu thuế trong khi giá_cả trên một loạt lĩnh_vực tăng thì làm_sao người_dân có_thể tích_luỹ để mua_sắm nhà_cửa , xe_cộ ... cải_thiện cuộc_sống " .
Ông Trần_Quang_Thắng , Viện_trưởng Viện quản_lý và kinh_tế , nói : " Làm như_thế sẽ mất_lòng dân . Nhiều người thu_nhập 3 - 4 triệu/tháng phải mua nhà cho người có thu_nhập thấp trả_góp trong 20 năm . Bây_giờ lại đánh thuế_thu_nhập vào đối_tượng này thì thật_là bất_hợp_lý " .
Bà Phạm_Chi_Lan_Bộ_Tài chính bất_lực trong thu thuế , phải hạ mức khởi_điểm để tận_thu ?
Có ý_kiến cho rằng việc hạ mức khởi_điểm thu thuế của Bộ Tài chính chứng_tỏ sự bất_lực trong việc thực_thi thu thuế và phải chọn biện_pháp hạ mức khởi_điểm để tận_thu ?
- Bà Phạm_Chi_Lan : Hiện còn nhiều nguồn thuế có ý_nghĩa lớn hơn , thu lớn hơn , như thuế trên doanh_nghiệp , thuế nhập_khẩu ... chưa được thu tốt . Lẽ_ra Bộ Tài chính nên tập_trung khắc_phục những tồn_tại để hệ_thống thu thuế tốt hơn , thay_vì mở_rộng diện nộp thuế để tận_thu .
Hiện vẫn còn tình_trạng tiêu_cực trong ngành thuế : để kéo_dài nợ thuế rất lâu và không có biện_pháp xử_lý , dẫn đến trốn_thuế ; mặc_cả giữa người đi thu và đối_tượng thu dẫn đến một lượng tiền thuế đáng_kể vào túi cán_bộ thuế và người trốn_thuế , do hệ_thống thuế thiếu minh_bạch , dễ du_di . Giữa nơi này , nơi khác , hay cùng một sản_phẩm mức thuế cũng thiếu đồng_nhất .
Hơn_nữa , chỉ cần kiểm_soát chi cho tốt thì đã tiết_kiệm được rất nhiều , đảm_bảo nguồn ngân_sách mà không cần thu thêm thuế . Nhà_nước ta " chăm_lo " nhiều đến thu thuế , nhưng lại ít chăm_lo kiếm soát chi_tiêu cho tốt . Thất_thoát , lãng_phí hiện_nay cho thấy điều đó .
Xu_hướng chung của các nước là ngày_càng giảm thu từ người_dân , nhưng vẫn đảm_bảo chi_tiêu do quản_lý chặt_chẽ và thúc_đẩy nền kinh_tế đi lên .
- Ông Trần_Quang_Thắng : Thay_vì hạ mức khởi_điểm thu thuế để tận_thu , có_thể tăng các khoản thu khác : nới rộng thị_trường bất_động_sản , thu thuế sử_dụng đất .
Các dự_án lớn của nhà_nước cần tăng_cường sự tham_gia đấu_thầu của tư_nhân . Nguồn thu từ tư_nhân rất đáng_kể và không trốn được vì có văn_bản xác_nhận cụ_thể : phần_nào được hưởng , phần_nào phải đóng cho đất_nước .
Ngoài_ra , phải tư_nhân_hoá mạnh_mẽ , làm nảy_sinh nhiều tập_đoàn . Từ đó sẽ có nguồn thu từ tập_đoàn , hiệp_hội , thay_vì từ người nghèo .
Ông Trần_Quang_Thắng " Doanh_nghiệp sẽ càng khó_khăn trong hội_nhập "
Ông ( bà ) có_thể phân_tích căn_nguyên đầy_đủ của việc Bộ Tài chính hạ mức khởi_điểm đóng thuế_thu_nhập cá_nhân ?
- Bà Phạm_Chi_Lan : Có người lập_luận nên đánh thuế_thu_nhập cá_nhân với tất_cả mọi người có thu_nhập để đảm_bảo công_bằng . Điều này chỉ đúng với xã_hội có mức thu_nhập bình_quân cao hơn VN . Các nước từng_trải qua thời_kỳ kém phát_triển không đánh thuế người thu_nhập thấp . Chỉ khi bình_quân thu_nhập đã cao thì họ mới đánh thuế mọi người_dân có thu_nhập .
Bao_giờ thu_nhập bình_quân đầu người của VN lên tới 1.500 - 2.000 USD thì hãy mở_rộng diện đóng thuế . Đừng chỉ lấy cái lợi của nhà_nước , bỏ_qua quyền_lợi của người_dân .
- Ông Trần_Quang_Thắng : Bộ_trưởng Tài_chính cần giải_thích cụ_thể cho biện_pháp này , để người_dân cùng biết và đóng_góp ý_kiến .
Thu_nhập 3 triệu đồng / tháng chủ_yếu thuộc về công_nhân , viên_chức . Vậy , biện_pháp mở_rộng diện thu thuế của Bộ Tài chính ảnh_hưởng mạnh đến các doanh_nghiệp ?
- 3 triệu đồng / tháng là mức lương phổ_biến của đội_ngũ quản_trị bậc thấp , lực_lượng lao_động mới tốt_nghiệp đại_học .
Đánh thuế_thu_nhập của họ thì họ sẽ đòi_hỏi các doanh_nghiệp phải trả lương cao hơn . Như_vậy tạo thêm gánh nặng , buộc doanh_nghiệp tăng giá_thành và sẽ khó cạnh_tranh trong điều_kiện hội_nhập . Điều này càng chứng_tỏ đưa ra biện_pháp trên vào thời_điểm hiện_nay là bất_hợp_lý .
Hơn_nữa , dù đánh thuế khởi_điểm ở mức nào thì cũng cần tính đến gia_cảnh của đối_tượng một_cách thực_sự khoa_học ?
- Ở các nước , như Thuỵ_Điển , những người độc_thân chịu thuế_thu_nhập cao hơn hẳn những người có gia_đình . Ở nước ta , trách_nhiệm của mỗi cá_nhân với gia_đình càng nặng hơn . Với việc đánh thuế người thu_nhập không cao , xã_hội đã không chăm_lo cho các gia_đình , lại còn hạn_chế các cá_nhân chăm_lo cho gia_đình .
Theo Viet NamNet

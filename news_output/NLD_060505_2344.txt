﻿ Huda_Huế tiếp_tục “ bất bại ” ?
Kém_Quân khu 5 , đội_xếp trên mình , 5 điểm , lại gặp Huda_Huế , đội bóng bất bại sau 14 vòng đấu Giải_Bóng đá hạng Nhất_Quốc gia-Cúp Alphanam_Fuji 2006 , ngay trên sân Tự_Do , CLB chót bảng TPHCM đang ở vào thế đường cùng , buộc phải có điểm để cải_thiện vị_trí của mình .
Chính những yếu_tố này sẽ biến trận đấu trở thành_tâm điểm của lượt 15 , diễn ra_vào chiều 6-5 . Có được 3 điểm sau trận này , chủ nhà Huế nhiều cơ_hội lọt vào top 3 khi chỉ cách đội Halida_Thanh_Hóa đầu_bảng 3 điểm . Các trận đấu còn lại : Halida_Thanh_Hóa - Quân_khu 5 , Quân_khu 4-Thể Công_Viettel , An_Giang - SXKT Đà_Lạt , Đồng_Nai - Đồng_Tháp , Đá_Mỹ nghệ Sài_Gòn - Sơn_Đồng_Tâm_Long_An , Tây_Ninh - Quảng_Nam .
Q.Liêm

﻿ Paris_Hilton sẽ thay_đổi hình_ảnh
Không bao_lâu nữa , Paris_Hilton sẽ lại xuất_hiện trước công_chúng trong một bộ phim mới , hết_sức bất_ngờ . Đó là vai_diễn mẹ Teresa .
Đạo_diễn T . Rajeevnath muốn tìm một diễn_viên có vai_vế trong xã_hội cho bộ phim của mình . Trả_lời với báo_giới ông cho rằng , sẽ thay_đổi hình_ảnh của Paris để trông cô trở_nên thánh_thiện hơn .
Ông nói : “ Hình_ảnh này sẽ trở_nên mạnh_mẽ trong tôi nhất_là khi trao_đổi với Paris . Cô ấy đã biểu_lộ sự vui_sướng khi biết tôi đưa tên cô ấy vào danh_sách vai_diễn trong phim ” .
Trước_đây , Paris đã từng tham_gia diễn_xuất trong phim House_Of_Wax . Cô nàng đỏng_đảnh này cũng đang lên kế_hoạch cho sự_nghiệp ca_hát của mình trong năm nay .
L . C_Theo_The_Sun

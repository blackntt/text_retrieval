﻿ Sass & amp ; Bide : Cô nàng ngổ_ngáo
Một_chút cảm_hứng từ siêu_mẫu Kate_Moss , nhãn_hiệu nổi_tiếng này đã cho thấy xu_hướng thời_trang chủ_đạo của mình là thiên về trang_phục dành cho người có dáng mảnh_khảnh . Đây là dáng người thích_hợp với chất_liệu vải jeans da .
Nhãn_hiệu nổi_tiếng này thực_sự khởi_động cho xu_hướng jeans da hứa_hẹn sẽ làm_nên đột_phá trong mùa này . Chất_liệu Jeans bó sát sẽ tiếp_tục tạo thành một cơn " sốt " khi mà những người_mẫu tự_tin sải bước trên trên sàn catwalk với những đôi giày gót nhọn , những đôi vớ cao màu đen bó sát .
Ngoài_ra , trong mẫu thời_trang này ta còn bắt_gặp một bộ sưu_tập theo phong_cách của lính hải_quân với điểm nổi_bật là sọc có vằn và những chiếc váy , áo hình chữ T . Các cô nàng ngổ_ngáo trong vai những tên cướp_biển thực_sự khuấy_động cho tuần_lễ thời_trang New_York .
N . B

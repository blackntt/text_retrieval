﻿ Cổ_tích mùa trăng rằm
Chú Cuội_Trần_Minh_Thảo trao quà cho trẻ_em kém may_mắn - Ảnh : Kim_Anh
TT - 1 . Mấy hôm rồi các anh_chị trong Hội_Thanh niên khuyết_tật TP.HCM tụ_họp lại , mướt_mồ_hôi luyện những bài hát với chủ_đề ánh trăng đêm rằm để biểu_diễn trước khán_giả là các em khuyết_tật , mồ_côi ở Trung_tâm Nuôi_dưỡng bảo_trợ trẻ_em Gò_Vấp . Chương_trình được trình_làng .
Ca_sĩ Phương_Dung chống đôi nạng di_chuyển từ sân_khấu xuống tận phía khán_giả nhí ngồi để cùng hát Ước_mơ xanh . Ngồi trên xe_lăn , đôi tay yếu_ớt , cậu_bé Nguyễn_Văn_Tâm vẫn cố gồng lên đung_đưa chiếc lồng đèn_hình ông mặt_trời với nụ_cười tươi_rói khi xem chú Cuội khuấy_động trên sân_khấu .
Đêm nơi đây chợt lắng lại khi tất_cả chia_sẻ những câu_chuyện vượt lên chính mình của các anh_chị khuyết_tật để hôm_nay ai cũng có nghề_nghiệp , lạc_quan yêu_đời ...
Như anh Quốc_Hưng , như chị Thanh_Thúy đã “ đứng ” trên đôi nạng gỗ để học_tập , hoạt_động xã_hội và nay cùng là đồng_nghiệp tại văn_phòng Handicap_International . Như_Liên dù đi_lại khó_khăn vẫn bước vững_chãi đến giảng_đường đại_học ; ca_sĩ Phương_Dung vốn là chủ một shop lưu_niệm , hoa tươi ; anh Lý_Long_Hưng làm kỹ_thuật_viên điện_tử ...
2 . “ Chú Cuội , chú Cuội đến chơi với tụi con nè ” - những tiếng í_ới réo gọi của đám trẻ làm chú Cuội hết ở chỗ này lại phải lăng_xăng qua chỗ khác . Như trong cổ_tích bước ra , chú Cuội mặc áo_bà_ba , đầu quấn khăn , mang bên mình một túi quà to_đùng phát cho từng em nhỏ có hoàn_cảnh kém may_mắn .
TP.HCM : Học_sinh góp hàng ngàn lồng đèn giúp bạn vui trung_thu
Ngay sau khai_giảng năm_học 2005-2006 , HS nhiều trường THPT , THCS , tiểu_học của 24 quận , huyện trên địa_bàn TP.HCM đã tham_gia đóng_góp lồng đèn cho thiếu_nhi vui trung_thu bằng nhiều hoạt_động : thi làm lồng đèn , quyên_góp lồng đèn cũ , mua lồng đèn tặng bạn ...
Đến nay , số lồng đèn do HS thành_phố đóng_góp trung_bình mỗi quận , huyện là hơn 3.000 cái . Được biết , số lồng đèn này dành cho thiếu_nhi có hoàn_cảnh khó_khăn , mái_ấm , nhà mở ... ở TP.HCM tham_dự “ Đêm hội trăng rằm ” năm nay của các quận , huyện .
HỒNG_VÂN

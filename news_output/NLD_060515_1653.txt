﻿ Wenger không quá lo_lắng về sự ra_đi của các trụ_cột
HLV có_thể sẽ mất đi một_số trụ_cột vào kỳ chuyển_nhượng mùa Hè này ( NLĐO ) - HLV Arsene_Wenger ( CLB Arsenal ) khẳng_định đang chuẩn_bị một_số hợp_đồng vào kỳ chuyển_nhượng mùa Hè này để thay_thế một_số cầu_thủ chủ_chốt sắp ra_đi .
Tuy_nhiên , mục_tiêu trước_mắt là chiếc Cúp_Champions_League vào giữa tuần này .
Trận_Chung kết Champions_League cũng có_thể là trận đấu cuối_cùng của Thierry_Henry , Ashley_Cole và Robert_Pires trong màu áo Arsenal .
HLV Wenger nói : “ Chúng_tôi phải chấp_nhận mọi chuyện và đối_mặt với sự ra_đi của họ . Điều quan_trọng là bây_giờ chúng_tôi sẽ cùng nhau đến Paris và mang Cúp về . Sau đó , chuyện gì đến sẽ đến ” .
Tiền_đạo Henry đang được khuyên đầu_quân cho chính đội bóng đối_thủ Arsenal trong trận chung_kết Champions_League là Barcelona . Trong khi đó , hậu_vệ Cole đã được Real_Madrid mời_gọi .
Tiền_vệ Pires sẽ đưa ra quyết_định ra_đi hay không sau khi kết_thúc trận Chung_kết tại Paris . Hiện_tại CLB Arsenal chỉ đề_nghị gia_hạn hợp_đồng thêm 1 năm với tiền_vệ này . “ Chuyện gì sẽ đến trong một hoặc hai tháng tới không quan_trọng bằng những gì chúng_tôi cùng nhau thể_hiện được trong_suốt mùa bóng và trong trận Chung_kết Champion_Leagues ” , HLV Wenger nói thêm .
Trần_Nam

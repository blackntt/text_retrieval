﻿ Anh : 1/10 học_sinh vào “ web người_lớn ”
Khoảng 12% học_sinh trong độ tuổi từ 13 - 18 khi được Tổ_chức Nghiên_cứu giáo_dục quốc_gia hỏi đã trả_lời rằng đây là một trong những lý_do chính để họ sử_dụng Internet .
Thêm vào đó , bài_tập về nhà cũng là một nguyên_nhân phổ_biến nhất cho việc sử_dụng Internet , với hơn 30 học_sinh thì_có ba người viện lý_do như_vậy . Những điều phát_hiện trên đang là một phần trong cuộc nghiên_cứu dài tám năm ở 6.400 học_sinh tại Anh .
Tổ_chức NFER tiến_hành cuộc nghiên_cứu này với tư_cách là đại_diện của Bộ Giáo dục và kỹ_năng , đã thấy rằng 52% học_sinh sử_dụng Internet cho việc chat ( tán_gẫu ) và 36% dùng để mua_sắm , đặt các dịch_vụ hoặc hàng_hóa trên mạng . Khoảng 18% đọc tin_tức và các vấn_đề thời_sự , 9% tham_gia thảo_luận ở các diễn_đàn .
Cuộc nghiên_cứu cũng cho thấy với học_sinh , truyền_hình cũng được xem là hình_thức được tin_cậy , 48% tin hoàn_toàn vào truyền_hình hoặc nhiều hơn . Điều đáng suy_nghĩ hơn : 13% học_sinh đã bỏ tiền mua các tờ tạp_chí không thích_hợp với lứa tuổi mình .
QUỐC_DŨNG ( Theo BBC )

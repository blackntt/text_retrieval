﻿ Đã đóng tiền vẫn bị cúp nước
Theo nhiều người_dân ở đây , tiền nước hằng tháng đều đóng cho ban quản_lý chung_cư ( hầu_hết hộ dân đã đóng tiền nước tháng mười , mười_một và chưa nhận được hóa_đơn tiền nước tháng mười_hai ) . Tuy_nhiên , không hiểu vì_sao đã ba_tháng nay ban quản_lý vẫn chưa thanh_toán với Chi_nhánh cấp_nước Thủ_Đức ! Một người_dân cho_biết “ không liên_lạc được ” với trưởng_ban quản_lý chung_cư . Quá bức_xúc , hôm_qua 15-1 , người_dân đã tự phá niêm chì và mở nước trở_lại .
Cùng ngày , ông Nguyễn_Xuân_Cầu - giám_đốc Chi_nhánh cấp_nước Thủ_Đức - cho_biết hiện ban quản_lý chung_cư đã thanh_toán hóa_đơn kỳ 10 .
QUANG_KHẢI

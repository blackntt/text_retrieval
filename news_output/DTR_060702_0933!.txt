﻿ Phát_hiện vòng_hoa cổ trong mộ Ai_Cập
Các nhà khảo_cổ tại Ai_Cập đang hy_vọng tìm thấy xác ướp khi khai_quật một căn phòng mộ tại Luxor thì thay vào đó phát_hiện một vòng_hoa lớn .
Vòng_hoa 3.000 tuổi này là cái đầu_tiên được phát_hiện từ trước tới nay . Nó được tìm thấy trong quan_tài cuối_cùng trong số 7 cái mà các nhà khảo_cổ hy_vọng tìm thấy xác ướp của một bà hoàng hay mẹ của Tutankhamun .
Giám_đốc Bảo_tàng Ai_Cập tại Cairo_Nadia_Lokma cho_biết phát_hiện này thậm_chí còn tốt hơn cả việc tìm thấy xác ướp .
" Vòng_hoa này thật_là hiếm , chưa từng có cái nào như_thế trong bảo_tàng . Chúng_tôi đã từng nhìn thấy trong tranh vẽ , nhưng đây là lần đầu_tiên thấy tận_mắt " - Nadia_Lokma nói .
Các chuyên_gia cho_biết hoàng_gia Ai_Cập cổ thường đeo những vòng_hoa gắn các sợi vàng trên vai kể_cả khi sống và khi chết .
Đây là ngôi mộ thứ 63 được phát_hiện kể từ khi thung_lũng được lập bản_đồ đầu_tiên vào thế_kỷ 18 và chỉ cách mộ của Tutankhamun 5 m . Việc phát_hiện ra_ngôi mộ đã đảo_lộn quan_niệm thông_thường rằng không còn ngôi mộ nào để tìm_kiếm tại Thung_lũng các ông_hoàng .
Theo M . T .
Khoa_Học

﻿ Hôm_nay 8-10 , áp_thấp_nhiệt_đới tiếp_tục gây mưa to
Chiều_tối qua , sau khi đi vào sát vùng bờ biển Thừa_Thiên - Huế , Đà_Nẵng , áp_thấp_nhiệt_đới đã yếu đi một_ít , sau đó đổ_bộ vào địa_phận Quảng_Nam - Đà_Nẵng , sức gió mạnh nhất ở vùng gần tâm áp_thấp_nhiệt_đới mạnh cấp 6 ( 39-49km/giờ ) , giật trên cấp 6 .
Dự_báo hôm_nay áp_thấp_nhiệt_đới di_chuyển theo hướng tây tây nam , trên địa_phận Quảng_Nam - Đà_Nẵng rồi suy_yếu dần . Ngoài_ra , chiều và tối_qua không_khí lạnh đã ảnh_hưởng đến hầu_hết các tỉnh thuộc Bắc_bộ và Bắc_Trung bộ .
Do ảnh_hưởng của áp_thấp_nhiệt_đới kết_hợp với không_khí lạnh , hôm_nay ở vịnh Bắc_bộ và vùng_biển ngoài khơi Trung bộ có gió mạnh cấp 6 , cấp 7 , giật trên cấp 7 . Biển động mạnh . Các tỉnh từ Nghệ_An đến Phú_Yên có mưa vừa đến mưa to , riêng các tỉnh từ Hà_Tĩnh đến Quảng_Ngãi có mưa to đến rất to . Dự_kiến tổng_lượng mưa ở các tỉnh thuộc khu_vực này có_thể lên đến 200mm . Cần đề_phòng lũ cao , lũ_quét và sạt_lở đất ở vùng núi .
PV

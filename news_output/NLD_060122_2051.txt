﻿ 1,7 tỉ đồng làm công_tác xã_hội
Bà_con nghèo quận 4 - TPHCM tiếp_nhận quà Tết của Công_ty Cổ_phần Dầu thực_vật Tân_Bình ( NLĐ ) - Sáng 22-1 , Hội_Chữ thập đỏ , Phòng LĐ - TB-XH quận 4 - TPHCM và Hội_Tâm thiện trao 353 phần quà Tết , mỗi phần trị_giá 120.000 đồng cho bà_con lao_động nghèo , gia_đình neo_đơn .
Dịp này , Phòng LĐ - TB-XH quận 4 đã trao 53 chiếc xe_lăn ( 1,7 triệu đồng / chiếc ) của Hội_Bảo trợ Bệnh_nhân nghèo TPHCM cho các trường_hợp đi_lại khó_khăn . Trước đó , ngày 21-1 , Hội_Chữ thập đỏ quận 4 phối_hợp Công_ty Cổ_phần dầu thực_vật Tân_Bình trao 300 phần quà , mỗi phần trị_giá 120.000 đồng cho người nghèo và khuyết_tật trong quận . Năm 2005 , Hội_Chữ thập đỏ quận 4 vận_động được hơn 1,7 tỉ đồng làm công_tác xã_hội .
Tin - ảnh : L . Minh

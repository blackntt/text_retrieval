﻿ Phạm_Văn_Mách đoạt HCV
Mặc_dù từng đoạt HCV thế_giới , nhưng chưa bao_giờ Mách đăng_quang ở giải vô_địch châu Á .
Chiến_thắng này đã giúp bộ sưu_tập HCV của anh thật_sự hoàn_chỉnh , khi có đầy_đủ từ thấp nhất_là giải vô_địch Đông_Nam Á đến châu Á và thế_giới .
Ở giải vô_địch châu Á lần 39 này , Mách đã giành chiến_thắng sau khi vượt qua Val_Pedrian_Lim ( Singapore ) và Som_Khit ( Thái_Lan ) .
Trong khi đó , lực_sĩ từng sáu lần vô_địch tại giải này là Lý_Đức ( 80kg ) đã thi_đấu không thành_công khi chỉ giành HCĐ .
N . K .

﻿ Tuyệt_đỉnh Công_phu đoạt giải phim hay nhất châu Á
Với vai_diễn trong Tuyệt_đỉnh công_phu , Châu_Tinh_Trì đã đoạt giải diễn_xuất điện_ảnh xuất_sắc nhất . Giải_Nghệ sĩ mới xuất_sắc thuộc về Lynn_Chen với vai_diễn trong phim Saving_Face .
Hai ngôi_sao trong show truyền_hình nổi_tiếng Lost - Daniel_Dae_Kim và Yunjin_Kim - được trao giải gương_mặt truyền_hình nam và nữ nổi_bật nhất .
Đây là giải_thưởng tôn_vinh những thành_tựu trong lĩnh_vực nghệ_thuật , giải_trí và những đóng_góp cá_nhân cho hình_ảnh tiến_bộ của cộng_đồng người Mỹ gốc châu Á .
Một_số giải_thưởng đặc_biệt khác : giải Inspiration cho nhà_nghiên_cứu bệnh AIDS Dr . David_Ho ; giải_thưởng Building_Bridges cho đạo_diễn , tác_giả Quentin_Tarantino ; giải Visibility cho nữ diễn_viên Lucy_Liu ; và giải_thưởng Thành_tựu trọn đời được trao cho nhà_sản_xuất quá_cố Ismail_Merchant - người ghi dấu_ấn trên các phim nh ư Howards_End and The_Remains of the Day và đã qua_đời hồi tháng 5 .
Buổi lễ trao giải sẽ được phát trên kênh_truyền_hình AZN vào ngày 29-1 .
L.TH . ( Theo AP )

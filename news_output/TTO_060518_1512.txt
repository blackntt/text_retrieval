﻿ ' ' Marathon ' ' vận_động Quốc_hội Mỹ thông_qua PNTR
Bà Tôn_Nữ_Thị_Ninh cho_biết :
- Nếu muốn vào WTO trong năm nay thì từ đây đến hết tháng 7 phải vận_động Quốc_hội Mỹ thông_qua PNTR . Vì sau tháng 7 , Quốc_hội Mỹ nghỉ_hè và kể từ tháng 9 chắc chắc sẽ rất bận_rộn chuẩn_bị bầu_cử . Cho_nên thời_gian rất eo_hẹp .
Những thuận_lợi và khó_khăn của việc vận_động này là gì , thưa bà ?
- Thuận_lợi là tình_hình chung về quan_hệ Việt_Mỹ là tích_cực , phát_triển theo hướng ổn_định và tăng_cường . Năm_ngoái có chuyến đi của Thủ_tướng Phan_Văn_Khải và năm nay Chủ_tịch Hạ_viện Mỹ_Dennis_Hastert vừa sang thăm VN .
Theo_dõi bình_luận của Chủ_tịch Hạ_viện Mỹ và thành_viên trong đoàn , thái_độ chung là có thiện_ý . Đương_nhiên vấn_đề PNTR phải đem ra Quốc_hội Mỹ bỏ_phiếu và chúng_ta phải làm thế_nào đó được đa_số .
Thuận_lợi thứ_hai là Tổng_thống Bush sẽ sang thăm VN nhân Hội_nghị cấp cao APEC và thăm song_phương . Nói_chung phía Mỹ cũng đặt mục_tiêu là đạt được_một_cái gì đó tích_cực về song_phương trước chuyến thăm .
Mặc_dù hành_pháp độc_lập với lập_pháp nhưng trong chừng_mực nào_đó , Chính_phủ Mỹ cũng có sự hỗ_trợ . Trong_Quốc hội , người Đảng cộng_hoà của Tổng_thống Bush chắc là xu_hướng thuận_lợi .
Vậy còn những khó_khăn ?
- Truyền_thống của Đảng cộng_hoà thiên về tự_do thương_mại hơn là Đảng dân_chủ . Đảng dân_chủ thiên về bảo_trợ cho thị_trường trong nước . Khách_quan mà nói , khó_khăn thứ nhất_là phải chuẩn_bị quan_tâm các nghị_sĩ Đảng dân_chủ . Dù về chính_trị chung họ ủng_hộ quan_hệ với VN nhưng riêng về thương_mại không đơn_giản .
Khó_khăn thứ_hai , Quốc_hội Mỹ khác với bên chính_quyền . Cử_tri vận_động các nghị sỹ của mình lên_tiếng , bảo_vệ quyền_lợi của địa_phương mình hoặc công_ty của mình . Giả_định ngành dệt_may cho là Chính_phủ Mỹ trong thương_lượng đã ' ' hơi nhân_nhượng ' ' VN thì có_thể họ có bước cuối_cùng là lobby nghị_sĩ của họ để đưa vấn_đề này ra chất_vấn , thảo_luận tại Quốc_hội . Cho_nên phải nhờ bạn_bè Mỹ đánh_giá sắp tới chỗ nào sẽ khó_khăn để chuẩn_bị tinh_thần vận_động .
Còn một khó_khăn nữa_là một_số nghị_sĩ công_kích mình về dân_chủ , nhân_quyền . Trong số đó , có người nói chúng_ta về nhân_quyền nhưng có_thể chấp_nhận cho mình vào WTO . Nhưng cũng có một_số nghị_sĩ , đặc_biệt bị kích_động bởi thiểu_số cực_đoan trong cộng_đồng người Việt , không loại_trừ , tìm cách gây sức_ép . Họ có_thể đưa nội_dung liên_quan đến nhân_quyền làm điều_kiện trong văn_bản thoả_thuận .
Với thời_gian hơn 2 tháng , chúng_ta sẽ vận_động như_thế_nào ?
- Chắc_chắn Quốc_hội và Chính_phủ mình sẽ có những vận_động . Cụ_thể thế_nào tôi chưa_thể nói được . Tôi cũng sắp_sửa đi Mỹ đây .
Bà có tin rằng chúng_ta thành_công trong thời_gian ngắn này ?
Bà Tôn_Nữ_Thị_Ninh , Phó_Chủ nhiệm Ủy_ban Đối_ngoại của Quốc_hội : ' ' Khung luật cơ_bản đã đáp_ứng WTO ' '
Đến thời_điểm hiện_nay , công_tác xây_dựng luật của Quốc_hội đã đáp_ứng được yêu_cầu gia_nhập WTO ?
- Tôi đi cùng Phó_Chủ tịch Quốc_hội Nguyễn_Văn_Yểu sang thăm Mỹ hồi đầu năm thì ai cũng cho rằng Quốc_hội VN làm_luật hết_sức tích_cực . Khung luật cơ_bản đáp_ứng WTO năm_ngoái đã làm xong . Đương_nhiên sắp tới có một_số luật nữa ( như Luật chứng_khoán ) thì chỉ bổ_sung thêm . Vấn_đề duy_nhất là triển_khai kịp_thời , có văn_bản hướng_dẫn nếu cần .
Cái khó của VN không hẳn ở phần làm_luật mà ở triển_khai những biện_pháp thực_hiện . Trong thực_hiện đôi_khi không đồng_bộ , Trung_ương hiểu thế_này , địa_phương hiểu thế khác . Cho_nên thông_thường tiếp_xúc với các đối_tượng nước_ngoài , kể_cả Mỹ , họ phân_vân về phần thực_thi hơn phần xây_dựng luật . Phần xây_dựng luật nói_chung họ khen ! - Thành_công nếu chúng_ta phối_hợp với bạn_bè Mỹ trong có doanh_nghiệp , luật_sư và nhà lobby một_cách tổng_hợp và vận_động đúng chỗ . Vì có hơn 400 hạ_nghị_sĩ Mỹ và 100 thượng_nghị_sĩ .
Vấn_đề nằm chủ_yếu ở hạ_viện , thượng viên sẽ thuận_lợi hơn . Nếu mình biết , khẩn_trương làm đúng tôi không loại_trừ có_thể vận_động thành_công trong thời_gian ngắn . Nhưng tôi không nói ' ' đóng đinh ' ' 100% là sẽ được !
Trong việc này làm_sao phải kết_hợp chặt_chẽ giữa Chính_phủ và Quốc_hội . Chính_phủ thông_qua Đại_sứ_quán tại Mỹ vận_động , đồng_thời Quốc_hội góp sức thêm .
Chúng_ta có phương_án kết_thúc vòng đa_phương , làm thủ_tục gia_nhập WTO , sau đó mới quay lại làm nốt phần về PNTR ?
- PNTR chỉ tác_động đến hiệu_lực thành_viên WTO của VN trong quan_hệ VN với Mỹ . Như tôi đã nói , việc Quốc_hội Mỹ thông_qua PNTR sớm là có_thể chứ không phải 100% . Lỡ rơi vào mấy % không chắc_chắn đó thì VN phải cố hoàn_tất phần đa_phương tại Geneve và với Ban thư_ký của WTO .
Nếu_Ban thư_ký WTO , đứng đầu là Tổng_giám_đốc Pascal_Lamy , sẵn_sàng khẩn_trương trong 3 tháng tới làm xong phần đa_phương và xuất_hiện cơ_hội từ đây cho_đến tháng 11 thì có_thể kết_nạp VN vào WTO . Nhưng thực_chất tư_thế thành_viên WTO chỉ áp_dụng với các nước thành_viên WO , trừ Mỹ .
Còn phương_án ưu_tiên hiện_nay là ' ' maratong ' ' các mặt để cho cả phần song_phương ( gồm PNTR ) và đa_phương đều xong trước tháng 11 .
Với diễn_biến thuận_lợi , WTO có_thể gia_nhập WTO trước tháng 11 ?
- Có_thể được . Đương_nhiên đòi_hỏi chúng_ta khẩn_trương và ứng_xử một_cách phù_hợp ở các diễn_đàn khác_nhau .
Theo Viet NamNet

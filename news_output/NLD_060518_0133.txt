﻿ Hai vụ cướp xe táo_bạo
( NLĐ ) – Rạng sáng 17-5 , một vụ cướp xe_gắn_máy bằng súng lại xảy_ra tại TPHCM . Tại đường Trịnh_Đình_Thảo , phường Hòa_Thạnh , quận Tân_Phú , anh N . H . N ( SN 1984 , ngụ quận 10 ) đang chạy xe_gắn_máy chở bạn_gái bị 4 thanh_niên đi xe_gắn_máy dùng súng khống_chế cướp xe và 2 điện_thoại_di_động .
Công_an quận Tân_Phú đang truy_bắt các đối_tượng trên . Trước đó , rạng sáng 16-5 , tại phường 10 , quận Gò_Vấp , anh Trần_Trọng_Quyền ( SN 1971 , ngụ Kiên_Giang , hành_nghề chạy Honda ôm ) đang chở khách bị 2 thanh_niên khống_chế , dùng dao chém vào đầu và cướp xe .
N.Lâm

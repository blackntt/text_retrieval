﻿ Triệu_tập thêm 4 cựu cầu_thủ CSG
Sau khi bắt tạm giam Trương_Tấn_Hải về tội “ nhận hối_lộ ” , Cục CSĐT tội_phạm về TTXH ( Bộ Công an ) cho_biết , Cơ_quan công_an đã triệu_tập thêm 4 cầu_thủ CSG trong mùa bóng 2000-2001 . Đó là các cầu_thủ Huỳnh_Hồng_Sơn , Hồ_Văn_Lợi , Lương_Trung_Tuấn và Nguyễn_Minh_Phương .
Được biết , các cầu_thủ này sẽ phải trả_lời trước cơ_quan điều_tra về vụ HLV phó CLB_SLNA Nguyễn_Hữu_Thắng mang tiền bồi_dưỡng đội CSG trước trận đấu cuối của mùa giải 2000-2001vào ngày 27/5/2001 để CSG thắng Nam_Định ( NĐ ) , giúp SLNA vô_địch .
Huỳnh_Hồng_Sơn hiện đang được CSG cho CLB Bóng_đá TPHCM mượn , Lương_Trung_Tuấn đá cho Bình_Định , Minh_Phương thi_đấu ở GĐTLA và chỉ một_mình Hồ_Văn_Lợi còn bám_trụ đội bóng cảng này .
Vì_sao là Trương_Tấn_Hải ?
Việc_Cơ quan Điều_tra ra quyết_định khởi_tố và bắt tạm giam Trương_Tấn_Hải , cựu tiền_đạo CSG là điều khá bất_ngờ trong giới bóng_đá , vì theo nhiều người am_tường thì trong_suốt thời_gian khoác_áo CSG , vai_trò của Trương_Tấn_Hải ở CLB này khá lu_mờ , luôn bị xếp vào vị_trí dự_bị trong các trận đấu ở mùa giải 1997 cho_đến 2001 .
Trong sinh_hoạt cá_nhân , Trương_Tấn_Hải là người thường_xuyên bị lãnh_đạo CLB lúc ấy điểm mặt khá kỹ , bởi cầu_thủ này thường_xuyên tìm vận rủi may trong những ổ cá_cược bóng_đá ngoài xã_hội . Do_vậy , việc Hữu_Thắng tìm đến và đưa tiền “ bồi_dưỡng ” cho Trương_Tấn_Hải cũng là điều khó hiểu .
Nhiều người cho rằng , có_lẽ xuất_phát từ mối quan_hệ giữa Hữu_Thắng và Tấn_Hải trong thời_gian cùng tham_gia đội_tuyển quốc_gia năm 1999 và điều quan_trọng hơn là do biết được Trương_Tấn_Hải là người thích chuyện “ đỏ_đen ” , nên Hữu_Thắng móc_nối , dàn_xếp cho SLNA đoạt chức vô_địch .
Ai là người kế_tiếp ?
Sự quanh_co của Trương_Tấn_Hải trước Cơ_quan Điều_tra xem_ra khá hợp_lý , bởi_vì ở trận CSG thắng NĐ , cựu tiền_đạo này không có tên trong danh_sách thi_đấu vì chấn_thương .
Do_vậy , để thực_hiện “ phi_vụ ” này , không đơn_giản chỉ có Trương_Tấn_Hải là người có_thể hưởng trọn 100 triệu đồng từ tay Nguyễn_Hữu_Thắng để quyết_định mọi việc , mà số tiền này có_thể còn được chia cho một_số người khác trong đội CSG .
Trao_đổi với phóng_viên , bà Nguyễn_Xuân_Thái , nguyên Trưởng_Đoàn_Bóng đá CSG mùa giải 2000-2001 , cho_biết : “ Diễn_biến ở lượt trận cuối mùa giải 2000-2001 rất phức_tạp , trong đó yêu_cầu cao nhất của CSG là buộc phải thắng NĐ mới an_toàn trụ hạng ” .
Bà Thái nói tiếp : “ Từ khi có nghi_vấn về trận đấu CSG - NĐ , hơn 1 tháng qua tôi có liên_lạc với một_số thành_viên trong đội để tìm_hiểu thực_hư , thậm_chí còn khuyên họ , ai lỡ cầm tiền_của SLNA thì sớm thành_thật khai_báo . Nhưng tất_cả đều khẳng_định hoàn_toàn không biết ” .
Tiếp sau Trương_Tấn_Hải và 4 cầu_thủ trên , có nguồn tin cho_biết sắp tới sẽ có thêm một_số thành_viên Ban Huấn luyện CLB_CSG phải làm_việc với Cơ_quan Điều_tra . Tại thời_điểm diễn ra trận đấu CSG gặp NĐ , ông Phạm_Huỳnh_Tam_Lang là HLV trưởng ; Đặng_Trần_Chỉnh là HLV phó .
Tuy_nhiên , dù mang chức_danh HLV trưởng nhưng do phải tập_trung cùng đội_tuyển quốc_gia liên_tục , nên phần việc chuyên_môn ở đội CSG , ông Tam_Lang gần_như giao cho Đặng_Trần_Chỉnh trực_tiếp điều_hành .
Cũng liên_quan đến vụ_án chạy chức vô_địch của SLNA , có nguồn tin cho_biết , một_vài cầu_thủ NĐ đã nhận tiền_của Hữu_Thắng để “ nằm ” trong trận đấu với CSG .
Theo T . Khiêm - A.Phương-V.Quang Người lao_động

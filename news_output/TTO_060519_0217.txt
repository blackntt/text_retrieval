﻿ TP.HCM : 87 GV dự Hội_giảng GV nghề
Đặc_biệt , hội thi lần này có sự tham_gia của GV ở 11 cơ_sở dạy nghề ngoài công_lập và doanh_nghiệp có tổ_chức dạy nghề , một trung_tâm dịch_vụ việc_làm và Trung_tâm Giáo_dục dạy nghề thanh_thiếu_niên thành_phố .
Đáng lưu_ý là nhiều nữ GV tham_dự hội thi có tuổi_đời chưa quá 25 và nghề công_nghệ_thông_tin có nhiều GV dự thi nhất với 18 bài giảng .
Được biết , hội thi lần này sẽ chọn 16 GV giỏi nhất tham_dự Hội_giảng GV dạy nghề toàn_quốc dự_kiến sẽ được tổ_chức từ ngày 17 đến 23-7-2006 tại TP Vinh , Nghệ_An .
Hà_Nội : HS được thay_đổi nguyện_vọng xét tuyển lớp 10
Ngày 18-5 là hạn chót để HS lớp 9 đăng_ký xét tuyển vào lớp 10 tại các trường THPT công_lập . Tuy_nhiên sau thời_hạn này , HS vẫn còn cơ_hội chuyển_đổi nguyện_vọng đã đăng_ký - Sở GD - ĐT Hà_Nội cho_biết .
Theo Sở GD - ĐT , ngày 29-5 sở sẽ công_bố công_khai số_lượng HS đăng_ký dự_tuyển vào từng trường THPT công_lập . Những HS muốn xin thay nguyện_vọng dự_tuyển sẽ nộp đơn ( theo mẫu chung ) tại phòng GD - ĐT ngày 30-5 và tại Sở GD - ĐT ngày 31-5 .
THANH_HÀ - PHÚC_ĐIỀN

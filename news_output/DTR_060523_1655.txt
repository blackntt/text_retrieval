﻿ Nistelrooy : “ Tốt nhất tôi nên ra_đi ”
Tiền_đạo Ruud_Van_Nistelrooy cho rằng tốt nhất anh nên rời khỏi MU trong mùa_hè này sau những bất_đồng sâu_sắc với ông thầy Alex_Ferguson .
Trong cuộc phỏng_vấn trên kênh thể_thao của BBC , cầu_thủ người Hà_Lan này cho rằng : “ Tôi sẽ ra_đi hay ở lại MU ? Không ai biết chính_xác điều gì sẽ xảy_ra nhưng có_lẽ tốt hơn cả tôi nên ra_đi ” .
Dù_vậy tiền_đạo từng ghi 150 bàn trong 200 lần khoác_áo cho Quỷ đỏ MU thừa_nhận sẽ không đưa ra bất_kỳ quyết_định nào cho tới khi VCK World_Cup kết_thúc . Anh nói : “ Tôi sẽ cân_nhắc lời mời của bất_kỳ CLB nào thực_sự quan_tâm tới tôi . Nhưng điều đó chỉ có_thể diễn ra sau khi kết_thúc World_Cup vào tháng Bảy tới .
Tôi và người đại_diện hiểu rõ tốt nhất vào thời_điểm hiện_nay là chỉ tập_trung vào ĐTQG và không nên để những vụ_việc chuyển_nhượng ảnh_hưởng tới phong_độ thi_đấu ” .
Cầu_thủ này cũng cho rằng : “ Tôi biết nhiều người_ở Manchester không hiểu vấn_đề giữa tôi và MU . Tôi yêu đội bóng này và thực_sự tôi cũng không hiểu nổi mọi chuyện với MU là như_thế_nào nữa ” .
Van_Nistelrooy : Tạm quên đi những rắc_rối ở CLB để tập_trung vào ĐTQG và World_Cup ( AFP )
Van_Nistelrooy và Sir_Ferguson đã không nói_chuyện với nhau một lời nào kể từ khi tiền_đạo này không được HLV Alex gọi tham_dự trận đấu chia_tay với Roy_Keane , một trận đấu mà theo Ruud , anh đã rất háo_hức được tham_gia bởi tình_cảm quý_mến giành cho người đội_trưởng cũ của mình .
Hiện những đội bóng nước_ngoài như Inter , AC Milan hay các đội trong nước như Chelsea , Tottenham , Newcastle đang “ ngấm_ngầm ” cuộc chạy_đua săn_tìm chữ_ký của cầu_thủ này .
Dù_vậy trung_phong với 47 bàn ghi được tại Champions_League trong sự_nghiệp cho rằng : “ Nếu mọi thứ ổn_định có_lẽ tôi sẽ tiếp_tục chơi cho MU mùa giải tới . Tuy_nhiên tôi không phải là người không hiểu tình_thế hiện_nay .
Không được gọi tham_gia trận đấu cuối tại Premiership và trận đấu kỷ_niệm với Roy_Keane rõ_ràng không phải tín_hiệu tốt_lành gì .
Nhưng có_lẽ , vội_vàng vào thời_điểm hiện_nay cũng không_thể giải_quyết được vấn_đề . Hãy chờ cho_đến khi World_Cup kết_thúc và mọi thứ sẽ được làm sáng_tỏ ” .
Vũ_Phong

﻿ Xe_máy rúc gầm xe rác , 1 người chết
Do thiếu quan_sát , xe_máy mang BKS 43H2-7776 đã đâm vào đuôi xe ôtô chở rác đang dừng tránh tàu_hỏa . Chủ xe_máy chết ngay tại_chỗ .
Vụ tai_nạn xảy_ra lúc 9h10 ngày 14/3 tại ngã ba Huế ( TP Ðà_Nẵng ) , tại nơi giao nhau giữa QL 1A và tuyến đường_sắt Bắc - Nam với đường Ðiện_Biên_Phủ và Tôn_Ðức_Thắng .
Lúc này xe ôtô chở rác mang biển_số 43H-0056 của Công_ty môi_trường Ðô thị Ðà_Nẵng , do Bùi_Nhơn điều_khiển , đang dừng lại chờ tránh đoàn tàu_lửa chạy ngang qua . Ông Nguyễn_Lương ( 1964 , trú An_Hải_Bắc , quận Sơn_Trà , Ðà_Nẵng ) điều_khiển mô_tô chạy cùng chiều nhưng lại thiếu quan_sát dẫn đến tự gây tai_nạn . Hậu_quả là ông Lương đã chết ngay tại_chỗ . Vụ tai_nạn giao_thông đã làm kẹt xe gần 30 phút .
Lâu_nay , khu_vực ngã ba Huế là điểm_nóng về tình_trạng xe dù , bến có và cũng là nơi thường_xuyên xảy_ra TNGT dẫn đến chết người_ở Đà_Nẵng . Theo Hải_Châu_Tiền_Phong

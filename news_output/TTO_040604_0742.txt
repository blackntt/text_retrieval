﻿ Mất xe được đăng_ký xe mới
Ngày 29-4-2004 , Công_an TP trả_lời như sau : đối_với trường_hợp xe môtô đã đăng_ký và bị mất_cắp hoặc chuyển_nhượng nhưng chưa làm thủ_tục sang_tên , đến nay chưa xác_định được người mua để làm thủ_tục theo qui_định thì sẽ được đăng_ký xe mới ( công_văn 514 của Tổng_cục Cảnh_sát ) .
Công_an TP đã chỉ_đạo công_an quận , huyện , Phòng_Cảnh sát giao_thông đường_bộ niêm_yết công_văn này tại văn_phòng công_an phường , xã , nơi nhận hồ_sơ đăng_ký xe , giao trách_nhiệm trưởng công_an phường , xã tiếp_nhận đơn của nhân_dân , phân_công cảnh_sát khu_vực xác_minh thực_tế không có xe sử_dụng . Sau đó các đội đăng_ký xe tiếp_nhận hồ_sơ và đăng_ký theo đúng qui_định .
NGUYỄN_TRUNG_THÔNG ( phó ban chuyên_trách )

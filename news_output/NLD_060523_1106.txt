﻿ ' ' Mốt ' ' ngoại_tình
Trong cuộc_sống hiện_đại , nhiều người đã không ngần_ngại đem hạnh_phúc gia_đình ra đánh_đổi chỉ vì muốn chứng_tỏ sức quyến_rũ của mình .
Chồng làm giám_đốc , hai đứa con du_học ở Úc nên chị Lam khá dư_dả về thời_gian lẫn tiền_bạc . Chị tham_gia đủ các lớp thẩm_mỹ , tennis , khiêu_vũ ...
Khi gia_nhập câu_lạc_bộ khiêu_vũ , chị Lam thấy không ít người chọn cho mình một anh " kép ruột " để cùng dập_dìu theo điệu nhảy . Họ đều là những người đã có gia_đình . Việc cặp_kè với những tay kép trẻ này chẳng_qua là một_cách phô_trương " thành_tích " của họ .
Ngoại_tình cho giống người_ta
Chị Lam nhìn lại mình . Chị còn trẻ_trung và hấp_dẫn hơn khối cô ở đấy . Chẳng lý_do nào chị lại thua_kém họ . Thế_là kế_hoạch " săn kép " được vạch ra , với sự giúp_đỡ của mấy cô giàu kinh_nghiệm . Sau 6 tuần , bộ sưu_tập của chị cũng ngót_nghét năm ... " em giai " .
Dù quy_ước không được đến nhà nhưng xem_ra khó ngăn được sự tò_mò của các gã trai hám của . Những vụ ghen_tuông xảy_ra khi các người_tình của chị đụng_độ nhau .
Tin_tức về bà_mẹ thích " gặm cỏ non " cũng bay sang tận Úc . Hậu_quả , chồng chị bỏ đi_sau khi đặt lên bàn lá đơn đã ký sẵn .
Người đàn_ông " sơ cua "
Chị Linh , 29 tuổi có một người chồng thương_yêu chị hết_mực . Anh chỉ có mỗi_một tội : Thường_xuyên đi công_tác xa nhà .
Cảm_giác mỏi_mòn chờ_đợi chồng khiến chị thấy mình như bị bỏ_rơi . Nhìn những phụ_nữ khác có " bóng tùng " che_chở , chị cũng muốn tìm ai đó có_thể giúp mình khoả_lấp nỗi cô_đơn .
Chớ trêu thay , người lấp chỗ trống chẳng ai xa_lạ . Anh_ta chính là bạn_thân thời_đại học với chị và chồng . Dù biết đang " cưỡi_trên_lưng_cọp " nhưng mỗi khi ở bên tình_nhân , chị lại trở_lại thời xuân_sắc .
Những thay_đổi của chị không qua được mắt chồng . Anh nhận thấy cử_chỉ lấm_lét , những cú điện_thoại , tin nhắn của vợ rất đáng ngờ . Anh bắt_đầu theo_dõi và đột_ngột trở_về nhà khi kỳ công_tác chưa kết_thúc .
Cảnh_tượng đập vào mắt làm anh choáng_váng : Trên giường , vợ và gã bạn_thân đang quấn lấy nhau .
Tình và tiền
Chưa bao_giờ chị Hậu có ý_nghĩ nghi_ngờ chồng , dù anh thường_xuyên vắng nhà . Đến khi có người mách rằng anh đang cặp với một bà khách quen , chị mới ngã_ngửa .
Hoá_ra , bấy_lâu_nay anh bám theo những qúy bà để " kiếm thêm " tình lẫn tiền , như một_số đồng_nghiệp vẫn làm .
Việc ngoại_tình ngỡ chỉ là trò vô_thưởng_vô_phạt . Nào_ngờ , những cuộc rong chơi bên ngoài đấy lại đem sóng_gió vào tổ_ấm .
Cây kim giấu kín trong bọc lâu ngày cũng có ngày lộ ra , huống_chi việc thường_xuyên đi_ngang_về_tắt . Ngoài tình vì bất_kỳ lý_do gì cũng khó có_thể chấp_nhận . Ngoại_tình vi đua_đòi , thích thể_hiện bản_thân lại càng khó tha_thứ hơn . Sau mỗi cuộc vui , những kẻ thiêu_thân kia còn lại gì cho mình ?
Theo Tiếp_Thị & amp ; Gia_Đình

﻿ Thành_lập sáu đoàn giám_sát kỳ thi thử trắc_nghiệm ngoại_ngữ
Bộ GD - ĐT đã quyết_định thành_lập sáu đoàn giám_sát kỳ thi thử được tổ_chức theo đúng quy_trình thi tốt_nghiệp THPT “ thật ” này .
Sáu đoàn giám_sát của bộ bao_gồm các chuyên_viên của Cục Khảo thí và Kiểm_định chất_lượng giáo_dục , Vụ_Giáo dục trung_học , Vụ_Đại học và Sau_đại_học , Thanh_tra Giáo_dục sẽ có_mặt tại 12 tỉnh_thành Hà_Nội - Hà_Tây , Nghệ_An - Hà_Tĩnh , Đà_Nẵng - Quảng_Nam , Khánh_Hòa - Ninh_Thuận , TP.HCM - Bình_Dương và Cần_Thơ - Hậu_Giang để giám_sát tất_cả các khâu chuẩn_bị , tổ_chức thi .
Các đoàn giám_sát có một nhiệm_vụ quan_trọng là theo_dõi chặt_chẽ quy_trình thực_hiện kỳ thi của các địa_phương , công_việc của chủ_tịch hội_đồng coi thi , giám_thị và thí_sinh theo tài_liệu hướng_dẫn của Bộ GD - ĐT đã ban_hành để có đánh_giá nhận_xét phục_vụ cho việc rút kinh_nghiệm để tổ_chức kỳ thi tốt_nghiệp và tuyển_sinh năm 2006 .
Được biết , các đoàn giám_sát sẽ đi kiểm_tra đột_xuất các Hội_đồng thi mà không báo trước .
THANH_HÀ

﻿ Gollum hóa_thân thành King_Kong
Các chuyên_viên sẽ sử_dụng diễn_xuất của Serkis để từ đó tạo ra quái_vật King_Kong trên máy_vi_tính . Tuy_nhiên , đạo_diễn Jackson cho_biết không phải vì_vậy mà King_Kong sẽ trở_nên " mềm_mại " hoặc giống người hơn . Chất hoang_dã và hung_bạo của chú khỉ này vẫn sẽ được giữ nguyên .
Trong bộ ba phim Lord of the Rings , Serkis đã góp_phần quan_trọng để tạo ra nhân_vật Gollum , một người hobbit bị biến_dạng . Anh đã tạo ra toàn_bộ cử_động , từ mặt đến cơ_thể , để các chuyên_viên vẽ lại trên máy_tính . Serkis cũng đã lồng_tiếng cho Gollum .
King_Kong của đạo_diễn Jackson là bộ phim được làm lại từ phim King_Kong năm 1933 , vốn là một trong những phim kinh_dị thuộc hàng kinh_điển nhất . Bộ phim sẽ được bấm máy vào tháng 8 sắp tới tại New_Zealand .
Bên_cạnh Serkis , tham_gia bộ phim còn có nữ diễn_viên người Úc Naomi_Watts và nam diễn_viên thắng giải Oscar_Adrien_Brody . Đạo_diễn Jackson cũng đã quy_tụ rất nhiều thành_viên của Lord of the Rings để thực_hiện bộ phim này , như nhà soạn kịch_bản Fran_Walsh và Philippa_Boyens , nhà soạn nhạc Howard_Shore và quay_phim Andrew_Lesnie .
Dự_định bộ phim sẽ được tung ra_vào tháng 12-2005 .
TRẦN_MỸ_HẰNG ( Theo BBC News )

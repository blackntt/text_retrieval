﻿ Unilever VN tổ_chức ngày hội nghề_nghiệp
( NLĐ ) - Ngày hội nghề_nghiệp dành cho sinh_viên năm 2006 của Công_ty Unilever VN sẽ được tổ_chức vào ngày 28-4 , tại khách_sạn Caravelle_Saigon TPHCM .
Hơn 500 sinh_viên năm cuối đạt kết_quả học_tập xuất_sắc tại các trường đại_học ở TPHCM được mời tham_dự . Ngày hội là một trong những hoạt_động của chương_trình tuyển_dụng quản_trị viên tập_sự dành cho sinh_viên năm cuối do Công_ty Unilever VN tổ_chức hằng năm , nhằm tạo cơ_hội cho sinh_viên tiếp_xúc và tìm_kiếm cơ_hội làm_việc , khả_năng phát_triển nghề_nghiệp ...
N.Mai

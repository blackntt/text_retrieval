﻿ Hà_Tây : Bắt 3 " đinh tặc "
Ngày 16/9 , CA Thường_Tín , Hà_Tây đã bắt khẩn_cấp 3 " đinh tặc " . Những đối_tượng này chuyên rải đinh trên tuyến đường Pháp_Vân - Cầu_Giẽ , dẫn đến nhiều tai_nạn nguy_hiểm cho người tham_gia giao_thông .
Đó là Đỗ_Văn_Việt ( sinh năm 1987 ) , Đoàn_Quang_Đạo ( sinh năm 1988 ) , Phan_Văn_Vũ ( sinh năm 1988 ) , đều thường_trú ở xã Thắng_Lợi - Thường_Tín , Hà_Tây .
Bước_đầu , 3 đối_tượng khai nhận tổ_chức rải đinh và tổ_chức điểm vá xe_máy tại đường Pháp_Vân - Cầu_Giẽ ( đoạn Thường_Tín ) . Thủ_đoạn của chúng là phân_công 2 tên đi rải đinh , còn 1 tên đón nạn_nhân để vá xe với giá " cắt_cổ " . Theo Lao động

﻿ Điểm thi ĐH Mở-bán công TP.HCM , TDTT 1 , Y_tế công_cộng , CĐ Cộng_đồng Quảng_Ngãi
Mời bạn BẤM_VÀO_ĐÂY để xem điểm của bảy trường này và của các trường ĐH - CĐ đã được cập_nhật trên Tuổi_Trẻ_Online gồm :
1 . Học_viện Công_nghệ Bưu_chính viễn_thông - phía Bắc . 2 . Học_viện Công_nghệ Bưu_chính viễn_thông - phía Nam .
3 . ĐH Ngoại_thương - Hà_Nội . 4 . ĐH Ngoại_thương - cơ_sở 2 TP.HCM .
5 . ĐH Thủy_sản - Bắc_Ninh . 6 . ĐH Thủy_sản - Nha_Trang . 7 . ĐH Thủy_sản - TP.HCM .
8 . ĐH Dược_Hà_Nội . 9 . ĐH_Y Hải_Phòng . 10 . ĐH_Y Thái_Bình .
11 . ĐHDL Quản_lý kinh_doanh Hà_Nội . 12 . ĐHDL Thăng_Long .
13 . Cao_đẳng sư_phạm Mẫu_giáo TW3 . 14 . Nhạc_viện TP.HCM .
15 . ĐHDL Kỹ_thuật công_nghệ TP.HCM . 16 . ĐHDL Lạc_Hồng .
17 . ĐHDL Công_nghệ Sài_Gòn . 18 . ĐH Giao_thông vận_tải - phía Nam .
19 . ĐH Sư_phạm Đồng_Tháp .
Các bạn cũng có_thể quay số điện_thoại 19001733 để biết điểm thi của các trường này .
Xin lưu_ý :
- Có hai cách để xem điểm hoặc bạn nhập họ tên ( đầy_đủ họ và tên theo tiếng Việt có dấu ) hoặc số_báo_danh ( chỉ nhập phần số , không nhập phần chữ ) để xem điểm thi .
- Để sử_dụng được tiếng Việt có dấu , bạn có_thể chọn chế_độ gõ Telex hay VNI trong ô " Tiếng_Việt " kế bên .
- Với những trường_hợp tìm thấy SBD và họ tên nhưng không có điểm , đó là do thí_sinh không thi .
TTO

﻿ 16 câu_hỏi dành cho người lãnh_đạo
Tướng_Douglas_Mac_Arthur ( 1880-1964 ) là nhà quân_sự chiến_lược xuất_sắc của Mỹ và là nhà quản_lý có tầm_nhìn_xa . Ông đã thiết_lập một danh_sách gồm 16 câu_hỏi để hướng_dẫn chính mình trong việc lãnh_đạo và chúng vẫn có_ích cho những vị sếp thời nay .
Hãy tham_khảo những câu_hỏi dưới đây , tự thẩm_vấn xem mình đã là một nhà_lãnh_đạo hoàn_hảo chưa nhé !
1 . Tôi có động_viên và khuyến_khích nhân_viên ?
2 . Tôi có dũng_cảm loại_bỏ những nhân_viên luôn thích chứng_tỏ mình quá mức ?
3 . Trong quyền_hạn của mình , tôi làm tất_cả bằng việc khuyến_khích , khích_lệ và động_viên để khắc_phục những điểm yếu và sai_lầm ?
4 . Tôi có biết tên_tuổi và đặc_điểm của những nhân_viên mà tôi chịu trách_nhiệm về họ ? Tôi có biết rõ về họ ?
5 . Tôi có quen với các kỹ_thuật , các đòi_hỏi , mục_tiêu và cách quản_lý của tôi ?
5 . Tôi có_dễ mất bình_tĩnh ?
6 . Tôi có hành_động đúng theo cách mà các nhân_viên của tôi muốn đi theo tôi ?
7 . Tôi có đùn_đẩy những công_việc lẽ_ra là của tôi ?
8 . Tôi có ôm khư_khư mọi việc và chẳng uỷ_thác cho ai việc_gì ?
9 . Tôi có phát_triển nhân_viên của mình bằng giao cho họ càng nhiều trách_nhiệm càng tốt ?
10 . Tôi có thực_sự muốn nhân_viên của mình hạnh_phúc như_thể người đó là 1 thành_viên của gia_đình tôi ?
11 . Tôi có phong_thái , giọng nói bình_tĩnh , từ_tốn , tự_tin hay tôi dễ nổi_cáu và dễ bị kích_động ?
12 . Tôi có phải là hình_mẫu cho nhân_viên trong cách ăn_mặc , cách cư_xử , tác_phong ?
13 . Tôi có bợ_đỡ cấp trên và bủn_xỉn với cấp dưới ?
14 . Tôi có cởi_mở với nhân_viên của mình không ?
15 . Tôi có nghĩ nhiều đến vị_trí hơn là công_việc không ?
16 . Tôi có khiển_trách một nhân_viên ngay trước mặt những người khác không ?
Tự bản_thân bạn cũng biết , câu trả_lời thế_nào là đúng_đắn phải không ? Theo Jobvn / Nghề lãnh_đạo

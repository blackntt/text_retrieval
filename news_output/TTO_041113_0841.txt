﻿ Nga : nâng_cấp quỹ_đạo bay của ISS
Vào thứ_sáu 12-11 , Valery_Lyndin , người_phát_ngôn của trạm điều_hành dưới mặt_đất cho_biết độ cao ( so với mặt_nước biển ) của ISS thông_thường là vào_khoảng 400 km nhưng hiện_nay trung_bình mỗi ngày nó đã giảm 150 mét độ cao .
Lyndin cho_biết thêm : " Trong những ngày qua , trung_bình mỗi ngày độ cao đã giảm 300 mét , điều này có nghĩa_là kể từ đợt nâng_cấp quỹ_đạo gần đây nhất thì ISS đã bị giảm độ cao 7 km .
Lyndin cho_biết trong vòng 6 tháng tới sẽ có một loạt thay_đổi sửa_chữa tại ISS và đưa nó trở_về tình_trạng ban_đầu nhằm thực_hiện nhiệm_vụ tiếp_nhận tàu_con_thoi của Mỹ .
KINH_LUÂN ( Theo AFP )

﻿ Cúp QG Italia : Inter và Lazio vào tứ_kết
Trên sân_nhà San_Siro , Inter_Milan với đội_hình gần_như mạnh nhất của mình ( có Adriano , Martins , Solari ... ) nhưng đã bất_lực trong việc tìm kếm bàn thắng trước đội bóng đã ngụp_lặn ở cuối bảng xếp_hạng Parma . Không_chỉ có vậy , Inter_Milan còn gặp may khi trọng_tài Antonio_Giannoccaro đã bỏ_qua pha phạm lỗi của trung_vệ Marco_Materazzi với Ruopolo trong vòng cấm ( phút 81 ) và điều đó đã giúp cho họ tránh được trận thua bẽ_mặt ngay trên sân_nhà .
Dù không giành được chiến_thắng , nhưng trận hòa 0 - 0 này cũng đã giúp Inter_Milan giành được vé đi tiếp nhờ trận thắng 1 - 0 trong trận lượt_đi . Tuy_nhiên , với những gì đã thể_hiện trong trận đấu này , các cầu_thủ Inter đang khiến cho các CĐV của họ hết_sức lo_lắng , nhất_là khi cuộc đua tại Serie A đang bắt_đầu bước vào giai_đoạn quyết_liệt ( hàng tấn_công của Inter đã tịt_ngòi trong cả 2 trận gần đây ) .
Tương_tự như Inter , trận hòa 0 - 0 trước đội bóng đến từ giải Serie C , Cittadella trong trận lượt_về cũng đã giúp đội bóng thủ_đô Rome , Lazio giành được vé đi tiếp ( Lazio thắng 2 - 0 trong trận lượt_đi ) . Và với kết_quả này , Lazio sẽ chạm_trán với Inter_Milan ở vòng tứ_kết , và đó sẽ là một trận đấu rất khó_khăn đối_với họ .
Các cặp đấu khác ở vòng tứ_kết :
Sampdoria - Udinese AC Milan - Palermo AS Roma - Juventus
Các trận lượt_đi vòng tứ_kết sẽ diễn ra_vào ngày 25-1 và lượt_về sẽ diễn ra_vào ngày 1-2 .
TIÊU_PHONG

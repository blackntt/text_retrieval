﻿ Món quà đặc_biệt
Tôi trở_thành mối quan_tâm đặc_biệt của cô , cô luôn dành nhiều thời_gian để giảng bài thêm , nhắc_nhở , nắn_nót từng con chữ cho tôi . Cảm_giác gò_bó , sợ_sệt ban_đầu trong tôi rồi cũng qua nhanh , cô trở_thành “ chuyên_gia giải_đáp thắc_mắc ” của tôi lúc_nào không hay .
Nhà tôi chỉ cách nhà cô chừng chục ngõ nên cứ đến cuối tuần hay bất_kỳ khoảng thời_gian rảnh_rỗi nào , tôi lại qua nhà nhờ cô giảng bài hay hỏi_han khi có_điều gì thắc_mắc . Cô kèm_cặp tôi trong_suốt những năm_học tiểu_học . Thỉnh_thoảng có củ khoai hay bắp_ngô nhà trồng được là mẹ lại sai tôi mang sang biếu cô .
Hồi ấy đó là món quà giá_trị nhất mà tôi có được để_dành biếu cô_giáo chủ_nhiệm của mình . Lần nào tôi cũng phải hứa “ con sẽ cố_gắng học thật giỏi ” thì cô mới nhận quà . Còn tôi thì luôn quyết_tâm cố_gắng vì lời hứa với cô_giáo .
Nhưng tôi vẫn nhớ nhất_là “ món quà đặc_biệt ” tặng cô dịp hè năm tôi học lớp 5 - năm cuối_cùng tôi phải chia_tay cô . Món quà ấy được mẹ tôi gói_ghém rất cẩn_thận trong túi_bóng nhỏ màu xanh mà ngay cả tôi cũng không được biết trước . Tôi vui_vẻ cùng mẹ sang chào cô : “ Con chúc cô khỏe_mạnh và dạy ngày_càng tốt hơn , con chỉ có cái này biếu cô thôi ạ ” .
- Có ít lạc nhà trồng được mang sang biếu cô_giáo - mẹ tôi tiếp_lời .
Thì_ra món quà đặc_biệt của mẹ là một túi củ lạc nhà vừa thu_hoạch .
- Kết_quả học_tập của con mới là món quà quí nhất , cố lên nhá - cô nhìn tôi cười hi_vọng .
Hơn 10 năm trôi qua , giờ tôi đã là sinh_viên . Mỗi lần về quê ghé thăm cô , cô vẫn nhắc lại kỷ_niệm về những món quà nhỏ ngày ấy . Đó là những kỷ_niệm mà mỗi lần nhớ đến tôi lại thầm cảm_ơn cô bởi chính những lời hứa với cô đã khắc sâu vào tiềm_thức tôi và thôi_thúc tôi cố_gắng .
ĐINH_NGUYỆT ( Hà_Nội )

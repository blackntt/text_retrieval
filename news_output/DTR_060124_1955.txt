﻿ Bệnh nghiện “ yêu ”
Nguyên_nhân gây nghiện , ngoài yếu_tố di_truyền còn phải kể đến các chấn_thương tâm_lý vì bị cưỡng_đoạt thể_xác . Nghiện sex cũng giống như nghiện ma_túy và nghiện rượu . Người nghiện càng cần đến chuyện gối chăn , xúc_cảm lại càng giảm .
Trong xã_hội , có rất nhiều hình_thức cám_dỗ , kích_thích con_người , từ sách_báo , phim_ảnh , quảng_cáo đến những trang_web đen . Đó là lý_do khiến nhiều người trở_nên “ hăng_hái ” , dẫn đến “ thái_quá ” rồi dần “ nghiện ” chuyện gối chăn .
Những người mắc chứng này thường bị giảm năng_suất làm_việc vì phải luôn theo_đuổi thần ái_tình . Họ mơ_tưởng đến chuyện trăng_hoa ngay trong giờ làm_việc hoặc kiệt_sức vì thức đêm ...
Chứng này xảy_ra ở cả nam và nữ . Họ không_thể tâm_sự với ai . Do_đó , cuộc_sống hôn_nhân và các mối quan_hệ của họ dễ trở_nên phức_tạp , rối nhiễu . Nguyên_nhân nào dẫn đến chứng nghiện này ?
Ám_ảnh “ gối chăn ” khiến con_người trở_nên mệt_mỏi
Chất endorphin là hormone được tiết ra khi “ quan_hệ ” ngày_càng giảm xuống khiến người_ta dễ cáu_kỉnh và lo_buồn . Theo thời_gian , việc tìm_kiếm người “ quan_hệ ” trở_nên thường_xuyên hơn , và sex đã trở_thành một liều thuốc không_thể thiếu trước khi “ con_nghiện ” muốn tập_trung vào một việc_gì . Nếu_không có , họ sẽ cảm_thấy không chịu_đựng nổi .
Nghiện “ yêu ” trở_thành một tật xấu bị bài_trừ vì có những vấn_đề liên_quan đến pháp_lý . Nhiều người bị lợi_dụng hoặc chính họ lợi_dụng chuyện “ yêu_đương ” để mang lợi cho mình . Để loại_bỏ tình_trạng này , người_ta phải nhận_thức được_cái tôi tính_dục thầm_kín của mình . Họ cần được trị_liệu bởi các nhà tâm_lý , thầy_thuốc .
Con_người đủ quyết_tâm chữa bệnh nghiện tình_dục
Dục_năng vốn là bản_chất sinh_lý tự_nhiên , là yếu_tố để duy_trì nòi_giống . Tuy_nhiên , y_học phương đông từ lâu cũng đã coi_trọng việc tiết_dục .
Khi sinh_hoạt gối chăn được điều_tiết hợp_lý thì âm dương cân_bằng , cơ_thể khỏe_mạnh . Khoa_học cho thấy , tiết_dục không có hại mà_còn rất cần_thiết cho sự phát_triển nhân_cách . Nó còn đảm_bảo việc chủ_thể phát_triển tâm_lý tích_cực trong hôn_nhân .
Để giới_hạn ham_muốn chuyện gối chăn , bạn phải có một cái đầu tỉnh_táo , hiểu được những tác_động lôi_kéo của môi_trường xung_quanh . Ngoài_ra , trong thực_phẩm hàng ngày , rau_răm có tác_dụng ức_chế ham_muốn . Một_ít thực_phẩm khác như đậu_tương , cam_thảo , sò lụa , cao khỉ ... đều có tính_năng giảm bớt “ lửa yêu ” của con_người .
Việc vận_động nhiều rất có lợi cho chuyện nâng cao và cải_thiện tình_trạng gối chăn . Vì_thế , hãy cố_gắng rèn_luyện để làm_chủ được ham_muốn của mình .
Theo Tiếp_Thị & amp ; Gia_Đình

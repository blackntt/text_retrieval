﻿ Viết một đơn xin việc thành_công cho vị_trí nhân_viên kinh_doanh
Đơn xin việc cho vị_trí nhân_viên kinh_doanh cần chú_trọng đến kết_quả , nhấn_mạnh bạn đã đóng_góp như_thế_nào cho những phần mấu_chốt trong doanh_nghiệp ? Bắt_đầu bằng cách viết phần tóm_tắt nghề_nghiệp làm nổi_bật khả_năng kinh_doanh của bạn và có giá_trị đối_với người sử_dụng lao_động trong tương_lai . Liệt_kê thêm những lý_do chính tại_sao họ nên mời bạn đến phỏng_vấn , đồng_thời giới_thiệu rõ_ràng lĩnh_vực chuyên_môn và kiến_thức chuyên_ngành của bạn .
Chẳng_hạn , nếu bạn nộp đơn cho vị_trí trình_dược_viên , những cụm_từ chính đó cũng_như kiến_thức hỗ_trợ của bạn cũng cần được đưa vào đơn xin việc . Đây là phần lý_tưởng để trình_bày động_cơ , năng_lực và sự nhiệt_tình vốn_dĩ rất quan_trọng đối_với nghề bán hàng .
Biến phần Mô_tả công_việc của bạn thành bảng Thành_tích hoạt_động
Đưa ra bảng thành_tích kinh_doanh của bạn trong phần kinh_nghiệm làm_việc là rất quan_trọng . Đối_với mỗi chức_vụ / công_ty , viết một đoạn văn ngắn mô_tả trách_nhiệm của bạn ( như phụ_trách phạm_vi thị_trường , ngân_sách , giám_sát ... ) . Sau đó đưa ra một danh_sách gạch đầu dòng những thành_tích nổi_bật của bạn ; đảm_bảo những thông_tin này bao_gồm các số_liệu cụ_thể và rõ nghĩa đối_với những người_ở ngoài công_ty .
Để tăng sức thuyết_phục cho bảng thành_tích của mình , bạn không_chỉ đưa ra kết_quả làm_việc mà_còn trình_bày bạn đạt được những kết_quả xuất_sắc này như_thế_nào . Bạn hãy xem qua một_số câu mô_tả thành_tích có tác_động mạnh :
Xây_dựng phòng kinh_doanh từ con_số không , vạch ra kế_hoạch chiến_lược mang lại doanh_thu 1 triệu đô - la từ việc kinh_doanh phần_mềm và tư_vấn chỉ trong một năm . Duy_trì mức tăng doanh_thu mặc_dù thị_trường đang sụt_giảm với sức cạnh_tranh khốc_liệt .
Nuôi_dưỡng mối quan_hệ với bạn_hàng trong ngành công_nghiệp chất_bán_dẫn , tái thành_lập công_ty , sản_phẩm và dịch_vụ cũng_như phát_hiện nhu_cầu mới của khách_hàng .
Đạt được sự công_nhận 100% của bạn_hàng gồm chín khách_hàng sản_xuất chất_bán_dẫn , mà trước_đây từng phàn_nàn khâu dịch_vụ khách_hàng của công_ty . Xác_định vấn_đề và làm_việc trực_tiếp với giám_đốc điều_hành để lấy lại lòng tin của họ và phát_triển các giải_pháp hai bên cùng có lợi .
Những câu_hỏi sau đây sẽ giúp bạn suy_nghĩ về các thành_tích của mình :
Công_ty của bạn đã được lợi gì từ năng_lực bán hàng của bạn ?
Biểu_hiện trong công_việc của bạn so với đồng_nghiệp như_thế_nào ?
Doanh_số cụ_thể bạn đạt được là bao_nhiêu ( viết ra số tiền cụ_thể nếu thông_tin ấy không cần bảo_mật , còn không thì ghi con_số phần_trăm ) .
Bạn đạt được hạn_ngạch hoặc các mục_tiêu kinh_doanh khác tốt đến mức nào ?
Bạn có nhận được giải_thưởng bán hàng nào không ?
Bạn có nhận được phần_thưởng là phân_khu thị_trường mới nhờ vào biểu_hiện trong công_việc của mình không ?
Bạn có giành được khách_hàng khó_tính nào không ? Bạn có cứu_vãn được một mối_hàng có nguy_cơ bị mất không ?
Bạn có tham_gia phát_triển sản_phẩm hoặc tung sản_phẩm mới ra thị_trường không ?
Bạn có vượt qua thử_thách nghiêm_trọng nào chưa , như bán hàng trong điều_kiện thị_trường khó_khăn , vượt qua phản_cảm ban_đầu hoặc xâm_nhập vào thị_trường mới ?
Bạn có lập ra một chương_trình huấn_luyện bán hàng hoặc hướng_dẫn các chuyên_viên bán hàng khác tiến_bộ trong công_việc ?
Sự tận_tâm của bạn đối_với dịch_vụ khách_hàng , quá_trình thực_hiện giao_dịch và hỗ_trợ hoàn_hảo đối_với khách_hàng có dẫn đến các mối_hàng lâu_dài hoặc thêm nhiều khách_hàng được giới_thiệu đến không ?
Bạn có điều_khiển các cuộc đàm_phán hợp_đồng mang lại những thỏa_thuận làm_ăn có lợi cho công_ty không ?
Bạn có đàm_phán với nhà_bán_lẻ hoặc nhà_cung_cấp để đảm_bảo được_giá ưu_đãi ?
Bạn đã từng viết bài cho các ấn_phẩm trong ngành hoặc nói_chuyện tại sự_kiện hoặc hội_nghị nào chưa ?
Bạn có phục_vụ trong một ủy_ban hoặc hội_đồng , hoặc tham_gia vào dự_án đặc_biệt nào không ?
Yếu_tố bảo_mật
Luôn nhớ rằng nhiều công_ty coi chiến_lược bán hàng và kết_quả kinh_doanh là thông_tin cần bảo_mật . Nguy_cơ các đối_thủ cạnh_tranh phát_hiện chiến_lược mang đến thành_công cho công_ty là có thật , do_vậy bạn cần chắc rằng mình đã không đưa vào bất_cứ thông_tin nào mà công_ty cũ cũng_như công_ty hiện_tại của bạn cần bảo_mật . Lẽ đương_nhiên bạn có_thể đưa vào thông_tin được công_bố rộng_rãi cho công_chúng ( ví_dụ như các số_liệu trong bản báo_cáo hàng năm hoặc trên trang_web của công_ty ) .
Những cụm_từ then_chốt / từ chuyên_môn
Đại_diện kinh_doanh , chuyên_viên kinh_doanh , trưởng_phòng kinh_doanh , giám_đốc kinh_doanh khu_vực , phó_giám_đốc kinh_doanh , nhân_viên chăm_sóc khách_hàng , giám_đốc bộ_phận chăm_sóc khách_hàng , nhân_viên phòng kinh_doanh , kỹ_sư kinh_doanh , giám_đốc phòng kinh_doanh , giám_đốc hỗ_trợ kinh_doanh , đại_diện kinh_doanh trong khu_vực , giám_đốc khu_vực , giám_đốc kênh bán hàng , đại_diện nhà_sản_xuất , nhân_viên kinh_doanh phòng kỹ_thuật , trình_dược_viên , nhân_viên kinh_doanh dược_phẩm , giám_đốc kinh_doanh qua mạng , đại_diện đầu_tư , nhân_viên kinh_doanh ngành CNTT .
Bán giải_pháp , xây_dựng mối quan_hệ , bán hàng thông_qua các mối quan_hệ , quan_hệ kinh_doanh , dịch_vụ khách_hàng , quan_hệ với khách_hàng , mở_rộng thị_trường , bán hàng kèm tư_vấn sản_phẩm / dịch_vụ , tiếp_thị sản_phẩm , đàm_phán và đúc_kết , kinh_doanh theo kênh , doanh_nghiệp với doanh_nghiệp / doanh_nghiệp với khách_hàng , thế_hệ dẫn_đầu , nhà_sản_xuất thiết_bị chính_thức , kỹ_năng giao_tiếp , phát_triển mới trong kinh_doanh , chào_hàng , PowerPoint , đạt được và vượt quá hạn_ngạch , bán hàng ra bên ngoài , bán hàng trong nội_bộ , mở_rộng kinh_doanh .
Theo HRVietnam

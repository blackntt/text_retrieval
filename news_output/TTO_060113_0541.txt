﻿ Thêm một khu ngoại quan nối Trung_Quốc - VN
Theo ông Tô_Quốc_Tuấn , cán_bộ ban quan_hệ quốc_tế của VCCI ( Phòng_Thương mại và công_nghiệp VN ) , xuất_phát từ lợi_thế nằm giữa thủ_phủ Nam_Ninh và thủ_đô Hà_Nội , thị_xã này có_thể phát_triển thành trung_tâm lưu_chuyển hàng_hóa với hệ_thống kho vận , trung_tâm thông_tin , nhà_nghỉ phục_vụ giao_thương và du_lịch xuyên biên_giới với VN .
Phía_Sùng_Tả cũng gợi_ý về việc xây_dựng một trung_tâm tương_tự tại miền Bắc VN và thị_xã Bắc_Ninh được đề_cập tới . Hiện các tuyến đường_bộ cấp một đang được gấp_rút hoàn_thành nhằm phục_vụ giao_thông giữa Sùng_Tả với các địa_phương lân_cận .
C . HÀ

﻿ Dịch_vụ online đe_dọa tương_lai Microsoft
Hãng phần_mềm khổng_lồ đang phải đối_mặt với một tương_lai khó_khăn nhất vì các dịch_vụ trực_tuyến của Google đang phát_triển như vũ_bão , các nhà_phân_tích nhận_định .
“ Nếu để_ý đến các thông_báo mới_đây của Microsoft , chúng_ta sẽ phải xem_xét những gì sẽ diễn ra trong môi_trường máy_tính_để_bàn doanh_nghiệp ” , David_Ferris , nhà_phân_tích cao_cấp của công_ty Ferris_Research , nói . “ Microsoft sẽ gặp hạn trong vòng 3 - 5 năm tới ” .
Ông Ferris nhấn_mạnh , liên_minh Google/Sun có_thể sẽ là trở_ngại lớn nhất của Microsoft_Office . “ Hầu_hết các công_ty phải sử_dụng chiếc máy_tính cồng_kềnh vì họ phải làm_việc trên Office ” , Ferris nói .
Malte_Nuhn , một nhà_phân_tích của công_ty Millward_Brown_Optimor , nói thêm : “ Rất nhiều ứng_dụng mà trước_đây mọi người phải chạy trên hệ_điều_hành thì bây_giờ có_thể chuyển sang làm_việc online . Thực_tế , w eb đang dần trở_thành hệ_điều_hành ảo ” .
Tuy_nhiên , ông Nuhn tin rằng Microsoft sẽ giải_quyết vấn_đề này . “ Khi web trở_thành một hệ_thống ưu_việt hơn , và tốc_độ kết_nối băng_thông lớn hơn sẽ hỗ_trợ các ứng_dụng online ” , ông này nhận_định .
Ông Ferris khẳng_định : “ Nếu_Google và Sun hợp_tác phát_triển phiên_bản phần_mềm văn_phòng OpenOffice thì triển_vọng của hệ_điều_hành sắp ra_mắt Vista sẽ bị tác_động mạnh ” .
N.Hương Theo VNuNet

﻿ Tuần_lễ thời_trang quốc_tế Trung_Quốc
Trong_suốt tuần_lễ , các nhà_tạo_mẫu sẽ cho ra_mắt những bộ sưu_tập mới nhất và những mẫu thiết_kế này đánh_giá được là xu_hướng thời_trang mới của mùa tiếp_theo . Hàng trăm người_mẫu đã được huy_động để biểu_diễn phục_vụ cho tuần_lễ thời_trang sôi_động .
Với việc tổ_chức đều_đặn hàng năm , ban tổ_chức hy_vọng rằng tuần_lễ thời_trang là cơ_hội để ngành thời_trang TQ có dịp quảng_bá tên_tuổi cũng_như phấn_đấu đưa TQ trở_thành trung_tâm thời_trang châu Á .
TTO giới_thiệu một_số mẫu thiết_kế tiêu_biểu trong đêm khai_mạc Tuần_lễ thời_trang quốc_tế Trung_Quốc lần thứ 14 hôm chủ_nhật vừa_qua .
NH . B ( Theo Chinadaily )

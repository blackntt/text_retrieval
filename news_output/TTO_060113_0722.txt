﻿ Hà_Nội công_bố 48 điểm bán gia_cầm phục_vụ tết
Khó tìm gà ta qua kiểm_dịch
Mua gà sạch tại siêu_thị - Ảnh : V . HƯNG_TT - UBND thành_phố Hà_Nội vừa công_bố 48 điểm bán gia_cầm phục_vụ tết . Cụ_thể , huyện Sóc_Sơn có 5 điểm bán , 2 điểm giết_mổ ; huyện Đông_Anh 2 điểm bán , 7 điểm giết_mổ ; huyện Gia_Lâm 6 điểm bán và 7 điểm giết_mổ ; huyện Từ_Liêm 1 điểm bán và 1 điểm giết_mổ ; huyện Thanh_Trì 7 điểm bán .
Riêng các quận nội_thành thực_hiện nghiêm_túc lệnh cấm_vận chuyển gia_cầm sống vào nội_thành . Thành_phố chỉ cho_phép bán gia_cầm đã qua chế_biến . Quận_Ba_Đình có nhiều điểm bán nhất ( 8 ) , kế đó là Long_Biên , Đống_Đa , Hoàn_Kiếm mỗi quận 4 điểm ; Hai_Bà_Trưng ( 3 ) ; Thanh_Xuân ( 2 ) ; Hoàng_Mai và Cầu_Giấy mỗi nơi 1 điểm bán .
Bên_cạnh đó Hà_Nội cũng cho_phép thành_lập thêm 15 điểm giết_mổ , ngoài 2 cơ_sở tại Công_ty_cổ_phần Phúc_Thịnh và Trung_tâm nghiên_cứu gia_cầm Thụy_Phương . Để tăng_cường công_tác kiểm_dịch , thành_phố tiếp_tục duy_trì 8 trạm kiểm_dịch , trực 24/24 giờ tại Ngọc_Hồi , Dốc_Lã , Nhổn , Nam_Thăng_Long , Pháp_Vân , Thượng_Đình , đường Láng - Hòa_Lạc , Cầu_Chui .
Gà sạch trên thị_trường Hà_Nội hiện không thiếu nhưng chủ_yếu là gà_công_nghiệp hoặc gà tam hoàng . Các quầy bán gà ta ( 60.000đ/kg ) đã qua kiểm_dịch quá ít nếu_không muốn nói là tìm mãi nhưng không thấy .
Nguyên_nhân dẫn đến nguồn gà ta quá hiếm và thiếu chính là do hiện_tại Hà_Nội chưa có cơ_sở nào chăn_nuôi , giết_mổ gà ta với số_lượng lớn . Lượng gà ta được bày bán là do các hộ dân nhỏ_lẻ nuôi và chủ_yếu ở các xã ngoại_thành .
Ông Vương_Tiến_Ngọc , giám_đốc Công_ty_cổ_phần Phúc_Thịnh , cho_biết đang kiến_nghị với các cơ_quan_chức_năng cho nhập gà ta đã qua kiểm_dịch tại các địa_phương khác về giết_mổ trên dây_chuyền của công_ty .
VIỆT_HƯNG - TRANG_ANH

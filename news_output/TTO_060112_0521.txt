﻿ Hà_Nội ban_hành qui_định bán gia_cầm sạch
Theo đó , tại các siêu_thị , trung_tâm thương_mại , thịt gia_cầm phải có dấu kiếm dịch , giấy xác_nhận xuất_xứ và giấy chứng_nhận kiểm_dịch của cơ_quan thú_y tại cửa_khẩu hoặc tại địa_điểm giết_mổ có phép ; phải có hợp_đồng mua_bán giữa nhà_cung_cấp và đơn_vị trực_tiếp kinh_doanh ; phải đựng trong bao_bì đóng_gói đủ tiêu_chuẩn vệ_sinh an_toàn thực_phẩm , có niêm_yết giá ; bảo_quản trong tủ kính có hệ_thống làm mát , sản_phẩm chưa qua chế_biến không được để lẫn với các thực_phẩm khác .
Các cửa_hàng kinh_doanh đường_phố phải có giấy chứng_nhận đảm_bảo vệ_sinh an_toàn thực_phẩm , đăng_ký kinh_doanh , có giấy chứng_nhận tạm_thời đủ điều_kiện kinh_doanh do Sở Thương mại cấp .
NGỌC_MINH

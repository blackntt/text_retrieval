﻿ 84% thị_phần ĐTDĐ thuộc về Nokia và Samsung
Trên thị_trường sản_phẩm ĐTDĐ , mặc cho Samsung_Mobile công_bố việc họ quyết_tâm qua_mặt Nokia về thị_phần ĐTDĐ tại Việt_Nam trong năm 2004 nhưng trong quí I/2004 , thị_phần của Nokia và Samsung vẫn không thay_đổi so với năm 2003 .
Theo công_ty nghiên_cứu thị_trường GFK , Nokia vẫn giữ vị_trí dẫn_đầu với 47% thị_phần và Samsung là 37% , 16% thị_phần còn lại thuộc về Motorola , Sony_Ericsson , Siemens , LG , Huyndai , V-Fone , S-Fone , Panasonic ...
Nhìn những con_số trên chúng_ta có_thể tưởng_tượng ra sự thống_lĩnh thị_trường của Nokia và Samsung , xem_ra 16% thị_phần còn lại là quá nhỏ_bé cho trên_dưới 10 nhãn_hiệu khác . Vậy đối_thủ trước_mắt của hai người khổng_lồ này trong thời_gian tới là ai ?
Theo nhiều chuyên_gia trong lĩnh_vực ĐTDĐ , Sony_Ericsson là đáng ngại hơn cả vì họ đang có một_số sản_phẩm khiến bất_cứ người tiêu_dùng nào cũng phải quan_tâm . Trong những tháng đầu năm 2004 , lần đầu_tiên người dùng nghe đến khái_niệm hai mặt ( dual-front ) của một chiếc ĐTDĐ .
Sony_Ericsson chủ_trương thiết_kế những chiếc ĐTDĐ có hai mặt đều quan_trọng như nhau không có khái_niệm mặt trước và mặt sau như những chiếc ĐTDĐ trước_đây , và thực_tế những chiếc ĐTDĐ mới dual - front của hãng này đã tạo được ấn_tượng tốt ngay trong những lần giới_thiệu đầu_tiên .
Kèm theo việc Nokia chiếm ưu_thế trên thị_trường là các ĐTDĐ không nắp ( Bar ) đang chiếm tới 64% thì phần ( đa_số các ĐTDĐ của Nokia là không nắp ) tuy_nhiên cũng lưu_ý là số_lượng người sử_dụng ĐTDĐ không nắp đã giảm đến 9% vì cùng kỳ năm ngoài ĐTDĐ không nắp chiếm 75% thị_phần . Và xu_hướng trong thời_gian tới ĐTDĐ có nắp ( ( bao_gồm ĐTDĐ nắp gấp vỏ sò , nắp trượt , nắp đậy bàn_phím ) sẽ được tiêu_thụ nhiều vì ưu_điểm gọn , thời_trang và tiện_dụng của chúng .
Chưa bao_giờ người Việt_Nam mua nhiều ĐTDĐ như những tháng đầu năm 2004 , quí I/2004 đã có khoảng 300.000 ĐTDĐ được bán ra , gần bằng gấp đôi so với cùng kỳ năm ngoài . Xu_hướng tiêu_dùng cũng đã thay_đổi rất rõ_ràng , các công_nghệ mới đã được ứng_dụng và tích_hợp vào ĐTDĐ một_cách nhanh_chóng .
Những tháng đầu năm 2004 số_lượng ĐTDĐ có màn_hình màu chiếm tới 75% số_lượng máy bán ra ( cùng kỳ năm_ngoái chỉ là 13% số_lượng máy có màn_hình màu ) . ĐTDĐ có máy_ảnh chiếm 22% số_lượng máy bán ra trong quí I/2004 , trong khi cùng thời_điểm năm_ngoái chỉ có 2% ĐTDĐ bán ra là có tích_hợp máy chụp ảnh .
Khi hầu_hết các ĐTDĐ đếu có màn_hình màu , tin nhắn MMS thì ĐTDĐ không_thể không có máy_ảnh . Vì bản_chất tin nhắn MMS là công_cụ cho_phép người dùng gửi tin nhắn có kèm hình_ảnh và âm_thanh chính vì_vậy một chiếc ĐTDĐ ngày_nay không có máy_ảnh mới là chuyện lạ .
Nhưng cũng phải nhấn_mạnh rằng máy_ảnh của ĐTDĐ dù có đạt tới độ_phân_giải megapixel như một_số model được giới_thiệu gần đây của Sony_Ericsson và Nokia ( nhưng chưa được bán ở Việt_Nam ) thì nó cũng chưa_thể thay_thế được một chiếc máy_ảnh số thông_thường .
Trong quí I/2004 , vẫn theo GFK top 10 về số_lượng ĐTDĐ được bán ra tại Việt_Nam lần_lượt thuộc về các nhãn_hiệu và model sau ( xếp theo thứ_tự từ cao đến thấp ) : Nokia 6610 , Nokia 6100 , Samsung C100 , Nokia 3100 , Samsung A800 , Nokia 1100 , Samsung E700 , Nokia 2100 , Samsung S500 , Samsung X430 . Như_vậy top 10 hoàn_toàn thuộc về Nokia và Samsung . Đáng tiếc là sản_phẩm Sony_Ericsson T610 , model đứng đầu top nhiều tháng qua tại châu Á - Thái_Bình_Dương lại không có một vị_trí tốt tại thị_trường Việt_Nam .
Bảng so_sánh số_lượng ĐTDĐ bán ra trong quí I/2004 so với quí I/2003
Quí I/2004
Quí I/2003
Số_lượng ĐTDĐ bán ra
300.000
170.000
Điện_thoại không nắp
64%
75%
Điện_thoại có nắp
36%
25%
Điện_thoại có máy_ảnh
22%
2%
Điện_thoại có màn_hình màu
75%
13%
( Nguồn GFK Vietnam )
Đương_nhiên là trong nửa đầu của năm 2004 , còn có nhiều sự_kiện khác liên_quan đến thị_trường ĐTDĐ tại Việt_Nam như việc người dùng ĐTDĐ của S-Fone và Mobifone đã có_thể nhắn_tin được cho nhau ; Việc_Mobifone và Ericsson thử_nghiệm thành_công dịch_vụ ĐTDĐ thế_hệ thứ 3 ( 3G ) tại Việt_Nam ; CityPhone cung_cấp dịch_vụ nhắn_tin ; Thậm_chí cả việc Mobifone và Vinaphone vẫn tiếp_tục “ dắt tay ” nhau thử_nghiệm dịch_vụ GPRS trong_suốt nhiều tháng qua mà không biết bao_giờ mới tuyên_bố kết_thúc thử_nghiệm ...
Năm 2004 còn nửa chặn đường nữa và tin chắc rằng thị_trường ĐTDĐ Việt_Nam sẽ tiếp_tục có nhiều biến_động khác , tỷ_như sự_kiện Vietel sẽ chính_thức khai_thác dịch_vụ ĐTDĐ trong thời_gian tới . Người_ta dự_đoán sẽ tiếp_tục có những quả bom giảm_giá khác và người hưởng lợi lớn nhất một lần nữa lại thuộc về người dùng .
Theo PCW

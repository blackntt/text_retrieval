﻿ TP.HCM : 68,61% cán_bộ trẻ diện qui_hoạch giữ vị_trí cao
Từ năm 1999 đến nay , tổng_số cán_bộ công_chức trẻ từ ba nguồn : cán_bộ trong các cơ_quan đơn_vị , SV và công_nhân do Ban tổ_chức Thành_ủy phân_công công_tác là 549 người . Trong đó 191 người ( 68,61% ) được đề_bạt , bổ_nhiệm trưởng , phó_phòng quận , huyện , phường , xã , sở ban_ngành . 313 người ( 54,51% ) đã đứng vào hàng_ngũ Đảng CSVN .
Dịp này , Thành_ủy cũng giới_thiệu một_số cán_bộ trẻ diện qui_hoạch tiêu_biểu ở Thành_đoàn , quận huyện , cơ_sở ...
ĐẶNG_TƯƠI

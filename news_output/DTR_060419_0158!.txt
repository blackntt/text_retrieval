﻿ Phát miễn_phí tài_liệu sơ_cứu tại_chỗ
Đây là nội_dung chính trong dự_án “ Nâng cao năng_lực điều_phối thông_tin cho cộng_đồng về vận_chuyển cấp_cứu và chăm_sóc chấn_thương ” do Bộ y_tế triển_khai .
Theo đó , dự_án này sẽ được triển_khai trong cả nước , người_dân sẽ được cung_cấp miễn_phí tài_liệu về hướng_dẫn sơ_cứu tại_chỗ , sơ_cứu ban_đầu cho tai_nạn chấn_thương và những tai_nạn thường gặp khác trong cộng_đồng .
Cục Y_tế Dự_phòng ( Bộ Y_tế ) cho_biết : Điều này sẽ giúp giảm số ca tử_vong cũng_như biến_chứng thương_tật trong cộng_đồng .
Theo thống_kê chưa đầy_đủ , mỗi năm cả nước ta có tới 4 triệu người bị tai_nạn thương_tích do các nguyên_nhân như : tai_nạn nghề_nghiệp , va_chạm , bị đánh_đập , ngã … trong đó , số tử_vong lên tới 70.000 người . Phạm_Thanh

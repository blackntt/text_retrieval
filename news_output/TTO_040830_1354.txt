﻿ “ Lam_Trường và những người bạn " chia sẽ nỗi đau da_cam tại Huế
Ca_sĩ Hà_Kim_Ngọc , giữa tiết_mục diễn , đã trích một phần thù_lao của mình đóng_góp vào Quỹ ủng_hộ nạn_nhân chất_độc da_cam đồng_thời kêu_gọi , và được đông_đảo các bạn trẻ sôi_động hưởng_ứng . Nhiều khách nước_ngoài vào xem đêm nhạc cũng tham_gia ký_tên và ung hộ Quỹ " Góp tay xoa_dịu nỗi đau da_cam " .
Từ những ngày_trước , thanh_niên và trí_thức Huế ( đặc_biệt một nhóm học_sinh lớp 12 chuyên toán trường Quốc_Học ) đã tự tổ_chức vận_động với hơn 1000 chữ_ký " Ký_tên vì công_lý " .
Tin , ảnh : TH . LỘC

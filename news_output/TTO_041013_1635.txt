﻿ Son_Yea_Jin và Jung_Woo_Sung kết_hôn ?
Son_Yea_Jin ( nổi_tiếng từ phim Hương mùa_hè ) sẽ đóng vai người_tình tuyệt đẹp của Jung_Woo_Sung ( anh_hùng trong Những tay_đua kiệt_xuất và hàng_loạt phim_điện_ảnh ăn_khách như Moo_In - đóng cùng Chương_Tử_Di ) .
Cảnh cưới trong phim được đầu_tư nhiều không kém phim Khăn_tay vàng ( đã chiếu trên truyền_hình VN ) . Áo_cưới cho Son trị_giá trên 10 triệu won , còn Jung_Woo_Sung thì diện luôn một bộ " hot " nhất trong thời_trang dành cho mùa thu .
Giới báo_chí thì tin chắc rằng họ sẽ đoạt giải " Đôi tình_nhân màn_ảnh đẹp nhất trong năm " do khán_giả bầu_chọn hàng năm . Còn đối_với Jung_Woo_Sung , đây là cơ_hội tuyệt_vời giúp anh trở_lại màn_ảnh nhỏ một_cách ấn_tượng sau nhiều năm đầu_tư cho điện_ảnh .
TỐ_UYÊN ( Theo Chosun ilbo )

﻿ Thanh_Lam : " Tôi tin khán_giả sẽ là nhà tài_trợ chính "
Tính cho_đến thời_điểm này , Thanh_Lam là ca_sĩ ngôi_sao đầu_tiên thực_hiện lời hứa làm liveshow từ năm cũ ( diễn ra_vào 2 đêm : 2 và 3/3/2006 tại Nhà_hát lớn Hà_Nội với chủ_đề Em tôi ) .
Chị từng nói sẽ chờ có tài_trợ mới dám làm liveshow , nhưng chương_trình lần này không có tài_trợ chị vẫn quyết_tâm làm . Chị có sợ bị ... lỗ_vốn ?
Thực_ra cũng hơi lo , nhưng đã lâu lắm rồi Thanh_Lam không được hát một_mình một chương_trình trước người_yêu nhạc . Chương_trình lần này Thanh_Lam muốn " báo_cáo " với khán_giả những gì đã và đang làm . Dù không có tiền , tôi vẫn phải làm_vì tôi tin khán_giả sẽ là nhà tài_trợ chính .
Trước_đây , các chương_trình của Thanh_Lam đều để lại dấu_ấn riêng . Còn điểm đặc_biệt của liveshow Em tôi ?
Em tôi có tất_cả những ca_khúc từng gắn liền cùng tên_tuổi Thanh_Lam được phối mới trở_lại như Chia_tay hoàng_hôn , Giọt nắng bên thềm , Không_thể và có_thể ... và rất nhiều những ca_khúc mới và cũ của Lê_Minh_Sơn .
Thể_hiện nhiều ca_khúc của Lê_Minh_Sơn , chị có sợ khán_giả ngán ?
Tôi không lo điều này . Các ca_khúc cũ thì được phối mới trở_lại bên_cạnh một_số ca_khúc mới nhất của Lê_Minh_Sơn nên sẽ không làm khán_giả ngán đâu .
Trong liveshow có 2 khách mời đặc_biệt là ca_sĩ Trọng_Tấn và con_trai Thanh_Lam . Chị không sợ " chỏi " với chất giọng mình sao ?
Ban_đầu nhiều người cũng nghĩ vậy , nhưng đây là ... điểm nhấn của chương_trình mà ! Trọng_Tấn sẽ hát cùng tôi hai bài Cho con ngày mưa ( con_trai tôi cũng hát chung bài này ) và Xa_xa . Dù là lần đầu_tiên chúng_tôi - hai " trường_phái " cổ_điển và nhạc_nhẹ - kết_hợp cùng nhau nhưng chúng_tôi vẫn thấy rất hợp rơ . Nghe rất lạ bạn ạ , nên tôi hy_vọng khán_giả sẽ thích .
Chị có tính đưa nguyên chương_trình vào Sài_Gòn không ?
Kế_hoạch đưa liveshow vào Sài_Gòn cũng đã có . Song vận_chuyển nguyên một chương_trình lớn như_vậy trên cả ngàn cây_số không phải là chuyện nhỏ . Giá_như tại Sài_Gòn có tài_trợ thì ... tuyệt_vời ! Tôi sẽ tính đến chuyện này sau khi 2 đêm diễn kết_thúc .
Xin chúc chị thành_công ! Theo Thanh_Niên

﻿ Lần đầu ra_mắt
Ăn_mặc “ mát_mẻ ” : Đây là điều tối_kỵ khi bạn đi tới những buổi gặp_gỡ quan_trọng này . Cách ăn_mặc quá xa_rời truyền_thống sẽ khiến mẹ anh_ấy nghĩ rằng bạn không phải là người phụ_nữ đoan_trang .
“ Cháu đã được nghe kể chuyện về gia_đình nên hai bác cứ tự_nhiên với cháu . Cháu không phải là người hay kể chuyện linh_tinh đâu ” . Thực_ra dù bạn có biết rõ về hoàn_cảnh gia_đình anh_ấy cũng không nên nói như_vậy . Nó sẽ gây ra sự e_ngại trong_suốt buổi trò_chuyện .
Lợi_dụng việc bố hoặc mẹ anh_ấy có cảm_tình với bạn hơn người kia để nghiêng về phía này nói_xấu phía kia , gây chia_rẽ tình_cảm giữa hai người .
Nói_cười rất to và luôn xen vào câu_chuyện của người_lớn . Đây cũng là điều khiến họ không hài_lòng về bạn , họ sẽ đánh_giá là vô_ý_vô_tứ thậm_chí thiếu giáo_dục .
Có những hành_động quá thân_mật , âu_yếm với người_yêu . Người_ngoài nhìn vào sẽ nghĩ : “ Nó là đứa con_gái quá dễ_dãi và buông_thả ” .
Nói_chuyện với anh_ấy bằng thứ ngôn_ngữ mà bố_mẹ không hiểu để chứng_tỏ bạn hiểu_biết , nhưng họ sẽ cho rằng bạn coi_thường họ và e_ngại sau_này sẽ “ khó dạy con dâu ” .
Nói lời nịnh_nọt hoặc khen_ngợi thái_quá , không hợp lúc sẽ làm bố_mẹ anh_ấy cho rằng bạn là người không thật lòng .
Nếu bạn phải chở mẹ anh_ấy đi mua_sắm , bạn nhớ không được phóng nhanh vượt ẩu , hoặc chỉ gợi_ý những món hàng đắt tiền cho mẹ anh_ấy mua vì bà sẽ nghĩ rằng bạn là người không biết tiết_kiệm và sẽ là người_quản_lý gia_đình kém .
Dùng những khoảng_trống trong cuộc trò_chuyện để làm_việc riêng như gọi điện_thoại , kiểm_tra tin nhắn hoặc nhìn lơ_đãng sang chỗ khác . Điều này khiến ai cũng cảm_thấy khó_chịu .
Theo Dân_Trí

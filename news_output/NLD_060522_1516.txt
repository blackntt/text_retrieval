﻿ Đã tiếp_cận được với 3 tàu bị nạn của Đà_Nẵng
. Các ngư_dân sống_sót sẽ cập cảng sông Hàn vào ngày_mai 23-5
Theo thông_tin của Ủy_ban tìm_kiếm cứu nạn , cho_đến 14 giờ chiều nay 22-5 , các tàu của lực_lượng hải_quân và Ủy_ban tìm_kiếm cứu nạn đã tiếp_cận với 3 tàu bị nạn của Đà_Nẵng , còn 1 tàu vẫn chưa tiếp_cận được vì tàu thiếu dầu , các tàu cứu nạn đang chuyển dầu tới chiếc tàu này .
Trên 4 tàu tổng_cộng có các ngư_dân và 17 xác nạn_nhân . Hiện Ủy_ban quốc_gia tìm_kiếm cứu nạn đang tiếp_tục liên_lạc với 6 tàu còn bị mất_tích . 4 tàu này dự_kiến nhanh_nhất là 18 giờ ngày 23-5 sẽ cập cảng Đà_Nẵng . Các công_việc chuẩn_bị đón các ngư_dân và nạn_nhân đã được các cơ_quan chuẩn_bị một_cách tốt nhất .
Thông_tin cho_đến giờ_phút này , hiện có 11 tàu của các ngư_dân bị chìm , trong tổng_số 221 ngư_dân thì_có 27 người chết , 89 người bị mất_tích , và đã vớt được 105 người . Hiện_nay các lực_lượng cứu_hộ đang cố_gắng liên_lạc với số tàu chưa tìm thấy .
Danh_sách tàu chìm và mất_tích của Đà_Nẵng :
7 tàu bị chìm với 140 lao_động :
1 . Tàu ĐN a 90079 TS của ông Ngô_Tấn_Nhất - SN 1958 , trú tổ 30 , Thanh_Khê_Đông , có 22 lao_động .
2 . Tàu ĐN a 90190 TS của ông Trương_Văn_Minh - SN 1972 , trú tổ 9 , Xuân_Hà , có 20 lao_động .
3 . Tàu ĐN a 90053 TS của bà Lê_Thị_Huệ - SN 1965 , trú tổ 15b Thanh_Khê_Đông , có 21 lao_động .
4 . Tàu ĐN a 90199 TS của ông Phạm_Văn_Xinh - SN 1966 , trú tổ 36 Thanh_Khê_Đông , có 21 lao_động .
5 . Tàu ĐN a 90093 TS của ông Ngô_Văn_Chiếu - SN 1931 , trú tại tổ 4 Xuân_Hà , có 17 lao_động .
6 . Tàu ĐN a 90321 TS của ông Trần_Văn YÁ - SN 1943 , trú tổ 32 Thanh_Khê_Đông , có 21 lao_động .
7 . Tàu ĐN a 90154 TS của bà Nguyễn_Thị_Phượng - SN 1965 , trú tổ 36 Thanh_Khê_Đông , có 18 lao_động .
3 tàu vẫn còn mất liên_lạc với 58 lao_động :
1 . Tàu ĐN a 6126 TS của Đỗ_Văn_Đường - SN 1960 , trú tổ 35 Thanh_Khê_Đông , có 19 lao_động .
2 . Tàu ĐN a 6018 TS của ông Bùi_Văn_Vịnh - SN 1956 , trú tổ 8 Xuân_Hà , có 20 lao_động .
3 . Tàu ĐN a 90247TS của ông Nguyễn_Văn_Ánh - SN 1957 , trú tổ 39 Xuân_Hà , có 19 lao_động .
Thống_kê của UBQG tìm_kiếm - cứu nạn , đến cuối ngày 21-5 đã xác_định được 22 tàu chìm với 315 ngư_dân bị nạn . Trong đó , Đà_Nẵng có 7 tàu chìm và 3 tàu mất_tích với 198 lao_động ; Quảng_Ngãi có 5 tàu mất_tích và chìm với 43 lao_động ; Quảng_Nam có 3 tàu mất_tích với 39 lao_động .
Theo một nguồn tin , các ngư_dân Việt_Nam được các tàu Trung_Quốc cứu sống đã nêu cao khẩu_hiệu với dòng chữ " Cảm_ơn Chính_phủ Trung_Quốc , cảm_ơn các thuỷ_thủ Trung_Quốc " . Các tàu Việt_Nam sau khi được giúp_đỡ về lương_thực , thực_phẩm , nước uống và nhiên_liệu đã cùng tàu của Trung_Quốc tiếp_tục tìm_kiếm người bị nạn . Ông Đỗ_Hữu_Tâm , Chủ tàu DNA 90019 ở tổ 3 , phường Xuân_Hà , quận Thanh_Khê ( thành_phố Đà_Nẵng ) cảm_động nói : " Tôi thay_mặt nhân_dân quận Thanh_Khê cảm_ơn các tàu cứu_hộ của nước bạn " .
Danh_sách 15 tàu đánh_cá và 330 ngư_dân Việt_Nam bị nạn được các tàu Trung_Quốc cứu_hộ gồm :
1.Tàu QNG9145TS : số ngư_dân đã được cứu là 24 người
2.Tàu DNA90189 : số ngư_dân đã được cứu là 32 người , số thi_thể trên tàu : 8
3.Tàu DNA90345 : số ngư_dân đã được cứu là 32 người , số thi_thể trên tàu : 5
4.Tàu DNA90299 : số ngư_dân đã được cứu là 33 người , số thi_thể trên tàu : 5
5.Tàu DNA90062 : số ngư_dân đã được cứu là 26 người , số thi_thể trên tàu : 1
6.Tàu DNA90152 : số ngư_dân đã được cứu là 24 người
7.Tàu DNA90046 : số ngư_dân đã được cứu là 22 người
8.Tàu DNA90244 : số ngư_dân đã được cứu là 20 người
9.Tàu DNA90125 : số ngư_dân đã được cứu là 22 người
10.Tàu DNA90354 : số ngư_dân đã được cứu là 28 người
11.Tàu QNG92288TS : số ngư_dân đã được cứu là 15 người ( trong đó có 3 người bị_thương )
12.Tàu QNG99279TS : số ngư_dân đã được cứu là 18 người ( trong đó có 3 người bị_thương )
13.Tàu QNG2160TS : số ngư_dân đã được cứu là 18 người , số thi_thể trên tàu : 2
14.Tàu QNG7350TS : số ngư_dân đã được cứu là 8 người15.Tàu QNG7139TS : số ngư_dân đã được cứu là 8 người
Theo VTV , VOV

﻿ “ Sẽ đổi_mới quy_trình thi_cử , đánh_giá SV ở các lớp tại_chức ”
Mặc_dù trong các quy_chế , quy_định về đào_tạo tại_chức trước_đây chưa có hình_thức xử_lý đối_với việc nhờ người học thuê nhưng theo tôi , đây cũng giống như trường_hợp thi hộ , thi kèm và nếu phát_hiện sẽ buộc thôi học những trường_hợp như_vậy .
Hiện_nay , Bộ GD - ĐT đang soạn_thảo quy_chế tuyển_sinh vừa học , vừa làm , thay cho những quy_chế trước_đây ( dự_kiến sẽ ban_hành vào năm nay ) trong đó sẽ tập_trung vào việc cải_tiến khâu quản_lý , học_hành , thi_cử , đánh_giá học_sinh - sinh_viên với những quy_trình mới để tiến tới nâng chất_lượng đào_tạo của hệ không chính_quy hòa_nhập với hệ chính_quy .
Cụ_thể : tăng_cường khâu kiểm_tra , thanh_tra đột_xuất để phát_hiện những trường_hợp gian_lận ; tổ_chức thi theo hình_thức trắc_nghiệm khách_quan để khâu chấm thi đảm_bảo độ_chính_xác , khách_quan .
Đặc_biệt sẽ tách việc dạy_học và đánh_giá sinh_viên thành hai khâu riêng_biệt ; không để người dạy tự ra đề kiểm_tra kiến_thức của người học mà sẽ có tiêu_chí đánh_giá riêng . Tiêu_chí này buộc giáo_viên phải dạy đủ chương_trình và người học phải học đủ chương_trình mới có_thể thi đỗ .
Như_vậy , người học không_thể nhờ người khác học hộ mà cũng có_thể đỗ được trong các kỳ thi . Bên_cạnh đó , cũng sẽ xem_xét đưa vào quy_chế những hình_thức kỷ_luật thích_đáng đối_với hình_thức nhờ học thuê .
Theo Thanh_Niên

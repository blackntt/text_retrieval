﻿ Nokia “ mở ” Google_Talk trên dòng Tablet
Hãng điện_thoại Phần_Lan sẽ cài_đặt ứng_dụng Google_Talk vào các thiết_bị cầm tay có khả_năng duyệt web của mình , cho_phép đàm_thoại và trao_đổi tin nhắn IM . Đây là động_thái nhằm giúp hãng tìm_kiếm Google “ thâm_nhập ” vào các thiết_bị liên_lạc di_động .
Theo một nguồn tin thân_cận với Nokia , ngày 13/5 tới , hãng điện_thoại sẽ tung ra một phiên_bản nâng_cấp của dòng Nokia 770 Internet_Tablet , hỗ_trợ Wi-Fi và đã được tải dịch_vụ Google_Talk của hãng tìm_kiếm số_một .
Người dùng thiết_bị hỗ_trợ Google_Talk sẽ có_thể gọi điện hoặc nói_chuyện trực_tiếp vào máy không dây , như_là điện_thoại_di_động hoặc thậm_chí còn đàm_thoại với những người dùng đã cài_đặt phần_mềm Google_Talk trên máy_tính hay các thiết_bị cầm tay khác . Tuy_nhiên , thiết_bị này không_thể gọi điện cho các điện_thoại bàn .
Thiết_bị hỗ_trợ Google_Talk này là phiên_bản nâng_cấp dòng máy Nokia 770 Internet_Tablet được tung ra từ tháng 12/2005 . Đây là loại máy đầu_tiên của hãng này được xuất_xưởng không phải là điện_thoại_di_động . Kích_thước lớn hơn một_chút so với PDA , màn_hình rộng có độ_phân_giải cao tạo sự thoải_mái khi duyệt web . Dòng máy này cũng có_thể chơi nhạc , xem video . Tuy_nhiên , theo Nokia , hiện_tại , thiết_bị này chỉ thu_hút được một thị_trường chuyên_biệt .
Nokia dự_định sẽ tung ra trên thị_trường toàn_cầu với giá khoảng 390 USD .
S.Hương Theo AP

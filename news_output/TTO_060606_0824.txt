﻿ World_Cup và nỗi lo thất_thoát tiền tỉ !
Lo_ngại đó là có cơ_sở , bởi BBC cho_biết các trận đấu tại Đức sắp tới sẽ được họ truyền_hình trực_tiếp trên mạng Internet , nhằm " giúp mọi người có_thể vừa làm_việc vừa nắm được diễn_biến mới nhất của trận đấu " .
Các nhà_phân_tích tại Công_ty tư_vấn luật Brabners_Chaffe đã cảnh_báo rằng việc xem THTT trong giờ làm_việc như_thế sẽ đem lại nhiều thiệt_hại cho nền kinh_tế Anh . Nghiên_cứu của công_ty này đã chỉ ra rằng nếu một_nửa giới công_chức Anh bỏ ra một giờ để xem các trận đấu trên mạng , nền kinh_tế tại đây sẽ thiệt_hại khoảng 4 tỉ bảng ( khoảng 5,8 tỉ euro ) .
Thiệt_hại này còn chưa kể đến việc nhân_viên xin nghỉ ốm một_số ngày nhưng thật_ra để xem bóng_đá , dù 80% giới chủ khẳng_định sẽ không chấp_nhận điều đó trong mùa World_Cup năm nay .
Xem_ra vừa đảm_bảo năng_suất làm_việc vừa không bỏ sót một trận đấu nào của đội nhà quả_là vấn_đề nan_giải với giới công_chức nước Anh .
P.QU ỲNH

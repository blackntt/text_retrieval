﻿ Gót chân A-sin
Đừng nhìn họ “ vào trong phong_nhã ra ngoài hào_hoa ” mà lầm tưởng . Bạn sẽ vừa ngạc_nhiên vừa thấy thú_vị khi biết được những điểm yếu của cánh mày_râu thế_giới ngày_nay .
- 70% đàn_ông Costa_Rica có vấn_đề với việc " ra_quân " sớm . Quá_trình " lâm_trận " của họ chỉ vỏn_vẹn 2-3 phút .
- 40% đàn_ông Ý độ tuổi “ băm ” vẫn sống cùng cha_mẹ .
- 57% đàn_ông Anh dính_líu đến trò đỏ_đen .
- 70% đàn_ông Việt_Nam thường_xuyên hút thuốc_lá ( thống_kê của WHO )
- Đàn_ông Thái_Lan hay nói_dối , cờ_bạc và uống rượu như nước_lã . Đây có_thể là lý_do phụ_nữ Thái thích chồng Tây hơn ?
- 37% đàn_ông Trung_Quốc hay_ho và khạc nhổ bừa_bãi nơi công_cộng .
- 33% đàn_ông Anh luôn mang " áo_mưa " khi đi nghỉ so với 50% lại mang … băng keo cá_nhân .
- Ở Ấn Độ , mọi nơi công_cộng đều là … nhà_vệ_sinh . Thế nên mới có chuyện đàn_ông New_Delhi sẽ bị phạt 1.000 rupees ( khoảng 22 đô_la Mỹ ) nếu bị bắt quả_tang " tiểu_đường " .
- 96% phụ_nữ Nhật phàn_nàn người đàn_ông của mình nghiện nói_chuyện sex qua điện_thoại .
Theo Thế_Giới_Văn_Hóa

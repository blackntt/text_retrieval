﻿ Maggie Q . - ngôi_sao gốc Việt đóng cặp với Tom_Cruise
Tháng 4-2001 , Maggie Q lần đầu_tiên lên bìa tạp_chí Times_Hồng_Kông . Lúc đó cô được coi là một phát_hiện mới trong giới người_mẫu .
Đến tháng 10 , Maggie Q tới Mỹ tham_gia phim Manhattan_Midnight . Sau_này , được Thành_Long chú_ý tới , cô đã diễn rất nhiều phim hành_động .
Nhưng sánh_vai cùng Tom_Cruise trong “ Nhiệm_vụ bất_khả_thi 3 ” mới thực_sự là một bước đột_phá trong sự_nghiệp điện_ảnh của cô . “ Nhiệm_vụ bất_khả_thi 3 ” sẽ ra_mắt vào 5-5 tới .
Đạo_diễn “ Nhiệm_vụ bất_khả_thi 3 ” JJ . Abrams và Tom_Cruise đã bao_giờ xem các phim chị đóng chưa ?
- Tôi không rõ họ có biết các phim của tôi không . Nhưng ở Hollywood tôi nhận ra một điều khá thú_vị . Đó là rất nhiều người không hề quan_tâm trước đó anh đã làm những gì , họ chỉ chú_ý đến bây_giờ anh có_thể làm được gì .
Khi tôi đi thử vai , họ không_bao_giờ nói : “ ồ , chúng_tôi đã xem phim chị diễn , rất tuyệt_vời ” , mà chỉ nói : “ Bây_giờ chúng_tôi yêu_cầu như_thế_này , chị có_thể làm được không ? ” .
Chị đã nhận vai trong “ Nhiệm_vụ bất_khả_thi 3 ” như_thế_nào ?
- Ban_đầu tôi gửi cho họ một đoạn băng , nhưng chờ mấy tuần liền mà không thấy động_tĩnh gì , tôi tưởng mình hết hi_vọng rồi . Không ngờ sau mấy tuần du_lịch từ châu Âu về , vừa đặt_chân đến Hong_Kong tôi đã nhận được điện_thoại yêu_cầu tôi sang Los_Angeles ngay .
Thế_là mấy tiếng sau đó tôi lập_tức lên máy_bay tới Mỹ . Họ cho tôi thử 3 cảnh , và cuối_cùng tôi đã được chọn .
Trời_đất , chị đã bay liền một_mạch 24 tiếng sang Los_Angeles ?
- Khi tới nơi tôi mệt bã ra , lại bị sốt nhẹ , nhưng vẫn vào thử vai . May_mà tôi vẫn cố_gắng được đến phút cuối .
Trong phim chị đóng vai gì ?
- Tôi vào_vai một thành_viên của đội đặc_công , cùng_với Ving_Rhames và Jonathan_Rhys - Meyers .
Vậy đây là dịp để chị thể_hiện tài_năng võ_nghệ của mình ...
- Đúng vậy . Hợp_tác với đạo_diễn JJ , tôi lại còn phát_huy được nhiều hơn_nữa . Đợi đến khi phim ra_mắt khán_giả , mọi người sẽ thấy không_chỉ riêng những màn đấu_võ hấp_dẫn , mà bản_thân vai_diễn cũng vô_cùng cuốn_hút . Mỗi nhân_vật đều nổi_bật , mới_mẻ . Tóm_lại diễn_viên trông rất chuyên_nghiệp .
Chị đã xem hai phần trước chưa ?
- Tất_nhiên tôi đã xem . Tôi cho rằng phần 1 , đội đặc_công đoàn_kết , phối_hợp nhịp_nhàng hơn , phần 2 hơi lạc_đề . Phần 3 này sẽ quay lại phong_cách của phần 1 , nhất_định sẽ làm khán_giả hồi_hộp đến phút chót .
Làm_việc với Tom_Cruise , chị thấy thế_nào ?
- Anh_ấy rất tuyệt_vời . Những lời đồn_đại về Tom đều không chính_xác , mọi người chẳng hiểu gì về anh_cả . Đóng phim cùng anh , tôi không hề thấy một tật xấu nào của Tom . Anh quả_thực rất tốt , đối_xử bình_đẳng với tất_cả mọi người . Không_thể tìm đâu ra một bạn diễn hoàn_hảo hơn được nữa .
Trong phim , chị và Tom có cảnh mặn_nồng nào không ?
- Không hề . Tôi chẳng có cơ_hội nào cả . Nhưng trong phim , tôi và Jonathan_Rhys - Meyers cũng có chút tình_ý với nhau . Nhưng nói_chung , quan_hệ giữa các thành_viên trong đội đặc_công thân_thiết như anh_em một nhà vậy
Bây_giờ chị định sẽ tiếp_tục đóng phim lâu_dài ở Mỹ , hay_là quay lại Trung_Quốc hoạt_động giống như Thành_Long ?
- Thực_ra tôi muốn thử sức ở cả hai nơi . Trước_mắt cứ làm_việc cho tốt đã , còn làm ở đâu cũng không quan_trọng lắm .
Theo Tiền_Phong - Xinhuanet

﻿ Phim mới của Choi_Ji_Woo khởi quay
Trong_Yeonriji , Choi_Ji_Woo vào_vai Han_Hye_Won - một phụ_nữ luôn cố_gắng sống mạnh_mẽ và bảo_vệ tình_yêu của mình đến_cùng dù đang đối_mặt với cái chết . Còn vai_diễn của Jo_Han_Sung là Lee_Min_Soo , giám_đốc một công_ty game , người được nhiều cô_gái theo_đuổi nhưng chưa từng nghiêm_túc trong tình_yêu . Tuy_nhiên , khi vô_tình gặp Hye_Won , Min_Soo đã hoàn_toàn thay_đổi quan_niệm tình_yêu của mình .
Ngoài đời , Jo_Han_Sun nhỏ hơn Choi_Ji_Woo đến 6 tuổi và thuộc lứa diễn_viên đàn_em của cô . Tuy_nhiên , khán_giả hi_vọng cả hai sẽ hợp_thành một cặp đẹp_đôi trên màn_ảnh . Năm_ngoái , Jo_Han_Sun từng gây ấn_tượng trên màn_ảnh rộng với bộ phim Sự lãng_mạn của riêng họ , đóng chung với Kang_Dong_Won và Lee_Chung_Ah .
Yeonriji do hai hãng phim Taewon_Entertainment và White_Lee_Entertainment cùng hợp_tác sản_xuất , dự_kiến sẽ kết_thúc các cảnh quay vào tháng 10 và bắt_đầu trình_chiếu vào năm tới tại Nhật_Bản và Hàn_Quốc .
T.MINH

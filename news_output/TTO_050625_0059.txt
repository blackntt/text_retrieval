﻿ Thủ_tướng Phan_Văn_Khải thăm ĐH Harvard và Viện MIT
Thủ_tướng Phan_Văn_Khải chụp hình lưu_niệm tại Đại_học Harvard
TT ( Boston ) - 10g sáng 24-6 giờ Boston ( tối 24-6 giờ VN ) , Thủ_tướng Phan_Văn_Khải đến thăm Trường đại_học ( ĐH ) Harvard , một trong những trường lớn nhất thế_giới .
Chủ_tịch ĐH Harvard_Lawrence H . Summers và Thủ_tướng Phan_Văn_Khải đã có thời_gian trao_đổi về chương_trình giảng_dạy cho sinh_viên , nghiên_cứu_sinh VN trong thời_gian tới .
Thủ_tướng đã đến tượng_đài John_Harvard chụp hình lưu_niệm , đặt tay lên chân tượng John_Harvard như một_cách để chúc may_mắn cho sinh_viên trong trường .
Harvard là trường đại_học có mối quan_hệ gắn_bó với VN từ nhiều năm qua . Chỉ từ năm 1996-2003 , với sự tài_trợ của Quĩ_Fulbright , Trường quản_lý_nhà_nước Kennedy thuộc ĐH Harvard đã thực_hiện dự_án giảng_dạy kinh_tế Fulbright giai_đoạn 1 với khoản kinh_phí từ 1 - 1,2 triệu USD/năm . Dự_án này vào giai_đoạn 2 ( 2004-2008 ) cũng vừa được ký_kết với khoản viện_trợ không hoàn lại là 7,5 triệu USD .
Rời ĐH Harvard , Thủ_tướng đến thăm Viện_Công nghệ Massachusetts ( MIT ) , một trong những trường nổi_tiếng về đào_tạo sinh_viên trong lĩnh_vực khoa_học , kỹ_thuật . MIT hiện có khoảng 10.000 sinh_viên ĐH và cao_học .
Buổi chiều , Thủ_tướng tham_dự hội_thảo có chủ_đề “ Làm thế_nào để tăng_cường khả_năng cạnh_tranh trong môi_trường toàn_cầu hiện_nay ” ; gặp_gỡ bàn tròn với đại_diện ĐH Harvard , Viện MIT , Quĩ giáo_dục VN , Viện_Yenching , Quĩ_Hanry_Luce , Viện_Open_Society_Institute , Tổ_chức tài_trợ Atlantic_Philanthropies , Ngân_hàng Thế_giới ... để bàn về xây_dựng trường ĐH tầm_cỡ quốc_tế tại VN .
ĐOAN_TRANG ( từ Boston )

﻿ 9 mẹo làm_đẹp mùa_hè của các sao
Bạn có biết đánh mắt làm_sao cho đẹp , tô son làm_sao cho gợi_cảm , buộc tóc làm_sao cho duyên_dáng ... Hãy cập_nhật các xu_hướng trang_điểm mới nhất trong hè này của các sao để cho mình quyến_rũ hơn và không bị lạc_hậu .
1 . Băng đô vải với họa_tiết vui_mắt :
Nên : Chọn những màu_sắc tươi_sáng để hợp với mùa_hè và " diện " quần_áo sao cho cùng tông màu với băng đô . Những chiếc băng đô này nếu kết_hợp đúng cách sẽ mang lại cho bạn vẻ đẹp hết_sức tinh_tế . Không nên : Đừng đeo băng đô quá xa về phía đằng sau , lúc đấy trông bạn chẳng khác_gì một cô bé học_sinh .
Beyonce_Knowles - Naomi_Watts
2 . Mắt khói huyền_ảo :
Nên : Dùng bút kẻ mắt nước_màu ghi đậm để viền xung_quanh mắt , riêng đuôi mắt sẽ nhấn bằng mascara hoặc chì mắt cùng màu .
Không nên : Viền mắt quá xa mi mắt , trông bạn sẽ chẳng khác_nào vừa khóc xong .
Nicole_Richie - Lindsay_Lohan
3 . Tất_cả đều màu đồng :
Nên : Làm nổi_bật cả khuôn_mặt , má , mắt và môi bằng một gam màu đồng hợp với khuôn_mặt bạn . Trông bạn sẽ rực_rỡ như ánh mặt_trời .
Không nên : Lạm_dụng màu đồng . Nếu_không trông bạn cứ như bị cháy nắng .
Mariah_Carey - Jessica_Simpson
4 . Tóc búi mượt :
Nên : Rẽ ngôi cho tóc và chải mượt rồi búi tóc thấp phía dưới đầu .
Không nên : Bôi quá nhiều chất dưỡng tóc làm mượt . Trông bạn cứ như vừa đi ép tóc về .
Rosario_Dawson - Kate_Bosworth
5 . Kẻ mắt nước :
Nên : Kẻ một đường viền mắt rõ bằng chì nước đến đuôi mắt thì vẽ hơi xếch lên trên . Bạn sẽ có một cái nhìn " chết người " hơn .
Không nên : Dùng son màu sẫm hoặc đánh má sáng quá . Nên nhớ đôi mắt lúc này là điểm nhấn của cả khuôn_mặt .
Rebecca_Romijn - Penelope_Cruz
6 . Tóc rối :
Nên : Hãy để tóc khô tự_nhiên sau đó dùng nước xịt tóc để đánh rối . Trông bạn sẽ vô_cùng sexy . Đối_với tóc thẳng : đánh rối ngay từ lúc tóc ướt đồng_thời sẽ tạo sóng cho mái_tóc .
Không nên : Đánh rối quá kỹ . Trông bạn chẳng khác_nào vừa bị điện giật .
Evangeline_Lilly - Jennifer_Aniston
7 . Mắt không màu :
Nên : Chọn màu mắt cùng_với màu_da của bạn . Nếu da bạn màu nâu sẫm , hãy chọn mắt màu hồng nhạt hoặc màu be . Cuối_cùng là chải mascara .
Không nên : Để mặt mộc chỉ vì mắt màu nhẹ . Trông bạn cứ như_không trang_điểm gì . Ít_nhất cũng phải đánh má màu hồng sẫm .
Ellen_Pompeo - Halle_Berry
8 . Son màu hồng nhạt :
Nên : Chọn son nền và son bóng màu nhạt
Không nên : Kẻ mắt màu đậm . Đôi môi hồng xinh_xắn đã là điểm nổi_bật rồi . Trên khuôn_mặt không cần quá nhiều điểm nhấn .
Chloe_Sevigny - Nicole_Kidman
9 . Tóc búi rối :
Nên : Đánh rối tóc như bình_thường và buộc tóc lên thật tự_nhiên .
Không nên : Cột tóc lên giữa đỉnh đầu .
Mandy_Moore - Eva_Longoria MMy Theo People

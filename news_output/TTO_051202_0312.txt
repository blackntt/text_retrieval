﻿ Việc_làm cho SV mới ra trường , SV làm thêm dịp tết
Làm_việc tại TP.HCM và các tỉnh miền Đông_Nam bộ , lương khởi_điểm 2 triệu đồng / tháng ) ; 10 kế_toán ( ĐH chuyên_ngành , lương 3 triệu đồng / tháng ) ; 20 NV tuyển_dụng , phụ_trách nhân_sự , NV văn_phòng ( tốt_nghiệp các ngành xã_hội , ngoại_ngữ , trình_độ CĐ trở_lên ) ;
10 kỹ_sư cần_trục tàu_biển ( tốt_nghiệp ĐH Hàng_hải , lương khởi_điểm 2-2 , 5 triệu đồng / tháng ) ; 20 NV kỹ_thuật ngành hàn , điện công_nghiệp ( trung_cấp , CĐ trở_lên ) . Tất_cả các vị_trí tuyển không cần kinh_nghiệm nhưng phải có kỹ_năng tiếng Anh ( giao_tiếp tốt ) và vi_tính văn_phòng , làm_việc tại TP.HCM .
Ngoài_ra , trung_tâm đang có nhu_cầu tuyển gấp 150 SV bán hàng cho các siêu_thị , trung_tâm thương_mại ở TP.HCM ( từ nay đến tết , 40.000-65.000 đồng/ngày ) .
T . V .

﻿ Một ngày , 44 người tử_vong vì tai_nạn giao_thông
Trong đó , đường_bộ xảy_ra 53 vụ , chết 43 người , bị_thương 42 người ; đường_thủy xảy_ra hai vụ , làm chết một người . Các địa_phương xảy_ra nhiều vụ tai_nạn giao_thông đặc_biệt : Hà_Nội 6 vụ , 2 người chết , 11 người bị_thương ; Bình_Dương 5 vụ , 6 người chết , 4 người bị_thương ; Bình_Thuận 4 vụ , 2 người chết , 2 người bị_thương ; Lạng_Sơn 3 vụ , 1 người chết , 6 người bị_thương ; TP.HCM 3 vụ , 3 người chết , 3 người bị_thương .
Cũng trong ngày 20-2 , lực_lượng cảnh_sát giao_thông đã xử_lý 4.767 trường_hợp vi_phạm trật_tự an_toàn giao_thông , thu nộp ngân_sách 526,1 triệu đồng , tạm giữ 23 ôtô , 1.479 môtô và 24 phương_tiện khác .
TTXVN

﻿ Italy có tổng_thống mới
Quốc_hội Italy hôm_nay đã bầu ông Giorgio_Napolitano , thượng_nghị_sĩ 80 tuổi của đảng Dân_chủ cánh tả , làm tổng_thống mới , mở_đường cho chính_phủ trung tả do Romano_Prodi lãnh_đạo lên cầm_quyền trong vài ngày tới .
Là nghị_sĩ suốt đời của đảng Dân_chủ cánh tả , Giorgio_Napolitano được chọn giữ chức_vụ tổng_thống trong vòng bỏ_phiếu thứ_tư với sự tham_gia của thành_viên hai viện quốc_hội và đại_diện các vùng .
Chủ_tịch hạ_viện Fausto_Bertinotti , đã tuyên_bố Napolitano chiến_thắng sau khi kết_quả kiểm phiếu cho thấy , ông giành được 543 trên tổng_số 1.010 phiếu - cao hơn nhiều so với số phiếu tối_thiểu để có_thể thắng_cử .
Với kết_quả này , Romano_Prodi đang tiến gần hơn tới việc thành_lập chính_phủ mới sau thắng_lợi sít_sao của liên_minh trung tả trong cuộc bầu_cử tháng trước .
Tổng_thống là chức_vụ hầu_như chỉ mang tính tượng_trưng ở Italy , nhưng lại có quyền chỉ_định người thành_lập chính_phủ và giải_tán quốc_hội . Tổng_thống sắp mãn_nhiệm Carlo_Azeglio_Ciampi đã từ_chối thực_hiện nghĩa_vụ chỉ_định thủ_tướng mới và muốn người kế_nhiệm ông làm_việc này .
Prodi cho rằng Napolitano - từng là đảng_viên cộng_sản - sẽ nhận được sự ủng_hộ của tất_cả người_dân Italy , ngay cả khi liên_minh trung hữu bỏ_phiếu chống lại ông trong cuộc bầu_cử .
Cuộc bầu_cử tổng_thống diễn ra từ ngày 8/5 , nhưng không ứng_viên nào giành đủ hai_phần_ba số phiếu cần_thiết trong 3 vòng đầu . Trong lần bỏ_phiếu thứ_tư , một ứng_viên chỉ cần giành được đa_số_tuyệt_đối sẽ được tuyên_bố thắng cuộc . Với sự ủng_hộ của liên_minh trung tả đối_với Napolitano , chiến_thắng của ông đã được dự_đoán trước .
Napolitano đã trải qua nhiều vị_trí cấp cao trong sự_nghiệp chính_trị . Ông từng đảm_nhận cương_vị chủ_tịch quốc_hội trong thời_kỳ 1992-1994 . Từ năm 1996 tới 1998 ông làm Bộ_trưởng Nội_vụ trong chính_phủ đầu_tiên của Prodi .
Tân tổng_thống là chính_trị_gia nổi_tiếng với quan_điểm ôn_hoà . Ông cũng là một trong những người ủng_hộ nhiệt_tình cuộc cải_cách dẫn đến việc đảng Cộng_sản Italy đổi tên thành đảng Dân_chủ cánh tả và từ_bỏ biểu_tượng búa liềm .
Phát_biểu trước các phóng_viên sáng nay , Prodi cho_biết ông hy_vọng sẽ nhận được lời mời thành_lập chính_phủ mới trong khoảng thời_gian từ chủ_nhật tuần này tới thứ_ba tuần tới , đồng_thời dự_tính cuộc bỏ_phiếu tín_nhiệm chính_phủ mới tại quốc_hội có_thể diễn ra_vào 23/5 .
Napolitano cũng tỏ ra hết_sức lạc_quan trước vòng bỏ_phiếu hôm_nay , khẳng_định rằng với tư_cách là tổng_thống ông sẽ đứng trên tư_tưởng của đảng để phục_vụ lợi_ích của nhân_dân .
Theo truyền_thống tại Italy , người đứng đầu nhà_nước thường là nhân_vật có khả_năng đoàn_kết đất_nước .
Theo Việt_Linh_Vnexpress / AP

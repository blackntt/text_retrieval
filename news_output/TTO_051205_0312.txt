﻿ Giải_đáp thắc_mắc về đăng_ký hộ_khẩu
- Trước_hết , chị và người chồng hờ ( đã đăng_ký kết_hôn ) phải làm thủ_tục xin ly_hôn , sau đó đăng_ký kết_hôn với người chồng cũ rồi bảo_lãnh nhập HK cho anh_ấy .
Trước_đây tôi ở Hà_Nội , ĐKHK cùng_với bố_mẹ tôi . Năm 1994 , bố_mẹ tôi được điều vào TP.HCM công_tác , chuyển HK vào theo . Tôi có vợ và cùng theo bố_mẹ vào TP.HCM sinh_sống chung một nhà .
Vậy tôi có được nhập HK vào nhà của bố_mẹ tôi không ? ( TRẦN_ANH_TÚ , 108H1 , Chu_Văn_An , P.26 , Q.Bình Thạnh )
- Theo qui_định mới thì anh không được ĐKHK vào nhà của bố_mẹ anh vì anh đã lập gia_đình .
Tôi và nhà hàng_xóm tranh_chấp nhau một lối đi , đúng_ra là cánh cửa_nhà tôi mỗi khi mở ra có nới qua một phần cửa của nhà bên_cạnh , nhưng không ảnh_hưởng đến quyền_sở_hữu tài_sản của hai bên .
Gia_đình bên kia đã có HK còn gia_đình tôi thuộc diện KT3 ( năm năm ) . Việc tranh_chấp đã ra hòa_giải nhiều lần ở UBND phường nhưng không thành .
Nay tôi xác_nhận tình_trạng nhà để ĐKHK thì UBND phường ghi “ nhà có đơn tranh_chấp gửi UBND phường , hòa_giải nhiều lần nhưng không thành ” . Vậy tôi phải làm_sao ? ( TRẦN_THỊ_HUYỀN , muangau2004 @ ... )
- Chị phải liên_hệ với nhà bên_cạnh để hai bên ngồi lại với nhau hòa_giải theo hướng “ đôi bên cùng có lợi ” , giải_quyết dứt_điểm việc tranh_chấp . Sau khi đã thống_nhất không còn tranh_chấp thì hai bên cùng làm tờ cam_kết nói rõ không còn tranh_chấp với nhau nữa và gửi lên UBND phường để xác_nhận . Sau đó chị xin xác_nhận lại tình_trạng nhà để được ĐKHK .
HOÀNG_KHƯƠNG thực_hiện

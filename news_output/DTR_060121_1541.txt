﻿ SEA Games 23 , ký_ức không_thể_nào quên ...
Ngẫm lại những khoảnh_khắc tưởng như vừa đến hôm_qua , ai chẳng nhớ phút_giây được chứng_kiến lá_cờ đỏ Việt_Nam kéo lên tại đấu_trường SEA Games 23 , những lúc được nghe bài Quốc_ca hùng_tráng và chợt nhìn thấy những giọt nước_mắt lăn dài trên má ai ...
Nỗi đau và hạnh_phúc
" Tôi vô_cùng sung_sướng " , " Tôi vô_cùng tự_hào " , " Em chẳng biết nói sao ... " - các phóng_viên nhà ta biết rất rõ , nghe rất nhiều những điệp_khúc quen_thuộc thường được các nhà VĐ thốt lên giữa tiếng thở đứt_quãng vì ... mệt .
Cảm_động hơn khi biết những chuyện đằng sau vầng hào_quang ấy . Ngày xuất_trận đầu_tiên , Bùi_Thị_Nhung và Ngọc_Tâm đã từng ôm nhau trong nước_mắt . Nhung cực_kỳ xuất_sắc nhưng cô phải biết_ơn Tâm rất nhiều .
Cô bạn_thân đã khiến Chaipech ( Thái_Lan ) - kỳ_phùng_địch_thủ của Nhung - khớp đến mức không_thể vượt qua mức xà 1,86 m để Nhung chiến_thắng ở mức xà 1,89 m .
Chiến_thắng kép của cả Nhung và Tâm là điều không ai dám nghĩ vậy_mà nó lại tới . Không quá bất_ngờ nhưng thật kỳ_diệu . Làm_sao không ôm nhau khóc kia chứ ! Sau những ánh đèn flash , Bùi_Thị_Nhung lại ngồi_bó_gối nhỏ_xíu trên khán_đài .
Tâm ngồi cạnh khe_khẽ hát : " Dẫu có khó_khăn thì bạn ơi hãy tin rằng . Dẫu có đớn_đau thì bạn ơi hãy giữ một niềm_tin . Niềm_tin chiến_thắng sẽ đưa ta đến bến_bờ vui . Niềm_tin chiến_thắng nối con tim yêu_thương mọi người . Niềm_tin chiến_thắng sẽ đưa ta đến giữa cuộc_đời . Niềm_tin chiến_thắng luôn mãi trong tim mỗi chúng_ta " .
Rồi chuyện Bông và Hằng . Với_Đỗ_Thị_Bông , nhà_vô_địch 800m , cảm_giác " ta đã thắng " chỉ tồn_tại trong vài giây , bởi đau_đớn vì chấn_thương và vì cơn đói hành_hạ . Trước khi vào_cuộc chiến_đấu , Bông nôn toàn ra nước . Thế_mà vẫn về nhất mới kỳ_diệu . Chạm đích , Bông vật ra , khuôn_mặt tím_tái . Bông bị rách cơ , đau khủng_khiếp , đau đến độ nước_mắt không chảy ra nổi .
Cho_dù đau_đớn do chấn_thương
Đỗ_Thị_Bông vẫn nỗ_lực hết_mình cán đích
Đêm đó , Bông chỉ nhắm_mắt được 3 tiếng . Hôm_sau , Bông phụ_giúp Trương_Thanh_Hằng bứt_phá trong cự_ly 1.500m , Bông cũng giành HCB và vết rách cơ càng nặng vì biến_tốc 100m cuối_cùng . Đau_đớn và hạnh_phúc .
Hạnh_phúc vì chiến_thắng cho mình và đồng_đội . Hạnh_phúc dung_dị như Trương_Thanh_Sang - nhà_vô_địch TDDC . Nhà nghèo , đông anh_em , Sang dấn_thân vào con đường thể_thao với mong_ước đổi_đời . Nhưng vinh_quang cũng phải đánh_đổi bằng mồ_hôi và nước_mắt .
HCV_SEA Games 23 - tấm HCV quốc_tế cuối_cùng trong sự_nghiệp của Sang bởi " có_lẽ đây là lần cuối_cùng mình tham_dự SEA Games " - đã đem lại cho gia_đình Sang một căn nhà do Sở TDTT_TP . HCM bán với giá ưu_đãi . Với chàng_trai này , chiến_thắng không phải để_dành riêng cho bản_thân mà cho cuộc_sống của cả gia_đình .
Và không_chỉ Thanh_Sang , còn rất nhiều VĐV khác của chúng_ta giành chiến_thắng để mang về cho cha_mẹ nghèo ở quê_nhà một khoản tiền để nuôi các em ăn_học .
Cuộc_đời đâu mãi chỉ niềm_vui
Cuộc_chơi nào cũng có chiến_thắng và thất_bại . Người_ta nói : " Thắng chỉ có một kiểu còn thua thì nhiều kiểu " . Thua vì mình không giỏi bằng đối_thủ là một nhẽ , thua vì trọng_tài mới đắng_cay . Lý_Đức miệng cười mà lòng như xát muối .
Những người Việt_Nam chứng_kiến bài biểu_diễn trên cả tuyệt_vời của anh lòng cũng_như xát muối . Nguyễn_Thị_Hằng - nữ đô_vật ôm lấy đồng_đội tức_tưởi như trẻ lên ba . Nguyễn_Đức_Thắng chết sững như Từ_Hải vì đang dẫn_điểm mà bị một trong những trọng_tài vật giỏi nhất châu Á ép phải thua .
Lý_Đức không thua đối_thủ khác
nhưng thất_bại trước trọng_tài
Nguyễn_Thị_Hòa lọt_thỏm giữa đường_phố Manila ồn_ào và đông_đúc . Chị đang sải nhanh những bước cuối_cùng để về đích . Tấm HCB đã cầm_chắc trong tay nhưng trọng_tài không công_nhận mặc_dù chúng_ta không hề phạm_luật . Hòa không khóc ngay lúc đó nhưng gương_mặt thất_thần , đau_đớn . Trở_về khách_sạn , Hòa mới thổn_thức !
Có ai trong chúng_tôi không nhớ những giọt nước_mắt của Nguyễn_Đình_Cương khi BTC không chấp_nhận thành_tích về nhất ở cự_ly 1.500m , vì đồng_đội Lê_Văn_Dương đã phạm_luật trên đường chạy .
Lần này trọng_tài đã đúng , chúng_ta thua do không nắm chắc luật . Về đích với thành_tích 3 phút 49 giây , tưởng như Đình_Cương đã mang lại chiếc HCV thứ 8 cho đội_tuyển điền_kinh Việt_Nam trong buổi chiều thi_đấu cuối_cùng . Thế_nhưng ... chàng_trai Ninh_Bình đổ sụp xuống trên khán_đài .
Òa khóc trong vòng_tay đồng_đội , Cương mếu_máo yêu_cầu mọi người xem_lại kết_quả một lần nữa từ BTC . Còn nhà_vô_địch cự_ly 800m Lê_Văn_Dương thì còn buồn hơn cả bạn , nỗi_buồn của người có lỗi . Nhìn đồng_đội nấc lên từng hồi vì hụt mất chiếc HCV quý_giá , Dương chỉ lặng_lẽ cúi đầu , thui_thủi tìm một góc khuất ngồi .
Bất_chợt tôi nhớ đến Ngọc_Tâm khi cô khe_khẽ hát trên khán_đài : " Tôi sẽ hát cho những niềm_vui những nỗi_buồn . Tôi sẽ hát cho giọt nước_mắt lăn trên môi ai . Đừng khóc nhé em , đừng buồn nhé anh . Vì cuộc_đời đâu mãi chỉ có niềm_vui ! ” .
Khi ta thua mà thắng ...
Cổ_nhân có câu : " Nếu chiến_thắng đối_thủ thì đã hạnh_phúc vô_cùng . Nhưng nếu chiến_thắng chính mình mới thực làm ta sung_sướng " .
Không ai có_thể quên được hình_ảnh VĐV Trần_Văn_Thắng , HCB cự_ly 3.000m vượt chướng_ngại_vật đã vừa chạy vừa khóc trên đường đua vì bị chấn_thương cơ_bắp . Đau_đớn và hạnh_phúc là hai cảm_xúc pha lẫn ở vạch đích của Thắng .
Thắng tâm_sự : " Tôi chỉ biết chạy và không được phép ngừng lại . Nếu ngừng lại , nói vui thôi nhé , thì sẽ gục ngã , sẽ không có thành_tích , sẽ tiếc_nuối mãi_mãi . Chỉ có ý_chí mới khiến tôi có_thể tiếp_tục cuộc đua " . Không giành huy_chương vàng nhưng chiếc huy_chương bạc của Thắng quý_giá hơn bất_kỳ phần_thưởng nào !
Tôi không_thể quên hình_ảnh rất cảm_động về đô_vật Nguyễn_Lan_Anh khi bị các bác_sĩ yêu_cầu rời thảm trước khi trận đấu kết_thúc đã dỗi hờn và bật khóc nức_nở . Lan_Anh bị trật khuỷu khá nặng ngay sau khi thực_hiện động_tác đè người .
Trọng_tài cho tạm dừng , bác_sĩ đội_tuyển cầm cánh_tay của Lan_Anh gõ nhẹ . Khuỷu được gá hờ vào khớp . Hai lần tạm dừng tiếp_theo là hai lần bác_sĩ phải cắn_răng để gõ nhẹ và khuỷu lại được gá hờ vào khớp . Có đau không nhỉ , Lan_Anh ? Đau lắm , đau vì không được đấu tiếp , còn cái trật khuỷu này ư , nào có thấy gì đâu , nào có nguy_hiểm gì đâu .
Đã có nước_mắt , những giọt nước_mắt vì sung_sướng và hạnh_phúc nhưng cũng có những giọt nước_mắt đau_khổ , tiếc_nuối lặng_lẽ rơi . Song không ai không muốn có_mặt tại đấu_trường này …
Theo Lan_Phương_Thanh niên

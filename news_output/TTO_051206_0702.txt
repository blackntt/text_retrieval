﻿ 12 chương_trình , công_trình trọng_điểm : Bây_giờ ra_sao ?
& gt ; & gt ; Sức_bật mới & gt ; & gt ; " Phát_huy cao_độ nội_lực , ra_sức khai_thác ngoại_lực ” & gt ; & gt ; “ Lợi_thế không_bao_giờ tồn_tại vĩnh_viễn ... ”
Tốc_độ đường truyền tăng 15 lần
Liên_quan trực_tiếp đến phát_triển kinh_tế là chương_trình công_viên phần_mềm Quang_Trung và khu công_nghệ_cao ( được chia thành hai dự_án ) .
Với dự_án công_viên phần_mềm Quang_Trung , thu_hút 65 doanh_nghiệp ( DN ) phần_mềm có tổng vốn kinh_doanh đăng_ký trên 22,9 triệu USD , hơn 3.800 người tham_gia học_tập và làm_việc trong đó có 32 DN nước_ngoài đến từ 13 quốc_gia ; thu_hút 25 nhà_đầu_tư khác đăng_ký thuê và giao đất để đầu_tư phát_triển công_nghệ_phần_mềm với tổng vốn đăng_ký dự_án đầu_tư trên 1.000 tỉ đồng . Hiệu_suất khai_thác mặt_bằng văn_phòng cho thuê đạt 96% .
Hơn 372 tỉ đồng là tổng giá_trị đầu_tư xây_dựng hạ_tầng kỹ_thuật và phúc_lợi trong khu công_viên phần_mềm Quang_Trung ( trong đó ngân_sách chi 193,6 tỉ đồng ) . Hệ_thống viễn_thông đã hoàn_tất và đưa vào hoạt_động , tốc_độ đường truyền được nâng lên 30 Mbps , gấp 15 lần tốc_độ năm 2001 .
Với dự_án khu công_nghệ_cao , mục_tiêu của dự_án là xây_dựng và đưa khu công_nghệ vào hoạt_động trong năm 2008 ( giai_đoạn 1 ) nên dự_án tiếp_tục được xác_định là công_trình trọng_điểm của năm năm tới .
Phát_triển nguồn nhân_lực
Chương_trình phát_triển nguồn nhân_lực gồm bảy chương_trình bộ_phận . Hầu_hết các chương_trình bộ_phận đều đạt kết_quả nhất_định như : chương_trình giáo_dục_phổ_thông ; chương_trình phát_triển dạy nghề và đào_tạo công_nhân kỹ_thuật : đạt mục_tiêu đề ra với kết_quả 40% lao_động qua đào_tạo nghề , trong đó có 20% lao_động có tay_nghề bậc 3/7 và tương_đương ; chương_trình phát_triển giáo_dục đào_tạo ; chương_trình đào_tạo , phát_triển cán_bộ quản_lý các DN vừa và nhỏ : mở 14 khóa đào_tạo 815 học_viên ( thành_công ) ; chương_trình đảm_bảo nguồn_lực hệ_thống chính_trị TP : hoàn_thành chỉ_tiêu đào_tạo về trung_cấp quản_lý_nhà_nước cho 4.614 người ; trung_cấp lý_luận chính_trị cho hơn 11.700 người , đào_tạo chuyên_viên cấp sở - ngành , quận - huyện 2.707 người ; chương_trình đào_tạo 300 tiến_sĩ - thạc_sĩ ; chương_trình phát_hiện bồi_dưỡng năng_khiếu , nhân_tài ...
Cổ_phần_hóa 305 DN nhà_nước
Với chương_trình củng_cố và sắp_xếp DN nhà_nước , qua năm năm đã cổ_phần_hóa được 305 DN . Chương_trình này cơ_bản hoàn_thành . Tuy_nhiên hiệu_quả kinh_doanh của các DN sau khi sắp_xếp chưa cao , còn vướng_mắc trong quản_trị DN và thủ_tục chuyển quyền sử_dụng đất .
270.000 đồng_hồ nước miễn_phí
Trong chương_trình nước_sạch cho sinh_hoạt của người_dân , có hai nội_dung đạt kết_quả khá : phát_triển mạng_lưới cấp_nước đạt trên 98% so với kế_hoạch ; gắn 270.000 đồng_hồ nước mới miễn_phí , đáp_ứng cơ_bản nhu_cầu về đồng_hồ nước cho nhân_dân ở các khu_vực đã có hệ_thống mạng cấp_nước . Các nội_dung khác chưa đạt yêu_cầu : công_suất cấp_nước chỉ đạt 65% so với mục_tiêu ( đạt 1.200.000m3/ngày - đêm so với mục_tiêu đề ra đến năm 2005 đạt 1.857.000m3/ngày - đêm ) ; tỉ_lệ thất_thoát nước vẫn còn ở mức cao : 33% ( chỉ_tiêu 29% ) .
Xử_lý 6.200 tấn rác / ngày
Chương_trình xử_lý rác có nhiều dự_án hoàn_thành , giải_quyết cơ_bản nhu_cầu về khối_lượng rác cần xử_lý là 6.200 tấn/ngày .
254 triệu lượt người đi xe_buýt
Chương_trình chống kẹt xe nội_thị có những chuyển_biến tích_cực , giải_quyết đáng_kể tình_trạng ùn_tắc giao_thông ở một_số khu_vực cầu_đường Nguyễn_Tri_Phương , Cộng_Hòa , Trường_Chinh , Nguyễn_Hữu_Cảnh , đường Bắc - Nam và cầu Ông Lãnh , đường Hùng_Vương , Điện_Biên_Phủ , đường dọc kênh Nhiêu_Lộc - Thị_Nghè , cầu Bình_Triệu 2 , Tân_Thuận 2 , Nhị_Thiên_Đường 2 , liên tỉnh_lộ 25 , nút giao_thông Hòa_Bình - hương_lộ 14 , các nút giao_thông trên đường xuyên Á ... ; đưa vào sử_dụng 2.043 xe_buýt mới ( nâng tổng_số xe hoạt_động là 3.250 xe ) , ước đạt 254 triệu lượt khách ...
Xóa 61 điểm ngập
Với chương_trình chống ngập nước nội_thị trong mùa mưa , trong năm năm qua TP đã xóa được 61 điểm ngập nước so với chỉ_tiêu là xóa 70 điểm ( sau khi điều_chỉnh ) . Tuy_nhiên hiện_nay TP vẫn còn tồn_tại 79 điểm ngập ( kể_cả các điểm mới phát_sinh ) .
Vượt chỉ_tiêu nuôi bò_sữa
Vượt 8,3% chỉ_tiêu phát_triển đàn bò_sữa là một trong những kết_quả tốt của chương_trình giống cây , giống con chất_lượng cao . Tổng đàn bò_sữa hiện_nay là 54.129 con ; chương_trình nuôi tôm_sú phát_triển ... Các chỉ_tiêu khác chưa đạt kết_quả tốt như nuôi tôm_càng_xanh , phát_triển rau an_toàn , phát_triển cây dứa cayenne ...
Tái_lập những “ con kênh xanh_xanh ”
Chương_trình di_dời và tái_định_cư 10.000 căn_hộ sống ven kênh_rạch nhằm cải_thiện môi_trường và chỉnh_trang đô_thị . Kết_quả sau năm năm đã giải_tỏa được hơn 12.500 mặt_bằng ( trong đó có 12.180 hộ dân ) , nhưng tiến_độ các dự_án xây_dựng nhà tái_định_cư còn chậm .
Kéo giảm tội_phạm , mại_dâm
Một trong ba chương_trình liên_quan trực_tiếp đến đời_sống văn_hóa xã_hội , an_ninh trật_tự , có chương_trình thực_hiện mục_tiêu ba giảm ( tội_phạm , ma_túy , mại_dâm ) đạt kết_quả : kéo giảm bình_quân 8% / năm tội_phạm ; giảm tốc_độ phát_sinh người nghiện mới ( tập_trung được 26.033 người nghiện ma_túy , giải_quyết việc_làm cho 10.000 học_viên và người sau cai_nghiện ) ; giảm 70-80 % tình_trạng mại_dâm , đứng_đường đón khách ...
Khởi_động dự_án hầm Thủ_Thiêm
Công_tác giải_phóng mặt_bằng của công_trình đại_lộ đông - tây và đường_hầm Thủ_Thiêm đạt trên 95% , các khu tái_định_cư của công_trình này đã khởi_công từ tháng 4-2004 . Ngày 15-1-2005 triển_khai gói_thầu tư_vấn và rà phá bom_mìn ; ngày 1-4-2005 khởi_công gói_thầu xây_dựng đường phía tây và mở_rộng đường ven kênh .
Khu tưởng_niệm các vua Hùng
Công_trình khu tưởng_niệm các vua Hùng trong công_viên lịch_sử văn_hóa dân_tộc triển_khai chậm , chỉ giải_tỏa được 278/430 hộ và thu_hồi 58/80ha đất trong toàn khu .
ĐOAN_TRANG tổng_hợp

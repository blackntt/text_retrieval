﻿ Giải_ngân vốn ODA thấp hơn kế_hoạch
Thông_tin từ Bộ KH - ĐT cho_biết , 5 tháng đầu năm , mức giải_ngân nguồn vốn hỗ_trợ phát_triển chính_thức ( ODA ) ước đạt khoảng 545 triệu USD , bằng 30% kế_hoạch năm .
Trong đó , vốn vay đạt khoảng 461 triệu USD , viện_trợ không hoàn lại đạt 84 triệu USD . Theo Bộ KH - ĐT , mức giải_ngân này tương_đương với cùng kỳ năm_ngoái , nhưng thấp hơn kế_hoạch đề ra năm 2006 .
Trong 5 tháng đầu năm 2006 , giá_trị các hiệp_định ODA được ký_kết đạt khoảng 1.331 triệu USD , trong đó vốn vay là 1.228 triệu USD , viện_trợ không hoàn lại là 103 triệu USD .
Nhật_Bản là nhà tài_trợ đứng đầu về mức ODA ký_kết , với tổng giá_trị các hiệp_định đạt 797,8 triệu USD , chiếm khoảng 60% tổng giá_trị ký_kết từ đầu năm đến nay . P . T

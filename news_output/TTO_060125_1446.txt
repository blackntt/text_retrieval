﻿ Israel cần rút khỏi các khu_vực chiếm_đóng khác
Theo thủ_tướng tạm_quyền Ehud_Olmert , Israel cần phải nhượng thêm nhiều phần lãnh_thổ bị chiếm_đóng khác ở Bờ_Tây để ấn_định một đường biên_giới rõ_ràng với người Palestine .
Phát_biểu tại cuộc hội_thảo về an_ninh tại Herzliya , nơi thủ_tướng Shraron thông_báo chiến_dịch rút quân khỏi dải Gaza , ông nói : " Để đảm_bảo sự tồn_tại của một mái nhà quốc_gia Do_Thái , chúng_ta không_thể tiếp_tục quản_lý những vùng đất_đai có đa_số người Palestine sinh_sống " .
Cũng trong bài phát_biểu trên , ông Olmert cho_biết Israel sẽ tiếp_tục kế_hoạch " Lộ_trình hoà_bình " của nhóm Bộ Tứ đề_xướng , trong đó Israel ngưng việc thành_lập các khu định_cư và phá_bỏ các tiền_đồn bất_hợp_pháp của họ .
Bên_cạnh đó , ông Olmert cũng yêu_cầu người Palestine giải_giáp các tổ_chức vũ_trang theo kế_hoạch hoà_bình này , trong đó đặc_biệt là tổ_chức Hamas và các tổ_chức Hồi_giáo vũ_trang chính_thống khác .
Trong khi người Palestine hãy còn dè_dặt trước ý_kiến của ông Ehud_Olmert , những người Do_Thái cực_hữu ủng_hộ các khu định_cư đã giận_dữ bác_bỏ kế_hoạch triệt_thoái mới ở Bờ_Tây .
L.XU ÂN ( Theo Reuters )

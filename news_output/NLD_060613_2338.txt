﻿ Gia_đình Beckham bị soi_mói
Cô chị Joanne và con_trai Brooklyn của Beckham trên khán_đài sân Frankfurt_Tờ_Bild , nhật_báo có sức quảng_bá lớn nhất nước Đức , hôm 12-6 đã mở đợt “ tấn_công ” nhắm vào gia_đình đội_trưởng D . Beckham của tuyển Anh sau khi theo_dõi họ ở trận Anh thắng Paraguay 1 - 0 trên sân Frankfurt .
Tờ này chê cô chị Joanne của Becks là “ cục hamburger mập ” với dòng bình_luận : “ Trời_ơi , cô ta thật mũm_mĩm . Cánh_tay này , ngực này , mông này , đều rất ... Anh . Joanne là loại cô_gái uống rượu trên bãi_biển ở Mallorca rồi sau đó sẵn_sàng leo lên bàn múa với ... ngực trần ” .
Tuy_nhiên , để tỏ ra có đánh_giá khách_quan , báo Bild khen mẹ của Beckham , bà Sandra , năm nay 50 tuổi , có nụ_cười hiền_hậu như “ nông_dân ” ( hay_là hàm_ý nói quê_mùa ? ) .
Beckham đã thuê chuyên_cơ riêng để họ_hàng sang Frankfurt . Đợt tấn_công của Bild được xem là cuộc khẩu_chiến mới nhất giữa nền bóng_đá Đức – Anh tại các giải đấu lớn . Có_vẻ như người Đức chuẩn_bị tâm_lý trước khi họ có_thể gặp Anh ở vòng 2 . Theo lịch thi_đấu , đội Anh có_thể gặp Đức nếu họ xếp nhì bảng B bởi Đức nhiều khả_năng xếp đầu_bảng A .
T . Đ

﻿ Khâu rồi tháo
Em đã nói là nếu muốn “ ăn ” , tụi em khối cách “ ăn ” . Nếu muốn giấu tiền có khối chỗ giấu chứ đâu cần trong túi áo hay túi quần . Vấn_đề là sếp phải làm_sao để nhân_viên như tụi em không thèm “ ăn ” , không muốn “ ăn ” , không_thể “ ăn ” chứ không phải là chuyện khâu khiếc đó .
Sếp cũng biết là thời này , dại gì các quan nhận tiền trực_tiếp , có đàn_em nhận hết . Có nhận cũng dại gì nhận tiền_mặt , có tài_khoản rồi . Tiền bỏ trong túi áo_quần cũng đâu có đủ , để cho nó phình ra cho thiên_hạ thấy sao ? Phải bỏ vào vali chứ , mà có_khi cả mấy vali , thậm_chí nhiều quá nên bỏ quên nữa .
BÚT_BI tình_cờ đọc thư

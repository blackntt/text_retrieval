﻿ 15 thanh_thiếu_niên hoàn_cảnh khó_khăn nhận khai_sinh trễ hạn
TT ( TP.HCM ) - Sáng 21-10-2005 , Quận đoàn 4 ( TP.HCM ) đã trao 15 giấy khai_sinh cho bạn trẻ có hoàn_cảnh đặc_biệt khó_khăn . Trong đó có hai anh_em Nguyễn_Kim_Thanh ( sinh 1994 ) , Nguyễn_Kim_Quyên ( 1999 ) , mẹ bỏ đi , sống với cha làm thợ_hồ , đều không có giấy khai_sinh , không được đến trường .
Thanh phải đi bán vé_số kiếm sống , được Đoàn phường giới_thiệu học lớp phổ_cập .
Được biết , trước đó Quận đoàn 4 cũng đã làm giấy khai_sinh trễ hạn cho bảy bạn nhỏ khó_khăn .
K.ANH

﻿ Bảo_tồn chữ Nôm của dân_tộc Dao
Dự_án sẽ xây_dựng thư_viện sách cổ và tổ_chức dạy chữ Nôm_Dao cho thế_hệ trẻ người bản_địa . Trong giai_đoạn 1 , lớp_học chữ Nôm_Dao sẽ có khoảng 10 học_viên .
Chữ_Nôm_Dao vẫn được người dân_tộc Dao dùng để ghi_chép các bài hát dân_ca , ghi gia_phả , ghi lịch , bài cúng , bài_thuốc , câu_đối . Tại_Lào_Cai có hàng ngàn cuốn sách cổ viết bằng chữ Nôm của người Dao được lưu_giữ rải_rác trong các gia_đình , thư_viện , trong đó có nhiều sách phản_ánh đầy_đủ nền văn_hóa , đời_sống sinh_hoạt , sản_xuất của đồng_bào Dao .
Trước_đây , trong nhiều vùng đồng_bào dân_tộc Dao , việc học chữ Nôm_Dao là yêu_cầu bắt_buộc đối_với nam_giới khi được " cấp sắc " , ghi_nhận sự trưởng_thành . Tuy_nhiên từ nửa thế_kỷ qua , không còn ai học và dạy chữ Nôm_Dao . Những người viết chữ Nôm_Dao còn rất ít , khoảng 4-5 nghệ_nhân , và đều là những người trên 60 tuổi .
Theo anh Vàng_Thung_Chúng , Trưởng_phòng Xây_dựng đời_sống văn_hóa cơ_sở thuộc Sở Văn hóa tỉnh Lào_Cai , nếu_không có biện_pháp bảo_tồn kịp_thời , đến năm 2010 , sẽ không còn ai biết chữ Nôm_Dao .
Trước_đây , một_số nghệ_nhân người Dao đã tự tổ_chức lớp_học dạy chữ Nôm_Dao nhưng do thiếu kinh_phí nên những lớp_học này không duy_trì được lâu .
Phát_hiện 20 di_chỉ khảo_cổ ở Pleikrông
Khi triển_khai dự_án khai_quật khảo_cổ vùng lòng hồ_thủy điện Pleikrông ( tỉnh Kon_Tum ) , Viện_Khảo cổ học và Sở Văn hóa-Thông tin tỉnh Kon_Tum đã phát_hiện 20 di_chỉ khảo_cổ .
Trong số các di_chỉ này có 12 di_chỉ quan_trọng , có giá_trị nghiên_cứu di_sản văn_hóa thời tiền_sử trong vùng ngập của lòng hồ_thủy điện Pleikrông nói_riêng và Tây_Nguyên nói_chung .
Trưng_bày 100 tác_phẩm mỹ_thuật về cách_mạng VN
Ngày 5-8 , tại Hà_Nội , Bảo_tàng Cách_mạng VN phối_hợp với Hội_Mỹ thuật VN khai_mạc phòng trưng_bày 100 tác_phẩm mỹ_thuật với đề_tài " Lịch_sử - cách_mạng , xây_dựng và bảo_vệ Tổ_quốc " .
Đây là các tác_phẩm được chọn_lọc từ 226 tác_phẩm của 177 tác_giả tham_gia cuộc phát_động sáng_tác tác_phẩm mỹ_thuật đề_tài " Lịch_sử - cách_mạng , xây_dựng và bảo_vệ Tổ_quốc " từ đầu năm 2005 .
Tại buổi khai_mạc , các họa sỹ Lê_Duy_Ứng , Nguyễn_Đức_Toàn , Hoàng_Uyên , Phạm_Lực đã trao_tặng Bảo_tàng Cách_mạng Việt_Nam tác_phẩm của mình .
Phòng trưng_bày mở_cửa 1 tháng , sau khi giới_thiệu với công_chúng , Bảo_tàng Cách_mạng VN sẽ mua theo giá thỏa_thuận những tác_phẩm có nội_dung phù_hợp để trưng_bày vĩnh_viễn tại Bảo_tàng .
Theo TTXVN

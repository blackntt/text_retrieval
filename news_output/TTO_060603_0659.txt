﻿ Tin_vắn quốc_tế
Mỹ bác_bỏ gợi_ý của CHDCND Triều_Tiên đề_nghị Nhà_Trắng cử đặc sứ tới Bình_Nhưỡng để bàn về chương_trình hạt_nhân của nước này và cho rằng giải_pháp ngoại_giao sáu bên mới là giải_pháp cho cuộc khủng_hoảng .
Hội_chứng tiếng chuông điện_thoại ( ringxiety ) là một chứng mà ngày_càng nhiều người trên thế_giới mắc phải , theo giáo_sư tâm_lý David_Laramie ( Mỹ ) . Họ thường tưởng_tượng ra tiếng chuông reo dù không có ai gọi đến . Đó là do não bộ con_người thích_nghi dần với nhu_cầu muốn được liên_lạc mọi lúc mọi nơi .
Tổng_thống Yushchenko bị đầu_độc bằng dioxin , kết_quả xét_nghiệm lại của nhóm chuyên_gia Ukraine , Nhật , Mỹ , Đức công_bố hôm_qua . Tuy_nhiên , câu_hỏi ai đầu_độc , khi nào và như_thế_nào vẫn chưa được công_khai .
Một người bị bắn bị_thương do tình_nghi khủng_bố khi cảnh_sát Anh lục_soát một nơi tình_nghi xưởng chế_tạo bom ở East_London , Anh . Cảnh_sát nói sẽ điều_tra sau khi điều_trị vết_thương người này .
Cảnh_sát Los_Angeles ( Mỹ ) vẫn chưa_thể làm rõ động_cơ vụ giết_hại hai vợ_chồng Việt_kiều Phuong_Hung_Le và Trish_Dawn_Lam cùng_với con_trai của họ trong nhà hồi tuần trước . Các nhà điều_tra đang liên_lạc với những người quen của gia_đình này với hi_vọng lần tìm ra hung_thủ .
S . N . - T.TR ÚC - T . Đ . T . - D . V .

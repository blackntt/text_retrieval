﻿ Tìm_hiểu về du_học Hoa_Kỳ
Được cung_cấp thông_tin và giải_đáp cụ_thể về chương_trình học , môi_trường sinh_hoạt tại Mỹ , chính_sách hỗ_trợ sinh_viên quốc_tế . Ngoài_ra còn được tư_vấn về thủ_tục du_học , hướng_dẫn chọn trường và kế_hoạch học_tập .
Để biết thêm thông_tin chi_tiết và đăng_ký phỏng_vấn , vui_lòng liên_hệ : Phòng du_học SEAMEO , 35 Lê_Thánh_Tôn , Q . 1 , TP.HCM . Tel : ( 8 ) 8245618 ( máy phụ : 118/119 ) , Fax : 8232175 , Email : osd@vnseameo.org hoặc Ong_Daniel M.Harrop email : dharrop@marianapolis.org , website : www.marianapolis.org/www.cbsa.org .
K . N .

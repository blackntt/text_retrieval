﻿ Niềm hạnh_phúc của một người con
Những bạn trẻ ở đây xúc_động kể nhiều_chuyện về người con ấy : sáng_sáng , chiều_chiều dắt mẹ đi theo con đường đầy sỏi trước nhà , băng qua đường_ray , đi theo con chợ xổm Nguyễn_Xí đầy nước bẩn để ra đường_cái Nơ_Trang_Long tập đi - vừa dìu vừa khích_lệ , “ dỗ_dành ” mẹ . Hôm nào mẹ mệt quá , không đi tiếp được , Hùng mướn xích_lô đưa mẹ về .
Hôm tôi gặp ở nhà , vợ anh bị bệnh . Hùng đang tất_bật với công_việc chăm_sóc cả mẹ lẫn vợ , con . Xin chụp hình , Hùng nhẹ_nhàng từ_chối : “ Có_lẽ đó là đạo_hiếu của bất_kỳ người con nào ” , rồi say_sưa kể về những bác_sĩ , y_tá hết_lòng vì bệnh_nhân , trong đó có mẹ anh .
Thật kỳ_lạ , trong bộn_bề vất_vả cho mẹ như_thế , đôi mắt người con_trai ấy rực lên khi nhắc đến chuyện “ khi thấy tôi dắt mẹ qua đường , người đi đường_không nỡ bóp còi , ai cũng nhường đường ” . Rồi anh cười vẻ hạnh_phúc : “ Cuộc_đời này còn đẹp quá phải không ? ” …
PHẠM_AN_HÒA ( SV khoa Ngữ_văn ĐH Văn_Hiến TP.HCM )

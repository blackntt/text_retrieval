﻿ À í a của Lê_Minh_Sơn đoạt giải Bài hát của năm
Không nhiều bất_ngờ nhưng cũng khó dự_đoán , người gặt_hái được niềm_vui lớn nhất không ai khác chính là nhạc_sĩ ( NS ) Lê_Minh_Sơn với ca_khúc đoạt giải Bài hát của năm ( giải_thưởng quan_trọng nhất , trị_giá 30 triệu đồng ) - ca_khúc À í a . À í a qua phần trình_diễn của ca_sĩ Trọng_Tấn cũng là ca_khúc thứ_hai của Lê_Minh_Sơn đã từng đoạt giải ca_khúc của tháng , sau ca_khúc Hát một ngày mới .
Giải_Bài hát có phong_cách dân_gian đương_đại nổi_bật ( 15 triệu đồng ) đã thuộc về Giọt sương bay lên của NS trẻ Nguyễn_Vĩnh_Tiến , vượt qua các đề_cử khác gồm ca_khúc Bà tôi của chính anh và Mưa_bay tháp cổ ( Trần_Tiến ) , Giấc_mơ trưa ( Giáng_Son ) .
Giải_Bài hát có phong_cách pop rock đương_đại nổi_bật ( 15 triệu đồng ) thuộc về ca_khúc Giấc_mơ của tôi của NS Anh Quân , vượt qua Thu tình_yêu ( Lưu_Hương_Giang ) , 12g ( Nguyễn_Duy_Hùng ) và Mong anh về ( Dương_Cầm ) .
NS Nguyễn_Vĩnh_Tiến với giải_thưởng dành cho Bài hát có phong_cách dân_gian đương_đại nổi_bật
NS Anh Quân với Giải_Bài hát có phong_cách pop rock đương_đại nổi_bật - Ảnh : VNN
Giải dành cho Nhạc_sĩ tham_gia phối_khí hiệu_quả nhất ( 10 triệu đồng ) được trao cho hai NS Phan_Cường ( Bà tôi , Giọt sương bay lên ) và Nguyễn_Đạt ( Thu_Tình yêu , Cơn lốc ) . Cùng được đề_cử ở giải_thưởng này là Minh_Đạo ( Mưa_bay tháp cổ , Cơn mưa tình_đầu ) và Anh Quân ( Giấc_mơ của tôi , 12 giờ , Chờ anh đêm mùa_đông ) .
Giáng_Son - cựu thành_viên của nhóm 5 dòng kẻ đã trở_thành nữ tác_giả đầu_tiên được vinh_danh trong lễ trao giải BHV với Giải dành cho Nhạc_sĩ ấn_tượng ( 10 triệu đồng ) . Các đề_cử khác gồm NS Lê_Minh_Sơn , NS Nguyễn_Vĩnh_Tiến và NS Nguyễn_Xinh_Xô .
NS Nguyễn_Đạt - giải Nhạc_sĩ tham_gia phối_khí hiệu_quả nhất
NS Giáng_Son - giải_thưởng Nhạc_sĩ ấn_tượng - Ảnh : VNN
Ngọc_Khuê - giải Ca_sĩ được khán_giả yêu_thích - Ảnh : VNN Ngoài 5 giải_thưởng do Hội_đồng thẩm_định bầu_chọn , hai giải_thưởng được đông_đảo khán_giả chờ_đợi - giải Bài hát được khán_giả yêu_thích nhất đã thuộc về ca_khúc Mưa_bay tháp cổ ( Trần_Tiến ) và giải Ca_sĩ được khán_giả yêu_thích đã được trao cho ca_sĩ trẻ Ngọc_Khuê ( Bà tôi , Giọt sương bay lên ) .
Bài hát Việt 2005 đã thu_hút 2.900.000 lượt khán_giả bình_chọn qua hộp thư_thoại 19001755 , 405.680 lượt khán_giả bình_chọn qua website baihatviet . vtv.vn .
Tại lễ trao giải , ban tổ_chức chương_trình Bài hát Việt cũng đã chính_thức phát_động cuộc_thi Bài hát Việt 2006 .

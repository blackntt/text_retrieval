﻿ Thủ_tục , chi_phí xuất_khẩu lao_động ?
1 . Lao_động đi Canada là trường_hợp xin visa ngắn_hạn làm_việc tại nước_ngoài . Về nguyên_tắc chung , các thủ_tục phải chuẩn_bị : đơn xin cấp visa ( Tổng_lãnh_sự_quán Canada tại TP.HCM cấp ) , các giấy_tờ về hộ_tịch và hộ_khẩu ( khai_sinh , giấy đăng_ký kết_hôn nếu có , CMND , hộ_khẩu ) , lý_lịch tư_pháp , bằng_cấp .
Nơi liên_lạc và tư_vấn : Trung_tâm tiếp_nhận hồ_sơ thị_thực Canada - số 1B Phạm_Ngọc_Thạch , phường Bến_Nghé , quận 1 , TP.HCM .
Sau khi hoàn_tất các giấy_tờ tại VN , Công_ty phía Canada sẽ có trách_nhiệm đăng_ký với bộ_phận di_trú Canada để cho_phép lao_động nhập_cảnh .
Nếu bạn cần chi_tiết thêm , có_thể liên_hệ Tổng_lãnh_sự_quán Canada tại TP.HCM ( số 235 Đồng_Khởi , phường Bến_Nghé , quận 1 TP.HCM ) .
2 . Hiện_nay , thị_trường Canada , Úc chưa có ký_kết chính_thức nào từ phía Nhà_nước , các đơn_vị xuất_khẩu lao_động đang trong giai_đoạn thăm_dò , tìm_hiểu thông_tin và khai_thác thử qua một_vài hợp_đồng nhỏ_lẻ .
3 . Về thủ_tục hồ_sơ lao_động làm_việc tại nước_ngoài , bạn hãy tham_khảo câu trả_lời 1 . Về mức phí dịch_vụ hiện_nay tùy_thuộc vào sự thỏa_thuận giữa người lao_động với Công_ty làm dịch_vụ . Về phía nhà tuyển_dụng và quy_định tiếp_nhận lao_động nước_ngoài không có chi_phí nào .
Về ngành_nghề tuyển , VN mới đưa được lao_động đi làm nghề giết_mổ gia_cầm ( Công_ty TNHH Khải_Nam , số 565/4 Nguyễn_Trãi , phường 7 - quận 5 ) , lao_động nghề làm bánh ( Công_ty Du_lịch - Dịch_vụ dầu_khí Việt_Nam ( OSC ) , địa_chỉ OSC tại 92 Calmette quận 1 - TP.HCM ) .
Như_vậy , trình_độ tiếng Anh giỏi sẽ giúp bạn có nhiều thuận_lợi khi tham_gia thị_trường lao_động xuất_khẩu . Bạn có_thể tìm địa_chỉ các Công_ty XKLĐ trên các trang_web hoặc hỏi tổng_đài 1080 .
N . BAY thực_hiện

﻿ Cần_Thơ : chỉ hơn 3% trẻ dưới 6 tuổi được miễn_phí
Chỉ có 3,3% trẻ_em dưới 6 tuổi đến Bệnh_viện Nhi_Đồng_Cần_Thơ được khám và chữa bệnh miễn_phí .
Theo bệnh_viện , nguyên_nhân là do qui_định phân tuyến khám chữa bệnh , bệnh_viện bắt_buộc bệnh_nhi phải có các giấy_tờ chứng_minh độ tuổi , hoặc thẻ khám chữa bệnh miễn_phí kèm theo giấy giới_thiệu chuyển viện của bệnh_viện tuyến dưới .
Trong khi phần_lớn người_nhà bệnh_nhân không tín_nhiệm các trạm y_tế hay bệnh_viện quận , huyện nên đưa trẻ vượt_tuyến .
Theo Tuổi_Trẻ

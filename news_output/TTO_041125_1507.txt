﻿ Cần lắm những cuộc trưng_cầu_dân_ý !
Liên_tiếp các sự_kiện đã cho chúng_ta thấy các cơ_quan_chức_năng chưa thật_sự tiếp_cận được những nhu_cầu , suy_nghĩ của người_dân . Vừa_qua một bạn_đọc của báo Tuổi_Trẻ đã nêu suy_nghĩ cho rằng các cơ_quan_chức_năng làm_việc vô_trách_nhiệm và thiếu tính khoa_học . Tôi hoàn_tòan đồng_ý với ý_kiến đó và tin rằng đã có rất nhiều người cùng suy_nghĩ như_vậy .
Thử_hỏi mỗi lần thay_đổi rồi thấy bất_cập thì bãi_bỏ các cơ_quan_chức_năng đã làm lãng_phí bao_nhiêu ngân_sách ? Tiền_công được sử_dụng một_cách dễ_dàng như_thế sao ?
Theo tôi mỗi lần dự_thảo một kế_họach nào_đó các cơ_quan cần có một cuộc trưng_cầu ý_kiến của người_dân bởi họ là người trực_tiếp chịu ảnh_hưởng của những thay_đổi đó . Đừng tạo cho họ cái cảm_giác các cơ_quan_chức_năng chỉ biết đứng xa nhìn vào và ban luật .
TRẦN_THỊ_THÚY_VÂN

﻿ BIDV được nâng hạng trong vai_trò nhà phát_hành ngoại_tệ
( NLĐO ) - Tổ_chức xếp_hạng tín_nhiệm quốc_tế Moody ’ s Investors_Service_Ltd . , ( “ Moody ’ s ” ) của Mỹ vừa có thông_báo chính_thức về việc nâng hạng cho các định_chế tài_chính khu_vực châu Á – Thái_Bình_Dương . `
Cụ_thể , đối_với Việt_Nam : Xếp_hạng trái_phiếu chính_phủ dài_hạn được nâng từ mức Ba3 lên mức Ba2 .
Lý_do của sự điều_chỉnh này là thay_đổi trong phương_pháp xếp_hạng đối_với mức trần trái_phiếu ngoại_tệ quốc_gia để phản_ánh đúng hơn mức_độ phát_triển của các nền tài_chính .
Theo đó , Ngân_hàng Đầu_tư và Phát_triển Việt_Nam ( BIDV ) hiện là định_chế tài_chính duy_nhất tại Việt_Nam được Moody ’ s xếp_hạng cũng được nâng hạng . Mức xếp_hạng nhà phát_hành ngoại_tệ của BIDV trước_đây là : Ba3 được nâng lên Ba2 với triển_vọng ổn_định . Xếp_hạng về Năng_lực tài_chính Ngân_hàng ( BFRS ) vẫn giữ ở mức tích_cực .
G.Linh

﻿ Bình_Dương : hỗ_trợ đào_tạo 207 cử_nhân , thạc_sĩ , tiến_sĩ
Bình_Dương ( BD ) sẽ hỗ_trợ để đào_tạo 155 cử_nhân , 51 thạc_sĩ và 1 tiến_sĩ . Đó là nội_dung quyết_định do chủ_tịch UBND tỉnh BD Nguyễn_Hoàng_Sơn vừa ký về việc ban_hành danh_mục chuyên_môn cần đào_tạo năm 2005 .
Đối_với SV , hằng tháng tỉnh sẽ trợ_cấp 1,5 lần mức lương tối_thiểu ( 1,5 x 350.000đ = 525.000đ ) kể_cả thời_gian nghỉ_hè ; học sau ĐH được hưởng chế_độ đi học như cán_bộ của tỉnh .
Tất_cả phải có hộ_khẩu BD trước 31-12-2001 và sau khi tốt_nghiệp phải về tỉnh công_tác ( theo phân_công ) ít_nhất bằng ba lần thời_gian được hỗ_trợ . Đặc_biệt , tất_cả SV đều phải học ở các trường ĐH công_lập . Hồ_sơ đăng_ký hết hạn vào 30-11-2005 , liên_hệ Sở Nội vụ BD ( ĐT : 0650 821324 ) để biết thêm chi_tiết .
Theo Tuổi_Trẻ

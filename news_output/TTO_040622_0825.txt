﻿ Làm rõ hoạt_động của liên_doanh Phú_Mỹ_Hưng
Theo nội_dung làm_việc , tổ công_tác ( gồm đại_diện các bộ ngành trung_ương và các sở ngành liên_quan của thành_phố do Phó_chủ_tịch UBND_TP Nguyễn_Thiện_Nhân làm tổ_trưởng ) sẽ nghe lại phương_án giải_quyết vấn_đề thuế_thu_nhập của Công_ty liên_doanh Phú_Mỹ_Hưng nhằm thống_nhất việc định_mức thuế chính_xác là 10% hay 25% ; đồng_thời xác_định việc cấp chủ_quyền nhà cho người mua nhà của công_ty này như_thế_nào khi đơn_vị này chỉ được giao đất để sử_dụng trong 50 năm .
Dự_kiến tổ công_tác sẽ làm_việc trong ba ngày , sau đó có kết_luận báo_cáo Thủ_tướng Chính_phủ .
Đ . TRANG

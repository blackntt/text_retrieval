﻿ Ông Đào_Đình_Bình xin_lỗi cử_tri Quảng_Bình
Ông Đào_Đình_Bình , đại_biểu Quốc_hội ( ĐBQH ) tại tỉnh Quảng_Bình , vừa xin_lỗi cử_tri Quảng_Bình tại cuộc tiếp_xúc cử_tri ở huyện Quảng_Trạch , Đồng_Hới chuẩn_bị cho kỳ họp thứ 9 Quốc_hội khóa 11 sắp tới .
Trên cương_vị Bộ_trưởng Giao_thông vận_tải , ông đã để xảy_ra những sai_phạm nghiêm_trọng tại Ban quản_lý các dự_án 18 ( PMU 18 ) .
Trong những cuộc tiếp_xúc cử_tri vừa_qua của đoàn ĐBQH tỉnh Quảng_Bình , cử_tri TP Đồng_Hới đã bức_xúc khi nêu lên vấn_đề tiêu_cực tại PMU 18 và đặt câu_hỏi về trách_nhiệm của lãnh_đạo cấp trên đối_với những sai_phạm tại cơ_quan này . Nhiều ý_kiến đề_nghị QH cần xem_xét trách_nhiệm của ĐBQH tại nơi đã xảy_ra tiêu_cực . Theo L.Giang Báo_Tuổi trẻ

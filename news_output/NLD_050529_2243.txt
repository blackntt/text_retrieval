﻿ Cháy phòng tập của vận_động_viên môn thể_dục_dụng_cụ
( NLĐ ) - Chiều 29-5 , CLB thể_dục Trần_Hưng_Đạo , số 257 Trần_Hưng_Đạo , quận 1 ( thuộc Sở Thể dục Thể_thao TPHCM ) đã phát cháy dữ_dội . Theo những người chứng_kiến thì ngọn lửa phát ra từ khu tập_luyện của các vận_động_viên thể_dục_dụng_cụ .
Do thời_gian này CLB không hoạt_động nên không gây thiệt_hại cho các vận_động_viên . Người bị_thương duy_nhất sau đám cháy là anh Nguyễn_Hùng_Sơn , bảo_vệ của CLB . Đám cháy đã thiêu rụi diện_tích 250 m2 của CLB thể_dục . Theo nhận_định , nguyên_nhân của vụ cháy có_thể do điện bị chập .
N.Hậu

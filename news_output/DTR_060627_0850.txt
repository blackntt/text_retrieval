﻿ Microsoft thử_nghiệm Office 2007 trực_tuyến
Hôm_qua , hãng phần_mềm khổng_lồ phát_hành một bản beta của ứng_dụng văn_phòng phiên_bản mới , cho_phép người dùng chạy thử_nghiệm miễn_phí mà không cần cài_đặt hoặc download về máy .
Phiên_bản online này có giao_diện người dùng mới hoàn_toàn , thích_ứng với nhiệm_vụ mà người dùng thực_hiện . “ Office 2007 online sẽ tạo ra một cơ_hội tuyệt_vời để mọi người có_thể dễ_dàng làm_việc ” , Microsoft tuyên_bố .
Để sử_dụng online , người dùng sẽ phải download bản plug - in và đăng_ký trực_tuyến tại website của Microsoft . Một thách_thức lớn mà hãng có trụ_sở ở Redmond đang đối_mặt là làm_sao thuyết_phục được khách_hàng mua được một phiên_bản mới của Office trong khi phần_mềm hiện_tại vẫn sử_dụng tốt .
Gã khổng_lồ phần_mềm cho_biết hiện_tại đã có hơn 2,5 triệu lượt download phiên_bản beta 2 của Office 2007 từ khi được tung ra hồi tháng 5 . Bản chung_cuộc của ứng_dụng xử_lý văn_phòng này sẽ chính_thức được phát_hành vào đầu năm tới .
Bản online đã có phiên_bản tiếng Anh . Các bản tiếng Pháp , Đức , Nhật và Tây_Ban_Nha sẽ được ra_mắt vào cuối tuần này .
N . H_Theo VNuNet , CNet

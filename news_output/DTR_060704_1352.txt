﻿ Một giảng_viên chết trong trường ĐH Thủy_Sản_Nha_Trang
Lúc 5h sáng_qua ( 3/7 ) , tại hội_đồng thi trường Đại_học Thuỷ_Sản - phường Vĩnh_Phước , TP Nha_Trang , Khánh_Hòa - người_ta phát_hiện thấy giáo_viên Lê_Bá_Phi , người giám_sát đề thi môn Toán , đang trong tình_trạng hấp_hối .
Thầy_Lê_Bá_Phi sinh năm 1955 , thường_trú tại 15/3B , đường Hồng_Bàng , phường Tân_Lập , Nha_Trang , hiện đang là phó chủ_nhiệm khoa cơ_bản của trường ĐH Thủy_Sản .
Trong kỳ thi môn Toán , thầy Phi được hội_đồng thi giao nhiệm_vụ giám_sát đề thi .
Khi được phát_hiện , giáo_viên này đang trong tình_trạng hấp_hối . Nhà_trường đã đưa đi cấp_cứu nhưng đến bệnh_viện tỉnh thì chết .
Hiện công_an địa_phương đang tổ_chức khám_nghiệm , làm rõ cái chết của thầy Phi . H.Minh

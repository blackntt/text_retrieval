﻿ “ Vang mãi khúc quân_hành ” : cuộc hội_ngộ của hơn 1000 cựu_chiến_binh
Theo đó , chương_trình này sẽ tài_trợ cho cuộc hành_quân về TPHCM của hơn 1000 cựu_chiến_binh trên khắp đất_nước để tái_hiện lại cuộc hành_quân thần_tốc , anh_dũng của quân_dân Việt_Nam trong chiến_dịch Hồ_Chí_Minh lịch_sử năm 1975 .
Theo dự_kiến , cuộc hành_quân sẽ bắt_đầu_từ 21.4 đến 2.5.2005 . Những đại_biểu từ 64 tỉnh_thành trên cả nước tham_dự cuộc hành_quân tái_hiện lịch_sử có quy_mô lớn này sẽ được chia làm 3 cánh quân : Đồng_Bằng , Tây_Nguyên , và Đồng_bằng sông Cửu_Long . Ba cánh quân này sẽ hội_ngộ và gặp nhau vào đúng trưa 30.4.2005 tại Dinh_Thống_Nhất TPHCM .
Đ . TÂM

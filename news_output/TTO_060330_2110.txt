﻿ Bơ sữa và canxi giúp ngăn_ngừa ung_thư ruột kết
Trên tạp_chí Mỹ_Journal of Clinical_Nutrition , tiến_sĩ Susanna C . Larsson thuộc Viện_Karolinska , Stockholm ( Thụy_Điển ) và đồng_sự cho_biết các nghiên_cứu gần đây cho thấy “ mối quan_hệ nghịch_đảo giữa lượng canxi tiêu_thụ và nguy_cơ mắc bệnh ung_thư ruột kết ” .
Họ đã tiến_hành nghiên_cứu về mối quan_hệ giữa lượng canxi tiêu_thụ và nguy_cơ mắc bệnh ung_thư ruột kết trên 45.306 đàn_ông Thụy_Điển từ 45-79 tuổi và chưa từng có tiền_sử mắc bệnh ung_thư .
Trong 6,7 năm sau , tổng_cộng có 449 người mắc bệnh ung_thư ruột kết . Các nhà_nghiên_cứu phát_hiện nguy_cơ mắc bệnh ở những người tiêu_thụ nhiều canxi nhất thấp hơn 32% so với những người tiêu_thụ ít canxi nhất . Việc tiêu_thụ nhiều sản_phẩm bơ sữa cũng giúp giảm nguy_cơ mắc bệnh này .
TƯỜNG_VY ( Theo Reuters )

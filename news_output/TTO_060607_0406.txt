﻿ Xây_dựng văn_hóa không tham_nhũng
Giải_pháp đầu_tiên mà bản báo_cáo này đề_cập nêu rõ : nên nghiên_cứu cải_cách toàn_diện về tổ_chức của Đảng , các cơ_quan của Quốc_hội , Chính_phủ , các cơ_quan tư_pháp , các tổ_chức chính_trị , chính_trị - xã_hội theo hướng gọn , nhẹ , hiệu_lực , hiệu_quả , công_khai , minh_bạch , chịu sự giám_sát của nhân_dân .
Bởi theo đoàn công_tác , thực_tế kinh_nghiệm của Thụy_Điển khẳng_định “ tham_nhũng tỉ_lệ_nghịch với công_khai , minh_bạch ” , càng công_khai , minh_bạch bao_nhiêu , tham_nhũng càng bị ngăn_chặn , đẩy_lùi bấy_nhiêu .
Đặc_biệt , bản báo_cáo cho rằng cần “ xây_dựng văn_hóa không có tham_nhũng ở cả khu_vực công và khu_vực tư ” ; không_thể để tham_nhũng thâm_nhập vào học_đường và trong nhận_thức của các thế_hệ tương_lai . Đồng_thời phải đề_cao vai_trò của báo_chí trong đấu_tranh phòng , chống tham_nhũng .
Đ . TR .

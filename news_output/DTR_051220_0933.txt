﻿ Deco phủ_nhận tin_đồn từ thành Madrid
Tiền_vệ người Bồ_Đào_Nha_Deco đã phản_bác những suy_đoán của báo_chí Madrid rằng anh sẽ rời Barcelona vào cuối mùa bóng .
" Tôi đã có bản hợp_đồng 5 năm ở đây và tôi chẳng có bất_kỳ lý_do gì để ra_đi " , anh nói trên kênh_truyền_hình Bồ_Đào_Nha RDP ngày hôm_qua .
Tuần trước , tờ báo thể_thao Tây_Ban_Nha AS đã trích_dẫn một nguồn không chính_thức từ Ban lãnh_đạo Barca rằng : CLB sẽ bán một_số tên_tuổi lớn vào cuối mùa bóng và Deco là một trong số đó .
Tuy_nhiên , cầu_thủ 28 tuổi này đã khẳng_định cả chủ_tịch Barcelona ông Joan_Laporta và HLV trưởng Frank_Rijkaard đảm_bảo với anh rằng : " anh vẫn là một phần quan_trọng của CLB " .
" Báo_chí tại Madrid muốn gây mất đoàn_kết trong nội_bộ Barcelona . Đó chính là cách duy_nhất họ có_thể làm được đối_với chúng_tôi " , anh nói .
Barcelona đã ký hợp_đồng 4 năm với cầu_thủ gốc Brazil này hồi tháng 6/2004 từ đội bóng Bồ_Đào_Nha_Porto . Sau đó trong mùa_hè vừa_qua , Deco đã gia_hạn hợp_đồng với đội bóng xứ Catalan thêm một năm nữa .
Theo An_Hà_Tiền phong / AP

﻿ Hollywood kiện các trang_web sao_chép lậu phim_ảnh
Theo thông_báo của MPAA , tổ_chức bảo_vệ quyền_lợi của các hãng phim lớn của Hollywood , các trang_web bị kiện đã để khách_hàng của họ sử_dụng dịch_vụ mạng để tải những bộ phim có bản_quyền của Hollywood . Vụ_kiện này là chặng thứ_tư trong chiến_dịch chống các hoạt_động sao in , trao_đổi lậu phim_ảnh của Hollywood .
MPAA cho_biết các hãng phim lớn của Hollywood mất khoảng 6,1 tỉ USD/năm trên các thị_trường thế_giới do nạn ăn_cắp bản_quyền các bộ phim của họ
ĐỨC_TRƯỜNG ( Theo AFP )

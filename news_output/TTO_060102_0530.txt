﻿ Giao_lưu trực_tuyến " Trò_chuyện đầu năm - Khởi_sự doanh_nghiệp "
" Phải tăng nhanh hơn_nữa số_lượng doanh_nghiệp , nhưng quan_trọng hơn là mỗi DN phải kinh_doanh giỏi , đạt hiệu_quả và sức cạnh_tranh cao . Doanh_nhân nước ta là doanh_nhân của thế_kỷ 21 , nhất_thiết chúng_ta sẽ có những doanh_nhân tầm_cỡ thế_giới " - Đó là gửi_gắm của Thủ_tướng Phan_Văn_Khải dành cho giới trẻ VN chọn kinh_doanh như một sự khởi_nghiệp .
Không có chiếc chìa_khóa thành_công chung cho tất_cả chúng_ta . Nhưng hành_trình đi tìm chiếc chìa_khóa riêng cho mỗi người vẫn có những bạn_bè , anh_em sẻ_chia những kinh_nghiệm .
Bạn_đọc có_thể chia_sẻ kinh_nghiệm với các doanh_nhân trẻ : TS Ngô_Trần_Công_Luận , 37 tuổi - Tổng_giám_đốc Công_ty Nhà_đất đô_thị mới ; Giản_Tư_Trung , 31 tuổi - Chủ_tịch HĐQT Công_ty đào_tạo doanh_nghiệp PACE ; Nguyễn_Thu_Phong , 31 tuổi , Tổng_giám_đốc công_ty_cổ_phần kiến_trúc xây_dựng Nhà vui .
Ngay từ bây_giờ , bạn_đọc có_thể bấm vào đây để đặt câu_hỏi ( lưu_ý : bạn_đọc nên gõ câu_hỏi có dấu bằng font Unicode )
Giản_Tư_Trung - chủ_tịch Hội_đồng_quản_trị công_ty đào_tạo doanh_nghiệp PACE
Người luôn nuôi khát_vọng
Giản_Tư_Trung
Khởi_nghiệp kinh_doanh từ thời còn là SV_ĐH ; Làm_chủ cơ_sở nhựa Chợ_Lớn ( 1992 )
- Làm_việc cho ba tập_đoàn kế_toán và tư_vấn hàng_đầu thế_giới : DDT , PWC , KPMG .
- Làm_việc cho UB Chứng_khoán nhà_nước tại Hà_Nội .
- Làm_Giám đốc công_ty kiểm_toán .
- Chủ_tịch hội_đồng_quản_trị PACE .
- Phó chủ_nhiệm CLB doanh_nhân Sài_Gòn 2030 , Phó chủ_nhiệm CLB doanh_nhân Sài_Gòn . Khởi_nghiệp kinh_doanh từ khi còn là sinh_viên đại_học , với anh Giản_Tư_Trung , cái thời làm thợ sơn bảng , thợ chụp ảnh … và đứng ra thành_lập và làm_chủ cơ_sở nhựa Chợ_Lớn để bươn_chải kiếm sống luôn là thời_đoạn nhắc anh phải vượt lên cùng_với những khát_vọng của mình .
Giản_Tư_Trung hiện_thời là chủ_tịch Hội_đồng_quản_trị của PACE , tổ_chức giáo_dục chuyên_nghiệp , nơi đào_tạo doanh_nhân và giám_đốc . PACE có_mặt và khẳng_định vị_thế ở VN trong sứ_mạng góp_phần đưa kiến_thức của thế_giới vào VN để phát_triển con_người cho các doanh_nghiệp đang hoạt_động tai VN .
Hơn 1 vạn lượt doanh_nhân và doanh_nghiệp đã và đang theo học tại PACE . Ông chủ_tịch hội_đồng_quản_trị Giản_Tư_Trung và PACE đã cùng chia_sẻ thực_trạng “ khát ” nguồn nhân_lực , đặc_biệt là nguồn nhân_lực cao_cấp với các doanh_nghiệp , nhất_là doanh_nghiệp VN .
Với quan_niệm của một người , cùng một lúc , thực_hiện được cả 2 nỗi đam_mê : làm kinh_doanh và nghiện cứu khoa_học nên PACE cũng mang tâm_niệm : tôn_thờ giá_trị thực “ nhân_viên thực làm , giảng_viên thực dạy , học_viên thực_học ” … Giản_Tư_Trung đưa ra quan_điểm của mình cho PACE , quan_trọng là mình làm được điều gì , đó là ý_nghĩa lớn nhất …
Anh đưa ra triết_lý sống dành cho mình : “ chơi là làm những gì mình thích , làm là chơi những gì mình … không thích , vì cái chung thì mới có cái riêng , đó là điều mình theo_đuổi bền_vững nhất . Khi_không phân_biệt được đâu là làm , đâu là chơi thì lúc đó cuộc_sống sẽ thăng_hoa trong những cái riêng chung mà mình có được … ” .
Trò_chuyện với Giản_Tư_Trung , còn nhận ra cá_tính mạnh_mẽ và luôn cẩn_trọng trong từng công_việc . Gốc_Nghệ_An , lớn lên ở Nha_Trang và lập_nghiệp tại Sài_Gòn , từng vùng_đất ít_ra cũng kết_tụ và tạo cho anh một khí_chất đặc_biệt .
& gt ; & gt ; Điều đáng_sợ : không có khát_vọng lớn !
Nguyễn_Thu_Phong - tổng_giám_đốc công_ty_cổ_phần kiến_trúc xây_dựng Nhà_Vui
Khát_khao sáng_tạo những ngôi nhà … của niềm_vui
Tốt_nghiệp Đại_học kiến_trúc năm 1997 , chàng sinh_viên từng là thủ_khoa Đại_học Kiến_trúc năm 1992 - đội_trưởng đội SV 96 Nguyễn_Thu_Phong được giữ lại trường để giảng_dạy . Nhưng nghề giáo không đủ cho Thu_Phong giải_phóng những năng_lực cá_nhân của mình , bắt_tay vào “ khởi_sự doanh_nghiệp ” lúc này , với Thu_Phong , không phải là ước_mơ làm_giàu mà như một lối_thoát của một trí_thức trẻ ước_mơ được làm_việc .
Sau khi website nhavui . com - website đầu_tiên chuyên_ngành xây_dựng ra_đời tháng 9-2000 , tháng 11-2001 , Trung_tâm tư_vấn thiết_kế và vật_liệu xây_dựng đầu_tiên chuyên cung_cấp dịch_vụ tư_vấn thiết_kế nhà_ở mang tên Nhà vui cũng ra_đời ở Trần_Quốc_Thảo , TP.HCM .
Ảnh : VNN
Nguyễn_Thu_Phong
- Tổng_giám_đốc công_ty_cổ_phần kiến_trúc xây_dựng Nhà vui
- Chủ_tịch HĐQT Công_ty_cổ_phần Thương_mại Gia_Phúc
- Tham_gia giảng_dạy tại trường Đại_học kiến_trúc từ năm 1997 đến nay
- Phó_chủ_tịch Hội kiến_trúc_sư TP.HCM
- Phó_chủ_tịch Hội liên_hiệp thanh_niên thành_phố . Đến nay , Nhà vui có tất_cả 5 trung_tâm tư_vấn - thiết_kế tại các địa_bàn khác_nhau của TP.HCM và Hà_Nội , trở_thành đơn_vị đi đầu trong lĩnh_vực thiết_kế và thi_công nhà_ở thấp_tầng , đặc_biệt là lĩnh_vực nhà_ở tư_nhân . Công_ty đã tư_vấn thiết_kế nhiều công_trình trên 34 tỉnh_thành trong cả nước , với khoảng 200 công_trình / năm .
Nhà vui cũng chính là môi_trường cho hàng_loạt kiến_trúc_sư trẻ thỏa chí “ khát_khao sáng_tạo cuộc_sống ” như slogan của công_ty . “ Tiên_phong ” là cam_kết của công_ty Nhà vui với khách_hàng về một đội_ngũ kiến_trúc_sư trẻ xác_định sứ_mạng chuyên_nghiệp trong lĩnh_vực tư_vấn thiết_kế nhà_ở , tập_trung sáng_tạo vì một không_gian sống hoàn_thiện .
Bên_cạnh những giải_thưởng được Hội kiến_trúc_sư Việt_Nam trao_tặng ( giải “ Bông mai vàng 2004 ” , giải nhất cuộc_thi “ Ý_tưởng - giải_pháp kiến_trúc tối_ưu cho nhà_ở liền kế lô phố trong đô_thị ” ) , chứng_nhận quản_lý chất_lượng ISO 9001 : 2000 , giấy chứng_nhận thương_hiệu Việt yêu_thích “ Chìa_khóa vàng 2004 ” , Giải_thưởng Sao vàng đất Việt dành cho thương_hiệu “ Nhà vui ” và sản_phẩm “ Thiết_kế nhà_ở dân_dụng ” năm 2005 đã phần_nào khẳng_định thành_công đáng quý của một doanh_nghiệp trẻ và của riêng Nguyễn_Thu_Phong .
Trò_chuyện với Nguyễn_Thu_Phong , độc_giả sẽ khám_phá khá nhiều điều thú_vị về những ước_mơ cháy_bỏng của một người trẻ khát_khao làm_việc và giữ sáng niềm_tin cho triết_lý công_việc của mình : “ Một đời lao_động để xây nên ngôi nhà mơ_ước . Hãy biến những nhọc_nhằn , buồn chán trong quá_trình xây_dựng nhà_ở trở_thành niềm say_mê , nhẹ_nhàng , vui_sướng ! ” .
Ngô_Trần_Công_Luận - Tổng_giám_đốc công_ty Nhà_đất đô_thị mới
Tiến_sĩ cơ_học đất đã thành ông chủ kinh_doanh nhà_đất
Đã chín năm anh trở_về từ đất_nước Anh quốc ( ĐH Oxford ) - một hành_trình mà anh tự gọi_là “ làm_công không ngơi_nghỉ ” . Trong đó có làm_công … thật_sự , khi tiến_sĩ bị nhiều nơi từ_chối nguyện_vọng làm_việc , anh chấp_nhận bán hàng cho một công_ty sơn , rồi làm phần_mềm gia_công với bạn_bè . Làm cần_cù , nghiêm_túc ! Những giấc_mơ chưa rõ_ràng nhưng anh đã thấy được một điều gì qua đó ? Sự hấp_dẫn của thương_trường ! Không_chỉ có khoa_học làm cho người_ta sáng_tạo hơn , làm_chủ những ý_tưởng kinh_doanh cũng cần những người dám đương_đầu .
Ngô_Trần_Công_Luận :
- HS giỏi vật_lý tỉnh Long_An
- Giải_thưởng nghiên_cứu khoa_học Eureka của Báo_Tuổi trẻ ( chương_trình Vì ngày_mai phát_triển )
- Bảo_vệ tiến_sĩ cơ_học đất DH Oxford ( Anh )
- Được các công_ty Anh mời ở lại trong lĩnh_vực thương_mại_hóa phần_mềm cho các trường đại_học về cơ_học đất
- Giám_đốc công_ty TNHH Nhã_Đạt với www . nhadat.com
- Tổng_Giám đốc hệ_thống công_ty Nhà_đất đô_thị mới gồm 9 văn_phòng tại TP.HCM , Hà_Nội , Cần_Thơ_Mới 5 năm trước thôi , trang_web nhadat . com đã làm giới buôn_bán nhà_đất TP.HCM phải ngạc_nhiên vì một ý_tưởng đẹp . Click vào đó , bạn sẽ thấy cả một không_gian “ họp chợ nhà_đất ” linh_đình trên mạng , muốn tìm sạp hàng , quy_hoạch , phân lô ... tùy_thích . Người tiên_phong mang giao_dịch điện_tử vào hệ_thống môi_giới nhà_đất không ai khác là Ngô_Trần_Công_Luận - anh đã quyết_định làm một tiến_sĩ - bán hàng , thành_lập ra công_ty TNHH Nhã_Đạt .
Một ý_tưởng thật đẹp nhưng anh “ bật_mí ” rằng Nhã đạt là một công_trình chưa đi đến thành_công , đúng hơn là " sém ... sập_tiệm " . Sẽ kết_thúc câu_chuyện “ khởi_sự doanh_nghiệp ” ở đây hay tìm cách “ vượt dòng_sông ” ? Từ một cậu_bé nghèo ở Long_An , đỗ xuất_sắc vào ĐH Bách_khoa TP.HCM , tự học Anh_văn mà đỗ học_bổng du_học , học cơ_học đất mà_lại được các công_ty Anh mời làm_việc về lập_trình , anh có_thể chưa thành_danh nhưng cũng chưa khi nào gục ngã dễ_dàng .
Thế_là Nhã_Đạt được quyết_định phát_triển thành Công_ty nhà_đất đô_thị mới bao_gồm cả nhadat . com và kinh_doanh các dự_án bất_động_sản , tiếp_thị , tư_vấn , nghiên_cứu thị_trường và cả đầu_tư nhà_đất . Từ 2-3 nhân_viên với 20 m 2 văn_phòng , trong 3 năm qua , hệ_thống công_ty anh đã lập 7 văn_phòng tại TP.HCM , 2 văn_phòng tại Hà_Nội và Cần_Thơ , với trên 100 nhân_viên có trình_độ chuyên_nghiệp . Công_ty của anh năm 2004 đã có_mặt trong tổng_cộng 99 dự_án nhà_đất và tham_gia hầu_hết những dự_án nhà_đất tại TP.HCM .
Nhưng anh không ngại_ngần khẳng_định trong bối_cảnh thị_trường nhà_đất đang đóng_băng , nay công_ty anh cũng không nằm ngoài những khó_khăn chung . Chỉ có_điều anh đã không còn “ run ” như thời “ mới khởi_nghiệp ” , anh sẽ “ vượt sông ” !
Làm_quen với Ngô_Trần_Công_Luận , bạn sẽ thấy tự_tin hơn với những điều tưởng không_thể mà biến thành có_thể . “ Lúc_nào tôi cũng chỉ cố cần_mẫn như một người làm_công . Mọi thứ trong môi_trường kinh_doanh tại VN đều đang biến_chuyển , một người bán bánh_mì vẫn có_thể bán bánh_mì tốt hơn . Nếu bạn đang định làm_gì , hãy làm nhanh lên ! ” - anh chia_sẻ . Anh còn mời_gọi : " Bạn trẻ nào có hứng_thú với việc phát_triển www . nhadat.com , hãy thử sức với chúng_tôi ! " .
& gt ; & gt ; Đi sẽ đến
TTO

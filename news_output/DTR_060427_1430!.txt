﻿ Một tuần , 3 lần trúng xổ_số
Chỉ trong vòng 1 tuần , cặp vợ_chồng may_mắn xứ Wales đã liên_tiếp 3 lần giành giải độc_đắc từ những con_số may_mắn , với tổng trị_giá bỏ_túi là 2,5 triệu bảng Anh ( gần 68 tỷ VN đồng ) .
Đầu_tiên , bà Jenny_Cooper , 49 tuổi , phó hiệu_trưởng của 1 trường trung_học trúng thưởng vẻn_vẹn được 10 bảng . Bà dùng hết số tiền đó mua thêm mấy tấm vé_số và thẻ cào khác .
Một trong số những tấm thẻ cào này “ rinh ” cho bà giải_thưởng thứ 2 , tuy món tiền thưởng khá nhỏ . Lần này bà mua vé lô - tô .
Và may_mắn thật_sự đã đến trong lần thứ 3 : giải độc_đắc trị_giá tới 2.470.696 bảng Anh , nghiễm_nhiên đưa gia_đình Cooper trở_thành triệu_phú .
Ông chồng Gareth , 54 tuổi , vốn là công_nhân đã nghỉ mất_sức cùng cô con_gái Hannah , 17 tuổi cho_biết : với số tiền kếch_xù này , họ sẽ tậu 1 trang_trại , 1 chiếc xe đời_mới và đi du_lịch xả_láng .
T.Vân Theo Annanova

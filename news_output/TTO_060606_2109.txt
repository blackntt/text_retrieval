﻿ Thu_hồi tiền " bao " con quan du_học
16 người được kết_luận là không đúng đối_tượng và không tham_gia xét tuyển theo quy_chế do chính Vietnam_Airlines ban_hành .
Ông Chấn khẳng_định , việc đài_thọ đi học với cam_kết khi trở_về làm_việc tại Vietnam_Airlines trong 25 năm là cách để hãng tạo nguồn nhân_lực trong tương_lai . Tuy_nhiên , người_phát_ngôn của Vietnam_Airlines lại tự mâu_thuẫn với chính mình khi buộc thừa_nhận : " Trong số đó có những em thi trượt đại_học ở Việt_Nam " . Theo nguồn tin riêng , tỷ_lệ này là 7 trong 16 người .
Theo quy_chế , ngoài các tiêu_chuẩn về học_lực , những sinh_viên được Vietnam_Airlines chi_trả học_phí trong thời_gian học_tập ở nước_ngoài phải là con_em của cán_bộ trong ngành . Nhưng , Tổng_giám_đốc Nguyễn_Xuân_Hiển vẫn ký quyết_định cử_tuyển một_số con của cán_bộ cấp cao trong ngành tư_pháp ... du_học .
Ngay sau khi báo_chí lên_tiếng , ngày 2/6 , Tổng_giám_đốc Nguyễn_Xuân_Hiển đã ký thông_báo chuyển chế_độ đài_thọ kinh_phí học_tập của 16 sinh_viên sang chế_độ tự_túc . " Các học_viên có trách_nhiệm nộp toàn_bộ kinh_phí do Tổng_công_ty hàng_không Việt_Nam đã đài_thọ " , văn_bản nêu .
Ông Hiển cũng được xác_định phải chịu trách_nhiệm chính trong việc cử_tuyển sai quy_định này .
Quyết_định trên được phê_duyệt trước ngày ông Hiển sang Pháp giải_quyết vụ_kiện với một luật_sư người Italia , mà theo phán_quyết của toà phúc_thẩm Paris , Vietnam_Airlines phải trả hơn 5,2 triệu euro vì thua kiện .
Cũng trong chiều 6/6 , việc Vietnam_Airlines mua 4 máy_bay tầm xa Boeing 777 nhưng lại sử_dụng động_cơ tầm trung cũng được đặt ra với người_phát_ngôn Nguyễn_Tấn_Chấn . Vietnam_Airlines cho rằng , vì chủ_yếu khai_thác các tuyến bay tới tầm trung và tầm ngắn ( Đông_Bắc Á , Australia , châu Âu ) nên việc không mua động_cơ tầm xa ( chủ_yếu đi Mỹ ) là phù_hợp . " Chúng_tôi đã tính bài_toán kinh_tế có lợi nhất , chọn loại có chi_phí thấp nhất " , ông Chấn cho_biết . Tuy_nhiên , ông không giải_thích nổi vì_sao Vietnam_Airlines lại mua 4 Boeing 777 ( loại máy_bay tầm xa ) mà không sử_dụng loại máy_bay tầm trung , để tiết_kiệm chi_phí . Đây cũng là 4 chiếc Boeing 777 mà Việt_Nam có trong tay . 6 chiếc cùng chủng_loại đang khai_thác là đi thuê . " Bộ_phận kỹ_thuật sẽ giải_trình sau với các nhà_báo " , ông Chấn thoái_thác .
14 người có con học ở Nga , Ukraina được Vietnam_Airlines đài_thọ :
1 . Ông Dương_Thanh_Biểu , Phó_viện_trưởng VKSND Tối_cao 2 . Ông Vũ_Văn_Vượng , trường hàng_không VN 3 . Ông Nguyễn_Tấn_Chấn , Văn_phòng đối_ngoại VietnamAirlines
4 . Bà Đặng_Thị_Thứ , Đoàn bay 919 5 . Ông Nguyễn_Văn_Hưng , Giám_đốc Xí_nghiệp A76 6 . Ông Phương_Minh_Hoà , thiếu_tướng , Phó tư_lệnh quân_chủng Phòng_không - Không_quân
7 . Ông Trịnh_Huy_Quách , Phó chủ_nhiệm Uỷ_ban kinh_tế và ngân_sách của Quốc_Hội 8 . Ông Uông_Chu_Lưu , Bộ_trưởng Tư_pháp 9 . Ông Lê_Thanh_Hùng , Đài_truyền_hình Việt_Nam
10 . Ông Đinh_Chí_Lợi , cán_bộ Bộ Nội vụ 11 . Ông Nguyễn_Tam_Khoa , nguyên trưởng_ban tổ_chức cán_bộ 12 . Ông Phạm_Doãn_Hồng , Giám_đốc Công_ty Xuất_nhập_khẩu hàng_không
13 . Ông Nguyễn_Văn_Hồng , Xí_nghiệp A76 14 . Ông Lê_Quốc_Chân , làm nghề thuốc đông_y
Hai người có con đi Mỹ
1 . Ông Trần_Văn_Mai , Giám_đốc Xí_nghiệp A75 2 . Ông Võ_Thanh_Quang ( Bộ_chỉ_huy quân_sự tỉnh Đồng_Nai )
Theo Vn Express

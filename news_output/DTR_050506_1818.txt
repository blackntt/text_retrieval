﻿ Cho phá_sản ba nhà_máy đường
Ba nhà_máy đường Quảng_Bình , Quảng_Nam và Bình_Thuận sẽ phá_sản vì số lỗ đã vượt quá vốn của nhà_máy .
Theo Tổng công_ty Mía đường 1 ( đơn_vị chủ_quản của Nhà_máy Đường_Quảng_Bình ) và Tổng_công_ty Mía đường 2 ( chủ_quản nhà_máy đường Quảng_Nam và Bình_Thuận ) , phải chọn phương_án phá_sản vì các nhà_máy này lỗ quá lớn , kể_cả khi được xử_lý tài_chính theo quyết_định 28 của Thủ_tướng Chính_phủ vẫn tiếp_tục lỗ .
Được biết , hai nhà_máy đường Quảng_Bình và Quảng_Nam được xây_dựng ở nơi không có đất trồng mía , không có nguyên_liệu hoạt_động nên đã phải đóng_cửa từ hai năm qua . Phương_án di_dời hai nhà_máy này vào đồng_bằng sông Cửu_Long cũng phá_sản vì khu_vực này cũng đang thiếu mía .
Nhà_máy Đường_Bình_Thuận dù đã được thay_đổi cơ_quan chủ_quản , được tổ_chức lại nhưng vụ 2004-2005 cũng chỉ hoạt_động được 30% công_suất . Theo Tuổi trẻ

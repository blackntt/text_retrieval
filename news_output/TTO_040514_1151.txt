﻿ Hàng_không châu Âu : thị_trường khó nuốt lại đông người
Với slogan quảng_cáo " Bay khác đi ! " , hãng hàng_không JetGreen Airways cung_cấp dịch_vụ với giá rất rẻ , nhưng có chỗ ngồi rộng , giải_trí trên máy_bay và phục_vụ thức_ăn miễn_phí . Chuyến bay đầu_tiên của hãng cất_cánh vào ngày 4 - 5 .
Tuy_nhiên , công_ty Ireland này đã không kham nổi lỗ_lã chỉ sau một tuần hoạt_động và tuyên_bố đóng_cửa khi đã bán ra 40 nghìn vé .
Lãnh_đạo công_ty lên_tiếng xin_lỗi các hành_khách đã lỡ mua vé khứ_hồi nhưng khách_hàng không_thể_nào liên_lạc được với ban giám_đốc sau ngày công_ty sập_tiệm .
Hồi tháng 1 , hãng hàng_không Ireland , Jetmagic , cũng tuyên_bố phá_sản do cạnh_tranh không nổi sau chưa đầy một năm hoạt_động . Hãng hàng_không quốc_gia Ireland , Aer_Lingus , cũng đã phải chuyển sang dịch_vụ bay giá rẻ sau khi phá_sản hồi năm 2001 .
ANH_QUÝ ( Theo Reuters )

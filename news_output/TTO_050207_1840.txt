﻿ Kinh_tế Việt_Nam và điều_ước trước thềm năm mới
Để làm được điều này , một lượng công_việc rất lớn sẽ phải hoàn_thành trong thời_gian tương_đối ngắn với ít_nhất hai phiên đàm_phán đã phương và hoàn_tất đàm_phán song_phương với 21 thành_viên của WTO .
Tuy_nhiên đấy không phải là việc không hoàn_thành được . Cái cơ_bản là ngoài việc tiếp_tục hoàn_thiện hệ_thống pháp_lý của chúng_ta phù_hợp với đòi_hỏi của cuộc_chơi này , các DN_VN cũng chuẩn_bị sẵn_sàng để bước vào sân_chơi đó .
Nhìn thấy được sự chuẩn_bị tích_cực , sự sẵn_sàng của các DN_VN cho " cuộc_chơi " mới ở " sân_chơi " lớn nhất thế_giới này cũng là một mong_muốn lớn của tôi . Các DN sẵn_sàng bước vào_cuộc cạnh_tranh với các DN nước_ngoài rồi thì việc đàm_phán sẽ thuận_lợi hơn nhiều .
Trần_Xuân_Giá , Trưởng_ban Nghiên_cứu của Thủ_tướng : Bớt nhiều các " giấy_phép con " cho người_dân
Ông Trần_Xuân_Giá_Sau hơn 4 năm thi_hành Luật_Doanh nghiệp , chúng_ta đã bỏ được một số_lượng đáng_kể các loại giấy_phép con trong sản_xuất , kinh_doanh của DN , nhưng trong cuộc_sống bình_thường của mỗi người_dân thì những " giấy_phép con " - những thủ_tục hành_chính không cần_thiết vẫn còn rất nhiều .
Chính vì_vậy , tôi mong_muốn trong năm Ất Dậu này , những phiền_hà trong thủ_tục hành_chính trên tất_cả các mặt của cuộc_sống , từ thủ_tục đầu_tiên là " khai_sinh " cho_đến thủ_tục cuối_cùng là " khai_tử " cần phải được " bới " ra hết , xoá_bỏ hết để người_dân không còn bị nhũng_nhiễu .
Trong cuộc đấu_tranh này , tôi mong Tuổi_Trẻ sẽ là lực_lượng tham_gia tích_cực nhất trong việc " bới " ra những loại " giấy_phép con " không cần_thiết đó , tương_tự như đã tham_gia ý_kiến giúp xoá_bỏ những giấy_phép con cho sản_xuất kinh_doanh .
NHẬT_LINH ghi

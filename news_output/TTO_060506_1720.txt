﻿ Mỹ thành_lập liên_minh ủng_hộ Việt_Nam gia_nhập WTO
Các tập_đoàn công_ty hàng_đầu của Hoa_Kỳ tham_gia liên_minh bao_gồm Boeing , Cargill , Citigroup , FedEx , Ford_Motor , GE , JC Penny , New_York_Life , Nike , Time_Warner , Phòng_Thương mại Mỹ , Hội_đồng Ngoại_thương quốc_gia ...
Liên_minh cũng ra_mắt website kêu_gọi các tổ_chức , nghiệp_đoàn , công_ty Mỹ tiếp_tục tham_gia liên_minh tại địa_chỉ http : / / www.usvtc.org/coalition.asp . Đây là dấu_hiệu cho thấy đàm_phán Việt - Mỹ về việc Việt_Nam gia_nhập WTO đang ở giai_đoạn cuối_cùng và cộng_đồng doanh_nghiệp Mỹ đang tích_cực chuẩn_bị cho thời_kỳ hậu WTO của Việt_Nam .
Theo Thanh_Niên

﻿ Sân_Thanh_Hóa căng nhất
Trong bảy cặp đấu của vòng này , cuộc đụng_độ giữa đội đang dẫn_đầu giải là chủ nhà Thanh_Hóa và Huda_Huế ( hạng ba ) được xem là gay_cấn nhất . Cửa lên hạng chuyên_nghiệp với Thanh_Hóa đang cận_kề , do_vậy họ không được phép thua để tránh bị nhóm xếp kế soán ngôi đầu_bảng .
Huda_Huế cũng không được phép thua trận này , bởi ngay sau lưng họ là một loạt đội bóng đang chực_chờ lật_đổ vị_trí thứ_ba .
Vị_trí chót bảng khó thay_đổi với CLB_TP . HCM khi đến làm_khách trên sân của đội đang đứng nhì bảng Đồng_Tháp ( 34 điểm ) .
S . H .

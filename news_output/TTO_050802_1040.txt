﻿ Phim_Sympathy for Lady_Vengeance lập kỷ_lục tại Hàn_Quốc
Trong 4 ngày chiếu đầu_tiên với 370 màn_ảnh , Sympathy for Lady_Vengeance đã thu_hút khoảng 1,45 triệu lượt khán_giả đến rạp ( riêng Seoul khoảng 387 ngàn lượt ) , vượt qua kỷ_lục của phim War of the Worlds ( đạo_diễn Steven_Spielberg ) lập trước đó với 1,43 triệu khán_giả trong 4 ngày đầu chiếu tại Hàn_Quốc .
Sympathy for Lady_Vengeance cũng là phim Hàn_Quốc duy_nhất có_mặt trong top 6 phim ăn_khách nhất tuần qua , xếp trên các bộ phim của Hollywood là The_Island ( Ewan_Mc Gregor , Scarlette_Johansson đóng vai chính ) , Stealth ( Jamie_Foxx đóng vai chính ) , Robots , War of the Worlds và Madagascar .
Phần cuối của “ bộ ba phim báo_thù ” do Park_Chan_Wook đạo_diễn này được xem là có khởi_đầu thành_công nhất về mặt doanh_thu so với Sympathy for Mr . Vengeance ( năm 2002 ) và Old_Boy ( năm 2003 ) .
Với sự góp_mặt của Lee_Young_Ae , phim được hi_vọng sẽ vượt qua thành_công của một “ quả bom tấn ” khác của ông là JSA - Joint_Security_Area ( cũng do Lee_Young_Ae đóng vai chính ) - phim ăn_khách nhất tại Hàn_Quốc năm 2000 với 5,8 triệu lượt khán_giả .
Hiện_tại , kỷ_lục phim Hàn_Quốc ăn_khách nhất mọi thời_đại thuộc về bộ phim Taegukgi ( Jang_Dong_Gun và Won_Bin đóng vai chính ) với hơn 11,7 triệu khán_giả .
T.MINH ( Theo KFCC )

﻿ 3 học_bổng lớn của quỹ Giáo_dục Singapore
Cơ_quan Xúc_tiến Giáo_dục Singapore đang dành 3 suất học_bổng cho HSSV Việt_Nam , mỗi suất trị_giá 10.000 USD . Học_bổng sẽ được trao dưới hình_thức hỗ_trợ chi_phí sinh_hoạt để đảm_bảo việc học_tập và sinh_hoạt của sinh_viên ở Singapore .
Đối_tượng đăng_kí Học_bổng Giáo_dục Singapore là những học_sinh Việt_Nam đang học lớp 12 hoặc sinh_viên đại_học năm thứ nhất với thành_tích học_tập xuất_sắc , tích_cực tham_gia vào các hoạt_động ngoại_khoá và hoạt_động xã_hội và được chấp_nhận vào các khóa đại_học tại các trường công_lập của Singapore là trường ĐH Quốc_gia Singapore ( National_University of Singapore - NUS ) , trường ĐH Công_nghệ Nanyang và ĐH Quản_lí Singapore ( Singapore_Management_University - SMU ) trong năm_học 2006 - 2007 .
Cơ_quan Xúc_tiến giáo_dục Singapore tiếp_nhận hồ_sơ đăng_kí học_bổng cho tới ngày 31/5 .
Hồ_sơ đăng_ký học_bổng phải bao_gồm giấy gọi nhập_học cho niên_khóa 2006 - 2007 tại 1 trong 3 trường công_lập nêu trên .
Nơi tiếp_nhận hồ_sơ :
Trung_tâm Thông_tin Singapore ( SVC )
Tầng 4 , toà nhà Parkson - Saigontourist
45 Lê_Thánh_Tôn , Quận 1 , TPHCM
Liên_hệ qua điện_thoại :
TPHCM : Tel : 08 . 8277646 , Fax : 08 . 8277648 Hà_Nội : Tel : 04 . 8223914 , Fax : 04 . 8223915
Ông Darren_Oh , Giám_đốc Khu_vực Đông_Dương , Tổng_cục Du_lịch Singapore cho_biết : Cơ_quan Xúc_tiến Giáo_dục Singapore sẽ tiến_hành xét hồ_sơ ngay sau khi hết hạn nhận hồ_sơ . Các ứng_viên xuất_sắc nhất sẽ được tiến_hành phỏng_vấn để chọn ra 3 người phù_hợp nhất để trao học_bổng . Các bạn học_sinh sinh_viên và phụ_huynh có_thể liên_hệ Trung_tâm Thông_tin Singapore để lấy đơn đăng_ký . Mai_Minh - Hồng_Hạnh

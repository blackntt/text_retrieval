﻿ Nông_nghiệp 2006 : Chuyển_dịch cơ_cấu kết_hợp thâm_canh
Theo mục_tiêu đề ra của Bộ NN & amp ; PTNT , diện_tích gieo_trồng lúa năm 2006 vẫn giữ ở mức 7,35 triệu ha , nhưng sản_lượng lương_thực phải đạt 36 triệu tấn , tăng 200.000 tấn so với năm 2005 , trên cơ_sở mở_rộng diện_tích áp_dụng mô_hình " ba tăng , ba giảm " .
Ngoài_ra , mục_tiêu phát_triển ngành chăn_nuôi trong năm nay phải tạo ra bước chuyển_biến mới theo hướng phát_triển hình_thức chăn_nuôi tập_trung công_nghiệp và bán công_nghiệp gắn với phát_triển giết_mổ , chế_biến tập_trung ; tập_trung phát_triển mạnh đàn lợn , đàn bò và trâu thịt .
H . ĐĂNG

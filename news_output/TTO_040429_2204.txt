﻿ Liberty và VASS cung_cấp bảo_hiểm trách_nhiệm sản_phẩm
Thomas O’ Dore , trưởng đại_diện của Liberty_Mutual tại Hà_Nội , hàng_hóa VN xuất_khẩu sang Mỹ đang tăng nhanh và kèm theo đó các doanh_nghiệp VN đang đối_diện với những rủi_ro liên_quan đến trách_nhiệm sản_phẩm .
Ông cho_biết tòa_án Mỹ đã từng ra phán_quyết buộc một nhà xuất_khẩu phải bồi_thường hàng triệu USD cho một người bị ngã vì một cái ghế bị hỏng . “ Nếu_không được bảo_hiểm , người sản_xuất ghế có_thể mất cả cơ_nghiệp chỉ vì thiếu một cái ốc_vít trên ghế mà anh_ta làm ra ” , ông O’ Dore nói .
NHƯ_HẰNG

﻿ Khánh_Hòa : người tiêu_dùng kiện công_ty cấp_thoát_nước
Theo đơn và ông Sơn trình_bày , gia_đình ông có bốn người_lớn và một em bé , lắp đồng_hồ nước năm 1999 với tiền nước khoảng 60.000 đồng/tháng . Đến năm 2001 tiền nước tăng vọt lên 150.000 đồng/tháng . Từ năm 2002 đến nay , ông phải nộp từ 200.000 đồng đến trên 300.000 đồng/tháng .
Đặc_biệt tháng 2-2004 chỉ_số nước là 287m3 . Công_ty Cấp_thoát_nước đã cử nhân_viên kỹ_thuật xuống kiểm_tra và phát “ giấy_báo thay đồng_hồ nước ” nhưng lại yêu_cầu phải làm đơn xin thay đồng_hồ mới , đồng_thời vẫn phát giấy_báo tiền nước số 0005548 ngày 12-3-2004 với số tiền phải nộp 513.750 đồng cho một tháng sử_dụng nước ( tháng 2-2004 ) .
TRẦN_MINH_NGỌC

﻿ Bắt giam nguyên phó_giám_đốc Sở NN-PTNT Phú_Yên
Hai đối_tượng này bị khởi_tố vào ngày 14-11-2005 về hành_vi cố_ý làm trái các qui_định của Nhà_nước gây hậu_quả nghiêm_trọng trong vụ_án rút_ruột công_trình chống ngập_lụt TP Tuy_Hòa . Theo kết_quả điều_tra ban_đầu , Lê_Mao và Nguyễn_Khánh_Tho đã thông_đồng với nhà_thầu là Xí_nghiệp Xây_lắp điện nước 3 ( thuộc Công_ty Cơ_khí xây_dựng và lắp máy_điện nước Hà_Nội ) cùng tham_ô tại gói_thầu số 4 có giá_trị gần 21 tỉ đồng .
Liên_quan đến vụ_án trên , đến nay Công_an Phú_Yên đã bắt giam tám trong số 11 bị_can bị khởi_tố .
TẤN_LỘC

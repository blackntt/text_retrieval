﻿ Thắng 1 - 0 trước Ajax : Inter đã không cầu_hòa
Các cầu_thủ Inter chúc_mừng Stankovic ( NLĐO ) - Trong trận đấu muộn vòng 1/16 Champions_League , Inter_Milan đã có trận thắng 1 - 0 trước Ajax ( Hà_Lan ) để lọt tiếp vào vòng Tứ_kết .
Sau trận hòa 2-2 ở lượt_đi trên sân Ajax , Inter chỉ cần hòa 0 - 0 hoặc 1-1 là đủ để_trở thành_đội bóng thứ 3 của Italia tiếp_tục tham_gia vòng đấu_loại 8 đội .
Tuy_nhiên , đội bóng áo sọc xanh đen thành Milan đã thi_đấu với tinh_thần quyết_thắng .
Ngay từ đầu trận , Inter dâng lên tấn_công dồn_dập .
Phút 12 , tiền_đạo Martins đã tung cú sút dội khung_thành đội khách Ajax .
Phút 27 , Lindenbergh ( Ajax ) để bóng chạm tay_trong vòng cấm_địa trong nỗ_lực cản_phá pha tấn tấn_công của Inter . Cầu_thủ người Brazil_Adriano ( Inter ) lại sút hỏng quả phạt_đền khi bóng vượt qua tay thủ_môn Stekelenburg nhưng lại chệch khung_thành Ajax trong gang_tấc .
Dấu_ấn trận đấu được Stankovic xác_lập vào phút 57 . Nhận đường chuyền dài từ phần sân_nhà , Stankovic loại_bỏ hậu_vệ Maduro , tung cú sút cực mạnh hạ gục thủ_thành Ajax .
Sau khi bị thủng lưới , HLV Ajax_Danny_Blind thay liền 2 cầu_thủ Rosales , Rosenberg bằng Babel , Charisteas vào phút 72 và 74 .
Ajax bắt_đầu tấn_công nhiều hơn , trong khi Inter lui về bảo_toàn tỉ_số .
Cơ_hội nguy_hiểm nhất mà Ajax tạo được về phía khung_thành Toldo là cú sút vọt xà của Babel ở phút 82 .
Như_vậy , Inter đã giành chiến thắngchung cuộc 3-2 và sẽ gặp đội bóng Tây_Ban_Nha_Villarreal ở vòng tứ_kết .
Trận lượt_đi vòng tứ_kết sẽ diễn ra_vào ngày 28 , 29-3 và trận lượt_về sẽ diễn ra ngày 4 , 5-4 .
Các cặp đấu khác vòng tứ_kết :
Arsenal - Juventus
Lyon - AC Milan_Benfica - Barcelona
Trần_Nam

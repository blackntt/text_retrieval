﻿ Thuế_thu_nhập của nghệ_sĩ : cần điều_chỉnh những bất_hợp_lý
Đầu_tiên các nghệ_sĩ băn_khoăn : mức thu dựa trên thu_nhập khởi_điểm 3 triệu đồng là rất eo_hẹp cho họ . Bởi đặc_trưng nghề_nghiệp của nghệ_sĩ phải cần trang_bị son_phấn , trang_phục rất tốn_kém , gọi_là bồi_dưỡng thanh_sắc . Có_khi một bộ trang_phục biểu_diễn đã tốn cả triệu đồng .
NSƯT Việt_Anh nói : " Các cơ_sở sản_xuất thì được khấu_hao máy_móc , còn chúng_tôi lấy thanh_sắc của mình làm phương_tiện sản_xuất thì không được khấu_hao gì cả . Chưa kể các chi_phí gián_tiếp khác . Tôi đề_nghị mức khởi_điểm là 10 triệu đồng " .
" Nên chiếu_cố cho những nghệ_sĩ lớn_tuổi , hoặc trong bộ_môn hát_bội , cải_lương vì lâu_lâu chúng_tôi mới có một sô . Thu_nhập của nghệ_sĩ thuộc loại không thường_xuyên , thí_dụ tháng này nhận được một vai đóng phim 5 triệu đồng , nhưng 3 tháng sau mới có vai nữa , vậy mỗi tháng chỉ sống bằng 1,5 triệu , nên tính thuế trên con_số 5 triệu thì tội_nghiệp chúng_tôi quá " .
NSND Diệp_Lang

﻿ Điều_kiện tài_chính khi làm thủ_tục xin visa du_học Đức
- Trả_lời của chị Hải_Hằng , Giám_đốc chương_trình , công_ty tư_vấn Quốc_tế Cát_Tường :
Trong trường_hợp một trong hai người có visa du_học và đã sang Đức du_học , người đó có_thể bảo_lãnh cho người còn lại xin visa sang Đức , nhưng chỉ là visa dạng bảo_lãnh , chỉ được lưu_trú tối_đa tại Đức trong thời_hạn 3 tháng . Trong thời_gian 3 tháng đó , nếu bạn xin được thư mời học của một trường nào_đó thì vẫn phải quay về Việt_Nam để xin lại visa du_học .
- Về điều_kiện tài_chính , bạn vẫn phải tuân theo các điều_kiện về tài_chính theo yêu_cầu :
+ Đóng học_phí , phí ký_túc_xá và đầy_đủ các khoản phí khác cho trường .
+ Tài_khoản 7.000 euro mở tại một ngân_hàng của Đức đứng_tên người đi học .
Đây là điều_kiện cần_thiết đối_với mỗi cá_nhân khi làm thủ_tục xin visa du_học ở Đức . Nếu đi hai người , bạn phải đáp_ứng điều_kiện này cho cả hai người .
TTO

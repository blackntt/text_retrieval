﻿ Cá chết trên sông Sài_Gòn do chất_độc từ củ_mì
Hiện_tượng cá chết hàng_loạt diễn ra ngày 17-4 vừa_qua cũng đúng vào thời_điểm xả nước_thải ra sông Sài_Gòn của Nhà_máy chế_biến bột mì Miwon VN .
Trung_tâm Quốc_gia quan_trắc cảnh_báo môi_trường và phòng_ngừa dịch_bệnh thủy_sản khu_vực Nam_bộ vừa báo_cáo như trên đến các cơ_quan hữu_trách về nguyên_nhân khiến cá chết trên sông Sài_Gòn .
Một chuyên_gia cho_biết cyanure là chất_độc cực mạnh có trong củ_mì tươi , chất này có khả_năng hòa_tan trong nước , đối_với người , chỉ cần đưa vào cơ_thể một lượng cyanure nhỏ ( 0,15 - 0,2 gram ) là đã có_thể gây chết . Một thành_viên trong đoàn khảo_sát nói đoàn đã kiến_nghị cưỡng_chế , giải_tỏa các hộ nuôi cá bè trên lòng hồ Dầu_Tiếng và cưỡng_chế việc xả nước_thải của những nhà_máy trên sông để bảo_vệ nguồn nước .
THU_THẢO

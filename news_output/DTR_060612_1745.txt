﻿ Lật tàu_hỏa ở Israel , 150 người bị_thương
Một vụ tai_nạn tàu_hoả nghiêm_trọng vừa xảy_ra ở miền trung Israel hôm_nay 12/6 khi một đoàn tàu chở khách đã đâm phải một chiếc xe_tải làm 2 toa tàu bị lật , khiến hàng trăm người bị_thương .
Cảnh_sát Israel cho_biết đoàn tàu này chở 200 hành_khách từ thủ_đô Tel_Aviv tới Haifa đã đâm vào một chiếc xe_tải đang cố vượt qua đường_ray tại thành_phố Netanya .
Người lái xe_tải này đã may_mắn thoát chết khi kịp nhảy ra khỏi xe . Cảnh_sát đang điều_tra xem liệu đây có phải là một vụ khủng_bố hay không và phủ_nhận thông_tin nói rằng đã có người chết .
Một nhân_chứng tại hiện_trường cho_biết đã có những nỗ_lực nhằm thông_báo cho người lái tàu biết để dừng lại nhưng không_thể . Lực_lượng cứu_hộ đã được cử tới hiện_trường , chuyển những người bị_thương vào bệnh_viện .
Tháng 6/2005 , một đoàn tàu cũng đã đâm phải một chiếc xe_tải ở phía nam Tel_Aviv làm 7 người thiệt_mạng và hơn 150 người khác bị_thương .
Nguyên_Hưng_Theo AP , Reuters

﻿ Iran : sẽ đàm_phán với Mỹ nhưng không ngưng làm_giàu uranium
Vào hôm_qua , Mỹ tuyên_bố nước này sẵn_sàng đàm_phán với Iran với điều_kiện Iran ngưng ngay các hoạt_động hạt_nhân nhạy_cảm . Các nhà_phân_tích nói đây là một thay_đổi lớn trong chính_sách của Mỹ và là một nỗ_lực nhằm tìm_kiếm giải_pháp tháo_gỡ cho vấn_đề hạt_nhân của Iran .
“ Chúng_tôi sẽ không từ_bỏ quyền_lợi đương_nhiên được làm_giàu uranium của đất_nước mình . Nhưng chúng_tôi sẵn_sàng tổ_chức các cuộc đàm_phán về các vấn_đề mà hai bên quan_tâm ” , Ngoại_trưởng Manouchehr_Mottaki nói .
Ông cũng thêm rằng nếu Mỹ “ muốn có bất_kỳ thay_đổi nào trong tình_hình hiện_tại , họ phải thay_đổi cách cư_xử của mình và nên cư_xử đúng_đắn và hợp_lý ” .
Trong một diễn_biến khác liên_quan đến vấn_đề Iran , các cường_quốc thế_giới đang chuẩn_bị nhóm_họp tại Vienna để thảo_luận về các điều_kiện khuyến_khích Iran ngưng làm_giàu uranium .
TƯỜNG_VY ( Theo AP , BBC )

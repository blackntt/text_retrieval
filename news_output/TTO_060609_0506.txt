﻿ Kiến_nghị dừng thực_hiện dự_án thủy_lợi Ô_Môn - Xà_No
Hầu_hết các nhà_khoa_học có_mặt tại hội_thảo đều cho rằng việc thực_hiện dự_án Ô_Môn - Xà_No là áp_đặt và không phù_hợp với ĐBSCL . Với những phân_tích cụ_thể , các nhà_khoa_học đề_nghị phía thực_hiện dự_án ( Bộ NN& amp ; PTNT ) cần phải cân_nhắc cặn_kẽ các ảnh_hưởng tiêu_cực khi dự_án hoàn_thành . Thực_tế cho thấy nhiều dự_án trước đó như : ngọt hóa Ba_Lai , Nam_Mang_Thít , bán_đảo Cà_Mau ... đang khiến người_dân lao_đao , khổ_sở .
Tiểu dự_án này thuộc dự_án phát_triển thủy_lợi ĐBSCL được đầu_tư 600 tỉ đồng và đang được thi_công .
PHƯƠNG_NGUYÊN

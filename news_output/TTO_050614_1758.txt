﻿ Sạt_lở bờ sông Sài_Gòn
Cùng_với cát , có 2 xáng múc , một chiếc 36 tần , một chiếc 45 tấn và một dàn máy rửa cát cũng đã chìm trong đợt sạt_lở này .
Tại hiện_trường , chúng_tôi ghi_nhận , đất vẫn còn tiếp_tục sạt và sâu bên trong nhiều vết nứt đã xuất_hiện . Theo số_liệu đo_đạc của cán_bộ Tổ trật_tự đô_thị phường Linh_Đông , diện_tích của vựa cát bị sạt lên đến 3000m 2 đồng_thời kéo_theo 1200m 2 của khu đất vườn bên_cạnh .
Bà Võ_Thị_Bích_Diễm , chủ_tịch UBND phường Linh_Đông cho_biết khu_vực vựa cát đã từng xảy_ra sạt_lở khoảng 500m 2 , cuốn_trôi một căn nhà cấp 4 và cách đó vài trăm mét , một dải đất khá rộng của công_viên_nước Saigon water park khoảng 1000m 2 cũng trong tình_trạng tương_tự . Sau đó , chính_quyền đã phải huy_động gần 300 lao_động để đắp một bờ bao chạy dọc sông để chống sạt .
Giải_thích cho việc sạt_lở sáng nay , ông Nguyễn_Văn Út cho_biết vào đêm trước ( 13-6 ) , mặc_dù cát đã đầy nhưng do salan cát cập bến cần giải_phóng phương_tiện nên 2 chiếc xáng đã ra sát bờ và đến sáng thì xảy_ra sự_cố như trên .
Nhiều người_dân rất thắc_mắc vì_sao khu_vực này được cảnh_báo có nguy_cơ sạt_lở cao nhưng ông Út vẫn có giấy_phép của phòng Kinh_tế quận Thủ_Đức .
TRẦN_CHÁNH_NGHĨA

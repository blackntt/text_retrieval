﻿ Huy_Khánh : May vì lấy_được vợ hiểu mình !
" Lấy vợ hơn tuổi lại giàu_có , nếu là người bình_thường thì cũng chẳng ai bàn_tán , nhưng là nghệ_sĩ thì hoàn_toàn khác ... " , nổi_danh từ bộ phim " Dốc tình " chàng diễn_viên đẹp_trai Huy_Khánh tâm_sự .
Lên chức bố , anh cảm_thấy cuộc_sống của mình thay_đổi như_thế_nào ?
Khi chưa cưới vợ tôi không phải lo_nghĩ nhiều , đi chơi thoải_mái với bạn_bè , nhưng từ lúc có vợ phải hạn_chế việc này và suy_nghĩ nhiều hơn . Công_việc phải lựa_chọn xem có phù_hợp với mình hay không và có ảnh_hưởng gì đến gia_đình không . Và tất_nhiên là phải làm_việc nhiều hơn .
Các nghệ_sĩ khác hay quan_niệm lập gia_đình sớm sẽ ảnh_hưởng đến công_việc , đến tên_tuổi , sự mến_mộ của khán_giả . Nhưng tôi không nghĩ như_vậy , mà cho rằng người nghệ_sĩ được nhiều người mến_mộ là nhờ tài_năng chứ không phải vì người đó còn độc_thân hay đã lập gia_đình .
Anh_chị đã có dự_định gì cho cậu con_trai cưng của mình . Nối_nghiệp bố hay_là làm kinh_doanh như mẹ ?
Tôi và bà_xã vẫn chưa có dự_định gì cho cậu con_trai cưng của chúng_tôi cả . Nhiều lúc vui , thường nói là muốn sau_này con làm bác_sĩ để về già còn chữa bệnh cho bố_mẹ . Còn theo nghệ_thuật nó rong_ruổi , phiêu_lưu lắm .
Ngoài người vợ xinh_đẹp và cậu con_trai kháu_khỉnh , anh còn bận_tâm đến gì nhất ?
Tôi cảm_thấy may_mắn vì lấy_được người vợ hiểu công_việc của mình và rất ủng_hộ tôi . Bà_xã nói tôi cứ thoải_mái làm nghề , không cần bận_tâm đến chuyện gia_đình và khi nào cảm_thấy không thích nghệ_thuật nữa thì hãy quay về phụ_giúp việc kinh_doanh cho cô ấy .
Lấy vợ hơn tuổi lại giàu_có , nếu là người bình_thường thì cũng chẳng ai bàn_tán , nhưng là nghệ_sĩ thì hoàn_toàn khác .
Với tôi , giả_dụ lấy người bằng tuổi thì có_thể đã chuyển nghề khác . Lấy vợ hơn tuổi mà suy_nghĩ của họ trẻ_trung , thoải_mái và xinh_đẹp nữa thì không có gì để phàn_nàn cả .
Anh_chị cảm_thấy hợp nhau ở điểm nào nhất ?
Chúng_tôi hợp nhau ở chỗ rảnh_rỗi thì đi mua_sắm , nhiều lúc trong túi còn ít tiền nhưng vẫn muốn ăn thật ngon , ăn cho khi nào hết tiền thì_thôi .
Chúng_tôi đi chơi bất_kỳ lúc_nào không kể ngày thường hay thứ Bảy , Chủ_nhật . Trừ những ngày phải đi làm .
Vai_diễn trong Dốc tình có ý_nghĩa thế_nào đối_với anh ?
Đúng là bộ phim Dốc tình đã ảnh_hưởng rất lớn đến sự_nghiệp của tôi vì đây là bộ phim được sự ủng_hộ khá cao của khán_giả . Sau phim này ra đường nhiều người cứ gọi tôi bằng cái tên phim Dốc tình . Tôi nghĩ đây là cái mốc quan_trọng trong sự_nghiệp của tôi .
Sau phim , giờ thấy anh chuyển qua đóng_kịch ?
Tôi vừa đóng xong bộ phim_truyền_hình 10 tập , Mảnh vỡ của đạo_diễn Võ_Việt_Hùng và đang tham_gia vở kịch Sống thử tại sân_khấu 6B Võ_Văn_Tần . Ưu_tiên số 1 của tôi là cho điện_ảnh . Ở trường tôi học kịch nhiều hơn điện_ảnh nên muốn trong thời_gian rảnh trau_dồi thêm lĩnh_vực này . Sau khi diễn xong Chuyến tàu đến thiên_đường và Sống thử tôi thấy rất tự_tin , muốn được tham_gia một_số vở khác tại đây .
Anh là fan của đội nào tại World_Cup lần này ?
Tôi rất thích đội_tuyển Italia và Brazil nhưng kết Italia hơn . Anh thấy không , hôm_nay tôi mặc áo của đội_tuyển Italia để ủng_hộ họ . Tôi thích xem World_Cup với bạn_bè , vì xem ở nhà thì mau buồn_ngủ lắm . Bà_xã không thích xem bóng_đá nhưng cũng hỏi tôi dự_định xem World_Cup ở đâu . Theo Thể_Thao & amp ; Văn_Hoá

﻿ Các tổn_thương mô miệng ở trẻ_em
Chẩn_đoán dựa vào kết_quả soi trực_tiếp phết mẫu hoặc nuôi_cấy mẫu lấy từ sang thương . Bệnh nấm candida albicans miệng - họng thường tự khỏi ở trẻ khỏe_mạnh , trường_hợp bệnh kéo_dài , cần điều_trị bằng nystatin , fluconazone giúp bệnh mau khỏi và giảm nguy_cơ lây_truyền .
Bệnh nấm candida miệng - họng còn là vấn_đề quan_trọng trong điều_trị suy tủy . Bệnh nấm candida toàn_thân thường xuất_hiện và gây tử_vong trong điều_trị suy tủy , và hầu_như không_thể tránh khỏi ở các bệnh_nhân đã có nhiễm nấm candida miệng - họng , thực_quản hay ruột . Điều_trị kết_hợp dung_dịch chlorhexidine 0,2% và fluconazole phòng_ngừa nhiễm nấm candida miệng - họng cho bệnh_nhi ghép tủy xương có ý_nghĩa phòng và giảm nguy_cơ nhiễm nấm candida miệng - họng , toàn_thân và thực_quản .
Loét áp tơ
Là một sang thương đặc_hiệu ở miệng , có xu_hướng tái_phát . Loét áp tơ được ghi_nhận chiếm khoảng 20% dân_số , biểu_hiện dưới dạng vết loét có bờ giới_hạn rõ , đáy có mô hoại_tử màu trắng và xung_quanh có vòng viêm đỏ . Sang thương tồn_tại trong vòng 10-14 ngày và tự lành không để lại sẹo .
Viêm nướu - miệng Herpes
Sau thời_kỳ ủ_bệnh khoảng 1 tuần_lễ , người nhiễm virus herpes bắt_đầu có các biểu_hiện như sốt và mệt_mỏi . Khoang miệng có_thể có các biểu_hiện ở các mức_độ khác_nhau bao_gồm nướu sưng đỏ và các đám mụn nước nhỏ rải_rác khắp miệng . Các triệu_chứng trên thường biến_mất sau 2 tuần và không để lại sẹo . Có_thể bù nước nếu trẻ bị mất nước . Các thuốc_nước súc miệng có tác_dụng giảm đau và giảm sốt giúp trẻ dễ_chịu hơn .
Viêm môi Herpes tái_phát
Khoảng 90% số người có kháng_thể chống virus herpes simplex , khác với bệnh viêm nướu - miệng nguyên_phát do herpes , bệnh_lý viêm do herpes tái_phát thường chỉ khu_trú ở môi . Ngoài biểu_hiện mụn nước gây đau và các biểu_hiện không điển_hình khác , viêm môi herpes tái_phát thường không có các biểu_hiện toàn_thân . Điều_trị bằng thuốc kháng virus ít có hiệu_quả hơn so với liệu_pháp làm dịu ở các bệnh_nhân khoẻ_mạnh bị virus herpes tái_phát .
Nốt_Bohn
Xuất_hiện ở trẻ sơ_sinh , là các nốt bẩm_sinh có ở khoang miệng và lưỡi tại vùng sóng hàm dưới và sóng hàm trên , và ở vùng khẩu cái cứng . Đây là các sang thương hình_thành từ phần biểu mô còn sót lại của các tuyến tiết nhầy . Nốt bohn không cần điều_trị , tự biến_mất sau vài tuần_lễ .
Nang lá răng
Xuất_hiện ở trẻ sơ_sinh , là các nang nhỏ nằm dọc theo vị_trí mào sóng hàm dưới và mào sóng hàm trên . Các sang thương này hình_thành từ biểu mô còn sót lại của lá răng . Nang lá răng không cần điều_trị , tự biến_mất sau vài tuần_lễ .
Hạt_Fordyce
Khoảng 80% người trưởng_thành có nhiều hạt nhỏ , màu trắng hay vàng , hợp_thành đám hay mảng ở niêm_mạc miệng , chủ_yếu là ở môi và má trong . Đây là các tuyến bã bất_thường . Tuyến này xuất_hiện ngay sau sinh , có_thể to ra và có biểu_hiện ban_đầu ở dạng các mảng xuất tiết màu vàng ; tần_suất xuất_hiện khoảng 50% ở trẻ_em . Hạt_Fordyce không cần điều_trị .
Áp - xe lợi
Có biểu_hiện nốt mềm , màu đỏ ở kế các chân răng bị áp - xe răng mạn_tính . Áp - xe lợi thường ở vị_trí sẽ rò mủ áp - xe răng . Điều_trị áp - xe lợi chủ_yếu là chẩn_đoán chính_xác răng bị áp - xe để nhổ bỏ hay xử_lý tủy .
Khô nứt môi
Thường gặp ở trẻ_em , có biểu_hiện khô môi , tiếp_theo là tạo vảy và nứt môi , kèm theo cảm_giác rát bỏng . Bệnh thường do dị_ứng với chất tiếp_xúc ( đồ_chơi hay thức_ăn ) , hoặc với tia_sáng mặt_trời . Bệnh nặng hơn khi thiếu sự làm ướt của lưỡi và bị làm khô thêm bởi gió , đặc_biệt khi thời_tiết lạnh . Viêm khô nứt môi thường có kèm sốt . Thường_xuyên bôi chất gel loãng sẽ giúp sang thương mau lành và phòng_tránh bệnh .
Tật dính lưỡi
Có đặc_điểm là dây hãm lưỡi ngắn gây cản_trở chuyển_động lưỡi . Dây hãm lưỡi dài thêm khi trẻ lớn lên . Nếu tật dính lưỡi mức_độ nghiêm_trọng ảnh_hưởng đến hoạt_động phát_âm thì nên chỉ_định can_thiệp phẫu_thuật .
Lưỡi bản_đồ
Thường lành_tính và không gây ảnh_hưởng lâm_sàng , biểu_hiện dưới dạng một hay vài mảng màu đỏ tươi và phẳng , xung_quanh có viền trắng , vàng hay xám , nằm trên nền mô lưỡi màu đỏ bình_thường . Bệnh chưa rõ nguyên_nhân và không có chỉ_định điều_trị .
Lưỡi nứt
Đặc_điểm lâm_sàng có nhiều khe / rãnh nhỏ ở mặt lưng lưỡi . Nếu đau có_thể xử_trí bằng cách cạo làm sạch lưỡi hay súc miệng để làm giảm số_lượng vi_khuẩn có bên trong các rãnh .
BS . PHẠM_HỒNG_ĐỨC ( Melbourne - Australia ) Báo_Sức khoẻ Đời_sống

﻿ VN cử một đại_diện tham_dự cuộc_thi piano Chopin châu Á
Lưu_Hồng_Quang , sinh năm 1990 , sẽ tham_gia dự thi ở bảng dành cho các thí_sinh tuổi từ 16 trở xuống . Đến với âm_nhạc từ nhỏ , lên 7 tuổi , Quang bắt_đầu học đàn piano một_cách chính_quy . Kể từ đó tới nay , Quang học_tập dưới sự hướng_dẫn của GS.TS.NGND Trần_Thu_Hà và liên_tục đạt danh_hiệu học_sinh xuất_sắc .
Theo nhận_định của NSND Đặng_Thái_Sơn , hiện_nay , xu_hướng âm_nhạc hàn_lâm thế_giới đang trẻ_hóa tài_năng , nhiều thí_sinh dưới 16 tuổi tham_dự các cuộc_thi âm_nhạc lớn rất thành_công .
Tuy_nhiên với VN , đây là lần đầu_tiên chúng_ta cử một thí_sinh nhỏ_tuổi tham_dự cuộc_thi này , một cuộc_thi sẽ hội đủ các tài_năng trẻ từ Trung_Quốc , Hàn_Quốc , Nhật_Bản , Triều_Tiên ...
Mỗi thí_sinh sẽ phải chuẩn_bị cho mình 4 tác_phẩm theo danh_mục mà BTC yêu_cầu . Riêng_Lưu_Hồng_Quang chọn : Prelude-Fuga D dur ( J.Bach ) ; Valse N7 ( Chopin ) ; Etude N2 Op25 ( Chopin ) và Fantasie - Impromptu ( Chopin ) để trình_bày .
Theo Thanh_Niên

﻿ Đại_học FPT sẽ tuyển_sinh trong năm 2006
Đó là thông_tin ông Lê_Trường_Tùng , Giám_đốc Dự_án xây_dựng đại_học FPT đưa ra sáng nay 15/3 . Trong năm đầu_tiên này , trường tuyển_sinh 500 sinh_viên hệ cao_đẳng và đại_học .
Dự_kiến , ông Lê_Trường_Tùng sẽ giữ cương_vị Hiệu_trưởng Trường ĐH Tư_thục FPT . Ông Tùng cho_biết : “ Mục_tiêu trước_mắt của ĐH Tư_thục FPT là đào_tạo và cung_cấp nguồn nhân_lực chất_lượng cao chuyên_ngành CNTT và các nhóm ngành khác có liên_quan trước_hết cho tập_đoàn FPT , đồng_thời cho các doanh_nghiệp CNTT nói_chung và doanh_nghiệp phần_mềm Việt_Nam nói_riêng ” .
Ngay sau khi hòan_thành được các thủ_tục cần_thiết , ĐH_FPT sẽ tuyển_sinh ngay trong mùa tuyển_sinh này . “ Chúng_tôi sẽ không kịp trong kỳ thi ĐH tới , nhưng sẽ dựa vào kết_quả thi_tuyển của các thi sinh để chọn_lựa , tất_nhiên là với sự cho_phép của Bộ GD - ĐT” , ông Tùng nói .
Trước_mắt , trong năm nay có_thể nhà_trường sẽ mở cơ_sở đào_tạo ngay tại trụ_sở FPT , số 89 Láng_Hạ , Hà_Nội . Bảo_Trung

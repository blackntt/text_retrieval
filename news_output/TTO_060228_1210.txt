﻿ Chân bị chai
- BS Nguyễn_Đình_Sang ( Chuyên_khoa BS gia_đình , TT_Y tế quận I ) : Khi một vùng nào_đó của cơ_thể chịu_đựng một áp_lực lâu ngày hoặc thường_xuyên bị cọ_xát thì vùng da chỗ đó dày lên tạo thành vết chai . Bản_thân vết chai không nguy_hiểm , chỉ khi nào vết chai quá dày , cứng như ở ngón chân , ở lòng bàn_chân … và khi đi_lại , trọng_lượng cơ_thể khiến vết chai cấn vào da_thịt gây đau .
Khi vết chai gây đau hoặc gây mất thẩm_mỹ , bác_sĩ có_thể phẫu_thuật gọt bớt chỗ chai bằng dao_mổ hay khoét chỗ chai bằng dao điện .
Nếu chai ít có_thể dùng thuốc bôi làm mềm vết chai như pommade Salicylée 5% ngày 2 lần sau khi rửa sạch da .
Để phòng_ngừa bị chai da chân , bạn nên đi giày cho vừa chân , dùng giầy bằng loại da mềm , nên mang vớ và dùng một miếng đệm trong giày .
Khi_không cần_thiết thì nên cởi giày ra cho chân được thoải_mái và thoáng , xoa_bóp bàn_chân cho máu_huyết lưu_thông để nuôi_dưỡng da được tốt hơn . Tối trước khi ngủ nên ngâm chân vào nước ấm khoảng 15 – 20 phút , xoa_bóp nhẹ và lau khô .
Bạn nên đến phòng_khám đa_khoa để các bác_sĩ khám tổng_quát cho thử đường_máu , khám da_liễu … và điều_trị thích_hợp .
Bạn có những thắc_mắc về sức_khỏe của mình mà không biết hỏi ai . Bạn cần được tư_vấn cho những thắc_mắc về sức_khỏe của mình , của người_thân ... Phòng_mạch Online của Tuổi_Trẻ_Online sẽ giúp bạn giải_đáp 1.001 thắc_mắc về sức_khỏe . Mọi thắc_mắc về sức_khỏe xin gửi về địa_chỉ email : tto@tuoitre.com.vn
Để chính_xác về nội_dung , vấn_đề cần hỏi , xin bạn_đọc vui_lòng gõ có dấu ( font chữ unicode ) . Xin chân_thành cảm_ơn !
T . LÊ thực_hiện

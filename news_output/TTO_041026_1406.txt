﻿ Tình_nguyện_viên , sức_bật cho sự_nghiệp
" Những người trong vai_trò lãnh_đạo thường có nền_tảng tốt như những tình_nguyện_viên . Bà Rhoda_Barr , trưởng phóng dịch_vụ khách_hàng của Thành_phố New_York , dựa trên nhóm tư_vấn tình_nguyện_viên , một tổ_chức liên_kết giữ các nhóm phi lợi_nhuận và nhóm thành_viên tiềm_năng .
Thực_ra , trong một cuộc nghiên_cứu với hãng vận_tải Hoa_Kỳ đã thấy rằng các tổ_chức hoạt_động tình_nguyện không_chỉ đem lại lợi_ích cho cộng_đồng mà_còn có ảnh_hưởng rất tốt để phát_triển các tố_chất của một người lãnh_đạo .
Theo những tình_nguyện_viên " lão_luyện " các kỹ_năng mà họ đạt được nó có khả_năng ảnh_hưởng , dàn_xếp công_việc , và giúp họ công_tác tốt hơn cũng_như có_thể tạo ra những sáng_kiến , hướng giải_quyết mới và định_hình một_cách nhìn khác_lạ hơn , sắc_sảo hơn .
Thêm nữa , để phát_triển các kỹ_năng một_cách linh_hoạt , một thành_viên ban quản_trị cho_biết sự tình_nguyện trong khả_năng người lãnh_đạo thể_hiện đuợc với giám_đốc quản_lý những cá_tính , ưu điiểm , những gì thế mạnh của mình .
Một_vài công_việc tình_nguyện_viên giúp bạn mang lại niềm đam_mê công_việc , nhiều khi nó còn biến niềm đam_mê cuộc_sống vào trong công_việc . Từ công_việc tình_nguyện trong nhiều vị_trí khác_nhau bạn có_thể đạt thành_công trong sự_nghiệp .
NHƯ_BÌNH ( Theo BBC )

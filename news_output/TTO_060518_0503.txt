﻿ Bão_tố từ HLV Scolari
Bầu_không_khí ở Bồ_Đào_Nha đã nóng hẳn lên với những tranh_cãi xung_quanh việc vắng_mặt của Ricardo_Quaresma , cầu_thủ đang khoác_áo Porto .
Ricardo_Quaresma sinh ngày 26-3-1983 . Cùng_với Cristiano_Ronaldo , Tiago_Mendes , anh được xem như " thế_hệ vàng " thứ_hai của Bồ_Đào_Nha .
Năm 2002 cả châu Âu đều biết đến tiền_đạo này khi anh giúp đội_tuyển U-16 Bồ_Đào_Nha đăng_quang chức vô_địch châu Âu . Một năm sau , Quaresma đã có trận đấu chính_thức đầu_tiên trong màu áo đội_tuyển quốc_gia gặp Bolivia vào tháng 6-2003 .
Phong_độ thi_đấu tuyệt_vời của Quaresma đã góp công lớn giúp Bồ_Đào_Nha có_mặt tại Đức mùa_hè này . Ở mùa bóng vừa_qua , Quaresma được xem như_là " chìa_khóa " quan_trọng giúp Porto lập cú đúp khi đoạt chức vô_địch VĐQG và cúp QG .
Chính vì thi_đấu quá xuất_sắc và là " thần_tượng " của nhiều người hâm_mộ Bồ_Đào_Nha nên chỉ chưa đầy 24 giờ sau khi tin Quaresma bị loại khỏi World_Cup thì đã có nhiều sự chỉ_trích nhắm vào ông Scolari .
Phát_biểu trên trang_web UEFA , Rui_Rocha - chuyên_gia bình_luận bóng_đá Bồ_Đào_Nha nói : " Anh_ấy là tài_năng lớn nhất của Bồ_Đào_Nha - kể từ thời của Luis_Figo . Chắc_chắn Bồ_Đào_Nha sẽ mạnh hơn rất nhiều khi có Quaresma tại World_Cup , tôi thật không hiểu tại_sao Quaresma lại không được chọn " .
Các hãng thông_tấn cũng ngạc_nhiên không kém khi giật những dòng tít như : " Scolari loại khỏi đội_tuyển chìa_khóa chiến_thắng Quaresma " ; " Quaresma bị Scolari làm_ngơ " hay " Scolari hất_hủi Quaresma " ...
Quaresma cũng rất bất_ngờ khi nói : " Tôi đã mất cơ_hội tham_dự các giải đấu lớn cùng đội_tuyển Bồ_Đào_Nha ở Euro 2004 ,
Olympic tại Athens ... Vì_thế tôi rất muốn tham_dự World_Cup 2006 . Dù đã rất cố_gắng nhưng tôi vẫn không được chọn " .
Ngoài_Quaresma , việc gọi tiền_vệ Costinha cũng gây nhiều tranh_luận khi Costinha liên_tục phải ngồi ngoài ở Dynamo_Moscow do thường_xuyên vi_phạm kỷ_luật của CLB .
QUỐC_THẮNG

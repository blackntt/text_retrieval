﻿ Bạn_bè mời qua Úc chơi , xin visa thế_nào ?
- Trả_lời của ông Lâm_Dũng , chuyên_viên tư_vấn về di_trú ( Công_ty Tư_vấn Di_trú và Du_học Uniworld ) :
Đối_với trường_hợp của anh , anh dự_định sang Úc du_lịch với sự bảo_lãnh của một người bạn hiện đang cư_ngụ tại Úc thì thủ_tục xin visa tại Việt_Nam sẽ không đơn_giản như một hồ_sơ du_lịch bình_thường đi thăm thân_nhân .
Tuy_nhiên , nếu bạn của anh thật_sự muốn anh đến Úc du_lịch và anh có_thể xin được visa tại Việt_Nam thì bạn anh nên làm hồ_sơ bảo_lãnh du_lịch cho anh theo thủ_tục " có đóng tiền thế_chân " ( Security bond ) . Sau khi bạn anh nộp hồ_sơ bảo_lãnh cho anh và nếu được chấp_thuận thì bạn của anh sẽ được yêu_cầu đóng một khoản tiền thế_chân là 15.000 AUD vào tài_khoản do Bộ di_trú Úc quy_định .
- Sau thời_gian anh du_lịch tại Úc , khi anh trở_về Việt_Nam thì khoản tiền thế_chân này sẽ được hoàn_trả lại đủ cho người bảo_lãnh ( Sponsor ) .
- Nếu trong thời_gian anh du_lịch tại Úc và anh vi_phạm một trong những điều_kiện trên visa của anh thì người bảo_lãnh sẽ bị mất khoản tiền " Security bond " này .
Chúc anh thành_công .
TTO

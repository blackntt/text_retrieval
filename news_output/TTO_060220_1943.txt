﻿ Hành_trang du_học Mỹ : Chuẩn_bị cho ngày lên_đường
Những vật_dụng hàng ngày :
- Ngoài những vật_dụng cá_nhân , các bạn nên mang theo những thuốc dự_phòng căn_bản như nhức đầu , cảm .
- Quần_áo : các bạn nên mang theo quần_jean áo_thun vì đây là trang_phục ở đa_số các trường đại_học và cao_đẳng ở Mỹ .
- Các bạn cũng_nên mua kem giữ ẩm và son giữ ẩm vì thời_tiết ở một_số vùng ở Mỹ tương_đối lạnh sẽ làm các bạn dễ khô da .
- Những postcard và hình_ảnh giới_thiệu về VN sẽ là những món quà ấn_tượng giúp bạn nhanh_chóng hòa_đồng với thầy_cô , bạn_bè nơi đây .
Những vật_dụng liên_quan đến việc học :
- Trước_hết các bạn nên vào trang_web của trường ( đa_số các trường cao_đẳng và đại_học của Mỹ đều có trang_web riêng ) và vào cataloge để xem chương_trình học của mình . Sau đó các bạn đến những nhà_sách để mua những quyển từ_điển liên_quan đến ngành học bên_cạnh từ_điển Anh-Anh , Anh-Việt . Cụ_thể nếu bạn học ngành Accouting nên từ_điển Tóan_Anh - Việt .
- Sách_Understanding_English_Grammar hoặc TOEFL sẽ giúp các bạn có_thể viết những bài luận ( essay ) dễ_dàng hơn .
- Máy_tính học toán cũng là một công_cụ cần_thiết cho việc học ở Mỹ của các bạn nhưng các bạn nên hỏi trước giáo_sư vì một_số môn_học Toán không cho_phép sử_dụng máy_tính như College_Algebra .
- Một vật_dụng quan_trọng các bạn nên mua đó là máy_ghi_âm . Nó sẽ giúp các bạn có_thể nghe lại bài giảng của giáo_sư nhiều lần và tiếp_thu tốt hơn trong những ngày đầu ở Mỹ .
- Không nên mang tập từ nhà sang vì ở Mỹ , các bạn sẽ sử_dụng binder ( bia sách ) và giấy khổ 8x11 để các bạn dễ_dàng nộp bài_tập cho giáo_sư .
Ngày lên_đường :
- Các bạn nên có_mặt trước giờ bay hai tiếng để kịp làm các thủ_tục giấy_tờ cần_thiết .
- Khi ký_gửi hành_lý , chú_ý hỏi rõ nhân_viên hàng_không về thông_tin nơi bạn sẽ nhận lại hành_lý vì có những chuyến bay transit ( quá_cảnh ) không bắt_buộc các bạn phải lấy hàng lý ra kiểm_tra . Tuy_nhiên , ở một_số sân_bay lớn như Los_Angeles , các bạn sẽ buộc phải lấy hành_lý ra cửa_khẩu .
- Khi các bạn dừng máy_bay tại nơi quá_cảnh , việc đầu_tiên các bạn nên làm là tìm cửa lên máy_bay kế_tiếp và lấy vé máy_bay ( boarding pass ) . Số ghế và cửa ra_vào luôn được đánh_dấu trên boarding pass của bạn . Nếu chẳng may các bạn không tìm được cửa lên máy_bay , hãy nhờ đến sự giúp_đỡ của các nhân_viên hải_quan tại cửa_khẩu . Họ sẽ yêu_cầu được xem boarding pass của bạn để có sự hướng_dẫn chính_xác nhất cho bạn . Vì_thế các bạn phải giữ cẩn_thận boarding pass suốt chuyến bay .
- Khi chẳng may trễ chuyến bay , các bạn hãy bình_tĩnh tìm hãng hàng_không trên vé máy_bay của bạn . Sau đó , hãy trình_bày hoàn_cảnh của mình với các nhân_viên hàng_không và nhờ họ sắp_xếp chuyến bay khác cho bạn .
- Trên chuyến bay từ Việt_Nam sang Mỹ , hãng hàng_không sẽ phát cho bạn phiếu nhập_cảnh I-94 và tờ khai hải_quan . Các bạn nên điền vào các phiếu ngay trên máy_bay để tiết_kiệm được thời_gian .
- Tại sân_bay đầu_tiên của Mỹ , các bạn cần làm những thủ_tục hải_quan để nhập_cảnh vào Mỹ . Do_đó , các bạn nên chuẩn_bị sẵn passport , thư nhập_học I-20 còn niêm_phong , tờ khai hải_quan trong hành_lý xách tay để xuất_trình dễ_dàng .
- Sau khi hạ_cánh xuống phi_trường cuối_cùng , các bạn nên đến chỗ Baggage_Claim để lấy hành_lý và liên_lạc với người đón bạn qua điện_thoại công_cộng đặt ở sân_bay .
HUỲNH_THỊ_KIM_CHI ( Du_học_sinh Houston_College , Texas )

﻿ Liệu_pháp cho người bận_rộn
Bạn dường_như không còn thời_gian rỗi , bị cuốn vào vòng xoáy chóng_mặt bởi bị cuộc_sống hiện_đại . Một_vài chỉ_dẫn dưới đây sẽ không làm hao tốn quỹ thời_gian của bạn nhưng lại giúp_ích rất nhiều cho cơ_thể .
Không có thời_gian để ăn sáng
Biện_pháp tối_ưu : Nhất_thiết bạn không được bỏ bữa ăn quan_trọng này . Nếu thường_xuyên bỏ_qua , cơ_thể bạn sẽ rơi vào tình_trạng chán ăn và làm giảm sự trao_đổi chất . Nên chọn món ăn có chất tinh_bột cùng_với một cốc sữa tách bơ . Hàm_lượng calo trong mỗi bữa sáng nên là 400-500 , để đảm_bảo năng_lượng cho việc bắt_đầu một ngày mới . Ngoài_ra , nếu được , bạn có_thể ăn nhẹ vào giữa buổi .
Biện_pháp thay_thế : Nếu quá vội , các loại bánh làm từ bột ngũ_cốc hoặc bánh_quy cũng rất tốt cho bữa sáng .
Vấn_đề về răng miệng
Biện_pháp tối_ưu : Chọn loại kem đánh răng phù_hợp là cần_thiết nhưng tốt nhất nên dùng nước súc miệng 2 lần / ngày_sau khi đánh răng để có tác_dụng ngăn_ngừa và giảm_thiểu sự hình_thành bựa răng và các dấu_hiệu xấu của bệnh về răng miệng .
Biện_pháp thay_thế : Có_thể dùng nước_sạch thay cho nước súc miệng sau khi đánh răng . Cách thực_hiện : Súc thật mạnh và đẩy lực theo cả chiều dọc và chiều ngang .
Thiếu thời_gian chăm_sóc sắc_đẹp
Biện_pháp tối_ưu : Mỗi tháng dành cho mình một ngày nghỉ thật xả_láng ở thẩm_mỹ_viện với các dịch_vụ chăm_sóc thật toàn_diện . Nó sẽ chỉ_tiêu tốn của bạn 2 giờ đồng_hồ là_cùng .
Biện_pháp thay_thế : Bạn có_thể mua các hương_liệu và kem dưỡng rồi tự làm ở nhà . Bắt_đầu bằng việc tự ngâm mình trong bồn_tắm có ướp hương_hoa oải hương , sau đó bôi kem dưỡng thể và massage nhẹ toàn_thân , chắc_chắn bạn sẽ cảm_thấy rất sảng_khoái .
Bạn luôn cần đến liệu_pháp tâm_lý
Biện_pháp tối_ưu : Mỗi ngày bạn dành ít phút để viết ra giấy những xúc_cảm của mình , cả vui lẫn buồn . Đây là cách rất hữu_hiệu giúp bạn tăng thêm sự bình_tĩnh khi đối_diện với tình_huống thật . Hơn_nữa , nếu chẳng may mắc sai_sót , hãy cố tự bỏ_qua , có như_vậy bạn mới tránh được cảm_giác e_dè , tự_ti .
Biện_pháp thay_thế : Phim_ảnh cũng có tác_dụng khá hiệu_quả trong những trường_hợp như_thế_này . Chẳng_hạn , nếu bạn muốn khóc , hãy xem những bộ phim tình_cảm sướt_mướt khiến bạn muốn rơi lệ . Bằng cách đó bạn sẽ giải_tỏa được tâm_trạng và cảm_thấy bớt căng_thẳng hơn . Phương_Minh

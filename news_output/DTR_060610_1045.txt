﻿ Hilary_Duff : Người_đẹp ... thiếu sức_sống !
Liệu khi nhìn hai bức ảnh này , các bạn có nghĩ rằng đó chỉ là một người ? Quả_vậy , chính_xác đó là Hilary_Duff . Bên trái là tấm ảnh khi cô 16 tuổi và bên phải là tấm ảnh mới nhất của cô được chụp hôm mồng 8/6 tại New_York .
Trong bộ_đồ đen tuyền , Hilary_Duff đã khiến tất_cả mọi người có_mặt tại bữa tiệc hôm 8/6 do tạp_chí Ocean_Drive tổ_chức phải sững_sờ . Nhìn cô chẳng khác_nào “ chị_em ” với nữ diễn_viên Nicole_Ritchie : thân_hình khẳng_khiu , mỏng_manh và gương_mặt nhợt_nhạt , xanh mướt . Trông cô không có chút sức_sống nào nhất_là khi cô mới chỉ sắp bước vào tuổi 20 ! Hậu_quả của một chế_độ ăn_kiêng vô_độ , hay Hilary đang làm_việc quá_sức hoặc cô mắc phải một căn_bệnh gì đó ?
Có_lẽ khi nhìn ngắm lại những bức ảnh cũ , Hilary sẽ rất giật_mình . Cô sẽ phải nhanh_chóng xem_xét lại bản_thân và cuộc_sống của mình để các fan hâm_mộ của cô_đỡ phải lo_lắng cho cô chứ ?
Trông_Hilary quá tiều_tụy và già trước tuổi !
Vĩnh_Ngọc
Theo Hollywoodrag

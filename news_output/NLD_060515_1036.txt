﻿ Sóc_Trăng : bệnh_nhân sốt_xuất_huyết tăng đột_biến
Do ảnh_hưởng thời_tiết mưa_nắng bất_thường nên những ngày gần đây số bệnh_nhân bị bệnh sốt_xuất_huyết ( SXH ) ở Sóc_Trăng tăng đột_biến .
Bác_sĩ Nguyễn_Đình_Thanh_Liêm - trưởng khoa dịch_tễ Trung_tâm Y_tế dự_phòng tỉnh Sóc_Trăng - cho_biết chỉ tính riêng trong tuần vừa_qua chín huyện_thị đã phát_hiện 111 ca ( tăng 70 ca so với tuần trước , chiếm tỉ_lệ 170,7% ) , nâng tổng_số bệnh_nhân nhiễm và nghi nhiễm SXH trong toàn tỉnh tính_từ đầu năm đến nay lên 846 ca .
Đáng chú_ý là bệnh SXH không_chỉ tấn_công trẻ nhỏ mà đối_tượng trên 15 tuổi cũng bị nhiễm ( 35/111 ca trong tuần ) .
Theo Tuổi_Trẻ

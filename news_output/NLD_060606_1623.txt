﻿ Jan_Koller sẽ đến Monaco
Cầu_thủ Jan_Koller đã được định_đoạt ? Cuối_cùng những đồng_đại đã chấm_dứt , tương_lai của tiền_đạo cao_kều Jan_Koller được định_đoạt bằng bản hợp_đồng thi_đấu cho CLB Pháp_Monaco với thời_hạn 2 năm .
Cầu_thủ người CH Séc kết_thúc hợp_đồng với CLB Đức_Borussia_Dortmund nên có_thể ra_đi theo luật Bosman dạng chuyển_nhượng tự_do .
Trước_Monaco , CLB xứ Sicilian_Palermo đã theo_đuổi Koller từ rất lâu nhưng không thành do đòi_hỏi mức lương của tiền_đạo cao_kều này quá cao .
Tiền_đạo 33 tuổi có chiều_cao 2m01 này là chân_sút số_một trong lịch_sử của CH Séc với 40 bàn sau 66 trận khoác_áo đội_tuyển quốc_gia .
Anh đến Dortmund từ 2002 và đã có giai_đoạn thành_công cùng đội bóng này nhưng rồi những khó_khăn tài_chính đẩy Dortmund vào khủng_hoảng khiến họ phải bán đi các cầu_thủ trụ_cột . Mùa bóng 05-06 trôi qua thật tệ_hại với Koller khi anh bị chấn_thương không_thể thi_đấu nhiều , chứng_kiến CLB lâm vào khủng_hoảng và anh bạn Rosicky ra đi_sau nhiều năm chung vai sát_cánh .
Theo Thanh_Niên

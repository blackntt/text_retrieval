﻿ Bỏng
- BS Nguyễn_Đình_Sang ( Chuyên_khoa bác_sĩ gia_đình , TT_Y tế quận I ) : Bỏng là tai_nạn thường gặp trong sinh_hoạt gia_đình . Người_ta thường chia bỏng ra làm 3 mức_độ :
- Bỏng độ 1 : bỏng chỉ tác_động đến lớp biểu_bì ( là lớp da nông nhất ở trên cùng ) . Da chỉ bị ửng đỏ và tróc ra sau vài ngày ( thí_dụ như bỏng do tắm nắng ngoài bãi_biển ) .
- Bỏng độ 2 : bỏng tác_động đến lớp chân bì tạo thành bóng_nước nhưng một phần lớp chân bì vẫn còn nên da có_thể tái_tạo lại được , do_đó khi lành thường không để lại sẹo nếu_không bị bội_nhiễm và diện_tích bỏng lớn .
- Bỏng độ 3 : lớp da bị phá_huỷ hoàn_toàn , có_khi cháy tới cơ và xương .
Người_ta còn phân_loại bỏng theo diện_tích cơ_thể . Diện_tích bỏng càng lớn thì mức_độ càng nặng . Bỏng được coi là nặng khi diện_tích bỏng & gt ; 20% diện_tích cơ_thể và bỏng sâu độ 3 . Ngoài_ra , người_già , trẻ_em thường diễn_biến nặng hơn khi bị bỏng .
Bạn bị dầu bắn vào tay gây bỏng tạo bóng_nước là bỏng độ 2 . Khi bị bỏng nên :
- Nhúng_tay vào vòi_nước lạnh ngay_lập_tức hay đắp vết bỏng bằng gạc , khăn_tay sạch có nhúng nước_lạnh cho_đến khi bớt đau .
- Băng lại bằng gạc vô_trùng .
- Không được bôi lên vết bỏng các chất như kem đánh răng , nước_mắm … vì có_thể gây nhiễm_trùng và thoát huyết_tương nhiều hơn .
- Không được chích vỡ các bóng_nước .
- Nếu bóng_nước đã vỡ , rửa vết bỏng bằng dung_dịch nước muối sinh_lý , băng nhẹ vô_trùng bằng gạc không có bụi hoặc để thoáng không băng nhưng phải giữ cho thật sạch để da dễ lành .
Dùng kháng_sinh ngừa bội_nhiễm , thuốc giảm đau ( như paracetamol ) , chống ngứa ( như chlorpheniramine 4mg uống tối 1 viên ) .
Bạn nên đến phòng_khám đa_khoa ở địa_phương để khám và điều_trị cụ_thể hơn .
Bạn có những thắc_mắc về sức_khỏe của mình mà không biết hỏi ai . Bạn cần được tư_vấn cho những thắc_mắc về sức_khỏe của mình , của người_thân ... Phòng_mạch Online của Tuổi_Trẻ_Online sẽ giúp bạn giải_đáp 1.001 thắc_mắc về sức_khỏe . Mọi thắc_mắc về sức_khỏe xin gửi về địa_chỉ email : tto@tuoitre.com.vn
Để chính_xác về nội_dung , vấn_đề cần hỏi , xin bạn_đọc vui_lòng gõ có dấu ( font chữ unicode ) . Xin chân_thành cảm_ơn !
T . LÊ thực_hiện

﻿ Bệnh_Viện_Nhi trung_ương sẽ thực_hiện ca ghép tủy xương đầu_tiên
Hiện bệnh_viện đã chọn 2 bệnh_nhân bị suy tủy nặng để thực_hiện phẫu_thuật và đang làm các xét_nghiệm giữa người cho và người nhận cũng_như tập_kết máy_móc chuẩn_bị ca ghép .
Theo ông Nguyễn_Thanh_Liêm , Giám_đốc Bệnh_viện , trong tháng 6 tới , bệnh_viện cũng sẽ tiến_hành ca ghép gan thứ_hai - ca ghép gan thứ_năm của cả nước - cho một bé gái 12 tháng tuổi ở Hà_Nội . Các bác_sĩ của bệnh_viện sẽ tiếp_tục cộng_tác với chuyên_gia đến từ Hàn_Quốc , tuy_nhiên vai_trò của các bác_sĩ Việt_Nam là chính . Bệnh_viện tiếp_tục miễn_phí cho bệnh_nhân của ca ghép gan này .
Ngoài_ra , Bệnh_viện Nhi_Trung ương cũng đã sẵn_sàng cho việc ghép tim , tuy còn phải đợi luật_pháp cho_phép được lấy tim của người đã chết_não để ghép .
Theo TTXVN

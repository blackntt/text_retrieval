﻿ Sẽ thành_lập các trường sau_đại_học ?
Trước_mắt , các trường này sẽ được xây_dựng trên cơ_sở các viện khoa_học như Viện_Khoa học tự_nhiên và công_nghệ quốc_gia , Viện_Khoa học xã_hội quốc_gia và một_số cơ_sở nghiên_cứu khác .
Mục_đích của việc thay_đổi cơ_cấu này là để tận_dụng được lực_lượng cán_bộ đang làm_việc tại các viện hiện_nay , kết_hợp tốt công_tác nghiên_cứu khoa_học với đào_tạo sau ĐH . Ngoài_ra , một_số viện nghiên_cứu khác sẽ được đưa về các trường ĐH để cùng thực_hiện mục_tiêu trên .
H.THU ẬT

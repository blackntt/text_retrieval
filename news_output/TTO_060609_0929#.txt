﻿ FIFA áp_dụng hệ_thống liên_lạc cho trọng_tài
Các cầu_thủ sẽ không còn than_thở với trọng_tài như_thế_này nữa ...
TT - Các trọng_tài sẽ hạn_chế được những quyết_định sai_lầm nhờ vào hệ_thống liên_lạc mà FIFA chính_thức cho áp_dụng tại VCK World_Cup 2006 vào hôm_nay 9-6 ...
Theo ông Vincent_Mauro , thành_viên Ủy_ban trọng_tài của FIFA , hệ_thống liên_lạc mới này bao_gồm một micro mini và tai_nghe , cho_phép các trợ_lý và trọng_tài thứ_tư có_thể liên_lạc với trọng_tài chính để có_thể điều_khiển trận đấu một_cách tốt nhất .
Tuy_nhiên vẫn có không ít trọng_tài vẫn lo_lắng về những rắc_rối tiềm_ẩn của hệ_thống mới này . Van_Der_Ende , trọng_tài người Hà_Lan từng điều_khiển tại hai kỳ World_Cup 1994 và 1998 , cho_biết : “ Ý_tưởng đó thật hay nhưng chưa chắc nó đã có tác_dụng 100% . Rất có_thể hệ_thống sẽ bị nhiễu sóng và trọng_tài ngoài sân không_thể nghe được tín_hiệu " .
Dù_sao đây cũng là lần đầu_tiên hệ_thống liên_lạc giữa các trọng_tài sẽ được sử_dụng tại các trận đấu ở World_Cup , dù trước đó UEFA đã thử_nghiệm hệ_thống này ở mùa bóng vừa_qua ở các giải đấu châu Âu giữa các CLB .
T . Đ . ( Theo Xinhua Net )

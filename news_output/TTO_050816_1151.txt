﻿ Thủ_tục nhập_cảnh Việt_Nam
- Trả_lời của Cục quản_lý Xuất_nhập_cảnh :
Trường_hợp này bạn không nêu rõ hiện bạn đã nhập quốc_tịch Úc hay còn giữ quốc_tịch VN . Vì_vậy chúng_tôi trả_lời theo cả 2 trường_hợp :
Trường_hợp bạn vẫn còn giữ quốc_tịch VN mà hộ_chiếu VN của bạn còn thời_hạn thì bạn có_thể sử_dụng hộ_chiếu đó về VN mà không phải xin thị_thực nhập_cảnh VN . Thời_gian lưu_trú tại VN không hạn_chế . Tuy_nhiên , trong thời_gian ở VN , bạn phải làm thủ_tục đăng_ký tạm_trú với chính_quyền địa_phương nơi bạn sẽ đến ở . Nếu bạn chưa có hộ_chiếu hoặc hộ_chiếu đã hết hạn , bạn cần liên_hệ Cơ_quan đại_diện VN ở Úc để xin cấp hoặc gia_hạn hộ_chiếu trước khi về VN .
Trường_hợp người VN đã nhập quốc_tịch nước_ngoài , có nhu_cầu về VN thì_phải liên_hệ Cơ_quan đại diên VN ở nước_ngoài để làm thủ_tục nhập_cảnh ( xin visa nhập_cảnh ) . Thời_gian lưu_trú ở VN tùy_thuộc vào thời_hạn visa nhập_cảnh ( bạn có_thể xin gia_hạn thời_gian lưu_trú tại VN nếu có nhu_cầu ) .
TTO

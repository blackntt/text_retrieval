﻿ Phải nhận người lao_động trở_lại
( NLĐ ) - Đó là phán_quyết của Tòa_Lao động TAND_TPHCM trong phiên xét_xử sơ_thẩm ngày 4-5 , vụ_kiện đơn_phương chấm_dứt HĐLĐ giữa bà Phan_Thị_Mỹ_Trang và Công_ty TNHH Dịch_vụ Công_nghệ_cao Schmidt_Việt_Nam ( SVN ) .
Đồng_thời với việc phải nhận bà Trang trở_lại làm_việc , công_ty còn phải hủy quyết_định cho thôi_việc trái pháp_luật , công_khai xin_lỗi bà Trang và bồi_thường tiền_lương trong những ngày bà Trang không được làm_việc ( khoảng 229 triệu đồng ) . Bà Trang làm_việc cho Công_ty SVN ( chi_nhánh TPHCM ) từ năm 1994 , công_việc là kế_toán_trưởng . Ngày 1-7-2004 , bà Trang_bị công_ty cho nghỉ_việc theo điều 17 Bộ Luật_Lao động , nhưng thực_tế SVN không hề có sự thay_đổi cơ_cấu , tổ_chức ( Báo NLĐ đã thông_tin ) .
Th.Anh

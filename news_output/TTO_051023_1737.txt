﻿ NXB sẵn_sàng đổi lại sách " Mật_mã Da_Vinci " cho bạn_đọc
- Theo đánh_giá của tôi , bản dịch này chưa hoàn_hảo , bằng_chứng là có một_số độc_giả đã góp_ý một_số chỗ sai , kém .
Gọi_là một " thảm_họa " thì hơi nặng . Có_thể bạn_đọc đó hơi khắt_khe . Để đánh_giá toàn_bộ bản dịch không_chỉ căn_cứ trên các lỗi đó .
Trước khi phát_hành , NXB đã nhận ra sự " chưa hoàn_hảo " đó , thưa ông ?
- Chưa hoàn_hảo là chưa tốt nhất . Song trước khi phát_hành chúng_tôi chưa biết_điều đó , vì thực_tế , dịch_giả Đỗ_Thu_Hà từng làm nhiều cuốn cho NXB rất tốt nên NXB hoàn_toàn tin_tưởng .
Chị Hà hiện là giảng_viên của khoa Ngôn_ngữ - Đại_học Khoa_học Xã_hội và Nhân_văn nên chắc_chắn trình_độ tiếng Việt cũng tốt rồi .
Nhưng trong quá_trình dịch , chị bị tai_nạn ba_tháng , do_đó rất ảnh_hưởng tới việc dịch_thuật . Có_thể chị nhờ người khác , nhưng điều này cần hỏi dịch_giả .
Có_nghĩa , NXB giao bản dịch mà không biết người dịch làm_gì với nó ?
- NXB chỉ biết một người , và biết người này từng làm tốt nhiều cuốn . NXB yên_tâm khi giao cho dịch_giả Thu_Hà .
Nhưng cũng có_thể do cuốn này nổi_tiếng quá , nên độc_giả đòi_hỏi quá khắt_khe chăng ?
Do_đó , NXB cũng không tìm_hiểu quá_trình dịch ...
- Chị Hà hiện_nay bị sốc , và không được bình_tĩnh , nhiều cuộc điện_thoại trách_cứ ...
Tìm ra một lỗi từ nho_nhỏ cho tới trầm_trọng trong bản dịch này là rất dễ . Tìm cho ra một trang hoàn_toàn sạch ( nghĩa_là không có lỗi nào ) là việc khó hơn nhiều , đòi_hỏi rất nhiều kiên_nhẫn và kỳ_công .
Các lỗi này có_thể chia làm ít_nhất bốn loại :
1 . Sai vì kém tiếng Anh ở cấp sơ_đẳng . 2 . Sai vì kém tiếng Việt ở cấp sơ_đẳng . 3 . Sai vì kém kiến_thức tôn_giáo , lịch_sử , văn_hóa ... 4 . Bỏ hẳn nhiều đoạn .
( Trần_Tiễn_Cao_Đăng ) Nói về đạo_đức nghề_nghiệp thì chị ấy hoàn_toàn tốt , có trách_nhiệm trong công_việc . Đây cũng là một " tai_nạn " không mong_muốn !
Thế còn trách_nhiệm của NXB ?
- Trách_nhiệm của NXB là nhiều chứ không ít !
Đáng_lẽ cần phải đưa một dịch_giả tên_tuổi hiệu_đính . Nhưng do chúng_tôi muốn làm gấp để kịp đi dự Triển_lãm sách quốc_tế tại Đức vừa khai_mạc hôm 19-10 nên nghĩ rằng chỉ một_mình chị Hà hiệu_đính là được rồi .
Như_thế thì khác_nào " vừa đá bóng vừa thổi còi " ...
- Ở_NXB , trình_độ tiếng Anh của biên_tập_viên chỉ đạt chứng_chỉ B , C thì làm_sao hơn chị Hà được . Nếu muốn " bắt lỗi " thì trình_độ phải cao hơn người_ta .
Với cuốn này , tôi cũng rất cẩn_thận giao cho hai trưởng_ban là chị Đặng_Thị_Huệ và anh Nguyễn_Thế_Vinh đọc chéo , trong đó có Thế_Vinh là người giỏi về tôn_giáo .
Nhưng , không ngờ , lỗi lại do phần dịch , lại đúng cuốn nổi_tiếng và bị để_ý ...
Thực_tế , có_khi , nhiều cuốn có lỗi mà không ai phát_hiện ra .
Phải_chăng buông_lỏng khâu dịch , mà chú_trọng phát_hành cũng là một nguyên_nhân ?
- Không phải buông_lỏng , mà mình quản_lý không chắc .
Nếu bản dịch không đạt , NXB sẵn_sàng đổi sách
Hiện NXB đang sửa_sai cho bản dịch tiếng Việt này ?
- Chúng_tôi kiểm_tra từng trang một . Những phần sai do bạn_đọc phát_hiện đang được xem_xét , nếu đúng như_vậy thì sửa .
Ngoài_ra , còn nhờ các chuyên_gia tiếng Anh có tên_tuổi hiệu_đính . Chắc rằng , khi nhìn thấy tên_tuổi họ , độc_giả cũng yên_tâm .
Như_thế là dịch lại toàn_bộ ?
- Không dịch lại , vì nếu thế chẳng_lẽ bản dịch hỏng . Với một cuốn như Mật_mã Da_Vinci , không mua được cũng gay , mua được cũng gay . Có_khi cũng tại ra_mắt " ồn_ào " , họp_báo hẳn_hoi lại thành_ra ...
Có ý_kiến cho rằng , với gần 3.000 người đã mua bản dịch tiếng Việt cuốn tiểu_thuyết này , NXB cần phải xin_lỗi họ .
- Muốn đánh_giá bản dịch cần có hội_đồng . Và nếu hội_đồng đó đánh_giá bản dịch không đạt chất_lượng thì NXB sẽ sẵn_sàng đổi lại sách cho độc_giả .
Nhưng tôi vẫn tin , với dịch_giả Đỗ_Thu_Hà , bản dịch không đến_nỗi như_thế .
Còn trong khi chưa có hội_đồng ...
- Thì cũng chưa_thể khẳng_định điều gì cả .
Đại_diện của tác_giả Dan_Brown đã biết về sự_cố này ?
- Đến nay , họ vẫn chưa biết .
Nếu qua báo_chí họ biết thì_sao ?
- Thực_ra , điều này cũng không vi_phạm hợp_đồng đã ký_kết , chỉ ảnh_hưởng đến uy_tín của NXB .
Theo Thể thao và Văn_hóa

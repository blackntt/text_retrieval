﻿ Xuân_Bình và hành_trình Đông - Tây
Chỉ đến khi quyết_định rời bỏ cơ_quan trở_thành một phóng_viên , nhiếp_ảnh_gia tự_do và sau bảy năm vật_vã cái tên Xuân_Bình mới trở_thành một thương_hiệu , lúc này mới có thêm nhiều người ... nhớ đến anh .
Cái tên Xuân_Bình được người_ta nhắc đến kể từ năm 1995 , khi một_số tờ báo loan tin anh đoạt giải_thưởng lớn nhất cuộc_thi ảnh quốc_tế ACCU cho tác_phẩm “ San_sẻ ” . Tuy_vậy , mọi chuyện chỉ thực_sự bắt_đầu_từ khi anh trở_thành một phóng_viên , nhiếp_ảnh_gia tự_do . Trước ngày lên_đường thực_hiện “ những chuyến đi mơ_ước ” , Xuân_Bình đã dành cho Dân_Trí một cuộc trò_chuyện . Người phỏng_vấn và người được phỏng_vấn là bạn , lại cùng tuổi nên câu_chuyện dễ nói hơn rất nhiều .
Kẻ viết phần_mềm ... 6D
Vì_sao “ ông ” lại bỏ cơ_quan ra hẳn ngoài làm báo tự_do , khi mà bao_nhiêu người muốn chui vào để nương_thân ở một nơi nào_đó ?
Theo tử_vi , tôi và ông cùng đứng chữ Nhâm , thích khác người , ngông , khó thuần_phục , chỉ thích nương dựa vào chính bản_thân , không thích bầy_đàn . Với_lại trong hay ngoài biên_chế cũng là làm_việc để kiếm sống ở đâu thuận thì đến . Tại_sao phải chôn_chân ở chỗ lương thấp , người_làm thì ít , người ăn và phán thì nhiều lại lắm gian kế . Lo đối_phó với cánh này nhọc ghê lắm !
Vậy từ lúc ra ngoài , “ ông ” thấy thế_nào ?
Có nhiều thời_gian để sống thật với bản_thân hơn . Làm tử_tế với những điều mình thích . Chơi với kẻ đồng_cảm . Gần vợ_con nhiều hơn . Chẳng nhiều lý_do bám bàn nhậu trả nợ miệng . Thời_gian đi , đọc , nghĩ nhiều hơn chỉ_số nhai nuốt ...
Một môi_trường sống lý_tưởng . Nhưng sao cuộc_chơi vẫn đơn_lẻ ?
Làm báo tự_do không hề dễ . Sống tử_tế bằng nó còn khó hơn .
Nguyên_tắc của cuộc_chơi này là gì ?
Ba trong một . Làm_việc có phương_pháp như một nhà_khoa_học . Xông_xáo , nhạy_cảm như một nhà_báo . Thăng_hoa như ... ( từ này tôi rất ghét nhưng chưa có từ dùng thế ) nghệ sỹ .
Nghe_nói anh có một công_thức hành_nghề ảnh lạ_tai lắm ?
6D - đọc nhiều , đi nhiều , đến đúng lúc , đặt máy đúng chỗ , đốt nhiều phim , cạc ... và ...
Với nghề viết ?
Đọc nhiều , đi nhiều , đến đúng chỗ , đón cảm_hứng nhanh_nhất , đẩy bài đến toà_soạn lẹ nhất và ... ha_ha
Thu_hoạch của bảy năm hành_nghề báo tự_do là gì ?
Một tủ_sách gần 10.000 đầu_sách , tạp_chí khảo_cứu văn_hoá , một thư_viện ảnh hơn 1 triệu file được phân_loại khá tỉ_mỉ từ_mẫu vẽ chim xẻ trên gốm Chu_Đậu , các chạm_khắc đình chùa , lễ_hội làng , những thay_đổi trên dáng vũ_nữ Chàm , các kiểu nhà_sàn châu Á đến những nhạc_cụ Tây_Nguyên hay mắt thuyền của ngư_dân các vùng duyên_hải ...
Đọc anh trả_lời phỏng_vấn trên tạp_chí Sành_Điệu tết Bính_Tuất có người nghi_ngờ rằng anh ... nói vống lên về số ảnh chụp , số bài đã viết .
Người đó hiện đang điều_hành một tờ báo rất lớn ( Xuân_Bình khoát tay hình tờ báo và cười ha_hả ) . Anh_ấy là người tài , nhưng tiếc nỗi lại sống quá lâu trong một khuôn_khổ cũ_kỹ , một khoẻn đất cứ bị vu cho là lớn rộng nên chẳng thèm biết giang_hồ có những_ai hành_đạo . Tiếc . Rất tiếc .
Ông thuộc “ típ ” người thích nói sự_thật , nói thẳng_thắn , không sợ mất_lòng ?
Có lần một người bạn được đề_bạt lên một chức_vụ cao . Tôi không hề mừng mà cảm_thấy như mình mất đi một cái gì đó . Tôi nhắn_tin cho bạn “ Gửi lời chia buồn sâu_sắc tới mày . Làng báo nước_nhà mất đi một người có khả_năng làm báo giỏi và có thêm một quan_chức có nguy_cơ tha_hoá cao ” . Người_ta bảo tôi là người luôn thu_hút hoả_lực về phía mình . Chẳng biết đúng hay sai .
Gia_đình nhà_báo , nhiếp_ảnh_gia Xuân_Bình .
Bắt_đầu_từ ngày 25/2/2006 , nhà_báo , nhà nhiếp_ảnh tự_do nổi_tiếng Xuân_Bình sẽ lên_đường thực_hiện một loạt chuyến đi mơ_ước . Hành_trình đầu_tiên của anh sẽ là Tây_An , Cam_Túc , Lan_Châu , Đôn_Hoàng nơi khởi_đầu " con đường tơ_lụa ” huyền_thoại . Hành_trình tiếp_theo sẽ là Campuchia , Miama , Tây_Tạng , đó là Đông_du .
Giữa tháng 4 , Xuân_Bình sẽ trở_về VN để chuẩn_bị hành_trang cho chuyến Tây du trong khoảng hai tháng tới một loạt quốc_gia như Ytalia , Pháp , Tây_Ban_Nha , Anh ... và chứng_kiến không_khí hừng_hực của World_Cup 2006 vào cuối tháng 5/2006 . Xuân_Bình chính_thức sẽ là đặc_phái_viên của Dân_trí trên những nẻo đường mà anh sẽ qua .
Hành_trình ... chữa bệnh ngu , hèn
Chúng_ta trở_lại cuộc hành_trình sắp tới một_chút . Anh là người thích xê_dịch , vậy có triết_lý nào về sự vận_động luôn ám_ảnh ?
Người_Việt không có những hành_trình ra_đi lớn như Marco_Polo , Cristoph_Colombo , Trịnh_Hoà , cũng không có luôn sự đón_nhận hoành_tráng như hậu_duệ Khổng , Lão đón_nhận Thích ca Mâu_Ni ...
Nghề đi anh trọng những_ai ?
Phương_Tây , tôi kính_nể Kant ( triết_gia ) vì chưa một lần bước chân ra khỏi thành_phố quê_hương , âu cũng là cách đi siêu_việt . Phương_Đông cảm_kích một Huyền_Trang thỉnh_kinh ở Tây_Vực , một Fukuzawa ( Nhật_Bản ) quyết_liệt từ_bỏ Hà_Lan để đến với nước Anh . Trong nước , tôi luôn tự_vấn trước những cuộc đi của Phan_Châu_Trinh và Phan_Bội_Châu . Những bạn trẻ hôm_nay , tôi yêu thành_viên của nhóm Tây_Bắc . Một_số thành_viên nhóm này sẽ cùng đi Tây_Tạng .
Anh tìm cho mình điều gì của những điểm đến ?
Đến_nơi khởi_phát con đường tơ_lụa , tôi muốn hỏi đất_trời một câu : dân_tộc Trung_Hoa cao_ngạo kia nếu_không có những bước chân lạc_đà mải mết trên sa_mạc Gobi đi_về Tây_Vực thì_sao nhỉ ? Đặt_chân tới Ytalia , quê_hương của Dante , nếu_không có ông , không có Thần khúc , ai sẽ rung đập cánh cửa trung_cổ để Shakespear và Servantes mở tung đại_lộ hướng nhân_loại bước nào kỷ_nguyên Phục_Hưng . Tới_Đức , việc đầu_tiên là tìm đến với Kant để tự_vấn ba điều ông đã đặt ra : tôi có_thể tri_thức gì ? Tôi phải làm_gì ? Tôi có quyền hy_vọng gì ? Còn trở_về Tây_Tạng là để học ... cách đi chậm , thở sâu và đừng gục ngã vì thiếu dưỡng khi trước khi ... về đích .
Anh không hề giỏi ngoại_ngữ , không hề có bạn_bè thân_thiết dọc đường vậy sẽ đi làm_sao đây ? làm được_cái gì đây ?
Những năm qua , tôi có rất nhiều kinh_nghiệm khi đi làm báo giới_thiệu , quảng_bá du_lịch cho Singapore , Malaysia , Trung_Quốc , Thái_Lan . Tất_nhiên quá buồn và cay_đắng khi bước ra thế_giới trong tâm_thế , tư_thế của một kẻ mù_loà , câm_điếc về mọi nghĩa . Nhưng có sao đâu nếu lý_do chính của những chuyến đi là ... chữa bệnh .
Có_điều gì cảm_thấy cân_nhắc trước khi xuất_hành không ?
Tôi thấy trong mắt vợ_cả niềm ân_cần và điều lo_lắng khi rút gần hết tiền tiết_kiệm để lo cho chuyến đi của chồng , số tiền ấy_là quá ít . Và rất có_thể tôi sẽ trễ hẹn chuyến đi xuyên Việt lần thứ_ba với “ Phim ” và “ Bút ” hai cậu con_trai trong dịp hè này .
Với anh , gia_đình , vợ_con có ý_nghĩa như_thế_nào ?
Đó là con đường để biến mọi ước_mơ thành hiện_thực .
Cảm_ơn và chúc chuyến hành_trình xuyên Đông_Tây của anh thành_công !
Đức_Trung ( thực_hiện )

﻿ Phim mới về Đức_Phật : " Tuyển_chọn " diễn_viên qua máy_tính
" Các kỹ_sư phần_mềm đã tạo nên những hình_ảnh tuyệt_vời dựa trên lịch_sử và những thông_tin có sẵn từ nhiều hội Phật_giáo . Chúng_tôi cần có một gương_mặt phù_hợp với những hình_ảnh này và đặc_tính của nam diễn_viên thủ vai Đức_Phật cũng rất quan_trọng . Chúng_tôi muốn bộ phim trở_thành tài_liệu tham_khảo cho các tín_đồ Phật_giáo nói_riêng và loài_người nói_chung " , B.K.Modi - Chủ_tịch Hội_Điện ảnh Phật_giáo có trụ_sở tại Beverly_Hills , bang California ( Mỹ ) , nơi sản_xuất bộ phim nói trên - cho_biết .
Đức_Phật sinh_ra trong một gia_đình hoàng_tộc vào thế_kỷ thứ 6 trước Công_nguyên ở Kapilavastu , hiện thuộc vùng biên_giới giữa Ấn Độ và Nepal .
Vì ngài cấm tạo_dựng các hình_ảnh của mình nên ban_đầu Đức_Phật được các môn_đệ miêu_tả với hình_ảnh một chiếc ngai rỗng , một_đôi chân hay chiếc bánh_xe .
Những hình_ảnh của Đức phật lần đầu_tiên xuất_hiện khoảng 500 năm sau khi ngài qua_đời khi các nghệ_sĩ ở Kandahar ( Afghanistan ) và các nhà điêu_khắc ở thành_phố Mathuna ( Bắc Ấn Độ ) bắt_đầu tạo nên chân_dung ngài .
Đạo_diễn Kapur từng nổi_tiếng thế_giới vào giữa thập_kỷ 90 của thế_kỷ trước với bộ phim Bandit_Queen .
Theo Thể thao & amp ; Văn_hoá

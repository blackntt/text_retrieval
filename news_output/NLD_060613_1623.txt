﻿ Thiên_thạch rơi xuống bắc Na_Uy
Nông_dân Peter_Bruvold - chuẩn_bị sẵn máy_ảnh để chụp cảnh con lừa cái của mình sinh con - đã may_mắn " bắt " được thiên_thạch giữa trời hè sáng Lúc 2 giờ sáng 7-6 ( giờ_địa_phương ) , một thiên_thạch lớn bay trên bầu_trời Troms và Finnmark , bắc Na_Uy và rơi xuống với sức va_đập mạnh ngang với quả bom rơi xuống Hiroshima , Nhật .
Nhật_báo Aftenposten nói thiên_thạch xuất_hiện như một quả cầu lửa , được nhìn thấy rõ trên quãng đường hàng trăm kilômet trong bầu_trời hè sáng trên vòng Bắc cực .
Sau đó , thiên_thạch rơi xuống một sườn núi không người_ở tại Reisadalen , bắc Troms . Trạm vật_lý địa_cầu Karasjok ( Na_Uy ) đã đo được sức va_đập của thiên_thạch vào mặt_đất . Nhiều nhà_cửa lay_động và rèm cửa bị thổi bật vào trong nhà những khu dân_cư lân_cận .
Knut_Jorgen_Roed_Odegaard - nhà thiên_văn hàng_đầu của Na_Uy - nói ông hi_vọng chứng_minh rằng thiên_thạch rơi xuống Na_Uy này là lớn nhất , nặng khoảng 1.000kg , phá kỷ_lục trước_đây của thiên_thạch Alta nặng 90kg vào năm 1904 . Ông nói khi so_sánh thiên_thạch này với bom_nguyên_tử ở Hiroshima là so về sức nổ chứ thiên_thạch không có phóng_xạ .
Theo Tuoi_Tre

﻿ Bạn của chàng
Người phụ_nữ đàn_ông thích kết_bạn và để yêu , để lấy_làm vợ có_thể là hai mẫu người hoàn_toàn khác_nhau nhưng đôi_khi … tình_yêu vẫn bắt_đầu bằng câu nói : “ Chúng_ta hãy là bạn của nhau nhé ” .
Các nhà tâm_lý_học đã tìm ra những mẫu phụ_nữ tiêu_biểu mà các quý ông sẵn_lòng được kết_bạn như sau :
Quyến_rũ
Quyến_rũ là cá_tính tạo_hóa dành riêng cho phụ_nữ . Họ tỏ ra “ hâm_mộ ” nam_giới và không hề giấu_giếm về điều này . Nhưng chớ hiểu lầm người phụ_nữ quyến_rũ với người phụ_nữ “ thiếu đứng_đắn ” , vì người phụ_nữ thiếu đứng_đắn , đàn_ông có_thể “ sử_dụng ” họ khi cần , còn người phụ_nữ quyến_rũ thì luôn làm cho người đàn_ông say_mê về_lâu_về_dài .
Dịu_dàng
Đó là những cô_gái có sự vui_vẻ , khiến cho mọi người xung_quanh bị ảnh_hưởng theo lối sống của họ . Cô ấy luôn tỏ ra hạn phúc khi gặp bạn và bạn cũng luôn thấy mong_muốn được gặp_gỡ , gần_gũi cô ấy .
Chung_thủy
Đàn_ông sợ nhất_là bị người_yêu , vợ cho “ mọc_sừng ” vì_thế , họ luôn tìm đến những người phụ_nữ mà mọi quan_tâm của cô ấy trong đời đều dành cho bạn . Bạn là người được cô ấy chọn thì dù bạn có ở trong hoàn_cảnh nào , cô ấy cũng là người chia_sẻ với bạn mọi niềm_vui , nỗi_buồn mà không một lời ca_thán , oán_trách .
Thân_thiện
Bạn vừa gặp cô ấy trong một khoảng thời_gian ngắn_ngủi thôi mà đã tưởng_chừng như quen nhau tự bao_giờ rồi . người phụ_nữ như này luôn đem lại cảm_giác thoải_mái cho phái_mạnh : có nhiều sở_thích , ý_muốn , nguyện_vọng giống nhau … Nếu kết_bạn với mẫu người như cô ấy , cô ấy sẽ luôn ở bên bạn .
Thẳng_thắn
Đây là người phụ_nữ rất giỏi trong giao_tiếp , ứng_xử . Họ không_bao_giờ đóng_kịch , không sống giả_dối nên họ cũng không thích những người đàn_ông luôn nói lời “ có cánh ” .
Họ nói_năng cũng thẳng_thắn như ý_nghĩ của họ và họ có_thể thực_hiện được những lời họ đã nói ra . Họ không lãng_mạn , nhưng đối_với họ , bạn biết rõ mình đang ở vị_trí nào trong cuộc_đời họ .
Không màng đến vật_chất
Đây là mẫu người phụ_nữ khó tìm nhất trong thời nay , bởi cô ấy phải có tính tự_lập trong kinh_tế , không muốn sống dựa vào bất_kỳ người đàn_ông nào , dù đó là người_yêu hay chồng .
Những người phụ_nữ này khi yêu ai , tình_yêu xuất_phát từ trái_tim chứ không màng đến việc mình được hưởng gì từ người đàn_ông đó .
Tự_tin
Người phụ_nữ tự_tin hài_lòng với bản_thân mình , kể_cả điều tốt lẫn điều xấu . Cô ấy cũng tự_tin cả trong suy_nghĩ về bạn , kể_cả với ưu_điểm hay khuyết_điểm .
Cô ấy cũng có đủ lòng tự_trọng để vững_tin đi theo con đường mà mình đã lựa_chọn , tin vào người mà cô ấy chọn . Cô ấy sẽ là người bạn_đường - bạn_đời lý_tưởng của người đàn_ông nếu_như hai người cùng có chung một lý_tưởng .
Có cá_tính
Đây là người phụ_nữ luôn thông_minh , dí_dỏm và hoạt_bát . Cô ấy cũng luôn là trung_tâm trong các buổi nhóm_họp , giao_tiếp … Có_mặt cô ấy , mọi buổi gặp_mặt đều trở_nên vui_vẻ , sống_động hơn .
Những người phụ_nữ có cá_tính luôn biết cách thu_hút nam_giới như thỏi nam cham vậy . Ở bên cô ấy , dường_như bạn quên mất cả khuyết_điểm của cô ấy nữa .
Độc_lập
Phải là người phụ_nữ có bản_lĩnh thì mới độc_lập được . Họ dám “ cạnh_tranh ” với giới mày_râu trong mọi lĩnh_vực và chính điều này đã tạo ra_sức “ hút ” một_cách tự_nhiên , mạnh_mẽ đối_với cánh mày_râu .
Thật_ra , đàn_ông rất “ mê ” mẫu người phụ_nữ mà họ vừa yêu lại vừa kính_trọng . P . M

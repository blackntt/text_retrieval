﻿ Doanh_nghiệp Tây_Ban_Nha coi_trọng đầu_tư vào VN
Ông La - ca-sa cho_biết chuyến thăm cho thấy đây là thời_điểm rất thích_hợp để đầu_tư vào VN và Thái_Lan vì hai nước này đang củng_cố các chính_sách kinh_tế và xã_hội , đồng_thời chuẩn_bị nhiều dự_án phát_triển hạ_tầng trong các lĩnh_vực có_thể mở ra nhiều cơ_hội làm_ăn cho các doanh_nghiệp Tây_Ban_Nha .
Ông La - ca-sa còn nhấn_mạnh các quyết_định của Chính_phủ VN thể_hiện rõ quan_điểm phi tập_trung hóa và theo_đuổi một con đường phát_triển rất năng_động , với mức tăng_trưởng kinh_tế lên tới hơn 8% trong năm 2005 .
Nhà_doanh_nghiệp này cho rằng là một nước sản_xuất và xuất_khẩu dầu_mỏ , VN có điều_kiện để phát_triển các dự_án hạ_tầng rất hấp_dẫn đối_với các nhà_đầu_tư Tây_Ban_Nha , trong đó có các dự_án về giao_thông vận_tải như xây_dựng đường tàu_điện_ngầm tại thành_phố Hồ_Chí_Minh , đường xe_điện tại Hà_Nội và dự_án xây_dựng đường_sắt .
Ông La - ca-sa cho_biết thêm các doanh_nghiệp Tây_Ban_Nha cũng quan_tâm đến các dự_án nước_sạch , hóa_dầu và chế_tạo_máy của VN và hy_vọng việc đầu_tư của Tây_Ban_Nha trong các dự_án nói trên sẽ củng_cố vị_trí của Tây_Ban_Nha ở Việt_Nam trong vòng 3-4 năm tới .
Theo TTXVN

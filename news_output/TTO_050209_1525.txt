﻿ Philippine tổng_tấn_công vào Abu_Sayyaf
Trung_tá Buenaventura_Pascual , phát_ngôn_viên của quân_đội Philipppine cho_biết là quân_đội nước này đã tung ra cuộc tấn_công chống lại lực_lượng Abu_Sayyaf có liên_hệ đến nhóm khủng_bố al Qaeda và một cuộc tấn_công khác nhằm vào lực_lượng của Mặt_trận giải_phóng dân_tộc Moro ( MNLF ) . Quân_đội Philippine đã tổn_thất 20 lính và cũng đã tiêu_diệt được 40 quân nổi_dậy qua 3 ngày giao_tranh . Có khoảng 30 lính Philippine bị_thương .
Cuộc giao_tranh bắt_đầu vào ngày thứ_hai 7-2 khi hàng trăm quân nổi_dậy lập căn_cứ trong vùng rừng_núi Jolo đã phục_kích và tấn_công vào một đoàn xe quân_sự ở thị_trấn Patikul trên đảo Jolo
Có khoảng 3000 lính_dù Philippine với sự hỗ_trợ của máy_bay ném bom và máy_bay_trực_thăng đã tấn_công trả_đũa vào lực_lượng của Abu_Sayyaf và MNLF gồm khoảng 500 tay súng .
Trước_đây 2 nhóm nổi_dậy này đều đã ký_kết các thỏa_thuận hòa_bình với chính_phủ Philippine nhưng các thành_phần chống_đối quá_trình hòa_bình trong 2 nhóm này vẫn tiếp_tục cuộc chiến_tranh_du_kích của riêng mình . Đa_số các tay súng của lực_lượng MNLF đều đã gia_nhập vào hàng_ngũ của nhóm Abu_Sayyaf .
HOÀNG_KIM_ANH ( Theo Reuters )

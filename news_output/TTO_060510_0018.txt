﻿ Ngân_hàng Thế_giới tổ_chức cuộc_thi về kinh_doanh
Mục_đích của cuộc_thi là khuyến_khích những nghiên_cứu , tìm_tòi mới của các đối_tượng từ khắp_nơi trên thế_giới cho các nhà hoạch_định chính_sách kinh_tế , tài_chính , cộng_đồng tài_chính quốc_tế hay các nhà_đầu_tư trong và ngoài nước .
Cuộc_thi có sáu giải_thưởng gồm : một giải vàng trị_giá 30.000 USD , hai giải bạc - mỗi giải 15.000 USD và ba giải đồng - mỗi giải 10.000 USD . Các bài dự thi phải viết bằng tiếng Anh , tối_đa 4.000 chữ và nộp về Công_ty Tài_chính quốc_tế của WB chậm nhất ngày 30-6-2006 qua đường bưu_điện hoặc thư_điện_tử . Xem thông_tin chi_tiết tại http : / / www.ifc.org/ifcext/economics.nsf/Content/competition
HƯƠNG_GIANG

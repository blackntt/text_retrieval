﻿ Một giờ ' ' nhăng_cuội ' ' với Quốc_Bảo
Nếu_Quốc_Bảo cưỡi chiếc xe cà_tàng chờ cô nào_đó ở đầu phố , chắc_chắn sẽ được những cánh_tay vẫy : " Ê , xe_ôm ! " . Liệu chàng nhạc_sĩ của những ca_khúc thời_thượng nổi_tiếng trong lớp trẻ có toe toe cười khi tôi mô_tả " nếu " như_vậy ?
Tôi cũng chẳng hỏi ông ảnh_hưởng nhạc_sĩ nào , nhạc_sĩ nào_là thần_tượng , và với ông , ông thích cô ca_sĩ nào hát nhạc của ông nhất . Tôi chỉ đi men cuộc_đời ông - một con_người của âm_nhạc , thế thôi .
Tôi sợ nhất_là vô_tình mô_tả vẻ bề_ngoài thô thô của các nữ ca_sĩ . Đúng nghĩa tởn đến ... cụ luôn , mặc_dù , ẩn sau sự miêu_tả bề_ngoài thô thô ấy_là lời khen tụng ngất_trời tài_năng , tâm_hồn của các cô , các bà . Một bài_học để đời khi tôi viết về ca_sĩ Lê_Dung theo bài " tán " trên đã bị ca_sĩ Lê_Dung chửi cho một trận đã_đời .
Quốc_Bảo - đàn_ông tôi chả sợ giận . Một_chiều Sài_Gòn có một gã nhạc_sĩ bề_ngoài như một bác xe_ôm ấy ( xin khoe trước , chính tôi đã từng đóng vai xe_ôm trong phim " Dốc tình " ) hẹn tôi chuyện_trò nhăng_cuội cho vui ở một nhà_hàng rất danh_tiếng ngay trung_tâm Sài_Gòn với cái giá một ly kem ... 60.000 đồng .
Đúng giờ , tôi xuất_hiện , bên_cạnh gã một nàng ca_sĩ tên Thủy_Tiên đẹp ... như tiên . Mốt mà , các nhạc_sĩ tên_tuổi thường cùng một cô ca_sĩ trẻ xinh_đẹp nào_đó cặp_kè . Tôi sợ sẽ hỏi một nơi mà mắt một nẻo khi chuyện_trò cùng Quốc_Bảo . Tôi rủ Quốc_Bảo qua ngồi bàn khác để cô nàng xinh_đẹp có_thể là một ngôi_sao ca_nhạc tương_lai kia ngồi thưởng_thức món mì Italia một_mình .
Này , giọng ông sao cứ lơ_lớ chả rõ miền nào vậy ?
Tôi họ Bùi , bố quê Hà_Nam , mẹ quê Thái_Bình , đẻ tôi ở Sài_Gòn .
Ông tự nhận dân Bắc hay Nam ?
Tôi tự coi mình là dân Sài_Gòn .
Ông sinh_ra sau cuộc di_cư 1954 , cụ_thể là năm nào ?
Đây là tư_liệu tôi không công_bố .
Nhưng đến mấy tuổi ông đến với âm_nhạc thì thuộc loại tư_liệu công_khai chứ ?
5 tuổi , phải nói cho rõ câu trên , tôi tự coi mình là dân Sài_Gòn vì quá hiểu Sài_Gòn , nhưng tôi vẫn tự_hào từ trong tiềm_thức xứ Bắc luôn ẩn_hiện trong tôi .
Cụ_thể ?
Khả_năng sử_dụng ngôn_ngữ mà tôi học được từ bố_mẹ tôi - một thứ ngôn_ngữ giàu âm_điệu .
Là nhạc_sĩ chắc ông thích ngao_du , những lúc xa Sài_Gòn ông thích gì nhất ?
Quán cà_phê , không_khí quán cà_phê .
Phải_chăng có sự khác_biệt nhau giữa không_khí quán cà_phê Hà_Nội , Huế , Sài_Gòn , Đà_Lạt ?
Mỗi nơi có thú_vị riêng của nó . Nhưng cái không_khí vừa riêng_tư , vừa chung_đụng vừa quen vừa lạ mỗi kẻ khách chỉ có ở Sài_Gòn .
Mở hay kín ?
Cả hai .
Nào , trở_lại cái tuổi lên 5 ...
Đó là lúc tôi biết tò_mò . Một lần tôi vô_tình mở một tạp_chí cho trẻ_con , ở trang cuối tôi thấy vẽ những dấu_hiệu gì đấy kỳ_quặc lắm không giống những chữ a , b , c mà tôi học .
Tôi coi đó như những con thú hoặc những bông hoa xinh_xinh , tôi lấy bút đồ theo như đồ một bức tranh . Mẹ thấy tôi làm_vậy lén hỏi một ông thầy nhạc : " Thằng bé nhà tôi 5 tuổi có_thể học nhạc được không ? " . Ông thầy nhạc bảo : " Thích thì cho học ! " . Ông dạy tôi đọc những cái dấu_hiệu ấy được gọi_là ký xướng_âm . Tôi bắt_đầu mở những nốt_nhạc ra như mở cái đồng_hồ xem bên trong có gì , tháo tung rồi cố ráp lại .
Và hứng_thú ?
20 tuổi_tôi mới có cảm_giác hứng_thú . Tôi say_sưa ráp các nốt_nhạc theo ý mình không phải trên cây đàn Măng đô lin nữa mà trên cây đàn ghi ta . Tôi không dám gọi những bức ráp ấy_là ca_khúc . Sau đó tôi học nhạc chính_quy suốt 3 năm trời và thêm 10 năm nữa làm ông thầy dạy nhạc , lúc ấy tôi mới thực_sự hiểu cái trò_chơi tuổi_thơ của mình chính là âm_nhạc .
Ông đến với âm_nhạc thực_sự chuyên_nghiệp từ khi nào ?
Noel 1997 khi tôi tham_gia đầy_đủ quy_trình sản_xuất một đĩa_nhạc , mặc_dù từ 6 năm trước đó tôi đã theo các nhạc_sĩ Trịnh_Công_Sơn , Phạm_Trọng_Cầu đi hát ...
Bây_giờ ở những quán cá phê Sài_Gòn , liên_tục đốt thuốc , ông có thói_quen nhâm_nhi âm_nhạc của mình ?
Tôi không có thói_quen ấy !
Vậy lúc_nào ông nghe âm_nhạc của mình ?
Chỉ ở phòng thu , nghe rất kỹ . Khi sản_phẩm ghi_âm xong , tôi không nghe nữa ...
Nhưng người_ta thấy ông vào quán cà_phê thì người_ta mở bài hát của ông như một lời chào đối_với ông ?
Tôi bảo chủ quán tắt đi .
Ông " cắm " lâu nhất_là mấy tiếng đồng_hồ ở một quán cà_phê ?
4 tiếng .
Còn vừa bước vào là đi ra ngay ?
Rất nhiều lần .
Vì điều gì ?
Tiếng ồn sinh_ra bởi âm_nhạc . Tôi rất sợ tiếng ồn sinh_ra bởi âm_nhạc . Vào quán , tôi không dị_ứng chỗ ngồi , mà chỉ dị_ứng cách mở nhạc , ví_dụ bài nhạc tôi không thích hoặc bài hát thu âm tồi , tần_số chói tai ...
Quốc_Bảo trò_chuyện với tôi , không ngừng đốt thuốc hoặc để thuốc tự cháy , còn đôi mắt thỉnh_thoảng liếc mắt xem cô ca_sĩ trẻ Thủy_Tiên đang làm_gì . Tôi không hỏi chuyện đời tư của ông . Tôi chỉ tả lại những gì tôi quan_sát .
Ông là người thích tháo tung ra ráp cái này vào chỗ cái kia nhưng khó có nhạc_sĩ nào ở Việt_Nam hiện_nay thoát khỏi những cái khuôn ...
Thoát khỏi ư ? Thật_ra là muốn phá bung mọi sự gò_bó . Bằng cách nào ? Bằng cách tự phân_thân thôi , chia mình ra làm hai mảng , một cho đại_chúng , một cho ý_đồ nghệ_thuật riêng của mình . Đó là một sự chấp_nhận để tồn_tại .
Ông có mệt_mỏi vì sự chấp_nhận này ?
Tự mình thấy mình chỉ là con ốc_vít trong guồng quay và ... chán . Cái cảm_giác đó xảy_ra thường_xuyên , nhưng ( phả hơi thuốc kèm theo một nụ_cười vu_vơ ) không đến_nỗi trầm_trọng .
Tình_thế đó do bản_tính cá_nhân ông hay do nhìn ra đâu cũng thế ?
Câu hai hợp_lý hơn .
Tôi có ông bạn rất mê âm_nhạc , lúc ở Việt_Nam ông cũng thích một_số nhạc_sĩ Việt_Nam , nhưng qua định_cư ở nước_ngoài , nghe nhạc nước_ngoài thường_xuyên , về thăm quê , ông bạn nói với tôi : " Đếch nghe nhạc Việt_Nam được nữa mày ạ ! " ...
Với tư_cách một người Việt_Nam , sản_xuất âm_nhạc Việt_Nam , tôi nghĩ vẫn có một lối đi mặc_dù khá ngoắt nghéo để hòa_nhập với âm_nhạc thế_giới .
Cách nào ?
Cách nào thì_phải nghiên_cứu , tìm_hiểu xem thế_giới muốn nghe gì ở âm_nhạc Việt_Nam .
Phải_chăng ở đây có vấn_đề tiết_tấu dẫn đến âm_nhạc Việt_Nam chưa được thế_giới tiếp_nhận theo nghĩa hút_hồn ?
Âm_nhạc phương Đông không mạnh về tiết_tấu . Nhưng tôi nghĩ vẫn có những cái cách hút_hồn được thế_giới , đó là âm_sắc của nhạc_cụ , của giọng hát .
Giọng hát ư ? Chắc ông thừa biết các ca_sĩ Việt_Nam đang ở mặt_bằng nào ? Còn các nhạc_sĩ của ta lâu_nay ưa quẩn_quanh với chính mình . Vấn_đề hòa_nhập theo tôi phụ_thuộc ở những tài_năng âm_nhạc lớn luôn lao_động cực_nhọc , luôn học_hỏi và sáng_tạo .
Tôi thừa_nhận với ông không ít nhạc_sĩ của ta còn ... lười . Nhưng vấn_đề còn là không_khí âm_nhạc và đặc_biệt là môi_trường thưởng_thức âm_nhạc của công_chúng .
Phải_chăng công_chúng nào thì âm_nhạc ấy vẫn luôn là công_thức đúng và luôn được áp_đặt như một định_mệnh ? Vấn_đề công_chúng là vấn_đề của xã_hội . Ông thú_thật đi , có lúc_nào ông cùng các đồng_nghiệp của ông cảm_thấy mệt_mỏi trước công_chúng như công_chúng hôm_nay ?
Quốc_Bảo nói nhanh : - Sợ thì đúng hơn ! Thôi tôi sắp đến giờ lên học tiếng Pháp rồi .
Quốc_Bảo đứng_dậy không cần biết thái_độ của tôi thế_nào , ông đi qua bàn của cô ca_sĩ trẻ xinh_đẹp , hình_như ông muốn dành ít phút còn lại trước khi đi học ngoại_ngữ cho riêng cô ca_sĩ này . Tốt thôi , tôi đứng_dậy . Chào nhé ! Theo Mỹ_Thuật

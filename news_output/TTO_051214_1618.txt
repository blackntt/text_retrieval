﻿ Ông_già Noel giao sách tận nhà
Dịch_vụ ông_già Noel giao sách và quà được thực_hiện bằng hai hình_thức : miễn_phí với những hoá_đơn từ 150.000 đồng ( ở TP.HCM ) và 100.000 đồng ( ở các tỉnh_thành khác ) . Với những hoá_đơn này , khách_hàng được giao sách và quà miễn_phí tại các quận nội_thành TP.HCM cũng_như nội_thị các tỉnh_thành khác .
Ngoài_ra , dịch_vụ ông_già Noel giao sách còn đáp_ứng các khách_hàng có yêu_cầu , với mức phí từ 30.000 đồng đến 50.000 đồng .
Đợt này , Phương_Nam có giảm_giá một_số mặt_hàng sách , từ 10% đến 80% , trưng_bày tủ_sách quân_đội nhân_dân mừng ngày thành_lập QĐND_VN , và dự_kiến sẽ phát_hành 2500 tựa sách thiếu_nhi bản tiếng Anh vào dịp cận Noel .
LAM_ĐIỀN

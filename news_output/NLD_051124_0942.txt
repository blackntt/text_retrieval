﻿ Thêm 14 trường ĐH , CĐ được đào_tạo liên_thông
Ngày 23-11 , Thứ_trưởng Bộ GD - ĐT Bành_Tiến_Long đã quyết_định phê_duyệt cho 21 trường ĐH , CĐ được đào_tạo liên_thông ( ĐTLT ) với 3.580 chỉ_tiêu .
Trong số này có bảy trường đã tham_gia thí_điểm ĐTLT nay được mở_rộng thêm ngành đào_tạo với tổng_cộng 1.350 chỉ_tiêu .
Còn lại 14 trường gồm ba trường ĐH và 11 trường CĐ mới bắt_đầu được thực_hiện ĐTLT từ năm 2006 với 2.230 chỉ_tiêu .
Hầu_hết các trường được ĐTLT từ bậc THCN lên CĐ . Riêng hai trường ĐH bán_công Tôn_Đức_Thắng và ĐH Sư_phạm kỹ_thuật Hưng_Yên được ĐTLT từ THCN lên ĐH , hai trường ĐH Xây_dựng Hà_Nội và Học_viện Ngân_hàng được ĐTLT từ bậc CĐ lên ĐH .
Theo Tuổi_Trẻ

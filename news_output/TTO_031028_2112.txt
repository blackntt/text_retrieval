﻿ Thị_trường PDA sẽ giảm 25% trong năm 2003
Trong quý 3 , mặc_dù Palm vẫn giữ vị_trí đầu_bảng trên thị_trường PDA , nhưng thị_phần của hãng đã giảm từ 41,8% xuống còn 35,6% . Trong khi đó , bằng cách tung ra các sản_phẩm mới hấp_dẫn , HP đã nhanh_chóng giành thêm “ miếng bánh ” , đạt 24,5% thị_phần ( tăng 52,5% so với quý trước ) .
Sony đứng vị_trí thứ_ba với 11,5% thị_phần , giảm so với con_số 11,9% của quý 2 . Mặc_dù mới tham_gia thị_trường nhưng Dell đã nhanh_chóng giành vị_trí thứ 4 với 5,7% và Toshiba đứng thứ 5 với 3,6% thị_phần .
THÙY_MINH ( Theo Techweb )

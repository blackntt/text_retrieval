﻿ Indonesia thả kẻ chủ_mưu vụ đánh bom Bali
Chính_quyền Indonesia hôm_nay 14/6 đã thả trước thời_hạn giáo_sĩ Hồi_giáo Abu_Bakar_Bashir , kẻ đứng đằng sau vụ đánh bom đảo Bali năm 2002 khiến 202 người thiệt_mạng .
Abu_Bakar_Bashir , 68 tuổi , đã được thả ra sau hơn 26 tháng thụ_án trong một nhà_giam ở thủ_đô Jakarta . Khoảng 300 người ủng_hộ giáo_sĩ này đã đến đón_chào ông và hô vang khẩu_hiệu “ Chúa_trời vĩ_đại ” .
Sau khi ra khỏi nhà_tù , giáo_sĩ Bashir vẫy tay chào những người ủng_hộ và đám đông phóng_viên đang đợi_chờ đưa tin . “ Tôi sẽ tiếp_tục chiến_đấu để nâng cao vị_thế của đạo Hồi . Cảm_ơn thánh Allah , cảm_ơn những người ủng_hộ tôi , ” ông nói
Khi được hỏi , liệu ông có lo_lắng khi bị cảnh_sát giám_sát sau khi ra tù , giáo_sĩ Bashir nói rằng ông không quan_tâm . “ Nếu muốn , họ ( cảnh_sát ) cứ_việc , ” Bashir trả_lời báo_chí .
Với việc giáo_sĩ Bashir được thả ra , nhiều người lo_ngại một làn_sóng tấn_công khủng_bố mới sẽ gia_tăng tại Indonesia , đất_nước Hồi_giáo lớn nhất thế_giới .
Tháng 3/2005 , Bashir bị chính_quyền Indonesia kết_án 30 tháng tù_giam vì có liên_quan tới vụ đánh bom khủng_bố hòn đảo Bali năm 2002 làm 202 người thiệt_mạng . Mỹ lên_án Indonesia khi nói rằng mức án như_vậy là quá nhẹ đối_với một tên khủng_bố như Bashir .
Mỹ cho rằng Bashir là một lãnh_đạo của tổ_chức khủng_bố Hồi_giáo Jemaah_Islamiyah có liên_hệ với al - Qaeda . Tuy_nhiên Bashir đã phủ_nhận .
Nguyên_Hưng_Theo AFP

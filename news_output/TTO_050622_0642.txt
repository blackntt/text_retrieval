﻿ Kiên_Giang : 50 giáo_viên ở Phú_Quốc bị quên nâng lương
Hầu_hết trong số này là giáo_viên từ đất_liền tình_nguyện ra công_tác lâu_dài tại đảo Phú_Quốc . Người bị “ quên ” không được nâng bậc lương lâu nhất_là tám năm ( bốn bậc ) , ít_nhất là bốn năm ( hai bậc ) - theo qui_định hai năm xét nâng bậc lương một lần .
Giải_thích vấn_đề này , ông Phạm_Đình_Huy - trưởng Phòng GD - ĐT huyện đảo Phú_Quốc - thừa_nhận đây là thiếu_sót của ban_giám_hiệu các trường và cán_bộ phụ_trách tổ_chức của Phòng GD - ĐT huyện đã “ quên ” đề_xuất tái_hợp đồng nâng lương cho số giáo_viên đúng niên_hạn theo qui_định , dẫn đến thiệt_thòi quyền_lợi cho giáo_viên .
H . T . DŨNG

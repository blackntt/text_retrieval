﻿ Bi chuẩn_bị lưu_diễn tại châu Á
Tổng_cộng cả chuyến lưu_diễn này sẽ có khoảng 30 buổi biểu_diễn . Mặc_dù lịch diễn cụ_thể vẫn chưa được xác_định nhưng chuyến lưu_diễn vòng_quanh châu Á sẽ được khởi_đầu bằng hai đêm diễn tại Hong_Kong vào ngày 8 và 9-10 tới .
Hiện rất nhiều khán_giả đang trông_đợi chương_trình Rainy_Day in Hong_Kong này . Toàn_bộ vé đã được tiêu_thụ hết ngay trong ngày đầu_tiên bán vé . Còn giá vé “ chợ_đen ” dự_kiến sẽ cao gấp 5 lần giá vé chính_thức .
Ngay cả “ Thiên vương ” Quách_Phú_Thành , ca_sĩ có vũ_đạo xuất_sắc nhất Hong_Kong cũng có lời khen_ngợi về vũ_đạo của Bi và cho_biết nếu thời_gian cho_phép , anh cũng sẽ tham_gia chương_trình của Bi tại Hong_Kong với tư_cách khán_giả .
Vào tháng 7 , Bi cũng đã được chào_đón cực_kỳ nồng_nhiệt tại Hong_Kong khi đến đây tuyên_truyền cho album mới của mình .
T.MINH ( Theo CRI )

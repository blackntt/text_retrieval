﻿ Cá hấp jambon
Đầu_bếp Quách_Tuấn của nhà_hàng Á_Đông giới_thiệu cùng bạn_đọc sự kết_hợp của Âu và Á trong món cá hấp jambon . Có_thể dùng cá_bống_mú , cá chẻm để làm món ăn này do các loại cá này ít xương và dễ lấy xương
Nguyên_liệu :
Cá_bống_mú : con 800g
Jambon : 100g
Nấm đông cô : 50g
Tỏi băm : 1 muỗng cà_phê
Muối : 1/2 muỗng cà_phê
Tiêu : 1/4 muỗng cà_phê
Đường : 1/2 muỗng cà_phê
Cải rổ : 12 cây
Cà_chua : 1 trái
Cách chế_biến :
1 . Cá làm sạch , lạng lấy hai bên phi_lê , bỏ xương_sống giữ đầu , kỳ và đuôi . Cắt cá thành từng miếng , ướp gia_vị vào cá cho thấm khoảng 10 phút .
2 . Nấm đông cô ngâm cho nở chẻ đôi .
3 . Xếp theo thứ_tự 1 miếng nấm , 1 miếng jambon và 1 miếng cá cho_đến hết .
4 . Mang đĩa cá hấp cách_thuỷ từ 8-10 phút . Cải rổ luộc chín riêng cho vào đĩa cá khi đã hấp xong .
Cá hấp jambon ăn với nước tương_ớt .
Theo SGTT

﻿ Báo_Giác_Ngộ khánh_thành trụ_sở mới
Tòa_sọan mới của báo Giác_Ngộ - Ảnh : H . S .
TTO - Sáng 12-11 , tòa_soạn mới của tuần_báo Giác_Ngộ đã được khánh_thành long_trọng tại địa_chỉ cũ là 85 Nguyễn_Đình_Chiểu , Q . 3 , TP.HCM , sau hơn một năm thi_công xây_dựng .
Báo_Giác_Ngộ đuợc thành_lập từ ngày 7-11-1975 . Từ 1976-1996 , tờ báo được thực_hiện dưới hình_thức bán_nguyệt_san . Từ năm 1996 đến nay là tuần_báo và mỗi tháng có thêm phụ_trương nguyệt_san nghiên_cứu Phật học .
Nhân_dịp khánh_thành tòa_soạn mới , CLB Thư_họa báo Giác_Ngộ cũng tổ_chức triển_lãm mỹ_thuật chào_mừng gồm 70 tác phầm tranh , tượng , gốm , thư_pháp ( Hoa , Việt ) . Triển_lãm sẽ kéo_dài đến 20-11-2005 , tại tầng thượng báo Giác_Ngộ .
H . SƠN

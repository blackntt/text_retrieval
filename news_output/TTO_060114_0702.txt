﻿ Thử_thách cho AC Milan
Hiện_tại , AC đang đứng thứ 2 trên bảng xếp_hạng Serie A và kém đội đầu_bảng Juventus 9 điểm , và nhiều hơn đội_xếp phía sau Inter 1 điểm ( sau 18 vòng ) .
Vì_thế , chỉ có chiến_thắng trước AS Roma ngay trên sân_khách thì họ mới giữ vững được vị_trí thứ 2 của mình và cũng chỉ có chiến_thắng mới giúp họ không bị đội đầu_bảng Juventus nới rộng khoảng_cách sau 19 vòng đấu ( Juventus và Inter nhiều khả_năng sẽ giành trọn 3 điểm ) .
Nếu đối_thủ của AC Milan không phải là AS Roma , mà là một đội bóng nhỏ nào khác thì việc giành trọn 3 điểm sẽ rất dễ_dàng . Tuy_nhiên , đây lại là AS Roma , một đội bóng mạnh của thành Rome , và là một đại_gia đang trên con đường tìm lại ánh hào_quang xưa .
Lịch thi_đấu vòng 19 :
Lecce - Sampdoria_Messina - Palermo_Ascoli - Empoli_Fiorentina - Chievo_Inter - Cagliari_Juventus - Reggina_Livorno - Siena_Parma - Lazio_Udinese - Treviso AS Roma - AC Milan_Không chỉ có vậy , AS Roma còn được thi_đấu trên sân_nhà trong trận đấu này , và đó sẽ lại là một bất_lợi nữa cho các cầu_thủ AC Milan .
Dù phải thi_đấu trên sân_khách và lại đụng phải đối_thủ khó_chịu nhưng với bản_lĩnh của một đội bóng lớn , và cùng_với thành_tích đối_đầu vượt_trội , AC Milan hoàn_toàn có quyền hy_vọng về một chiến_thắng , nhất_là khi họ đang có trong tay bộ đôi sát_thủ : Shevchenko - Gilardino đang đạt phong_độ rất cao .
Trong khi AC Milan phải giáp_mặt với một AS Roma đầy khó_chịu , thì Juventus và Inter chỉ đụng phải những đối_thủ nhẹ ký trên sân_nhà .
Đội đầu_bảng Juventus sẽ đón_tiếp đội bóng yếu Reggina trên sân_nhà với đầy_đủ những cầu_thủ ưu_tú nhất của mình . Và với phong_độ đang rất ổn_định , có_thể nhận thấy Juventus sẽ không gặp nhiều khó_khăn để giành trọn 3 điểm trước Reggina đang phải đối_mặt với cuộc_chiến trụ hạng .
Tương_tự như Juventus , Inter cũng chỉ gặp phải đối_thủ nhẹ ký Cagliari trên sân_nhà . Sau 2 trận liên_tiếp không giành được chiến_thắng và cũng chưa ghi được 1 bàn thắng , các cầu_thủ Inter đang bị các CĐV chỉ_trích rất dữ_dội . Vì_thế , trận đón_tiếp Cagliari sẽ là cơ_hội lý_tưởng để hàng công của họ giải_tỏa cơn khát bàn thắng , và cũng là cơ_hội để các cầu_thủ Inter lấy lại hình_ảnh của mình trong mắt các CĐV .
Một trận đấu quá chênh_lệch , và sẽ rất bất_ngờ nếu_như Inter tiếp_tục không giành được chiến_thắng ?
T . VĨ

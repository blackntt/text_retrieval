﻿ LHQ cảnh_báo nguy_cơ thiếu nơi ẩn_náu cho nạn_nhân Indonesia
Các nhà_khoa_học nói một trận động_đất khác khó có khả_năng xảy_ra sớm nhưng các chấn_động mới vẫn còn lan_tỏa khắp khu_vực xảy_ra thảm_họa , nơi LHQ cho_biết đang cần khẩn_cấp vật_liệu xây_dựng để dựng nơi ẩn_náu cho những người còn sống_sót .
Nhà_chức_trách cũng cảnh_báo trong khi thuốc_men và thực_phẩm đang được tiếp_tế thì tình_trạng thiếu_thốn nhà_vệ_sinh đang làm tăng lên các lo_ngại về dịch_bệnh khi chất_thải tràn ra ở nhiều khu_vực .
Charlie_Higgins , điều_phối_viên cứu_trợ của LHQ tại khu_vực bị động_đất cho_biết nhiều người mất nhà_cửa vẫn đang phải đối_mặt với việc sống ngoài_trời , sống chật_vật trong các lều tạm và nhiều người khác vẫn cần các dụng_cụ và hàng tiếp_tế để xây_dựng những nơi trú_ẩn tốt hơn sớm ngay khi có_thể .
“ Hiện không có nhiều lều trại , mặc_dù lều trại là thứ hữu_dụng trong tình_huống này ” , Charlie_Higgins nói .
Chính_phủ Indonesia nói họ sẽ chi hơn 160 triệu USD cho việc xây_dựng lại nhà_cửa trong khu_vực bị động_đất , trong khi nhiều viện_trợ quốc_tế hơn đang được đổ_dồn về đây để giúp cho hàng chục_ngàn người bị_thương và mất nhà_cửa .
Trong khi đó , ở phía bắc khu_vực bị động_đất , ngọn núi_lửa Merapi làm tăng thêm các lo_ngại nó sẽ phún xuất khi vẫn tiếp_tục phun những đám tro bụi nóng_bỏng và đẩy những dòng dung_nham trào xuống sườn núi .
T.VY ( Theo AFP )

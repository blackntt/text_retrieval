﻿ USDA : Sản_lượng cà_phê của VN có_thể đạt 13,8 triệu bao
Báo_cáo trên cũng dự_đoán tiêu_thụ cà_phê trong nước của VN , ước đạt 38 tấn niên_vụ hiện_nay , có_thể tăng thêm 3% lên 39 tấn niên_vụ tới . Tuy_nhiên , mức tiêu_thụ này vẫn chỉ chiếm 5% tổng_sản_lượng cà_phê của VN .
Trong những năm gần đây , cả hai sản_phẩm cà_phê bột và cà_phê hòa_tan của các thương_hiệu Cafe_Moment , Vinacafe , Nescafe , Trung_Nguyên , G7 , VN , Biên_Hòa đã trở_nên quen_thuộc , trong đó hai hãng chiếm thị_phần lớn nhất tại thị_trường VN là Vinacafe với 50,4% và Nescafe với 33,2% .
Theo Hiệp hội Cà_phê và Ca cao và Bộ Thương mại VN , xuất_khẩu cà_phê của VN trong 6 tháng đầu niên_vụ 2005-2006 giảm 15% so với cùng kỳ niên_vụ trước , đạt 371 tấn , song tăng 25% về giá_trị .
Mỹ , Đức , Tây_Ban_Nha , Italia và Hàn_Quốc hiện là những nước nhập_khẩu cà_phê hàng_đầu của VN .
Theo TTXVN

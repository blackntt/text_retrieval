﻿ Yêu = đánh mất mình ?
TT - T . quen X . qua một chương_trình ca_nhạc rock khi ấn_tượng bởi X . - một chàng_trai ăn_nói lưu_loát , có duyên và đặc_biệt là giọng hát rất “ dữ_dội ” . Sau buổi biểu_diễn T . và các fan lên tặng hoa , được X . mời đi ăn khuya .
Sau vài tháng thư_từ qua_lại , hai tâm_hồn đồng_điệu chính_thức quen nhau .
Với quan_điểm “ yêu là phải chấp_nhận hi_sinh , tất_cả vì người_yêu ” nên T . bắt_đầu cuộc cải_tổ chính bản_thân mình cho xứng_đôi với phong_cách rock của X . . Nếu trước_đây T . chỉ thích màu xanh nước_biển và trắng thì nay quần_áo chuyển sang tông đen hết , áo_thun thì in hình những ban_nhạc nổi_tiếng trên thế_giới , cây đàn , sấm_sét ... vì theo T . , “ rock mà ” .
Có lần , một người bạn buột_miệng nói đại : “ Còn cái đầu sao không uốn xù lên cho giống X . luôn đi ! ” . Nói_chơi , ai_ngờ T . sáng_mắt , gật_đầu khen phải .
Đúng là khi yêu người_ta có_thể phải hy_sinh nhiều thứ , nhưng cuối_cùng hãy cứ là mình trong mọi hoàn_cảnh .
Có nên tự đánh mất mình trong mắt người khác không nhỉ ? ! Bữa sau T . nghỉ học đi uốn_tóc ngay . Tối đó T . về cười tíu_tít : “ Bạn anh X . khen tao nhìn quá rock , thành_công rồi ” . Và để rock hơn T . bắt_đầu học cách ăn_to_nói_lớn , đi hơi ngang_tàng một_chút và đặc_biệt là khi nghe nhạc sôi_động phải ngồi lắc_lư , giựt giựt ra_chiều am_hiểu , sành_điệu .
Bẵng đi một thời_gian lo thi học_kỳ , làm luận_văn tốt_nghiệp bạn_bè cùng phòng mới có dịp ngồi lại bên nhau . Buổi họp_mặt T . khóc thật nhiều , tháng rồi X . đã thẳng_thắn đề_nghị hai người nên chia_tay vì không hợp , và X . đã làm đám nói với cô bạn là giáo_viên tiểu_học gần nhà . Có lần X . dẫn vợ tương_lai theo giới_thiệu với T . : tóc dài , rất hiền , ăn_nói nhỏ_nhẹ y_như T . ngày_trước .
KIỀU_CHINH

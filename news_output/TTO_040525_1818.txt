﻿ Trao tiền_của bạn_đọc cho 5 em mồ_côi
Một_số địa_điểm sẽ nhận quà : Trại_Phong_Quy_Hòa ; Khoa nhi bệnh_viện Đa_Khoa tỉnh và bệnh_viện TP . Quy_Nhơn ; Cơ_sở dạy nghề cho trẻ_em tàn_tật Nguyễn_Nga ; Các lớp_học tình_thương ; Trung_tâm bảo_trợ Xã_hội tỉnh ; Trẻ_em nghèo 3 huyện miền núi … với tổng_số 1.015 suất quà , mỗi suất trị_giá 20.000 đồng .
Nhân_dịp nầy , UBDS - GĐ& amp ; TE tỉnh cũng sẽ tổ_chức cấp_phát 117 chiếc xe_lăn cho trẻ khuyết_tật , ( gồm 96 chiếc của tổ_chức Phi_chính_phủ Mỹ , và 21 chiếc của bạn_đọc báo Gia_đình xã_hội tặng ) .
Ngoài_ra , theo kế_hoạch , ngày 31-5 , Văn_phòng báo Tuổi_Trẻ tại Bình_Định sẽ phối_hợp với Ủy_ban dân_số GĐ& amp ; TE tỉnh trao số tiền 2.300.000 đồng của bạn_đọc báo Tuổi_Trẻ lần thứ 2 gởi tặng 5 em mồ_côi cả cha lẫn mẹ ở xã Phước_Thuận , Tuy_Phước nhân ngày thiếu_nhi 1-6 ( lần thứ 1 bạn_đọc báo Tuổi_Trẻ đã gởi tặng các em 1.700.000 đồng ) .
TÚ_ÂN

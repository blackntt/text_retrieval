﻿ Bạn có dám “ đá ” đối_thủ ra_rìa không ?
Bạn muốn có quyền_lực , tiền_bạc và sự ảnh_hưởng ? Bạn đang phải ngồi làm những công_việc của một nhân_viên quèn trong khi bạn là một nhà_lãnh_đạo bẩm_sinh ? Cuộc_chiến giành quyền_lực của bạn có quá nhiều đối_thủ , bạn có_thể “ đá bay ” họ không ?
Khi cơ_hội đến , không phải ai trong chúng_ta cũng biết nắm_bắt và tận_dụng nó . Nếu bạn có tố_chất của một người lãnh_đạo , có_thể bạn sẵn_sàng gạt phăng đối_thủ để tiến lên , cho_dù người đó là bạn_thân của bạn . Cũng có_thể bạn là một người cam_chịu , chấp_nhận nhìn kẻ khác thăng_tiến , mình cam_phận làm người phục_tùng . Hoặc bạn muốn đi lên , nhưng không nỡ “ đá ” bạn mình ra_rìa .
Bạn thuộc loại nào ? Hãy thật_thà trả_lời những câu_hỏi dưới đây , để hiểu rõ tham_vọng của mình .
1 . Bạn có đủ tính chuyên_nghiệp để không thuê một người bạn tốt vào làm_việc , mặc_dù cô ấy đang trong tình_trạng rất bi_đát vì thất_nghiệp ?
2 . Bạn có giữ được sự dịu_dàng của phụ_nữ không ngay cả khi bạn đang phải chiến_đấu với một đám người hỗn_tạp ?
3 . Bạn có luôn_luôn chuẩn_bị đầy_đủ cho buổi họp không , ngay cả khi tối hôm trước bạn đi “ ăn_chơi nhảy_múa ” suốt đêm ?
4 . Bạn không cho_phép cảm_xúc của mình lấn_át , mặc_dầu bạn đang cảm_thấy mệt_mỏi và khó_chịu ?
5 . Bạn tiếp_tục tin vào chính mình ngay cả khi có những tin_đồn không hay về bạn ?
6 . Bạn có_thể giữ một bộ_mặt lạnh_tanh khi đuổi việc một người nào_đó ?
7 . Bạn có một sự quyết_tâm đi đến thành_công ngay cả khi sếp của bạn nói là sắp cho bạn nghỉ_việc ?
8 . Bạn có đủ sức_khỏe và sự chịu_đựng để làm_việc trong những ngày nghỉ , dù phải bỏ bữa tiệc cưới của người bạn_thân nhất ?
9 . Bạn có_thể thân_thiện với tất_cả mọi người , từ tiếp_tân cho_đến giám_đốc điều_hành ?
10 . Bạn có đủ thoáng để lắng_nghe ý_kiến của một nhân_viên văn_phòng mới vào ?
11 . Bạn có đủ khách_quan để xem_xét một vấn_đề từ nhiều khía_cạnh khác_nhau ?
12 . Bạn có_thể thông_cảm với người làm_thuê cho bạn mặc_dù cô ta làm_bạn giận phát điên ?
13 . Bạn có tham_gia vào những cuộc nói_chuyện vui_đùa trong phòng làm_việc nhưng biết giữ một khoảng_cách giới_hạn ?
14 . Bạn có chuẩn_bị cho việc rời bỏ công_việc hiện_tại để tìm_kiếm một công_việc khác tốt hơn khi bạn thấy không hài_lòng với nó ?
15 . Bạn có sẵn_sàng thay_đổi thái_độ đối_với một vấn_đề nào_đó nếu một người khác đưa ra giải_pháp hay hơn bạn ?
16 . Bạn có đủ tham_vọng để mỗi ngày_một cố_gắng hơn , làm_việc cật_lực hơn trong công_việc hiện_tại và không nghĩ đến việc đầu_hàng , buông_xuôi nó để đi du_lịch ?
17 . Bạn có còn suy_nghĩ tích_cực vào những lúc tuyệt_vọng nhất không ?
18 . Bạn có luôn là chính mình ngay cả khi giả_bộ thành một người nào khác sẽ tốt hơn cho bạn trong tình_huống ấy ?
19 . Bạn có khuyến_khích , động_viên và truyền cảm_hứng cho người khác ngay cả khi bạn đang làm_việc rất căng_thẳng ?
20 . Bạn có hành_động nhanh_chóng để thiết_lập một dự_án mới và phản_ứng nhanh hơn nhiều khi mọi việc tiến_triển bất_ngờ ?
21 . Bạn có luôn giữ vững quan_điểm và phong_cách khi mà bạn có hàng đống công_việc và nhận hàng trăm email mỗi ngày ?
22 . Bạn có nhận ra đâu là cơ_hội thực_sự để tập_trung vào đó và biết bạn đang lãng_phí thời_gian vào những việc_gì không ?
23 . Bạn có khích_lệ chính mình để nhảy qua bức tường chắn trước mặt , chấp_nhận một sự mạo_hiểm và chỉ tin vào chính bản_thân ?
Bạn có_thể là một con ong_chúa ?
Với 1 điểm cho 1 câu trả_lời có . Tổng điểm sẽ nói lên quyết_tâm thăng_tiến của bạn .
a . Thấp hơn 7 điểm : Hiện_tại bạn là một nhân_viên thừa_hành cần_mẫn . Nhưng đừng thất_vọng , bạn vẫn có_thể tiến xa trong công_việc . Tìm một công_việc mà bạn yêu_thích , làm_việc chăm_chỉ và tập_trung vào những năng_khiếu của bạn .
b . Từ 7 đến 15 điểm : Bạn là một người lãnh_đạo tiềm_năng và bạn cần phải đánh_thức khả_năng của mình . Bạn là một thành_viên của đội lãnh_đạo , nhưng thiếu sự tự_tin và tham_vọng . Bằng một_chút nỗ_lực và rèn_luyện , một ngày nào_đó bạn có_thể trở_thành một bà chủ lớn nếu bạn muốn .
c . Từ 16-23 điểm : Chúc_mừng bạn ! Bạn đang hoặc sẽ là chủ của công_ty tầm_cỡ . Bạn là một nhà_lãnh_đạo thực_thụ và chúng_tôi muốn trở_thành những người cộng_sự cho bạn . Tuy_nhiên , có_điều cần cảnh_báo với bạn : Đừng phát_xít hóa phong_cách lãnh_đạo của mình , như một Hitler nữ nhé ! Theo Jobvn / Tintuconline

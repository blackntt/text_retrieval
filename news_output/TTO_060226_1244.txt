﻿ Tư_liệu điểm_chuẩn tuyển_sinh ĐH - CĐ 2005
Đây là danh_sách có đầy_đủ các trường ĐH , CĐ , Học_viện , các trường khối công_an , quân_đội ... trong cả nước . Điểm_chuẩn này dành cho học_sinh phổ_thông khu_vực 3 , chưa tính điểm ưu_tiên .
Các_Học viện
1 . Học_viện Công_nghệ Bưu_chính Viễn_thông ( phía Bắc và phía Nam ) 2 . Học_viện Hành_chính quốc_gia ( phía Bắc và phía Nam )
3 . Học_viện Ngân_hàng 4 . Học_viện Quan_hệ quốc_tế
5 . Học_viện Tài_chính 6 . Phân_viện Báo_chí tuyên_truyền
Các trường công_an , quân_đội
7 . Học_viện Cảnh_sát nhân_dân 8 . Học_viện Khoa_học_quân_sự 9 . Học_viện Kỹ_thuật mật_mã
10 . Học_viện Kỹ_thuật quân_sự 11 . Học_viện Quân_y 12 . Học_viện Chính_trị quân_sự
13 . Học_viện Biên_phòng 14 . Học_viện Hậu_cần 15 . Học_viện Hải_quân
16 . Học_viện Phòng_không không_quân 17 . ĐH An_ninh nhân_dân
18 . Trường sĩ_quan Công_binh 19 . Trường sĩ_quan lục_quân 1
20 . Trường_Sĩ quan lục_quân 2 21 . Trường_Sĩ quan pháo_binh 22 . Trường_Sĩ quan đặc_công
23 . Trường_Sĩ quan phòng hóa 24 . Trường_Sĩ quan tăng thiết_giáp 25 . Trường_Sĩ quan thông_tin
Các trường ĐH
26 . ĐH Quốc_gia Hà_Nội ( gồm các trường ĐH Công_nghệ , ĐH Khoa_học_tự_nhiên , ĐH_KHXH& amp ; NV , ĐH Ngoại_ngữ , Khoa_Kinh tế , Khoa_Luật , Khoa_Sư phạm ) .
27 . ĐH Thái_Nguyên ( gồm các trường ĐH Kinh_tế và Quản_trị kinh_doanh , ĐH Kỹ_thuật công_nghiệp , ĐH Nông_lâm , ĐH Sư_phạm , ĐH_Y khoa , Khoa_Công nghệ thông_tin , Khoa_Khoa học tự_nhiên ) .
28 . ĐH Bách_khoa Hà_Nội 29 . ĐH Công_đoàn 30 . ĐH Dược_Hà_Nội
31 . ĐH Điều_dưỡng Nam_Định 32 . ĐH Giao_thông vận_tải 33 . ĐH Hải_Phòng
34 . ĐH Hàng_hải 35 . ĐH Hồng_Đức 36 . ĐH Hùng_Vương ( Phú_Thọ )
37 . ĐH Kinh_tế quốc_dân 38 . ĐH Kiến_trúc Hà_Nội 39 . ĐH Lao_động - Xã_hội
40 . ĐH Lâm_nghiệp 41 . ĐH Luật_Hà_Nội 42 . ĐH Mỏ địa_chất
43 . ĐH Mỹ_thuật_công_nghiệp 44 . ĐH Ngoại_ngữ Hà_Nội 45 . ĐH Ngoại_thương ( phía Bắc và phía Nam )
46 . ĐH Nông_nghiệp 1 47 . ĐH Răng - Hàm - Mặt 48 . ĐH Sư_phạm Hà_Nội
49 . ĐH Sư_phạm Hà_Nội 2 50 . ĐH Sân_khấu Điện_ảnh 51 . ĐH Sư_phạm kỹ_thuật Hưng_Yên
52 . ĐH Tây_Bắc 53 . ĐH Thể_dục thể_thao 1 54 . ĐH Thương_mại
55 . ĐH Thủy_lợi ( phía Bắc và phía Nam ) 56 . ĐH Văn_hóa Hà_Nội 57 . ĐH Vinh
58 . ĐH Xây_dựng Hà_Nội 59 . ĐH_Y Hà_Nội 60 . ĐH_Y Hải_Phòng
61 . ĐH_Y Thái_Bình 62 . ĐH_Y tế công_cộng 63 . ĐH dân_lập Thăng_Long_Hà_Nội
64 . ĐH dân_lập Phương_Đông 65 . ĐH dân_lập Quản_lý và Kinh_doanh Hà_Nội 66 . Viện ĐH Mở_Hà_Nội
67 . ĐH Huế ( gồm các trường ĐH Khoa_học , ĐH Kinh_tế , ĐH Nghệ_thuật , ĐH Ngoại_ngữ , ĐH Nông_lâm , ĐH Sư_phạm , ĐH Y , Khoa_Giáo dục thể_chất )
68 . ĐH Đà_Nẵng ( gồm các trường ĐH Kinh_tế , ĐH Bách_khoa , ĐH Ngoại_ngữ , ĐH Sư_phạm , CĐ Công_nghệ )
69 . ĐH Quy_Nhơn
70 . ĐH Quốc_gia TP.HCM ( gồm các trường ĐH Bách_khoa , ĐH Khoa_học_tự_nhiên , ĐH_KHXH& amp ; NV , ĐH Quốc_tế , Khoa_Kinh tế )
71 . ĐH An_Giang 72 . ĐH Cần_Thơ
73 . ĐH Công_nghiệp TP.HCM 74 . ĐH Đà_Lạt 75 . ĐH Giao_thông vận_tải TP.HCM
76 . ĐH Kiến_trúc TP.HCM 77 . ĐH Kinh_tế TP.HCM 78 . ĐH Luật TP.HCM
79 . ĐH Mỹ_thuật TP.HCM 80 . ĐH Ngân_hàng TP.HCM 81 . Trung_tâm Đào_tạo Bồi_dưỡng Cán_bộ y_tế TP.HCM
82 . ĐH Nông_lâm TP.HCM 83 . ĐH Sư_phạm Đồng_Tháp 84 . ĐH Sư_phạm kỹ_thuật TP.HCM
85 . ĐH Sư_phạm TP.HCM 86 . ĐH Tây_Nguyên 87 . ĐH Thủy_sản
88 . ĐH_Y Dược_Cần_Thơ 89 . ĐH_Y Dược TP.HCM 90 . ĐH Mở bán_công TP.HCM
91 . ĐH bán_công Tôn_Đức_Thắng 92 . ĐH dân_lập Công_nghệ Sài_Gòn 93 . ĐH dân_lập Văn_Lang
94 . ĐH dân_lập Kỹ_thuật công_nghệ 95 . ĐH dân_lập Lạc_Hồng 96 . ĐH dân_lập Ngoại_ngữ - Tin_học TP.HCM
Các trường CĐ
97 . CĐ bán_công Quản_trị Kinh_doanh 98 . CĐ Công_nghiệp Hà_Nội 99 . CĐ Cơ_khí luyện_kim 100 . CĐ Kinh_tế Kỹ_thuật Công_nghiệp 1
101 . CĐ bán_công Công_nghệ & amp ; Quản_trị doanh_nghiệp 102 . CĐ bán_công Hoa_Sen 103 . CĐ Giao_thông vận_tải 3
104 . CĐ Kinh_tế đối_ngoại 105 . CĐ Kinh_tế Kỹ_thuật Công_nghiệp 2 106 . CĐ Kỹ_thuật Cao_Thắng
107 . CĐ Sư_phạm Mẫu_giáo TW3 TP.HCM 108 . CĐ Sân_khấu điện_ảnh TP.HCM 109 . CĐ Tài_chính Kế_toán IV
110 . CĐ Sư_phạm TP.HCM 111 . CĐ Văn_hóa TP.HCM 112 . CĐ dân_lập Công_nghệ_thông_tin TP.HCM
QUỐC_DŨNG

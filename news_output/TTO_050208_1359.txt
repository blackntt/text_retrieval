﻿ Trung_Quốc chế_tạo thành_công vaccin cúm_gà
Nếu được tiêm loại vaccine này thì gà có_thể kháng bệnh trong vòng 10 tháng ( lâu hơn loại thuốc hiện_nay 4 tháng ) . Tuy_nhiên đại_diện của WHO tại Manila cho_biết loại vaccine mới này vẫn chưa được công_nhận hoàn_toàn vì trong một_số trường_hợp vaccine lại có tác_dụng che_giấu các triệu_chứng bệnh ở gia_cầm đã nhiễm_bệnh .
Nhật_báo Trung_Hoa đưa tin nước này đang có dự_định sẽ tiêm phòng cho tất_cả các loài chim nước nhưng không nói rõ quy_mô của dự_án này . Bộ_trưởng y_tế Trung_Quốc cho_biết công_tác này rất quan_trọng vì nó sẽ cắt đứt được chuỗi truyền bệnh .
Theo ước_tính của WHO nếu virus H5N1 biến_dạng và có_thể lây từ người sang người thì số người chết có_thể lên đến 100 triệu người .
KINH_LUÂN ( Theo BBC_NEWS )

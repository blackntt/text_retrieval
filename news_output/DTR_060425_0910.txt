﻿ 10 cách giảm nguy_cơ ung_thư vú
Ung_thư vú là nỗi lo của nhiều phụ_nữ , song thực_ra bí_quyết phòng_tránh ung_thư lại rất đơn_giản bởi nó nằm trong chính thói_quen sinh_hoạt hàng ngày của mỗi người .
1 ) Ăn_uống hợp_lý
Ăn nhiều rau_quả vì trong rau_quả có chất chống oxi hóa giúp bảo_vệ các cơ_quan khỏi bị phá_hủy . Hãy để_ý lựa_chọn các màu xanh , đỏ , vàng , cam khi mua rau và đỏ , xanh , hồng tía khi mua hoa_quả .
2 ) Tập_thể_dục để phòng_ngừa
Tập_thể_dục có mối liên_quan chặt_chẽ với phòng_ngừa ung_thư và ngăn ung_thư tái_phát . Tập_luyện đều_đặn giúp cơ_thể giảm lượng chất_béo và cơ ngực thêm chắc khỏe .
3 ) Giữ thân_hình mảnh_mai
Thừa cân đồng_nghĩa với tăng nguy_cơ ung_thư vú . U bướu ung_thư có chứa nhiều estrogen . Chất_béo trong cơ_thể lưu_trữ estrogen rất tốt do_đó nếu bạn thu_nạp nhiều chất_béo , lượng estrogen sẽ vượt quá mức bình_thường . Hãy áp_dụng chế_độ ăn ít béo hoặc ăn nhiều rau để giảm lượng estrogen .
4 ) Ngừng hút thuốc
Trong thuốc_lá chứa thành_phần arcinogens có khả_năng tích_tụ thành dạng lỏng quanh vú . Hút thuốc_lá kể_cả chủ_động hay thụ_động đều tăng đáng_kể nguy_cơ ung_thư vú và phổi vì_thế nên từ_bỏ thuốc_lá vì lợi_ích sức_khỏe lâu_dài .
5 ) Không uống rượu thường_xuyên
Uống rượu thường_xuyên cho_dù một lượng nhỏ cũng có_thể tăng mức estrogen . Các u bướu ung_thư vú cực_kỳ nhạy_cảm với estrogen do_đó rượu cũng chính là tác_nhân gây nguy_cơ ung_thư cho các tế_bào .
6 ) Kiểm_tra sức_khỏe định_kỳ
Bạn nên kiểm_tra sức_khỏe định_kỳ và yêu_cầu có bản copy các kết_quả chụp chiếu . Hãy theo_dõi để nhận ra bất_kỳ thay_đổi nào có hại cho cơ_thể một_cách kịp_thời .
7 ) Phát_hiện bệnh sớm
Chụp chiếu ung_thư giúp tìm ra các u bướu trước khi chúng phát_triển đến mức có_thể nắn được và lan_rộng ra ngoài vùng vú . Phát_hiện bệnh ngay từ giai_đoạn đầu khiến việc điều_trị hiệu_quả hơn và tăng khả_năng sống_sót . Do_đó từ tuổi 40 bạn nên chụp kiểm_tra u ngực mỗi năm một lần .
8 ) Cẩn_thận với liệu_pháp thay_thế hormone
Những người điều_trị bằng hormone dù tạm_thời hay lâu_dài cũng đều có nguy_cơ phát_triển ung_thư vú vì_thế phải hết_sức cẩn_thận khi quyết_định lựa_chọn liệu_pháp chữa bệnh này . Hãy nói_chuyện với bác_sĩ để biết điều_trị bằng hormone có đem lại lợi_ích cho bạn không và có liệu_pháp nào thay_thế không .
9 ) Mang bầu và cho con bú
Mang bầu , cho con bú kết_hợp luyện_tập thể_dục , duy_trì cân nặng hợp_lý , tránh rượu thuốc_lá đều có_thể giảm nguy_cơ ung_thư . Lý_do là khi mang thai và cho con bú đã làm giảm tổng_số chu_kỳ kinh_nguyệt . Có con trước tuổi 30 cũng giảm được nguy_cơ mắc bệnh .
10 ) Tâm_trạng tốt
Tâm_trạng có ảnh_hưởng rất lớn đến sức_khỏe toàn_diện của bạn : thể_lực , trí_tuệ , cảm_xúc và tinh_thần . Hãy tạo_dựng cho mình một tương_lai tốt_đẹp bằng cách cân_bằng cuộc_sống , ăn_uống lành_mạnh , tập_thể_dục đều và quên hết mọi ưu_phiền để tâm_hồn luôn thư_thái .
Huyền_Trang_Theo_About

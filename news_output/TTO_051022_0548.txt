﻿ Bệnh_viện Nhi_đồng Cần_Thơ : Đã có tiền chống dột , ngập
Ông Dương_Bá_Diện , phó_chủ_tịch thường_trực HĐND_TP , nói : “ Tình_trạng bệnh_viện xuống_cấp và thiếu trang_thiết_bị như báo phản_ánh là hoàn_toàn chính_xác ! Trước đó chúng_tôi đã nghe nhưng thực_tế khó_khăn đến mức thiếu cả máy hỗ_trợ hô_hấp mà mỗi máy chỉ khoảng 50 triệu đồng và kéo_dài trong nhiều năm thì_phải tính_toán ! ” .
TP đã có chủ_trương qui_hoạch xây_dựng mới Bệnh_viện Nhi_Đồng 350 giường tại khu_vực quốc_lộ 91 nối dài ( quận Ninh_Kiều ) . Trước_mắt HĐND đã làm_việc với Sở Kế hoạch - đầu_tư , Sở Tài chính sớm hỗ_trợ nguồn vốn 713 triệu đồng mà bệnh_viện đã đề_nghị để sửa_chữa chống dột ngập , khắc_phục tình_trạng nhếch_nhác , thiếu_thốn như hiện_nay .
TRẦN_ĐỨC

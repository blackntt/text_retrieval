﻿ Giảm cân_bằng ... nước ép trái_cây
Jamie không phải là người mê tập_thể_dục , nên cô đã đến cầu_cứu bác_sĩ liệu_pháp tự_nhiên Larry_Rawdon . Thay_vì kê toa là tập aecrobics và nhịn ăn_bớt , ông đã cho cô liều thuốc từ ... nước ép trái_cây . Với liệu_pháp này , điều “ khó_khăn ” nhất_là cô phải siêng_năng ... cọ rửa máy ép trái_cây và xay sinh_tố , nhưng nó đã giúp cô giải_phóng đi được gần 20kg thể_trọng thừa_thãi !
Nguyên_tắc dùng nước ép trái_cây giảm béo
Một điều cần làm rõ ở đây : đừng ăn_kiêng cực_đoan theo kiểu chỉ dùng toàn nước ép trái_cây . Hãy bắt_đầu_từ những loại rau trái lành_mạnh như : cà_rốt , táo , củ_cải ... Bạn sẽ bất_ngờ cảm_thấy mình muốn ăn những khối_lượng thực_phẩm nhỏ hơn và bớt thích ăn quà vặt .
Dù một_số người cãi lý rằng nước ép trái_cây quá giàu năng_lượng và quá ít chất xơ đối_với người ăn_kiêng , nhưng bác_sĩ Larry cho rằng điều này chỉ đúng khi bạn uống nước ép trái_cây đóng_hộp . Thật_ra , nước ép trái_cây tươi giúp bạn “ nghiện ” những loại thực_phẩm lành_mạnh , giúp giảm cân cho cả một cuộc_đời .
Nước ép trái_cây có_thể giúp_ích gì cho bạn ?
- Giúp bạn giải_phóng bớt mỡ thừa
Nghiên_cứu cho thấy : càng ăn nhiều rau trái , cơ_thể càng ít mang vác chất_béo dư_thừa . Đa_số mọi người trong chúng_ta ăn ít hơn 3 khẩu_phần rau / ngày . Chuyên_gia dinh_dưỡng Maureen_Keane , tác_giả cuốn sách “ Nước ép trái_cây vì sức_khỏe ” , cho_biết : “ Một_số người ăn_kiêng ghét rau , nhưng thường lại hay thích nước ép rau trái . Và nếu_như bạn thay_thế thực_phẩm chế_biến sẵn bằng nước ép , bạn sẽ hồi_sinh lại những gai vị_giác đã bị “ đầu_độc ” bởi muối , chất phụ_gia thực_phẩm . Hương_vị của thực_phẩm tự_nhiên được thăng_hoa , và bạn bắt_đầu mong_muốn nhiều hơn_nữa những thực_phẩm lành_mạnh sau đây : nước ép rau trái , trái_cây nguyên , xúp , xà_lách rau trộn . Một_khi điều đó xảy_ra , giảm cân là điều chắc_chắn .
- Cung_cấp những dưỡng_chất chống lại cơn đói bụng
Tiến_sĩ Fuhrman , tác_giả cuốn sách “ Ăn để sống ” , cho_biết : " Đa_số chúng_ta ăn vội_vàng , nhai không kỹ nên 80% dưỡng_chất tiêu_thụ không được tiêu_hóa hết . Thiếu dinh_dưỡng là một trong những nguyên_nhân khiến chúng_ta thèm ăn . Nước ép rau trái có_thể cứu bạn . Bằng cách xuyên qua thành tế_bào , nó khiến cho nhiều vitamine , khoáng_chất , siêu chất chống oxy_hóa được hấp_thu ngay_lập_tức . Bộ_não đánh_hơi được dòng cung_cấp dưỡng_chất này , và phát thông_điệp ngưng ngay công_cuộc lùng_sục thức_ăn của chúng_ta ” .
- Là loại nguyên_liệu giàu phẩm_chất và hiệu_năng
Do không có chất xơ , nên nước ép trái_cây giúp cho đường thực_vật hấp_thu vào ruột_non ngay_lập_tức , làm tăng lượng năng_lượng nhanh_chóng . Hấp_thu nước ép trái_cây còn giúp xây_dựng lượng chất chống oxy_hoá trong mô , thúc_đẩy việc tôn_tạo nguồn năng_lượng , giúp tiêu_hao nguồn calories dư_thừa .
- Có nên ăn_kiêng bằng nước ép rau trái đóng_hộp ?
Nước ép trái_cây đóng_hộp được tiệt_trùng để ngăn_ngừa hư_hỏng , khiến hủy_hoại nhiều dưỡng_chất trong quá_trình chế_biến . Các nhà_sản_xuất đã thêm vào vitamin và khoáng_chất , nhưng nhiều vi_chất không_thể thay_thế được .
- Bạn nên xay ép loại rau trái nào ?
Càng hấp_thu phong_phú các loại rau trái , bạn càng đạt nhiều lợi_ích cho sức_khỏe . Vì_vậy , hãy kích_hoạt tính sáng_tạo của bộ_não bằng cách phối_hợp nhiều loại rau trái : cà_rốt , nho , táo , lê , cà_chua , bông cải , ớt_ngọt , rau_thơm ...
- Trữ nước ép rau trái trong tủ_lạnh được bao_lâu ?
Cho nước ép rau trái vào hộp đậy kín nắp , nó sẽ lưu_giữ được hương_vị suốt 24 giờ . Nhưng tốt nhất_là uống ngay sau khi xay ép , dưỡng_chất còn giữ được nguyên_vẹn .
- Uống nước ép trái_cây vào thời_điểm nào để giảm cân ?
Giữa các bữa ăn chính , bạn hãy uống nước ép vào bất_cứ lúc_nào thấy đói , bạn sẽ hài_lòng và không cần ăn vặt thêm . Nước ép được dùng trước bữa ăn chính sẽ cắt bớt 200g calories từ tổng năng_lượng hấp_thu .
Theo Phụ_Nữ TP.HCM

﻿ Mẹ càng nhiều tuổi càng dễ sinh_đôi
Bạn có biết những cặp sinh_đôi không cùng trứng chiếm 1/3 số ca mang thai song_sinh và chủ_yếu ở những bà_mẹ ngoài 30 tuổi ? Theo giải_thích của các nhà_khoa_học Hà_Lan , đó là do buồng_trứng của những phụ_nữ lớn_tuổi dễ sản_xuất đa trứng hơn những phụ_nữ trẻ .
Các nhà_nghiên_cứu thuộc ĐH Vrije ( Amsterdam ) đã quan_sát sự hình_thành và phát_triển của nang trứng ở 500 phụ_nữ .
Theo quy_luật tự_nhiên , hormone khuyến_khích nang trứng phát_triển FSH sẽ là khởi_nguồn cho sự phát_triển của nhiều trứng cùng 1 lúc . Khi trứng trưởng_thành , hormone FSH sẽ giảm xuống . Đáng chú_ý là lượng hormone FSH gia_tăng cùng_với tuổi_tác , cả ở những phụ_nữ chỉ có 1 hay đa nang buồng_trứng .
Các nhà_khoa_học Hà_Lan đã phân_tích các nang trứng phát_triển trong 959 chu_kỳ của người phụ_nữ và tất_cả đều trải qua quá_trình thụ_tinh trong tử_cung . Các nhà_nghiên_cứu đã quan_sát thất những buồng_trứng có nhiều noãn phát_triển cùng 1 lúc , tức_là có hơn 1 trứng rụng và được thụ_tinh , đã diễn ra ở 105 phụ_nữ với tỉ_lệ : 5 người_ở độ tuổi 30 , 45 người_ở độ tuổi 30-35 và 55 người_ở độ tuổi trên 35 .
Điều đáng ngạc_nhiên là ở những phụ_nữ bước vào độ tuổi mãn_kinh , 38 – 48 tuổi , buồng_trứng vẫn sản_xuất rất nhiều hormone FSH và cũng tạo ra nhiều trứng rụng cùng một lúc hơn phụ_nữ ở các lứa tuổi khác . Tuy_nhiên , trong hầu_hết các trường_hợp ở lứa tuổi này , hiện_tượng thụ_tinh sẽ rất khó_khăn và hầu_như không_thể dẫn tới hiện_tượng đa thai do chất_lượng noãn_bào của trứng thấp .
Đã có vac - xin phòng viêm tai
Các nhà_khoa_học CH Séc vừa phát_triển thành_công một loại vac - xin phòng viêm tai , một loại bệnh không quá nguy_hiểm nhưng rất phổ_biến ở trẻ_em .
Loại vac - xin này có khả_năng ứng_phó với 2 loại vi_khuẩn là streptococcus gây viêm phổi và haemophilus gây cúm .
Các chuyên_gia về tai và thính_lực Anh Quốc cho_biết lợi_ích lớn nhất của loại vac - xin mới này là ngăn_ngừa được chứng viêm tai giữa .
Thành_phần chính của vac - xin là protein của cả khuẩn streptococcus và haemophilus .
Khoảng 5.000 trẻ từ 3 – 15 tháng đã được tiêm thử loại vac - xin này .
Bệnh tiểu_đường gia_tăng
Nước_ngọt , thủ_phạm gia_tăng số người bị
bệnh tiểu_đường .
“ Hầu_hết số nạn_nhân mới của bệnh tiểu_đường là những người béo phì , những người có chỉ_số cơ_thể là 30 hoặc cao hơn ” , trưởng nhóm nghiên_cứu , bác_sĩ Caroline S . Fox , thuộc Viện_Huyết học – Tim_mạch và phổi quốc_gia ( bang Massachusetts , Hoa_Kỳ ) thông_báo .
Kết_quả này dựa trên cuộc nghiên_cứu với 3.104 người_ở độ tuổi trung_bình 47 tham_gia , kéo_dài từ thập_niên 70 đến nay .
Kết_quả cho thấy ở thập_niên 70 , sự ảnh_hưởng của căn_bệnh tiểu_đường đối_với cộng_đồng ở mức thấp nhất , khoảng 2% số phụ_nữ và 2,7% số nam_giới bị mắc bệnh này . Đến thập_niên 90 , tỉ_lệ này đã leo lên mức khá cao : 3,7% ở nữ và 5,8% ở nam .
Như_vậy , so với những năm 70 , nguy_cơ bị bệnh tiểu_đường trong thập_niên 80 , 90 đã tăng lên 40% và 105% .
Một lưu_ý rất quan_trọng là số người bị tiểu_đường tỉ_lệ_thuận với số người béo phì nhưng cân nặng không phải là nguyên_nhân duy_nhất dẫn tới những con_số kinh_hoàng kể trên . Những thay_đổi trong sinh_hoạt và chế_độ ăn cũng là một nguyên_nhân được tính tới . Ngoài_ra , một nghiên_cứu độc_lập trước đó cho thấy những thức uống chứa đường cũng là 1 trong những thủ_phạm gây ra căn_bệnh tiểu_đường .
Phương_Uyên_Theo BBC , Reuters

﻿ Lăng_kính màu hồng hay lời biện_bạch cuối năm ?
BĐ_VN đã có một năm đại thành_công và đáng tuyên_dương , đó là ấn_tượng mà người_ta nhận được nếu đọc “ Những sự_kiện nổi_bật của BĐ_VN 2005 ” được đăng trên website của LĐBĐVN ( VFF ) . Phải_chăng căn_bệnh thành_tích vẫn chưa chịu buông VFF ?
C hiều ngày 31/12/2005 , website chính_thức của LĐBĐVN đã bình_chọn một_số sự_kiện nổi_bật của bóng_đá nước_nhà trong năm 2005 .
Điều này sẽ không có gì lạ_lẫm nếu_như nhiều trong số những sự_kiện được cho là nổi_bật này không được tô_hồng và vô_tình bị biến thành một bản báo_cáo thành_tích cuối năm của VFF . Không hiểu VFF đang bình_chọn dựa trên tiêu_chí nào ?
Và những sự_vụ rối_ren , hàng_loạt bê_bối vốn đã làm lu_mờ hình_ảnh bóng_đá VN nói_chung và uy_tín của VFF nói_riêng được giấu đi đâu ?
Sự_kiện đầu_tiên được bình_chọn chính là Đại_hội LĐBĐ_VN khóa V , với sự có_mặt của các đại_biểu đến từ khắp các địa_phương , các CLB đủ các Hạng cao_thấp , và một phần không nhỏ đóng_góp cho sự thành_công của ĐH còn có đại_diện của ban Tư_tưởng văn_hoá TW , bộ Công_an , UBTDTT , Bộ giáo_dục đào_tạo , TW đoàn TNCS Hồ_Chí_Minh , đại_diện các doanh_nghiệp v . v …
Với tinh_thần “ Đổi_mới - Dân_chủ - Trí_tuệ - Trách_nhiệm ” , ĐH đã quyết_tâm phấn_đấu cho mục_tiêu : “ Tiếp_tục đẩy_mạnh quá_trình xã_hội_hoá , chuyên_nghiệp hoá bóng_đá Việt_Nam , nâng cao trình_độ bóng_đá nước_nhà , đáp_ứng sự mong_đợi của toàn xã_hội ” .
ĐH cũng đã bầu ra được các chức_danh chủ_chốt cho LĐ nhiệm_kỳ V : Đó là Chủ_tịch Nguyễn_Trọng_Hỷ , Tổng_thư_ký Trần_Quốc_Tuấn , các Phó_chủ_tịch Lê_Thế_Thọ , Vũ_Quang_Vinh , Lê_Hùng_Dũng .
Một ĐH quy_mô hoành_tráng là vậy , với những khẩu_hiệu to_tát là vậy , nhưng những gì mà VFF làm được để cụ_thể_hoá các mục_tiêu , phương_hướng này là gì ?
Đổi_mới ? Xin thưa người hâm_mộ chưa cảm_nhận thấy sức nặng của hai từ này trong năm vừa_qua .
Dân_chủ ? Trên lý_thuyết đúng là như_vậy , nhưng trên thực_tế , VFF năm qua được ví_như một chiến_trường , nơi mà vị này nhân cơ_hội buộc_tội vị nọ , xỉa_xói vị kia .
Trí_tuệ ? Cách quản_lý và giáo_dục không hiểu trí_tuệ đến mức nào mà để cuối năm hết lượt trọng_tài , HLV , GĐĐH_CLB và hàng_loạt cầu_thủ kéo nhau vào nhà_đá vì những việc_làm không minh_bạch .
Trách_nhiệm ? Nếu thế_thì ông Thọ đã không bỏ ghế mà đi khi SEA Games 23 kết_thúc , ông Dũng cũng đã không phải thanh_minh mỏi mồm về chuyện tiền_nong khen_thưởng , và cũng chưa chắc vụ bán_độ lại lên đến mức trầm_trọng như hiện_nay .
Nếu nói rằng bóng_đá VN được “ chuyên_nghiệp hoá ” e rằng cũng hơi quá lạc_quan . Giải chuyên_nghiệp đã có 5 năm_tuổi , ấy vậy_mà vẫn chưa thấy những thay_đổi chuyên_nghiệp trong cơ_chế bóng_đá nước_nhà .
Còn “ xã_hội_hoá ” , công_tác đào_tạo cầu_thủ trẻ và sự quan_tâm đến BĐ phong_trào đã được quan_tâm đến mức nào ? Thật tiếc là chưa thấy “ xã_hội_hoá ” mà chỉ thấy nền bóng_đá nước_nhà đang bị “ xã_hội_đen ” hoá mà thôi .
Nói chừng đó thôi_thì cũng đã quá đủ để thấy năm 2005 , ĐH này đã đưa bóng_đá VN “ đáp_ứng sự mong_đợi của toàn xã_hội ” đến đâu rồi .
HCB trong tiêu_cực của U23 VN vẫn làm VFF " hả_hê " ?
Một sự_kiện nữa được cho là nổi_bật đó là việc U23 VN giành HCB tại SEA Games 23 . Để minh_chứng cho sự chuẩn_bị rất tốt của đội bóng , hàng_loạt “ kỳ_tích ” đã được dẫn ra .
Đầu_tiên là chiếc Cup vô_địch LG Cup sau những chiến_thắng được cho là “ hoàn_hảo ” trước U23 Bulgaria hay SV Hàn_Quốc và trận hoà không bàn thắng với U21 Syria .
Kế sau đó là một giải đấu được xem là “ làm ngây_ngất khán_giả nhà ” tại Agribank_Cup với chiến_thắng 2-1 trước U23 Thái_Lan , đội bóng mà sau đó đã đánh_bại U23 VN tại SEA Games 23 với 3 bàn không gỡ , lúc đó , người_ta gọi U23 Thái_Lan là đối_thủ “ vượt_trội ” .
Nực_cười hơn , VFF còn kể_ra đây hai trận thắng “ xuất_sắc ” của U23 VN trước đối_thủ Myanmar ( chỉ cần thắng 1 bàn ? ) và Malaysia ở bán_kết ( trận đấu mà bàn thắng của Công_Vinh đã làm “ hư bột , hỏng đường ” toan_tính bẩn_thỉu của bao_nhiêu con_người ? ) .
Chiếc HCB này là một sự_kiện trong năm của bóng_đá nước_nhà ? Thế_thì muôn_vàn cái tai_hoạ và bê_bối đằng sau cái ánh bạc lấp_lánh đó để đi đâu ?
Cũng may là chúng_ta thất_bại trong trận chung_kết , chứ nếu giành được chiếc HCV thì chắc giờ này những Quyến , Vượng , Quốc_Anh v . v … đang ngồi trên những núi tiền và hả_hê ngồi nghe những lời tán_dương đường_mật , và nhiều vị quan_chức chắc cũng không tránh khỏi những lời tuyên_dương vì hoàn_thành xuất_sắc nhiệm_vụ với bóng_đá nước_nhà .
Bài bình_chọn cũng đồng_thời cho rằng , các giải đấu năm 2005 đã “ kết_thúc tốt_đẹp ” , và riêng giải Hạng nhất còn được coi là đã để lại “ những ấn_tượng không dễ phai_mờ ” ( ! ? ) .
Để chứng_minh cho ý_kiến này , cú đúp của GĐT . LA và cuộc đua tới chức vô_địch của K.Khánh Hoà và Tiền_Giang đã được đưa ra .
Nhưng nên nhớ rằng đó là những thành_công của riêng các đội bóng , còn các giải đấu này liệu có_thể được coi là thành_công khi mà nó kết_thúc với muôn_vàn vụ sự có_thể được coi là vô_tiền_khoáng_hậu .
Một ông trọng_tài vừa chỉ tay vào chấm_phạt_đền đã ngay_lập_tức quay lại phạt cầu_thủ bị đốn ngã , một loạt trọng_tài dắt nhau vào nhà_đá , một HLV trưởng cùng một GĐ_ĐH bị khởi_tố vì tội đưa hối_lộ , hai CLB bị đánh sập hạng , các nhà tài_trợ bỏ chạy không quay mặt lại , kéo_theo mùa giải mới hoãn lui hoãn lại . Mùa bóng 2005 đã kết_thúc , nhưng thực_sự đã “ tốt_đẹp ” hay chưa ?
GĐT Long_An , điểm sáng hiếm_hoi năm vừa_qua .
Trong bài viết này , VFF cũng cho rằng “ Năm 2005 cũng là năm ghi dấu sự trưởng_thành đáng khen_ngợi của bóng_đá trẻ Việt_Nam ” .
Với dẫn_chứng là hai đội_tuyển U17 và U20 lọt vào VCK các giải U_Châu Á 2006 ( đặc_biệt là đội U20 sau trận hoà với U20 Lào , đội bóng được VFF đánh_giá là ứng_cử_viên hàng_đầu tại các giải trẻ khu_vực ) , VFF tự_tin rằng bóng_đá VN sẽ có những lứa cầu_thủ trẻ tài_năng “ tiến bước cùng sự phát_triển của bóng_đá Việt_Nam trên đấu_trường khu_vực , châu_lục và thế_giới ” .
Không_thể phủ_nhận những thành_tích bước_đầu đó , nhưng cần nhìn lại xem , chỉ mới cách đay vài năm thôi , U16 VN còn làm được nhiều hơn thế , nhưng cho_đến bây_giờ , lứa cầu_thủ đã từng được coi là “ thế_hệ vàng thứ 2 ” đã làm được những gì để cải_thiện vị_trí của bóng_đá VN ?
Và nếu nói rằng bóng_đá trẻ đã có một năm thành_công thì hãy nhìn lại xem được bao_nhiêu gương_mặt trẻ triển_vọng để bổ_sung vào các đội_tuyển , bao_nhiêu cầu_thủ tuổi U dành được một suất chính_thức tại các CLB ?
Có_lẽ trước khi nhắc đến việc đào_tạo cầu_thủ trẻ năm 2005 như một thành_công , nên_chăng hãy nghĩ_lại xem vì_sao mà cầu_thủ trẻ VN lại khó tìm đến thế .
Bài bình_chọn khép lại với sự_kiện “ Chiến_dịch triệt_tiêu tiêu_cực ” , bài viết cho rằng VFF đang tiếp_tục cố_gắng và chủ_động “ làm triệt_để vấn_đề , góp_phần tiến tới lành_mạnh hóa nền bóng_đá nước_nhà cũng_như thúc_đẩy sự phát_triển bóng_đá của chúng_ta . ”
Câu nói trên phải_chăng chúng_ta đã nghe rất nhiều lần , trong rất nhiều kỳ ĐH và trong rất nhiều cuộc họp của VFF trong nhiều năm qua ?
Không_thể phủ_nhận những nỗ_lực của VFF trong thời_gian vừa_qua , đặc_biệt là những việc_làm cụ_thể của CT Nguyễn_Trọng_Hỷ , nhưng liệu_chừng đó là đã đủ để nói rằng VFF đang thực_hiện một chiến_dịch triệt_tiêu tiêu_cực ?
Liệu điều đó có là sự_thật khi mà những hoài_nghi âm ỷ trong lòng người hâm_mộ về những lấn_cấn tiền_nong , những bất_đồng nội_bộ mà biểu_hiện rõ nhất_là chuyện ông Thọ vừa_qua , không ít người nhân cơ_cấu xé , nhưng khi cần có người đứng ra quyết_định thì LĐ lại nhanh_nhẩu “ chuyền bóng ” sang chân UBTDTT .
Liệu cuộc_chiến với tiêu_cực này của VFF sẽ hiệu_quả đến đâu nếu vẫn còn những con_người mang nặng chủ_nghĩa_cơ_hội đứng trong hàng_ngũ của mình ?
Dù có muốn thừa_nhận hay không , năm 2005 vẫn là một miền tối của bóng_đá VN . Thay_vì cố lãng tránh sự_thật đau_lòng đó , hãy đối_mặt và vượt qua nó để xây_dựng lại một nền bóng_đá trong_sạch , khoẻ_mạnh hơn .
Xin chúc những điều tốt_đẹp nhất cho bóng_đá VN trong năm mới 2006 ! Hồng_Kỹ

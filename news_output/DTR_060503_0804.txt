﻿ 145 học_sinh trường nghề nhận giải_thưởng Trần_Văn Ơn
Sáng nay 3/4 , Trung_ương Đoàn đã tổ_chức họp_báo thông_báo về trao giải_thưởng Trần_Văn Ơn cho 145 học_sinh xuất_sắc và cán_bộ Đoàn , Hội tiêu_biểu ở các trường chuyên_nghiệp và dạy nghề trên cả nước .
Những học_sinh được trao giải_thưởng Trần_Văn Ơn là những học_sinh tiên_tiến của năm_học trước hoặc đoạt giải cao trong các kỳ thi môn_học , kỳ thi tay_nghề , nghiệp_vụ từ cấp trường trở_lên ; có đóng_góp xuất_sắc trong công_tác nghiên_cứu , sáng_tạo , công_tác Đoàn .
Giải_thưởng ưu_tiên cho học_sinh người dân_tộc_thiểu_số , học_sinh có thành_tích đặc_biệt xuất_sắc như dũng_cảm lập_công bảo_vệ an_ninh trật_tự , cứu người , cứu tài_sản .
Năm_học 2005 - 2006 , Trung_ương Đoàn sẽ trao Giải_thưởng Trần_Văn Ơn cho 145 em học_sinh được chọn từ các trường THCN và dạy nghề trên cả nước . Giải_thưởng gồm bằng_khen của Trung_ương Đoàn , bằng chứng_nhận thành_tích kèm theo 500.000đ/học sinh .
Giải_thưởng do Công_ty Bảo_hiểm_nhân_thọ Prudential_Việt_Nam tài_trợ với tổng_số tiền là 130 triệu . Lễ trao giải sẽ được tổ_chức tại TP . Nam_Định vào ngày 7/5 tới . Mai_Minh - Hồng_Hạnh

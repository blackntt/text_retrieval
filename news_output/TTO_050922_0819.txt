﻿ Thủ_tướng cho thôi chức Chủ_tịch Hội_đồng_quản_trị VNPT
Liên_quan đến ông Luân , trước đó , cơ_quan cảnh_sát điều_tra Bộ Công an đã ra quyết_định triệu_tập , ghi lời khai với ông này vì liên_quan đến vụ_án nâng khống giá thiết_bị bưu_điện .
Với cương_vị Chủ_tịch Hội_đồng_quản_trị VNPT , ông Vũ_Văn_Luân phải trả_lời về trách_nhiệm trong vụ tiêu_cực nâng khống giá in niên_giám điện_thoại ở Bưu_điện Nghệ_An và Hà_Tây ; cũng_như vụ_án Nguyễn_Lâm_Thái được phá gần đây .
Cũng trong hôm_nay , Thủ_tướng ký quyết_định bổ_nhiệm 18 chức_danh mới . Ông Đoàn_Việt_Trung , Trưởng_ban Ban Kỹ thuật phát_thanh - Đài_Tiếng nói VN , giữ chức Phó_tổng giám_đốc Đài_Tiếng nói VN . Ông Hà_Minh_Huệ , Trưởng_ban Thư_ký biên_tập , Thông_tấn_xã VN , giữ chức Phó_tổng giám_đốc Thông_tấn_xã VN . Ông Trần_Mai_Hưởng , Tổng_biên_tập báo VietNam News ( Thông_tấn_xã VN ) , giữ chức Phó_tổng giám_đốc Thông_tấn_xã VN .
Ông Huỳnh_Vĩnh_Ái , Tỉnh_uỷ_viên - Bí_thư Huyện_uỷ Tân_Hiệp , tỉnh Kiên_Giang , giữ chức Phó chủ_nhiệm Uỷ_ban Thể_dục Thể_thao . Ông Nguyễn_Ngọc_Thuật giữ chức Thứ_trưởng Bộ Nông nghiệp và phát_triển nông_thôn .
Ông Vũ_Văn_Luân sinh ngày 9-11-1945 tại Hà_Nội ; tốt_nghiệp Đại_học Thông_tin liên_lạc , chuyên_ngành Vô_tuyến_điện .
Tháng 11-2002 , ông Luân được bổ_nhiệm làm Chủ_tịch Hội_đồng_quản_trị VNPT . Trước đó , ông Luân là phó_tổng giám_đốc thường_trực VNPT .
Theo Vn Express

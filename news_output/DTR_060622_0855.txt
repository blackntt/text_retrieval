﻿ “ Chỉnh ” em chồng
Những tai_nạn nhỏ trong giao_tế với em chồng là vấn_đề thường gặp của các chị dâu trẻ . Tuy_nhiên , nếu rộng_lượng và “ cao_tay ” , bạn không_những có_thể xử_lý được mà_còn thắt chặt thêm tình_cảm gia_đình .
Thúy , 26 tuổi , nhân_viên hành_chính , rất khổ_sở vì sở_thích được ướm thử , dùng thử và “ cầm nhầm ” của cô em chồng tên Hà , 20 tuổi , làm thợ_may tại nhà .
Không_những thích dùng mỹ_phẩm và quần_áo của chị dâu , Hà còn hay mượn vật_dụng của Thúy nhưng thường_xuyên “ quên trả ” .
Có lần , Hà tỉnh_bơ lấy chiếc áo mới mua chưa kịp mặc trị_giá hơn 500.000 đồng của Thúy , khoác lên con ma - nơ-canh trong hiệu may để làm mẫu cho khách .
Ngay cả Tuấn , cậu em chồng , cũng đem lại rắc_rối không nhỏ . Tuy vai em , Tuấn vẫn lớn hơn Thúy 3 tuổi , tính_tình lại khá bỗ_bã . Mỗi khi vắng_mặt anh_trai , Tuấn hay vỗ vai , vuốt tóc chị dâu .
Có_khi cậu ta còn gọi Thúy bằng “ em ” ngọt_xớt khiến cô rất ngượng nhưng chẳng biết phản_ứng sao cho phải . Nói ra với chồng , Thúy sợ làm sứt_mẻ tình anh_em . Còn không nói thì cứ ấm_ức trong lòng .
Suy_nghĩ mãi , Thúy quyết_định làm chìa_khóa phòng và tủ riêng cho mình . Mỗi khi ra khỏi phòng , cô khóa lại ngay .
Để_Hà không suy_nghĩ nhiều về việc cô thường_xuyên khóa cửa , Thúy tỏ ra thân_mật với Hà hơn , cô thường mua tặng Hà nhiều món vật_dụng giống_hệt thứ cô vẫn dùng .
Một lần , khi Tuấn vừa định vuốt tóc Thúy , cô bảo : “ Tuấn đừng làm_vậy , kẻo người_ngoài nhìn thấy lại đánh_giá sai về chị_em mình ” .
Câu chỉnh nhẹ_nhàng nhưng có ý_tứ của Thúy khiến Tuấn giật_mình . Để_Tuấn đỡ áy_náy , Thúy cười : “ Chị không chấp đâu , cậu yên_tâm ” .
Như_vậy , không cần đao_to_búa_lớn , tư_cách và bản_lĩnh của một người chị dâu đã được khẳng_định trước sự kính_nể của các em chồng . Theo Tiếp_Thị_Gia_Đình

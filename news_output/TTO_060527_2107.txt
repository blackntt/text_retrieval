﻿ Đông_Nam Á dễ bị tổn_thương trước nạn tin_tặc
Trung_tâm chống khủng_bố khu_vực Đông_Nam Á khẳng_định nguy_cơ này đang ngày_càng tăng , vì các nước_đang_phát_triển trong khu_vực chưa xây_dựng được hệ_thống bảo_vệ và kiểm_soát hiệu_quả cho hệ_thống máy_tính của các cơ_quan nhà_nước , hệ_thống tài_chính , ngân_hàng và các máy_tính_cá_nhân .
Các phần_tử khủng_bố và bọn tin_tặc có_thể phát_động các cuộc tấn_công vào hệ_thống mạng công_cộng của các cơ_quan nhà_nước , làm tê_liệt hoạt_động của các cơ_quan này , hoặc phát_tán các virus máy_tính để giúp chúng thu_thập và phá_hoại hệ_thống dữ_liệu .
Ngoài_ra , các phần_tử khủng_bố đang sử_dụng mạng Internet để tuyên_truyền và tổ_chức các cuộc đánh bom thông_qua mạng .
Tại hội_nghị trên , các chuyên_gia an_ninh mạng đã thảo_luận các biện_pháp giúp các chính_phủ trong khu_vực ngăn_chặn các phần_tử khủng_bố và tin_tặc khai_thác công_nghệ_thông_tin .
Các chuyên_gia an_ninh mạng khu_vực đã phát_hiện hơn 1.000 website khủng_bố và tin_tặc ở các nước Đông_Nam Á . Vì_vậy , nhu_cầu nghiên_cứu ứng_dụng các giải_pháp chống khủng_bố trên mạng để bảo_vệ các tài_sản kỹ_thuật_số đã trở_nên cấp_thiết đối_với các chính_phủ các nước trong khu_vực .
Malaysia thông_báo xây_dựng Trung_tâm phản_ứng khẩn_cấp nhằm hỗ_trợ các nước trong khu_vực xử_lý và khắc_phục các cuộc tấn_công tin_học vào các hệ_thống kinh_tế và buôn_bán . Các công_ty an_ninh mạng hàng_đầu thế_giới như Symantec_Corp của Mỹ và Kaspersky của Nga là đối_tác chủ_chốt của trung_tâm này .
Theo TTXVN

﻿ 25 nghệ sỹ đẹp nhất Hàn_Quốc
Theo thăm_dò của tạp_chí Movieweek , dưới đây là 25 nghệ sỹ Hàn_Quốc có gương_mặt đẹp nhất trong năm nay . Danh_sách này sẽ được đăng_tải trên số báo tháng tới của tạp_chí danh_tiếng nhất_nhì xứ kim_chi .
Đây là năm thứ_hai , Movieweek tiến_hành cuộc điều_tra này . Những nghệ sỹ có tên trong danh_sách này được tuyển_chọn kỹ_lưỡng từ 130 nghệ sỹ đình_đám nhất của làn_sóng Hàn_Quốc trong năm qua .
Đứng đầu trong danh_sách dành cho nữ nghệ sỹ là một gương_mặt gạo_cội , nữ nghệ sỹ Kim_Hye_Soo , tiếp sau cô là Jun_Ji_Hyun và Lee_Young_Ae . Điều ngạc_nhiên chính là nữ nghệ sỹ có gương_mặt “ mộc ” đẹp nhất Hàn_Quốc , Song_Hye_Gyo lại không có tên trong top 10 .
Với gương_mặt điển_trai đầy vẻ nam_tính , Jang_Dong_Gun lại một lần nữa trở_thành nam nghệ sỹ đẹp nhất xứ Hàn trong khi ngôi_sao của Chuyện tình mùa_đông Bae_Joong_Jun lại không có_mặt trong danh_sách này . Đáng chú_ý là năm nay , nam diễn_viên mới nổi Lee_Jun_Ki của bộ phim Brokeback_Mountain của châu Á cũng lọt vào danh_sách này . Mặc_dù số_lượng phim có sự góp_mặt của Lee_Jun_Ki ( đứng thứ 13 ) chưa nhiều , nhưng hiện anh đang được đánh_giá là tài_năng trẻ triển_vọng của điện_ảnh xứ Hàn .
Danh_sách các nữ diễn_viên có gương_mặt đẹp nhất showbiz Hàn_Quốc
1 . Kim_Hye_Soo
2 . Jun_Ji_Hyun
3 . Lee_Young_Ae
4 . Jeon_Do_Yeon
5 . Lee_Na_Young
6 . Jang_Jin_Young
7 . Jang_Hye_Jung
8 . Bae_Doo_Na
9 . Eom_Jung_Hwa
10 . Kim_Tae_Hee
Danh_sách các nam diễn_viên có gương_mặt đẹp nhất showbiz Hàn_Quốc
1 . Jang_Dong_Gun
2 . Cha_Seung_Won
3 . Jo_In_Sung
4 . Hwang_Jeong_Min
5 . Jung_Woo_Sung
6 . Ryoo_Seung_Beom
7 . Won_Bin
8 . Yoo_Ji_Tae
9 . Lee_Byung_Hun
10 . Kang_Don_Won
Kim_Hye_Soo
Kim_Hye_Soo trở_nên quen_thuộc với khán_giả Việt_Nam qua bộ phim “ Thành_thật với tình_yêu ” . Mặc_dù đã qua tuổi 40 nhưng không ai có_thể phủ_nhận được vẻ quyến_rũ đài_các ở cô . Luôn đi đầu trong các phong_cách thời_trang và là một nghệ sỹ hoạt_động xã_hội tích_cực nhất giới showbiz Hàn_Quốc , Kim_Hye_Soo luôn nhận được sự yêu_mến của khán_giả cũng_như giới truyền_thông .
Lee_Young_Ae
Nổi_danh từ lâu nhưng tính tới nay bộ phim ấn_tượng nhất của Lee_Young_Ae chính là “ Nàng_Dae_Jang_Kum ” . Phim đã được yêu_cầu phát_sóng nhiều lần tại Hàn_Quốc và nhiều nước khác trong khu_vực châu Á .
Bae_Doo_Na
Chưa từng lọt vào danh_sách kiều_nữ của điện_ảnh Hàn_Quốc , nhưng Bae_Doo_Na luôn phấn_đấu không ngừng để khẳng_định bản_thân qua các vai_diễn . Bộ phim mới nhất có sự tham_gia của cô là “ The_Host ” ra_mắt vào cuối tháng 6 này tại Hàn_Quốc hứa_hẹn một cơn_sốt lớn tại các rạp chiếu vào mùa_hè năm nay .
Kim_Tae_Hee
Không ai có_thể phủ_nhận được vẻ đẹp của Kim_Tae_Hee . Theo điều_tra mới_đây của website điện_tử Chosun , Kim_Tae_Hee là một trong những nữ nghệ sỹ có_vẻ đẹp tự_nhiên nhất , không nhờ tới dao_kéo thẩm_mỹ . Tuy_nhiên , để được xếp vào hàng sao như Jun_Ji_Hyun hay Song_Hye_Kyo , có_lẽ Kim_Tae_Hee cần phải cố_gắng và lựa_chọn những vai_diễn nặng_ký hơn .
Jun_Ji_Hyun
Sau thành_công của Hoa_cúc dại tại một_số nước châu Á , trong thời_gian tới Jun_Ji_Hyun quyết_định ưu_tiên những vai_diễn trong phim_truyền_hình và có kinh_phí khiêm_tốn .
Jang_Dong_Gun
Với bộ phim Typhoon , bom tấn 2005 của Hàn_Quốc vừa được công_chiếu tại Mỹ , Jang_Dong_Gun đã nhận được sự đánh_giá rất cao của giới phê_bình điện_ảnh Hollywood .
Jung_Woo_Sung
Sau vai chàng sát_thủ si_tình trong Hoa_cúc dại , cơn_sốt Jung_Woo_Sung đã lan nhanh khắp châu Á . Ngoài_Hoa cúc dại , bộ phim Khoảng khắc đóng cùng nữ diễn_viên Song_Ye_Jin của anh cũng được đánh_giá rất cao .
Won_Bin
Do chấn_thương đầu_gối , W on Bin đã phải xuất_ngũ trước . Mặc_dù vắng bóng trên màn_ảnh hơn 1 năm nhưng sức hút của nam diễn_viên này vẫn không hề thuyên_giảm . Hiện , các fan của anh đang mong_chờ sự trở_lại màn_ảnh của Won_Bin trong thời_gian tới .
Mi_Vân_Theo_Movieweek

﻿ Dạy con ở nhà một_mình
Hướng_dẫn trẻ cách sử_dụng các vật_dụng quen_thuộc trong nhà để nó biết tự xoay_xở khi_không có bạn bên_cạnh . Nên viết những hướng_dẫn đó ra giấy rồi dán đâu_đó để trẻ thường_xuyên nhìn thấy và ghi_nhớ dễ_dàng .
Dạy trẻ cách sử_dụng điện_thoại và ghi lại lời nhắn khi có ai gọi đến . Đặt ra những tình_huống giả_định khẩn_cấp để trẻ luyện_tập cách nói_chuyện qua điện_thoại khi muốn được giúp_đỡ .
Lưu_ý trẻ cẩn_thận khi tiếp_xúc với người lạ , ngay cả bạn_bè cũng không nên mời vào nhà mà đợi cha_mẹ trở_về .
Nếu trẻ có anh_chị_em , nên dặn_dò không nên cãi_vã nhỏ_nhặt và gọi điện than_phiền khi_không cần_thiết . Nhưng bạn cũng_nên để_ý lắng_nghe khi con muốn chia_sẻ những lo_lắng hay phiền_muộn .
Hãy nhớ rằng dù trẻ có khôn_ngoan hay cư_xử chững_chạc thì cũng có_khi mắc sai_lầm . Do_đó , hãy động_viên con thật nhiều và xem những sai_sót như cách học_hỏi thêm kinh_nghiệm .
Cho con thấy sự tin_tưởng và sẵn_sàng thưởng " hậu_hĩnh " nếu nó không sợ_hãi và làm tốt việc được giao khi ở nhà một_mình .
Theo Thanh_Niên

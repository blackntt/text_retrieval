﻿ Mỹ chuẩn_bị phóng tàu_con_thoi lần 2
Dự_kiến Discovery sẽ được phóng trong thời_gian từ ngày 1 đến 19-7 . Quyết_định chính_xác ngày phóng Discovery sẽ được đưa ra_vào giữa tháng 6 .
Giám_đốc chương_trình tàu_con_thoi Wayne_Hale bày_tỏ niềm tin_tưởng Discovery sẽ cất_cánh đúng như kế_hoạch và NASA sẽ có_thể phóng tiếp 2 con tàu nữa trước cuối năm nay . Nếu thất_bại thì NASA sẽ buộc phải rút ngắn quá_trình " về hưu " dự_kiến vào năm 2010 của tàu Discovery , trong khi thế_hệ tàu_vũ_trụ thay_thế vẫn đang trong giai_đoạn phát_triển .
Ban_đầu tàu Discovery dự_kiến được phóng vào tháng này , tuy_nhiên kế_hoạch đã được thay_đổi khi các nhà_khoa_học phát_hiện cảm_biến thùng nhiên_liệu bị hỏng .
Nhiệm_vụ của đội bay tàu_con_thoi Discovery lần này sẽ là thử_nghiệm hệ_thống an_toàn mới , tiếp_tế và cung_cấp nguyên_liệu cho Trạm không_gian quốc_tế ( ISS ) , thực_hiện ít_nhất hai chuyến đi bộ ngoài không_gian . Ngoài_ra , nhà du_hành người Đức thuộc Cơ_quan vũ_trụ châu Âu sẽ ở lại trên ISS trong nhiều tháng nhằm thực_hiện các thí_nghiệm khoa_học .
TƯỜNG_VY ( Theo BBC , Xinhua )

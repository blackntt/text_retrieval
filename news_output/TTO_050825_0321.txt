﻿ Đà_Nẵng : lập 2 phương_án vừa kiểm_định vừa đối_chứng
Sau khi bàn_bạc , tổ kiểm_định cùng các hộ dân đã thống_nhất hai phương_án bao_gồm : Tháo_gỡ ba điện_kế của ba hộ dân ( gồm hai điện_kế AMR và một điện_kế điện_tử ) niêm_phong đưa sang nhờ Trung_tâm Tiêu_chuẩn đo_lường chất_lượng 2 kiểm_định .
Với bốn điện_kế AMR của bốn hộ khác , ngành điện sẽ lắp_đặt thêm mỗi hộ một điện_kế cơ ( không có thiết_bị đo_đếm từ xa ) để đối_chứng . Thời_gian lấy đối_chứng của phương_án này là một tháng . Riêng các điện_kế tháo ra đưa đi kiểm_định sẽ có kết_quả sau một tuần .
Tin , ảnh : Đ . NAM

﻿ Nha_Trang : hàng trăm chim yến làm tổ trong một ngôi nhà
Theo Nha_Trang : hàng trăm chim yến làm tổ trong một ngôi nhà một thành_viên trong gia_đình này , chim yến tụ_tập đến ở và làm tổ tại đây đã được nhiều năm ( khoảng từ 1996 ) , đến nay đàn chim đã tăng lên tới cả trăm con .
Theo giám_đốc Sở Xây dựng tỉnh Khánh_Hòa_Trần_Kỳ_Định , ngôi nhà kể trên thuộc sở_hữu nhà_nước do tỉnh quản_lý .
Ngoài_mặt bằng tầng trệt đang cho thuê kinh_doanh , còn có ba gia_đình đã và đang được bố_trí cho ở các tầng bên trên . Trong đó có hai phòng phía sau của tầng hai và tầng ba_không bố_trí sử_dụng , được đóng kín một thời_gian dài ; qua lỗ thông gió chim yến đã tập_trung vào ở và làm tổ .
Khi có người phát_hiện và báo cho cơ_quan_chức_năng đến kiểm_tra thì chim yến đã làm được khoảng 45 tổ , đến nay số tổ yến đã tăng lên tới khoảng 84 tổ .
Dự_kiến sắp tới tỉnh sẽ bố_trí cho các hộ đang sống trong ngôi nhà này di_chuyển đi nơi khác để giao lại toàn_bộ cho Công_ty Yến_sào Khánh_Hòa quản_lý , nhằm thu_hút thêm nhiều chim yến vào ở làm tổ để khai_thác kinh_doanh ( tổ chim yến - hay còn gọi_là yến_sào - là mặt_hàng rất có giá_trị ) .
PHAN_SÔNG_NGÂN

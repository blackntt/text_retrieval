﻿ Bill_Gates sẽ thăm VN tháng tới
Chuyến thăm của chủ_tịch tập_đoàn tin_học hàng_đầu thế_giới nhằm mục_đích tìm_hiểu tình_hình công_nghệ_thông_tin tại VN và đưa ra những chương_trình giúp_đỡ VN phát_triển mạnh hơn_nữa trong lĩnh_vực công_nghệ_thông_tin .
Hiện_tại , Bộ Bưu chính - viễn_thông đang xúc_tiến phối_hợp với Phòng_Thương mại và công_nghiệp VN ( VCCI ) để thống_nhất nội_dung buổi làm_việc giữa Bill_Gates với VCCI , trong đó Microsoft sẽ hỗ_trợ các doanh_nghiệp vừa và nhỏ của VN ứng_dụng công_nghệ_thông_tin trong kinh_doanh . Trong chuyến thăm của mình , dự_kiến ông Bill_Gates sẽ được Thủ_tướng Phan_Văn_Khải tiếp .
K . HƯNG

﻿ Basten chuẩn_bị cho trận gặp Ecuador
Huâ ́ n luyê ̣ n viên Marco van Basten_Huâ ́ n luyê ̣ n viên Ha ̀ Lan_Marco van Basten đã công_bố danh_sách đô ̣ i hi ̀ nh tạm_thời chuâ ̉ n bi ̣ cho trâ ̣ n giao hư ̃ u vơ ́ i Ecuador .
Đây se ̃ la ̀ cơ hô ̣ i đâ ̀ u tiên cho ông Van_Basten thư ̉ nghiê ̣ m kê ̉ tư ̀ buô ̉ i bô ́ c thăm World_Cup khi đô ̣ i ông vào cùng bảng vơ ́ i Argentina , Serbia & amp ; Montenegro va ̀ Bơ ̀ Biê ̉ n Nga ̀ .
Tiê ̀ n đa ̣ o cu ̉ a Ajax_Klaas_Jan_Huntelaar la ̀ mô ̣ t trong ba câ ̀ u thu ̉ lâ ̀ n đâ ̀ u tiên đươ ̣ c go ̣ i va ̀ o đô ̣ i Ha ̀ Lan . Đô ̣ i hi ̀ nh chi ́ nh thư ́ c cu ̉ a Ha ̀ Lan se ̃ đươ ̣ c công_bố va ̀ o nga ̀ y 24-2 và trâ ̣ n đâ ́ u giao hư ̃ u vơ ́ i Ecuador se ̃ diê ̃ n ra ơ ̉ sân Amsterdam nga ̀ y 1-3 .
Danh sa ́ ch ta ̣ m thơ ̀ i :
Maarten_Stekelenburg ( Ajax ) , Henk_Timmer , Edwin van der Sar ( Manchester_United )
Urby_Emanuelson ( Ajax ) , Tim de Cler ( AZ Alkmaar ) , Kew_Jaliens ( AZ ) , Joris_Mathijsen ( AZ ) , Barry_Opdam ( AZ ) , Giovanni van Bronckhorst ( Barcelona ) , Ron_Vlaar ( Feyenoord ) , Khalid_Boulahrouz ( Hamburg ) , Nigel de Jong ( Hamburg ) , Andre_Ooijer
Hedwiges_Maduro ( Ajax ) , Wesley_Sneijder ( Ajax ) , Denny_Landzaat ( AZ ) , Mark van Bommel ( Barcelona ) , Nicky_Hofs ( Feyenoord ) , Janssen ( Vitesse_Arnhem ) .
George_Boateng ( Middlesbrough ) , Phillip_Cocu ( PSV ) , Edgar_Davids ( Tottenham_Hotspur ) Klaas-Jan Huntelaar ( Ajax ) , Robin van Persie ( Arsenal ) , Roy_Makaay ( Bayern_Munich ) , Arjen_Robben ( Chelsea ) , Romeo_Castelen ( Feyenoord ) , Dirk_Kuyt ( Feyenoord ) , Ruud van Nistelrooy ( Manchester_United ) , Jan_Vennegoor of Hesselink ( PSV ) .
Theo Thanh_Niên

﻿ Mùi ngò gai , bộ phim hợp_tác giữa Việt_Nam và Hàn_Quốc
Đề_tài tâm_lý xã_hội có pha hài nhẹ_nhàng , Mùi ngò gai phản_ánh một phần cuộc_sống đương_đại của người trẻ VN qua hình_ảnh của Vi , một phụ_nữ VN biết chọn cho mình cách sống đầy nghị_lực trong tình_yêu và sự_nghiệp .
Đặc_biệt trong phim nhấn_mạnh văn_hóa ẩm_thực VN qua món phở .
Các diễn_viên tham_gia có Ngọc_Trinh , Minh_Đạt , Kim_Hiền , Mỹ_Duyên , Thành_Lộc , Kim_Xuân , Việt_Anh ... Dự_kiến phim sẽ hoàn_thành vào_khoảng tháng 8-2006 . Đây là bộ phim mở_đầu cho sự hợp_tác giữa Hãng phim VIFA - VN và Công_ty FNC ( thuộc Tập_đoàn giải_trí CJ Entertainment của Hàn_Quốc ) .
N.CH ƯƠNG - H.NAM

﻿ Tù_nhân khâu mồm đòi xem World_Cup
Hai tù_nhân người Bulgari đã tự_động khâu mồm họ lại và không chịu ăn_uống gì cả để đòi được xem các trận đấu trong mùa giải World_Cup đang diễn ra ở Đức .
Ông Vesselin_Kotzev , Giám_đốc nhà_tù Pazardzhik cho_biết hai phạm_nhân bị tù chung_thân yêu_cầu phải có một chiếc TV đặt trong phòng giam của họ .
Ông này kể , tù_nhân còn đòi_hỏi cung_cấp “ lương_thực ” để họ bồi_bổ trong mùa World_Cup , như chè , café và thuốc_lá .
Kotzev nói : “ Tôi đang thương_lượng thuyết_phục họ không nên khâu mồm vì đó không phải là cách hay để ép_buộc chúng_tôi ” .
Buồn_cười là 2 tù_nhân này để hở một khóe miệng để có_thể hút thuốc như ngày thường . Những tên tội_phạm đang lãnh án chung_thân rất mê bóng_đá này còn rủ_rê 15 tù_nhân khác làm theo họ để giành số đông áp_đảo nhà_tù .
N . H_Theo_Ananova

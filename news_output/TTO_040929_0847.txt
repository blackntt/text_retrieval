﻿ Lãnh án tù chung_thân vì lừa_đảo bằng “ sổ_đỏ ”
Do vay tiền_của nhiều người mà không có khả_năng trả nợ nên Nguyễn_Văn_Tê đã nghĩ cách nhờ Nguyễn_Anh_Tình xin giấy chứng_nhận quyền sử_dụng đất lưu_không ( do Tổng_cục Địa_chính phát_hành ) để tự viết nội_dung vào và thuê người khắc dấu để làm giả . Tình đã xin Bân và hai lần được Bân cho tổng_cộng 23 tờ để đưa cho Tê đi làm giả . Lợi_dụng các “ sổ_đỏ ” giả này , Tê đã chuyển_nhượng hoặc dùng để thế_chấp vay tiền , lừa_đảo của 12 người số tiền 5,4 tỉ đồng và 150.430 USD .
V . C . M

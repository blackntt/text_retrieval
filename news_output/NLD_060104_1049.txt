﻿ Xuất_hiện dấu_vết của vật_thể bay lạ
Vật_thể bay lạ đã từng xuất_hiện nhiều lần trước_đây . Nhiều hãng tin của Argentina ngày 2-1 đưa tin cho_biết tại tỉnh Entre_Rios , cách thủ_đô Buenos_Aires ( Argentina ) khoảng 1.700 km về phía Bắc , vừa xuất_hiện một hiện_tượng được cho là dấu_vết của Vật_thể bay lạ ( UFO ) .
Chuyện xảy_ra tại một trang_trại ở thành_phố Victoria thuộc tỉnh Entre_Rios khi lượng nước bể_bơi trong vườn nhà này bị cạn đi một_nửa trong vòng vài giờ đồng_hồ và dưới đáy bể còn đọng lại nhiều dấu_vết lạ trông giống như bàn_chân vịt khổng_lồ .
Hiện_tượng Vật_thể bay lạ ( UFO ) đã từng diễn ra tại địa_phương này cách đây 15 năm . Các nhà_khoa_học chuyên nghiên_cứu UFO đã đến hiện_trường và xác_nhận trong bể_bơi vẫn còn những dấu_vết lạ và giải_thích đây là hiện_tượng UFO , liên_quan đến sự xuất_hiện khá thường_xuyên một thứ ánh_sáng lạ trên hồ nước “ Ông cá ” nằm bên_cạnh chiếc cầu lớn nối tỉnh Entre_Rios với tỉnh Rosario .
Theo TTXVN

﻿ “ Thực_hiện đầy_đủ nhiệm_vụ tư_vấn và phản_biện xã_hội ”
& gt ; & gt ; Nhức_nhối , tự_ái và lo_toan & gt ; & gt ; Cần có đoàn kiến_trúc_sư
Chủ_tịch nói : “ KTS không_chỉ có bổn_phận xây_dựng những tòa nhà đẹp mà_còn có sứ_mệnh xã_hội lớn_lao , góp_phần cải_tạo và đổi_mới xã_hội " .
Các KTS đã kiến_nghị thành_lập lại Hội_đồng tư_vấn kiến_trúc của Thủ_tướng ( được thành_lập 1994 và đã giải_thể năm 1999 ) .
Cũng tại ĐH , sau khi phân_tích , đánh_giá thực_trạng đội_ngũ KTS và nền kiến_trúc nước_nhà , hầu_hết các KTS đã thống_nhất kiến_nghị thành_lập Đoàn KTS - một tổ_chức độc_lập với Hội KTS .
Theo Báo cáo đề_xuất của Đoàn_chủ_tịch ĐH , Đoàn KTS là tổ_chức của các KTS đang hành_nghề tư_vấn và thiết_kế kiến_trúc , không bắt_buộc phải là Hội_viên Hội KTS . Đoàn KTS sẽ bảo_vệ bản_quyền của các thiết_kế kiến_trúc , làm trung_gian quyền_lợi của các KTS với khách_hàng của mình . Đoàn KTS có quyền cấp thẻ hành_nghề và thu_hồi thẻ hành_nghề của các KTS thành_viên .
ĐH còn làm_việc ngày 13-8 để thống_nhất các ý_kiến đóng_góp văn_bản đề_nghị thành_lập Đoàn KTS và bầu Đoàn_chủ_tịch mới của Hội KTSVN .
THU_HÀ

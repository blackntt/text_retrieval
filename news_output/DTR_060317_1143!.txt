﻿ Ôtô vô_chủ có súng nằm ở sân_bay
Ngày 16/3 , lực_lượng an_ninh sân_bay Tân_Sơn_Nhất phát_hiện một chiếc xe ôtô vô_chủ tại khu_vực sân_bay , trong xe có súng săn và đạn .
Chiếc xe hiệu Fiat biển_số 61K-3984 sau đó được xác_định là của ông Nguyễn_Văn_Vốn . Ông Vốn cho_biết , ngày 7/3 , ông đến Cục thuế TPHCM làm_việc và để xe trên đường Nguyễn_Thị_Minh_Khai , Q . 1 , lúc quay ra thì chiếc xe đã “ không_cánh_mà_bay ” .
Hiện công_an tiếp_tục điều_tra sự_việc . N.Lưu

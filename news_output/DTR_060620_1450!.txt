﻿ Kỳ II : Cuộc ăn_mày oan_nghiệt
Để viết tác_phẩm này , Minh_Chuyên đã theo_đuổi nhiều năm trời trong đó , gần 2 năm trời tác_giả tự_nguyện cùng_với nhân_vật đi hàng chục chuyến , đến hàng chục cơ_quan vượt hàng ngàn km gặp hàng trăm nhân_chứng . Chỉ riêng tiền photo tài_liệu đã hàng trăm_ngàn đồng ( tiền năm 1988 ) .
Có_lẽ ở ta , hiếm có tác_phẩm nào lại được thực_hiện kỳ_công như_vậy và cũng hiếm có nhà_báo , nhà_văn nào nhập_cuộc đầy dấn_thân như_vậy . “ Nhiều lúc nản lắm nhưng vì tình đồng_đội mà cố ” ( Minh_Chuyên ) . Hành_trình đó , đã được Minh_Chuyên kể lại trong tác_phẩm .
Hồ_sơ của Định lại theo Minh_Chuyên lên Hà_Nội đến Cục Tổ chức động_viên . Lên từ thứ_ba , chiều thứ_năm mới đúng ngày trực . Tiếp tôi hôm ấy_là một đại_uý chừng năm_mươi tuổi , người thấp nhỏ . Sau mới biết anh tên là Bảo . Đại_uý Bảo xem lướt hồ_sơ rồi nói :
- Lính quân_đoàn X - à ? Trường_hợp này chúng_tôi giới_thiệu đồng_chí về đơn_vị giải_quyết nhé ?
- Báo_cáo anh , đơn_vị ở mãi Campuchia .
- Không , đã chuyển ra ngoài Y rồi - Đại_uý Bảo nói .
- Thưa anh , mấy năm trước gia_đình đã đưa em Định đi tìm đơn_vị ở trong Nam , mãi không thấy . Giờ lại tìm ngoài Bắc , biết ở đâu mà tìm ?
Thấy anh Bảo im_lặng , tôi nói tiếp :
- Các anh dưới tỉnh nói chỉ cần trên này ghi mấy chữ chuyển Bộ_chỉ_huy quân_sự tỉnh là ở dưới đó các anh_ấy làm . Thực_tế , nhiều trường_hợp tỉnh đã giải_quyết rồi mà .
Tôi dẫn_chứng và nài mãi , cuối_cùng , bất_đắc_dĩ đại_uý Bảo mới hạ_bút ghi sau tờ giấy giới_thiệu “ Chuyển_Bộ_Chỉ huy Quân_sự Thái_Bình . Trường_hợp đồng_chí Trần_Quyết_Định theo nguyên_tắc thì E24 phải giải_quyết . Nhưng theo nguyện_vọng và tình_hình cụ_thể của đồng_chí Định , tỉnh xem_xét nếu giải_quyết được thì giải_quyết . Cục không làm cụ_thể được ” .
Trở_về Thái_Bình , hai , ba lần đến Ban Tổ chức động_viên tỉnh_đội , chúng_tôi đều được trả_lời “ Cục ghi toàn những ý lấp_lửng , không dứt_khoát , chúng_tôi giải_quyết làm_sao được ? ” .
Tôi lại lên Hà_Nội , gặp đại_uý Bảo . Anh Bảo nói :
- Thôi được , chúng_tôi sẽ linh_động làm một quyết_định bổ_sung quân_số cho quân_khu . Một quyết_định đề_nghị địa_phương giải_quyết chính_sách cho anh Định . Tuần sau đồng_chí lên nhé .
Tôi về kể lại , nhà Định mừng lắm . Nhưng khốn thay , tuần sau lên gặp , anh Bảo lại lắc_đầu :
- Trường_hợp của Định , chúng_tôi có hướng giải_quyết như lần trước nói với anh . Nhưng khi xin ý_kiến , các đồng_chí lãnh_đạo Cục yêu_cầu phải chuyển tới đồng_chí Tham_mưu_trưởng sư 10 giải_quyết . Sư 10 bây_giờ đóng quân khu_vực Y , đồng_chí đến ga ... Rẽ phải . Hỏi_thăm vào ... rồi đến ...
Nói xong , anh Bảo trả lại tập hồ_sơ kèm theo tờ công thư_ký ngày 9/10/1987 gửi Tham_mưu_trưởng sư 10 . Anh Bảo dặn thêm “ Phải cố_gắng tìm gặp đồng_chí Tham_mưu_trưởng mới giải_quyết được ” . Nhưng xem công thư , thấy Cục chỉ đề_nghị sư 10 xác_minh và cho hướng giải_quyết chứ không phải đề_nghị giải_quyết .
Lần này về , đọc tờ công thư , nhiều người nản_lòng . Suốt mấy năm trời tập hồ_sơ đã đi qua nhiều cửa , từ xã lên huyện , lên tỉnh lên trung_ương , tới cả Cục Tổ chức động_viên của Bộ Tổng tham_mưu mà cửa nào cũng chỉ được ghi vào góc đơn mấy chữ quen_thuộc cho hướng giải_quyết , đề_nghị nghiên_cứu , xác_minh , giúp_đỡ , xem_xét ...
Một_số người an_ủi :
- Thôi , leo cây sắp đến buồng , cứ cố_gắng đi một_vài chuyến nữa , may_ra thì ...
Nhưng hoàn_cảnh nhà Định không có điều_kiện đi ngay được . Phải gần một tháng sau , Định mới cơm đùm , cơm gói lên_đường . Bảy ngày_sau , anh phờ_phạc quay về . Ở khu_vực đại_uý Bảo hướng_dẫn , có nhiều đơn_vị đóng quân , Định không tìm được sư 10 . Hết tiền ăn , anh phải bỏ về .
Lại gần hai tháng nữa chuẩn_bị , vay_mượn tiền_nong . Lần này cả tôi và Định cùng ra_đi . Thấy chúng_tôi rậm_rịch chuẩn_bị lên_đường , anh Đoàn_Duyến thương_binh hạng 3 , bạn tôi rỉ_tai :
- Dấn vào cho_xong ! Việc_gì phải đi mãi cho mệt !
Tôi bảo :
- Trường_hợp của anh Định bị_thương là sự_thật , lại đầy_đủ giấy_tờ ...
Anh Duyến mỉm cười :
- Giấy_tờ , thật giả , bây_giờ người_ta có quan_tâm lắm đâu . Cứ có khoản kia là xong tất . Còn không á , có_khi thật lại hoá giả . Mấy tay ở La_Sào , đánh_đấm gì đâu vẫn có sổ thương_binh nghiêm . Khối tay thương_binh loại hai chạy lên hạng 1 . Thằng T . con ông Đ , khoẻ như trâu về hưởng chế_độ mất_sức . Đấy , chúng_nó mạnh vì gạo , bạo vì tiền !
- Nhưng hoàn_cảnh nhà Định , gạo còn chả đủ ăn , lấy đâu mà mạnh , mà bạo - Tôi nói .
- Còn hơn tàu_xe vào Nam ra Bắc , tốn quá ấy chứ ! Không có , phải cố mà lo . Cứ đi mãi , liệu bao_giờ mới giải_quyết được ?
Tôi và Định vẫn quyết_định đi theo hướng đã chọn , kiên nại và chịu_khó , dù có phải vất_vả . Vì chúng_tôi vẫn tin , ở đời không phải mọi chuyện đều bi_quan , tiêu_cực như anh Đoàn_Duyến nghĩ ...
Đầu xuân năm 1988 , khi đợt rét cuối mùa dai_dẳng còn bứt vào da_thịt , tôi và Định lại lên_đường đến khu_vực Y tìm sư 10 . Thật không may , sư_đoàn vừa chuyển vào làm kinh_tế ở Tây_Nguyên . Chán_chường , mệt_mỏi , thất_vọng , hai chúng_tôi cuốc_bộ ra ga nhảy tàu về xuôi . Rủi_ro không_chỉ có thế , trong nhà chờ vừa đông , vừa tối lại nhốn_nháo , loáng cái , kẻ_cắp đã nẫng mất chiếc ba - lô của Định . Chăn màn , quần_áo , cơm nắm , tép khô , cả tiền_nong mất sạch . May_mà hồ_sơ , giấy_tờ , chứng_minh_thư , Định đút_túi áo trong nên vẫn còn . Thế_là chúng_tôi lâm vào cảnh thật khó xử : Đi thì dở , ở lấy gì mà ăn , về tiền đâu mua vé !
Mười giờ đêm , sau hồi còi dài lanh_lảnh , đoàn tàu hối_hả , xả_hơi , từ_từ , dừng lại trước sân ga . Chúng_tôi nhanh chân leo lên toa số 7 . Ngồi vừa ấm chỗ , một nhân_viên nhà_ga , người cao_to , tay đeo băng đỏ , hông lắc_lư xà_cột đen xộc đến kiểm_tra vé . Định giơ hồ_sơ , giấy_tờ ra trình_bày , xin đi nhờ . Anh nhân_viên gạt đi . Tôi trình_bày lại hoàn_cảnh mất_cắp , anh nhân_viên nắm cổ_tay tôi , tay kia túm áo Định , sừng_sộ kéo chúng_tôi ra cửa toa . Trước hàng trăm con mắt đổ về phía mình , tôi hổ_thẹn nhảy xuống ngay . Còn_Định cứ bám chặt vào chấn_song toa tàu . Anh thanh_niên đẩy một cái . Định ngã_ngửa đập đầu xuống rìa đường . Tôi lao tới đỡ anh dậy . Định nhăn mặt nén đau , lại nằm_xuống .
Đêm ấy , chúng_tôi đành nhịn_đói ngồi ôm nhau cho đỡ rét , đợi sáng . Hôm_sau , không còn cách nào khác , chúng_tôi đành phải nhẫn_nhục hành_khất để lấy tiền mua vé . Trong phòng đợi tàu , tôi lân_la đến bên một ông chừng năm_chục tuổi , đeo kính , mặc com_lê , thắt ca - la-vát đỏ , ngồi cạnh một chiếc va ly màu_da đồng . Đoán ông là cán_bộ dễ thông_cảm , nhưng ngập_ngừng mãi , tôi mới dám nói :
- Thưa bác , hai anh_em cháu đi tìm đơn_vị đề giải_quyết chính_sách . Chẳng may bị kẻ_cắp lấy hết , không còn đồng nào mua vé . Xin bác thông_cảm giúp chúng_cháu một tý !
Ông_ta nhìn tôi lạnh_lùng , rồi lắc_đầu :
- Không có tiền !
Đến bên một người trung_niên , mặc áo măngtôsan màu sữa , đầu đội mũ_phớt , đứng dựa vào bức tường nhà_ga . Định run_run trình_bày rồi hỏi xin . Anh_ta hất hàm :
- Làm mà ăn ! Trông người như_thế , xin không nhục à ?
Một cái gì nhói trong lồng_ngực , tôi vờ quay mặt đi . Định không dám nói_gì thêm , lẳng_lặng lùi ra . Lúc này tôi mới thấu_hiểu nỗi cực_nhục của những người đi ăn_mày mà hàng ngày vẫn gặp . Nhưng chẳng còn cách nào khác . Đói thì cố nhịn , nhưng tiền mua vé lấy đâu ra ? Lại đành phải ...
Chúng_tôi lững_thững tới chỗ hai anh bộ_đội ngồi ngoài sân ga . Lúc đầu , họ nghĩ bọn tôi là kẻ_cắp vờ để chôm_chỉa . Sau tin , một anh móc_túi áo đưa cho Định tờ hai chục_ngàn đồng . Chúng_tôi cảm_ơn , ra mấy chỗ người xách túi đứng sát đường_ray . Người thì lắc_đầu , người thì mở ví lấy cho mươi đồng . Ít_nhiều cũng là quý , chúng_tôi không dám nài thêm . Một bà buôn sắn bảo :
- Tí nữa tàu dừng , chuyển hộ mấy bì này lên , tôi cho tiền !
Ăn cơm nắm từ trưa hôm trước , khiêng mấy bao sắn nặng , tôi và Định bủn_rủn chân_tay . Định run như người sắp lên cơn_sốt , mặt anh tái_mét , nhợt_nhạt , mắt lờ_đờ , mồ_hôi trán vã ra . Tôi hoảng quá , sợ anh bị choáng . Bà buôn sắn trả công một_trăm_rưởi . Định phải mấy lần ngửa_tay nữa , chúng_tôi mới gom đủ tiền mua hai cái vé tàu xuôi ...
Gần nửa tháng sau , bận công_tác , tôi không có dịp về thăm nhà . Bỗng một hôm nhận được tin Định đang phải cấp_cứu ở bệnh_viện . Lòng tôi đau_nhói . Tôi nghĩ , có_lẽ Định đã liều_mình nên mới_phải đi cấp_cứu . Tôi sực nhớ hôm ở trên tàu quay về , Định gục vào_vai tôi , vừa sụt_sùi khóc , vừa nói : “ Em chả thiết sống nữa . Hẳn như phải nằm dưới mộ ở nghĩa_trang Thạch_Tây , gia_đình em lại được vẻ_vang , anh_em mình cũng chẳng đến_nỗi chịu khổ_nhục như_thế_này ... ” .
Đến bệnh_viện , tôi mới hay không phải như mình đoán . Định bị choáng và ngất , có_lẽ tại anh nghĩ_ngợi và dằn_vặt quá nhiều ?
Ngồi đối_diện với tôi bên giường_bệnh , ông Vọng đặt bàn_tay đen sạm dăn_deo lên trán Định , ngẩng lên nói với tôi :
- Hôm nào em nó khỏi , hai anh_em lại cố đi chuyến nữa , may_ra tìm được ông sư_trưởng ...
Tôi gật_đầu để ông yên_tâm mà lòng cứ miên_man nghĩ về những điều rủi may của Định . Từ ngày sống_sót về quê , Định có ham_muốn gì lớn ngoài cái nguyện_vọng rất chính_đáng là có những giấy_tờ cần_thiết để làm một người sống bình_thường ?
Vậy_mà , đã mười năm rồi , mười năm lận_đận long_đong , anh vẫn chưa lo nổi cái thủ_tục bình_thường để được làm một người còn sống .
Bùi_Hoàng_Tám_Kỳ III : “ Cơn địa_chấn ” bàng_hoàng xã_hội

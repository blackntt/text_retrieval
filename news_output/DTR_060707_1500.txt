﻿ Siết chặt kỷ_luật phòng thi ĐH , CĐ đợt 2
Trong đợt 2 của mùa tuyển_sinh năm nay , do có nhiều khối thi nên Bộ GD - ĐT vừa gửi công_văn khẩn yêu_cầu Chủ_tịch Hội_đồng tuyển_sinh các trường nhắc cán_bộ coi thi kiểm_tra chặt_chẽ , ngăn_chặn tuyệt_đối việc mang điện_thoại_di_động vào phòng thi .
Giám_thị kiên_quyết đình_chỉ thi đối_với những thí_sinh mang tài_liệu , vật_dụng trái_phép vào phòng thi , trong đó có điện_thoại_di_động , các phương_tiện thu phát truyền tin khác .
Bộ GD - ĐT yêu_cầu các giám_thị khi phát_hiện thí_sinh mang điện_thoại_di_động vào phòng thi mặc_dù để chế_độ tắt vẫn phải lập biên_bản và tạm giữ điện_thoại_di_động để công_an kiểm_tra , xác_minh làm rõ .
Đối_với các giám_thị nếu vi_phạm quy_chế sẽ bị xử_lý kỷ_luật . Tuỳ vào mức vi_phạm các trường sẽ có hình_thức xử_lý cụ_thể như hạ bậc lương hoặc xử_lý kỷ_luật cán_bộ . Ngoài_ra , sinh_viên tham_gia coi thi có vi_phạm cũng sẽ bị xử_lý .
Cũng trong công_văn khẩn này , Bộ GD - ĐT đề_nghị Chủ_tịch các Hội_đồng thi cần làm rõ và xử_lý kịp_thời những cán_bộ liên_đới trong việc để thí_sinh mang điện_thoại_di_động vào phòng thi .
Kết_thúc đợt 1 tuyển_sinh , cả nước có 252 thí_sinh và 23 giám_thị vi_phạm quy_chế . Minh_Hạnh

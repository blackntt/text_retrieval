﻿ Góp tay xoa_dịu nỗi đau da_cam
Ông Nguyễn_Sơn ( phải ) , Phó_giám_đốc Công_ty Fosco , trao số tiền ủng_hộ chương_trình đến Tổng_biên_tập Báo_Tuổi_Trẻ_Lê_Hoàng - Ảnh : T.Nguyên
Bạn_Minh_Thảo , công đoàn_viên của Công_ty Dịch_vụ cơ_quan nước_ngoài ( Fosco ) , đã nói như_thế trong buổi phát_động phong_trào “ Ủng_hộ nạn_nhân chất_độc da_cam ” của Fosco chiều_tối qua 31-8 .
Minh_Thảo cho_biết mình thật_sự xúc_động khi nghe anh Lê_Hoàng , Tổng_biên_tập Báo_Tuổi_Trẻ , giải_thích chương_trình “ Góp tay xoa_dịu nỗi đau da_cam ” do báo phát_động là để giúp các nạn_nhân nhiễm dioxin đi đòi công_lý cho mình .
Hơn 200 công đoàn_viên của Fosco có_mặt tại lễ trao thẻ công đoàn_viên và phát_động phong_trào ủng_hộ nạn_nhân cũng đã tham_gia ký_tên ủng_hộ chương_trình “ Một chữ_ký - triệu tấm_lòng ” và đóng_góp một phần kinh_phí cho chương_trình “ Góp tay xoa_dịu nỗi đau da_cam ” .
Đồng_thời , công_đoàn Fosco đã trích 50 triệu đồng “ tham_gia chương_trình trước , sau đó sẽ có thư_ngỏ đến các nhân_viên đang làm_việc ở 70 cơ_quan ngoại_giao và 1.000 văn_phòng đại_diện để tham_gia cuộc vận_động này ” - ông Nguyễn_Sơn , Phó_giám_đốc công_ty , cho_biết .
Yuko_Ohigashi - tài_năng piano Nhật ủng_hộ nạn_nhân chất_độc da_cam VN
Yuko_Ohigashi và mẹ - Ảnh : T.Ng .
Sáng 31-8 , ngay sau khi đặt_chân đến VN , tài_năng piano Nhật_Yuko_Ohigashi ( 16 tuổi ) và mẹ đã đến Báo_Tuổi_Trẻ ký_tên ủng_hộ nạn_nhân chất_độc da_cam VN .
Yuko biết đàn piano từ 6 tuổi , sáng_tác nhạc lúc 8 tuổi và hiện đang học tại Trường trung_học dành cho tài_năng trẻ Interlochen_Arts_Academy ( Mỹ ) .
Dịp này , Công_ty Kiến_Vàng ( đơn_vị tổ_chức ) cũng tham_gia ký_quỹ ủng_hộ 2 triệu đồng vào chương_trình “ Góp tay xoa_dịu nỗi đau da_cam ” của Báo_Tuổi_Trẻ .
Đây là lần đầu_tiên Yuko đến VN . Ngoài những tối biểu_diễn tại bar Yesterday ( từ 31-8 đến 2-9 ) , Yuko còn biểu_diễn độc_tấu piano cho bạn nhỏ mồ_côi chùa Diệu_Giác ( Q . 2 , TP.HCM ) .
Quỹ “ Góp tay xoa_dịu nỗi đau da_cam ” đã có trên máy ATM
Bạn_Lê_Tấn_Thọ , nhân_viên Công_ty Microtec VN , người đầu_tiên ủng_hộ quỹ " Góp tay xoa_dịu nỗi đau da_cam " thông_qua máy ATM - Ảnh : Đông_Hải
Từ hôm_nay 1-9 , tất_cả khách_hàng có thẻ ATM của Ngân_hàng Ngoại_thương VN ( VCB ) đã có_thể đóng_góp vào quỹ “ Góp tay xoa_dịu nỗi đau da_cam ” của Báo_Tuổi_Trẻ .
Chương_trình do Báo_Tuổi_Trẻ phối_hợp với VCB thực_hiện nhằm giúp bạn_đọc thuận_tiện hơn trong việc ủng_hộ tài_chính cho vụ_kiện lịch_sử mà Báo_Tuổi_Trẻ đã phát_động gần một tháng nay .
Chương_trình đã được cài_đặt cho 200 máy rút_tiền tự_động ( ATM ) của VCB trên toàn_quốc . Bạn_đọc chỉ cần đưa thẻ ATM vào máy , sau đó chọn mục “ ủng_hộ từ_thiện ” , bấm số tiền mà bạn ủng_hộ , chọn đồng_ý là hoàn_tất công_việc đóng_góp . Số tiền này sẽ được chuyển vào tài_khoản của Báo_Tuổi_Trẻ tại VCB .
Anh Huỳnh_Song_Hào , trưởng_phòng kinh_doanh dịch_vụ thẻ_tín_dụng VCB , cho_biết : “ Vì hơn 4 triệu nạn_nhân của chất_độc da_cam , VCB không_thể không góp tay vào . Hy_vọng với gần 400.000 khách_hàng đang sử_dụng thẻ ATM , chúng_ta sẽ có thêm nguồn_lực tài_chính giúp các nạn_nhân trong vụ_kiện sắp tới ” .
TP Cần_Thơ : Hàng ngàn người “ ký_tên vì công_lý ”
Ban_chấp_hành Đoàn_Xí nghiệp liên_hiệp dược Hậu_Giang đã kêu_gọi cán_bộ , nhân_viên trong xí_nghiệp “ ký_tên vì công_lý ” . Nhiều cơ_sở Đoàn khối doanh_nghiệp TP Cần_Thơ như : Bưu_điện TP , Công_ty Du_lịch TP , Công_ty ximăng Hà_Tiên II ... và đông_đảo người_dân cũng đã tham_gia với hơn 1.000 chữ_ký ủng_hộ nạn_nhân chất_độc da_cam .
Bên_cạnh đó , tại văn_phòng đại_diện Báo_Tuổi_Trẻ tại TP Cần_Thơ cũng đã có hơn 1.000 chữ_ký của sinh_viên Trường_Đại học Cần_Thơ và học_sinh các trường trung_học trên địa_bàn thành_phố .
Thiếu_nhi cả nước vào_cuộc
Ảnh : Tố_Oanh
“ Đội_viên , thiếu_niên , nhi_đồng cả nước ngay từ bây_giờ hãy tích_cực tham_gia hưởng_ứng ký_tên ủng_hộ nạn_nhân chất_độc da_cam ” - đó là thông_điệp Hội_đồng Đội trung_ương chính_thức kêu_gọi tại lễ phát_động diễn ra_chiều 31-8 tại Nhà_Thiếu nhi TP.HCM .
Hơn 200 bạn nhỏ của chín tỉnh , thành : Ninh_Bình , Hải_Dương , Nghệ_An , Đà_Nẵng , TP.HCM , Gia_Lai , Đắc_Lắc , An_Giang và Đồng_Tháp đã ký đầu_tiên .
Bạn_Nguyễn_Hải_Như ( TP.HCM ) phát_biểu : “ Qua báo , đài , chúng_em đã cảm_nhận được nỗi đau_đớn về thể_xác và tinh_thần mà các nạn_nhân chất_độc da_cam , đặc_biệt là các bạn cùng trang_lứa , thật quá_sức chịu_đựng ” . Không_chỉ ký_tên , các bạn nhỏ còn góp tiền ủng_hộ tại_chỗ hơn 6 triệu đồng .
Được biết , các bạn nhỏ ở TP.HCM có_thể tham_gia ký_tên tại Nhà_Thiếu nhi TP và các nhà thiếu_nhi quận , huyện .
T.NGUY ÊN - T.NG . - ĐÔNG_HẢI - T.TH ÁI - N . YẾN - TỐ_OANH

﻿ Mỹ trục_xuất hai nhân_viên Iran tại LHQ
Đó là cảnh các cơ_sở_hạ_tầng , hệ_thống giao_thông NewYork , trong đó có cả những đường_hầm , đường xe_buýt và các ga tầu điện ngầm . Phái_bộ Iran tại LHQ đã bác_bỏ các cáo_buộc trên , cho_biết những người này chỉ chụp ảnh những địa_điểm thu_hút du_lịch của NewYork .
Adam_Ereli , người_phát_ngôn Bộ ngoại_giao Mỹ hôm_qua nói Mỹ đã quyết_định hành_động như trên sau khi FBI phát_hiện hai người này quay video những địa_điểm trên hồi tháng năm . Phía_Mỹ đã thông_báo cho Iran hai lần về những vụ chụp ảnh nhưng do Iran cương_quyết phủ_nhận nên " Mỹ không còn cách nào khác ngoài việc trục_xuất " . Stuart_Holliday , trợ_lý đặc sứ Mỹ tại LHQ nói các nhân_viên này " dính_líu tới những hoạt_động không phù_hợp với nhiệm_vụ của họ " , một_cách nói của giới ngoại_giao ám_chỉ hoạt_động tình_báo . Cả hai người này đã rời Mỹ tối 26-6 .
Iran đã bác_bỏ cáo_buộc này và khẳng_định nó phi_lý .
T . DANH ( theo AFP )

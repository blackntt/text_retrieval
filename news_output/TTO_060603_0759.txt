﻿ Nga : chánh công_tố_viện ra_đi
Chủ_tịch HĐLB Sergei_Mironov thông_báo HĐLB đã họp phiên toàn_thể để bỏ_phiếu sau khi tổng_thống Nga gửi tới thượng_viện đề_nghị này trên cơ_sở “ nguyện_vọng riêng của ông V . Ustinov ” .
Theo đánh_giá của cựu chánh_văn_phòng Công_tố_viện Issa_Kostoev , ông Ustinov đã có công đưa được các luật địa_phương vào guồng của luật liên_bang , một việc nếu_không kịp điều_chỉnh đã có_thể “ dẫn tới tan_rã đất_nước ” . Tuy_nhiên , ông Ustinov đã không ngăn_chặn được sự gia_tăng của tội_ác ở Nga .
Theo phó_chủ_tịch Đuma_Nga_Lyubov_Sliska , việc ra_đi của ông Ustinov có_thể liên_quan đến các hoạt_động chống tham_nhũng , công_việc mà Tổng_thống Nga V . Putin trong thông_điệp liên_bang 12-5-2006 từng khẳng_định sẽ là một trong những nhiệm_vụ chính của Nga hiện_nay .
D . V . ( Theo NR , RIA )

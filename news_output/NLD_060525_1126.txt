﻿ Smicer có_thể “ mất ” World_Cup
Smicer vẫn chưa được tập_luyện với đồng_đội Tiền_vệ Vladimir_Smicer có_thể không cùng đội_tuyển CH Séc tham_dự World_Cup do tình_trạng chấn_thương dai_dẳng mà anh gặp phải khi còn thi_đấu ở CLB .
Smicer đã đến kiểm_tra ở bệnh_viện Innsbruck ngày hôm_kia , nhưng không nhập_viện mà vẫn trở_lại trung_tâm huấn_luyện Seefeld , Áo nhưng chưa tập cùng đồng_đội .
HLV trưởng Karel_Bruckner đã khẳng_định ông sẽ chờ_đợi tiền_vệ số 7 này đến sát ngày khai_mạc giải .
Smicer đã ghi được 27 bàn trong 81 lần khoác_áo CH Séc , nhưng đã không thi_đấu cho CLB chủ_quản Bordeaux từ tháng 2 đến nay do chấn_thương gân keo .
World_Cup 2006 là lần đầu_tiên CH Séc tham_dự một giải đấu lớn tầm_cỡ thế_giới kể từ khi tách khỏi Tiệp_Khắc ( cũ ) , nhưng họ vẫn được đánh_giá cao . Bởi CH Séc đang đứng thứ_hai sau Brazil trên bảng xếp_hạng của FIFA tháng vừa_qua . Họ sẽ cùng bảng E với Italia , Ghana và Hoa_Kỳ .
Theo Tiền_Phong

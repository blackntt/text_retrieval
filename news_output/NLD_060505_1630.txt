﻿ 500 cảnh_sát nước_ngoài hoạt_động tại Đức
Khoảng 500 cảnh_sát nước_ngoài sẽ tới Đức trong chiến_dịch hỗ_trợ an_ninh nhằm ngăn_ngừa nguy_cơ bạo_lực do các hooligan gây ra ở World_Cup .
August_Hanning , quan_chức cấp cao Bộ Nội vụ Đức thông_báo sẽ có hơn 200 cảnh_sát thuộc 32 nước có đội_tuyển tham_dự VCK cùng khoảng 300 sĩ_quan từ các nước EU tham_gia bảo_vệ an_ninh tại Đức . Riêng_Chính phủ Anh đã cử tới 79 sĩ_quan trong đó có 44 người mang quân_phục tháp_tùng đoàn cổ_động_viên Anh gần 100 nghìn người tới Đức .
Theo Tiến_Phong

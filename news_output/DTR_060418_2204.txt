﻿ HLV phó Mai_Đức_Chung : “ ĐTVN bị buộc phải dùng sở_đoản ”
Mặc_dù vẫn tỏ ra hài_lòng với tinh_thần thi_đấu của cả đội , HLV phó Mai_Đức_Chung cho rằng ĐTVN đã phải dùng sở_đoản bóng bổng để đối_chọi với lối chơi pressing quyết_liệt của U23 Australia .
Ông Chung cho_biết : “ Trận này nếu ĐTVN chuẩn_bị sức_khoẻ tốt hơn thì hiệp 2 thậm_chí còn tốt hơn_nữa . Ở V-League cũng_như giải Hạng nhất chúng_ta chưa bao_giờ phải chịu áp_lực lớn như_thế_này , đối_thủ đã chơi áp sát rất dữ_dội .
Đối_thủ có chiều_cao rất tốt , họ đã nhận_định được điều đó và áp_dụng lối đá bóng bổng cũng_như presing toàn sân ” .
Ông khẳng_định , BHL rất hài_lòng về trận đấu này , đặc_biệt về tinh_thần và thái_độ của cầu_thủ . Tuy_nhiên , ông cũng lưu_ý một_số cầu_thủ , đặc_biệt là Hoàng_Thương , người đã bị thay ra cuối hiệp 1 , đã chơi không đúng sức .
Ông tiết_lộ các cầu_thủ đã không_thể triển_khai được chiến_thuật của BHL đề ra trước trận đấu . Ông nói : “ Đáng_lý ra cần phối_hợp bóng sệt , nhưng do đối_thủ bắt quyết_liệt và đá pressing do_đó chúng_ta không có cơ_hội để đá sệt . Các cầu_thủ bị bắt_buộc phải chơi bóng bổng ” .
Khi được hỏi về đối_thủ U23 Iran , ông cho_biết : “ Các đối_thủ của chúng_ta đều “ cao ” hơn chúng_ta , cả về tầm_vóc và về chuyên_môn . Khi các đội gặp nhau thì không lộ rõ sự khác_biệt , tuy_nhiên khi họ gặp chúng_ta thì sự chênh_lệch ấy lộ rất rõ ” .
Về trường_hợp của Mạnh_Dũng , ông Chung giải_thích : “ Đội_hình trong bóng_đá cần phải ổn_định . Cặp trung_vệ của chúng_ta đang đá tốt , do_đó không cần phải xáo_trộn , trừ khi bị chấn_thương ” .
Ông Chung cũng cho_biết , ĐTVN sẽ tiếp_tục chơi quyết_liệt để giành được kết_quả tốt trước ứng_cử_viên số 1 U23 Iran .
HLV Arnold tiếc_rẻ cho trận hòa với VN
( ảnh : Việt_Hưng )
HLV Graham_Arnold của U23 Australia :
“ Chúng_tôi hôm_nay chơi không được tốt như mong_đợi do chúng_tôi đã 8 tuần không thi_đấu , và một phần là do chúng_tôi đã mất quá nhiều sức_lực trong trận gặp Iran . Chúng_tôi đã kiểm_soát thế_trận trong hầu_hết thời_gian thi_đấu , nhưng đã không ghi được bàn thắng .
Đó là một điều rất đáng tiếc . Tuy_nhiên , các bạn cũng đã chơi rất hay , mỗi khi chúng_tôi có bóng các bạn đều áp sát rất nhanh . Tôi đặc_biệt ấn_tượng với tiền_đạo số 9 ( Công_Vinh ) , anh_ta rất nhanh , chạy chỗ rất khéo_léo và tỏ ra rất nguy_hiểm ” .
HLV Arnold cũng đã đưa ra nhận_xét về điểm mạnh và điểm yếu của ĐTVN . Ông cho rằng ĐTVN đã thực_sự làm ông bất_ngờ vì những bước_tiến mạnh_mẽ của mình . Tuy_nhiên , theo ông thì bài_toán thể_lực vẫn là điều mà BHL_ĐTVN cần phải chú_tâm . Hồng_Kỹ ( lược ghi )

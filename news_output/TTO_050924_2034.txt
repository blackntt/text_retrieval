﻿ Đừng đợi
Đừng đợi đến khi được yêu_thương rồi mới yêu_thương . Đừng đợi đến khi cô_đơn rồi mới nhìn thấy hết giá_trị của những người bạn . Đừng đợi một công_việc thật vừa_ý rồi mới bắt_đầu làm_việc . Đừng đợi đến khi có thật nhiều rồi mới sẻ_chia đôi_chút . Đừng đợi tới khi vấp_ngã rồi mới nhớ lại những lời khuyên . Đừng đợi đến khi có nhiều thời_gian rồi mới bắt_đầu một công_việc . Đừng đợi đến khi làm người khác buồn lòng rồi mới xin_lỗi . Đừng đợi , vì bạn không_thể biết mình sẽ phải đợi bao_lâu ... !
CHUNG_THANH_HUY ( st )

﻿ Gặp sĩ_quan Mỹ bị kết_án 115 năm tù vì hoà_bình cho Việt_Nam
& gt ; & gt ; Trao_Kỷ niệm chương “ Vì_Hoà bình hữu_nghị giữa các dân_tộc ” tặng ông Daniel_Ellsberg
Sau gần 40 năm , ở tuổi 75 , nhân_vật huyền_thoại này trở_lại VN trong chuyến du_lịch cùng các sinh_viên Mỹ . Đã có sự giới_thiệu của anh Michael_Ellsberg , con_trai ông , nhưng Daniel vẫn trả_lời một_cách dè_dặt mỗi khi tôi liên_lạc với ông_từ VN .
Có_lẽ đây chính là đặc_tính của một sĩ_quan từng làm_việc trong cơ_quan hoạch_định chính_sách chiến_tranh của Lầu_Năm_Góc và trải qua nhiều sóng_gió cuộc_đời .
Vậy nhưng khi gặp nhau tại Hà_Nội , Daniel lại cởi_mở hơn tôi tưởng . Không còn sự e_ngại , không cần những câu nói xã_giao , Daniel sẵn_sàng bộc_bạch với những tình_cảm chân_thành .
Thật bất_ngờ , Daniel thậm_chí còn biểu_diễn màn ảo_thuật bằng những chiếc khăn màu cho bạn_bè VN xem với sự nhanh_nhẹn đến khó tin ở tuổi 75 . Chính những màn ảo_thuật này đã giúp ông làm_quen với nhiều bạn nhỏ khi đang làm nhiệm_vụ ở VN .
Quyết_định gây chấn_động
Nhận bằng Tiến_sĩ kinh_tế ĐH Harvard năm 1962 , nhưng cuộc_đời lại đưa Daniel đến với Bộ Quốc phòng năm 1964 và ngay sau đó ông được phái sang chiến_trường VN .
Sau hơn 3 năm ở VN , làm_việc cho cố_vấn đặc_biệt của Bộ_trưởng Quốc_phòng và Đại_sứ_quán Mỹ ở Sài_Gòn , năm 1967 , Daniel lúc đó 36 tuổi , trở_về Mỹ . Dan được phân vào nhóm nghiên_cứu tối_mật về hoạch_định chính_sách đối_với VN của Bộ_trưởng Quốc_phòng McNamara .
Hỏi lý_do nào khiến ông đi đến quyết_định gây chấn_động nước Mỹ năm 1971 ? Daniel cho_biết thực_ra ông đã bắt_đầu copy các tài_liệu mật từ năm 1969 .
Ở vị_trí của mình , Daniel biết rằng Tổng_thống Nixon muốn “ leo_thang ” trong cuộc_chiến ở VN . Tuy_nhiên , chính_quyền Nixon lại dùng những lời nói_dối để bao_che cho hành_động leo_thang , kéo_dài chiến_tranh . Daniel muốn các nhà lịch_sử , báo_chí và công_luận hiểu rõ sự dối_trá này .
Mặt_khác Daniel cũng nhận_thức được những gì mình và gia_đình có_thể phải hứng_chịu sau đó . Khi đưa ra quyết_định này , Daniel cho_biết , ông đã sẵn_sàng vào tù .
Đây là một quyết_định khó_khăn với nhiều sự giằng_xé nội_tâm , nhưng cuối_cùng phần yêu_mến hoà_bình , mong_muốn chiến_tranh kết_thúc sớm , tôn_trọng công_lý , sự_thật trong con_người Daniel đã chiến_thắng .
Sau khi trao tài_liệu mật cho tờ The_New_York_Times đầu_tiên , Daniel và vợ , bà Patricia , bị chính_quyền tìm mọi cách ngăn_cản để các tờ báo khác không có được tài_liệu .
Trong vòng 2 tuần , với sự giúp_đỡ của bạn_bè và những người hoạt_động trong phong_trào phản_chiến , tài_liệu đã được phân_phát cho 19 tờ báo lớn ở Mỹ .
Daniel nhớ lại thời_điểm đó FBI từng miêu_tả vợ_chồng ông như_là mục_tiêu săn_đuổi lớn nhất . Một khoảng thời_gian rất căng_thẳng , nhưng Daniel được sự ủng_hộ của vợ_con , thậm_chí họ còn giúp ông photo và phân_phát tài_liệu .
Lúc đó con của Daniel còn nhỏ và thực_sự không hiểu gì , nhưng Daniel muốn con mình được biết những điều mà người cha đã làm . Ngay sau đó Daniel bị bắt , bị toà_án cáo_buộc tới 12 tội nghiêm_trọng với bản_án 115 năm tù . Vợ của ông_bà Patricia cũng bị kết_án .
Tuy_nhiên , năm 1973 , bản_án 115 tù của Daniel đã bị bãi_bỏ cùng_với việc Tổng_thống Nixon và một_số quan_chức Nhà_Trắng bị điều_tra . Cậu con_trai thứ của Daniel là Michael sinh_ra sau khi chiến_tranh VN kết_thúc .
Dù_vậy Michael tâm_sự với tôi rằng anh hiểu rõ những gì cha mình đã làm và luôn tìm cách bù_đắp cho những gì mà ông từng phải gánh_chịu . Các con của Daniel cũng_như nhiều người Mỹ yêu_chuộng hoà_bình và công_lý luôn xem ông như một huyền_thoại .
Với câu_hỏi liệu đã bao_giờ ông cảm_thấy hối_hận về việc tiết_lộ tài_liệu mật , Daniel tâm_sự rằng ông chỉ hối_hận là đã không làm điều này sớm hơn . Năm 1964 - 1965 khi làm_việc cho trợ_lý đặc_biệt của Bộ_trưởng Quốc_phòng Mỹ tại VN , Daniel được tiếp_cận với nhiều tài_liệu mật và nếu tiết_lộ ngay đã có_thể làm thay_đổi một điều gì đó trong cuộc chiến_tranh này .
Tòa_án khi xét_xử Daniel đã cáo_buộc hành_động của ông là phản_quốc , nhưng theo Daniel việc phản_đối chiến_tranh , mang lại hoà_bình , nói rõ sự_thật với người_dân mới thể_hiện rõ lòng yêu nước .
Người_dân Mỹ có sự phán_xét công_bằng . Khi_Daniel bị kết_án , hơn 25.000 người đã quyên_góp ủng_hộ ông .
Tuy_nhiên , cũng có người Mỹ cho rằng Daniel đã hành_động sai , đặc_biệt vì trước đó ông đã tuyên_thệ giữ bí_mật các tài_liệu . Daniel giải_thích nếu hối_hận ông đã không tiếp_tục có những hành_động phản_đối chiến_tranh đến ngày_nay .
Trở_lại chiến_trường xưa
Trở_lại VN , Daniel mới hiểu rằng dù chiến_tranh qua đi đã lâu , người_dân vẫn tiếp_tục phải hứng_chịu những hậu_quả của nó như bom_mìn còn sót lại .
Daniel còn được trực_tiếp chứng_kiến những đổi_thay sau hơn 30 năm chấm_dứt chiến_tranh , những khuôn_mặt rạng ngời của giới trẻ trên các đường_phố ngập_tràn xe môtô , những người phụ_nữ chân_lấm_tay_bùn trên đồng_ruộng , các em thơ vẫn trên lưng trâu và đặc_biệt là sự thân_thiện mà bạn_bè VN dành cho ông cũng_như các thành_viên trong đoàn ...
Tất_cả khiến Daniel thực_sự xúc_động và càng hiểu rõ rằng mình đã hành_động đúng trong quá_khứ . Trên mảnh đất này , Daniel đã từng cầm súng , nhưng sau đó lại bất_chấp mọi hậu_quả để tiết_lộ tài_liệu mật với hi_vọng chiến_tranh sớm kết_thúc .
Từ sau chiến_tranh VN đến nay , Daniel đã viết rất nhiều sách , báo , thường_xuyên tham_dự các buổi nói_chuyện liên_quan đến chiến_tranh VN ở các trường đại_học trên khắp nước Mỹ .
Những tiết_lộ mới của ông gần đây như trong cuốn “ Secrets : A_Memoir of Vietnam and the Pentagon_Papers “ ( Các bí_mật : Hồi_ký về VN và các tài_liệu Lầu_Năm_Góc ) trở_thành một trong những cuốn sách bán_chạy nhất ở Mỹ .
Tất_cả những hoạt_động đó , theo Daniel , là vì hoà_bình , công_lý và sự_thật với hi_vọng những điều như chiến_tranh VN sẽ không_bao_giờ xảy_ra nữa .
Trang_web mà cậu con_trai Michael lập riêng cho ông luôn nhận được sự quan_tâm của nhiều người Mỹ , đặc_biệt là sinh_viên . Mặc_dù đã 75 tuổi , nhưng lịch làm_việc của Daniel dày_đặc với những buổi nói_chuyện trên khắp nước Mỹ . Hiếm khi Daniel có_mặt ở nhà mình tại thành_phố Kensington , bang California .
Theo Tiền phong

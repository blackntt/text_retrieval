﻿ Trên 88% du_khách Nhật mê món ăn Việt
Theo kết_quả nghiên_cứu này , mua_sắm tại VN cũng được du_khách Nhật khá thích_thú , với kết_quả 82% du_khách khẳng_định có đi mua_sắm khi đến VN .
Về mục_đích của khách du_lịch Nhật đến VN , có đến 69% cho_biết đến VN với mục_đích nghỉ_ngơi và 17,6% với mục_đích thương_mại - đầu_tư , so với các nước trong khu_vực là 66% và 11,7% . Tuy_nhiên , về xu_hướng khách du_lịch Nhật trở_lại , chỉ có 3,3% du_khách khẳng_định trở_lại VN lần 2 và 5,7% trở_lại lần 3 , so với các nước trong khu_vực là 13,8% và 25,7% .
X.TO ÀN

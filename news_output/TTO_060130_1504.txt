﻿ Lý_An đoạt giải_thưởng của Hiệp_hội đạo_diễn
Trong 57 năm qua , chỉ có 6 lần một đạo_diễn được Hiệp_hội đạo_diễn Mỹ tôn_vinh mà sau đó không giành tượng vàng Oscar .
Để nhận được giải_thưởng này , ông Lý_An ( đạo_diễn bộ phim cao_bồi đồng_tính Brokeback_Mountain ) đã vượt qua các tên_tuổi sừng_sỏ như Steven_Spielberg ( đạo_diễn Munich ) , George_Clooney ( Good_Night and Good_Luck ) , Paul_Haggis ( Crash ) và Bennette_Miller ( Capote ) .
Vào năm 2000 , với bộ phim kiếm_hiệp Ngọa hổ tàng long , ông Lý_An đã được trao giải Thành_tựu đạt được nhưng sau đó ông lại không nhận được giải Oscar ( lần đó Oscar đạo_diễn xuất_sắc nhất thuộc về Steven_Soderbergh ) .
Trong quá_khứ Martin_Scorcese , Steven_Spielberg , Francis_Ford_Coppola , Stanley_Kubrick , Woody_Allen , Billy_Wilder , Alfred_Hitchcock và John_Ford là những người đã nhận được giải Thành_tựu đạt được do Hiệp_hội đạo_diễn trao_tặng .
Đ . K . L ( Theo AFP )

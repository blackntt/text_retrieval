﻿ Cathay_Pacific được nhận danh_hiệu hãng hàng_không của năm
Hãng hàng_không Cathay_Pacific của Hong_Kong đã được nhận danh_hiệu hãng hàng_không của năm và danh_hiệu hãng hàng_không tốt nhất ở châu Á .
Hãng hàng_không British_Airways ( Anh ) giành được danh_hiệu hãng hàng_không tốt nhất ở đông Âu và giải_thưởng hãng hàng_không xuyên Đại_Tây_Dương tốt nhất .
Hãng hàng_không Continential_Airlines ( Mỹ ) năm thứ_ba liên_tiếp được nhận danh_hiệu hãng hàng_không tốt nhất ở Bắc_Mỹ và năm thứ_tư liên_tiếp được nhận danh_hiệu hãng hàng_không có dịch_vụ vé hạng sang tốt nhất .
Còn hãng hàng_không American_Airlines ( Mỹ ) được nhận danh_hiệu hãng hàng_không cho sự lựa_chọn đối_với các hành_khách bị tàn_tật , trong khi sân_bay Changi của Singapore vẫn tiếp_tục là sân_bay tốt nhất .
OAG nổi_tiếng nhất với cơ_sở_dữ_liệu các hãng hàng_không , nắm giữ các chi_tiết chuyến bay của 1.000 hãng hàng_không và hơn 3.500 sân_bay trên thế_giới .
Theo TTXVN

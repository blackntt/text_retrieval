﻿ Bị phạt tiền , 50 năm sau mới trả
Muộn còn hơn không - đó là quan_điểm của cụ John_Gedge khi quyết_tâm gửi trả bằng_được Ban quản_lý công_viên Fairmount số tiền 15 USD , cho tấm vé phạt của 52 năm trước .
Ngày 15 tháng 7 năm 1954 , John_Gedge lặn_lội từ nước Anh sang thăm thành_phố Philadelphia và bị bảo_vệ công_viên Fairmount viết biên_lai phạt do chạy xe quá tốc_độ .
Khoản tiền phạt chỉ vẻn_vẹn 5 bảng Anh , bằng 14 USD lúc đó và tương_đương 9 USD bây_giờ . Không may , tấm biên_lai đã bị bỏ quên trong chiếc áo_khoác cũ_kĩ hơn nửa thế_kỉ , cho_đến mãi gần đây cụ Gedge mới vô_tình phát_hiện ra .
Cụ Gedge hiện đã 84 tuổi , sống tằn_tiện bằng lương hưu tại một viện dưỡng_lão thuộc miền tây vùng Sussex nước Anh . Trả xong món tiền phạt , cụ thấy lòng mình thật thanh_thản .
Đăng_Linh_Theo AP

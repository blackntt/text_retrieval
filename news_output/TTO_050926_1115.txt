﻿ Thêm một phụ_nữ Indonesia chết do nghi nhiễm cúm_gia_cầm
Sardikin_Giriputro , phó_giám_đốc bệnh_viện trên - bệnh_viện mà theo chỉ_định của chính_phủ chuyên điều_trị cho các trường_hợp bệnh_nhân nghi nhiễm cúm_gia_cầm - cho_biết bệnh_nhân này chết vào sáng_sớm hôm_nay 26-9 . Người này được nhập_viện hôm thứ_năm 22-9 .
Các xét_nghiệm ban_đầu cho thấy người phụ_nữ này đã nhiễm cúm_gia_cầm , ông nói . “ Chúng_tôi đã xét_nghiệm huyết_thanh của bệnh_nhân và kết_quả cho thấy bệnh_nhân đã bị nhiễm cúm_gia_cầm ” , Giriputro nói . Ông này thêm rằng các nhân_viên y_tế sẽ trích mô phổi để xác_định kết_quả cuối_cùng .
Ông cũng cho_biết một bé trai 8 tuổi đã được công_bố nhiễm cúm_gia_cầm hồi tuần trước đã được xác_nhận sau khi được các xét_nghiệm .
Cúm_gia_cầm đã giết 4 người tại Indonesia , quốc_gia đông dân thứ_tư trên thế_giới , chưa tính đến người phụ_nữ mới tử_vong hôm thứ_hai .
T.VY ( Theo Reuters )

﻿ Con tàu bị chìm ngoài khơi Bahrain không có giấy_phép
Theo người_phát_ngôn Bộ Nội vụ Col_Tariq al-Hassan , con tàu này đã đăng_ký là tàu đánh_cá . Trên thực_tế , nó đã hoạt_động như một nhà_hàng nổi .
Một đại_diện của một công_ty đã thuê con tàu để đãi tiệc nhân_viên khẳng_định công_ty không chịu trách_nhiệm về tai_nạn này .
Thuyền_trưởng con tàu đang bị các công_tố_viên địa_phương thẩm_vấn . Họ cho_biết ông này không có giấy_phép được chở khách .
Ít_nhất 57 người thiệt_mạng , trong đó có 21 người Ấn Độ và 12 người Anh , 5 người Nam_Phi , người Thái_Lan , Đức , Hàn_Quốc , Singapore … Ít_nhất 68 người được giải_cứu trong khi 1 người vẫn còn mất_tích . Đa_số những người có_mặt trên con tàu định_mệnh trên là công_nhân của công_ty Murray và Roberts có trụ_sở tại Nam_Phi - công_ty thuê con tàu nói trên để làm lễ kỷ_niệm nhân_dịp hoàn_thành tòa tháp Trung_tâm thương_mại thế_giới tại Bahrain .
Kiểm_tra xác con tàu bị chìm - Ảnh : AFP
Vớt xác những nạn_nhân thiệt_mạng - Ảnh : AP Chủ tàu cho rằng tàu chìm là do chở quá trọng_tải và đổ lỗi cho công_ty du_lịch vận_hành con tàu , trong khi giám_đốc Simon_Hill , người sống_sót sau thảm_họa , nói công_ty đã đề_nghị thuyền_trưởng đừng ra khơi nếu con tàu không an_toàn .
Nhà_chức_trách tại Bahrain nói hiện còn quá sớm để nói đến nguyên_nhân khiến tàu bị chìm .
T.VY ( Theo BBC )

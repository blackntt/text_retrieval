﻿ Lễ khai_mạc LHP_VN lần thứ 14 : Đậm_đà sắc_màu Tây_Nguyên
Lễ khai_mạc gồm 2 phần : ngoài_trời tổ_chức các tiết_mục ca_múa đặc_sắc của dân_tộc Êđê và lễ mời rượu_cần các đại_biểu theo nghi_thức thác đổ của dân_tộc Ê đê . Và phần nghi_lễ chào cờ , trong hội_trường . Lá_cờ Liên_Hoan_Phim_Việt_Nam 14 được Hồng_Ánh và Hồ_Bài_Bình – hai diễn_viên xuất_sắc của LHP lần thứ 13 kéo lên .
Mặc_dù lễ khai_mạc được trực_tiếp truyền_hình trên sóng truyền_hình tỉnh Đăklăk , nhưng khán_giả tập_trung rất đông bên ngoài , chăm_chú theo_dõi hai màn_hình lớn đựơc đặt trước cửa_nhà văn_hóa trung_tâm .
Nhân_dịp TP Buôn_Ma_Thuột 100 năm hình_thành và phát_triển và dây cũng là lấn đầu_tiên LHP_VI ệt Nam được tổ_chức Tây_Nguyên , bộ phim_tài_liệu “ Điện_ảnh với Tây_Nguyên ” ( đạo_diễn Nguyễn_Thất ) được chiếu trong buổi lễ khai_mạc . Cùng_với những tiết_mục đặc_sắc nhất của đoàn ca_múa nhạc dân_tộc Đăklăk , tỉnh chủ nhà đã tạo ra một không_khí LHP rất riêng , đậm_đà màu_sắc núi_rừng Tây_Nguyên .
Ca_sĩ Y_Moan , diễn_viên điện_ảnh Minh_Thư , nhóm Trio 666 cũng góp_mặt trong chương_trình văn_nghệ khai_mạc .
Sáng 5-11 , sẽ diễn ra buổi toạ_đàm với khán_giả về phim VN tại khách_sạn Cao_Nguyên . Các rạp vẫn tiếp_tục chiếu_phim phục_vụ nhân_dân thành_phố Buôn_Ma_Thuột cho_đến hết ngày 8-11-2004 .
Tin , ảnh : H . Y

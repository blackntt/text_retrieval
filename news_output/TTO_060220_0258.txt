﻿ ADB giúp tăng_cường quan_hệ kinh_tế Đông Á và Nam Á
Nghiên_cứu của OREI sẽ ước_lượng các tác_dụng có_thể có_của việc tự_do hóa mậu_dịch trong nhiều hoàn_cảnh khác_nhau , giúp ADB xác_định các lĩnh_vực có_thể được hưởng lợi nhất từ việc hợp_tác , và xác_định việc chọn_lựa chính_sách ở vùng , tiểu_vùng ...
Các nghiên_cứu riêng_rẽ cũng sẽ được thực_hiện đối_với mức_độ hội_nhập hiện_tại trong việc buôn_bán hàng_hóa và đầu_tư , buôn_bán dịch_vụ , cơ_sở_hạ_tầng và việc tạo thuận_lợi cho kinh_doanh .
Q . HƯƠNG ( Theo ADB.org )

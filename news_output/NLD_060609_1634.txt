﻿ Gas_Petrolimex bán tiếp 62.500 cổ_phần
Theo Trung tâm_giao dịch chứng_khoán Hà_Nội , Công_ty_cổ_phần Gas_Petrolimex sẽ bán tiếp 62.500 cổ_phần chưa phân_phối hết cho các tổ_chức và cá_nhân đã đăng_ký và tham_gia đấu_giá cổ_phần của Công_ty .
Số cổ_phần được phân_phối sẽ xét theo thứ_tự trả_giá từ cao xuống thấp với mức giá nhà_đầu_tư đã đặt mua tại cuộc bán_đấu_giá cổ_phần ngày 10-5-2006 Quy_chế bán_đấu_giá cổ_phần ra bên ngoài của Công_ty Cổ_phần Gas_Petrolimex . Kết_thúc phiên đấu_giá ngày 10-5 , toàn_bộ 4.050.000 cổ_phần Gas_Petrolimex có_giá khởi_điểm là 17.500 đồng/cổ phần , đã được các nhà_đầu_tư đặt mua với giá trúng_thầu bình_quân là 56.080 đồng/cổ phần.Tuy nhiên một_số nhà_đầu_tư trúng đấu_giá nhưng từ_chối mua .
Theo TTXVN

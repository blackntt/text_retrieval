﻿ Xe nhốt người trong khoang hành_lý bị đình_chỉ hoạt_động
Ôtô khách mang biển_số 92K-4221 , do Phạm_Công_Trí ( 33 tuổi ) - lái_xe của Hợp_tác_xã vận_tải kinh_doanh tổng_hợp Tam_Kỳ điều_khiển , chạy tuyến Quảng_Nam - TP.HCM đã bị công_an Quảng_Ngãi phát_hiện nhốt 22 hành_khách trong khoang hành_lý vào lúc 11g ngày 7-2 .
Ban chỉ_đạo an_toàn giao_thông Quảng_Nam cũng đang thụ_lý điều_tra để xử_lý trường_hợp xe_khách 92K-4110 cũng của đơn_vị này bị hành_khách gửi đơn tố_cáo bắt_chẹt hành_khách , bỏ khách giữa đường .
HOÀI_NHÂN

﻿ Mổ hậu_môn " giải_cứu " … bóng_đèn
Fateh_Mohammad , phạm_nhân trong ngục Multan , Pakistan , một đêm chợt thức_giấc với chiếc bóng_đèn thủy_tinh … nằm lọt_thỏm trong hậu_môn .
Thề trước thánh Allah , Mohammad không hề biết vì_sao , và bằng cách nào mà vị khách “ kì_cục ” kia lại có_thể chu_du xuống tận nơi thầm_kín nhất trong cơ_thể mình .
“ Tôi bừng tỉnh_giấc vì chợt thấy đau quặn ở phần bụng_dưới . Khi được đưa đến bệnh_viện và chẩn_đoán nguyên_nhân , tôi “ choáng ” nặng . Không hiểu ai đã trêu ác như_vậy , nhân_viên trại_giam hay tù_nhân cùng phòng ” .
Các bác_sĩ bệnh_viện Nishtar sau đó đã tiến_hành phẫu_thuật để “ giải_cứu ” chiếc bóng_đèn . “ Rất may là chúng_tôi đã gắp được nó ra nguyên_vẹn . Nếu bị vỡ , chắc_chắn ca mổ sẽ rất phức_tạp và đe_dọa tính_mạng bệnh_nhân ” – theo lời kể của tiến_sĩ Farrukh_Aftab .
Fateh_Mohammad , người đàn_ông 40 tuổi đang chịu án 4 năm tù_giam vì tội làm rượu - hành_động bị coi là bất_hợp_pháp theo đạo Hồi . Sau phẫu_thuật , hiện_tại anh chỉ mới uống được nước và còn rất yếu .
Người_ta đoán đã có kẻ chơi ác chuốc cho anh say rượu rồi nhét bóng_đèn vào khi anh vẫn còn mê_man .
Hải_Minh_Theo_Reuters

﻿ Hàn_Quốc : sinh con được thưởng
Hàn_Quốc đang xem_xét các biện_pháp mới để giải_quyết một_số lý_do mà những cặp vợ_chồng đưa ra vì không muốn có con , trong đó có chi_phí học_tập cao và không ai giữ trẻ . Tháng tư vừa_qua , Seocheon - một hạt có dân_số giảm từ 150.000 người trong thập_niên 1960 xuống còn 65.000 người hiện_nay - bắt_đầu treo giải_thưởng cho các cặp vợ_chồng sinh con : 300 USD cho đứa con_thứ nhất hoặc thứ_hai , 800 USD cho đứa con_thứ ba . Lee_Kwon_Hee , phó_trưởng hạt , nói : “ Một_số làng đã không nghe tiếng trẻ_em khóc từ 18 năm nay ” .
Q . HƯƠNG ( Theo TST )

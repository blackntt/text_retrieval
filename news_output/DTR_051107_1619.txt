﻿ Victoria_Beckham - Hạnh_phúc và bình_yên
Năm hết Tến đến , tạp_chí thời_trang Elle chuẩn_bị một_số câu_hỏi cho các sao khi Xuân chuẩn_bị về . Người đầu_tiên được chọn là Victoria_Beckham .
Không có những câu_hỏi shock và cũng không liên_quan nhiều đến những scandal mới_đây nhưng qua phần trả_lời của Posh có_thể cảm_nhận được cô vẫn đang hạnh_phúc và bình_yên .
Cô muốn được tặng gì trong đêm Giáng_sinh ?
Một bộ đèn_cầy ướp hương hoa_hồng của nhà Jo_Malone và Dyptique .
Và trong đêm ấy cô sẽ làm_gì ?
Ở nhà với gia_đình . Không cần yến_tiệc linh_đình , có David và 3 nhóc_tì là đầm_ấm rồi . Với tôi qua nhiêu chuyện xảy_ra thì hạnh_phúc mang lại chính là bọn trẻ . Nhìn chúng chơi_đùa , bày trò đánh trận là dường_như mọi thứ xung_quanh tan biến hết .
Được xem là một người cực_kỳ sành mode , vậy trong năm 2005 này cô có mua được gì ưng_ý nhất không ?
Có đấy , đó là một chiếc túi_xách của nhà Hermes , tôi mua nó với giá gần 5.000 USD . Tuy đắt nhưng nó rất đẹp và theo tôi nó có một vẻ đẹp cổ_điển mà điều đó thì chẳng bao_giờ bị xem là sến cả .
Trong năm nay lúc_nào cô thấy mình ăn_mặc đẹp nhất ?
Đó là khi đi dự buổi party Tie & amp ; Tiara_Ball của Elton_John . Tôi đã mặc chiếc đầm trắng - xanh của nhà tạo mốt Cavalli ( cũng là bạn_thân của Posh ) . Đứng trước tủ quần_áo mà tôi chẳng biết chọn bộ nào cho ưng_ý và cuối_cùng quyết_định chọn nó và rút cuộc nó không làm tôi thất_vọng .
Vic cùng nhà_tạo_mẫu Cavalli
Và nhân_vật nào làm cô ấn_tượng nhất trong năm ?
Đó là vua Joan_Carlos . Ông đã mời vợ_chồng tôi đến dùng cơm trưa . Phải nói là ông ấy rất tuyệt và cuốn_hút .
Tham_dự nhiều party đình_đám hẳn cô cũng phải có ấn_tượng cho một đêm vui nhất ?
Đó là đêm mừng David tròn 30 tuổi . Nó chẳng xa_hoa gì đâu , chỉ tổ_chức trong quán bar Buddah ở Madrid , bọn tôi mời bạn_bè từ Anh quốc , gia_đình và đồng_đội của chồng tôi ở Real . Bữa đó âm_nhạc hay hết_chỗ_nói . Bon tôi vui_vẻ đến khuya và đến giờ trong tôi vẫn còn dư_vị của nó .
Cô thích nhất bộ phim_truyền_hình nào ?
Sex and the city . Tôi vừa phát_hiện rằng tôi đang cực thích Sex and the city . Những câu_chuyện quay xung_quanh cuộc_đời của những người phụ_nữ có gia_đình làm cho tôi có thêm được nhiều kinh_nghiệm sống . Nói_chung là tôi thích nó .
Vậy còn về âm_nhạc ?
Chắc_chắn đó sẽ là The_Emancipation of Mimi của Mariah_Carey . Tôi chưa bao_giờ là fan của Mariah nhưng thật_sự album mới nhất của cô ấy làm tôi rất ấn_tượng .
Sách thì_sao ? Có thích cuốn nào nhất ?
Hôm sinh_nhật tôi , một người bạn đã tặng tôi cuốn A gun for hire của Helmut_Newton gồm những bức ảnh chụp thời_trang nổi_tiếng nhất của ông . Đó là một cuốn sách xem xong thế_nào bạn cũng sẽ cảm_nhận được đầy_ắp tinh_thần bên trong .
Câu_hỏi cuối , bộ phim_nhựa cô thích nhất trong năm nay có tên là gì ?
Herbie : Full loaded . Cả nhà tôi đã kéo nhau đi xem bộ phim này . Và bây_giờ mỗi khi ra đường và trông thấy chiếc con bọ của hãng VW là bọn trẻ cứ hét toáng lên " Nhìn kìa , Herbie đấy ! " . Theo Vietnamnet

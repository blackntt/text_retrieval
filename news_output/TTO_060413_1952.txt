﻿ 10 bí_quyết thoả_thuận lương trong cuộc phỏng_vấn tuyển_dụng
Nghe có_vẻ hơi lạ_lẫm với bạn , nhưng điều này là cần_thiết . Trong thực_tế , đã có nhiều ứng_viên đi đến buổi phỏng_vấn trong khi vẫn còn mơ_hồ về những con_số ảnh_hưởng đến tương_lai của mình . Biết rõ quyền_lợi của mình , bạn sẽ có thêm nhiều lý_lẽ và tự_tin hơn để nói_chuyện tiền_bạc .
2 . Biết rõ giá_trị của mình
Bạn có bao_giờ thử nghĩ xem mình giá_trị đến mức nào . Một lần , hãy viết ra ra giấy những mặt ưu_điểm của bạn : kỹ_năng , năng_lực , kiến_thức , kinh_nghiệm , khả_năng nổi_bật … Động_tác tưởng_chừng đơn_giản này sẽ giúp bạn có cái nhìn tích_cực hơn về bản_thân mình . Nhờ thế , khi các nhà tuyển_dụng cố làm_bạn thiếu tự_tin về giá_trị của mình trong lúc thương_lượng , bạn vẫn có cơ_sở quật_ngã những lời nhận_xét cố làm_bạn mất phương_hướng .
3 . Đừng hé lộ mức lương hiện_tại
Tránh tiết_lộ vội_vàng với nhà quản_lý tương_lai về mức lương hiện_tại hoặc đưa ra đề_nghị quá sớm về con_số bạn mong_muốn kiếm được trong tương_lai . Khi viết thư xin việc hay resume , bạn tránh đưa các con_số vào , thay vào đó hãy ghi là “ thương_lượng ” . Tại_sao cần phải cẩn_trọng như_thế ? Bởi_vì , một_khi bạn phô_bày những thông_tin cực_kỳ nhạy_cảm này bạn sẽ có nguy_cơ bị thiệt_thòi trong quá_trình thương_lượng về lương . Nhà tuyển_dụng có_thể căn_cứ vào đó trả cho bạn số tiền không tương_đương với mức họ dự_định .
4 . Tránh đề_xuất mức lương mong_muốn quá chi_tiết , cụ_thể
Bí_quyết này sẽ giúp bạn tận_dụng và tìm_kiếm thêm nhiều lợi_thế hơn_nữa . Nếu bạn đưa ra thông_tin chi_tiết về mức lương , bạn sẽ có nguy_cơ hưởng ít hơn số tiền mà công_ty dự_định chi_trả cho bạn . Thêm vào đó , nếu bạn chọn mức lương không phù_hợp với năng_lực bản_thân bạn sẽ tự mang thêm rắc_rối vào mình . Vì_thế nên linh_động và nhạy_bén , tùy theo diễn_biến câu_chuyện mà phất cờ .
5 . Đừng dè_dặt khi nói_chuyện lương_bổng
Chẳng có gì xấu_hổ khi định_giá sức_lao_động của mình cả . Để bảo_đảm lợi_nhuận , nhà tuyển_dụng sẽ cố ép bạn xuống mức tối_thiểu , sự dè_dặt của bạn sẽ tạo thêm cơ_hội cho họ . Hãy tự_tin khi và thẳng_thắn đòi_hỏi quyền_lợi cho chính mình , bạn bán sức_lao_động để kiếm cơm chứ không ngồi chơi chờ hưởng lợi .
6 . Tận_dụng thời_cơ
Lúc_nào là thời_điểm quan_trọng . Một qui_luật cốt_yếu cần ghi_nhớ là đừng vội_vàng nhanh_nhảu_đoảng . Tuyệt_đối không nên thoả_thuận qua điện_thoại , tốt nhất_là mặt_đối_mặt . Nếu_như cái giá họ đưa ra không làm_bạn vừa_ý thì cố giữ bình_tĩnh và thể_hiện sự không hài_lòng của mình một_cách khéo_léo rõ_ràng . Như_thế , bạn có_thể tác_động nhà tuyển_dụng nâng cao mức lương . Nếu tình_thế có_vẻ khó_khăn , người phỏng_vấn không_thể quyết_định nhanh_chóng , hãy đề_nghị một cuộc hẹn khác . Hãy tỏ ra nhiệt_tình và sẵn_sàng hợp_tác .
7 . Lưu_tâm đến các quyền_lợi khác
Nếu nhà tuyển_dụng dứt_khoát không thay_đổi con_số đã đưa ra , bạn có_thể yêu_cầu được biết về những khoản khác như : tiền trợ_cấp , tiền thưởng , lợi_ích từ lợi_nhuận , tiền thưởng cho thành_tích vượt_trội … Bạn yêu_cầu nhà tuyển_dụng cam_kết về thời_hạn tăng lương , các khoản thu_nhập khác được hưởng trong hợp_đồng rõ_ràng . Nếu nhà tuyển_dụng tỏ ra cứng_rắn , bạn có_thể hỏi họ về các hình_thức làm_việc khác như bán thời_gian hay tư_vấn …
8 . Định rõ giới_hạn chấp_nhận được
Đây là điều bạn cần phải luôn lưu_tâm và suy_nghĩ nghiêm_túc tường_tận trước mỗi buổi phỏng_vấn . Hãy vạch ra giới_hạn rõ_ràng , con_số tối_đa mà bạn có_thể đạt được , con_số tối_thiểu mà bạn chấp_nhận được .
9 . Đừng quên những bài_học quá_khứ
Nhớ lại những lần thương_lượng về lương trước_đây mà bạn đã trải qua , cho_dù đó là những sai_lầm thì vẫn là những bài_học quý_giá giúp bạn thêm kinh_nghiệm trong “ cuộc_chiến ” giành quyền_lợi cho chính mình .
10 . Tiền không phải là tất_cả
Chúng_ta luôn muốn được trả công xứng_đáng , nhưng nên nhớ tiền không phải là tất_cả . Tiền_lương cần nhưng đừng để nó chi_phối mọi hành_động của bạn . Đừng để đồng_tiền ép bạn vào những công_việc không yêu_thích hoặc từ_bỏ những cơ_hội lớn của tương_lai .
HR Vietnam ( Theo Ezinerarticle )

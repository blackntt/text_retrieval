﻿ Ngô_Kha - ngụ_ngôn của một thế_hệ
( Nhà_xuất_bản Thuận_Hóa và Công_ty văn_hóa Phương_Nam )
TT - Một tập sách đầy ý_nghĩa nhân Ngày truyền_thống sinh_viên - học_sinh 9-1 . Bên_cạnh tập thơ Hoa cô_độc , hai trường_ca Ngụ_ngôn của người đãng_trí , Trường_ca Hòa_bình ... và những tác_phẩm lần đầu được công_bố , bạn_đọc sẽ còn được tiếp_cận với một con_người qua các bài viết của bằng_hữu , văn_nghệ_sĩ ... - những người cùng thời xuống_đường tranh_đấu cho hòa_bình trên đất Huế .
Nhắc đến phong_trào sinh_viên - học_sinh Huế và các đô_thị miền Nam trước 1975 , Ngô_Kha là một tên_tuổi lớn . Một trí_thức dấn_thân , một nhà_thơ tranh_đấu , một con_người hành_động . Không_chỉ trong cuộc_đời , những câu_thơ của anh cũng là một sự quyết_liệt đến tận_cùng để bảo_vệ khát_vọng và niềm_tin của mình , của thế_hệ mình và của dân_tộc mình .
Ngày 30-1-1973 , khi Hiệp_định Paris chưa ráo mực , anh bị nhà cầm_quyền bấy_giờ bắt đi và từ đó anh mãi_mãi không_bao_giờ trở_về ...
L . Đ . DỤC

﻿ Được sử_dụng mẫu phiếu đăng_ký dự thi từ Internet
Với mẫu phiếu download và in ra từ mạng Internet , TS vẫn phải khai và có dấu xác_nhận đầy_đủ theo qui_định . Việc này mới chỉ tạo thuận_lợi cho TS được một phần nhỏ , trong trường_hợp TS cần khai lại phiếu nhiều lần . Còn TS vẫn phải mua bộ hồ_sơ ĐKDT in sẵn có đầy_đủ phong_bì vì trên đó có nhiều thông_tin cần_thiết khác .
Trên hồ_sơ ĐKDT do các sở GD - ĐT trong cả nước in_ấn và phát_hành năm nay vẫn có dấu ký_hiệu qui_ước của từng sở . Tuy_nhiên , bộ chủ_trương TS sẽ không bị hạn_chế , bắt_buộc nộp hồ_sơ ở đâu thì_phải sử_dụng mẫu hồ_sơ do địa_phương đó phát_hành .
T . HÀ

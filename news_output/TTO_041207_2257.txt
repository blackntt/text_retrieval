﻿ Những nữ diễn_viên có thu_nhập cao nhất
Trong năm 2004 , cátxê trung_bình của 2 người_đẹp này đều vào_khoảng hơn 6 triệu bảng Anh một phim nhưng xét về mặt thành_công thì Renee_Zellweger hơn hẳn Lopez .
Đứng đầu danh_sách này là " Người đàn_bà đẹp " Julia_Roberts với cátxê lên đến 10,3 triệu bảng Anh một phim .
Trong năm 2004 tuy không xuất_hiện trên màn_ảnh rộng ( chỉ tham_gia lồng_tiếng trong phim Shrek 2 vai công_chúa Fiona ) nhưng ngôi_sao trong phim Charlie ' s Angels_Cameron_Diaz vẫn đứng ở vị_trí thứ 2 với cátxê 10,3 triệu bảng một phim .
Nicole_Kidman , Reese_Witherspoon và Drew_Barrymore , mỗi người kiếm được khoảng 8 triệu bảng Anh một phim và xếp từ vị_trí thứ 3 đến thứ 5 .
Điều đáng ngạc_nhiên là trong danh_sách 10 dễin viên nữ có thu_nhập cao nhất trong năm 2004 lại thiếu_vắng những tên_tuổi quen_thuộc như Jodie_Foster , Meg_Ryan , Gwyneth_Paltrow và Michelle_Pfeiffer .
Danh_sách 10 nữ diễn_viên có thu_nhập cao nhất trong năm 2004 :
1 . Julia_Roberts 2 . Cameron_Diaz 3 . Nicole_Kidman 4 . Reese_Witherspoon 5 . Drew_Barrymore 6 . Halle_Berry 7 . Sandra_Bullock 8 . Angelina_Jolie 9 . Renee_Zellweger 10 . Jennifer_Lopez
KINH_LUÂN ( Theo BBC_NEWS )

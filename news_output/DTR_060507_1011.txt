﻿ Thực_đơn cho sức_khỏe
Nào , hãy lấy giấy bút và liệt_kê những thực_phẩm được bạn thường_xuyên lựa_chọn . Chúng sẽ cho bạn biết rất nhiều điều về tình_trạng sức_khoẻ của bạn trong hiện_tại và cả tương_lai đấy .
Nhóm “ thừa calories ”
Những người thuộc nhóm này ăn nhiều bột ngũ_cốc , bánh kem , kẹo , bánh snack mặn , thịt nhiều mỡ và các sản_phẩm làm từ bơ sữa . Chế_độ ăn cung_cấp quá nhiều calories , khiến lượng cholesterol tăng và chất_béo bão_hòa .
Nếu bạn là người trong nhóm này , nên “ phanh ” bớt các sản_phẩm bơ sữa và thịt mỡ để giảm lượng calories cũng_như chất_béo bạn thu_nạp vào cơ_thể . L ựa chọn bánh snack ít muối hơn và ăn các đồ t r áng m iệng như hoa_quả thay_vì ăn bánh kem . Cũng_nên tăng_cường vitamin và chất xơ cho cơ_thể .
Nhóm “ bệnh tim_mạch ”
Sở_dĩ có cái tên như_vậy vì nhóm này ăn rất ít rau_quả và ngũ_cốc . Hậu_quả là thiếu chất dinh_dưỡng và chất xơ , dễ bị huyết_áp_cao vì cơ_thể thừa natri ( ăn quá nhiều đồ chế_biến sẵn ) và thiếu kali ( không ăn rau_quả ) .
Nếu bạn thuộc nhóm này , hãy “ cải_thiện ” bằng cách ăn nhiều rau , quả , ngũ_cốc để lấy thêm chất thực_vật tự_nhiên , có tác_dụng ngừa ung_thư rất tốt . Hơn thế nữa , nghiên_cứu khoa_học cho thấy đây là chế_độ ăn hiệu_quả cho người muốn giảm cân .
Nhóm “ tinh_bột ”
Cũng ăn ít rau và hoa_quả nhưng người trong nhóm này lại rất thích tinh_bột . Vì_thế họ nhiều calories , dễ bị tăng mức insulin dẫn đến bệnh tiểu_đường . Bạn thuộc nhóm này ? Nên hạn_chế lượng tinh_bột đưa vào cơ_thể . 3-4 bát cơm / ngày là đủ , nếu cần có_thể ăn thêm một lát bánh_mì .
Nhóm “ thường_thường bậc trung ”
Sức_khỏe nhóm này xếp vào hàng “ hẻo ” nhất . Nồng_độ cholesterol và mức huyết_áp của họ khá cao do lười vận_động và “ nhẵn_mặt ” với bệnh béo phì . Ăn ít rau , nhiều chất_béo là đặc_điểm nổi_bật của nhóm . Lời khuyên cho những_ai thuộc nhóm này : vận_động giảm cân , ăn nhiều rau_quả và vitamin .
Nhóm chuẩn
Nhóm này ăn nhiều và ăn đa_dạng các loại rau , quả , ngũ_cốc , cá , ít chất_béo . Nhìn_chung họ có chế_độ dinh_dưỡng toàn_diện nhất trong 5 nhóm và ít nguy_cơ tim_mạch . Bạn là người nhóm này ? Xin chúc_mừng bạn đã chọn chế_độ ăn đúng_đắn , nhưng đừng quên vận_động , tập thể_thao để duy_trì sức_khỏe nhé .
Huyền_Trang
Theo MSN

﻿ Ngành xây_dựng được trả lương cao nhất
Theo kết_quả điều_tra về tiền_lương và quan_hệ lao_động được Bộ Lao_Động_Thương_Binh và Xã_hội thực_hiện trong tháng 6 , điều gây ngạc_nhiên là người lao_động trong ngành xây_dựng có mức lương cao nhất , chứ không phải ngành thương_mại , dịch_vụ .
Cụ_thể , trong 6 tháng đầu năm 2005 , người lao_động trong ngành xây_dựng được trả mức lương 1,84 triệu đồng / tháng , còn ngành thương_mại , dịch_vụ là 1,76 triệu đồng / tháng .
Cuộc điều_tra được tiến_hành bằng hình_thức phát phiếu và phỏng_vấn trực_tiếp tại 500 doanh_nghiệp ở cả ba vùng Bắc , Trung , Nam .
Trong đó có 41,6% doanh_nghiệp có quy_mô lao_động dưới 100 lao_động ; 18,8% doanh_nghiệp có quy_mô từ 100 đến 300 lao_động và 40,6% số doanh_nghiệp có quy_mô từ 300 lao_động trở_lên .
Kết_quả cuộc điều_tra cho thấy , năm 2006 người lao_động trong ngành xây_dựng nhận mức lương bình_quân là 1,84 triệu đồng / tháng .
Trong khi đó người lao_động trong ngành thương_mại , dịch_vụ chỉ nhận mức lương bình_quân là 1,76 triệu đồng / tháng . Lao_động ngành công_nghiệp là 1,7 triệu đồng / tháng .
Thấp nhất_là lao_động làm_việc trong các ngành nông , lâm , ngư_nghiệp với mức lương bình_quân 1,2 triệu đồng / tháng .
Chỉ có ngành xây_dựng là trả mức lương bình_quân cho người lao_động bằng 100% mức lương người lao_động được hưởng năm 2004 . Các ngành còn lại được trả thấp hơn mức được hưởng năm 2004 .
Lý_giải về điều này , các chuyên_gia tiền_lương thuộc Bộ Lao động – Thương_binh và Xã_hội cho rằng tiền_lương giảm có_thể là do hiệu_quả sản_xuất , kinh_doanh của doanh_nghiệp không được như trước .
Hơn_nữa , việc điều_chỉnh tiền_lương qua các đợt như trên có_thể không có tác_dụng với các doanh_nghiệp , bởi trên thực_tế , hầu_hết số doanh_nghiệp này đã trả lương cho người lao_động theo mức cao hơn nhiều so với Chính_phủ quy_định . Mức lương tối_thiểu bình_quân được các doanh_nghiệp ngành xây_dựng trả cho người lao_động là 613.000 đồng/tháng . Ngành thương_mại và dịch_vụ trả cho người lao_động theo mức lương tối_thiểu bình_quân là 574.000 đồng/tháng . Ngành công_nghiệp áp_dụng mức lương tối_thiểu bình_quân là 567.000 đồng/tháng và ngành nông , lâm , ngư_nghiệp là 498.000 đồng/tháng .
Theo Đầu_Tư

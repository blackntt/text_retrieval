﻿ Bốc_thăm cúp FA : Liverpool gặp M . U
Hai CLB hạng nhất còn sót lại đến vòng 5 là Colchester và Brentford đều phải gặp các đối_thủ đến từ Premiership . Trong đó Colchester nhiều khả_năng sẽ phải gặp " ông kẹ " Chelsea .
Bolton sau khi xuất_sắc vượt qua Arsenal ở vòng 4 sẽ gặp một đối_thủ nhẹ hơn là West_Ham trong khi Aston_Villa gặp Manchester_City .
Các cặp đấu vòng 5 cúp FA :
Preston hoặc Crystal_Palace - Coventry hoặc Middlesbrough_Newcastle_Utd - Southampton_Aston_Villa - Manchester_City_Everton hay Chelsea - Colchester_Charlton - Brentford_Liverpool - Man_Utd_Bolton - West_Ham_Stoke - Reading hay Birmingham
Các trận đấu vòng 4 giữa Preston - Crystal_Palace ; Coventry - MIddlesbrough ; Everton - Chelsea và Reading - Birmingham sẽ thi_đấu lại vì đã có kết_quả hòa trong lần gặp đầu_tiên .
Các trận đấu sẽ diễn ra_vào 2 ngày 18 và 19 tháng 2 .
Đ . K . L ( Theo AFP )

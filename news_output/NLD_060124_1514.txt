﻿ Yamba_Asha bị cấm thi_đấu tại World_Cup
Yamba_Asha_Cầu thủ người Angola_Yamba_Asha đã bị cấm thi_đấu 9 tháng vì sử_dụng doping , và chắc_chắn sẽ vắng_mặt tại World_Cup 2006 tại Đức mùa_hè này .
Trong bản thông_báo mới nhất của FIFA ngày hôm_qua , lệnh cấm Yamba_Asha thi_đấu bất_kỳ trận đấu nào cho_đến ngày 8-8 đã được đưa ra .
Hậu_vệ trái của Angola chính là cầu_thủ duy_nhất đã chơi cả 12 trận cho đội_tuyển nước này trong vòng_loại World_Cup 2006 khu_vực châu Phi . Sau khi bị phát_hiện sử_dụng doping , anh đã bị cấm thi đầu_từ tháng 11 năm trước . Và anh cũng không được tham_dự CAN 2006 .
Bài kiểm_tra doping đối_với cầu_thủ 29 tuổi này được bất_ngờ được đưa ra trong trận đấu vòng_loại World_Cup giữa Angola với Rwanda ở Kigali hồi tháng 10 . Anh cũng không được đăng_ký trong danh_sách CLB_AS Aviacao tham_dự vòng thứ nhất Champions_League châu Phi tháng tới .
Tiền_Phong

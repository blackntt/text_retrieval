﻿ TP.HCM : từ 1-3 thi sát_hạch giấy_phép lái ôtô bằng máy_vi_tính
Đến nay đã có năm trung_tâm thi bảo_đảm điều_kiện thi vi_tính . Riêng_Trường dạy lái_xe Tiến_Bộ ( quận Tân_Phú ) và Trung_tâm Dạy nghề Nhà_Bè chưa bảo_đảm điều_kiện thi vi_tính sẽ chuyển hồ_sơ học_viên dự thi sang các trung_tâm khác .
Theo ông Lực , việc thi lý_thuyết trên máy_tính sẽ hạn_chế tiêu_cực trong quá_trình thi vì từ 300 câu_hỏi của bộ đề thi được đưa vào vi_tính sẽ chuyển thành hàng vạn bộ đề thi khác_nhau , tránh được tình_trạng học tủ , học mẹo và học mánh .
N . ẨN

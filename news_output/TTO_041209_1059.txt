﻿ Lưu_học_sinh tại Bắc_Kinh đón năm mới
Đêm diễn hoành_tráng này do chính các lưu_học_sinh thực_hiện . Đêm diễn kéo_dài gần ba giờ đồng_hồ với 19 tiết_mục vô_cùng hấp_dẫn . Trong đó , các bạn lưu_học_sinh Việt_Nam đã đóng_góp tiết_mục “ múa nón ” đặc_sắc với nền nhạc “ Dáng đứng Bến_Tre ” cùng sáu cô_gái Việt_Nam thướt_tha trong các tà áo_dài .
Kết_thúc đêm diễn , một bữa tiệc họp_mặt thân_mật và những tràng pháo tay vang lên không dứt như muốn nổ tung cả khán_phòng đã đọng lại trong lòng hàng ngàn bạn sinh_viên nhiều cảm_xúc .
Sáu cô_gái lưu_học_sinh VN trong tiết_mục múa nón
Đêm diễn khép lại với các bạn lưu_học_sinh trong trang_phục truyền_thống của nước mình
Tin , ảnh : LƯU_ĐÌNH_KHÁNH ( từ Bắc_Kinh )

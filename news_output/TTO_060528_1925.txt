﻿ Tiểu_phẩm : Thường_dân xuất_sắc
Chiều nay nhất_định phải khao thôi , tôi dừng xe sát lề_đường để gọi điện_thoại cho chiến_hữu . Đang alô ngon trớn thì mụ chủ quán nước ngay cái lề_đường tôi đang đứng gầm_gừ :
- Tránh chỗ cho người_ta buôn_bán !
“ To_gan nhỉ ” . Tôi nhủ thầm , cần cho cái nhà bà này biết thế_nào là lễ_độ . Chìa cái thẻ màu xanh da_trời ra , tôi dằn từng tiếng một :
- Bà có biết tôi là ai không ? Bà có biết cái vỉa_hè bà đang bày bàn_ghế ra mua_bán được lấy tiền ở đâu ra để làm không ? Bà có biết ...
Tôi không_thể nói tiếp khi nhìn khuôn_mặt bà ta đang chuyển từ màu vàng sang màu xanh . Không_khéo bà ta sắp ngất đến_nơi .
Về gần đến nhà , tôi sực nhớ đến một chuyện chưa làm_nên quay xe lại hướng về phía trụ_sở ủy_ban phường . Tại phòng tiếp dân , cô nhân_viên trực nheo mũi nhìn tôi :
- Cần gì , giấy_tờ đâu ?
Tôi chìa tấm thẻ “ Thường_dân ưu_tú ” ra . Cô nhân_viên gần_như bật ra khỏi ghế , đon_đả :
- Ôi , thưa anh ưu_tú , mời anh sang bên này xơi nước . Dạ trà này em vừa_mới pha , loại hảo_hạng đó , chúng_em có_thể giúp gì anh ạ ?
Tôi thong_thả nhấp một ngụm trà thơm rồi nói :
- Hồ_sơ xin cơi_nới nhà để có thêm chỗ ở cho thằng con mới cưới vợ của tôi chẳng hiểu vì_sao cứ hẹn lên hẹn xuống , cô gọi người có trách_nhiệm ra đây trả_lời cho tôi .
Cô_gái dạ rối_rít và chạy đi . Gần_như ngay_lập_tức , ông phó_chủ_tịch xuất_hiện , ông_ta lễ_phép :
- Thưa anh , hồ_sơ của anh xong rồi_đây ạ , tại mấy đứa nhân_viên không chu_đáo . Chúng_tôi sẽ kỷ_luật ngay . Thật xấu_hổ , chúng không biết hằng tháng ngửa_tay nhận lương là tiền từ đâu nữa .
Cả ông phó_chủ_tịch lẫn cô nhân_viên tiễn tôi ra tận cổng , ông_ta nói với theo :
- Anh ưu_tú đi đường cẩn_thận nhé , cho tôi gửi lời hỏi_thăm chị và mấy cháu , hôm nào rỗi nhớ ghé thăm chúng_tôi .
Cuộc_đời bỗng đẹp lạ , cả cái không_khí nóng_bức lẫn tiếng xe_cộ ồn_ào cũng trở_nên đáng yêu làm_sao . Nhưng đón tôi ở nhà lại là khuôn_mặt chẳng yêu_đời tí nào của vợ tôi . Cô ta kéo tôi vào hẳn trong phòng ngủ rồi thì_thào :
- Con vợ thằng Tám tàng vừa sang đây .
Tưởng gì , lại sang khoe_khoang về cái thẻ “ Thường_dân ưu_tú ” của chồng chứ gì . Tôi cười_khẩy rồi rút tấm thẻ màu xanh như_thể bà tiên rút cây phất_trần làm_phép rồi thong_thả từng tiếng với vợ :
- Chồng ... của ... em ... cũng ... có ... đây ...
Vợ tôi lắc_đầu :
- Anh ơi_là anh , thằng_cha Tám tàng đã được đổi cái thẻ này lấy loại thẻ “ Thường_dân xuất_sắc ” rồi .
Cha_mẹ ơi ! Tôi bỗng nhớ lại thái_độ vô_cùng tử_tế của mấy vị cán_bộ phường lúc nãy . Mà đó là tôi mới chỉ là ưu_tú , không biết thằng_cha Tám tàng lên phường với cái thẻ xuất_sắc thì sẽ như_thế_nào nhỉ ? Tôi gạt tay vợ ra và phóng qua nhà tay hàng_xóm đáng ghét . Tám tàng tiếp tôi khá vui_vẻ :
- Bây_giờ tôi chẳng cần đi đâu xin_xỏ cái gì cả , thỉnh_thoảng lại có anh cán_bộ phường ghé nhà hỏi : “ Anh Tám xuất_sắc có cần gì tụi em không ạ ? ” . Ông lên_đời cái thẻ của mình đi , dễ_ợt à , cứ đóng_góp thật nhiều vào ngân_sách là được thôi .
Tám tàng chỉ nói đến thế . Tôi về nhà và lục tung mấy tờ báo cũ ra , để xem ... thứ gì góp nhiều tiền cho ngân_sách quốc_gia : Dầu_mỏ , vườn nhà mình chắc là không đào được dầu rồi . Bia và thuốc_lá , cũng không được , sức uống và sức hút của mình có_hạn ... A đây rồi , tôi lao ra đường và hét lên :
- Vé_số , vé_số ... lại đây !
THỤC_ANH

﻿ Jessica_Simpson lại đeo nhẫn_cưới !
Mới_đây người_ta vừa nhìn thấy Jessica lại đeo nhẫn_cưới , bằng cách lồng vào một sợi dây_chuyền và đeo lên cổ .
Một người bạn_thân của Jes cho_hay Jes thỉnh_thoảng vẫn nói_chuyện với chồng cũ : “ Ai cũng có những kỉ_niệm . Nick sẽ luôn_luôn là một phần quan_trọng của cuộc_đời Jessica ” .
Ngoài chiếc nhẫn , trên dây_chuyền 3,5 cara của Jess còn 2 cây thánh_giá . Một chiếc do bố_mẹ tặng cho cô , còn chiếc kia từng thuộc về mẹ Jess và cô em_gái Ashlee . Vậy_là 2 chiếc thánh_giá và chiếc nhẫn Jessica đang đeo là những kỉ_vật hết_sức quan_trọng đối_với cô .
Vậy_Nick nghĩ gì ? “ Tôi muốn Jessica được hạnh_phúc ” , Nick nói với People .
Gần đây có tin_đồn Jessica đang “ đi_lại ” với anh bạn_thân , nhà_tạo_mẫu tóc Ken_Pavés . BM Theo People

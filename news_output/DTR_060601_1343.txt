﻿ Những cột mốc trong sự_nghiệp Shevchenko
' ' Quả bóng vàng ' ' châu Âu 2004 , chân_sút số 1 của ĐTQG Ukraina , cầu_thủ ghi_bàn xuất_sắc nhất trong lịch_sử Champions_League .
Sinh ngày 29/9/1976 tại Dvirkivshchyna , Ukraine .
- Gia_nhập đội trẻ của Dynamo_Kiev và lần đầu khoác_áo đội 1 trong trận thắng Shakhtar_Donetsk 3-1 ngày 28/10/1994 . Trong mùa bóng đó được ra sân 16 lần , ghi 1 bàn thắng .
- Gây_sự chú_ý trong mùa giải 1997/1998 khi lập hat - trick trong trận Dynamo_Kiev hạ Barcelona 4 - 0 trên sân Nou_Camp tại Champions_League .
- Giành 5 chức VĐQG và 4 Cúp QG với Dynamo_Kiev trước khi gia_nhập AC Milan năm 1999 với giá chuyển_nhượng 25 triệu USD .
- Giành danh_hiệu ' ' Vua_phá_lưới ' ' Serie A ngay trong mùa giải đầu_tiên với 24 bàn thắng , trở_thành cầu_thủ thứ_hai trong lịch_sử ( sau Michel_Platini ) làm được điều này .
- Ghi_bàn thường_xuyên trong 2 mùa giải tiếp_theo ( 38 bàn trong 63 trận tại Serie A ) nhưng Milan thi_đấu không thành_công .
- Mùa 2002/2003 : Bị chấn_thương trong giai_đoạn đầu nhưng trở_lại vào cuối mùa . Sút thành công_quả penalty quyết_định giúp Milan đánh_bại Juventus tại sân Old_Trafford để giành chức vô_địch Champions_League 2003 .
- Tháng 8/2003 : Ghi_bàn duy_nhất giúp Milan đánh_bại Porto 1 - 0 , giành Siêu_Cúp châu Âu .
- Mùa 2003/2004 : Lần thứ_hai giành danh_hiệu ' ' Vua_phá_lưới ' ' Serie A với 24 bàn thắng , giúp Milan giành được chức VĐQG đầu_tiên sau 5 năm .
- Tháng 12/2004 : Giành ' ' Quả bóng vàng ' ' châu Âu
- Tháng 10/2005 : Lần đầu_tiên trong lịch_sử đưa Ukraina giành quyền tham_dự World_Cup . Ghi 6 bàn trong tổng_số 18 bàn thắng của Ukraina tại vòng_loại World_Cup 2006 .
- Ngày 4/4/2006 : Ghi_bàn thứ 51 tại Champions_League trong trận Milan thắng Lyon 3-1 để trở_thành cầu_thủ ghi_bàn xuất_sắc nhất trong lịch_sử Champions_League .
- Ngày 12/5/2006 : Ông chủ của AC Milan , Silvio_Berlusconi khẳng_định ' ' Shevchenko muốn tới Anh ' ' . Ngay sau đó , Shevchenko tổ_chức họp_báo khẳng_định ý_định rời CLB .
- Ngày 31/5/2006 : Gia_nhập Chelsea với mức giá chuyển_nhượng kỷ_lục của CLB .
Theo D . N_VTC

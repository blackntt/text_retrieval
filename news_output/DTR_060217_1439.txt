﻿ Philippines : Lở đất chôn_vùi cả một thị_trấn , 200 người chết
Sáng nay , một trận lở đất đã chôn_vùi hàng trăm ngôi nhà và một trường có nhiều học_sinh nhỏ_tuổi trên hòn đảo Leyte , miền đông Philippines . Tổ_chức Chữ_thập_đỏ ước_tính ít_nhất 200 người chết , và 1.500 người mất_tích .
“ Có_vẻ như cả ngọn núi đã phát_nổ , và một khối_lượng lớn đất trút xuống , ” Dario_Libatan , một người sống_sót kể lại . “ Tôi không nhìn thấy một ngôi nhà nào còn đứng vững . ”
Lở đất xảy_ra trên hòn đảo Leyte sau 2 tuần mưa liên_tục .
Thượng nghị sỹ Richard_Gordon , trưởng_ban Chữ_thập_đỏ Philippines cho_biết cả một làng hình_như đã bị vùi lấp . Có_lẽ 200 người đã thiệt_mạng , và 1.500 người mất_tích . “ Chưa có ai đếm người chết , đó chỉ là ước_tính của chúng_tôi . ” – Gordon cho hãng AP biết . “ Chúng_tôi đang triển_khai công_tác cứu_trợ . Lở đất thường_xuyên xảy_ra ở vùng này . ”
Tỉnh_trưởng tỉnh Miền nam Leyte , Rosette_Lerias , cho_biết 500 ngôi nhà_ở làng Ginsahugan , thuộc thị_trấn St . Bernard , có_thể đã bị chôn_vùi . Động_đất xảy_ra vào_khoảng 9h sáng . Lúc đó học_sinh vẫn đang học ở trường tiểu_học của làng . “ Mặt_đất bị ẩm_ướt vì mưa nhiều . Cây bị bật gốc cùng_với bùn đất . ” – Tỉnh_trưởng cho_biết .
Ủy_viên tỉnh_ủy Eva_Tomol cho_biết chỉ có 3 ngôi nhà trong làng là vẫn còn đứng vững sau trận lở đất . Làng_Ginsahugan có khoảng 2.500 dân . 6 người sống_sót hiện đang được điều_trị trong bệnh_viện . “ Chúng_tôi hi_vọng rằng chỉ khoảng 1.000 người_dân ở đây bị mất_tích . "
Tháng 11/1991 , khoảng 6.000 người thiệt_mạng ở hòn đảo miền trung Leyte vì lũ_lụt và lở đất do một trận bão nhiệt_đới gây nên .
Trang_Thu_Theo AP

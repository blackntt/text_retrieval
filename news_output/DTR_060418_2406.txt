﻿ Ăn nhiều mơ có_thể tử_vong ?
Theo khuyến_cáo của Cơ_quan Tiêu_chuẩn Thực_phẩm Anh - Food_Standards_Agency ( FSA ) , trong quả mơ có một độc_tố tên gọi_là xyanua ( cyanide ) , nếu ăn nhiều có_thể gây ngộ_độc cấp_tính và dẫn tới tử_vong .
Vì_vậy , mỗi người chỉ nên ăn tối_đa 2 quả mơ chua mỗi ngày .
Sở_dĩ vài năm gần đây , việc tiêu_thụ mơ tăng mạnh là do có một_số tài_liệu cho rằng , quả mơ chứa nhiều vitamin B17 , có khả_năng thúc_đẩy hệ_thống miễn_dịch và chữa một_số bệnh ung_thư .
Tuy_nhiên , vitamin B17 chính là leatrile , trong hàm_lượng của nó có chứa thủy_ngân nên các nước như Mỹ , Pháp đã cấm sử_dụng .
Thêm một công_dụng mới của củ gừng
Ngoài tác_dụng chữa đau bụng , cảm cúm , buồn_nôn ... mới_đây , các nhà_khoa_học Mỹ còn phát_hiện khả_năng chữa bệnh ung_thư buồng_trứng của gừng . Kết_quả mới được công_bố ngày 11/4 vừa_qua .
Những hợp_chất trong gừng vừa có_thể tiêu_diệt các tế_bào ung_thư , vừa có tác_dụng ngăn_cản các tế_bào này kháng thuốc . Hiện các nhà_khoa_học đang gẩp rút thử_nghiệm để sớm chế ra loại thuốc điều_trị căn_bệnh ung_thư này .
Cà_chua ngăn_ngừa ung_thư
Hệ_thống siêu_thị Tesco tại Anh vừa giới_thiệu một loại cà_chua mới , có chứa hàm_lượng lycopene rất cao - một hoá_chất có tác_dụng chữa bệnh ung_thư tuyến_tiền_liệt .
Nam_giới ăn cà_chua giàu lycopene đều_đặn mỗi ngày sẽ giúp giảm 45% nguy_cơ mắc bệnh ung_thư tuyến_tiền_liệt .
Lycopene chính là thành_phần làm_nên màu đỏ truyền_thống của cà_chua , là chất ôxy hoá mạnh có_thể giúp các tế_bào tránh được sự tấn_công của các gốc tự_do và có tác_dụng giảm cholesterol trong máu .
Bùi_Then ( Tổng_hợp )

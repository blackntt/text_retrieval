﻿ Chưa hạ_màn !
( Dân_trí ) - Đêm nay , Liga chính_thức bước vào vòng đấu cuối_cùng nhưng vẫn chưa_thể “ hạ_màn ” bởi ngoài việc xác_định được “ nạn_nhân ” thứ 3 phải xuống hạng mùa tới , ngôi á_quân và vị_trí thứ 4 vẫn còn bỏ_ngỏ do cả Real_Madrid , Valencia , Osasuna cùng không thi_đấu .
Cơ_hội cho Sevilla !
Trong khi đó , nhà tân vô_địch UEFA Cup , Sevilla sẽ đá bù trận đấu ở vòng 34 với Barcelona và đây là cơ_hội thuận_lợi để đội bóng xứ Andalucia này san bằng khoảng_cách về điểm_số với đội_xếp thứ 4 Osasuna .
Chiến_thắng gần_như đã nằm gọn trong tay thày trò HLV Juande_Ramos bởi đối_thủ của họ , nhà ĐKVĐ Liga sẽ hành_quân tới sân Sanchez_Pizjuan với đội_hình thiếu tới 9 vị_trí chính_thức nhằm giữ sức cho trận chung_kết Champions_League với Arsenal vào ngày 17/5 tới .
Ngoài_ra , tinh_thần của đội chủ nhà cũng đang cực_kỳ hưng_phấn sau chiến_thắng tuyệt_vời 4 - 0 trước Middlesbrough trong trận chung_kết Cup UEFA , món quà kỷ_niệm vô_cùng ý_nghĩa nhân_dịp “ bách niên ” của CLB .
Sevilla sẽ có một mùa bóng hoàn_hảo ?
Giành trọn 3 điểm trong trận đấu này , Sevilla tiếp_tục gây sức_ép mạnh_mẽ lên Osasuna trong cuộc đua giành suất cuối_cùng dự vòng sơ loại Champions_League mùa tới và khiến hai trận đấu muộn nhất Liga giữa Real_Madrid – Sevilla , Osasuna - Valencia vào thứ 3 tới vô_cùng hấp_dẫn và quyết_liệt .
Sevilla - Barcelona Ở nhóm cuối bảng , sau khi Malaga và Cadiz đã chắc suất xuống hạng , cuộc_chiến nhằm “ đùn_đẩy ” tấm vé thông_hành cuối_cùng xuống hạng nhì chỉ còn là cuộc đấu tay_đôi giữa Espanyol và Alaves .
Đứng thứ 17 với 2 điểm nhiều hơn Alaves , Espanyol hiện đang có lợi_thế khi chỉ phải gặp một Real_Sociedad đã không còn mục_tiêu phấn_đấu . Hơn thế nữa , đội bóng xứ Basque này chưa bao_giờ hả_hê ra về khi rời sân Montjuic của Espanyol trong vòng 3 năm nay .
" Tuy đối_thủ đã không còn động_lực nhưng các cầu_thủ chúng_tôi vẫn cần phải tập_trung trong_suốt 90 phút để giành chiến_thắng . Cùng_với chiếc Cúp_Nhà vua , việc trụ hạng thành_công sẽ là một kết_thúc có_hậu cho một mùa bóng đáng nhớ đối_với chúng_tôi ” , HLV Miguel_Angel_Lotina của Espanyol phát_biểu .
Espanyol sẽ " cập bến " ?
Tuy không có sự phục_vụ của trung_vệ trụ_cột người Argentina_Pochettino và tiền_vệ David_Garcia do bị chấn_thương nhưng hàng công của đội chủ nhà vẫn sẽ có_mặt đầy_đủ các “ anh_tài ” như tiền_đạo đội_trưởng Tamudo hay “ tiểu phật ” Ivan_De la Pena cùng tiền_vệ rất cơ_động Costa .
Trong khi đó , tình_cảnh của Alaves quả_là “ ngàn_cân_treo_sợi_tóc ” bởi muốn giành quyền trụ hạng , Alaves không còn sự lựa_chọn nào khác là phải giành chiến_thắng trước đối_thủ khó chơi Deportivo và hy_vọng Espanyol không giành trọn được 3 điểm trước Sociedad .
Cái khó cho Alaves là Depor hiện vẫn còn mục_tiêu bởi đội bóng xứ Galacia đang đứng thứ 7 trên BXH , vị_trí được quyền tham_dự Cup_Intertoto , “ con đường vòng ” để bước ra châu Âu mùa tới .
Hiện vị_trí thứ 7 của Depor cũng chưa hoàn_toàn chắc_chắn trước sự “ nhăm_nhe ” của Villarreal ( thứ 8 ) và Getafe ( thứ 9 ) bởi cả 2 đội này đều chỉ kém Depor vẻn_vẹn 1 điểm . Nếu_Depor xảy chân , họ chắc_chắn sẽ bị 1 trong 2 đội bóng này qua_mặt .
Bởi_vậy , HLV Caparros và các học_trò sẽ không_thể “ buông ” trong trận đấu này và như_vậy , sẽ chỉ càng khó cho Alaves mà thôi . Lối_thoát nào cho đội bóng của chủ_tịch nổi_tiếng “ cứng_đầu_cứng_cổ ” Dmitry_Piterman đây ?
M.Hải

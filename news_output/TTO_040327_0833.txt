﻿ Tây_Ninh : tài_xế “ chiếc xe điên ” kháng_cáo xin giảm án
Theo kết_quả điều_tra , vào tối 10-1-2003 Quách_Ngọc_Hải sau khi say rượu đã lái ôtô chở theo hai người say rượu khác đều là cán_bộ của Viện_Kiểm sát Tây_Ninh lao như bay trên đường Tua_Hai ( thị_xã Tây_Ninh ) . Chiếc xe này đã đụng liên_tiếp vào bốn chiếc xe_gắn_máy ngược chiều . Trong số bốn nạn_nhân đi xe_máy , có hai người là cán_bộ Tòa_án thị_xã Tây_Ninh và một người khác bị_thương nhẹ , một là trưởng_phòng công_chứng ( Sở Tư pháp Tây_Ninh ) bị_thương nặng .
L . A . Đ

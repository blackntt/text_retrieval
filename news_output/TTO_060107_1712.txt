﻿ Học ngành Công_nghệ_thông_tin trường nào ở Singapore ?
Chị vẫn có_thể cho cháu thử nộp đơn vào các trường công_lập của Singapore như NUS và NTU . Đối_với NUS cháu sẽ không phải qua kỳ thi_tuyển mà chỉ xét dựa trên hồ_sơ học_tập .
Hồ_sơ bao_gồm : Khai_sinh , hồ_sơ học_tập ( bằng tốt_nghiệp THPT , học_bạ trung_học + bảng điểm ĐH + bằng tốt_nghiệp ĐH , photo không cần công_chứng ( kèm bản dịch tiếng Anh ) điểm IELTS / TOEFL . Đơn điền trực_tuyến trên mạng ( online ) .
Nếu_không thành_công chị có_thể tham_khảo chương_trình của hai trường sau có đào_tạo ngành Công_nghệ_thông_tin :
1 - Học_viện PSB Academy : là học_viện thuộc cơ_quan tiêu_chuẩn chất_lượng ( Productivity and Standard_Board ) của Singapore .
Điều_kiện nộp đơn vào học_viện PSB : Điểm trung_bình THPT 6.5 trở_lên , IELTS 5.5 .
Kỳ nhập_học : tháng 1 và tháng 8 .
Thời_gian học : 3 năm .
Học_phí : 33.600 SGD ( khoảng 20.500 USD ) cho 3 năm_học .
Chương_trình học do Trường ĐH Newcastle ( Úc ) cấp bằng cử_nhân .
Website : www.psbacademy.edu.sg
2 - ĐH James_Cook ( Úc ) tại Singapore ( JCU ) :
Đây là học_xá quốc_tế của ĐH James_Cook ( Úc ) đặt tại Singapore , sinh_viên có_thể chuyển_tiếp qua Úc tại bất_cứ giai_đoạn nào của chương_trình cử_nhân . Học tại Singapore sinh_viên có_thể hoàn_tất chương_trình Cử_nhân trong vòng 2 năm , do một năm_học 3 học_kỳ .
Điều_kiện : Tốt_nghiệp THPT , IELTS 6.0 ( không có điểm 5 ) .
Học_phí : 39.000 SGD / cả chương_trình ( 23.000 USD ) .
Kỳ nhập_học : tháng 3 , 7 , 10 .
Website : www.jcu.edu.sg
Q . DŨNG thực_hiện

﻿ Hướng_dẫn lưu_thông tạm trên đường Phạm_Văn_Chí
Riêng các loại xe ôtô của nhân_dân , doanh_nghiệp nằm trong khu_vực công_trường vẫn được lưu_thông , nhưng theo hướng_dẫn của lực_lượng điều_tiết giao_thông tại công_trường . Lộ_trình tạm_thời như sau :
- Hướng từ_đường Phạm_Phú_Thứ đến đường Mai_Xuân_Thưởng : Phạm_Phú_Thứ - Gia_Phú - Mai_Xuân_Thưởng ; hoặc Phạm_Phú_Thứ - Bãi_Sậy - Mai_Xuân_Thưởng .
- Hướng từ Mai_Xuân_Thưởng đến Phạm_Phú_Thứ : Mai_Xuân_Thưởng - Gia_Phú - Phạm_Phú_Thứ hoặc Mai_Xuân_Thưởng - Bãi_Sậy - Phạm_Phú_Thứ .
Ngoài_ra , Sở GTCC tiếp_tục gia_hạn phân_luồng giao_thông đường Hoàng_Diệu , quận Thủ_Đức cho_đến hết ngày 31-5-2006 để thi_công xây_dựng hệ_thống thoát nước trên đường Hoàng_Diệu , quận Thủ_Đức . Các loại phương_tiện vận_tải khi lưu_thông qua khu_vực này vẫn theo hướng giao_thông đã và đang thực_hiện .
PV

﻿ 99 tuổi vẫn làm thợ_máy
Tháng 9 tới cụ Buster_Martin sẽ tròn 100 tuổi , thế_nhưng 8 tiếng mỗi ngày cụ vẫn đều_đặn tới nhà_máy , làm công_việc bảo_dưỡng cho cả hạm_đội hơn trăm chiếc xem tải một_cách chỉn_chu , tươm_tất , chẳng ai chê vào đâu được .
Cách đây 3 năm , ông_cụ 97 tuổi đã miễn_cưỡng nghe lời con_cháu , nghỉ hưu để an_dưỡng tuổi già . Nhưng chỉ được 3 tháng , không chịu nổi cảnh hưu_trí tẻ_nhạt , cụ lại xin vào làm tại nhà máy_bơm nước Pimlico ở Lambeth , miền Nam nước Anh .
Theo lời kể của Buster , cụ có thâm_niên làm_việc từ năm 5 tuổi , khi còn là cậu_bé được các sơ nuôi_dưỡng ở trại trẻ mồ_côi Cornish . 10 tuổi , lang_bạt đến Luân_Đôn và quần_quật làm 18 tiếng mỗi ngày .
Sau đó cụ tham_gia Thế_Chiến II , phục_vụ trong Lực_lượng Hải_quân một thời_gian dài trước khi chuyển nghề thành thợ cơ_khí đầu những năm 1990 .
Cụ Buster tự_tin : “ Tôi sẽ còn làm_việc cho_đến khi từ_giã cuộc_đời thì_thôi , chắc lúc đó cũng khoảng 125 tuổi . Tôi chẳng thấy run tay run chân gì cả . Lúc_nào lấm_lem dầu_mỡ mới là lúc tôi thấy vui_vẻ nhất ” .
Hải_Minh_Theo_The_Sun

﻿ Nhật : đọc tin nhắn điện_thoại_di_động bằng đồng hề đeo tay
Đồng_hồ nhận tin nhắn từ điện_thoại_di_động Đồng_hồ không_chỉ để xem giờ mà_còn để nhận tin nhắn hay các tín_hiệu báo_động từ điện_thoại_di_động , điều đó sắp trở_thành hiện_thực với người Nhật .
Đây là sản_phẩm mới do hãng chế_tạo đồng_hồ Citizen chế_tạo . Mặt đồng_hồ được trang_bị một màn_hình chữ_nhật có hai cực phát ánh_sáng xanh , cho_phép giữ liên_lạc với điện_thoại_di_động ở khoảng_cách vài mét .
Ngoài_ra , đồng_hồ còn phát tín_hiệu cho_biết có một tin nhắn hay một cuộc_gọi đến ngay cả khi điện_thoại ở chế_độ im_lặng hay để xa tầm tay người sử_dụng . Nó cũng có_thể hiển_thị tên người gọi hay người gửi tin nhắn đến điện_thoại .
Một tính_năng quan_trọng khác của đồng_hồ hiện_đại này là chống mất_cắp điện_thoại_di_động . Theo đó , trong trường_hợp , liên_lạc giữa đồng_hồ và điện_thoại_di_động bị gián_đoạn , đồng_hồ sẽ phát tín_hiệu báo_động ngay_lập_tức về việc mất liên_lạc này . Hãng_Citizen thông_báo loại đồng_hồ này sẽ được sản_xuất hạn_chế với 5.000 chiếc dành cho nam và bán ra thị_trường từ ngày 7-7 tới . Hãng_Seiko cũng cho_biết sẽ tung ra những sản_phẩm tương_tự vào năm tới .
Theo Tuổi_Trẻ

﻿ Yêu nhiều hơn cả khi có em bé
Em bé ra_đời mang niềm_vui bất_tận cho cha_mẹ cùng không ít phiền_toái . Nhiều cặp tự hỏi sống lại thuở vợ_chồng son thế_nào khi bé “ chiếm ” hết thời_gian . Thật_ra nếu biết cách , bạn sẽ khiến tình_cảm vợ_chồng ngày_càng nồng_thắm .
Bày_tỏ tình_yêu không_bao_giờ thừa
Khi có con , cuộc_sống của hai bạn ít_nhiều bị xáo_trộn , thời_gian hai người dành riêng cho nhau cũng không được nhiều . Hãy tận_dụng email , tin nhắn , điện_thoại để bày_tỏ tình_cảm .
Gửi cho anh_ấy email nói rằng bạn quá háo_hức không_thể đợi đến khi hai người ở_riêng bên nhau . Những dòng thư bày_tỏ ham_muốn mãnh_liệt còn là chất kích_thích để vợ_chồng mong … đến tối .
Nhắn_tin với lời_lẽ tán_dương , âu_yếm , hẹn nhau đi ăn trưa cũng là cách hiệu_quả .
Sức_mạnh của … vòi_hoa_sen
Bạn đã thử cách này bao_giờ chưa ? Một hôm nào_đó , khi bọn trẻ đã ngủ , hãy cùng nhau tẩy hết những căng_thẳng của cuộc_sống thường_nhật dưới vòi_hoa_sen .
Trong màn hơi_nước ấm nóng lan_tỏa , hãy cùng trò_chuyện , thư_giãn và âu_yếm . Giây_phút thần_tiên hiếm_hoi trong ngày . Cuộc_sống vợ_chồng sẽ thi_vị và lãng_mạn hơn nhiều đấy .
Hò_hẹn ở nhà
Tranh_thủ vài tiếng buổi tối khi em bé ngủ , các bạn hãy trải chăn xuống sàn bên lò_sưởi , tắt hết đèn , bật lên bản_nhạc lãng_mạn . Chút rượu_vang cùng món tráng_miệng sẽ mang lại những giây_phút diệu_kỳ đáng nhớ .
Nhà bạn không có lò_sưởi ? Thế_thì cùng nhau nấu_ăn vậy nhé . Khi em bé đã ngủ , hai vợ_chồng hãy cùng nhau nấu thử_nghiệm món mới trong sách dạy nấu_ăn .
Giờ thì bày bàn nào . Bữa tối đã sẵn_sàng . Bạn cần thêm một chai rượu_vang , ít nến và … tiệc tình_yêu bắt_đầu .
Bày trò nghịch_ngợm
Có cô vợ đã “ cả_gan ” làm thế_này : Cô ấy nhờ người bạn_thân chụp hình mình trông rất sexy , sau đó kín_đáo đặt ảnh dưới mũ của chồng . Kết_quả thật khả_quan khi những ngày_sau đó chuyện the phòng sinh_động hẳn lên .
Nếu định áp_dụng “ chiêu ” này bạn phải thận_trọng nhé , cần tìm một “ phó_nháy ” đáng tin .
Cũng có_thể gửi con để hò_hẹn với nhau một tối . Đừng làm theo cách thông_thường . Mỗi người nên tự chuẩn_bị riêng và đến chỗ hẹn . Cứ làm như đây là lần đầu gặp_gỡ . Ăn_mặc đẹp , ấn_tượng , khác với ngày thường sẽ thổi vào tình_yêu hai người một làn gió mới .
“ Gần_gũi ” nhau khắp_nơi trong nhà khi bọn trẻ đã ngủ cũng là trò mạo_hiểm cực_kỳ thú_vị . Hơi “ ngỗ_nghịch ” và liều_lĩnh nhưng sẽ làm sống dậy cảm_giác mới_mẻ những ngày đầu .
Ngày nắng đẹp
Gửi con ở nhà_trẻ , sắp_xếp cuộc vui chỉ có hai người . Các bạn cùng ăn trưa , xem phim , đi dạo phố . Sẽ là một món quà bất_ngờ cho bé nếu được cả ba lẫn mẹ đón sau giờ học .
Một ngày dạo chơi đã qua , buổi tối là thời_gian gia_đình quây_quần đầm_ấm . Hãy nghỉ_ngơi và tận_hưởng .
Đầu_tư dài_hạn
Chỉ 18 năm nữa thôi con_gái sẽ không còn ở bên , chỉ có hai bạn với nhau . Vì_thế hãy lên kế_hoạch đầu_tư lâu_dài cho hôn_nhân của mình .
Hò_hẹn ít_nhất 2 tối/1tháng , thỉnh_thoảng đi du_lịch xa vào cuối tuần . Kéo_dài hơn những nụ_hôn bên bậc cửa , “ gần_gũi ” bất_ngờ vào lúc nghỉ trưa và trò_chuyện yêu_thương ngay trước mặt con_cái ( để con biết cha_mẹ có ý_nghĩa với nhau tới mức nào ) .
Điều đó sẽ mang lại cho con bạn cảm_giác yên_bình tuyệt_vời khi hiểu cha_mẹ rất yêu nhau .
Lãng_mạn dịu_dàng
Vừa có em bé , thật khó để tìm được lúc_nào thảnh_thơi mà không muốn ngủ . Song có một_cách rất hay để bạn kết_hợp chăm_sóc con và chính hai vợ_chồng .
Buổi tối , khi chàng vỗ_về bé ngủ , bạn hãy đến bên ôm lấy hai cha_con . Thật nhẹ_nhàng , kề sát và lãng_mạn . Anh_ấy chắc_chắn cảm_nhận được sự thiêng_liêng của tình_cảm gia_đình .
Huyền_Trang_Theo MSN

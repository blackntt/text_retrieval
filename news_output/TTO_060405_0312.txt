﻿ Giá trái_cây tại các chợ đầu_mối khu_vực ĐBSCL ngày 4-4-2006
Chợ_Tam_Bình ( TP.HCM ) : mức giá từ 8.000-12.000đ/kg . l Bưởi_Năm_Roi : Chợ_Tam_Bình ( Vĩnh_Long ) : giá 6.000-7.000đ/kg . Chợ_Vĩnh_Kim ( Tiền_Giang ) : giá từ 7.600 - 8.200đ/kg , tăng 200đ/kg . Chợ_Tam_Bình ( TP.HCM ) : giá phổ_biến từ 110.000 - 120.000đ/chục , giảm 20.000đ/chục so với ngày hôm_qua .
Nhãn da_bò : Nhãn tiêu da_bò tại chợ Vĩnh_Kim ( Tiền_Giang ) từ 8.500 - 12.500đ/kg , tăng 500đ/kg . Chợ_Tam_Bình ( Vĩnh_Long ) 3.000đ/kg . Chợ_Tam_Bình ( TP.HCM ) nhãn da_bò từ 8.000đ - 9.000đ/kg tăng 2.000đ/kg , nhãn xuồng cơm vàng 27.000 - 28.000đ/kg .
Các loại trái_cây khác : Chợ_Tam_Bình ( Vĩnh_Long ) : nho đỏ , nho xanh 16.000đ/kg ; mãng_cầu dai 12.000đ/kg ; xoài giá 10.000đ/kg , tăng 1.000đ/kg , sabôchê : 3.500đ/kg . Táo , lê Trung_Quốc từ 11.000 - 12.000đ/kg .
Chợ_Vĩnh_Kim ( Tiền_Giang ) : cam mật từ 5.700 - 6.700đ/kg ; vú_sữa Lò_Rèn từ 9.600 - 15.000đ/kg ; măng_cụt 6,400 đ/kg ; sabôchê lồng mứt : 4.600 - 6.000đ/kg ; xoài_cát Hòa_Lộc giá từ 13.000-17.000đ/kg , xoài_cát chu từ : 5.800 - 7.600đ/kg , giảm 200đ/kg .
Chợ_Tam_Bình ( TP.HCM ) : quít đường giá từ 10.000 - 12.000đ/kg , tăng 2.000đ/kg ; thanh_long Long_Bình 7.000 - 8.000đ/kg , giảm 2.000đ/kg , dứa 3.000 - 4.000đ/trái ; sầu_riêng khổ_qua từ 12.000 - 15.000đ/kg , giảm 3.000đ/kg .
( Nguồn : Trung_tâm Thông_tin - Bộ Nông nghiệp & amp ; phát_triển nông_thôn )
PV

﻿ Cá_cược dịp World_Cup có_thể lên đến hàng tỉ bảng
Cá_độ ở Anh lúc_nào cũng rất phổ_biến nhưng việc xóa_bỏ thuế cho người cá_cược năm 2001 và việc gia_tăng các địa_chỉ cá_cược trên mạng Internet đã tăng mạnh doanh_thu cá_cược hằng năm từ 7 tỉ bảng năm 2000 lên tới khoảng 50 tỉ bảng hiện_nay , theo các nhà_phân_tích .
“ Nước_Anh là trung_tâm cá_cược của thế_giới và việc trao_đổi ngoại_tệ cá_cược lớn nhất diễn ra tại Anh , chính đây là nơi phần_lớn tiền dành cho World_Cup sẽ được chi_tiêu ” , theo giáo_sư Leighton_Vaughan_Williams , một nhà tư_vấn về đánh_bạc cho chính_quyền Anh .
Mặc_dù phần_lớn việc cá_cược ở Anh vẫn còn diễn ra nơi những nhà đánh_cá chuyên_nghiệp nhưng nhiều người đang chọn_lựa cá_cược qua Internet , qua điện_thọai hoặc qua truyền_hình tương_tác . Việc tiếp_cận với truyền_hình vệ_tinh và truyền_hình_cáp truyền chương_trình thể_thao trực_tiếp cũng cho_phép người_ta cá_cược , trong_suốt trận đấu , ngay tại nhà .
K.NH ẬT ( Theo China_Daily )

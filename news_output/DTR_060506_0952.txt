﻿ Trương_Ngọc_Ánh vào_vai nóng_bỏng ?
Vì chi_tiết " bán mình " của vai Kiều trong Sài_Gòn nhật_thực , Trương_Ngọc_Ánh liên_tục bị vặn hỏi có đóng nhiều cảnh nóng không .
Nàng_Kiều mà Trương_Ngọc_Ánh thủ diễn trong bộ phim Sài_Gòn nhật_thực ( ĐD Othello_Khanh ) bấm máy ngày 4/5 vừa_qua , có số_phận khá giống Thúy_Kiều của Nguyễn_Du . Đó là một cô_gái trẻ đẹp đã có hôn ước nhưng chấp_nhận bán mình để trả nợ thay cho cha dượng và không_thể_nào lường được những bất_trắc đang chực_chờ mình ở phía trước .
Khi bị vặn hỏi về chuyện " nóng " hay " nguội " , ông Đặng_Tất_Bình , GĐ Hãng phim Truyện 1 , đơn_vị đồng sản_xuất phim này đã khéo trả_lời : " Các bạn cho_biết thế_nào là cảnh nóng thì Trương_Ngọc_Ánh sẽ cho_biết nó có ... nóng hay không ! "
NSƯT Như_Quỳnh trong vai bà Tú , mẹ của Kiều thì " cảnh_giác " : " Chẳng có cảnh nóng nào cả ! " .
Còn_Kiều thì thủng_thẳng , không phủ_nhận cũng chẳng khẳng_định đến sốt_ruột : " Độ nóng của vai_diễn không phải vì mặc đồ hở_hang hay kín_đáo . Nàng_Kiều của Ánh có nóng_bỏng hay không thì mọi người ... hãy chờ ! "
Thời_gian qua , Trương_Ngọc_Ánh khá có duyên với những bộ phim Việt_kiều hoặc phim hợp_tác . Một_vài phim trước_đây , chẳng_hạn trong Hạt mưa rơi bao_lâu , nàng An của Ánh có một_vài " xen " khá ngọt_ngào , quyến_rũ với các nhân_vật nam nên thiên_hạ tiếp_tục thắc_mắc cũng phải .
Chẳng biết nàng Kiều của Trương_Ngọc_Ánh trong Sài_Gòn nhật_thực có nóng thật hay không , nhưng xem qua vài cảnh quay , thấy nó cũng chẳng ... nguội mấy . Theo L . H_Vietnamnet

﻿ Mùi tham_nhũng ?
- Có_lý ! Nhưng còn cái mùi đang rất thời_sự , bà_con đang rất cần biết : mùi tham_nhũng , nó ra_sao nhỉ ?
- Cái mùi này khó định à nghe ! Tui nghĩ nó có_thể là mùi thịt kho_tàu - mùi nhà giàu , nhà có tiền ( tất_nhiên rồi , tham_nhũng để giàu , giàu hơn ! ) . Có_thể lại là mùi thơm nước_hoa mắc tiền_của mấy em chân dài ( có tiền làm_gì ? Để ... mua tiên chứ ! ) . Nhưng cũng có_thể là mùi tanh_hôi của các loại chất_thải ( cái gì cũng xơi , ắt phải nặng_mùi ) .
- Vậy_là nó có_thể biến_hóa đủ màu , đủ mùi . Cô Tư nhà_văn giúp bà_con nhận rõ cái mùi đặc_biệt này được không ?
- Không_chỉ cô Tư nhà_văn , bà_con mình ai có ý gì hay về cái mùi tham_nhũng , gửi vài dòng về cho anh Bút_Bi . Tập_họp lại chắc cũng nhiều điều thú_vị , nhiều điều mà các nhà “ mùi học ” cũng phải ... cúi đầu ! Chờ coi ...
BÚT_BI

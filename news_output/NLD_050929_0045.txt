﻿ 4 VĐV_VN dự giải marathon lớn nhất hành_tinh
( NLĐ ) - Tại cuộc họp_báo giới_thiệu nhà tài_trợ Standard_Chartered vào chiều_qua , 28-9 , HLV trưởng đội_tuyển điền_kinh VN Dương_Đức_Thủy cho_biết , 4 VĐV Nguyễn_Tiến_Xuân , Nguyễn_Văn_Khoa , Nguyễn_Chí_Đông và Nguyễn_Kiên_Trung sẽ tham_dự giải marathon lớn nhất hành_tinh năm 2006.Các VĐV_VN sẽ tranh tài cùng khoảng 80.000 VĐV trên toàn thế_giới ở 4 chặng đua nghiệt_ngã tại Nairobi ( Kenya ) , Singapore , Ấn Độ và Hồng_Kông để giành tổng giải_thưởng trị_giá 1,43 triệu USD .
N . N

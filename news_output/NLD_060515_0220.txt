﻿ # # D : \ Corpus \ HTML\ NLD\ www.nld.com.vn \ tools \ print.asp-news _ id = 150926.htm
Dựng lại 80 ngôi nhà bị sập do lốc
( NLĐ ) - Ngày 14-5 , UBND huyện Hướng_Hóa , tỉnh Quảng_Trị , cùng 2 doanh_nghiệp xây_dựng Hồng_Kỳ và Phú_Thăng đã dựng lại 80 ngôi nhà tại bản kinh_tế mới , xã A_Dơi bị lốc thổi sập .
Những gia_đình bị thiệt_hại nặng được huyện cứu_trợ lương_thực , giúp tấm_lợp để bà_con sớm ổn_định cuộc_sống . Trước đó , ngày 12-5 , một cơn lốc lớn bất_ngờ quét qua khu_vực này làm 80 ngôi nhà mới dựng bị sập và tốc mái .
L.An

﻿ Tuyển_chọn thành_viên cho Trại thanh_niên quốc_tế
Tham_gia chương_trình này , các bạn trẻ được đào_tạo những kỹ_năng để có_thể trở_thành những " công_dân quốc_tế " như : kỹ_năng lãnh_đạo , kỹ_năng thương_thảo , làm_việc theo nhóm , giao_tiếp , trình_bày và bản_sắc văn_hóa .
50 bạn trẻ đến từ năm nước : Philippines , Indonesia , Malaysia , Việt_Nam và vương_quốc Anh sẽ tham_gia nhiều hoạt_động cộng_đồng tại địa_phương trong_suốt thời_gian trại . Mục_tiêu chính của chương_trình là làm dấy lên ý_thức thanh_niên sống vì cộng_đồng .
Sau khi kết_thúc chương_trình , Hội_đồng Anh dự_định phát_động một cuộc_thi giữa các thành_viên cho những ý_tưởng hay nhất về các hoạt_động của thanh_niên hỗ_trợ phát_triển cộng_đồng và tài_trợ cho những ý_tưởng đoạt giải được thực_hiện .
Một_số SV trường ĐH được mời để nộp hồ_sơ tham_gia như : ĐH Quốc_gia , ĐH Ngoại_thương , Kinh_tế , Kiến_trúc , Nông_lâm ( chi_tiết sẽ do Đoàn trường phát_động ) ... 20 thí_sinh có bài viết hay nhất sẽ trải qua kỳ tuyển_chọn hùng_biện bằng tiếng Anh để chọn ra 10 người tham_gia trại . Sau đó , các thành_viên có một đợt tập_huấn trù_bị tại Phan_Thiết vào cuối tháng 12-2005 trước khi lên_đường đi Philippines .
BÍCH_DẬU

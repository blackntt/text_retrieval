﻿ Có_thể du_học Anh qua mạng ?
- Trả_lời của Trung_Tâm_Thông_Tin_Hội_Đồng_Anh :
Đối_với việc học qua mạng , chúng_tôi giới_thiệu một công_cụ tìm_kiếm các khóa học ở hình_thức online tại trang_web : www.educationuk-vietnam.org . Vui_lòng nhấp chuột vào Online & amp ; distance learning . Sau đây là một ví_dụ về việc tìm khoá học Quản_trị Kinh_doanh qua trang_web nói trên :
Dưới đây là danh_sách chính các trường cung_cấp khóa học online , tuy_nhiên ở Anh còn có rất nhiều trường khác có khóa học theo hình_thức này :
1 . Interactive_University www.interactiveuniversity.net/students/students
2 . University of London_External_Programme www.londonexternal.ac.uk
3 . The_Open_University ( OU ) www.open.ac.uk
4 . MBAs www.londonexternal.ac.uk
Sinh_viên nên liên_lạc trực_tiếp với các trường để biết thêm về yêu_cầu đầu_vào và cách nộp đơn cho mỗi trường .
Ngoài_ra sinh_viên cũng có_thể tham_khảo thêm thông_tin ở trang Part-time and Distance learning trong Education_Information_Sheets tại website : http : / / www.britishcouncil.org/learning-education-information-sheets
Liên_quan đến học_bổng du_học ở Vương quốcc Anh , mời bạn vào trang_web : www.educationuk-vietnam.org để tìm các thông_tin phù_hợp cho mình . Ứng_viên nên liên_lạc trực_tiếp với các trường để biết thêm chi_tiết
Sinh_viên cũng có_thể liên_lạc với Hội_Đồng_Anh theo các địa_chỉ sau :
Email : enquiries@britishcouncil.org.vn
Đia chỉ : British_Council , 25 Lê_Duẩn , Quận 1 , Tp_Hồ_Chí_Minh
Tel : 08 8238 862 , Fax : 08 8232 861
Chúng_tôi rất sẵn_sàng đón_tiếp và cung_cấp thông_tin cho các bạn có nhu_cầu tìm_hiểu .
Với những câu_hỏi về du_học Mĩ , hiện phía Lãnh_sự_quán Mĩ đang xem_xét trả_lời . TTO sẽ đăng_tải phần giải_đáp cho các câu nói trên ngay khi nhận được phúc_đáp của cơ_quan này .
TTO

﻿ Đêm tân_hôn nhận được tin nhắn “ âu_yếm ” vợ
Hai vợ_chồng trẻ đang hạnh_phúc tràn_trề trong ngày cưới và đêm tân_hôn chưa kịp trọn_vẹn thì một tin nhắn đầy yêu_thương gửi tới cho người vợ trẻ . Anh chồng tức điên tát vào mặt vợ và bỏ đi trong cơn giận_dữ .
“ Mặc_dù em đã có chồng nhưng anh vẫn yêu em ” là tin nhắn một ai đó gửi vào điện_thoại của cô vợ 19 tuổi chỉ ít phút trước khi hai người “ động_phòng ” lúc 2 giờ sáng ngày 14/5 .
Người chồng 25 tuổi này không tin lời giải_thích của cô vợ rằng cô không biết người nhắn_tin là ai . Anh này tát vợ và ném điện_thoại rồi chạy ra khỏi nhà , tờ Bernama cho_biết .
Sau khi nhận được đơn_kiện của cô vợ và gia_đình vợ , một cảnh_sát khu_vực đã bắt chú_rể về đồn để điều_tra . Ông này nói đây là trường_hợp duy_nhất xảy_ra trong 33 năm làm nghề điều_tra của ông .
N . H_Theo_Reuters

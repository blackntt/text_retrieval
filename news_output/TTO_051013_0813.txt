﻿ Tiếp_tục đổi_mới phương_thức vận_động quần_chúng
Để đáp_ứng yêu_cầu và nhiệm_vụ của công_tác dân_vận trong thời_kỳ mới , đồng_chí Tòng_Thị_Phóng - bí_thư Trung_ương Đảng , trưởng Ban Dân vận trung_ương - nêu rõ : đội_ngũ làm công_tác dân_vận cần tiếp_tục đổi_mới nội_dung , phương_thức vận_động quần_chúng nhằm nâng cao chất_lượng , hiệu_quả của công_tác dân_vận sát với mục_tiêu , yêu_cầu thực_tế , hướng mạnh về cơ_sở với phương_châm hành_động “ trọng dân , gần dân , hiểu dân , học dân và có trách_nhiệm với dân ” , “ nghe dân nói , nói dân hiểu và làm dân tin ” .
Nhân_dịp này , Ban Dân vận trung_ương đã trao_tặng kỷ_niệm_chương 29 cán_bộ có nhiều đóng_góp cho công_tác dân_vận .
TTXVN

﻿ Gần 200 người chết vì lũ_quét ở Indonesia
Tính đến hôm_nay đã có 188 người chết vì lũ_quét và lở đất tại tỉnh Nam_Sulawesi của Indonesia . Con_số này sẽ còn tăng cao vì còn gần 150 người vẫn mất_tích sau khi nước lũ đổ ồ_ạt trên đảo và có_thể họ đã bị cuốn ra biển .
Lũ_quét xảy_ra tại 7 quận của tỉnh Nam_Sulawesi , trong đó bị nặng nhất_là Sinjai , nơi có ít_nhất 174 người thiệt_mạng .
Một nạn_nhân may_mắn sống_sót kể lại trong bệnh_viện Sinjai rằng , ông đã bị cuốn ra biển khi nước lũ ập tới nhà ông sáng sớm_hôm thứ_ba . Ông được cứu nhờ bám vào được một mảnh gỗ trong_suốt 9 giờ , trong khi vợ và 2 con_trai của ông vẫn bị mất_tích .
Quân_đội và cảnh_sát Indonesia cùng phối_hợp với người_dân địa_phương tiếp_tục mở chiến_dịch tìm_kiếm người bị nạn . Một quan_chức phụ_trách hoạt_động này là Moersen_Buana cho_biết , nước đang rút tạo điều_kiện cho nỗ_lực cứu_trợ .
" Nhưng điều_kiện vệ_sinh đang trở_thành một vấn_đề . Người_dân không_thể sử_dụng các toilet bình_thường vì hệ_thống nước đã hoàn_toàn bị phá_hủy . Bệnh tiêu_chảy và viêm da đã bắt_đầu xuất_hiện " , ông Buana lo_ngại .
Những cơn mưa lớn thường_xuyên xảy_ra ở quốc_đảo nhiệt_đới Indonesia đã gây ra lũ và lở đất . Nạn chặt phá rừng bừa_bãi càng góp_phần làm dạng thiên_tai này thêm tàn_khốc . Trận lở đất trên đảo Java hồi tháng_một vừa_qua từng cướp đi sinh_mạng hơn 120 người .
Sulawesi là một hòn đảo giàu tài_nguyên của Indonesia , nơi có rất nhiều khu mỏ đang khai_thác . May_mắn là những hầm_mỏ này nằm cách xa nơi xảy_ra lũ_quét .
Theo Đình_Chính_Vnexpress / R euters , BBC

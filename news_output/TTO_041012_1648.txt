﻿ EFEO hợp_tác nghiên_cứu di_tích Hoàng_thành
- Đó là một bản thoả_thuận chung giữa hai cơ_quan trong lĩnh_vực hợp_tác nghiên_cứu khoa_học ( KH ) , làm khung cho 3 dự_án nghiên_cứu lớn là sự giao_lưu văn_hoá của VN với các nước láng_giềng , hệ_thống_hoá kho văn_bia của Viện_Hán_Nôm , và nghiên_cứu khảo_cổ , lịch_sử tại khu di_tích Hoàng_thành Thăng_Long .
Đối_với dự_án nghiên_cứu tại Hoàng_thành , sự hợp_tác của EFEO sẽ gồm những gì ?
EFEO đã có một danh_sách những công_việc dự_định hợp_tác với các chuyên_gia VN tại di_tích Hoàng_thành :
- Thành_lập tại khu di_tích một trung_tâm tư_liệu về thành cổ VN và thế_giới
- Nghiên_cứu xác_định vị_trí chính_xác của tường thành qua các thời_kỳ
- Nghiên_cứu khảo_cổ_học ( làm cataloge các di_vật khai_quật được , tìm giải_đáp những bí_ẩn xung_quanh các kết_quả khảo_cổ . v . v ... )
- Nghiên_cứu hệ_thống nước và thoát nước của Hoàng_thành , bảo_tồn di_vật , di_tích v . v ...
- Chính_phủ Pháp đã ký một dự_án tài_trợ 100.000 USD ( kinh_phí một phần là của Bộ Ngoại giao Pháp và một phần là của EFEO ) cho việc nghiên_cứu tại khu di_tích này từ tháng 7 đến hết năm 2004 .
Hiện_tại , TS Philippe_Papin - một chuyên_gia giàu kinh_nghiệm của EFEO , đã chuyển tới làm_việc tại Hà_Nội trong 1 năm để phụ_trách dự_án . Nhiều chuyên_gia xuất_sắc khác của EFEO cũng đang nỗ_lực trong dự_án này ( TS Andrew_Hardy - trưởng đại_diện EFEO Hà_Nội , chuyên_gia sử_học Philippe_Le_Failler , chuyên_gia bảo_tồn Bertrand_Porte . v . v ... ) .
Tuy_nhiên , bản thoả_thuận EFEO vừa ký với Viện KHXH_VN mới là thoả_thuận_khung . Việc triển_khai dự_án chi_tiết ( dự_định trong khoảng 3 năm ) thì còn phải đợi quyết_định chính_thức của Chính_phủ VN về khu di_tích .
Hà_Nội là nơi ra_đời của EFEO . Tuy_nhiên , hiện_nay văn_phòng EFEO tại đây chỉ có 2 chuyên_gia chính . Phải_chăng sức hấp_dẫn của mảnh đất này với EFEO đã giảm ?
- EFEO chỉ có 42 thành_viên chính_thức , và có 17 văn_phòng đại_diện tại 11 nước Châu Á . Vì_vậy , 2 chuyên_gia EFEO làm_việc cùng tại một văn_phòng thì cũng không phải là ít . Hơn_nữa , ngoài 2 chuyên_gia chính_thức , văn_phòng EFEO Hà_Nội cũng còn một_số chuyên_gia khác nữa .
Tuy_nhiên , trong thời_gian tới , EFEO cũng có dự_định đầu_tư nhiều hơn_nữa cho văn_phòng EFEO Hà_Nội . VN luôn đứng trong danh_sách quan_tâm hàng_đầu của EFEO . Đây là nơi " chôn_rau_cắt_rốn " của EFEO . Mối quan_hệ giữa chúng_ta rất đặc_biệt và ngày_càng mật_thiết . Lịch_sử , văn_hoá VN là một mảnh đất đầy quyến_rũ với chúng_tôi .
Sau những năm 50 , EFEO không có văn_phòng tại Hà_Nội , mãi đến 1993 , chúng_tôi mới nối lại được vết cắt này . Và giờ_đây , các chuyên_gia EFEO đang rất nỗ_lực để " bù " lại thời_gian đã mất đó với một số_lượng lớn các công_trình nghiên_cứu về VN cũng_như các dự_án hợp_tác ...
Theo LĐ

﻿ Thanh_tra Công_ty TNHH_T & amp ; H
( NLĐ ) - Ngày 24-4 , Thanh_tra Sở Bưu chính - Viễn_thông TPHCM đã công_bố quyết_định và triển_khai thanh_tra đối_với Công_ty TNHH_T & amp ; H , trụ_sở tại 202 Bis_Nguyễn_Thị_Minh_Khai , quận 3 - TPHCM .
Thời_gian thanh_tra trong vòng 20 ngày . Công_ty TNHH_T & amp ; H ( thành_lập năm 1992 ) là một trong những đơn_vị tư_nhân đầu_tiên trong lĩnh_vực CNTT có chức_năng cung_cấp các phần_cứng , phần_mềm ứng_dụng và các giải_pháp kỹ_thuật CNTT . Đơn_vị này cũng đã được UBND_TPHCM chỉ_định thầu nhiều dự_án thuộc Chương_trình 112 .
Huy-Văn

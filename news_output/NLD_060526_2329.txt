﻿ Thiết_bị giải_trí đa_năng
Sản_phẩm mới của Pioneer vừa được tung ra thị_trường hồi trung_tuần tháng này được giới chuyên_môn đánh_giá cao . AVIC-Z1 chắc_chắn sẽ là người bạn đồng_hành không_thể thiếu của những_ai thường_xuyên đi_lại trên những tuyến đường dài .
Ngoài chức_năng cài lệnh bằng giọng nói để mở radio , DVD và CD player với một thư_viện nhạc MP3 dung_lượng 30 GB , AVIC-Z1 có_thể báo âm tên từng con đường và số đại_lộ từ cơ_sở_dữ_liệu cài sẵn . Đặc_biệt nó có_thể bắt được sóng đài Sirius hay radio vệ_tinh XM , hiển_thị tuyến đường bị tắc_nghẽn , kết_nối ĐTDĐ qua cổng Bluetooth , chạy i Pod và thu_hình từ camera phía sau , đồng_thời bạn cũng có_thể kết_nối dễ_dàng với màn_hình rộng ...
A . H

﻿ Giá cổ_phiếu VNM và KDC gấp 10 lần mệnh_giá
Giá CP của Công_ty_cổ_phần Kinh_Đô ( KDC ) là 100.100 đồng/CP , của Công_ty_cổ_phần Sữa VN ( VNM ) là 100.000 đồng/CP . Ngoài_ra còn có một_số CP ngấp_nghé “ câu_lạc_bộ 10 lần mệnh_giá ” là Công_ty_cổ_phần chế_biến thực_phẩm Kinh_Đô miền Bắc ( NKD ) 99.000 đồng/CP , Công_ty_cổ_phần Cơ_điện lạnh ( REE ) 98.000 đồng/CP , Công_ty_cổ_phần Cáp vật_liệu viễn_thông ( SAM ) giá 95.500 đồng/CP , Công_ty_cổ_phần Hóa_An ( DHA ) 93.000 đồng/CP ... Trong năm 2006 , chỉ_số VN ở mức thấp nhất_là đầu năm , khi đó chỉ có 304,23 điểm .
( T.TU . )

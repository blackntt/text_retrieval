﻿ Phạt cơ_sở chế_biến yaourt hơn 14 triệu đồng
( NLĐ ) – Ngày 2-3 , Thanh_tra Sở Y_tế TPHCM đã quyết_định phạt cơ_sở sản_xuất bánh Vinh_Khoa ( quận 2 - TPHCM ) , nơi cung_cấp yaourt khiến hơn 250 học_sinh của 3 trường tiểu_học bán_trú quận Bình_Thạnh ngộ_độc phải nhập_viện cấp_cứu vào ngày 16-1 ( xem Báo_Người_Lao_Động ngày 17-1 ) 14,15 triệu đồng .
Thanh_tra cũng buộc cơ_sở này phải đình_chỉ hoạt_động làm yaourt , bồi_thường viện_phí cho toàn_thể học_sinh bị ngộ_độc phải nhập_viện và những chi_phí liên_quan đến xét_nghiệm . Thanh_tra Sở Y_tế cũng mời ban_giám_hiệu 3 trường liên_quan để xử_phạt với mức 750.000 đồng cho Trường_Chu_Văn_An ( để xảy_ra ngộ_độc lần thứ 2 ) và 300.000 đồng cho 2 trường còn lại .
Ph.Sơn

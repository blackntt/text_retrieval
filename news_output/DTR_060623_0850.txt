﻿ Bojinov " thế thân " cho Mutu
Theo một_số nguồn tin , Fiorentina đang trong quá_trình đàm_phán với Juventus để hy_vọng có_thể đổi Valeri_Bojinov lấy Adrian_Mutu .
Nếu “ Lão bà ” thành Turin bị tụt hạng , rất có_thể Mutu sẽ tìm cho mình một con đường riêng mới . Và_Viola đã đánh đúng vào thời_điểm nhạy_cảm khi nói họ muốn có được sự phục_vụ của tuyển_thủ quốc_tế người Rumani này .
Giám_đốc thể_thao của đội bóng vùng Tuscan đã liên_hệ với Juve để đưa ra đề_nghị . Tuy_nhiên , các cuộc đàm_phán cho_đến giờ vẫn chưa đi đến một kết_quả cụ_thể .
Juventus định_giá cựu tiền_đạo của Chelsea khoảng 7 triệu bảng - một con_số mà Fiorentina không cảm_thấy hài_lòng cho lắm .
Thay vào đó , Fiorentina đề_nghị Bianconeri đổi bằng tiền_đạo trẻ người Bungari_Bojinov . Đây là cầu_thủ đã thi_đấu không mấy thành_công kể từ khi chuyển đến Viola với hợp_đồng chuyển_nhượng trị_giá 11 triệu bảng từ Lecce .
Mặc_dù tân giám_đốc thể_thao của Juventus ông Alessio_Secco tỏ ra rất mến_phục tiền_đạo của Viola nhưng Bojinov dường_như không nằm trong kế_hoạch chiêu_mộ của CLB .
Sau khi đã thỏa_thuận thành_công hợp_đồng cho Fiorentina mượn Manuele_Blasi , hai bên có_thể gặp lại nhau vào tuần tới để thảo_luận thêm và tìm đến một sự đồng_thuận cho vụ Bojinov - Mutu .
HLV Cesare_Prandelli của Fiorentina được biết đang rất muốn tái_ngộ cùng “ kẻ tội_đồ ” ngày nào của Chelsea vì hai người đã từng có thời_gian gắn_bó và làm_việc cùng nhau rất thành_công tại Parma .
Đến_Juventus bằng hợp_đồng chuyển_nhượng tự_do , nhưng trên thực_tế Mutu không có nhiều sự lựa_chọn . Nhiều khi anh chỉ được HLV Fabio_Capello đưa vào sân như một tiền_vệ tự_do hơn là so với vai_trò của một tiền_đạo hộ công . Tuy_nhiên , tiền_đạo thất_thế này cho_biết thực_sự cảm_thấy hài_lòng với mùa giải đầu_tiên của mình tại CLB . Hoàng_Hiệp

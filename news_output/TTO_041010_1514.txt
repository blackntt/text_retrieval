﻿ Tổng_thống Hàn_Quốc và thủ_tướng Đức thăm Việt_Nam
Đây là lần đầu_tiên Tổng_thống Roh_Moo - Huyn đến thăm VN kể từ khi nhậm_chức vào năm 2003 và là chuyến thăm thứ_ba của Tổng_thống Hàn_Quốc từ trước đến nay .
Sáng_mai , 11-10 , Tổng_thống Hàn_Quốc sẽ vào TP.HCM tham_dự nhiều hoạt_động ủng_hộ quá_trình hợp_tác kinh_tế giữa hai nước . Ông sẽ đi thăm khu_công_nghiệp Thủ_Đức , Bình_Dương và tham_gia một_số hoạt_động quan_trọng của doanh_nghiệp Hàn_Quốc tại TP.HCM .
Trong thời_gian lưu lại VN , thủ_tướng Đức_Schroeder đã chứng_kiến lễ ký_kết năm bản thỏa_thuận hợp_tác về tài_chính , kỹ_thuật và thương_mại giữa hai quốc_gia với tổng giá_trị lên đến 260 triệu USD .
K.CH .

﻿ “ Ông trùm của các ông trùm ” ở Ý sắp ra hầu tòa
Việc bắt_giữ Bernardo_Provenzano đã mở ra nhiều cuộc điều_tra mới - Ảnh : AP_TTO - Bernardo_Provenzano , được xem là ông trùm mafia Ý tại Sicily , sẽ phải ra hầu tòa lần đầu_tiên vào ngày 2-5 tới sau hơn 40 năm trốn_tránh pháp_luật .
Theo tờ báo Ý_La_Repubblica , Provenzano , năm nay 73 tuổi , sẽ ra hầu tòa do có liên_quan đến nhiều vụ giết người trong những năm 1980 .
Hiện_Provenzano đang bị giam_giữ biệt_lập tại một nhà_tù được thắt chặt an_ninh tại Terni , miền trung nước Ý . Phía nhà_tù đã từ_chối cho_phép Provenzano tiếp_cận với báo_chí , radio hay truyền_hình .
Provenzano bị bắt_giữ vào thứ_ba tuần trước tại một nông_trang nhỏ gần Corleone , Sicily . Trước khi bị bắt , Provenzano đã bị xử vắng_mặt hàng chục tội mưu_sát và hơn 10 trát truy_bắt .
Tờ_La_Repubblica cho_hay Provenzano đã từ_chối gặp linh_mục vào ngày lễ Phục_sinh chủ_nhật qua , nhưng đã nhận một bữa ăn lễ Phục_sinh đặc_biệt với gà , bánh_bao , thỏ và chuối , được chuẩn_bị cách xa nhà_bếp của nhà_tù vì các lý_do an_ninh .
Hàng chục thư_từ và tài_liệu được phát_hiện tại nông_trang nơi Provenzano ẩn trốn đang được các chuyên_gia giải_mã . Cảnh_sát nói nhiều phần quan_trọng của các tài_liệu này đã bị mã_hóa hoặc sử_dụng một loạt các tên giả .
TƯỜNG_VY ( Theo BBC )

﻿ Du_học ở Ireland như_thế_nào ?
Em muốn hỏi về du_học Ireland ? Đời_sống , chi_phí học như_thế_nào ? Và em muốn học về Kinh_tế tại đây ! ( baotrinhvn83 @ )
Trả_lời của đại_diện tập_đoàn giáo_dục quốc_tế Study_Group tại VN :
Tại_Ireland có tổng_cộng 9 trường ĐH . Trong đó , trường ĐH Dublin , Trinity_College là 2 trường lâu_đời nhất , thành_lập từ năm 1592 . Tại 9 trường này đều đào_tạo chương_trình từ Cao_đẳng , Đại_học , Thạc sỹ và Tiến sỹ trên nhiều lĩnh_vực .
Về tổng_chi phí sinh_hoạt và học_tập thì còn phụ_thuộc vào từng trường , mức_sống của thành_phố đó và cách sống của mỗi học_sinh . Tuy_nhiên , về tổng_chi phí cho mỗi khóa học Kinh_tế dao_động từ 10.000 EUR / năm cho_đến 18.000 EUR / năm ( bao_gồm cả học_phí , sinh_hoạt_phí ... ) .
Về môi_trường học_tập , sinh_hoạt tại Ireland cũng khá tương_tự như ở Anh Quốc : ở đây có các chương_trình đào_tạo chuyên_sâu , môi_trường sống và học_tập cổ_kính , lâu_đời , khí_hậu lạnh ... Tuy_nhiên , việc học ở đây tương_đối rẻ hơn ở Anh Quốc .
Khuôn_viên ĐH Queen ' s University_Khuôn viên ĐH Queen ' s University
Tại thời_điểm hiện_tại , Ireland chưa phải là 1 quốc_gia du_học phổ_biến cho các du_học_sinh VN . Việc xin Visa thông_thường khá phức_tạp hơn so với du_học 1 số nước khác vì học_sinh có_thể phải gửi hộ_chiếu sang truờng mà mình đăng_kí để xin làm Visa . Cụ_thể là sau khi học_sinh được chấp_nhận vào học tại trường , trường sẽ làm hồ_sơ xin visa cho học_sinh tại Ireland . Sau đó , sẽ gửi visa này về VN cho học_sinh .
Để biết thêm thông_tin chi_tiết , bạn có_thể vào địa_chỉ web sau đây :
http : / / www.interstudy.org/Ireland/Ireland.html . Tại đây có các thông_tin chi_tiết giới_thiệu 5 trường ĐH nổi_tiếng tại Ireland và 1 trường tại Bắc_Ireland :
Theo Tuổi_Trẻ

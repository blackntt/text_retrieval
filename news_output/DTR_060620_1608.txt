﻿ VDB sẽ ưu_tiên cho vay các dự_án có độ rủi_ro lớn
Quỹ_Hỗ trợ phát_triển vừa nhận quyết_định của Thủ_tướng Chính_phủ chuyển sang hoạt_động theo mô_hình ngân_hàng - Ngân_hàng Phát_triển Việt_Nam ( VDB ) . VDB sẽ vận_hành ra_sao ? Báo_giới đã có cuộc phỏng_vấn ông Nguyễn_Quang_Dũng , Tổng_Giám đốc VDB xung_quanh vấn_đề này .
Đâu là sự khác_biệt về “ chất ” giữa VDB và Quỹ_Hỗ trợ phát_triển ( DAF ) ?
Theo quyết_định của Thủ_tướng Chính_phủ , VDB được thành_lập dựa trên Luật các tổ_chức tín_dụng và Luật_Ngân sách nhà_nước . Do_đó khác_biệt căn_bản là VDB hoạt_động theo hình_thức ngân_hàng . Các sản_phẩm sẽ đa_dạng hơn .
Tuy_nhiên , do là ngân_hàng thực_hiện chính_sách nên VDB chịu sự điều_chỉnh của Luật_Ngân sách , kế_thừa mọi quyền_lợi , trách_nhiệm từ Quỹ_Hỗ trợ phát_triển .
Hoạt_động của VDB không vì mục_đích lợi_nhuận , tỷ_lệ dự_trữ bắt_buộc bằng 0% , không phải tham_gia bảo_hiểm tiền gửi . VDB được Chính_phủ bảo_đảm khả_năng thanh_toán , được miễn nộp thuế và các khoản nộp ngân_sách nhà_nước theo quy_định .
VDB được phép huy_động vốn của các tổ_chức trong và ngoài nước để thực_hiện tín_dụng đầu_tư phát_triển và tín_dụng xuất_khẩu của Nhà_nước .
VDB cho vay đầu_tư phát_triển ; hỗ_trợ sau đầu_tư ; bảo_lãnh tín_dụng đầu_tư , cho vay xuất_khẩu ; bảo_lãnh tín_dụng xuất_khẩu ; bảo_lãnh dự_thầu và bảo_lãnh hợp_đồng xuất_khẩu .
Ông có_thể nói rõ hơn về chính_sách tín_dụng ưu_đãi của VDB ?
Như trên tôi đã nói , VDB sẽ thực_hiện chính_sách tín_dụng đầu_tư phát_triển và tín_dụng xuất_khẩu do Chính_phủ quy_định . Nói về vay ưu_đãi cần hiểu cặn_kẽ hơn và toàn_diện hơn , không nên bó_hẹp là bao_cấp về lãi_suất vốn vay .
VDB sẽ ưu_tiên cho vay các dự_án có độ rủi_ro lớn mà tư_nhân hoặc các nhà_đầu_tư nước_ngoài , các doanh_nghiệp ngần_ngại không đầu_tư , nhất_là ở những địa_bàn khó_khăn , đặc_biệt khó_khăn . VDB không tính đến lợi_nhuận của bản_thân VDB khi đầu_tư , nhưng dự_án phải có hiệu_quả chắc_chắn thì VDB mới cho vay .
Ưu_đãi nữa_là thời_gian vay vốn , VDB cho vay những dự_án lớn có thời_gian hoàn_trả vốn dài đến 10 năm , thậm_chí 15 năm-điều mà ít ngân_hàng thương_mại có_thể làm được .
Bên_cạnh đó , phần_lớn các dự_án sẽ được vay , sẽ đảm_bảo tiền vay bằng chính tài_sản hình_thành từ vốn vay . Trường_hợp phải thế_chấp thì chỉ cần thế_chấp 30% là đã có_thể vay 100% . Tới đây tỷ_lệ này dự_kiến hạ xuống còn 15% .
Đây chính là những ưu_đãi rất lớn mà nhiều ngân_hàng thương_mại không có được . Như_vậy , VDB không_chỉ ưu_đãi về lãi_suất cho vay như trước_đây nữa .
Nhiều khả_năng chúng_ta sẽ gia_nhập WTO vào cuối năm nay . Việc hỗ_trợ tín_dụng ưu_đãi cho đầu_tư và xuất_khẩu hàng_hóa liệu có vi_phạm các quy_định của tổ_chức này ?
Hiện_Bộ_Tài chính đang chủ_trì cùng Bộ Thương mại , Ngân_hàng Nhà_nước , Bộ Kế hoạch và Đầu_tư nghiên_cứu để ban_hành cơ_chế , chính_sách , hoạt_động của VDB . Ban soạn_thảo đang tham_khảo ý_kiến của đoàn đàm_phán gia_nhập WTO để không vi_phạm các thông_lệ quốc_tế trong hoạt_động hỗ_trợ .
Cũng cần nói thêm rằng , hội_nhập sâu , nhưng chúng_ta vẫn phải vận_dụng các chính_sách ưu_đãi cho phù_hợp với hoàn_cảnh Việt_Nam .
Trong lần chuyển_đổi này VDB sẽ làm_gì để lành_mạnh hóa và minh_bạch hóa tình_hình tài_chính , thưa ông ?
Khi thành_lập DAF , Chính_phủ đã cấp 5.000 tỉ đồng_vốn điều_lệ . Nhưng đó không phải là “ tiền tươi ” mà chiếm đến một_nửa là các dự_án bàn_giao từ Tổng_cục Đầu_tư phát_triển .
Cho_đến bây_giờ xử_lý vấn_đề nợ của các dự_án bàn_giao này vẫn chưa được dứt_điểm . Đó cũng là điều chúng_tôi cần rút kinh_nghiệm cho lần chuyển_đổi sang hoạt_động theo mô_hình ngân_hàng .
Để chuyển sang ngân_hàng , chúng_tôi đang tổ_chức kiểm_kê và bàn_giao vốn , tài_sản và nhân_sự để rõ_ràng , minh_bạch hóa các vấn_đề tài_chính .
Mang tên mới nhưng liệu nghiệp_vụ ngân_hàng của đội_ngũ nhân_viên có tương_xứng ?
Đây không phải là chuyển một cái tên mà chuyển_đổi hoạt_động của cả hệ_thống theo mô_hình hoạt_động mới phù_hợp với yêu_cầu phát_triển và hội_nhập .
Do_đó vấn_đề con_người là ưu_tiên số_một . VDB có thuận_lợi lớn là đội_ngũ cán_bộ hiện_nay trên 80% có trình_độ đại_học . Trên nền_tảng đó , chúng_tôi tiếp_tục đào_tạo nghiệp_vụ ngân_hàng mà trọng_tâm là nghiệp_vụ thanh_toán và quản_trị ngân_hàng . Theo Thời báo Kinh_tế Sài_Gòn

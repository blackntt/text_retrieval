﻿ Bạn “ liều ” đến mức nào ?
Hoài_bão và những ước_mơ - Bạn sẵn_sàng theo_đuổi không ? Khi nhìn thấy Vừng đã mở_cửa rồi , bạn dám vào mà chẳng sợ nguy_hiểm chứ ? Chúng_ta hãy cùng xem mình “ liều ” đến mức nào !
1 . Trong vòng 10 năm tới , bạn hy_vọng mình sẽ :
a . Lập gia_đình b . Có một công_việc hợp sở_thích nhưng lương không cao c . Một công_việc lương hấp_dẫn dù không thích lắm
2 . Trong 20 năm tới , bạn muốn :
a . Kiếm đủ tiền để sống b . Có một khoản tiền kha_khá dắt lưng c . Có thật nhiều tiền
3 . Trong các nghề sau , bạn thích nhất nghề nào ?
Kế_toán ; Nhà_Xây dựng ; Giáo_viên ; Nhà_báo ; Nghệ_sĩ nhiếp_ảnh ; Chính_trị_gia ; Kĩ_sư ; Cảnh_sát ; Ca_sĩ ; Diễn_viên ; Bác_sĩ
4 . Nâng cao mức_sống có quan_trọng với bạn không ?
5 . Bạn có nghĩ rằng người giàu nên giúp_đỡ kẻ nghèo không ?
6 . Bạn muốn có baby năm bao_nhiêu tuổi ?
a . 18 - 22 b . 23 - 26 c . 27 - 30 d . ngoài 30
7 . Khi bạn tham_gia một trò_chơi , bạn có luôn muốn mình giành chiến_thắng không ?
8 . Bạn có_thể nói một lời nói_dối vô_hại được không ?
9 . Theo bạn , cuộc_sống của người giàu_có hạnh_phúc và thú_vị hơn người khác không ?
10 . Bạn làm_việc vất_vả có phải chỉ vì hai chữ “ thành_công ” không ?
11 . Tìm được một công_việc khi vừa_mới ra trường , bạn có đi làm_việc đó ngay không ?
12 . Bạn muốn mình có nhiều tiền hơn bố_mẹ không ?
13 . Bạn có đồng_tình với câu nói này không ?
“ Mọi người đều sống vì bản_thân mình ”
14 . Bạn tự đánh_giá mình có chăm_chỉ không ?
15 . Điều gì sau đây quan_trọng nhất với bạn ?
a . Tình_yêu b . Hạnh_phúc c . Tiền_bạc d . Sức_khoẻ
Bạn hãy tính điểm cho từng câu
Câu 1 và câu 2
a : 0đ ; b : 5đ ; c : 10đ
Câu 3
0đ : bác_sĩ , hoạ_sĩ , nghệ_sĩ nhiếp_ảnh 2đ : Nhà xây_dựng , cảnh_sát , giáo_viên , nhà_báo 5đ : Kĩ_sư , ca_sĩ / diễn_viên
10đ : chính_trị_gia , kế_toán
Câu 4 và câu 5
Có : 10đ ; không : 0đ
Câu 6
a : 0đ ; b : 2đ ; c : 5đ ; d : 10đ
Câu 7 đến câu 14
Có : 10đ ; không : 0đ
Câu 15
a : 0đ ; b : 5đ ; c : 10đ ; d : 0đ
Kết_quả đây ! ! !
0 - 50 đ : Bạn dường_như không có tham_vọng bao_giờ . Bằng_lòng với những gì mình có , ý_nghĩ ấy luôn thường_trực trong bạn . Điều đó có_thể khiến bạn trở_nên thụ_động và tụt lại so với cuộc_sống xung_quanh . Đừng phí_hoài tuổi_trẻ như_vậy . Hãy tự xốc lại mình và cùng khám_phá thế_giới
50 - 100đ : Hoài_bão và tham_vọng - bạn có nhiều đấy nhưng lại chẳng hề muốn bỏ công_sức để giành lấy . Cơ_hội và những vinh_quang không tự cất_cánh đến với bạn được đâu . Nó chỉ thuộc về ai biết nắm_bắt và nỗ_lực hết_mình . Bạn cần làm_việc chăm_chỉ hơn để sau_này không phải hối_hận , tiếc_nuối .
Trên 100đ : Bạn quả_là người giàu tham_vọng . Con_người bạn đầy_ắp những hoài_bão và ý_chí quyết_tâm . Nhưng bạn hãy thận_trọng , bởi tham_vọng có_thể làm_bạn mất_trí và trở_nên mù_quáng mà không cần biết đến tình_thương yêu . Nhớ đối_xử tốt với mọi người , bạn nhé !
Theo Sinh_Viên VN

﻿ Châu_Âu tiếp_tục hứng_chịu giá_lạnh
Hai quốc_gia chịu thiệt_hại nặng nhất do lạnh_giá những ngày qua là Ukraine và Georgia - hai nước đang chịu sức_ép trước nhu_cầu nhiên_liệu sưởi ấm .
Bộ Y_tế Ukraine cho_biết đã có 181 người chết vì lạnh_giá trong 5 ngày qua , nhiều người trong họ là những người vô_gia_cư , và hơn 3.000 người phải nhập_viện . Trong khi đó hãng tin BBC cho_biết gần 200 người chết vì lạnh tại Ukraine và hàng trăm người khác chết do thời_tiết .
Nhiều nhà_máy của Ukraine có_thể phải đóng_cửa cho_đến khi cuộc khủng_hoảng lạnh_giá này đi qua . Tại_Alchevsk , hơn 67.000 người đang chịu cảnh không có lò_sưởi trong 6 ngày qua .
Tình_hình càng tồi_tệ hơn tại Georgia , khi nguồn dự_trữ khí_đốt đang giảm dần và nhiều người đã sống mà không có điện . Làm trầm_trọng thêm vấn_đề này là sự_cố đường_ống dẫn khí từ Nga sang Georgia bị phá_hoại và bị vỡ , làm gián_đoạn việc cung_cấp khí_đốt cho Georgia .
Tại khu_tự_trị Nội_Mông , một cơn bão_tuyết tràn vào thành_phố Ordos làm ảnh_hưởng đến 188.300 người_dân . Trận bão_tuyết này cũng đã làm chết 40.000 thú nuôi và làm hại khoảng 2,8 triệu ha đồng_cỏ . 3,32 triệu thú nuôi khác , nhất_là trâu_bò và cừu , hiện không có thức_ăn và đang có nguy_cơ chết đói khi nhiệt_độ xuống âm độ C .
Thời_tiết lạnh_giá và tuyết đang gây cản_trở việc đi_lại tại Hy_Lạp và Thổ_Nhĩ_Kỳ
Các quan_chức địa_phương hôm_qua cho_biết tuyết vẫn tiếp_tục rơi dày tại khu_vực này khi năm mới sắp đến . Hiện chưa có báo_cáo về thương_vong ở người nhưng nhiều dân_làng đang thiếu nước uống và thức_ăn .
Chính_quyền khu_vực đang nỗ_lực cứu_trợ cho khu_vực bị ảnh_hưởng , tuy_nhiên các đội cứu_hộ hiện chưa đến được một_số khu_vực do đường_sá nông_thôn cản_trở còn trực_thăng cứu_hộ lại không_thể đến được các khu_vực này .
T.VY ( Theo UPI , Xinhua , BBC )

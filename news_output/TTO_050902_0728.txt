﻿ Một thanh_niên VN được tặng “ Giải_thưởng TN_ASEAN” 2005
Với chủ_đề “ Tăng_cường năng_lực nghề_nghiệp cho thanh_niên ” , giải_thưởng năm nay được trao cho tám thanh_niên của các nước : Việt_Nam , Trung_Quốc , Indonesia , Philippines , Singapore , Campuchia , Brunei , Myanmar .
Nguyễn_Minh_Mẫn được trao giải_thưởng do thành_tích hai năm liền Trung_tâm DVVL dạy nghề thanh_niên tỉnh là một trong các đơn_vị dẫn_đầu khối trung_tâm dạy nghề và giải_quyết việc_làm của T . Ư_Đoàn . Trong 10 năm hoạt_động trung_tâm đã giới_thiệu việc_làm cho trên 37.500 bạn trẻ ( trong đó trên 12.000 bạn có việc_làm ổn_định ) và đào_tạo nghề cho gần 15.000 bạn trẻ .
Q.LINH

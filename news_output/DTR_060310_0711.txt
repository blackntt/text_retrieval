﻿ HLV Tam_Lang , Hoàng_Bửu bị triệu_tập
HLV Phạm_Huỳnh_Tam_Lang , cựu trung_vệ Võ_Hoàng_Bửu và 10 cầu_thủ CSG khác sẽ lần_lượt nhận được giấy triệu_tập của cơ_quan điều_tra trong những ngày tới . Quyết_định trên của C14 nhằm tiếp_tục khai_thác và củng_cố thông_tin Trương_Tấn_Hải thừa_nhận đã giúp SLNA vô_địch mùa bóng 2000-2001 .
Cuối giờ chiều_qua , theo nguồn tin riêng , cơ_quan điều_tra đã quyết_định sẽ triệu_tập HLV Phạm_Huỳnh_Tam_Lang - HLV trưởng của CSG mùa bóng 2000-2001 .
Cựu thủ_quân của ĐTQG Việt_Nam , Võ_Hoàng_Bửu , cùng 10 cựu cầu_thủ CSG khác cũng sẽ phải ra Hà_Nội để làm rõ những thông_tin mà Trương_Tấn_Hải , cựu tiền_đạo đội bóng nói trên mới bị bắt giam 4 tháng , khai đã nhận tiền_của HLV Hữu_Thắng để đánh_bại Nam_Định ở trận cuối_cùng , giúp SLNA giành chức vô_địch .
Tuy_nhiên , ông Tam_Lang khẳng_định : " Tôi hoàn_toàn không biết gì về vụ mua bán_độ gì cả . Cũng_như bao trận đấu khác , trước trận gặp Nam_Định , chúng_tôi muốn giành chiến_thắng để làm hài_lòng CĐV cũng_như ban lãnh_đạo CLB . Nếu cơ_quan điều_tra triệu_tập thì tôi cũng chỉ biết hợp_tác với cơ_quan điều_tra thôi " .
Trước đó , 4 cầu_thủ từng khoác_áo CSG ở mùa bóng chuyên_nghiệp đầu_tiên của bóng_đá Việt_Nam ( 2000-2001 ) là Lương_Trung_Tuấn , Hồ_Văn_Lợi , Nguyễn_Minh_Phương và Huỳnh_Hồng_Sơn đã phải lên trình_diện cơ_quan điều_tra .
Trong buổi làm_việc với C14 , cả 4 tên_tuổi trên đều phủ_nhận sự dính_líu của mình và khẳng_định chiến_thắng 5 - 0 của CSG là do " tinh_thần thi_đấu tốt của toàn đội , trong khi Nam_Định bị hụt hơi ở trận cuối_cùng " .
Nhưng , 2 ngày trong trại tạm giam T16 , Trương_Tấn_Hải thừa_nhận " đã chia tiền đầy_đủ cho các cầu_thủ làm trận đấu trên " và ban huấn_luyện đội bóng cũng biết " trận độ " đó .
Trong chiến_thắng 5 - 0 của CSG trước Nam_Định ở vòng cuối_cùng , Huỳnh_Hồng_Sơn là tác_giả của 3 bàn thắng , hai bàn còn lại do Hồ_Văn_Lợi ghi . Cảng_Sài gòn mùa bóng đó đứng thứ 4 và đoạt Giải phong_cách lần thứ 2 liên_tiếp .
Theo Minh_Hải_Vnexpress

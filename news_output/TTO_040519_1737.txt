﻿ Gửi ông Đỗ_Trung_Tá
Thật_là đáng ngạc_nhiên với thái_độ trả_lời quá vô_tư của ông Tá về những sai_phạm của VNPT , những sai_phạm xảy_ra trong thời_gian ong là người lãnh_đạo cao nhất của đơn vì này .
Nếu cứ như_thế thì trong tương_lai sẽ còn xảy_ra biết_bao_nhiêu là sai_phạm nữa , nhất_là khi mà đất_nước của chúng_ta cũng còn nhiều khó_khăn thì sự lãng_phí tiền_bạc này càng đáng bị lên_án , và như_vậy số tiền thất_thoát sẽ lọt vào túi của ai ?
Ngoài_ra , việc ông Tá cũng_như VNPT nói là cước bưu_chính - viễn_thông của Việt_Nam hiện_nay bằng hay thấp hơn các nước trong khu_vực , điều đó là hoàn_toàn không chính_xác .
Một ví_dụ là từ Malaysia gọi điện_thoại quốc_tế về Việt_Nam mất khoảng từ 1,5 RM đến 2RM ( Từ 6.000 - 8000 VNĐ ) , gọi từ Malaysia đi Mỹ hay các nước Châu_Âu như Anh , Pháp , Đức chỉ từ 0.5 cent đến 1.5RM ( 2.000 đến 6.000VN Đ ) .
Chúng_ta có_thể so_sánh giá cước của một nước lánh giềng cạnh chúng_ta như Malaysia với giá cước hiện_nay của Việt_Nam . Thật_là điều phi_lí khi thực_tế gọi về Việt_Nam ( một nước rất gần Malaysia ) lại mắc hơn gọi đi các nước thuộc châu Âu hay Châu_Mỹ .
Thêm một ví_dụ nữa , nếu_như gửi thư thường ( 20gr ) từ Malaysia về Việt_Nam chỉ 1RM ( 4.000VN Đ ) . Trong khi đó gửi thư thường quốc_tế ở Việt_Nam chúng_ta cao hơn nhiều . Như_vậy chúng_ta có_thể hiểu được sự_thật được ông Tá phát_biểu là như_thế_nào .
Đã đến lúc các vị Bộ Trưởng của chúng_ta cần phải nghĩ đến chuyện từ_nhiệm khi mắc sai_phạm như ông Lê_Huy_Ngọ trong thời_gian vừa_qua . Đừng để đến lúc các đoàn thanh_tra vào_cuộc thì lúc đó các vị mới chịu nhận sai_phạm về mình và khắc_phục hậu_quả thì lúc đó đã quá trễ .
NGUYỄN_VIỆT_DŨNG ( Malaysia )

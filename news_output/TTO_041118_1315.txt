﻿ Nhiều ngôi_sao Hàn_Quốc cùng đến VN
Nếu_không có gì thay_đổi các ngôi_sao Song_Hye_Kyo ( trong phim Trái_tim mùa thu , Một cho tất_cả ) , Cha_Tea_Hyun ( phim Cô nàng ngổ_ngáo ) , Jang_Na_Ra ( phim Ôi ngày hạnh_phúc ) , Ha_Ji_Won ( ngôi_sao đang lên của làng điện_ảnh Hàn ) ... sẽ cùng đến thăm Hà_Nội vào trung_tuần tháng 12 tới .
Đây sẽ là lần sang VN đông_đảo nhất của nghệ_sĩ Hàn_Quốc từ trước đến nay .
Hai năm trước Kim_Min_Jong ( diễn_viên trong phim Câu_chuyện trên đảo đang phát_sóng trên HTV9 ) , Kim_Nam_Joo ( phim Người_mẫu ) , nhóm ca nữ Baby VOX ... đã đến TP.HCM giao_lưu biểu_diễn tại sân Lan_Anh trong chương_trình kỷ_niệm 10 năm thiết_lập quan_hệ ngoại_giao Việt-Hàn vào gây được thiện_cảm tốt_đẹp với khán_giả .
TR . N

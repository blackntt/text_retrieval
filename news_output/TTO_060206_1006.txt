﻿ Anh Đào_Traxel viết tự_truyện
Qua hồi_ức về những bữa cơm gia_đình , cách tổng_thống Pháp dạy con , Anh Đào nói chị muốn mọi người biết về " khía_cạnh con_người " của ông Chirac .
Anh Đào_Traxel sinh năm 1957 , đến Pháp năm 1979 , trở_thành công_dân Pháp năm 1980 . Chị kết_hôn với một sĩ_quan cảnh_sát Pháp và làm_việc trong lĩnh_vực trợ_giúp nhân_đạo cho những gia_đình Pháp có người_thân là quân_nhân tử_trận .
Theo Le_Figaro , Anh Đào còn tham_gia trợ_giúp học_bổng cho học_sinh VN và hỗ_trợ những dự_án kinh_tế nhỏ cho VN thông_qua một_số tổ_chức phi_chính_phủ của Pháp , và từ năm 2002 chị đã bắt_đầu trở_về thăm VN . Chuyện được gia_đình Chirac nhận làm con_nuôi đã được Anh Đào giấu kín cho_đến năm_ngoái .
TH.TU . ( Theo Le_Figaro )

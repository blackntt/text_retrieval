﻿ Dược_thảo trị táo_bón kéo_dài
Táo_bón là một triệu_chứng do nhiều nguyên_nhân , có chứng táo_bón nhất_thời do một_số bệnh cấp_tính ( như các bệnh : nhiễm_khuẩn , truyền_nhiễm ) , do thay_đổi chế_độ sinh_hoạt , do ăn_uống ( như ăn thiếu rau ) gây ra .
Táo_bón kéo_dài thường do cơ_địa âm hư , huyết nhiệt , hoặc thiếu máu làm tân dịch giảm , hoặc do ở người cao_tuổi và phụ_nữ sau khi sinh nhiều lần , trương_lực cơ bị giảm , dẫn đến khí trệ làm_khó bài_tiết phân ra ngoài ; Hoặc do người dương hư_không vận_hành được khí , dẫn đến tân dịch không lưu_thông , hoặc do bị bệnh lỵ mạn_tính làm tỳ_vị kém vận hóa mà gây táo_bón .
Dược_thảo trong thành_phần các bài_thuốc trị táo_bón kéo_dài
Đại_hoàng : Kích_thích co_bóp ruột , gây ra tác_dụng nhuận_tràng và tẩy do chứa hoạt_chất anthragrinon .
Liều vừa_phải chữa kém ăn , ăn_không tiêu ; ngày uống 0,5 - 1g thuốc bột , thuốc viên hoặc đến 2g thuốc_sắc .
Liều cao là thuốc_tẩy nhẹ dùng cho người đầy bụng , táo_bón ; ngày dùng 3-10g , sắc uống .
Không dùng đại_hoàng một_cách thường_xuyên cho người hay bị táo_bón , vì thường sau khi gây tác_dụng nhuận_tràng , đại_tràng hay gây táo_bón mạnh hơn trước do trong đại_hoàng có chứa tanin gây táo_bón .
Chỉ_thực : Vỏ quả có tác_dụng làm tăng độ acid dịch_vị . Dùng chữa ăn_uống không tiêu , đầy hơi , tích trệ . Ngày dùng 6-12g sắc uống .
Chút_chít : Có tác_dụng làm tăng trương_lực và tăng nhu_động ruột , được dùng làm_thuốc nhuận_tràng hoặc thuốc_tẩy . Liều dùng để nhuận_tràng : 1-3g , để tẩy : 4-6g , dưới dạng thuốc_sắc hay thuốc bột .
Đương_quy : Có tác_dụng chống co thắt cơ_trơn ruột , giúp điều_trị táo_bón . Ngày dùng 10-20g , dạng thuốc_sắc .
Hà_thủ_ô_đỏ : Có tác_dụng giúp sinh huyết_dịch , cải_thiện chuyển_hóa chung , kích_thích nhu_động ruột , kích_thích tiêu_hóa , cải_thiện dinh_dưỡng . Dùng chữa táo_bón cho phụ_nữ sau khi sinh hoặc người cao_tuổi . Ngày dùng 10-20g dạng thuốc_sắc .
Hậu phác : Dùng chữa bụng đau đầy trướng , ăn_uống không tiêu , táo_bón . Ngày dùng 6-12g dạng thuốc_sắc .
Cam_thảo , sa_sâm nam : Cam_thảo chích ( tẩm mật sao ) có tác_dụng nhuận_tràng nhẹ , ngày dùng 4-10g . Sa_sâm nam có tác_dụng nhuận_tràng , lợi_tiểu . Ngày dùng 20-40g rễ tươi , hoặc 15-20g rễ khô sắc uống .
Huyền_sâm , mạch_môn : Là các vị thuốc có tác_dụng trị táo_bón . Liều dùng mỗi ngày của huyền_sâm là 4-12g , của mạch_môn là 6-20g , dạng thuốc_sắc .
Muồng trâu : Chứa các chất anthraquinon có tác_dụng nhuận_tràng , lợi_tiểu . Muồng trâu ( lá , cành , rễ ) được dùng làm_thuốc chữa táo_bón . Ngày dùng 4-12g để nhuận_tràng , 20-40g để tẩy .
Trắc_bá ( hạt ) : Có tác_dụng nhuận_tràng , được dùng trị táo_bón , ngày dùng 4-12g hạt trắc_bá ( bá tử nhân ) .
Vừng : Hạt vừng có tác_dụng nhuận_tràng , dưỡng huyết , bổ ngũ_tạng . Hạt vừng và dầu hạt vừng được dùng chữa táo_bón , tăng_cường dinh_dưỡng . Để nhuận_tràng , mỗi sáng uống một thìa_cà_phê dầu vừng , hoặc ăn một nắm vừng sống , hoặc cháo vừng .
Các bài_thuốc dân_gian
Táo_bón do cơ_địa hoặc sau khi mắc bệnh cấp_tính gây giảm tân dịch
Triệu_chứng : Táo_bón lâu ngày , thường_xuyên họng khô , miệng khô , thường lở_loét miệng , lưỡi đỏ , người háo khát nước .
Bài 1 : Sa_sâm , mạch_môn , mỗi vị 200g ; lá dâu , vừng đen , mỗi vị 100g , mật_ong vừa đủ . Tán bột làm viên , uống mỗi ngày 10-20g .
Bài 2 : Vừng đen 20g , sinh_địa , huyền_sâm , mạch_môn , sa_sâm , mỗi vị 16g , thạch hộc 12g , mật_ong vừa đủ . Tán bột làm viên , ngày uống 10-20g . Có_thể dùng thuốc_sắc liều thích_hợp .
Bài 3 : Ba tử nhân ( hạt trắc_bá ) 100g , bạch thược 50g ; đại_hoàng , hậu phác , chỉ_thực , mỗi vị 40g , tán bột , mỗi ngày uống 10-15g .
Bài 4 : Sinh_địa , sa_sâm , mạch_môn , ngọc trúc , mỗi vị 12g , đường_phèn 20g , sắc uống .
Bài 5 : Hạt vừng đen , lá cối_xay , mỗi vị 300g . Vừng đen rang chín , giã nhỏ rây bột . Lá cối_xay nấu nước rồi cô thành cao đặc . Trộn hai thứ làm thành bánh 10g , ngày uống 2 bánh hãm với nước sôi sau mỗi bữa ăn .
Táo_bón do thiếu máu
Gặp ở người thiếu máu , phụ_nữ sau khi sinh mất máu .
Triệu_chứng : Gồm triệu_chứng của hội_chứng thiếu máu kèm thêm chứng táo_bón kéo_dài .
Bài 1 : Vừng đen 200g , hà_thủ_ô_đỏ , kỷ tử , long_nhãn , tang thầm ( quả dâu ) , bá tử nhân ( hạt trắc_bá ) mỗi vị 100g , mật_ong vừa đủ . Tán bột làm viên . Mỗi ngày uống 10-20g , có_thể dùng dạng thuốc_sắc liều thích_hợp .
Bài 2 ( tử vật thang gia_vị ) : Thục_địa , bạch thược , mỗi vị 12g ; xuyên_khung , đương_quy , bá tử nhân , vừng đen , đại_táo , mỗi vị 8g . Sắc uống ngày_một thang .
Táo_bón do khí_hư
Thường gặp ở người_già , phụ_nữ sau khi sinh nhiều lần trương_lực cơ giảm .
Triệu_chứng : Cơ nhão , táo_bón , hay đầy bụng , chậm tiêu , ăn kém , ợ hơi .
Bài 1 : Đảng_sâm 16g , bạch_truật , hoài_sơn , sài_hồ , kỷ tử , vừng đen lượng vừa đủ . Sắc uống ngày_một thang .
Bài 2 ( bổ trung ích khí thang gia_vị ) : Hoàng kỳ , bạch_truật , đảng_sâm , sài_hồ , thăng ma , mỗi vị 12g ; đương_quy , nhục thung_dung , bá tử nhân , vừng đen , mỗivị 8g ; trần_bì , cam_thảo , mỗi vị 6g . Sắc uống ngày_một thang .
Bài 3 ( dùng cho người cao_tuổi , dương khí_kém , có các triệu_chứng táo_bón , sợ lạnh , tay_chân lạnh , ăn kém , lưng gối mỏi đau ) : Chút_chít , ý_dĩ , mỗi vị 12g ; bố_chính sâm , kỷ tử , hoài_sơn , hoàng_tinh , mỗi vị 10g , nhục quế 2g . Sắc uống ngày_một thang hoặc tán bột làm viên , mỗi ngày uống 10g
Táo_bón do bệnh_nghề_nghiệp ( khí trệ )
Đối_với những người bị táo_bón do làm các công_việc mà phần_lớn thời_gian ngồi lâu không thay_đổi tư_thế , hoặc do viêm đại_tràng mạn_tính thì thường dùng các thuốc kiện tỳ ( đảng_sâm , bạch_truật , ý_dĩ ) các thuốc hành khí ( chỉ_xác , chỉ_thực , hậu phác ) , phối_hợp với các thuốc nhuận_tràng ( vừng đen , chút_chít , lá muồng trâu ) .
Bài 1 : Muồng trâu , chút_chít , mỗi vị 20g ; đại_hoàng 4-6g . Sắc uống trong ngày .
Bài 2 : Rễ tươi chút_chít 8-12g , nhai sống , hay sắc nước uống .
Bài 3 : Chút_chít 10g ; chỉ_xác , mộc thông , mỗi vị 8g . Sắc nước uống , nếu sau một giờ chưa đi_tiêu được thì sắc nước thứ_hai uống tiếp . Theo Sức khỏe & amp ; Đời_sống

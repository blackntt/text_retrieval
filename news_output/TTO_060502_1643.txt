﻿ Luyện thi môn Văn , chuyên_đề 4 : Văn_xuôi kháng Pháp
& gt ; & gt ; Mời bạn bấm vào các đường link dưới đây để xem nội_dung bài giảng hôm_nay về " Văn_xuôi kháng Pháp " .
Nội_dung bài giảng được trình_bày theo trình_tự sau :
1 . Vấn_đề 1 : Đôi mắt - Nam_Cao ( bấm vào đây để xem )
- Các câu_hỏi liên_quan đến tác_phẩm
- Các đề làm văn liên_quan và các bài_làm gợi_ý
+ Đề 1 : “ … Đôi mắt của Nam_Cao được coi như_là bản Tuyên_ngôn Nghệ_Thuật của thế_hệ chúng_tôi , hồi ấy … ” ( Tô_Hoài ) . Phân_tích tác_phẩm để làm sáng_tỏ nhận_định trên . + Đề 2 : Phân_tích văn_sĩ Hoàng trong “ Đôi mắt ” của Nam_Cao để làm sáng_tỏ chủ_đề tác_phẩm .
- Tư_liệu và lời bình
2 . Vấn_đề 2 : Vợ_chồng A_Phủ - Tô_Hoài ( bấm vào đây để xem )
- Câu_hỏi về tác_phẩm
- Các đề làm văn và gợi_ý làm :
+ Đề 1 : Phân_tích Mị ( trọng_tâm đoạn trích SGK V.12 ) để thấy được “ Tô hoài đã xây_dựng nhân_vật theo quá_trình phát_triển cách_mạng ” + Đề 2 : Phân_tích giá_trị nhân_đạo của Vợ_chồng A_Phủ ( Tô_Hoài ) qua cuộc_đời của Mị và A_Phủ
- Các đề_bài luyện_tập
- Lời bình và tư_liệu
3 . Vấn_đề 3 : Vợ nhặt - Kim_Lân ( bấm vào đây để xem )
- Câu_hỏi về tác_phẩm
- Các đề làm văn và gợi_ý làm :
+ Đề 1 : Phân_tích giá_trị nhân_đạo sâu_sắc trong “ Vợ nhặt ” của Kim_Lân . + Đề 2 : Phân_tích tâm_trạng của bà_cụ Tứ trong “ Vợ nhặt ” của Kim_Lân .
- Các đề_bài luyện_tập
Bài giảng do Trung_tâm luyện thi chất_lượng cao Vĩnh_Viễn cung_cấp , được chia thành 3 file khác_nhau , trình_bày dưới dạng file PDF . Nếu máy bạn chưa cài_đặt phần_mềm Adobe_Acrobat_Reader để đọc các file PDF này , mời bấm vào đây để tải chương_trình về cài_đặt vào máy .
& gt ; & gt ; Mời các bạn bấm vào những đường link ở trên để theo_dõi nội_dung hướng_dẫn về các vấn_đề trong bài_học hôm_nay .
TTO ( Bài giảng do Trung_tâm luyện thi Vĩnh_Viễn cung_cấp )

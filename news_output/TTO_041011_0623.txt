﻿ TS Lưu_Trọng_Hiếu được tặng bằng tiến_sĩ danh_dự của ĐH Nông_nghiệp Thụy_Điển
Tiến_sĩ Lưu_Trọng_Hiếu được trao_tặng bằng tiến_sĩ danh_dự do những đóng_góp của ông trong việc phát_triển sự hợp_tác khoa_học giữa VN và Thụy_Điển , có nhiều thành_tích trong nghiên_cứu và đào_tạo nguồn nhân_lực cho VN và các nước_đang_phát_triển .
Trong 20 năm qua , tiến_sĩ Lưu_Trọng_Hiếu là điều_phối_viên quốc_gia , điều_phối_viên khu_vực nhiều dự_án hợp_tác nghiên_cứu khoa_học do chính_phủ Canada , Thụy_Điển , Úc và Tổ_chức Lương nông tài_trợ . Thông_qua các dự_án do các tổ_chức Thụy_Điển tài_trợ , Đại_học Nông_nghiệp Thụy_Điển đã giúp VN đào_tạo hơn 40 thạc_sĩ nông_nghiệp , hơn 20 tiến_sĩ nông_nghiệp .
Cũng thông_qua các dự_án hợp_tác trên , nhiều nghiên_cứu_sinh VN đã bảo_vệ thành_công luận_án tiến_sĩ trong nước cũng_như tại các nước Tây_Âu .
T . Đ . L .

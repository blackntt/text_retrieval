﻿ Thu_nhập 1 triệu đồng cũng sẽ phải chịu thuế
Theo Dự thảo Luật thuế_thu_nhập cá_nhân đang được Bộ Tài chính xây_dựng , người có thu_nhập 1 triệu đồng mỗi tháng cũng sẽ áp_dụng mức thuế_suất 5% . Tuy_nhiên , mức chiết giảm có_thể lên tới 12-20 triệu đồng / năm .
Tổng_cục Thuế cho_biết , nếu theo bản dự_thảo thì đối_tượng nộp thuế sẽ được mở_rộng ra tới toàn dân . Khi ấy , mức thuế khởi_điểm sẽ không phải là 5 triệu hay 3 triệu đồng / tháng mà người có thu_nhập khoảng 1 triệu đồng mỗi tháng cũng phải có nghĩa_vụ đóng thuế .
Tuy_nhiên , mức chiết giảm cho người nộp thuế sẽ được quy_định cụ_thể hơn , bao_gồm gia_cảnh và các chi_phí phát_sinh theo đặc_điểm từng ngành_nghề . Dự_kiến , chiết giảm cho cá_nhân người nộp thuế sẽ là 12-20 triệu đồng / năm .
Trong trường_hợp người nộp thuế có vợ , chồng hoặc con thì được chiết giảm thêm 3,6 triệu đồng / năm . Đối_với trường_hợp có người phụ_thuộc ( theo quy_định của pháp_luật ) mà người nộp thuế có nghĩa_vụ phải nuôi_dưỡng thì mức chiết giảm thêm 6 triệu đồng / người cho một năm .
Trường_hợp trẻ_em dưới 18 tuổi được trừ thêm 3,6 triệu đồng / người . Đối_với người nước_ngoài cư_trú tại VN , các mức chiết giảm gia_cảnh được tính bằng 150% so với các mức chiết giảm gia_cảnh tương_ứng đối_với người VN và các đối_tượng khác cư_trú tại VN .
Cũng theo bản dự_thảo , đối_tượng chịu thuế sẽ bao_gồm công_dân VN , người nước_ngoài , cá_nhân cư_trú hoặc không cư_trú tại VN , chủ_hộ , người đại_diện hộ gia_đình sản_xuất , kinh_doanh trong các lĩnh nông_nghiệp , lâm_nghiệp , ngư_nghiệp , diêm_nghiệp , nông_nghiệp , xây_dựng ...
Ngoài tiền_lương , tiền_công , các khoản thu_nhập khác như thu_nhập từ lãi các khoản cho vay , lợi cổ_tức , thu_nhập chuyển vốn , bất_động_sản , bản_quyền , trúng thưởng , cá_cược , thừa_kế ... cũng sẽ phải đóng thuế .
Biểu_thuế lũy_tiến từng phần
Bậc thuế
Tổng thu_nhập chịu thuế trong năm
Thuế_suất ( % )
1
Từ 0 đến 60 triệu đồng
5%
2
Trên 60 triệu đồng đến 180 triệu đồng
10%
3
Trên 180 triệu đồng đến 540 triệu đồng
20%
4
Trên 540 triệu đồng đến 1,62 tỷ đồng
30%
5
Trên 1,62 tỷ đồng
35% Theo Minh_Khuyên_Vn Express

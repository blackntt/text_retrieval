﻿ 6.300 tỷ đồng nâng_cấp tuyến đường_sắt Hà_Nội - Hải_Phòng
( Dân_trí ) – Tổng_Công ty Đường_sắt Việt_Nam đang hoàn_thiện thủ_tục đầu_tư nâng_cấp , điện_khí_hóa tuyến đường_sắt Hà_Nội - Hải_Phòng . Theo đó , ngành sẽ đầu_tư khoảng 6.300 tỷ đồng_vốn ODA Nhật_Bản để nâng_cấp và điện_khí_hóa toàn tuyến .
Giai_đoạn 1 sẽ đầu_tư xây_dựng đường_đôi các đoạn Yên_Viên - Gia_Lâm , Cao_Xá - Tiền_Trung , điện_khí_hóa đoạn Gia_Lâm - Lạc_Đạo .
Giai_đoạn 2 , tiếp_tục xây_dựng đường_đôi đoạn Lạc_Đạo - Cao_Xá , Tiền_Trung - Hải_Phòng , đồng_thời điện_khí_hóa đoạn Lạc_Đạo - Hải_Phòng , Gia_Lâm - Bắc_Yên_Viên .
Đây là dự_án thí_điểm cho việc phát_triển sức_kéo điện của ngành Đường_sắt , làm cơ_sở xây_dựng các tuyến khác và nằm trong quy_hoạch phát_triển đường_sắt Việt_Nam đến năm 2020 . PT

﻿ Wenger : hãy tin tôi
Đó là lời trấn_tĩnh của huấn_luyện_viên ( HLV ) Arsene_Wenger sau khi các cổ_động_viên Arsenal đã có phản_ứng khi CLB này để thủ_quân Viera ra_đi .
Hiện_nay phó_chủ_tịch Arsenal_David_Dein và HLV Arsene_Wenger đang bị các cổ_động_viên phản_đối vì họ cho rằng 2 ông đã để mất đi con át_chủ_bài trong cuộc chạy_đua cùng Arsenal đến chức vô_địch Premier_League nói_riêng và Champions_League nói_chung .
Trước_đây ông Wenger cũng đã bán đi những cầu_thủ con cưng tại sân Highbury như Marc_Overmas , Emmanuel_Petit và Paul_Merson . Nhưng lần này các cổ_động_viên Arsenal khó có_thể tiêu_hóa được_việc Viera chuyển sang Juventus . Ngoài việc đã mất Viera , Arsenal còn có nguy_cơ mất một pháo_thủ khác là Pires .
Hiện_nay ông Wenger đang dùng số tiền có được từ việc bán Viera để tuyển các pháo_thủ mới . Ngoài việc đã ký hợp_đồng với tiền_vệ người Belarus_Aleksander_Hleb ( với giá 7 triệu bảng ) , mục_tiêu của ông Wenger là tiền_vệ quốc_tế người Anh Jermaine_Jenas hoặc tiền_vệ Mahamadou_Diarra của Lyon ( để thế chỗ Viera ) .
Tuy_nhiên HLV Graeme_Souness của Newcastle chỉ đồng_ý bán Jenas cho Arsenal với điều_kiện trung_vệ Sol_Campell sẽ chuyển ngược_lại , từ Arsenal sang Newcastle .
Việc củng_cố đội_hình của ông Wenger đang gặp nhiều khó_khăn vì mới_đây Seville cũng đã từ_chối lời đề_nghị trị_giá 20 triệu euro của Arsenal nhằm có được chữ_ký của tiền_đạo 23 tuổi người Brazil_Baptista .
Nguyện_vọng của Baptista là vẫn sẽ được thi_đấu tại Tây_Ban_Nha ( nhưng không phải trong màu áo Seville ) , do_đó hiện Barcelona và Real đang có ưu_thế nhất trong việc chạy_đua giành chữ_ký của tiền_đạo này . Tuy_nhiên hiện_giờ cả 2 ông lớn vẫn chưa có động_tĩnh gì và dường_như họ cũng không muốn có Baptista vì hàng tiền_đạo của họ đang dư_thừa .
Ông Del_Nido , chủ_tịch Seville cho_biết họ đã ký một hợp_đồng kéo_dài 4 năm với tiền_vệ Enzo_Maresca của Juventus với mức phí 3 triệu euro .
Trong khi đó thủ_môn đoạt giải_thưởng " Zamora " ( thủ_môn xuất_sắc nhất Premier_Liga ) mùa bóng qua Victor_Valdes đã đồng_ý gia_hạn hợp_đồng đến năm 2010 với tân vô_địch Barcelona .
Ngoài_Valdes thì 2 ngôi_sao khác trong chùm sao đã tạo nên một mùa bóng 2004-2005 tuyệt_vời cho Barcelona là Samuel_Eto ' o và Deco cũng đã ký một hợp_đồng mới kéo_dài đến năm 2010 với CLB xứ Catalan .
KINH_LUÂN

﻿ Từ 3-6 : chấn_chỉnh xe_đò TP.HCM - Hà_Nội
Theo đó , ngoài hai chuyến xe_đò hoạt_động bình_thường mỗi ngày , sẽ đưa thêm chuyến xe_đò chất_lượng cao xuất_phát tại bến_xe miền Đông ( TP.HCM ) lúc 10g và tại Giáp_Bát ( Hà_Nội ) lúc 18g mỗi ngày .
Các xe_đò này sẽ chạy đúng hành_trình , lịch_trình , xuất bến và đến bến đúng giờ , đón trả khách và dừng xe cho khách ăn nghỉ đúng nơi qui_định , không chở hàng_hóa , súc_vật trong xe , không bắt khách ăn cơm giá cao mà xe phải đưa khách vào các nhà_hàng quán_ăn đã được hợp_đồng .
Giá vé xe_đò thường là 248.000 đồng/người và xe_đò chất_lượng cao 350.000 đồng/người .
N . ẨN

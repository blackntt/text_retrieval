﻿ TQ : đóng_cửa trường_học vì bệnh quai_bị
Sở Y_tế thành_phố này báo_cáo đã có 432 ca nghi mắc bệnh quai_bị hôm 2-12 từ 7 trường_học trong thành_phố , với 35 ca đã có kết_luận mắc bệnh . Các chuyên_gia y_tế cho_biết một_số trẻ khác có_thể mắc bệnh cúm hoặc viêm họng .
Cuối tuần qua , Sở Y_tế tỉnh Cát_Lâm đã cử một nhóm chuyên_gia kiểm_soát và ngăn_ngừa dịch_bệnh đến các vị_trí có dịch_bệnh để khảo_sát .
Nhóm này xác_nhận trong số học_sinh trên , có cả_thảy 39 trường_hợp mắc bệnh quai_bị tại thành_phố Shuangliao từ ngày 1-11 đến 3-12 , nhưng họ bác_bỏ khả_năng có_thể bùng_phát ổ dịch lớn do bệnh này có khuynh_hướng tấn_công trẻ vào mùa_đông và mùa xuân .
T.VY ( Theo Xinhua )

﻿ Ở xứ người không có tết thật !
Thấp_thoáng những tà áo_dài hết_sức thời_trang . Cũng có bánh_chưng xanh , câu_đối đỏ , bao lì_xì , cành mai , cành đào theo đúng phong_tục cổ_truyền của dân_tộc , nhưng sao tôi thấy chúng cứ giả giả !
Người em họ của chồng tôi qua Mỹ đã 30 năm , từ khi cô còn là một đứa trẻ vị_thành_niên . Trong 30 năm qua , cô không hề biết giao_thừa là gì vì “ tất_cả đều do mẹ lo ” . Còn cô , khi còn nhỏ phải lo đi ngủ sớm để sáng hôm_sau đi học và bây_giờ là đi làm .
Tuy_nhiên , hình_ảnh về tết ở quê_nhà dù lờ_mờ vẫn đeo_đẳng cô đến tận bây_giờ . Khi có ai đó hỏi cô về tết VN ở Mỹ , cô đã quả_quyết " ở nước_ngoài không có tết đúng nghĩa " , và trong sâu_thẳm lòng mình , cô thèm “ được một lần thưởng_thức hương_vị tết ở quê_hương ” .
Giờ thì bên này là mùa_đông với tuyết trắng_xóa . Vài ngày nữa tôi sẽ đón giao_thừa một_mình lúc nửa_đêm khi chồng tôi đã đi làm và các con tôi đang an_giấc . Tôi nhớ rất nhiều không_khí linh_thiêng của đêm giao_thừa ngồi chờ năm mới_rồi đi chùa hái lộc đầu năm , kế đến về nhà xông_đất , chúc tết ông_bà , cha_mẹ và xem chương_trình truyền_hình đêm giao_thừa ...
Chia_sẻ với tôi bây_giờ là những tấm thiệp điện_tử của bạn_bè khắp_nơi gửi về với những lời chúc hết_sức tốt_đẹp . Điều đó mang lại cho tôi cảm_giác an_ủi rằng mọi người vẫn còn nhớ đến mình ở đâu_đó trên đời !
LÂM_THÙY ( Pennsylvania )

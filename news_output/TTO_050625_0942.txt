﻿ Champions_League : Liverpool gặp TNS của xứ Wales ở vòng 1
Cũng trong buổi lễ bốc_thăm này , UEFA cũng đã tiến_hành phân cặp các trận đấu ở vòng sơ loại thứ 2 , và như kết_quả đã công_bố thì đội giành chiến_thắng trong cặp đấu giữa Liverpool và TNS ( ở vòng sơ loại thứ nhất ) sẽ gặp CLB_HB Torshavn của quần_đảo Faroe hoặc CLB_FBK Kaunas của Lithuania ( 2 đội này cũng phải loại nhau ở vòng 1 ) ở vòng sơ loại thứ 2 .
Phát_biểu sau lễ bốc_thăm , Giám_đốc điều_hành Liverpool_Rick_Parry cho_biết : " Thật tuyệt_vời , chẳng còn mong gì hơn thế . Chúng_tôi không quan_tâm TNS là đối_thủ như_thế_nào , vấn_đề là chúng_tôi đã không phải di_chuyển quá xa ' ' .
Cũng theo như Giám_đốc Parry thì các cầu_thủ Liverpool đã khép lại niềm_vui chiến_thắng tại Istanbul từ lâu , và hiện_tại cả CLB đang tập_trung cao nhất cho mục_tiêu bảo_vệ vương_miện của mình . " Chúng_tôi đã trở_lại mặt_đất rất nhanh sau chiến_thắng , và vì_thế bây_giờ chúng_tôi đang rất sẵn_sàng chuẩn_bị cho mùa giải mới " , giám_đốc Parry cho_biết thêm .
Trái_ngược với hình_ảnh tươi_cười của Giám_đốc Rick_Parry , Chủ_tịch Mike_Hughes của CLB_TNS cho_biết : " Chúng_tôi không có gì phải sợ Liverpool . Mặc_dù có rất nhiều CĐV Liverpool ở chỗ chúng_tôi , trong đó có cả 2 cậu con_trai tôi nhưng không vì_thế mà chúng_tôi phải e_ngại họ . Ngược_lại , trận đấu chính là một cơ_hội tuyệt_vời cho tất_cả chúng_tôi " .
Còn_Giám đốc Mike_Harris thì hồ_hởi phát_biểu : " Thật tuyệt_vời . Có rất nhiều đội bóng mà chúng_tôi mong_muốn được giáp_mặt ở vòng_loại thứ nhất , nhưng xét về khía_cạnh tài_chính thì Liverpool là số 1 ' ' .
Ngoài những trận đấu có ĐKVĐ Liverpool tham_dự , thì tại vòng sơ loại thứ 2 , các trận đấu giữa Dynamo_Kyiv ( Ukraina ) - FC Thun ( Thụy_Sĩ ) ; Malmo FF ( Thụy_Điển ) - Maccabi_Haifa ( Isreal ) và trận đấu CLB Celtic của Scotland ( được miễn vòng 1 ) với đội giành chiến_thắng trong cặp đấu giữa Kairat ( Albania ) và Artmedia ( Slovenia ) cũng được đánh_giá là những trận đấu rất hấp_dẫn .
Sau đây là kết_quả bốc_thăm cụ_thể của vòng sơ loại thứ nhất :
1 - Liverpool FC ( Anh ) - Total_Network_Solutions FC ( Wales )
2 - FC Haka ( Phần_Lan ) - FC Pyunik ( Armenia )
3 - HB Tórshavn ( Faroe ) - FBK Kaunas ( Litva )
4 - FC Levadia_Tallinn ( Estonia ) - FC Dinamo_Tbilisi ( Gruzia )
5 - NK Gorica ( Slovenia ) - KF Tirana ( Albania )
6 - FC Kairat_Almaty ( Kazakhstan ) - FC Artmedia_Bratislava ( Slovakia )
7 - PFC Neftchi ( Azerbaijan ) - FH Hafnarfj ö rdur ( Israel )
8 - FK Rabotnicki ( Macedonia ) - Skonto FC ( Latvia )
9 - Glentoran FC ( Bắc_Ireland ) - Shelbourne FC ( CH Ireland )
10 - FC Dinamo_Minsk ( Belarus ) - Anorthosis_Famagusta FC ( Síp )
11 - Sliema_Wanderers FC ( Malta ) - FC Sheriff ( Moldova )
12 - F91 Dudelange ( Luxembourg ) - NK Zrinjski ( Bosnia-Herzegovnia )
Các cặp đấu vòng sơ loại thứ_hai :
+ Thắng trận 3 - Thắng trận 1
+ Thắng trận 4 - Br ø ndby IF ( Đan_Mạch )
+ FK Partizan ( Serbia & amp ; Montenegro ) - Thắng trận 11
+ Debreceni VSC ( Hungaria ) - HNK Hajduk_Split ( Croatia )
+ Thắng trận 6 - Celtic FC ( Scotland )
+ Thắng trận 5 - PFC_CSKA Sofia ( Bulgaria )
+ RSC Anderlecht ( Bỉ ) - Thắng trận 7
+ Valerenga IF ( Nauy ) - Thắng trận 2
+ Thắng trận 8 - FC Lokomotiv_Moskva ( Nga )
+ Thắng trận 12 - SK Rapid_Wien ( Áo )
+ FC Dynamo_Kyiv ( Ukraina ) - FC Thun ( Thụy_Sĩ )
+ Malmo FF ( Thụy_Điển ) - Maccabi_Haifa FC ( Isreal )
+ Thắng trận 9 - FC Steaua_Bucuresti ( Romania )
+ Thắng trận 10 - Trabzonspor ( Thổ_Nhĩ_Kỳ )
Các trận lượt_đi của vòng sơ loại thứ nhất Champions_League sẽ diễn ra_vào ngày 12 hoặc 13/7 , và lượt_về sẽ vào ngày 19 hoặc 20/7 . Các trận lượt của vòng sơ loại thứ_hai sẽ diễn ra_vào ngày 26-27 / 7 và lượt_về sẽ diễn ra_vào ngày 2-3 / 8 .
T . VĨ

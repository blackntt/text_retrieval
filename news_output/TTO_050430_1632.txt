﻿ Học thạc_sĩ , Tiến_sĩ về Kiến_trúc ở Đức ?
- Trả_lời của trung_tâm thông_tin giáo_dục Đức ( DAAD ) :
Với câu_hỏi thứ nhất , xin trả_lời là bạn vẫn có_thể học lại bằng MA , nhưng có một_số trường sẽ yêu_cầu bạn đóng học_phí ( hầu_hết các trường ĐH ở Đức đều miễn học_phí cho SV ) .
2 . Nếu muốn làm nghiên_cứu_sinh , có nhiều cách để lấy_được bằng tiến_sĩ ở Đức :
+ Cách truyền_thống là tự tìm cho mình một giáo_sư đỡ_đầu ( Doktorvater – Doktormutter ) . + Cách thứ_hai là tham_dự các chương_trình sẵn có trong khuôn_khổ các chương_trình International_Degree_Programme . + Cách thứ 3 là tìm_hiểu thông_tin của các viện nghiên_cứu , vì họ cũng có các chương_trình đào_tạo tiến_sĩ ( Graduiertenkollegs ) . Bạn có_thể tìm thấy thông_tin về tất_cả các khả_năng nói trên tại địa_chỉ : www.daad.de/deutschland/en/2.2.8.html
3 . Về các chương_trình , đề_án ngắn_hạn , xin trả_lời là hiện_nay , DAAD ( Cơ_quan trao_đổi Hàn_Lâm_Đức ) có chương_trình học_bổng ngắn_hạn ( dưới 6 tháng ) dạy cho nghiên_cứu_sinh . Điều_kiện là ứng_viên phải có 1 dự_án tốt và rõ_ràng . Hàng năm , khoảng tháng 6-8 , DAAD sẽ thông_báo các chương_trình học_bổng của mình trên trang_web www . daadvn.org
4 . Về ngoại_ngữ , khi làm nghiên_cứu_sinh ở bậc tiến_sĩ , bạn cần phải có được trình_độ ngoại_ngữ mà vị giáo_sư đỡ_đầu yêu_cầu ( tiếng Anh hoặc tiếng Đức , tùy theo từng giáo_sư ) .
Bạn có_thể học ngoại_ngữ 1 năm trước khi vào chương_trình chính_khóa tại Đức . Tuy_nhiên , khoảng thời_gian 1 năm hay 450 tiết có đủ khả_năng tiếng Đức để nhập_học bằng tiếng Đức hay không là hoàn_toàn phụ_thuộc vào khả_năng và sự nỗ_lực của chính bạn .
TTO

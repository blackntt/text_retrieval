﻿ Alonso nới rộng khoảng_cách với Schumacher
Alonso cùng chiếc Cup vô_địch Grand_Prix_Monaco 2006 Với chiến_thắng dễ_dàng tại GP Monaco tối_qua 28-5 , Fernando_Alonso đã nới rộng khoảng_cách với cựu vô_địch Schumacher lên thành 21 điểm .
Đúng như nhận_định của giới chuyên_môn , sau khi giành pole ngay trên tay Schumacher hôm thứ_bảy , Alonso đã dễ_dàng vượt lên và kết_thúc 78 vòng đua chính ở vị_trí số_một . Bám sát nhà ĐKVĐ suốt 2/3 chặng đường là Raikkonen và Weber .
Tuy_nhiên , hai đối_thủ cạnh_tranh sừng_sỏ cuối_cùng đã phải nói lời chia_tay trong nỗi tức_giận .
Đầu_tiên là Weber nổi_cáu với chiếc xe bốc khói , ở vòng 48 . Và khi các tay_đua còn chạy trong trạng_thái “ safety car ” ( chạy chậm chờ vật cản được đem đi ) , đến lượt “ người tuyết Phần_Lan ” lâm vào cảnh tồi_tệ chẳng kém . Phần việc còn lại quá đơn_giản đối_với Alonso . Nhà ĐKVĐ cán đích , hơn người về nhì ( Montoya ) tới 14 giây .
Về phần Schumacher , sau khi bị ban tổ_chức tước mất pole hôm thứ_bảy do hành_động kém fair - play , tay_đua 37 tuổi đã thể_hiện nỗ_lực lội ngược dòng tuyệt_vời .
Với quyết_định xuất_phát ngay trên đường pit , cùng một bình xăng đầy ụ , nhà cựu vô_địch nhanh_chóng “ đánh_bại ” hàng_loạt đối_thủ , trước khi cán đích ở vị_trí thứ 5 . Trong số các bại_tướng của Schumacher , đáng chú_ý có Fisichella , Button và cậu em ruột Ralf_Schumacher .
Có_thể làm một_phép so_sánh đơn_giản , để chứng_minh khả_năng lái tuyệt_vời của tay_đua từng 7 lần vô_địch thế_giới . Cùng điều_khiển một loại xe ( F248 ) và cùng mang một bình xăng nặng , nhưng Massa ( xuất_phát ở vị_trí 21 ) chỉ cán đích thứ 9 , trong khi Schumi ( xuất_phát ở vị_trí 22 ) lại kiếm được 4 điểm thưởng . Rõ_ràng , nếu_không mất pole về tay Alonso , chắc_chắn tay_đua của đội Ferrari sẽ khiến Monte_Carlo chao_đảo .
F1 2006 mới đi được hơn 1/3 chặng đường , nhưng Fernando_Alonso đã nắm trong tay khoảng_cách hết_sức an_toàn ( 21 điểm ) . Với nó , chàng_trai 25 tuổi có_thể bắt_đầu nghĩ tới ngôi vô_địch lần thứ_hai liên_tiếp .
Kết_quả chi_tiết Grand_Prix_Monaco :
1 . Fernando_Alonso ( Tây_Ban_Nha ) , đội Renault , thành_tích 1 giờ , 43 phút , 43,116 giây
2 . Juan_Pablo_Montoya ( Colombia ) McLaren-Mercedes +14,567 giây
3 . David_Coulthard ( Anh ) Red_Bull - Ferrari +25,598
4 . Rubens_Barrichello ( Brazil ) Honda +53,337
5 . Michael_Schumacher ( Đức ) Ferrari +53,830
6 . Giancarlo_Fisichella ( Italy ) Renault +1 phút 2,072 giây
7 . Nick_Heidfeld ( Đức ) BMW Sauber sau 1 vòng
8 . Ralf_Schumacher ( Đức ) Toyota 1 vòng
9 . Felipe_Massa ( Brazil ) Ferrari 1 vòng
10 . Vitantonio_Liuzzi ( Italy ) Toro_Rosso - Cosworth 1 vòng
11 . Jenson_Button ( Anh ) Honda 1 vòng
12 . Christijan_Albers ( Hà_Lan ) Midland-Toyota 1 vòng
13 . Scott_Speed ( Mỹ ) Toro_Rosso - Cosworth 1 vòng
14 . Jacques_Villeneuve ( Canada ) BMW Sauber 1 vòng
15 . Tiago_Monteiro ( Bồ_Đào_Nha ) Midland-Toyota 2 vòng
16 . Franck_Montagny ( Pháp ) Super_Aguri - Honda 3 vòng
17 ( R ) Jarno_Trulli ( Italy ) Toyota 5 vòng
Bỏ_cuộc :
Christian_Klien ( Áo ) RedBull-Ferrari mới hoàn_thành 56 vòng
Nico_Rosberg ( Đức ) Williams-Cosworth 51 vòng
Kimi_Raikkonen ( Phần_Lan ) McLaren-Mercedes 50 vòng
Mark_Webber ( Australia ) Williams-Cosworth 48 vòng Takuma_Sato ( Nhật_Bản ) Super_Aguri - Honda 46 vòng
Theo VNExpress

﻿ Thêm 9.000 tỷ đồng bù_lỗ xăng_dầu
Tại hội_nghị toàn ngành sáng nay ( 3/7 ) , thứ_trưởng Bộ Tài chính Trần_Văn_Tá cho_biết từ nay đến cuối năm sẽ phải bù_lỗ cho giá xăng_dầu từ 8.000 - 9.000 tỷ đồng .
Số tiền này chủ_yếu từ ngân_sách , “ trích chéo ” từ lợi giá từ xuất_khẩu dầu_thô và chủ_yếu bù cho giá dầu để đảm_bảo ổn_định đầu_vào cho các ngành sản_xuất chủ_chốt .
Con_số trên có_thể sẽ không dừng lại khi giá dầu trên thị_trường thế_giới vẫn tiếp_tục tăng cao . Cuối tuần qua , giá dầu tại thị_trường New_York ( Mỹ ) đã vượt trên 73 USD/thùng ( giá giao_dịch hiện_tại đang là 73,93 USD/thùng ) .
Man_Financial , một công_ty môi_giới năng_lượng hàng_đầu của Singapore , cũng vừa đưa ra dự_báo về biên_độ của giá dầu trong ngắn_hạn là 70 - 80 USD/thùng , trong khi giá mục_tiêu có_thể lên tới 105 USD/thùng .
Giá dầu thế_giới tăng cao , giá trong nước vẫn phải bù_lỗ vì bán theo giá_trần của Chính_phủ . Theo Thứ trưởng Trần_Văn_Tá , việc bù_lỗ là cần_thiết để bình thị_trường trong nước , đặc_biệt là trong bối_cảnh điều_hành giá_cả năm 2006 gặp nhiều khó_khăn hơn những năm trước .
“ Rõ_ràng năm nay việc điều_hành giá gặp khó_khăn , khi mà các mặt_hàng như xi_măng , điện và than sẽ được điều_chỉnh theo hướng tăng . Đây là đầu_vào quan_trọng của nhiều ngành sản_xuất nên sự tác_động sẽ rất lớn ” , Thứ_trưởng Tá nhận_định . Theo Trịnh_Minh_Đức_Vn Economy

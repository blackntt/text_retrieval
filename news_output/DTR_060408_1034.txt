﻿ Kate_Moss trở_về với CK
Có_vẻ như vương_quốc Anh bé_nhỏ không_thể giữ_chân Kate_Moss . Vài tuần vừa_qua cô nàng siêu_mẫu còm_nhom này cứ bay qua bay lại giữa London và Los_Angeles để chụp hình , và dường_như càng ngày cô càng có nhiều “ cạ cứng ” ở Mỹ hơn .
Khi_không ăn tối với Jack_Nicholson thì Kate xuống phố dạo chơi mua_sắm cùng cô bạn tuổi teen mới Lindsay_Lohan hoặc đi cafe tán_gẫu với Jack_Osbourne .
Thứ 5 vừa_qua , Kate_Moss vừa quyết_định trở_lại Mỹ sau một thời_gian ở Anh . Người_ta bắt_gặp cô siêu_mẫu đã 32 tuổi này ở Soho – một khu_phố sang_trọng ở New_York .
Khoác trên mình một chiếc váy hoa ngắn rất bắt_mắt và áo_khoác đen , vẫn là đôi kính đen đã quá quen_thuộc , Kate xuống phố dạo chơi cùng 2 người bạn . Có_vẻ như cô nàng nghiện_ngập này không còn là mục_tiêu yêu_thích của các tay paparazzi nữa , cuộc_sống của Kate ở đây có_vẻ đã dễ_chịu hơn .
Mới_đây cô người_mẫu tưởng_chừng đã hết thời này vừa ký một hợp_đồng trị_giá 500.000 bảng Anh với Calvin_Klein – nhãn_hiệu thời_trang đã tạo nên tên_tuổi của cô từ những năm 90 .
BM Theo Hello !

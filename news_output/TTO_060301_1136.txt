﻿ Suy tim
- BS Nguyễn_Đình_Sang ( Chuyên_khoa BS gia_đình , TT_Y tế quận I ) : Mệt có nhiều nguyên_nhân , có_thể do bệnh ở đường hô_hấp như bệnh ở phổi gây khó thở , do suy_nhược cơ_thể , do thiếu máu , do hạ đường_huyết , do nguyên_nhân tâm_lý , do stress … hoặc mệt là triệu_chứng sớm của suy tim .
Trong suy tim trái , khó thở là triệu_chứng thường hay gặp nhất do sự ứ_đọng của các chất dịch ở phổi . Khi mới bắt_đầu suy tim thì chỉ khó thở khi hoạt_động thể_lực , về sau khó thở ngày_càng nhiều và cả khi nghỉ_ngơi , bệnh_nhân đang ngủ phải thức_dậy vì cơn khó thở .
Nguyên_nhân gây suy tim trái thường là do tăng huyết_áp , thiếu máu , cường_giáp , bệnh ở van tim ( như hở van 2 lá , hẹp van hoặc hở van đông mạch chủ ) , tim bẩm_sinh ( như hẹp động_mạch chủ ) , bệnh mạch vành , bệnh cơ tim …
Trong suy tim phải bệnh_nhân thường bị phù chân , gan to và triệu_chứng khó thở ít hơn .
Nguyên_nhân gây suy tim phải thường là do bệnh ở phổi ( như viêm phế_quản mạn , khí phế thủng … ) , do bệnh ở van tim ( như hở van 3 lá ) , do tim bẩm_sinh ( như tứ chứng Fallot , hẹp van động_mạch phổi … ) .
Do_đó để xác_định nguyên_nhân gây mệt , bạn nên đem người_nhà của bạn đến bệnh_viện đa_khoa nơi bạn ở để các bác_sĩ khám tổng_quát và cho làm các xét_nghiệm cần_thiết như công_thức máu , đường_máu , X_quang phổi , đo điện_tim , siêu_âm tim … từ đó mới có hướng điều_trị thích_hợp .
Bạn có những thắc_mắc về sức_khỏe của mình mà không biết hỏi ai . Bạn cần được tư_vấn cho những thắc_mắc về sức_khỏe của mình , của người_thân ... Phòng_mạch Online của Tuổi_Trẻ_Online sẽ giúp bạn giải_đáp 1.001 thắc_mắc về sức_khỏe . Mọi thắc_mắc về sức_khỏe xin gửi về địa_chỉ email : tto@tuoitre.com.vn
Để chính_xác về nội_dung , vấn_đề cần hỏi , xin bạn_đọc vui_lòng gõ có dấu ( font chữ unicode ) . Xin chân_thành cảm_ơn !
T . LÊ thực_hiện

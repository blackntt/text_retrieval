﻿ Trời rét làm thay_đổi đời_sống người_dân Hà_Nội
Người_Hà_Nội với trang_phục trong những ngày trời rét Rét đột_ngột tràn về , người Hà_Nội đổ_xô đi sắm đồ ấm . Các cửa_hàng quần_áo , chăn ga gối đệm và ăn_uống ... lập_tức " bội_thu "
Quần_áo ấm bán không kịp thở
Chỉ sau một đêm , không_khí lạnh đã tràn xuống cả khu_vực Bắc_Bộ và Trung_Bộ khiến sinh_hoạt của người_dân bị xáo_trộn . Tuần trước , chẳng ai đoái_hoài các đại_lý của nhiều công_ty may dọc phố Ngô_Quyền , Bà Triệu , Chùa_Bộc ... Thế_mà , trời vừa trở rét , người mua đã tấp_nập , người bán không kịp thở .
Trung_tâm Khí_tượng Thủy_văn Trung_ương cho_biết , lưỡi cao lạnh có cường_độ mạnh đang khống_chế thời_tiết Bắc_Bộ và Trung_Bộ . Đợt rét này sẽ còn kéo_dài , nhiệt_độ các khu_vực trên sẽ xuống thấp 12-15 o C , khu_vực miền núi 10-12 o C .
Khoảng ngày 13-12 , các khu_vực nói trên sẽ chịu ảnh_hưởng của một đợt không_khí lạnh có cường_độ mạnh .
Đây là đợt rét thứ_hai trong mùa_đông này song lại là đợt rét đậm , kéo_dài .
Nhờ rét , đống hàng tồn_kho của các đại_lý được dịp thanh_toán . Áo_gió , áo_phao là những bán " chạy " nhất giá dao_động từ 200.000 - 250.000 đồng/chiếc . Túi_tiền mỏng hơn , có_thể chọn 1 chiếc áo_len dày_dặn giá 50.000 - 70.000 đồng . Một bộ_đồ thể_thao ấm_áp cũng chỉ trên_dưới 100.000 đồng .
Cũng với " chiêu " bán hàng quen_thuộc , hàng tồn , hàng " lỗi mốt " đều được gắn tên gọi hấp_dẫn " Đại hạ giá " . Đầu mùa mà đã có hàng đại hạ giá , nghe cũng lạ . Nhưng khách_hàng cũng chẳng mấy quan_tâm , chỉ cần rẻ , ấm và " vừa_mắt " .
Cơm_hộp lại đắt hàng
Không_chỉ dưới nắng hè 37 - 38 o C người_ta mới ngại ra ngoài đường . Khi nhiệt_độ xuống dưới 15 o C , nhiều người cũng không muốn dùng cơm trong giá_rét . Thế_là không cần phải rời phòng ấm , một cú điện_thoại là có ngay hộp cơm ; hoặc một cuốc đi bộ là tìm thấy ngay một quán cơm văn_phòng thơm_nức mùi thịt rán .
Cửa_hàng cơm_hộp của chị Phương nằm gọn trên phố Đê_La_Thành 2 hôm_nay bán khá chạy . Ngày bình_thường cửa_hàng chị bán chỉ được gần 100 suất . ' ' Thế_là cũng đủ sống rồi . Nhưng mấy hôm_nay trời rét lượng khách gọi cơm đông hơn . Thêm mỗi ngày vài chục hộp thế cũng là mừng rồi ' ' , chị Phương cho_hay .
' ' Khách yêu_cầu cơm nóng , canh nóng và phải mang đến đúng giờ . Cửa_hàng chúng_tôi phải bố_trí người đưa đúng giờ để còn giữ khách trong mùa_đông này ' ' , chị Phương vui_vẻ nói .
Trẻ nhập_viện gia_tăng
Trời đột_ngột chuyển lạnh gây ảnh_hưởng không nhỏ đến sức_khoẻ trẻ_em . Bệnh_viện Nhi_Trung ương lại phải đối_mặt với tình_trạng quá_tải , nhất_là khoa Hô_hấp . Mỗi ngày khoa này phải tiếp_nhận và điều_trị trên_dưới 100 bệnh_nhân .
BS Nguyễn_Văn_Lộc , Phó_Giám đốc BV Nhi_Trung ương , cho_hay : ' ' Vài ngày qua các bệnh_nhân mắc các chứng_bệnh về hô_hấp và tiêu_hoá tăng lên rất nhanh do thời_tiết chuyển hẳn sang mùa_đông . Trời lạnh sẽ ảnh_hưởng rất lớn đến hệ hô_hấp của trẻ nhỏ , nhất_là trẻ sơ_sinh .
Bệnh_viện cũng phải tiếp_nhận và điều_trị rất nhiều bệnh_nhi mắc chứng_bệnh liên_quan đến hệ tiêu_hoá . Nếu_như ở những ngày thường số bệnh_nhân nhập_viện trên 10 cháu , thì những ngày cao_điểm , con_số này lên tới 30 - 40 bệnh_nhân / ngày , đặc_biệt trong mấy ngày gần đây , số bệnh_nhân đã lên đến trên 100 người .
Trong khi đó , ở khoa Hô_hấp của BV Nhi_Trung ương chỉ có 50 giường_bệnh , tình_trạng 2 đến 3 người chung một giường_bệnh lại diễn ra ' ' . Để giảm_thiểu các chứng_bệnh mùa lạnh cho trẻ , BS Nguyễn_Văn_Lộc đề_nghị các bậc phụ_huynh hạn_chế đưa các cháu ra ngoài_trời lạnh . Nếu bắt_buộc phải đi ra ngoài đường phải cho trẻ mặc đủ ấm . Ngoài_ra , cũng cần hết_sức quan_tâm tăng_cường dinh_dưỡng cho trẻ .
Theo VNN

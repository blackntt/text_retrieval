﻿ Đắm đò ở chùa Hương , hai người chết
Do chở quá_tải , đò bị bục , chủ đò Đức_Phan và khách đi đò là anh Vũ_Quang_Thắng ( 16 tuổi , xã Nguyên_Hòa , huyện Phủ_Cừ , Hưng_Yên ) bị thiệt_mạng .
7h sáng 12-2 , đò chở khách do cò_mồi dắt_mối trốn vé đi chùa Hương do chủ đò Trịnh_Đức_Phan ( 40 tuổi , trú tại đội 12 , thôn Đục_Khê , Hương_Sơn , Mỹ_Đức ) điều_khiển chở 12 người . Vì đò cũ , chở quá_tải nên đến giữa dòng thì bị bục , khách trên đò nhốn_nháo , hoảng_loạn làm toàn_bộ người trên đò bị hất xuống sông Đáy .
10 người đã được cứu thoát , nhưng chủ đò Đức_Phan và khách đi đò là anh Vũ_Quang_Thắng ( 16 tuổi , xã Nguyên_Hòa , huyện Phủ_Cừ , Hưng_Yên ) bị thiệt_mạng . Công_an huyện Mỹ_Đức đã kịp_thời có_mặt tại hiện_trường tổ_chức cứu nạn , khám_nghiệm tử_thi , giải_quyết hậu_quả . Đây là bài_học cảnh_báo cho việc chủ_quan tùy_tiện khi tham_gia giao_thông của khách đi lễ chùa , đối_tượng cò_mồi và chủ phương_tiện chở khách .
Theo CAND

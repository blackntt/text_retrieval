﻿ Parma bổ_nhiệm Pioli làm HLV trưởng
Pioli , 40 tuổi , chưa từng dẫn_dắt bất_kỳ 1 CLB nào ở Serie A . Ở mùa giải vừa_qua , Pioli dẫ dắt CLB Modena thi_đấu tại giải Serie B và suýt chút nữa ông đã giúp được đội bóng này giành quyền lên chơi tại giải Serie A ( thua Mantova trong trận đấu play - off ) .
Trong quá_khứ , Parma từng là một đội bóng mạnh tại giải Serie A và châu Âu ( từng giành 2 cúp UEFA vào năm 1995 và 1999 ) . Tuy_nhiên , kể từ sau khi CLB mẹ Parmalat tuyên_bố phá_sản vào năm 2003 , Parma cũng đã lâm vào_cuộc khủng_hoảng tài_chính và buộc phải bán đi hầu_hết các ngôi_sao ( trong đó có Crespo , Mutu , Adriano ... ) để trả nợ .
Hiện_tại , Parma không còn là một đội bóng mạnh , và cũng không còn khả_năng tranh_chấp ngôi vô_địch với các đại_gia : Juventus , AC Milan , Inter_Milan , AS Roma ... . Vì_thế , mục_tiêu của Pioli ở mùa giải tới không phải giúp Parma cạnh_tranh với Juventus , AC Milan , Inter_Milan ... mà là giúp CLB trụ hạng thành_công .
ANH_HÀO ( Theo Reuters )

﻿ Togo : Đội đầu_tiên đến Đức
Dù 1 tháng nữa , World_Cup mới bắt_đầu nhưng đã có nhiều đội bóng lên kế_hoạch đến Đức sớm .
Trong đó , Togo - đội lần đầu dự World_Cup dự_kiến trở_thành đội đầu_tiên hành_quân sang Đức ( vào ngày 15-5 ) . Angola - một “ tân_binh ” World_Cup - khác sẽ đến Đức vào ngày 20-5 . Những đội bóng lớn như Brazil , Argentina và Anh sẽ đến với World_Cup muộn hơn do một_số cầu_thủ của họ còn phải vướng_bận nhiệm_vụ ở CLB . Đội chủ nhà Đức sẽ trở_lại “ doanh_trại ” ở Berlin vào ngày 4-6 ( tức 5 ngày_trước trận khai_mạc World_Cup ) . Cũng trong ngày này , đương_kim vô_địch Brazil sẽ đặt_chân lên nơi đóng quân của họ ở Konigstein . Ukraine là đội đến Đức muộn nhất ( ngày 9-6 ) , sau trận giao_hữu với Luxembourg .
M . Trang

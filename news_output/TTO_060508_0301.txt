﻿ Chiếc áo của cô_giáo
Tôi góp_ý : “ Con_gái lớn rồi , ai_lại ngồi hớ_hênh thế ! ” .
Em tôi lý_luận : “ Cô_giáo em cũng ngồi thế_mà ! ” . Em kể : “ Khi nào nói_chuyện với phụ_huynh nam thì cô ngồi vắt chéo hai chân trông đàng_hoàng , thanh_lịch lắm . Còn nếu tiếp phụ_huynh nữ thì cô cũng xắn ngược ống_quần lên bắp_chân , rồi hai chân xoài rộng như_vậy ” .
Cô bạn ngồi cạnh được_thể kể thêm một loạt . Nào_là “ cô_giáo dạy sinh còn mặc chiếc áo cổ rộng mênh_mông , cúi xuống ký sổ đầu_bài là thấy hết tất_tần_tật ” . Nào_là “ cô dạy hóa toàn mặc vải voan kính , trông rõ từng ngấn bụng , trông rõ cả hoa_văn nội_y ” .
Em tôi “ tố_cáo ” thêm : nữ_sinh mặc áo_dài cổ trích hở khoảng 2 - 3cm là bị giám_thị gọi xuống phòng kiểm_điểm về tội làm hỏng vẻ đẹp truyền_thống của dân_tộc . Trong khi đó , rất nhiều cô_giáo mặc áo_dài không có cổ hay cổ thuyền ... “ Em hỏi chị , thế các cô có làm mất truyền_thống dân_tộc không ? ” .
Tôi giật_mình , không giải_thích được . Chỉ thầm biện_minh cho các cô_giáo : chắc các cô cứ nghĩ rằng HS còn trẻ_con lắm !
Nhưng nếu để cho HS bình_luận về tư_cách của mình qua những chuyện nhỏ_nhặt như_thế_này thì có_lẽ các cô cũng không nên cứ mãi ... vô_tư !
TRẦN_THU

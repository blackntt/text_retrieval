﻿ Người_Việt tại Đức bị bọn " tân phát_xít " tấn_công
Nhận được tin báo của dân trong khu_vực , cảnh_sát đã nhanh_chóng tới hiện_trường . Họ phải chui qua chỗ cửa_kính vỡ để vào bên trong .
Anh Tuấn cho_biết rất may là không bị ném chai xăng vào . Nếu điều đó xảy_ra thì số hàng_hóa trị_giá hàng trăm_nghìn euro trong cửa_hàng sẽ cháy hết , cơ_nghiệp tích_cóp từ hàng chục năm nay sẽ thành mây_khói . Theo anh Tuấn , người nước_ngoài luôn bị bọn đầu trọc “ tân phát_xít ” tấn_công nên các công_ty rất ngại khi thực_hiện các hợp_đồng bảo_hiểm .
Mới_đây , Hãng thông_tấn DPA của Đức dẫn tin từ cảnh_sát tỉnh Graefenhainichen thuộc miền tây nước Đức cho_biết hai người Việt tại đây đã bị tấn_công . Nhóm " tân phát_xít " sáu tên từ 15-19 tuổi đã dùng gậy , chai bia đánh trọng_thương hai người Việt này .
NGUYỄN_BÌNH

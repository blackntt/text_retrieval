﻿ Khi trẻ “ gay_go ” với bạn
Cách giải_quyết :
- Hãy nói rõ với trẻ rằng thái_độ của nó là không_thể chấp_nhận . Nếu trẻ cãi lại rằng nó đâu có “ nói ” gì hỗn_hào đâu , bạn hãy nhắc cho trẻ nhớ rằng nhiều lúc hành_động nói rõ_ràng hơn cả chính lời_nói .
- Hãy nhận thấy rằng thái_độ gay_go của trẻ chính là một_cách để trẻ tìm_kiếm sự quan_tâm và tìm_kiếm quyền được thể_hiện và tôn_trọng .
- Hãy cố_gắng hạn_chế việc_làm thái_quá vấn_đề lên - nhiều khi nói thì dễ hơn làm rất nhiều . Nếu trẻ có ý chọc_tức bạn và bạn nổi_giận lên thì rõ_ràng bạn đã thua trẻ rồi .
- Từ_chối làm_việc hay đối_thoại với trẻ khi trẻ tỏ ra không tôn_trọng và hỗn_hào . Hãy cho trẻ biết rằng bạn sẽ không đối_thoại với trẻ cho_đến khi nào trẻ kiểm_soát được hành_vi của nó .
- Để_ý những chương_trình tivi trẻ xem và những trò_chơi điện_tử trẻ chơi . Có vài phương_tiện giải_trí cổ_vũ cho những hành_động hỗn_hào và biến điều đó thành những chi_tiết gây cười hơn là cho đó là hành_động xấu và kém lễ_độ . Nếu trẻ từng là nạn_nhân của những trò bắt_nạt và chèn_ép , trẻ sẽ hiểu rõ_ràng hơn ai hết thái_độ gây_hấn và hỗn_hào thì không tốt chút nào .
Khi trẻ cãi tay_đôi
Triệu_chứng : Trẻ sẵn_sàng cãi tay_đôi với bạn và cướp lời bạn mỗi khi bạn yêu_cầu trẻ làm điều gì
Cách giải_quyết :
- Hãy hiểu rằng việc cãi tay_đôi chính là cách để trẻ tìm_kiếm sự quan_tâm và tìm_kiếm quyền được thể_hiện và tôn_trọng . Trẻ đang cố chứng_tỏ với bạn hay mọi người xung_quanh rằng trẻ có quyền hơn bạn .
- Hãy cố_gắng đừng phản_ứng thái_quá khi trẻ cãi tay_đôi . Hãy nhớ rằng nếu bạn mất bình_tĩnh thì phần thắng sẽ vào tay trẻ .
- Từ_chối tiếp_tục đối_thoại với trẻ khi trẻ cãi tay_đôi . Hãy cho trẻ biết rằng bạn sẽ không tiếp_tục nói đến khi nào trẻ thôi không cãi chày cãi lối .
- Hãy chọn_lựa hình_phạt thích_đáng . Tối_thiểu là trẻ sẽ không được làm một việc_gì mà trẻ đang cố cãi với bạn để giành được .
- Hãy dạy cho trẻ hiểu rằng trẻ không cần_thiết cứ tuôn ra ào_ào bất_cứ ý_nghĩ nào chợt đến trong đầu khi đối_thoại . Tự kiểm_soát lời_nói là một kỹ_năng quan_trọng khi trẻ giao_tiếp trong xã_hội . Nói cách khác , không có gì xấu nếu trẻ nghĩ rằng mẹ hành_động kỳ_cục như một bà phù_thủy nhưng việc trẻ nói ra ào_ào thì không tốt chút nào .
- Để_ý những chương_trình truyền_hình và các trò_chơi điện_tử . Có vài phương_tiện giải_trí cổ_vũ cho việc cãi tay_đôi và biến điều đó thành những chi_tiết gây cười hơn là cho đó là hành_động xấu và kém lễ_độ .
LẠI_TÚ_QUỲNH dịch ( Theo The_Mother of All_Parenting_Books )

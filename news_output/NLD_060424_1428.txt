﻿ Mãn_kinh - phụ_nữ hay bị trầm_cảm
Mãn_kinh là giai_đoạn phụ_nữ dễ mắc chứng trầm_cảm nhất . Các nhà_khoa_học đã tìm ra mối liên_hệ trực_tiếp giữa việc thay_đổi hormon với các triệu_chứng trầm_cảm và rối_loạn .
Trong giai_đoạn này , nguy_cơ mắc bệnh trầm_cảm ở phụ_nữ tăng 4 lần so với các giai_đoạn khác trong cuộc_đời . Đồng_thời nguy_cơ rối_loạn suy_nhược cũng tăng 2,5 lần .
Hiện_nay , các nhà_khoa_học đã tìm ra biện_pháp giúp phụ_nữ tránh được các triệu_chứng trầm_cảm do việc thay_đổi hormon . Đó là phương_pháp thay_thế hormon ( HRT ) . Phương_pháp này giúp tăng lượng hormon động_dục nữ giảm các triệu_chứng trầm_cảm .
Tuy_nhiên , hiện_nay vẫn còn nhiều tranh_cãi về phương_pháp HRT và có ý_kiến cho rằng phương_pháp này có_thể gây ung_thư vú .
Theo Tiền_Phong

﻿ Giá mới của dịch_vụ tắm bùn & amp ; suối khoáng nóng tại Nha_Trang
- Ngâm bùn khoáng tập_thể : 60.000 đ/véngười lớn ( NL ) và 30.000đ/vé trẻ_em ( TE ) . Mỗi loại vé vừa nêu đều tăng 10.000đồng
- Ngâm nước_khoáng nóng : 50.000đ/vé NL ( tăng 15.000đồng ) và 25.000đ/vé TE ( tăng 10.000đồng )
- Hồ bơi nước_khoáng ấm : 30.000đ/vé ( tăng 10.000đồng ) . Giá vé dành cho thiếu_niên là 20.000đ/vé và 10.000đ/vé ( đều tăng 5.000đồng )
- Massage - Xông_hơi 70.000đ/vé ( tăng 10.000đồng ) .
- Xe đưa_đón 2 vòng ( trong thành_phố Nha_Trang ) là 30.000đ/vé ( tăng 10.000đồng )
Đối_với các dịch_vụ khác như : V . I . P_SPA , ngâm tắm bùn khoáng nóng đặc_biệt , nhà nghỉ_dưỡng , Bungalow , Berber shop và sản_phẩm bùn tươi - bùn khô thì đều vẫn áp_dụng theo giá hiện_hành .
HOÀNG_GIAO

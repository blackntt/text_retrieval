﻿ Chàng bận_rộn : chờ hay … “ chặt ” ?
Ban_đầu tình_cảm của chúng_em rất tốt , nhưng bây_giờ anh_ấy phải đi dạy thêm ở nhiều nơi nên chúng_em không gặp nhau được . Anh_ấy đi dạy về rất khuya . Em chưa có dịp để biết nhà anh_ấy nên cũng không_thể chủ_động đến gặp .
Gần đây , anh_ấy cũng không gọi điện_thoại cho em . Em hỏi anh còn thương em nữa không thì anh nói vẫn thương và yêu em ; nhưng phải ráng chờ anh sắp_xếp thời_gian cho nhau . Em rất buồn nhưng không dám trách vì trước_đây tự em chủ_động ngỏ lời với anh . Ban_đầu anh không đồng_ý , bảo em còn quá nhỏ , phải lo học . Anh còn bảo hoàn_cảnh của anh còn khó_khăn ( bố anh sống ở quê ; anh chưa có nhà , không có thời_gian nên khó lo cho em được ) .
Anh_ấy còn nói yêu anh_em sẽ khổ vì anh thuộc loại người hay lo chuyện của người_ngoài hơn . Em nghĩ điều đó không quan_trọng vì em rất thương anh . Nhất_là khi anh_ấy từng nói sẽ chờ đến khi em tốt_nghiệp ĐH .
Theo cảm_nhận của em , anh_ấy rất tốt với em và tôn_trọng em . Nhưng em phân_vân quá : liệu với cách cư_xử của anh_ấy hiện_nay thì em nên đợi thêm một thời_gian nữa hay dứt_khoát chia_tay
duahaudo … @ yahoo.com

﻿ Vĩnh_Long : 147 SV tốt_nghiệp ĐH về xã , phường , thị_trấn
Bình_quân mỗi xã , phường nhận hai SV . Hai huyện Vũng_Liêm và Tam_Bình có số SV đến nhận công_tác nhiều nhất tỉnh .
Đến nay , trong tổng_số 1.845 cán_bộ cơ_sở của Vĩnh_Long đã có 113 cán_bộ xã , phường có trình_độ ĐH trở_lên ( 34 người đã chuyển công_tác về huyện ) , 334 người đạt trình_độ trung_cấp , cao_đẳng và 872 người chưa qua đào_tạo .
Vĩnh_Long phấn_đấu đến năm 2010 có 95% cán_bộ cơ_sở qua đào_tạo trung_cấp trở_lên . Trước_mắt , tổ_chức thi_tuyển người đúng chuẩn và thấp nhất phải có trình_độ trung_cấp .
LÊ_MỸ

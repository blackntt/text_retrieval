﻿ 8X “ vàng_mười ”
Gọi_là “ vàng_mười ” , bởi cả tấm HCV bơi 100m ếch của Nguyễn_Hữu_Việt và tấm HCV chạy 100m của Vũ_Thị_Hương tại SEA Games 23 đều đi vào lịch_sử của thể_thao Việt_Nam ...
Đó là những tấm HCV quốc_tế đầu_tiên ở môn bơi_lội và chạy tốc_độ , kể từ khi thể_thao Việt_Nam bước ra đấu_trường khu_vực .
Việt và Hương đều là những VĐV thế_hệ 8X . Và phía sau tấm HCV tựa “ vàng_mười ” của họ là những câu_chuyện mà không phải ai cũng biết …
“ Kình_ngư ” nhút_nhát
Việt tập bơi với mẹ từ hồi 7 tuổi , khi mẹ Việt – vốn là một VĐV bơi_lội của Hà_Nội – ngày_ngày ra bể_bơi gần nhà dạy các em học_sinh của xã Phục_Lễ ( Thủy_Nguyên , Hải_Phòng ) .
Cậu_bé còm_nhom ngày ấy dù rất sợ nước nhưng vẫn bám theo mẹ chỉ vì một lý_do khác còn “ ghê_gớm ” hơn : Sợ ma khi ở nhà một_mình .
Ngày đầu_tiên dạy con tập bơi , chị Thảo_Nhung – mẹ Việt – phải nhờ mấy cậu học_trò khiêng con … quẳng xuống_nước khiến cậu_bé vừa lóp_ngóp dưới bể , vừa khóc toáng lên .
Nhưng chỉ 2 năm sau ngày bị “ cưỡng_bức ” tập bơi ấy , Việt đã bơi nhanh như một chú rái_cá và ẵm luôn tấm HCV bơi 100m ếch lứa tuổi dưới 10 ở giải trẻ toàn_quốc năm 1997 trong màu áo Hải_Phòng .
Thấy “ thằng bé bơi hay_hay ” , HLV Chu_Thị_Bằng đề_nghị mẹ Việt giao con cho mình để đưa Việt vào tập ở Trung_tâm TDTT Hải_Phòng . Có hai đứa con : một trai , một gái , bố_mẹ Việt phải đắn_đo mãi mới đồng_ý với một yêu_cầu : Chỉ cho con tập_trung nếu có cô Bằng – người bạn của gia_đình - ở bên_cạnh .
Việt bắt_đầu những ngày tập_luyện xa nhà với sự chăm_sóc của cô Bằng như một người_mẹ thứ_hai . Tập_trung ở Đà_Nẵng , cô Bằng cùng đi với Việt . Gần 2 năm trời khổ_luyện dưới nước ở Nam_Ninh ( Quảng_Tây , Trung_Quốc ) , Việt cũng không xa “ mẹ Bằng ” .
Hôm bơi vòng_loại ở SEA Games 23 , cũng là lúc Việt vừa trải qua cơn_sốt do … mọc răng_khôn , phải ăn cháo gần 1 tuần_lễ . Khi đó , chuyên_gia Hoàng_Gia_Huy và cô Bằng chỉ dặn cậu học_trò bơi giữ sức để tránh sự chú_ý của đối_thủ nhưng không hiểu do quá mệt hay “ nhập_vai ” thành_thật quá mà Việt chỉ xếp_hạng 8/9 , vừa đủ để được vào bơi chung_kết , khiến thầy Huy và cô Bằng suýt nghẹt_thở bên hồ bơi Trace ( Los_Banos ) .
“ Em cứ cắm_đầu bơi thôi , chứ cũng không biết các VĐV khác bơi thế_nào . Sau_này cô Bằng kể , 75m đầu em vẫn bơi sau so với Worrawuti , VĐV Thái_Lan đã đứng đầu vòng_loại , nhưng tới 25m cuối thì em mới bứt lên hơn Worrawuti đúng một gang tay .
Chạm tay vào thành bể rồi , phải nhìn lên bảng đồng_hồ điện_tử , em mới tin rằng mình về nhất ” - Việt cười bẽn_lẽn kể về khoảnh_khắc huy_hoàng ở buổi thi chung_kết chiều 30/11/2005 .
… Chúng_tôi về nhà Việt đúng lúc nhà_vô_địch 17 tuổi này dắt xe_đạp đi tập . Quãng đường đến_nơi tập dài hơn 15 km , ngày nào Việt cũng hai lần đi xe_đạp ra bến xe_buýt , đi tiếp xe_buýt sang trung_tâm thành_phố Hải_Phòng rồi lại tiếp_tục đạp xe đến Trung_tâm TDTT và ngược_lại sau buổi tập .
Không dùng điện_thoại_di_động , cũng không biết đi xe_máy , sau mỗi ngày tập_luyện , “ kình_ngư ” Nguyễn_Hữu_Việt chỉ mê_mải chơi điện_tử trên máy_tính .
Hôm từ SEA Games trở_về , Việt rúc đầu_vào lòng mẹ : “ Mẹ cho con đi tập xa nhà thêm ít_lâu nữa , chứ bây_giờ con không muốn làm điều gì để mọi người lại bảo con mắc bệnh sao ” .
“ Nữ_hoàng ” bướng_bỉnh
Nếu_Việt trầm_lặng và nhút_nhát thì “ nữ_hoàng tốc_độ ” Vũ_Thị_Hương – cô_gái Thái_Nguyên - lại tạo ấn_tượng cho mọi người bằng một cá_tính mạnh_mẽ , hệt như guồng chân của cô trên đường chạy 100 m .
13 tuổi , Hương đã biết yêu . Đi thi Hội khỏe Phù_Đổng toàn_quốc ở Hải_Phòng năm 1999 , làm_quen với một anh_chàng 16 tuổi cùng đoàn , rồi thành mối tình_đầu lúc_nào không hay .
Vừa yêu vừa … chạy , sau khi giành tấm HCĐ 100m ở SEA Games 22 , phá KLQG , cuối tháng 2/2004 , Hương đưa ra một quyết_định khiến tất_cả các thầy_cô , gia_đình phải choáng_váng : Lấy chồng .
Quyết_định lấy chồng của Hương khiến bố_mẹ cô giận con_gái không thèm nhìn mặt đến hơn nửa năm trời , các HLV và đồng_đội cũng lạnh_nhạt , không hiểu hay_là có chuyện gì nên mới_phải vội thế .
Cưới xong , Hương còn phải … viết bản kiểm_điểm ở đội Thái_Nguyên với lý_do lấy chồng sớm , vi_phạm hợp_đồng . ĐTQG tập_trung , Hương cũng không được gọi , vì ai cũng nghĩ , lấy chồng rồi thì còn chạy làm_sao được nữa .
Bỏ_qua những ánh mắt nửa giận , nửa thương_hại , Hương cắn_răng lao vào tập_luyện . 8 tháng sau ngày lấy chồng , Hương giành 1 HCV , 1 HCB ở giải VĐQG mang về tặng chồng – mối tình_đầu năm_xưa .
Nhờ “ món quà tặng chồng ” ấy , tháng 1/2005 Hương lại được gọi vào ĐTQG . Nhưng ở ĐTQG , cô lại không được coi là niềm hy_vọng ở cự_ly 100m .
Khi BHL chuyển cô sang tập ở tổ chạy trung_bình , Hương cương_quyết từ_chối chỉ với một lý_do duy_nhất : “ Em cảm_thấy mình phù_hợp hơn với chạy 100m và 200m ” . Giữa tháng 5/2005 , Hương bị trả về địa_phương .
Ngày lại ngày , bố chồng lại chở nàng dâu đi tập cùng_với những lời động_viên an_ủi của mẹ chồng . Không còn đường piste êm_ái , thay vào đó là đường chạy xỉ than ở SVĐ Thái_Nguyên đã xây_dựng từ năm 1984 , Hương vẫn tập miệt_mài , bất_chấp cả những khi ngã bị xỉ than cào đến tóe máu .
3 tháng sau ngày bị trả về , ở giải VĐQG tháng 10/2005 trên sân Mỹ_Đình ( Hà_Nội ) , Hương giành HCV ở cả nội_dung 100m và 200m , như một lời cảm_ơn tất_cả những gì mà gia_đình chồng đã chăm_lo .
Chính_sự khẳng_định ấy đã khiến cô được gọi trở_lại ĐTQG vào ngày 5/11/2005 , đúng ngày đoàn thể_thao Việt_Nam làm lễ xuất_quân lên_đường dự SEA Games 23 .
Chỉ có chưa đầy 15 ngày để “ luyện thi cấp_tốc ” , Hương lại lao vào “ cày ” bài để rồi trở_thành cô_gái chạy nhanh_nhất Đông_Nam á , phá luôn KLQG của chính mình với thành_tích 11 giây 49 , mang về “ tấm HCV quý_giá nhất của điền_kinh Việt_Nam mà ngay cả trước năm 1975 cũng chưa có ai giành được ” , như lời của HLV Dương_Đức_Thủy .
“ Khi chạy 50m đầu , liếc sang bên thấy vẫn đang bị một VĐV Thái_Lan dẫn trước , mà hôm chạy vòng_loại VĐV này bị chấn_thương khóc kinh lắm , em vẫn còn kịp nghĩ không hiểu sao nó lại chạy ác thế ” -
Trong căn nhà nhỏ của gia_đình chồng , Hương kể lại rộn_ràng , hệt như lúc cô vừa trở_thành “ nữ_hoàng ” trên SVĐ Rizal_Memorial ( Philippines ) chiều 28/11/2005 . Theo Hạnh_Nguyên_Tiền_Phong

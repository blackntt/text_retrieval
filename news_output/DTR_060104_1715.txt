﻿ Văn_Quyến chưa muốn mời luật_sư ?
Sáng nay ( 4/1 ) , trao_đổi với phóng_viên , LS Phạm_Liêm_Chính cho_biết , ông vẫn chưa_thể vào trại tạm giam gặp Văn_Quyến vì theo CQĐT , Văn_Quyến vẫn chưa muốn mời luật_sư .
Trong mấy ngày qua , LS Phạm_Liêm_Chính và và bà Hồ_Thị_Niềm , mẹ cầu_thủ Văn_Quyến đã đến CQĐT làm các thủ_tục xin được cấp chứng_nhận bào_chữa và thăm_nuôi Văn_Quyến .
LS Chính được một lãnh_đạo của Phòng 9-C14 tiếp và cho_biết , trong những buổi làm_việc gần đây với CQĐT , Văn_Quyến đã biết thông_tin LS Phạm_Liêm_Chính có nhã_ý bào_chữa miễn_phí cho mình .
Tuy_nhiên , theo CQĐT , Quyến chưa có nhu_cầu mời luật_sư nên chưa_thể cấp giấy chứng_nhận bào_chữa cho LS Chính cũng_như sự thăm_nuôi của bà Niềm .
" Tôi nói rằng tôi đã có giấy_mời của mẹ Văn_Quyến ký ngày 1/1/2006 và tôi muốn gặp Văn_Quyến trong trại để xác_nhận ý_kiến của Văn_Quyến là Quyến có đồng_ý với mẹ là mời luật_sư hay không .
CQĐT cho_biết , hôm_nay sẽ vào trại tạm giam đưa giấy_mời luật_sư mà bà Hồ_Thị_Niềm đã ký mời LS Chính . Nếu_Quyến nhất_trí với mẹ , Quyến sẽ ký giấy_mời luật_sư từ trong trại " , LS Phạm_Liêm_Chính nói .
Cũng theo LS Chính , lãnh_đạo của Phòng 9-C14 cũng đồng_ý cho bà Niềm được gửi quà tiếp_tế cho Văn_Quyến . Bà Niềm cũng đã gửi một thùng mì_tôm , một hộp bánh trứng và ít quần_áo vào trại tạm giam cho con mình .
Các điều_tra_viên cũng cho_phép gửi tiền vào cho Văn_Quyến tiêu vặt . Anh Lê_Song_Hào , con_nuôi của bà Niềm đã gửi 500 ngàn vào cho Quyến .
Sáng 4/1 , LS Chính cũng đã được CQĐT thông_tin , đến thời_điểm này Quyến vẫn mạnh_khoẻ và không có vấn_đề gì về tư_tưởng . LS Chính đã được CQĐT cho đọc lời khai gần đây nhất của Quyến về việc chưa có nhu_cầu mời luật_sư .
Bà Hồ_Thị_Niềm cho_biết , sau khi nghe luật_sư nói vậy , bà cũng chỉ biết chờ_đợi chứ chẳng biết làm_gì hơn .
Theo Thế_Vinh - Bích_Ngọc_Vietnamnet

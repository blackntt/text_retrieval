﻿ EU thành_lập hãng thông_tấn mới
Ý_tưởng này được đưa ra do Ủy_ban châu Âu cho rằng người_dân các nước thành_viên EU thiếu thông_tin về EU và vì_vậy các công_dân Pháp , Hà_Lan và một_số nước châu Âu khác đã bác_bỏ Hiến_pháp mới của EU .
Dự_án cũng sẽ thiết_lập bộ_luật ứng_xử cho các nhà_báo và các cơ_quan báo_chí EU .
Dự_án liệt_kê 5 nguyên_tắc hoạt_động cho các nước thành_viên EU : “ Hạn_chế các nội_dung thường thấy , trao quyền hợp_pháp cho người_dân , làm_việc với các công_nghệ mới và công_nghệ truyền_thông , am_hiểu về quan_điểm chung của EU và cùng làm_việc với nhau ” .
Trong đề_nghị thành_lập cơ_quan báo_chí mới , Ủy_ban châu Âu cũng đề_xuất mở_rộng kênh_truyền_hình EuroNews phát liên_tục 24/24 giờ trong ngày .
T.VY ( Theo UPI )

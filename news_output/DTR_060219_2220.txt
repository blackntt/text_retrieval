﻿ Các cuộc đối_đầu lịch_sử giữa Chelsea và Barca
Gặp nhau không nhiều tại các Cup châu Âu , nhưng cứ mỗi lần giao_chiến , hai CLB này lại ghi dấu_ấn lịch_sử khó mờ phai . Có một điều hết_sức đặc_biệt , đó là cứ bên nào được chơi trên sân_nhà thì y_như_rằng họ sẽ giành chiến_thắng . Đêm thứ_tư tới , họ sẽ đấu lượt_đi vòng 16 đội Champions_League .
Nếu xét về danh_tiếng tại châu Âu , Chelsea với hai chiếc Cup C2 ( năm 1971 và 1998 ) chẳng_thể_nào sánh bằng Barcelona ( 1 Cup C1 1992 , 4 Cup C2 các năm 1979 , 1982 , 1989 , 1996 , và 3 Cup C3 năm 1958 , 1960 , 1966 ) .
Nhưng hễ cứ đụng nhau , hai CLB lại như " nước với lửa " , luôn đem đến những bất_ngờ khó tin nhất .
270 phút xúc_cảm trái_ngược cho lần đầu_tiên
( Bán_kết Cup_Hội chợ - tiền_thân của Cup UEFA , mùa bóng 1965/1966 , lượt_đi : Barca 2 - 0 Chelsea ; lượt_về : Chelsea 2 - 0 Barca ; play-off : Barca 5 - 0 Chelsea )
Thời_điểm này thuộc về giai_đoạn tương_đối " trầm " trong lịch_sử Barcelona . Đáng_kể nhất trong đội_hình của họ khi ấy chỉ là trung_vệ tài_năng Gallego . Nhưng , bằng hai bàn của Fuste và đội_trưởng Zaldua , Barca dễ_dàng cho Chelsea đo_ván 2 - 0 trong lượt_đi .
Đáng chú_ý , cầu_thủ nổi_bật bên phía đội khách là Terry_Venables , người về sau làm HLV giúp Barca vào chung_kết Cup C1 năm 1986 , và đưa đội_tuyển quốc_gia quê_hương lọt tới bán_kết Euro 1996 .
Gallego - trụ_cột của Barca
mùa bóng 1965/1966 .
Giành lợi_thế ở lượt_đi , nhưng lượt_về trên đất Anh lại là một trận cầu hết_sức tệ_hại cho đại_diện Tây_Ban_Nha . Sau khi cầm hòa thành_công trong hiệp một , Barca bỗng_dưng để thủng lưới hai lần trong vòng 7 phút , tất_cả đều đến từ các pha phản lưới nhà của Gallego và Reina .
Kết_quả hòa 2-2 chung_cuộc buộc hai đội phải đá một trận play - off . Và may_mắn đã thuộc về Barca khi lá thăm giúp họ được chơi trên sân_nhà . Chẳng bỏ lỡ cơ_hội bằng vàng , đoàn quân của HLV người Argentina , Roque_Olsen , đè_bẹp đối_thủ đến từ nước Anh bằng một chiến_thắng " 5 sao " , nhờ công Rife ( 2 bàn ) , Zaldua , Fuste và Zaballa .
Vào chung_kết một_cách ngoạn_mục , Barcelona xuất_sắc thắng tiếp Zaragoza với tổng tỷ_số 4-3 , dù đã thua trước 0 - 1 ở lượt_đi trên sân_nhà . Đó là lần thứ 3 họ nâng cao chiếc Cup_Hội chợ và trở_thành một trong 4 CLB đoạt nhiều Cup C3 nhất ( cùng Juventus , Inter_Milan và Liverpool ) .
Lần thứ_hai , Barca ngược dòng ngoạn_mục
( Tứ_kết Champions_League mùa bóng 1999/2000 , lượt_đi : Chelsea 3-1 Barca ; lượt_về Barca 5-1 Chelsea )
Ở thời_điểm đó , Barca của HLV Louis_Van_Gaal mạnh khủng_khiếp . Đoàn quân xứ Catalan " cuốn_trôi " mọi địch_thủ như Arsenal , Porto , Fiorentina , Hertha_Berlin trong hai vòng đấu bảng đầu_tiên , với thành_tích cực_kỳ thuyết_phục : 9 thắng , hòa 3 . Nổi_bật lên là phong_độ chói sáng của cặp tiền_vệ siêu_việt , Rivaldo - Figo .
Tuy_nhiên , điều mà Van_Gaal và các học_trò chẳng ngờ tới là một cái bẫy đã dành sẵn cho họ trong chuyến chuyến hành_quân tới Stamford_Bridge , tối 4/4/2000 . Bị cuốn vào thứ chiến_thuật phòng_thủ chặt kết_hợp phản_công cực nhanh do HLV Vialli " lập_trình " , Barca lộ rõ điểm yếu về tâm_lý .
Hậu_quả là lưới của đội khách bị rung lên tới 3 lần chỉ trong vòng 9 phút của hiệp một , bởi Zola ( 30 ' ) và Flo ( 34 ' , 38 ' ) . Nhưng dù_sao họ cũng vớt_vát được chút thể_diện . Figo đi bóng tuyệt đẹp trước khi dứt_điểm tung_lưới thủ_môn Chelsea ở phút 64 , đem lại hy_vọng nhỏ_nhoi lật_ngược tình_thế ở lượt_về .
Hạ gục một trong số_ít đội hàng_đầu châu Âu lúc đó , HLV Vialli và các học_trò hồ_hởi ra_mặt . Nhà cầm_quân trẻ này cũng thừa hiểu sức tấn_công khủng_khiếp của Barca một_khi được chơi tại Nou_Camp , nhưng là một người Italia , ông tự_tin vào những kiến_thức phòng_ngự thu_hoạch được sau nhiều năm chinh_chiến tại Serie A .
Trong khi đó , sự lựa_chọn dành cho gã khổng_lồ Catalan không_chỉ là " phải thắng lượt_về " , mà_còn là " phải thắng thật đậm " . Nhiều chuyên_gia đã đoán trước 60% khả_năng Chelsea đi tiếp , còn các nhà_cái thận_trọng hạ thấp tỷ_lệ đặt_cược dành cho đội bóng Anh .
Pha truy cản của trung_vệ Leboeuf ( Chelsea ) với
Kluivert trong trận đấu năm 2000
Nhưng , bóng_đá luôn đi liền với hai chữ bất_ngờ , và yếu_tố này càng được đẩy cao hơn trong cặp đấu Barca - Chelsea . Cái hàng phòng_ngự dày_đặc mà Vialli bố_trí đã bị chọc thủng tan_hoang ở trận lượt_về , bởi sự di_chuyển liên_tục của các kỹ_thuật gia_chủ nhà .
Rivaldo và Figo đưa Barca dẫn 2 - 0 sau 45 phút thi_đấu . Chelsea bất_ngờ le_lói hy_vọng bằng bàn gỡ của Flo phút 60 . Nhưng cú đánh_đầu hiểm_hóc của Dani phút 83 đã san bằng tỷ_số 4-4 hai lượt . Thậm_chí , lẽ_ra đội chủ sân Nou_Camp chẳng cần đến hiệp phụ nếu Rivaldo không bỏ lỡ quả phạt_đền ở cuối trận .
Tuy_vậy , bằng một quả 11 m khác phút 99 , siêu_sao người Brazil đưa Barca dẫn 4-1 lượt_về , trước khi Kluivert kết_thúc một chiến_thắng hoàn_hảo cho đội nhà 5 phút sau . Chelsea trở_về với thực_tại sau hai tuần " trên mây " .
" Người_Đặc_Biệt " ( bên phải ) khi còn
làm trợ_lý cho Van_Gaal tại Barca
Đáng chú_ý , ăn_mừng cùng Barca lần đó còn có trợ_lý của HLV Van_Gaal , người sau_này chính đội bóng xứ Catalan nhớ mãi cho tới ngày hôm_nay , Jose_Mourinho .
Dấu_ấn Mourinho trong cuộc chạm_trán thứ_ba
( Vòng hai Champions_League mùa 2004/2005 , lượt_đi : Barca 2-1 Chelsea ; lượt_về : Chelsea 4-2 Barca )
Hai trận gặp nhau mùa trước hầu_như ai cũng biết . Do quá hiểu Barca khi còn là trợ_lý ở CLB này , Mourinho áp_dụng chiến_thuật thận_trọng ở Nou_Camp lượt_đi và ông suýt thành_công với bàn đá phản của Belletti phút 33 .
Nhưng kể từ lúc Maxi_Lopez vào sân phút 64 , hàng thủ 4 người của Chelsea lập_tức bị rối_loạn bởi phải đối_phó với quá nhiều mũi tấn_công nguy_hiểm . Đích_thân tiền_đạo trẻ người Argentina dũng_mãnh xâm_nhập vòng cấm đối_phương sút hạ thủ_môn Petr_Cech gỡ hòa 1-1 ở phút 67 .
Chỉ 6 phút sau , một cú sút khác do Maxi_Lopez thực_hiện xuyên chéo khu_vực 16 m50 của Chelsea , vô_tình biến thành đường chuyền thuận_lợi cho Eto ' o đang lao tới như tên bắn , ấn_định tỷ_số 2-1 trận lượt_đi .
Chiến_thắng của Barca khiến các nhà bình_luận , giới chuyên_môn và HLV tại châu Âu thán_phục , vì Chelsea đương là đội bóng khó khuất_phục nhất . Hầu_hết đều nghĩ Barca với hàng công cực mạnh sẽ bảo_vệ được thành_quả trong lượt_về , chỉ trừ một người , Jose_Mourinho .
Năm_ngoái , Chelsea đã chiến_thắng bằng cái
đầu của Mourinho
Mất_Drogba , tiền_đạo số_một , do nhận thẻ_đỏ lượt_đi . Vắng_Robben , cầu_thủ chạy cánh không_thể thay_thế , do chấn_thương . Mặc_kệ !
Nhà cầm_quân người Bồ_Đào_Nha đưa vào sân 11 học_trò , và ra_lệnh xông lên khi đối_thủ chưa kịp ổn_định đội_hình cũng_như tâm_lý , để ghi liền 3 bàn trong 19 phút đầu do công Gudjohnsen ( 8 ' ) , Lampard ( 17 ' ) , Duff ( 19 ' ) , bằng thứ " vũ_khí " hết_sức giản_đơn : tận_dụng tối_đa mọi sai_sót của hàng tiền_vệ Barca và tung những cú chọc khe " chết người " .
Ronaldinho gỡ lại hai bàn cho đội khách , nhưng quả đánh_đầu ở phút 76 của John_Terry đã kết_thúc sớm cuộc phiêu_lưu của thày trò HLV Rijkaard . Còn_Mourinho thêm một lần lừng_lẫy tiếng_thơm .
Xen giữa hai lượt trận là cuộc tranh_cãi có_một_không_hai trong lịch_sử , khi Mourinho tố_cáo Rijkaard " ăn_giơ " với trọng_tài Anders_Frisk . Nhưng dù thế_nào , kết_quả trên sân vẫn khẳng_định đội xứng_đáng đi tiếp là Chelsea .
Các cặp đấu vòng 16 đội Champions_League 05/06 :
2h45 , 22/2
Bayern_Munich - AC Milan
Benfica - Liverpool
Real_Madrid - Arsenal
PSV Eindhoven - Lyon
2h45 , 23/2
Chelsea - Barcelona
Werder_Bremen - Juventus
Ajax - Inter_Milan
Rangers - Villarreal
( Lượt_đi ngày 21 và 22/2/2006 , lượt_về ngày 7 và 8/3 . Đội đứng trước phải đá lượt_đi trên sân_nhà . Riêng trận lượt_về Inter_Milan - Ajax lùi lại tới ngày 14/3 ) .
Theo Tiến_Dũng_Vnexpress

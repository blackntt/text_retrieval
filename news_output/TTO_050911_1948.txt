﻿ Thông_điệp yêu_thương : Dẫu thế_nào em vẫn sẽ về
Nhưng em lại sợ khi nghĩ đến những khoảng_cách đang lớn dần giữa anh và em . Thời_gian và khoảng_cách đã lấy đi của em và anh những giây_phút lãng_mạn của tuổi_trẻ và thấy bao nỗi nghi_ngờ và sợ_hãi ...
Dẫu thế_nào em vẫn sẽ về , về để tìm lại hạnh_phúc của mình ...
Em - KK
" Thầy " nhớ_thương nhiều !
" Trò " chỉ muốn nói với " Thầy " một câu thôi : " Trò vẫn rất thích Thầy , vẫn chờ Thầy trở_về bên trò ! " , khi " Thầy " đọc được những dòng này , xin " Thầy " hãy tin " Trò " , hãy cho " Trò " thêm một cơ_hội nữa nhé !
TRAN_NGA
Anh yêu của em , đã gần 4 tháng rồi mình không gặp nhau . Em nhớ anh nhiều lắm . Mặc_dù mình không được ở gần nhau nhưng em lúc_nào cũng nhớ đến anh . Em muốn nói với anh một điều là " Em yêu anh nhiều lắm ! Anh hãy cố_gắng học nha . Em chúc anh thành_công ! "
NGUYỄN_THỊ_ÁI_LY
Điều đầu_tiên em muốn nói : EM_YÊU_ANH_RẤT_NHIỀU ! Anh yêu , chúng_mình yêu nhau đã 2 năm rồi nhỉ , em cảm_thấy thật hạnh_phúc khi có anh , hãy như bây_giờ anh nhé , yêu em , lo_lắng cho em . Em yêu anh và mãi yêu anh .
N . T .
Em thật hạnh_phúc khi ở bên anh , anh là tình_yêu của em , là tất_cả những gì em có . Tình_yêu chúng_ta thật đẹp phải không anh , có những lúc vui_vẻ ở bên nhau , có lúc thì giận_hờn ... Nhưng điều đó càng làm cho tình_yêu chúng_ta thêm nồng_thắm , làm em càng yêu anh , em hy_vọng với tình_yêu , anh và em sẽ vượt qua tất_cả để đến với nhau . Hãy tin vào điều đó nghe anh ! Em sẽ không_bao_giờ để anh phải xa em và em cũng thế !
Càng xa anh , em mới thấy cô_đơn và lạnh_lẽo , em càng hiểu em yêu anh như_thế_nào ...
NGUYỄN_MINH_TH .
bonbonsocola khờ thương
Anh không_bao_giờ thất_vọng về em cả , mà anh rất tự_hào về em , anh cảm_ơn cuộc_đời đã cho anh được gặp em , được em yêu . Trong lúc khó_khăn này , mình sẽ cùng nhau vượt qua tất_cả em nhé . Hãy cho anh chia_sẻ cùng em . Em đừng có cứng_đầu nữa nha , anh sẽ làm tất_cả những gì có_thể tốt cho cả hai ta , em sẽ luôn ở bên anh phải không em ?
Anh rất cô_đơn khi_không có em bên_cạnh và anh không biết mình sẽ như_thế_nào nếu_không có em , vì_thế đừng bao_giờ nói chia_tay em nhé ! Em yêu anh và anh cũng vậy , vì_thế không có lý_do gì để chúng_ta chia_tay , em hãy bỏ hết những suy_nghĩ ngốc xít ấy đi nha . Anh sẽ đợi em , và không làm em buồn nữa . Mối tình nào mà chẳng có lúc trắc_trở nhưng chúng_ta biết vượt qua để mãi_mãi ở bên nhau , em hãy cùng anh thực_hiện điều đó nha .
CON_HEO_CÒI
uTym của anh !
Mình đến với nhau thật nhẹ_nhàng phải không em ? Cảm_ơn những ngày Hội_An ngắn_ngủi nhưng tràn_đầy hạnh_phúc . Anh đã ích_kỷ khi nói với em rằng , anh sợ tụi mình phải sống xa nhau , sợ rằng xa mặt sẽ cách lòng . Dù em ở đâu , nụ_cười của em vẫn tràn_ngập trong anh . Tụi mình đang bước chung trên một con đường dài , rất dài , hãy vững_vàng em nhé ! Giữ hộ anh trái_tim này nhé !
Dimero
Quân anh !
Thực_lòng khi ra_đi , em thấy mình có lỗi . Nhưng để làm_gì khi trái_tim anh không thuộc về em . Tại tính anh đa_tình hay vì em ích_kỷ ? Dù_sao cũng vậy , anh hãy vui bên người đó ! Tình_yêu nơi em mãi_mãi thuộc về anh dù bây_giờ anh không còn là người em yêu ! Hãy hạnh_phúc anh nhé ! Xin anh tha_thứ cho em ... Trái_tim em không_thể yêu hai người !
becon tinhnghich 16
Bạn có_thể gửi về Tuổi_Trẻ_Online nội_dung thông_điệp bạn muốn gửi đến cho " một_nửa " của mình , nội_dung không quá 50 chữ . Ngoài_ra , bạn có_thể đính kèm một tấm hình bạn muốn chuyển . Bài viết tham_gia xin gửi về email online@tuoitre.com.vn , chủ_đề ghi rõ là tham_gia chuyên_mục Thông_điệp yêu_thương .
TTO

﻿ Vụ điện_kế điện_tử : Hôm_nay EVN xin_lỗi khách_hàng
Chủ_tịch HĐQT_EVN cũng đề_nghị Công_ty Điện_lực TP chuẩn_bị đầy_đủ thiết_bị để thay điện_kế điện_tử bằng điện_kế cơ . Thời_gian thực_hiện từ 1-9-2005 đến 31-3-2006 . Nếu việc chuẩn_bị sớm hơn có_thể tiến_hành trong tháng tám tới .
Theo Công ty Điện_lực TP , do số_lượng điện_kế quá lớn cần phải thay trong một thời_gian ngắn nên có khả_năng công_ty sẽ ký hợp_đồng trực_tiếp mua một_số điện_kế hiệu EMIC trong giai_đoạn đầu . Sau đó công_ty tiến_hành đấu_thầu chọn đơn_vị cung_cấp .
Thông_tin từ công_ty cũng cho_biết loại điện_kế này đã được đăng_ký mẫu với Tổng_cục Tiêu_chuẩn đo_lường chất_lượng . Riêng việc kiểm_định vẫn thực_hiện tại Trung_tâm Thí_nghiệm điện , thuộc Công_ty Điện_lực TP . Trong trường_hợp đơn_vị này kiểm_định không xuể sẽ thuê đơn_vị có chức_năng .
Liên_quan đến việc bồi_thường thiệt_hại cho khách_hàng sử_dụng điện_kế điện_tử , Công_ty Điện_lực TP cho_biết sẽ lấy ý_kiến của UBND_TP , Sở Công nghiệp , Văn_phòng Khiếu_nại của người tiêu_dùng , Ủy_ban MTTQ_TP , Chi_cục Tiêu_chuẩn đo_lường chất_lượng TP ... trước khi triển_khai thực_hiện . Việc này sẽ được tiến_hành sớm trong thời_gian tới .
Đối_với việc xử_lý 312.000 điện_kế và khoản tiền gần 11,3 triệu USD ( chưa có thuế VAT ) mua điện_kế vẫn chưa đưa ra bàn trong cuộc họp .
PHÚC_HUY

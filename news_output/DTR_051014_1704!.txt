﻿ Tao_nhã nơi “ Trở_lại Gốm ”
Nằm ngay cạnh mặt_đường Quang_Trung ồn_ào náo_nhiệt xe_cộ và người đi đường , " Trở_lại Gốm " ( 34 Quang_Trung , Hà_Nội ) là một địa_chỉ tiện_lợi và thú_vị cho bạn lúc dừng chân .
" Trở_lại Gốm " là sự pha_trộn của một không_gian hoài_cổ . Từ chiếc ghế_ngồi , cái bàn đến những vật trang_trí của từng góc quán đều toát lên vẻ đẹp tao_nhã , sang_trọng . Quán cũng khá kén người nên chỉ mở_cửa phục_vụ khách theo giờ : sáng 7h - 9h30 , chiều 16h - 19h30 .
Các bình gồm , lọ gốm , cho_đến đồ trang_sức tô_điểm thêm nét đẹp của bạn_gái làm từ chất_liệu gốm đều được tạo nên từ bàn_tay khéo_léo , nghệ_thuật của lão nghệ_nhân Nguyễn_Chi . Giá mỗi_một bình gốm , lọ gốm nghệ_thuật khoảng 100 nghìn đồng / chiếc .
Cô chủ quán với giọng nói nhẹ_nhàng , mềm_mại khẳng_định , nét độc_đáo thu_hút các khách_hàng thân_thuộc của cô còn ở chỗ : Quán có một loại cà_phê đặc_biệt , xuất_xứ từ Huế , nay lại có_mặt ở Hà_thành .
Nguyễn_Hiền

﻿ Khen_thưởng 55 sinh_viên 3 tốt
( NLĐ ) - Ngày 5-1 , Hội_Sinh viên TPHCM chính_thức công_nhận danh_hiệu Sinh_viên 3 tốt cho 55 sinh_viên các trường đại_học , cao_đẳng tại TPHCM trong tổng_số 126 sinh_viên đạt tiêu_chuẩn từ cấp trường .
Các sinh_viên 3 tốt đã đạt các tiêu_chuẩn như : học_tập tốt ( điểm trung_bình 8,0 trở_lên , có đề_tài nghiên_cứu khoa_học hoặc bằng C ngoại_ngữ ) , rèn_luyện tốt ( hội_viên Hội_Sinh viên , đoàn_viên xuất_sắc , tham_gia các phong_trào đoàn_thể , hiến máu nhân_đạo ... ) , thể_lực tốt ( đoạt danh_hiệu thanh_niên khỏe , thành_viên đội_tuyển thể_thao cấp tỉnh trở_lên ) . Mỗi sinh_viên sẽ được tặng_thưởng biểu_trưng , giấy chứng_nhận Sinh_viên 3 tốt của Hội_Sinh viên TPHCM , bằng_khen của UBND_TPHCM và 1 triệu đồng .
H.Nhã

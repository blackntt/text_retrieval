﻿ Ronaldo với mục_tiêu lịch_sử
Tiền_đạo người Brazil đang đặt mục_tiêu giành danh_hiệu Vua_phá_lưới World_Cup lần thứ 2 liên_tiếp và qua đó , “ hạ_bệ ” kỷ_lục gia người Đức_Gerd_Muller trong danh_sách những cây săn bàn xuất_sắc nhất lịch_sử World_Cup .
Đây đã là kỳ World_Cup thứ 4 liên_tiếp của tiền_đạo này , dù tại USA 94 anh không được ra sân một phút nào và chỉ tham_dự để ... mở_rộng tầm_mắt !
Bốn năm sau đó , Ronaldo cùng các đồng_đội đã đi tới trận chung_kết nhưng lại để thua một_cách khó hiểu trước đội chủ nhà Pháp . Bản_thân tiền_đạo “ răng thỏ ” này cũng chỉ 4 lần tìm được mành lưới đối_phương và đành nhìn Davor_Suker giành danh_hiệu Vua_phá_lưới với 6 bàn thắng .
World_Cup 2002 tại Hàn_Quốc và Nhật_Bản mới thực_sự là giải đấu dành cho Ronaldo . Với quả đầu “ ngố ” kỳ_lạ , “ người_ngoài hành_tinh ” đã thể_hiện một phong_độ xuất thần khi độc_chiếm ngôi “ Vua_phá_lưới ” với 8 lần ... nhả đạn thành_công , trong đó có cú đúp trong trận chung_kết gặp ĐT Đức .
Đây cũng là thành_tích ghi_bàn cao nhất của một Vua_phá_lưới tại một VCK_BĐTG kể từ năm 1974 đến nay . Hiện_tại , Ronaldo đã có tổng_cộng 12 bàn thắng ở các kỳ World_Cup và chỉ còn kém kỷ_lục của Gerd_Muller 2 bàn mà thôi .
Bất_chấp vừa trải qua một mùa giải khó_khăn cùng Real_Madrid , cựu cầu_thủ xuất_sắc nhất thế_giới này vẫn hy_vọng anh sẽ “ nổ_súng ” đều_đặn cho ĐTQG và vượt qua kỷ_lục của huyền_thoại người Đức .
“ Tôi muốn giành danh_hiệu Vua_phá_lưới thêm một lần nữa và sẽ thật tuyệt_vời nếu vượt qua được thành_tích của Gerd_Muller . Chỉ 3 bàn thắng nữa thôi !
Tôi hy_vọng Brazil sẽ bảo_vệ thành_công ngôi vô_địch và tôi có_thể ghi được hơn 3 bàn thắng . Dù không thi_đấu thành_công cùng Real nhưng tôi đã sẵn_sàng cho các trận đấu tại Đức ” , Ronaldo tiết_lộ . M . H

﻿ Xứ_Sương mù " vạch_trần " ẩn_số Ecuador ?
Đêm nay , " ba chú Sư_tử " Anh sẽ bước vào vòng knock - out để đối_đầu với " ẩn_số " đến từ Nam_Mỹ : ĐT Ecuador với mục_tiêu có được một chiến_thắng " tưng_bừng " thỏa sự mong_đợi của các CĐV xứ_sở Sương_mù .
Ecuador - một " ẩn_số " thú_vị ...
Chỉ trong vòng hơn một năm qua , Ecuador đã khiến giới chuyên_môn cũng_như người hâm_mộ nhiều phen bất_ngờ bởi chính những màn trình diễn_xuất thần của họ .
Đầu_tiên là một vòng_loại World_Cup tuyệt_vời với những chiến_thắng vang_dội trước cả hai " ông lớn " Argentina và Brazil , qua đó về đích thứ 3 tại khu_vực Nam_Mỹ và kèm theo chiếc vé vào thẳng vòng bảng tại Đức - kỳ Mondial thứ_hai trong lịch_sử quốc_gia này .
Tiếp đó , trong một bảng đấu mà nhiều người dự_đoán 2 vị_trí đứng đầu sẽ không " thoát " khỏi tay hai đại_diện châu Âu_là : chủ nhà Đức và ĐT Ba_Lan , các cầu_thủ Ecuador lại làm lên_cơn chấn_động khi ra_quân bằng 2 chiến_thắng thuyết_phục : 2 - 0 trước Ba_Lan và 3 - 0 trước Costa_Rica , trong những thế_trận của kẻ " bề_trên " .
Họ đã kết_thúc cuộc đua đến vị_trí thứ_hai của bảng A quá chóng_vánh và buộc tất_cả phải nhìn mình bằng một con mắt hoàn_toàn khác : e_dè và thận_trọng .
Rooney càng được kỳ_vọng sau chấn_thương nghiêm_trọng của Owen . ( AP )
... nhưng người Anh vẫn lạc_quan
Đúng vậy , các CĐV xứ Sương_mù hoàn_toàn có_thể lạc_quan vào một chiến_thắng , thậm_chí là với cách_biệt lớn bởi lịch_sử cho thấy những đối_thủ ở mức trung_bình - khá đến từ Nam_Mỹ hầu_như chưa bao_giờ gây được khó_khăn cho " ba chú Sư_tử " .
Thứ nhất , trình_độ của các đội bóng này chưa đủ tầm để tạo ra được một thế_trận khống_chế người Anh hay nói rộng hơn là các " đại_gia " châu Âu , điều mà chỉ Argentina và Brazil mới có_thể làm được .
Thứ_hai , những lần đối_đầu gần đây với các đại_diện Nam_Mỹ ( không tính đến Argentina , Brazil ) , ĐT Anh đều giành chiến_thắng không mấy vất_vả . Trong đó , có_thể kể_ra trận thắng 3 - 0 trước Colombia tại France ' 98 , 4-1 trước Uruguay trong trận giao_hữu hồi đầu năm , hay chiến_thắng dễ_dàng 1 - 0 trước Paraguay tại vòng bảng vừa_qua .
Thậm_chí , ngay cả lần gặp nhau duy_nhất của hai đội cách đây 36 năm cũng kết_thúc với phần thắng 2 - 0 nghiêng về đội bóng xứ Sương_mù .
Tuy_nhiên , đó chỉ là giá_trị rút ra từ lịch_sử , còn hiện_tại HLV Eriksson cùng các học_trò đang phải chịu sức_ép rất lớn sau màn trình_diễn nghèo_nàn tại vòng bảng .
Cộng với vệc các trụ_cột như : Rooney , Lampard , Beckham ( hầu_như chưa thể_hiện được gì ) đang rất muốn khẳng_định lại giá_trị của mình , chắc_chắn ĐT Anh sẽ vào trận với quyết_tâm rất cao nhằm lấy lại niềm_tin nơi người hâm_mộ .
Rõ_ràng đây mới là điều quan_trọng , và Ecuador cũng là đối_thủ hợp_lý để họ thực_hiện mục_tiêu .
Cuộc đối_đầu giữa hai HLV Eriksson - Suarez . ( AFP )
Những thay_đổi bất_ngờ của HLV Eriksson
Gạt sang một bên phong_độ khá tốt của Peter_Crouch , HLV Eriksson quyết_định thay_đổi hoàn_toàn chiến_thuật , khi chuyển từ sơ_đồ 4-4 - 2 đã áp_dụng trong cả 3 trận vòng bảng sang sơ_đồ 4-1 - 4-1 với mũi_nhọn duy_nhất trên hàng công là Wayne_Rooney .
Trong đó , đáng chú_ý nhất_là 2 sự xáo_trộn ở tuyến giữa , đó là việc Owen_Hagreaves - cầu_thủ chơi tốt ở vị_trí tiền_vệ phòng_ngự trong trận hòa 2-2 với Thụy_Điển sẽ thay_thế hậu_vệ phải Jamie_Carragher , và lấp vào vị_trí của anh là Michael_Carrick - tiền_vệ của Tottenham_Hotspur .
Với thay_đổi này , HLV Eriksson đã giải_phóng cho cả Lampard và Gerrard khỏi nhiệm_vụ phòng_ngự , đồng_thời đẩy đội_trưởng của Liverpool lên chơi tiền_đạo lùi ngay sau Rooney .
Trong khi đó , chắc_chắn ĐT Ecuador sẽ ra_quân với đội_hình mạnh nhất và áp_dụng lối chơi phòng_ngự chặt - phản_công nhanh sở_trường . Điểm mạnh của đội bóng này là ở hai cánh với sự góp_mặt của bộ đôi chơi rất hiệu_quả là Valencia và Mendez , đây sẽ là nguồn cung_cấp bóng chủ_yếu cho cặp tiền_đạo nguy_hiểm Agustin_Delgado - Tenorio .
Tuy_nhiên , trước hàng thủ do Terry và Rio_Ferdinand ( mới trở_lại ) chỉ_huy , ĐT Ecuador sẽ không dễ để tiếp_cận khung_thành thủ_môn Robinson .
Liệu với chiến_thuật mới , người Anh sẽ có trận thắng " tưng_bừng " để " bóc_trần " ẩn_số Ecuador ? Câu trả_lời sẽ có tại Stuttgart vào đêm nay ...
Dự_đoán của Dân_trí : Anh thắng 3-1 .
Đội_hình dự_kiến :
Anh : Robinson - Hargreaves , Ferdinand , Terry , A_Cole - Beckham , Gerrard , Carrick , Lampard , J_Cole - Rooney .
Ecuador : Mora - De la Cruz , Hurtado , Espinoza , Reasco - Mendez , Castillo , Valencia , Edwin_Tenorio - Delgado , Carlos_Tenorio .
Trọng_tài : Frank_De_Bleeckere ( Bỉ ) . Phan_Trung

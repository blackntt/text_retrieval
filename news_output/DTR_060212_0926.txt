﻿ 2005 : Doanh_số ĐTDĐ vượt 800 triệu
Doanh_số điện_thoại_di_động trên toàn thế_giới sẽ nhanh_chóng đạt đến con_số hàng tỷ . Theo công_ty nghiên_cứu thị_trường i Suppli , năm nay , thị_trường điện_thoại_di_động sẽ ghi_nhận hơn 850 triệu chiếc được tiêu_thụ .
Năm_ngoái , tổng_số điện_thoại_di_động xuất_xưởng là 812,5 triệu , tăng 14% so với 713 triệu sản_phẩm được tung ra thị_trường năm 2004 , theo công_ty nghiên_cứu thị_trường i Suppli . Điều này có_nghĩa cứ 6 người thì_có 1 người mua điện_thoại mới .
Chỉ riêng quý IV , 241,5 triệu “ dế ” được xuất_xưởng , nhỉnh hơn so với con_số 239 triệu mà i Suppli dự_đoán .
Theo công_ty này , năm nay sẽ có hơn 850 triệu điện_thoại được trao_đổi trên thị_trường . Hãng điện_thoại Phần_Lan_Nokia vẫn là “ đại_gia ” trong lĩnh_vực di_động , xuất_xưởng 265 triệu chiếc trong năm 2005 , tăng 27,6% so với 2004 .
Hiện_tại , Nokia chiếm 32,6% thị_phần trên thị_trường điện_thoại_di_động . Trong khi đó , Motorola , thành_công với điện_thoại siêu mỏng RAZR , đứng ở vị_trí thứ_hai với 18% thị_phần và đã xuất_xưởng tăng 39,7% so với năm 2004 .
Năm qua , Samsung bị thụt_lùi về vị_trí thứ_ba , nhường lại ngôi_vị á_quân cho Motorola .
Trong khi điện_thoại_di_động đang trở_thành thiết_bị phổ_dụng thì máy_tính vẫn còn “ xa_lạ ” với một_số khu_vực ( năm_ngoái chỉ xuất_xưởng 250 triệu PC ) , theo công_ty khảo_sát thị_trường IDC . Các nhà_chức_trách ngành viễn_thông của các tiểu vương_quốc Ả Rập thống_nhất ước_tính , 91% người_dân ở đây có điện_thoại_di_động , trong khi đó , chỉ có 19% có máy_tính . N . H ( theo CNet )

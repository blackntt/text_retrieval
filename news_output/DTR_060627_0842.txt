﻿ Chàng mất “ phong_độ ”
Bình_thường anh_ấy luôn hào_hứng bên bạn . Nhưng gần đây có_vẻ kém nhiệt_tình , phong_độ thất_thường , có_khi mãi chẳng “ giương cao nòng súng ” . Rất có_thể đây là nguyên_nhân :
Bị vợ cằn_nhằn
Không gì khiến chàng mất hưng_phấn bằng việc bạn cứ lải_nhải , ca_cẩm hết chuyện này đến chuyện kia . Chỉ cần một câu than_vãn không đúng lúc , bạn sẽ dập tắt “ ngọn lửa ” của chàng .
Mệt_mỏi
Thức khuya , ăn_không ngon_miệng khiến sức_khỏe và lượng testosterone của chàng giảm_sút . Anh_ấy sẽ rất biết_ơn nếu bạn thường_xuyên tẩm_bổ và nhắc_nhở mỗi khi chàng làm_việc quá nhiều .
Chán_nản
Những chuyện không hài_lòng hoặc vài ý_kiến bất_đồng với bạn cũng có_thể là nguyên_nhân khiến chàng xuôi_xị . Hãy bình_tĩnh và tìm cách tháo_gỡ mọi khúc_mắc một_cách nhẹ_nhàng và vui_vẻ .
Rượu và chất kích_thích
Một_chút rượu có_thể khiến chàng “ hùng_dũng ” như sư_tử . Thế_nhưng , nhiều rượu quá sẽ biến anh_ấy thành khúc gỗ , chỉ biết ngáy mà thôi .
Các chất kích_thích cũng có tác_dụng giống như_vậy . Chúng làm tăng phong_độ trong thời_gian mới sử_dụng , sau đó khiến chàng “ ì ra ” một_cách khốn_khổ .
Ám_ảnh bệnh_tật
Trái với vẻ bề_ngoài mạnh_mẽ , nam_giới rất hoang_tưởng về bệnh_tật của họ . Ví_dụ : Nếu có một cái mụn ở “ chỗ hiểm ” anh_ấy sẽ lo quáng_quàng , suy_diễn đủ thứ và “ tịt_ngòi ” cho_đến khi mụn lặn mới thôi . Theo Tiếp_Thị_Gia_Đình

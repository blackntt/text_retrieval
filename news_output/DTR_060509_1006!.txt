﻿ Việc_làm hè cho sinh_viên : Chưa vào mùa đã “ nóng ”
Hè mới bắt_đầu nhưng thị_trường việc_làm thêm cho sinh_viên đã “ nóng ” ra_trò . Tại TPHCM , nhiều trung_tâm dịch_vụ việc_làm đã đông_nghẹt sinh_viên đến đăng_ký tìm việc …
Nhiều nơi tổ_chức việc_làm hè
Trung_tâm dịch_vụ việc_làm ( DVVL ) Khu_chế_xuất - Khu_công_nghiệp TPHCM đang có nhu_cầu tuyển gấp 500 lao_động thời_vụ lắp_ráp điện_tử tại Khu_chế_xuất Linh_Trung . Làm_công việc giản_đơn này , các sinh_viên có_thể kiếm được 5.000 - 7.000 đồng/giờ .
Trung_tâm DVVL Vinhempich cần tuyển 150 nam , nữ nghiên_cứu thị_trường . Yêu_cầu cần_thiết là sinh_viên , năng_động , tự_tin . Thu_nhập được trả là 25.000 đồng/bảng câu_hỏi .
Còn tại Trung_tâm DVVL Sinh_viên - Học_sinh - Profec , trung_bình mỗi tuần tiếp_nhận khoảng 100 nhu_cầu tuyển lao_động thời_vụ với các công_việc phỏng_vấn viên , dán poster , phát tờ_rơi , gia_sư ... Mức lương được rao tuyển cho công_việc này dao_động trong khoảng từ 40.000 đồng - 100.000 đồng/ngày .
Nơi tổ_chức việc_làm thời_vụ dịp hè thu_hút đông sinh_viên đăng_ký nhất_là Trung_tâm Hỗ_trợ Sinh_viên TPHCM . Hiện_tại , trung_tâm đang có trên 200 nhu_cầu tuyển_dụng , công_việc chủ_yếu là giới_thiệu sản_phẩm , phát tờ_rơi , bán hàng , dạy kèm , thu_nhập từ 5.000 đồng - 12.000 đồng/giờ .
Tìm cơ_hội từ làm thêm
Tuy những việc_làm thêm chỉ mang tính thời_vụ ngắn ngày song những đòi_hỏi tuyển_dụng cũng khá gắt_gao . Chẳng_hạn như việc khuyến_mãi , nhà tuyển_dụng yêu_cầu ứng_viên phải có ngoại_hình , hoạt_bát , giao_tiếp tốt . Còn đối_với ứng_viên chuyên về nghiên_cứu thị_trường , ngoài việc siêng_năng , họ cần có tính năng_động , khả_năng nhận_xét về thị_trường trong tương_lai . Có công_việc lại đòi_hỏi phải giao_tiếp tiếng Anh tốt .
Công_việc làm thêm không_những giúp sinh_viên có thêm khoản thu_nhập để trang_trải học_hành mà_còn mở ra cho họ nhiều cơ_hội việc_làm sau khi ra trường . Trong mấy tháng hè làm thêm , họ có điều_kiện tích_lũy kinh_nghiệm , tạo quan_hệ , củng_cố kiến_thức và làm_quen với môi_trường xã_hội .
Bà Phùng_Thị_Thùy_Trang , phụ_trách giới_thiệu việc_làm Trung_tâm Hỗ_trợ sinh_viên TPHCM , cho_biết , nhiều sinh_viên khi làm thêm ở cơ_quan tỏ ra có năng_lực nên đã được doanh_nghiệp giữ lại và ký hợp_đồng dài_hạn .
Còn theo bà Ngô_Bích_Phượng , Trưởng_Phòng_Giới thiệu việc_làm Trung_tâm DVVL Vinhempich , làm thêm mùa_hè là dịp để các bạn trẻ chứng_tỏ mình trước các nhà tuyển_dụng .
Theo Huỳnh_Nga_Người_Lao_Động

﻿ Kết_quả Olympic hóa_học SV toàn_quốc : Các trường Hà_Nội thắng lớn
Đặc_biệt , trong số 40 giải_thưởng của Bộ GD - ĐT trao_tặng 40 sinh_viên đoạt giải cao thì bốn cá_nhân đoạt giải nhất đều đến từ thủ_đô Hà_Nội . Đó là Phạm_Đình_Trọng , Phạm_Chiến_Thắng ( ĐH Khoa_học_tự_nhiên Hà_Nội ) ; Phạm_Huy_Dũng ( ĐH Bách_khoa Hà_Nội ) ; Nguyễn_Văn_Thái ( Học_viện Quân_y - Hà_Nội ) . Hai giải nhất trong số tám giải toàn đoàn cũng thuộc về ĐH Khoa_học_tự_nhiên Hà_Nội và ĐH Bách_khoa Hà_Nội .
Cờ luân_lưu kỳ thi Olympic hóa_học sinh_viên năm 2007 đã được trao cho ĐH Dược_Hà_Nội .
ĐÌNH_TOÀN

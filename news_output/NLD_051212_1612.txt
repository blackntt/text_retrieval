﻿ Sân_khấu mãi_mãi là giấc_mơ
Cái tên Mạnh_Cường rất_đỗi quen_thuộc với khán_giả phim_truyện nhựa và phim_truyền_hình VN . Những vai_diễn của anh thường có tính_cách đa_dạng . Đối_với sân_khấu và điện_ảnh , với tư_cách một diễn_viên chuyên_nghiệp , Mạnh_Cường thường nói : “ Diễn kịch là kén nhất ”
. Hỏi : Phim nào ông thấy mình đóng thành_công nhất và gây ấn_tượng nhất trong lòng khán_giả ?
- NSƯT Mạnh_Cường : Đó là các phim : Không còn gì để nói , Giấc_mơ dài , Mặt_trận không tiếng súng .
. Có người cho rằng dù có 10 năm làm diễn_viên chính ở Đoàn_Kịch nói Tổng_cục Chính_trị , nhưng ông vẫn không nổi_tiếng bằng diễn_viên điện_ảnh Mạnh_Cường . Điều đó làm ông buồn không ?
- Thật_ra người_ta nhầm . Vì ở Đoàn_Kịch nói Tổng_cục Chính_trị , tôi là một Nghệ_sĩ Ưu_tú . Tôi đã tham_gia 15 vở diễn , trong đó có 10 vở là vai chính và đã nhận được nhiều huy_chương vàng , huy_chương bạc .
. Nhưng một_số diễn_viên kịch lại cho rằng theo điện_ảnh dễ phát_triển nghề_nghiệp hơn ?
- Tôi không cho như_vậy . Tôi luôn thấy mình có_giá hơn khi là một diễn_viên sân_khấu . Còn sự nổi_tiếng chẳng_qua sân_khấu ít người xem hơn điện_ảnh . Đối_với diễn_viên thì đều phải diễn_xuất , nhưng ở không_gian khác_nhau thì cách diễn khác_nhau ... Và mỗi nơi có cái khó của nó , không phải diễn_viên sân_khấu xuất_sắc nào cũng có_thể thành_đạt trước ống_kính , và ngược_lại , rất nhiều ngôi_sao điện_ảnh đã " tối ' ' đi trước ánh đèn sân_khấu .
. À , như_vậy ông vẫn cứ muốn mình là một diễn_viên kịch ?
- ( Cười ) . Chính sân_khấu giúp tôi thành_công để bước tới điện_ảnh . Và sân_khấu mãi_mãi là giấc_mơ của tôi .
. Xin chuyển sang đề_tài khác nhé . Một diễn_viên điện_ảnh điển_trai như ôngđã làm bao_nhiêu cô , nhiêu mợ xin ... “ ' chết ” . .
- ( Lại cười ) . Đó là sự ngưỡng_mộ và chia_sẻ . Tôi rất vui và cám_ơn những tình_cảm của khán_giả , đặc_biệt là khán_giả phái_đẹp .
. Bà_xã có cảm_thông không ? - Tôi luôn nghĩ đến cuộc_sống gia_đình . Vui_đùa , bếp_núc , luôn ăn " cơm " nhà với vợ ( cười ) , vì đây là mảnh đất êm_ả nhất , là nơi duy_nhất tôi không phải diễn .
Theo Thể_Thao

﻿ Thêm 10 trường ĐH tham_gia kiểm_định chất_lượng
Tham_dự khóa tập_huấn này có các chuyên_gia đến từ bảy quốc_gia như Úc , Ấn Độ , Malaysia , Indonesia … và hơn 60 cán_bộ quản_lý các trường ĐH , chuyên_viên trong lĩnh_vực kiểm_định chất_lượng giáo_dục …
Theo ông Nghĩa , 10 trường ĐH đầu_tiên thực_hiện thí_điểm kiểm_định chất_lượng bắt_đầu_từ năm 2005 đã hoàn_tất khâu tự đánh_giá và bắt_đầu chuyển sang giai_đoạn đánh_giá ngoài và dự_kiến sẽ được công_nhận kết_quả kiểm_định trong năm 2006 .
Hiện_Bộ GD - ĐT đã lựa_chọn được 10 trường ĐH tiếp_theo tự_nguyện tham_gia kiểm_định chất_lượng , trong đó có hai trường ĐH dân_lập đầu_tiên tham_gia .
Đó là các trường : ĐH Nông_nghiệp 1 , ĐH Sư_phạm Hà_Nội , ĐH dân_lập Hải_Phòng , ĐH Ngoại_thương , ĐH Thương_mại , ĐH Sư_phạm ( thuộc ĐH Huế ) , ĐH Thủy_sản Nha_Trang , ĐH dân_lập Văn_Lang , ĐH Sư_phạm TP.HCM và ĐH Nông_Lâm TP.HCM .
Dự_kiến các trường cũng sẽ hoàn_tất hai khâu tự đánh_giá và đánh_giá ngoài vào tháng 8 và tháng 9 và được công_nhận kết_quả kiểm_định trong năm 2006 .
Ông Nghĩa cho_biết , với 20 trường ĐH được công_nhận kiểm_định chất_lượng , Bộ GD - ĐT hoàn_thành mục_tiêu Chính_phủ yêu_cầu có 20% trường ĐH được kiểm_định trong năm 2006 .
Trong giai_đoạn từ 2006 đến 2010 , Bộ GD - ĐT sẽ tiến_hành kiểm_định chất_lượng tất_cả các trường ĐH còn lại và bắt_đầu kiểm_định chất_lượng một_số chương_trình đào_tạo . Đồng_thời xây_dựng qui_chế , bắt_đầu triển_khai kiểm_định cả các trường CĐ , THCN , các khóa đào_tạo giáo_viên .
THANH_HÀ

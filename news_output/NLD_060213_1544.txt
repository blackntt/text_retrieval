﻿ Curbishley thúc_giục FA sớm lựa_chọn HLV
HLV của Charlton_Athletic ông Alan_Curbishley và HLV của Manchester_City ông Stuart_Pearce HLV của Charlton , ông Alan_Curbishley đã gọi điện cho LĐBĐ Anh ( FA ) để yêu_cầu sớm kết_thúc kế_hoạch tìm_kiếm để đưa ra quyết_định cuối_cùng ai sẽ là HLV đội_tuyển Anh sau World_Cup 2006 .
Curbishley được xem là một trong số những ứng_cử_viên sáng_giá người Anh thay_thế ông Sven-Goran Eriksson sau World_Cup mùa_hè tới .
Bên_cạnh đó , còn có HLV của Bolton ông Sam_Allardyce , ông Stuart_Pearce của Manchester_City và ông Steve_Mc Claren của Middlesbrough .
" Tôi thích_thú khi bỏ ra một bảng để mua tờ báo có tin tên tôi trên đó , nhưng đó không phải là giải_trí " , ông Curbishley nói .
" Có_lẽ những gì FA đang làm là chỉ mới thống_kê danh sach tên chúng_tôi , bởi họ biết lúc này chúng_tôi vẫn đang làm_việc . Do_đó , họ nói quyết_định cuối_cùng sẽ được đưa ra_vào tháng 5 và mọi người có_thể chờ_đợi " .
" Tuy_nhiên , tôi nghĩ FA cần đưa ra sớm quyết_định cuối_cùng . Họ cần thông_báo . Có nhiều người như chúng_tôi sẽ được phỏng_vấn hoặc không , hoặc sẽ chờ đến tháng 5 ? " .
Pearce , một ứng_cử_viên khác lại tỏ ra thông_cảm : " Tôi nghi_ngờ lúc này , họ ( FA ) không tiện để để thông_qua điều đó " , anh nói trên Sky_Sports . " Mọi chuyện phải ngã_ngũ 1 tháng trước khi World_Cup bắt_đầu - thông_báo vào ngày 10-5 - có_thể là 1 ý_kiến và là quyết_định của họ . Hãy hy_vọng và chờ_đợi , họ sẽ ra quyết_định đúng_đắn " .
Theo Tiền_Phong

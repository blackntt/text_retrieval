﻿ Hãy cứu lấy họ !
Tôi đang làm_việc ở một trong số những công_ty không muốn ký HĐLĐ không xác_định thời_hạn với người lao_động . Với quy_định đã nêu trên của Bộ_luật lao_động , một_số doanh_nghiệp chỉ sử_dụng lao_động trong thời_hạn của hai HĐLĐ xác_định thời_hạn , sau đó cho người lao_động nghỉ và tuyển người khác thay_thế .
Đối_sách trên của các doanh_nghiệp có tác_động không lớn đối_với lao_động “ áo trắng ” , vì lao_động trí_thức có_thể tích_lũy thu_nhập để dễ_dàng trải qua thời_gian tìm việc mới . Tuy_nhiên , đối_với lao_động “ áo xanh ” thì thật_là tàn_nhẫn .
Ngày 31-12-2004 là ngày hết hạn HĐLĐ xác_định thời_hạn thứ_hai của hơn 50 công_nhân ở công_ty nơi tôi đang làm_việc . Sau ngày này , 50 lao_động hầu_hết đến từ các tỉnh miền Trung và miền Bắc phải nghỉ_việc .
Do nghỉ trước thời_điểm công_bố thưởng tết nên họ chắc_chắn sẽ mất luôn khoản thu_nhập đó . Họ phải trải qua gần hai tháng mới tìm được việc_làm mới và hầu_như không đủ tiền để về quê ăn tết .
Có_thể đây là lần đầu_tiên doanh_nghiệp áp_dụng " đối_sách " quy_định nêu trên và số công_nhân phải nghỉ_việc ở công_ty tôi là hơn 50 . Nhưng đến ngày 30-6-2005 , có khoảng 100 công_nhân nữa cũng sẽ rơi vào tình_trạng tương_tự . Không biết rồi họ sẽ ra_sao .
Tất_nhiên việc chủ doanh_nghiệp cho nghỉ_việc khi hết hạn HĐLĐ là đúng luật , song chúng_tôi hi_vọng những nhà làm_luật có cách cứu lấy việc_làm cho những người lao_động này .
khotangluutru @ ...

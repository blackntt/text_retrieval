﻿ Đảng phải làm cho dân giàu , nước mạnh
& gt ; & gt ; Tiêu_chuẩn đảng_viên phải đi cùng thời_đại & gt ; & gt ; Thời_cơ vàng : Vận_hội mới & gt ; & gt ; Thời_cơ vàng : Vượt qua cái bóng của mình & gt ; & gt ; Thời_cơ vàng và hiểm_hoạ đen & gt ; & gt ; Hướng đến chân_lý sẽ vượt qua cái bóng của mình & gt ; & gt ; Người tài chưa được trọng_dụng hay bị đố_kỵ ?
Tôi là một người_dân bình_thường , trình_độ không cao có_thể không hiểu hết các ý_nghĩa sâu_xa mà tác_giả bài viết đã đưa ra . Nhưng cách lý_luận của tác_giả làm tôi cảm_thấy rằng tác_giả lấy các lý_luận lịch_sử ra để bảo_vệ và trói_buộc quan_điểm của mình là không hợp_lý với thời_đại .
Bởi lẽ , thời_đại ngày_càng phát_triển và chưa chắc một quan_điểm đúng trong lịch_sử lại có_thể đúng trong thời_đại mới . Ngay cả những định_luật toán_học vững_chắc trong lịch_sử cũng có_thể bị phản_chứng trong tương_lai , chưa nói chi là quan_điểm trong lịch_sử chưa chắc đã đúng .
Tôi rất tâm_đắc ý_kiến của tác_giả là hãy để tranh_luận các vấn_đề và làm_sao cho “ dân giàu , nước mạnh , xã_hội công_bằng , văn_minh ” là đủ .
PHẠM_TUẤN_KIỆT

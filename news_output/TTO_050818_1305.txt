﻿ Liam C . Kelley : Tìm_hiểu VN qua những ... bài_thơ đi sứ
- Vì những bài_thơ ấy rất khác với những điều mà các học_giả Mỹ vẫn nghĩ .
Ông có_thể nói cụ_thể hơn ?
- Ở Mỹ , các học_giả nghiên_cứu về VN nhiều nhất vào những thập_kỷ 60 và 70 của thế_kỷ trước , tức_là trong giai_đoạn chiến_tranh .
Những người Mỹ đầu_tiên nghiên_cứu lịch_sử VN vẫn quan_niệm người VN đi theo tinh_thần dân_tộc chủ_nghĩa , tức_là giữ văn_hóa riêng của mình , bởi lẽ họ thường_xuyên phải chống lại nạn ngoại_xâm . Và tư_tưởng này là yếu_tố rất quan_trọng giúp người VN đoàn_kết lại để chống Mỹ .
Xuất_phát từ quan_niệm đó , sách và bài viết của các học_giả Mỹ thường hay xoay quanh chủ_đề người Việt chống lại ảnh_hưởng của nền văn_hóa Trung_Quốc .
Ban_đầu tôi cũng nghĩ như_vậy . Vì_thế khi phát_hiện ra những tài_liệu gọi_là thơ đi sứ , tôi hình_dung nếu người VN mang tư_tưởng ấy , ắt họ phải biểu_hiện ra trong những bài_thơ mà họ làm khi đi sứ sang Trung_Quốc . Nhưng thực_tế lại không phải như_vậy .
Vậy ông thấy những bài_thơ đó như_thế_nào ?
- Tôi thực_sự ngạc_nhiên khi thấy trong những bài_thơ này dường_như không có một thái_độ kỳ_thị hay phản_ứng theo lối dân_tộc chủ_nghĩa như các học_giả Mỹ vẫn nghĩ .
Ngược_lại , trong các bài_thơ đó , người Việt đi sứ thậm_chí coi văn_hóa Trung_Quốc cũng là văn_hóa của VN .
Các vị sứ_giả VN , thường là các nhà Nho , cũng đọc Kinh_Thi , mặc phẩm_phục giống Trung_Quốc . Họ đến Bắc_Kinh , gặp những người Hàn_Quốc , Nhật_Bản và nói rằng tất_cả đều là người một nhà , ít thấy nói đến văn_hóa riêng của mình .
Tuy những người đi sứ chỉ là một nhóm người , không_thể đại_diện cho toàn_bộ dân_tộc VN , song trong một chừng_mực nào đấy , những bài_thơ đi sứ đã cho thấy một thái_độ rất khác của người Việt đối_với văn_hóa Trung_Quốc , ngược hoàn_toàn với quan_niệm của người Mỹ trước_đây .
Do_vậy , tôi quyết_định thử nghiên_cứu VN bắt_đầu_từ những bài_thơ đó với hy_vọng giúp giới học_giả Mỹ thay_đổi phần_nào cách nhìn_nhận trong vấn_đề này .
Ông vừa có một cuộc thuyết_trình tại Viện_Hán_Nôm ( Hà_Nội ) về " quan_niệm địa_lý Việt_Nam thời phong_kiến trong đó chính ông đề_cập đến bài_thơ Nam quốc sơn_hà , và ông rất chú_trọng đến chữ " thiên thư " trong bài_thơ này . Theo ông đấy không phải là lòng tự_tôn dân_tộc sao ?
- Có chứ , có_điều tôi không thấy những bài_thơ mạnh_mẽ như_vậy trong các tập thơ đi sứ . Nhưng tôi lại thấy lòng tự_tôn dân_tộc của người VN trong các bản dịch thơ đi sứ đó . Trong buổi thuyết_trình đó , tôi đã tiếp_thu được rất nhiều điều bổ_ích từ phía những thính_giả . Họ đã bổ_sung cho tôi rất nhiều kiến_thức để tôi tiếp_tục nghiên_cứu vấn_đề mà tôi đề_cập .
Ông nghiên_cứu văn_hóa VN bằng cả chữ Hán và chữ Quốc_ngữ ?
- Trước_kia , tôi quan_tâm đến Trung_Quốc và có kế_hoạch nghiên_cứu về những người Trung_Quốc ở Đông - Nam Á . Nhưng_Trung_Quốc thì quá rộng_lớn .
Sau đó tôi được biết rằng , trước thế_kỷ 20 , người VN dùng chữ Hán . Có người nói với tôi là ở VN có rất nhiều tài_liệu chữ Hán vì VN chịu nhiều ảnh_hưởng của văn_hóa Trung_Hoa , nhưng lại là một đất_nước rất khác_biệt so vái các nước khác ở châu Á . Tôi nghĩ rằng đó là một điều rất thú_vị , vậy_thì tại_sao lại không tìm_hiểu nó . Nên tôi bắt_đầu học tiếng Việt cách đây sáu năm ở Hawaii .
Còn tên cuốn sách , Đi qua những cột đồng , có nghĩa_là thế_nào ?
- Người VN và người Trung_Quốc đều biết đến truyền_thuyết về việc Mã_Viện dựng một cây cột đồng sau đó đọc thần_chú để lập ra một biên_giới . Sau đó cả người VN và Trung_Quốc đã nói rất nhiều về chiếc cột đồng này nhưng không biết nó nằm ở đâu cả . Do_đó tôi xem nó như biểu_tượng của một biên_giới , và người đi sứ phải vượt qua . Đấy chỉ là một_cách ví_von .
Xin cám_ơn ông !
Theo Thể thao và Văn_hóa

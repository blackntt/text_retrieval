﻿ Trình_diễn thời_trang cưới Ấn Độ và Pakistan
Triển_lãm với nội_dung tương_tự cũng sẽ được tổ_chức tại Hyderabad , Bangalore , Bangkok và London .
Masarrat_Misbah , chuyên_gia làm_đẹp nổi_tiếng của Pakistan cho_biết : " Có khá nhiều điểm khác_biệt trong trang_phục cưới cô_dâu của các nhà_thiết_kế thời_trang Ấn Độ và Pakistan . Và đây chính là cơ_hội cho các nhà_thiết_kế của 2 nước học_tập lẫn nhau . Sự_kiện thời_trang này còn mang ý_nghĩa thắt chặt tình hữu_nghị giữa hai quốc_gia " .
Trong con mắt nhà_nghề của các chuyên_gia , thông_qua chương_trình thời_trang này , các nhà_thiết_kế Pakistan cũng muốn nhắm vào thị_trường trang_phục cưới rộng_lớn của các cô_dâu Ấn Độ .
Dưới đây hình_ảnh của một_số trang_phục được giới_thiệu tại chương_trình .
VIỆT_ANH ( Theo Asian_News )

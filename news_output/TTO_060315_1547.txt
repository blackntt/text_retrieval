﻿ Tôi đi xăm
Thấy tôi trợn mắt nhìn mãi , H phẩy_tay cười : “ Bà đúng là nhà_quê , bây_giờ đi xăm là mốt của cánh con_gái Hà_thành đấy . Muốn thử không , tôi đưa đi ! ” .
Xăm ngực , xăm cổ … mới sành_điệu
H sành_điệu giảng_giải : “ Mình xăm hình này từ hồi mới thịnh , xăm ở một quán “ cò_con ” nên không được đẹp lắm còn bây_giờ có nhiều quán xăm có nghề , thích hình gì cũng có , thích màu nào cũng OK ! ” .
Tôi theo chân H đến một quán cơm bên đường Mã_Mây . Chưa kịp ngồi xuống H đã hất mặt ra dấu . Thì_ra ở đây hộp đựng đũa nào cũng dán kín 4 mặt quảng_cáo xăm hình - TATTOO số 5 Lương_Ngọc_Quyến .
Không tìm được cửa_hàng xăm , tôi gọi vào số máy 0983166 ... ghi trên tờ quảng_cáo . Một giọng đàn_ông tự_xưng là Huần trả_lời khá cảnh_giác : “ Em muốn xăm hả ? Gọi theo số máy 0983028 ... hoặc số 0905238 ... gặp anh Khánh nhé . Số 5 Lương_Ngọc_Quyến à ? Địa_chỉ “ ma ” đấy ! ” .
Quảng_cáo xăm hình tại một quán cơm trên đường Mã_Mây_Tôi gọi vào hai số máy được chỉ_dẫn , lắng_nghe cái điệp_khúc quen_thuộc : “ Thuê_bao quý_khách nằm ngoài vùng phủ_sóng ... ” . Đành gọi lại cho người đàn_ông tên Huần . Lần này giọng đầu dây bên kia có_vẻ cởi_mở hơn : “ Không gọi được à ? Chắc thằng này lại đổi số rồi . Thôi , muốn xăm cứ đến thẳng số 17 Cửa_Nam nhé ” .
Tôi cùng cô bạn diện một bộ_cánh thật mốt vòng xe đến địa_điểm được chỉ_dẫn . Một ngôi nhà nhỏ nằm giữa con phố ồn áo náo_nhiệt .
Nhìn từ ngoài vào , không ai có_thể nghĩ đây là địa_điểm xăm cả nếu_không chịu_khó ngẩng đầu cao hết cỡ để thấy cái biển quảng_cáo nằm chót_vót trên cao và bị cây_cối che_khuất tầm nhìn .
Cô nhân_viên trẻ_măng với vẻ mặt xởi_lởi : “ Ở đây xăm bằng công_nghệ mới , không đau đâu ” . Tôi hỏi : “ Có đảm_bảo không chị ? ” . “ Em yên_tâm , mỗi lần xăm đều lấy kim xăm mới chỉ dùng một lần ! ” .
“ Vậy xăm hình con bọ_cạp ở mu bàn_tay bao_nhiêu ? ” . “ Khoảng 700.000 đồng đến một_triệu , còn tuỳ vào độ lớn_nhỏ của hình xăm ” . “ Nếu sau_này muốn xoá thì_phải mất bao_nhiêu tiền ? ” .
“ Khoảng từ 1,5 - 2 triệu ” .
Cũng theo cô nhân_viên thì ngoài xăm vĩnh_viễn ra ở đây còn có công_nghệ phun hình xăm màu đang rất thịnh với lợi_thế không đau_đớn , lưu lại trên da khoảng 1 tuần lại dễ thay_đổi khiến dịch_vụ này rộn lên như một trào_lưu . Vì biết không gây đau_đớn , nhanh phai và cũng vì tò_mò tôi lật tập catalogue , chọn đại một hình hoa_văn , vén tay_áo lên .
Không giống như trong TP.HCM , hình_mẫu được in trên tờ giấy_than , sau đó in lên da lưu lại và người_ta tỉ_mẩn đưa cây_bút vẽ theo từng nét từ đơn_giản đến phức_tạp . Ở đây hình_mẫu được dính lên da và người_ta lấy dụng_cụ bằng inốc như súng phun nước của trẻ_con rồi bắt_đầu phun cả mực và gió vào hình để mau khô .
Vừa xịt cô nhân_viên vừa giảng_giải : “ Mực này được nhập từ nước_ngoài về , một lọ nhỏ cỡ chai thuốc nhỏ mắt cũng khoảng 2-3 triệu đồng còn hình_mẫu cũng được đặt mua từ bên đó và tải qua mạng về đấy ” .
Chỉ sau chừng 10 phút , hình xăm hoa_văn kích_thước 3cmx9cm trên cổ_tay tôi đã hoàn_thành , với giá 100.000 đồng . Cũng với những hình này nếu hoạ_tiết phức_tạp và kích_thước lớn hơn giá_cả có_thể lên tới trên 300.000 đồng .
Theo lời cô nhân_viên , ngày nào cũng có khoảng chục lượt khách tới đây để xăm mình hoặc phun hình gồm rất nhiều thành_phần nhưng đa_số hiện_nay là nữ ở độ tuổi ômai và thanh_thiếu_niên : “ Em cứ kỳ_thị chứ con_gái xăm bây_giờ là chuyện ... nhỏ . Các hình xăm nho_nhỏ ở trên mu bàn_tay , trên ngực , cổ , cạp_quần ... Cứ quần_bò cạp trễ , áo hai dây vào , “ chiến ” phải_biết ! ” .
Thú chơi thời_thượng
Xăm giờ đã trở_thành một thú chơi thời_thượng . Nó len_lỏi đến mọi ngõ_ngách và mọi thành_phần . Không phải chỉ_giới “ anh_chị ” xăm để phân_biệt đẳng_cấp ngôi_thứ , cũng không phải những tay_chơi xăm để khẳng_định mình mà ngay cả con_gái cũng đi xăm .
Không xăm ở lưng , tay hay ngực như cánh đàn_ông , con_gái “ độc_chiêu ” hơn , tức_là xăm ở những nơi nhạy_cảm mà chỉ cần nhìn sơ cũng thấy .
H hiện đang là thư_ký cho một Cty TNHH ở Hà_Nội cười : “ Mất gần 1 triệu và hơn 1 tuần đau_nhức , khó_chịu mới có được hình con nhện “ độc ” thế_này ... Sợ gì người_ta nói_ra_nói_vào , chỉ cần mình thích là được ” .
Với V , cô bé 15 tuổi đang học trường PTTH Phương_Nam đang sở_hữu một hình hoa_văn sau cạp_quần rất hồn_nhiên : “ Nhóm em có 3 đứa , đứa nào cũng xăm cả , chẳng_nhẽ em lại thua chúng_nó . Thế_này mới sành_điệu chị ạ ! ” . “ Em lấy tiền đâu để đi xăm ? ” . “ Hình xăm này hơi bị công_phu , mất cả triệu đồng . Em lấy tiền lì_xì và cả tiền mẹ đưa đóng học mới đủ ” .
Sành_điệu hay dại_dột ?
Không còn cái kiểu xăm thô_sơ mà đám con_trai vẫn tự xăm cho nhau bằng cây kim hoặc dao_lam với một_ít mực Tàu , con_gái đi xăm công_phu hơn và tất_nhiên là tốn_kém hơn . Một hình xăm nhỏ 3cmx4cm phải mất cả tiếng chịu_đựng đau_đớn với số tiền lên tới 1-2 triệu đồng .
H dẫn tôi đi vòng_vèo xe qua một_số địa_điểm xăm lậu ( tức_là những địa_điểm xăm không có biển ) nằm rải_rác trong những con hẻm nhỏ ở các khu_tập_thể Kim_Liên , khu_tập_thể Trương_Định hay dưới chân cầu Long_Biên ... nhưng đa_số các quầy này đều nhận khách quen cho “ an_toàn ” .
H có_vẻ hiểu_biết : “ Ở những khu_vực này xăm rẻ hơn một_nửa so với các quán trưng biển nhưng họ xăm cũng có nghề lắm . Chỉ_tội muốn xăm phải tự đem kim xăm đến ” .
“ Sao phải thế ? ”
“ Không an_toàn . Hồi trước mình xăm của “ lão ” Q trong xóm , giờ cả hai vợ_chồng đều bị bắt vì buôn_lậu ma_tuý rồi . Chẳng biết “ lão ” có bị bệnh gì không ! ” - H rùng_mình nói .
Thế mới thấy được_cái nguy_hại của thú chơi sành_điệu này . Với một cây kim nho_nhỏ nếu_không chú_ý cảnh_giác nó sẽ dễ_dàng trở_thành công_cụ làm lây_lan những căn_bệnh truyền_nhiễm nguy_hiểm .
Trường_hợp của T còn đáng trách hơn . Dù đã là sinh_viên nhưng T không có những kỹ_năng tối_thiểu để phòng_tránh các căn_bệnh lây_nhiễm qua máu .
Xăm ở một địa_điểm lậu lại không biết cần phải mang theo kim xăm nên T dùng chung với người xăm trước . Chỉ đến khi tôi hỏi , cô mới tá_hoả . Sau một đêm mất_ngủ T phải bỏ một khoản tiền không nhỏ vào bệnh_viện thử máu .
Còn V , khi nhà_trường gửi thư về cho phụ_huynh nhắc_nhở đóng tiền học_phí . Sau một hồi nói_dối vòng_vo , V đành thú_thật mọi chuyện . Mẹ V suýt té xỉu khi nhìn thấy hình xăm đen_ngòm nơi cạp_quần của con_gái .
Một trận đòn thừa_sống_thiếu_chết và chính mẹ V dắt tay cô con_gái đến bệnh_viện nhờ xoá vết xăm . Vết sẹo để lại sau vụ xoá hình xăm có_lẽ sẽ luôn ám_ảnh V .
Theo Tiền_Phong

﻿ Làm mứt khoai_lang
- Chọn khoai_lang bí , ruột vàng , gọt vỏ . - Khoai có_thể cắt thành miếng dài vuông cạnh , cỡ ngón_út hoặc cắt xéo ngang củ khoai thành lát dày khoảng lcm , rồi dùng ống xắn tròn , xắn khoét thành nhiều lỗ giữa lát khoai .
Nguyên_liệu chuẩn cho 1 kg khoai
- Pha mỗi lít nước + 20g vôi trắng , để qua đêm cho lắng trong , lấy phần nước lắng trong để ngâm khoai , nước phải ngập khoai hoàn_toàn , ngâm qua 3 giờ , vớt ra xả lại nhiều lần nước_lạnh cho thật sạch . Nước vôi có tác_dụng làm giòn khoai .
- Cân lại khoai , cứ một ký khoai + 800g đường_cát trắng .
- Sên mút : Để sên mứt khoai cho đẹp , nếu cắt lát lớn , phải chăm từng miếng khoai một . Cho khoai + đường + 3 muỗng súp nước_lạnh vào chảo , trộn nhẹ_tay , để lửa thật nhỏ , khi đường tan hết và sôi nhẹ , gỡ tách liên_tục đừng cho khoai dính nhau . Để khoai thấm đường cho_đến khi thấy đường cạn gần hết , cho vào 1 muỗng cà_phê thạch_cao phi , khi thấy đường bắt_đầu đặc lại gắp từng miếng khoai trải thẳng_thớm lên vỉ gác ngang chảo , để nguội miếng khoai . Nếu làm mứt lát lớn có_thể gói mỗi miếng trong một miếng giấy nylon trắng hoặc cho vào lọ thủy_tinh sạch , đậy kín .
Theo Phụ nữ TPHCM

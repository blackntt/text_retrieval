﻿ Một gia_đình làm lễ giỗ liệt_sĩ Đặng_Thùy_Trâm
Gia_đình ông Dương_Hồng_Thanh ( ngụ ở xã Hậu_Mỹ_Bắc B , huyện Cái Bè , Tiền_Giang ) ngày 22/6 vừa_qua đã tổ_chức lễ giỗ liệt_sĩ - bác_sĩ Đặng_Thùy_Trâm đúng vào ngày cách đây 36 năm nữ bác_sĩ Đặng_Thùy_Trâm hi_sinh tại chiến_trường Đức_Phổ .
Lễ giỗ của liệt_sĩ Trâm được tổ_chức rất trang_trọng . Hơn 200 thầy_cô_giáo , đoàn_viên thanh_niên địa_phương , chính_quyền địa_phương và ông Phạm_Thanh_Nhàn ( em rể của bác_sĩ Đặng_Thùy_Trâm ) hay tin đã đến thắp hương cho chị .
Ông Thanh cho_biết : cách đây hơn một năm , khi đọc quyển Nhật_ký Đặng_Thùy_Trâm , ông Thanh và những người trong gia_đình đã khóc rất nhiều vì có cảm_giác như người_thân trở_về .
Ngưỡng_mộ sự hi_sinh cao_cả của người nữ bác_sĩ anh_hùng , đặc_biệt do hoàn_cảnh gia_đình ông Thanh cũng gần giống gia_đình của chị nên ông quyết_định lập bàn_thờ hương_khói và làm lễ giỗ cho chị như một người_thân trong gia_đình . Theo V.TR Báo_Tuổi trẻ

﻿ NASA phóng thành_công tàu_vũ_trụ Discovery
Cơ_quan Hàng_không Vũ_trụ Mỹ ( NASA ) hôm_qua 4/7 đã phóng thành_công tàu_vũ_trụ con_thoi Discovery lên trạm không_gian quốc_tế ISS , sau khi phải hoãn lại vài ngày vì lý_do thời_tiết và những lo_lắng về kỹ_thuật .
Tàu_Discovery cùng 7 phi_hành_gia được phóng lên lúc 2h38 chiều giờ_địa_phương ( khoảng 1h38 sáng giờ Việt_Nam ) với nhiệm_vụ cung_cấp lương_thực , thiết_bị và các vật_dụng khác cho các nhà_nghiên_cứu trên trạm ISS . Dự_tính , tàu Discovery sẽ ở lại trạm ISS khoảng 12 ngày .
Lần phóng thử này được tiến_hành trong bầu_không_khí lo_âu vì trước đó người_ta đã phát_hiện lỗi kỹ_thuật ở bộ_phận chứa nhiên_liệu . Tuy_nhiên các quan_chức NASA khẳng_định lỗi này không gây nguy_hiểm cho các nhà du_hành_vũ_trụ .
Theo kế_hoạch ban_đầu , NASA dự_tính phóng tàu_vũ_trụ Discovery vào ngày 1/7 bất_chấp nhiều chuyên_gia kỹ_thuật lo_ngại bộ_phận chứa nhiên_liệu của con tàu này có vấn_đề . Việc phóng tàu Discovery sau đó được hoãn lại tới ngày 4/7 do thời_tiết xấu và NASA muốn kiểm_tra lại những trục_trặc ở bộ_phận nhiên_liệu .
Đây là lần đầu_tiên NASA phóng tàu_vũ_trụ vào ngày Quốc_khánh Mỹ và là lần phóng thứ 2 kể từ sau thảm_hoạ tàu Columbia năm 2003 , khiến toàn_bộ 7 phi_hành_gia thiệt_mạng .
Tháng 7/2005 , NASA mới quyết_định phóng tàu_con_thoi Discovery trong khi vẫn bị ám_ảnh bởi vụ nổ tàu Columbia . Rất may tàu Discovery đã trở_về trái_đất an_toàn , dù cũng gặp một_số trục_trặc kỹ_thuật .
Chi_Mai_Theo AP

﻿ Bình_Nhưỡng vừa phóng tiếp quả tên_lửa thứ 7
Trong khi cơn giận_dữ về vụ thử 6 quả tên_lửa sáng nay vẫn chưa nguôi_ngoai , thì hãng tin Kyodo trích nguồn của chính_phủ Nhật_Bản cho_hay , CHDCND Triều_Tiên vừa phóng tiếp một quả tên_lửa thứ_bảy .
Theo hãng tin này , Bình_Nhưỡng bắn thử quả tên_lửa thứ 7 vào_khoảng 4h chiều , giờ Hà_Nội , và nó đã rơi xuống 6 phút sau đó .
Các quan_chức quốc_phòng Mỹ hiện chưa_thể khẳng_định được thông_tin trên . Cũng chưa rõ quả tên_lửa thứ 7 này thuộc loại nào và nó rơi xuống đâu .
Trước đó , qua truyền_hình của Nhật , một quan_chức Bộ ngoại_giao CHDCND Triều_Tiên lên_tiếng bảo_vệ quyền thử tên_lửa của nước này và cho rằng vụ thử là vấn_đề chủ_quyền quốc_gia của riêng Bình_Nhưỡng .
Sau hàng_loạt các vụ thử tên_lửa của Bình_Nhưỡng , sáng_sớm nay thị_trường_chứng_khoán Nhật đã bị sụt_giảm . Đồng đô_la Mỹ tăng so với đồng yên Nhật .
Đoạn tin trên do kênh_truyền_hình TBS của Nhật thực_hiện và được kênh YTN của Hàn_Quốc phát_sóng . Đoạn tin cho thấy người đưa ra phát_ngôn trên là ông Ri_Pyong_Dok , nhà_nghiên_cứu về Nhật_Bản của Bộ ngoại_giao CHDCND Triều_Tiên . Ông cho rằng không ai có quyền can_thiệp vào chương_trình hạt_nhân của Bình_Nhưỡng .
“ Vụ thử tên_lửa là một vấn_đề hoàn_toàn nằm trong phạm_vi chủ_quyền của chúng_tôi . Không ai có quyền tranh_cãi về nó ” , ông khẳng_định . “ Khi tiến_hành vụ thử tên_lửa này , chúng_tôi không vi_phạm bất_kỳ hiệp_ước nào . ”
Đây là khẳng_định đầu_tiên của Bình_Nhưỡng về vụ thử sau khi có tin nước này đã phóng ít_nhất 6 quả tên_lửa , trong đó có một quả Taepodong - 2 tầm xa vào sáng_sớm ngày hôm_nay . Tuy_nhiên , theo các quan_chức Mỹ , quả tên_lửa tầm xa đã bị rơi ngay sau khi phóng , còn những quả khác đều rơi xuống biển Nhật_Bản .
Trong khi đó , Tổng_tham_mưu_trưởng quân_đội Nga lại tuyên_bố , Bình_Nhưỡng đã phóng tổng_cộng 10 tên_lửa . " Hệ_thống kiểm_soát của chúng_tôi có_thể xác_định được số tên_lửa đã được bắn ra " , tướng Yuri_Baluevsky nói với các phóng_viên tại thành_phố Chita , gần biên_giới với Mông_Cổ .
" 10 quả tên_lửa đã được bắn . Theo một bộ số_liệu , chúng thuộc các lớp khác_nhau . Nhưng theo một bộ số_liệu khác , chúng đều là tên_lửa liên lục_địa . Tôi chỉ có_thể xác_định chúng thuộc lớp nào sau khi có thêm số_liệu kỹ_thuật " .
Trang_Thu_Theo AP , Reuters

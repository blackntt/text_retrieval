﻿ Real_Madrid " hất_cẳng " Valencia
( Dân_trí ) - Real_Madrid đã chiếm lại ngôi nhì bảng khi tận_dụng thành_công cú ngã ngựa của Valencia đêm_hôm trước bằng chiến_thắng 3-2 trước chủ nhà Racing_Santander , đội bóng đang vật_lộn ở tốp cuối bảng xếp_hạng .
Sau khi Valencia thất_thủ 1-2 tại Son_Moix của Mallorca , Real_Madrid có cơ_hội để giành vị_trí thứ 2 nếu có 3 điểm trong trận đấu với Racing . Và dù thi_đấu không quá xuất_sắc , đội bóng Hoàng_gia vẫn đạt được mục_tiêu với sự toả sáng của Roberto_Carlos và Robinho .
Thế_trận diễn ra tương_đối cân_bằng và không hấp_dẫn cho_đến khi Real vượt lên bằng pha sút penalty thành_công của hậu_vệ Carlos ở phút 33 .
Công đầu_tiên thuộc về Robinho khi tiền_vệ này lừa bóng kỹ_thuật và buộc hậu_vệ của đội chủ nhà phải phạm lỗi trong vòng cấm .
Thiếu_vắng cả Ronaldo và Julio_Baptista trên hàng công , nhưng “ kền_kền trắng ” lại có sự toả sáng của các tiền_vệ và ... hậu_vệ .
Các cầu_thủ Real chia_sẻ niềm_vui ghi_bàn với Carlos .
Phút 60 , pha đột_phá dũng_mãnh của Roberto_Carlos khiến thủ_thành Dudu_Aouate phải lao ra cản_phá . Bóng đến chân Raul và tiền_đạo này đã nhanh chân chuyền cho Soldado ở vị_trí thuận_lợi dễ_dàng nâng tỷ_số lên 2 - 0 .
Chỉ 10 phút sau , đến lượt Robinho lập_công . Cầu_thủ người Brazil đã có pha xoay người sút bóng khá đẹp_mắt , hạ gục thủ_môn Aouate . Tuy_nhiên , trước đó phải kể đến sự nỗ_lực của đội phó Guti với pha dắt bóng kỹ_thuật qua 2 cầu_thủ đội chủ nhà .
Trong những phút còn lại , dù rất cố_gắng nhưng Racing cũng chỉ kịp gỡ lại 2 bàn sau những tình_huống khá may_mắn . Matabuena dứt_điểm ở cự_ly hẹp trong tình_huống lộn_xộn ở phút 76 .
Dù rất cố_gắng nhưng Racing vẫn phải chấp_nhận thất_bại .
Ít phút trước khi hết giờ , lại chính là Matabuena rút ngắn tỷ_số xuống còn 2-3 sau cú đá phạt trực_tiếp bật ra của đồng_đội . Tuy_nhiên , những nỗ_lực này đã quá muộn và Racing buộc phải chấp_nhận thất_bại .
Trận thua này khiến đội chủ nhà vẫn dậm chân tại_chỗ ở vị_trí thứ 17 với 37 điểm và chỉ còn hơn nhóm phải xuống hạng vẻn_vẹn đúng 1 điểm . Trận chiến chống xuống hạng của Racing sẽ rất khó_khăn khi vòng tới , họ phải gặp Osasuna , đội bóng đang đứng thứ 4 trên BXH .
Với 69 điểm , Real tạm_thời vươn lên vị_trí thứ 2 , hơn Valencia 1 điểm và Osasuna 4 điểm . Cuộc đua_tranh ngôi á_quân sẽ còn rất sôi_nổi bởi trong 2 vòng đấu cuối , Real sẽ phải gặp những đối_thủ “ khó xơi ” là Villareal và Sevilla trong khi Valencia sẽ có cuộc gặp “ thượng_đỉnh ” cuối_cùng với chính Osasuna ở vòng 38 .
M.Hải

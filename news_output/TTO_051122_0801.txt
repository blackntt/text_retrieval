﻿ Giao cầu Nông_Sơn cho UBND huyện Quế_Sơn quản_lý
Phó_chủ_tịch UBND tỉnh Quảng_Nam_Lê_Minh_Ánh cho_biết toàn_bộ công_trình cầu đã hoàn_thành và đã được nghiệm_thu . Hằng năm UBND tỉnh sẽ cấp kinh_phí duy_tu bảo_dưỡng theo qui_định .
Được biết , cầu Nông_Sơn là chiếc cầu được xây_dựng với sự đóng_góp của nhiều tấm_lòng đồng_bào cả nước và bạn_đọc báo Tuổi_Trẻ sau vụ đắm đò làm 18 học_sinh thiệt_mạng ( vào chiều ngày 19-5-2003 ) .
Với nghĩa_tình đó , Chủ_tịch UBND tỉnh Quảng_Nam_Nguyễn_Xuân_Phúc đã nhiều lần nhắc_nhở các đơn_vị liên_quan đặt bảng ghi_nhận tấm_lòng của bà_con , thế_nhưng đến nay các đơn_vị vẫn chưa thực_hiện .
HOÀI_NHÂN

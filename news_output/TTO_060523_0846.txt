﻿ Kiện_tướng Lê_Quang_Liêm không được đặc_cách vào lớp 10
Theo Bộ GD - ĐT , qui_chế tuyển_sinh THCS và THPT không qui_định đặc_cách tuyển thẳng vào lớp 10 đối_với những HS đoạt giải cá_nhân như Liêm mà HS chỉ được cộng điểm khuyến_khích khi dự_tuyển .
Trao_đổi với Tuổi_Trẻ , một cán_bộ lãnh_đạo Sở GD - ĐT_TP . HCM tỏ ra bức_xúc : “ Quyết_định của bộ gây khó cho HS , gây khó cho phụ_huynh và cho cả chúng_tôi . Trên thực_tế Lê_Quang_Liêm về nước chỉ trước một ngày diễn ra kỳ thi_tuyển sinh lớp 10 thì còn tâm_trí đâu mà thi ” .
HOÀNG_HƯƠNG

﻿ Iran " sẵn_sàng đối_thoại "
Phát_biểu tại Indonesia , ông Ahmadinejad nói thật " lố_bịch " khi các nước có vũ_khí_hạt_nhân lại dồn_ép không cho Iran phát_triển năng_lượng hạt_nhân .
Ông nói vụ tranh_cãi hạt_nhân với các cường_quốc phương Tây chỉ là sự " tuyên_truyền tâm_lý " và nói Iran rất khó bị tấn_công quân_sự .
Tổng_thống Ahmadinejad cũng cáo_buộc Cơ_quan năng_lượng nguyên_tử quốc_tế áp_dụng kiểu chuẩn_mực kép và làm_việc dưới sự ảnh_hưởng của Mỹ , Anh và Pháp .
Trong khi đó , Giám_đốc IAEA Mohamed_El Baradei đã hoanh nghênh nỗ_lực ngăn_chặn hành_động cấm_vận Iran sau khi ông Ahmadinejad nói sẵng sàng đàm_phán .
Tại_Mỹ , giới_chức Nhà_Trắng tuyên_bố sẽ không có liên_hệ trực_tiếp với Iran và việc cấm_vận phải là một phần trong đề_nghị " cà_rốt và cây gậy " do các cường_quốc đưa ra để ngăn_chặn chương_trình hạt_nhân của Iran .
Mặc_dù giới_chức Mỹ nói sẽ không từ_bỏ con đường ngoại_giao quá dễ_dàng nhưng nhấn_mạnh sẽ không cho_phép các cuộc đối_thoại kéo_dài mãi tại Hội_đồng bảo_an .
Theo kế_hoạch , trong vòng 10 ngày tới Anh , Pháp và Đức sẽ giới_thiệu một gói giải_pháp mang tính thuyết_phục lẫn đe_dọa nhằm vào Iran dựa trên mức_độ hợp_tác của Tehran .
Hiện vẫn chưa rõ liệu Nga và Trung_Quốc sẽ " mềm " hơn với đề_xuất cấm_vận Iran hay không .
Tại_Nga , Chủ_tịch Hội_đồng an_ninh Igor_Ivanov cảnh_báo hành_động tấn_công Iran sẽ làm " bùng_nổ " khu_vực Trung_Đông và xa hơn_nữa .
S . N . ( Theo AP , Financial_Times )

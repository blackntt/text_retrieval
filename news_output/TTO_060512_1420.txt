﻿ Tạm ngưng chích văcxin Priorix trên toàn_quốc
“ Để đảm_bảo an_toàn tiêm phòng văcxin cho nhân_dân , tạm ngừng ngay việc tiêm văcxin Priorix phòng sởi , quai_bị , rubella do GlaxoSmithKline Biologicals S . A sản_xuất cho tới khi Bộ Y_tế thông_báo tiếp_tục sử_dụng loại văcxin này ” - hôm_qua 11-5 , phó cục_trưởng Cục Y_tế dự_phòng Nguyễn_Văn_Bình đã ký thông_báo khẩn như trên gửi các sở y_tế trên toàn_quốc .
Theo Cục Y_tế dự_phòng , các trẻ 13-16 tháng tuổi vào Bệnh_viện Nhi_Đồng 1 trong vài ngày vừa_qua sau khi tiêm văcxin nói trên đều có biểu_hiện sốt cao , sưng tấy chỗ tiêm và nghi nhiễm_khuẩn huyết . Theo đánh_giá của nhiều chuyên_gia , nếu hiện_tượng nói trên xảy_ra với nhiều trường_hợp , một trong những nguyên_nhân được nghĩ đến nhiều là chất_lượng văcxin .
Hai chuyên_gia an_toàn văcxin sang VN
Chiều 11-5 , bà Nguyễn_Thị_Tường_Vi - giám_đốc phụ_trách bộ_phận văcxin , Công_ty GSK - cho_biết văcxin Priorix được sản_xuất tại Bỉ . Từ năm 2002 đến nay đã nhập_khẩu vào VN 139.064 liều , được phân_phối bởi Công_ty Zuellig_Pharma VN .
Số_lượng văcxin còn tồn_kho đến ngày 11-5 là 49.623 liều . Số_lượng của lô Prionix A 69 CA409A ( văcxin chích cho ba trẻ bị tai_biến nằm trong lô này - PV ) là 11.000 liều , nhập vào VN tháng 11-2005 .
Tính đến ngày 11-5 đã được phân_phối đến các đơn_vị y_tế trên toàn_quốc với hơn 5.000 liều đã được sử_dụng , còn gần 6.000 liều đang nằm ở kho của Công_ty Zuellig_Pharma .
Theo yêu_cầu của Sở Y_tế , GSK cũng đã thông_báo đơn_vị phân_phối tạm ngưng phân_phối văcxin này . Ngoài_ra , GSK tại VN cũng đã thông_báo tình_hình về công_ty_mẹ tại Bỉ . Trong ngày 10 và 11-5 đã có hai chuyên_gia y_khoa phụ_trách về vấn_đề an_toàn văcxin bay từ Bỉ sang để phối_hợp với cơ_quan_chức_năng VN và TP.HCM điều_tra nguyên_nhân .
LÊ_THANH_HÀ

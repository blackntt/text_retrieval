﻿ Bình_Thuận : tăng cân cho trẻ bằng độc_chất
Hôm_qua 1-6 cho_biết Trung_tâm Y_tế dự_phòng Bình_Thuận đã báo_cáo với ngành chủ_quản và kiến_nghị các cơ_quan_chức_năng của tỉnh xử_lý Trường mẫu_giáo tư_thục Thanh_Nguyên ( cơ_sở 2 , số 22C Cao_Thắng , TP Phan_Thiết ) vì đã sử_dụng chất Dexamethasone để tăng cân cho trẻ .
Kết_quả xét_nghiệm của Viện Y_tế công_cộng TP.HCM ngày 25-5 vừa_qua cho_biết trong số năm mẫu thức_ăn của trường được cơ_quan_chức_năng gửi kiểm_tra , có đến bốn mẫu ( canh chua cá , thịt xốt cà , tôm ram , canh sườn bí_đỏ ) có sự hiện_diện của dược_chất nói trên .
Theo các bác_sĩ chuyên_khoa , Dexamethasone được xếp trong bảng thuốc_độc , được dùng theo chỉ_định của ngành y_tế , nếu_không sẽ gây hại bao_tử , cao huyết_áp , loãng_xương , phù_thũng vì giữ nước trong cơ_thể ...
Được biết , Trường mẫu_giáo Thanh_Nguyên ( cơ_sở 2 ) đang nuôi_dạy 150 cháu .
Theo Tuổi_Trẻ

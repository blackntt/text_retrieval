﻿ “ Đêm trắng ” của sinh_viên Y_dược
Trong đêm hội , sau phần lễ rước đuốc truyền_thống với nghi_thức kết_nghĩa giữa SV khối 2003 và tân SV khối 2004 là chương_trình ca_nhạc gắn với chủ_đề “ Góp tay xoa_dịu nỗi đau da_cam ” khá đặc_sắc .
Sôi_nổi nhất_là phần bán_đấu_giá ba bức tranh được ghép từ hơn 20.000 ngôi_sao ( được trích từ hơn 100.000 ngôi_sao trong phần lập kỷ_lục “ ngôi_sao niềm_tin ” ) do chính các SV thực_hiện , ủng_hộ các nạn_nhân chất_độc da_cam .
Sau hơn một giờ đấu_giá , ba bức tranh đã được bán với tổng_số tiền 8,1 triệu đồng . Đảng_ủy , công_đoàn trường cũng ủng_hộ 5 triệu đồng và SV đã đóng_góp 500.000 đồng .
Toàn_bộ số tiền này sẽ được ủng_hộ chương_trình “ Góp tay xoa_dịu nỗi đau da_cam ” của báo Tuổi_Trẻ ( trước đó trường đã ủng_hộ 43.023.500 đồng ) .
TRẦN_HUỲNH

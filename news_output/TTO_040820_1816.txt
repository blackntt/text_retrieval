﻿ Xe_hơi chạy bằng năng_lượng mặt_trời nhanh_nhất thế_giới
Chiếc xe màu đen bóng ba bánh dành cho một người , dài 5m và rộng 2m này do một nhóm sinh_viên đại_học Hà_Lan chế_tạo . Nuna II có bộ khung khí_động_học giống như cánh máy_bay và được nạp nhiên_liệu bằng các tấm hấp_thu năng_lượng mặt_trời tương_tự các phi_thuyền của Cơ_quan hàng_không châu Âu ( ESA ) có_thể nâng tốc_độ lên đến 170 km/giờ .
Tháng 10-2003 , Nuna II đã thiết_lập một kỷ_lục mới tại vòng đua mang tên " Thách_thức mặt_trời thế_giới " tại Úc khi vượt qua 3.010 km chỉ với 31 giờ 5 phút .
M.KA-AP

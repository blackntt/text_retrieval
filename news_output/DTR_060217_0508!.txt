﻿ Giết 2 con bị bệnh hiểm_nghèo rồi tự_tử
19h30 ngày 14/2 , tại khu_phố 4 , thị_trấn huyện Tân_Châu ( tỉnh Tây_Ninh ) , chị Lưu_Thị_Phước ( SN 1978 ) đã dùng thuốc_trừ_sâu cho hai con bị mù và bị bại não uống .
Đó là hai cháu Lê_Thị_Trà_Giang và Lê_Thị_Hương_Giang , chúng là con sinh_đôi của chị Phước , sinh năm 2004 .
Sau đó chị Phước cũng uống để tự_tử . Hậu_quả 2 cháu chết tại bệnh_viện Đa_khoa Tây_Ninh , còn chị Phước được cấp_cứu tại bệnh_viện huyện Tân_Châu .
Qua điều_tra và khám_nghiệm hiện_trường được biết , cháu Trà_Giang bị bệnh mù mắt từ nhỏ còn cháu Hương_Giang bị bệnh bại não từ nhỏ . LH

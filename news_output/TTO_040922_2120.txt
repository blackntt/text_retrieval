﻿ Thời_gian vàng của một giám_đốc bệnh_viện
Ngay sau khi bị nạn , tâm_lý người_dân và cả BS ở tuyến dưới là muốn chuyển về Bệnh_viện ( BV ) Chợ_Rẫy . Nhẹ cũng chuyển , nặng cũng chuyển , chuyển cả bằng xích_lô , xe_máy ... Nhiều trường_hợp ở xa , bệnh_nhân ( BN ) chết trên đường vận_chuyển , có tới được BV Chợ_Rẫy cũng đành mang xác về .
Ông nói : “ Rất nhiều ca_từ tỉnh đưa lên nếu được giải_quyết sớm hơn thì sẽ không có di_chứng , có_thể BN không tử_vong ...
Trước những hình_ảnh đau_đớn của người_thân , câu_hỏi đặt ra trong đầu tôi : Tại_sao BN không được cứu_chữa ngay sau khi bị tai_nạn , mà có_khi phải đi mất tám tiếng đồng_hồ mới tới BV Chợ_Rẫy ?
Trong khi đây là khoảng thời_gian còn cứu_chữa được và cứu_chữa có hiệu_quả nhất . Từ đó chúng_tôi đưa ra khái_niệm “ thời_gian vàng trong chấn_thương sọ não ( CTSN ) ” .
Mà muốn tận_dụng được thời_gian vàng này chỉ có một_cách duy_nhất là xây_dựng mạng_lưới ngoại thần_kinh ( NTK ) , làm thế_nào để BS ở các tỉnh cũng mổ được CTSN” .
Tận_dụng tối_đa thời_gian vàng
Câu_chuyện đi xây_dựng mạng_lưới NTK bắt_đầu_từ những suy_nghĩ tự_phát như_vậy và anh_em lên_đường về các tỉnh .
Không có công_tác_phí , không có ôtô công . Nhiều BS trẻ bỏ phòng_mạch “ nằm ” dưới tỉnh một tuần , 10 ngày ...
Có nơi khó_khăn , anh_em ăn , ngủ tại phòng_khám , phòng cấp_cứu . Về rồi lại đi , với quyết_tâm “ cầm tay chỉ việc ” đến khi BV tỉnh cũng mổ CTSN và phải mổ được .
Ông tâm_sự : “ Gian_nan lắm . Tôi đi thuyết_phục BV tỉnh , sở y_tế , rồi lãnh_đạo tỉnh . Gom tất_cả dụng_cụ hiện có_của cơ_sở để đưa vô mổ sọ não - từ kềm , kéo , cái gặm xương ...
Tôi lội chợ_trời mua khoan điện công_nghiệp đem về cải_tạo thành khoan y_tế , dù có ít_ỏi cũng chia cho nhau và khi tìm không được nữa thì đi xin .
Cái khó lớn nhất mà chúng_tôi gặp là tỉ_lệ tử_vong mổ NTK bao_giờ cũng cao hơn bệnh khác nên một_số BS học xong chùn tay_không mổ , cứ chuyển viện .
Hỏi ra mới biết họ sợ mổ BN chết , mà ở tỉnh chỉ cần một ca thất_bại sẽ bị đồn ầm lên thì phòng_mạch ... ế ! Nên buổi khởi_đầu , những ca nặng , khó , tử_vong ... BS Chợ_Rẫy nhận hết , còn những ca nhẹ mổ thành_công dành cho BS cơ_sở ” .
Vậy đó . Họ âm_thầm nhận cái khó , cái rủi về mình . Nhiều đêm nghe gọi chi_viện ca khó là họ lên_đường .
Ông kể : “ Có một lần khi nhận được tin đã 9 giờ đêm . Với chiếc xe Jeep cũ , chúng_tôi lại lên_đường . Mổ xong đã gần 3g sáng , họ cho ngủ ở hội_trường BV . Đoàn có hai BS nam , hai nữ ... , cả êkip tìm tới nhà người_thân của một BS Chợ_Rẫy ở gần đó , gõ_cửa , lục cơm nguội ăn , ngủ một_chút rồi quay về . BN đó được cứu sống ” .
Khoa NTK bắt_đầu đi tỉnh năm 1993 , đến những năm 1995 , 1996 một_số đơn_vị như BV đa_khoa Bình_Dương , Đồng_Nai , Cần_Thơ ... đã mổ được CTSN .
Tại_Hội nghị Việt - Úc về NTK ( 3-1999 ) , BS của BV đa_khoa Cần_Thơ , Đồng_Nai đã tự_tin báo_cáo về những ca mổ CTSN với đúc_kết : Nhờ tránh mất thời_gian vàng do chuyển viện , mổ CTSN ở tuyến tỉnh đã góp_phần làm giảm tỉ_lệ tử_vong , di_chứng cho BN bị máu tụ ngoài màng_cứng .
Đến nay , mạng_lưới BS ở tỉnh được BV Chợ_Rẫy đào_tạo mổ CTSN là 156 người , rải khắp các BV 28 tỉnh phía Nam . Mạng_lưới này vẫn tiếp_tục được củng_cố .
Chưa hề có một ngân_sách nào dành cho chương_trình này , cũng không có một biên_bản cam_kết nào , mà chỉ có chung một mục_tiêu “ cướp ” lấy thời_gian vàng cho mạng sống của những người bị TNGT , “ chỉ có một hợp_đồng thỏa_thuận bắt_nguồn từ ... những trái_tim nhân_hậu ... ” - BS Trương_Văn_Việt nói .
Người thích … đột_phá
Năm 1995 , khi còn là trưởng khoa NTK , BS Việt cứ ao_ước “ làm thế_nào để BV có được một máy CT-scanner ” .
Giờ_đây BV có bốn máy CT-scanner ( một máy multislice ) , máy cộng_hưởng từ ( MRI ) , máy chụp mạch_máu số_hóa xóa nền ( DSA ) , máy xạ_trị X-knife ...
Khoa ung_bướu được thành_lập trong chương_trình chống ung_thư quốc_gia , kinh_phí cấp 9 tỉ để đầu_tư máy Cobalt .
Ông quyết_định “ đốt giai_đoạn ” , không dùng máy Cobalt mà vay vốn thành_phố để trang_bị hệ_thống xạ_trị 87 tỉ đồng .
Vậy_là những người mắc bệnh hiểm_nghèo trước_đây phải chịu chết hoặc bằng mọi giá phải tìm ra nước_ngoài chữa_trị , thì nay điều kỳ_diệu đã đến ( không ít Việt_kiều từ nước_ngoài đã tìm về BV Chợ_Rẫy ) .
Bằng nội_lực , chỉ trong thời_gian ngắn BV đã trang_bị được hệ_thống thiết_bị kỹ_thuật hiện_đại .
Ông tâm_sự : “ Phải vượt qua tất_cả . Điều quan_trọng là làm thế_nào để BV có thiết_bị hiện_đại nhất và phải khai_thác hiệu_quả nhất . Và tôi đang ấp_ủ nhiều chương_trình bứt_phá nữa trong trang_bị kỹ_thuật y_khoa , đó là máy thở . Chúng_tôi đang nghiên_cứu hình_thức để thuê máy thở và đào_tạo sử_dụng sao cho hiệu_quả ” .
Không_chỉ có những táo_bạo trong đầu_tư trang_bị kỹ_thuật , ông còn có những cải_cách táo_bạo khác .
Bộ Y_tế qui_định xếp lịch mổ mỗi tuần , ở nhiều BV lớn có BN nằm chờ thường từ 10 ngày đến cả tháng , rất tốn_kém và đau_đớn .
Có nơi hẹn 2-3 tháng , BN phải phong_bao để được mổ sớm . Nhiều BV mở ra mổ dịch_vụ , từ 2g chiều đã đóng_cửa phòng mổ chương_trình để mổ dịch_vụ . Vậy_là người nghèo cũng phải đi vay_mượn để được mổ sớm .
Giữa năm 2000 , ông cho sắp_xếp lại toàn_bộ qui_trình khám , xét_nghiệm và quyết_định xếp lịch mổ mỗi ngày .
Quyết_định làm đảo_lộn cả một trình_tự , thói_quen . Phòng mổ làm_việc đến 7 - 8 giờ tối để giải_quyết hết số ca đã lên lịch trong ngày . Khi_không còn cảnh ứ_đọng , BN không cần phải đăng_ký mổ dịch_vụ , tiêu_cực cũng không còn đất sống .
Vừa nhận chức giám_đốc , ông đã quyết_định ... thay ngay 13 trưởng khoa . Một quyết_định gây chấn_động tâm_lý nhiều người .
Ông nói : “ Khi ấy tôi biết rằng sẽ có khó_khăn và tôi cũng lượng sức mình phải chịu_đựng những phản_ứng . Nhưng chúng_tôi có được thuận_lợi khác đó là một đội_ngũ cán_bộ trẻ ” .
Bây_giờ , BV Chợ_Rẫy đã đạt chuẩn để người_bệnh , bạn_bè trong và ngoài nước tin_cậy . Đặc_biệt nhiều BS trẻ đang tiềm_ẩn tài_năng đã tìm đến BV Chợ_Rẫy để được thể_hiện .
BV có 1.100 giường với số nằm nội_trú trên 2.000 người thì ít_nhất có khoảng 5.000 thân_nhân nuôi bệnh , chưa kể số ngoại_trú .
Trước_đây người_bệnh tự lo bữa ăn , thân_nhân đun_nấu dưới gốc cây , dọc bờ tường ... vô_cùng nhếch_nhác .
Ông hợp_đồng với Công_ty Dussmann ( Đức ) cung_cấp suất ăn định_chuẩn theo bệnh_lý , hợp_đồng với Dussmann làm sạch BV theo công_nghệ mới , bàn_giao một_số hộ_lý cho công_ty để được huấn_luyện sử_dụng thiết_bị vệ_sinh chuyên_ngành , có người được đào_tạo ở nước_ngoài .
Ông nói : “ Dù trong qui_chế và cơ_cấu tổ_chức của Bộ Y_tế là hộ_lý làm vệ_sinh BV và chăm_sóc BN , nhưng theo tôi cả hai nhiệm_vụ này đều không hợp_lý . Làm vệ_sinh BV mà không được học thì nhiều nguy_hiểm cho chính bản_thân , cho BV cũng_như cho môi_trường . Thế_giới đã có chuyên_ngành làm sạch BV , tại_sao ta không tham_gia ? ” .
Không để người_ở tỉnh xa nuôi bệnh phải lây_lất khắp các hành_lang , ông xây nhà cho thân_nhân ngay tại khu nhà dành cho giám_đốc BV thời trước giải_phóng .
PGS.TS Trương_Văn_Việt quê ở Giồng_Riềng , Kiên_Giang . Từ 1975-1977 phó khoa_ngoại thần_kinh BV Chợ_Rẫy , 1977-1979 thực_tập_sinh ở CHDC Đức , 1979-1989 trưởng khoa_ngoại thần_kinh , 1994-1996 phó_giám_đốc và từ 1997 là giám_đốc BV Chợ_Rẫy đến nay .
Năm 1989 ông bảo_vệ luận_án phó_tiến_sĩ tại Viện_Hàn lâm đào_tạo sau_đại_học ( DDR ) CHDC Đức với đề_tài “ Một thủ_thuật mổ dò động_mạch cảnh xoang hang ” .
Theo đánh_giá của GS.TS H.G.Niebeling - viện_trưởng Viện_Phẫu thuật thần_kinh , chủ_tịch hội_đồng : tác_giả đã không bó_tay mà đưa ra kỹ_thuật mới giải_quyết được một khó_khăn trong lĩnh_vực dò động_mạch cảnh xoang hang .
Ông đã được trao_tặng Huy_chương Kháng_chiến chống Mỹ hạng II , III ; Huân_chương Lao_động hạng III ; danh_hiệu thầy_thuốc_ưu_tú ; huy_chương Vì sự_nghiệp phát_triển công_nghệ ; huy_chương Vì thế_hệ trẻ ...
Ông từng là phó chủ_nhiệm bộ_môn ngoại thần_kinh Trường đại_học Y_dược TP.HCM ( 1981 ) - rồi chủ_nhiệm ( 1996 ) , đã tham_gia giảng_dạy , hướng_dẫn luận_án cho bốn tiến_sĩ , 19 thạc_sĩ và nhiều BS chuyên_khoa cấp I , II .
KIM_SƠN

﻿ Trường CĐ_SKĐA_TP . HCM tạm ngưng tuyển_sinh 4 ngành
Các chuyên_ngành này đã có in chỉ_tiêu trong cuốn “ Những điều cần biết về tuyển_sinh năm 2006 ” của Bộ GD - ĐT , nhưng điều_kiện về cơ_sở vật_chất đang cải_tạo và xây_dựng không đảm_bảo việc giảng_dạy .
Các chuyên_ngành còn lại vẫn tuyển_sinh bình_thường ở bậc cao_đẳng , trung_cấp . Thí_sinh từ Thừa_Thiên - Huế trở vào , tuổi từ 15-23 ( riêng lớp cao_đẳng diễn_viên kịch điện_ảnh tuổi từ 18-23 ) nộp hồ_sơ dự thi kèm lệ_phí thi từ 15-3-2006 đến hết ngày 10-6-2006 tại các sở GD - ĐT hoặc nộp trực_tiếp về trường : 125 Cống_Quỳnh , Q . 1 , TP.HCM .
H.OANH

﻿ Bình_Dương : giải_quyết các khiếu_nại do đoàn kiểm_tra đất_đai tiếp_nhận
Bà Vân cho_biết nội_dung của các đơn thư khiếu_kiện chủ_yếu là về giá_cả đền_bù , bố_trí tái_định_cư . Tại buổi làm_việc , bà Vân yêu_cầu trong tháng chín Sở Tài nguyên - môi_trường phải thành_lập văn_phòng đăng_ký quyền sử_dụng đất , đồng_thời sớm trình Chính_phủ về điều_chỉnh qui_hoạch đất_đai của tỉnh . Còn đối_với giá đền_bù thì tỉnh sẽ trực_tiếp có các buổi đối_thoại với dân để giải_quyết hợp_tình , hợp_lý .
Ngay sau những kiến_nghị của đoàn kiểm_tra Bộ Tài nguyên - môi_trường , tỉnh đã thành_lập đoàn làm_việc tiến_hành kiểm_tra lại các dự_án trên địa_bàn tỉnh . Trong ngày đầu_tiên , tỉnh đã quyết_định thu_hồi chín dự_án chưa thi_công hoặc thi_công kéo_dài .
ANH_THOA

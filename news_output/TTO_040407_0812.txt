﻿ Hai biện_pháp để quản_lý giá thuốc
TS A.Rietveld đề_xuất biện_pháp thương_lượng với doanh_nghiệp và tạo ra thế cạnh_tranh về giá_thành . Theo đó , Bộ Y_tế , bệnh_viện có_thể trực_tiếp thương_lượng với doanh_nghiệp , nếu giá thuốc phù_hợp mới chấp_nhận cho lưu_hành trên thị_trường hoặc đưa vào điều_trị bệnh_nhân . VN cần thường_xuyên tìm_hiểu giá thuốc trên thế_giới và cung_cấp cho người_dân , tránh tình_trạng doanh_nghiệp cung_cấp thuốc vào VN với giá cao hơn các nước .
Theo ông A.Rietveld , hiện rất nhiều nước trên thế_giới , nhất_là các nước châu Âu , Mỹ , Nhật , Canada ... áp_dụng biện_pháp này và Bộ Y_tế luôn là cơ_quan quản_lý giá thuốc , nhiều nước trên thế_giới đã quản_lý giá thuốc từ lâu ...
TS A.Rietveld hiện làm_việc tại Tổ_chức Sức_khỏe châu Âu . Tháng hai vừa_qua do tình_hình giá thuốc trở_nên cấp_bách , Bộ_trưởng Bộ Y_tế Trần_Thị_Trung_Chiến đã đề_nghị WB tài_trợ ngân_sách mời chuyên_gia quốc_tế giúp tìm biện_pháp quản_lý giá thuốc ở VN .
LAN_ANH

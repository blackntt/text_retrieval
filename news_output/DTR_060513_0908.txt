﻿ Bản_lĩnh Henin-Hardenne
( Dân_trí ) - Đương_kim vô_địch Justine_Henin - Hardenne đã lọt vào đến bán_kết giải Đức mở_rộng sau khi vượt qua đối_thủ người Nga_Svetlana_Kuznetsova với tỷ_số 6-4 , 7-6 ngày hôm_qua .
Trận đấu diễn ra đúng như đã được dự_đoán trước . Hai tay_vợt so_kè nhau từng điểm một . Quả_thật không dễ_dàng cho tay_vợt người Bỉ ở trận này khi đã phải tập_trung cho từng đường bóng mới có_thể giành được chiến_thắng sát_nút .
Những đòn tấn_công đầy sức_mạnh cùng những pha lên lưới chính_xác của Henin-Hardenne đã khiến cho nhà_vô_địch giải Mỹ mở_rộng 2004 Kuznetsova không_thể chống_đỡ .
Henin-Hardenne sẽ gặp tay_vợt chiến_thắng trong cặp Amelie_Mauresmo và Martina_Hingis . Trận đấu được xem là chung_kết sớm của giải này đã phải tạm dừng bởi_vì hệ_thống đèn cung_cấp không đủ ánh_sáng trên sân_đấu .
Những cú đánh dũng_mãnh của Henin
khiến Kuznetsova chống_đỡ rất khó_khăn .
Tay_vợt người Thụy_Sỹ khởi_đầu ấn_tượng . Cô vượt lên dẫn trước_sau 2 game đầu_tiên và có chiến_thắng chung_cuộc séc thứ nhất với tỷ_số 6-4 . Séc hai cũng có tỷ_số tương_tự nhưng lần này phần thắng thuộc về Mauresmo .
Sau 2 séc đấu bất_phân_thắng_bại , trọng_tài đã quyết_định hoãn trận đấu vì hệ_thống đèn trên sân không đủ sáng . Trận đấu sẽ tiếp_tục trong ngày hôm_nay trước khi diễn ra các trận bán_kết .
Phát_biểu sau trận đấu , Henin-Hardenne khẳng_định sẽ thi_đấu với phong_độ tốt nhất trong trận bán_kết cho_dù đối_thủ có là ai đi_nữa .
Cô nói : “ Tôi phải ngã mũ chào Hingis về những gì tôi đang được chứng_kiến . Nhưng_Amelie rõ_ràng là tay_vợt xuất_sắc nhất thế_giới ở vào thời_điểm hiện_nay . Tay_vợt người Pháp đã chứng_tỏ được điều này với mọi người .
Bất_ngờ đã xảy_ra khi Na_Li của Trung_Quốc vượt qua Patty_Schnyder của Thụy_Sỹ với tỷ_số 2-6 7-6 7-6 . Cô đã xuất_sắc khi 3 lần cứu được điểm match point trong trận đấu .
Mặc_dù bị chấn_thương bắp_đùi và chấn_thương chân nhưng sự kiên_trì đã giúp tay_vợt người Trung_Quốc giành phần thắng chung_cuộc .
Ở một trận đấu khác , tay_vợt Nga_Nadia_Petrova đã lội ngược dòng ngoạn_mục trước tay_vợt đồng_hương Dinara_Safina . Séc đấu đầu_tiên , Nadia_Petrova thua 3-6 nhưng cô thắng lại với tỷ_số 6-4 và 6-3 ở 2 séc sau .
Tại bán_kết , Petrova sẽ đối_đầu với Li_Na của Trung_Quốc . Đây sẽ là trận đấu được đánh_giá là nhẹ_nhàng cho tay_vợt Nga khi cô đang có một phong_độ rất tốt ở thời_điểm hiện_tại . Nhưng chúng_ta hãy cùng chờ xem và biết_đâu bất_ngờ lại xảy_ra . Hoàng_Hiệp

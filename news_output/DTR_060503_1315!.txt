﻿ Teo cơ delta - Có_thể chữa khỏi ?
Đến nay tại nhiều địa_phương như Phú_Thọ , Hà_Tĩnh , Sơn_La , Hà_Tây ... đã phát_hiện khoảng 2.000 bệnh_nhân teo cơ delta Nhiều người rất hoang_mang không biết căn_bệnh này có_thể chữa khỏi không ?
Theo PGS.TS Nguyễn_Ngọc_Hưng , Trưởng khoa Phẫu_thuật Nhi_Viện_Nhi_Trung ương , teo cơ delta hoàn_toàn chữa khỏi được . Những trường_hợp nặng cần phẫu_thuật là có_thể hồi_phục chức_năng vận_động , còn những ca nhẹ có_thể tự tập_luyện mà không cần phẫu_thuật .
BS Hưng cho_biết , đến nay đã có 1.238 trường_hợp bị bệnh “ chim sệ cánh ” được phẫu_thuật . Kết_quả sau phẫu_thuật rất tốt , nhiều trẻ_em có_thể sinh_hoạt bình_thường , phục_hồi tốt , không để lại di_chứng .
Chưa xác_định được nguyên_nhân
Đến nay , vẫn chưa xác_định được nguyên_nhân gây xơ_hoá cơ delta . Từ thực_trạng bệnh teo cơ delta thường khu_trú mang tính tập_trung trong một khu_vực , phạm_vi nhất_định … có người cho rằng nguyên_nhân là do môi_trường . Tuy_nhiên , giả_thiết này chưa thuyết_phục được các nhà_khoa_học .
Cũng có ý_kiến cho rằng việc tiêm thuốc ( kháng_sinh , vắc-xin ) vào cơ một thời_gian dài có_thể dẩn đến sự co cứng cơ cũng_như xơ_hoá cơ delta .
Theo PGS.TS Nguyễn_Ngọc_Hưng , bệnh này cũng xuất_hiện ở Mỹ , Nhật , Đài_Loan . Nhiều nghiên_cứu cho thấy , hầu_hết trường_hợp thoái_hoá cơ delta đều xảy_ra sau tiêm các loại thuốc vào vùng cơ delta mà các thuốc sử_dụng thường gặp là Penicilin , Steptomycin , Lincomycin . Tuy_nhiên đến nay thể khẳng_định mối liên_quan giữa tiêm thuốc vào vùng cơ delta và gây xơ_hoá cơ delta .
Dấu_hiệu xơ_hoá cơ
Theo TS Hưng , hầu_hết các bệnh_nhân đều bị “ xệ cánh ” cả 2 bên vai , khuỳnh tay , sệ vai , te cơ …
Người_dân có_thể phát_hiện sớm bệnh bằng cách phát_hiện những biều hiện của trẻ như trẻ không_thể khép chặt hai tay vào thành ngực , hạn_chế các hoạt_động của khớp vai . Thậm_chí có cháu , hai tay dang cách xa thân tới 70 độ .
Ông Lý_Ngọc_Kính , Vụ_trưởng Vụ điều_trị ( Bộ Y_tế ) cũng cho_biết , chưa_thể khẳng_định tiêm vắc - xin là nguyên_nhân dẫn tới bị teo cơ delta vì trong số những người teo cơ có những bệnh_nhân đã 60 tuổi , lứa tuổi chưa bao_giờ tiêm vắc - xin .
Tuy nguyên_nhân của bệnh teo cơ delta chưa được tìm ra nhưng Bộ Y_tế đã hoàn_thành phác_đồ điều_trị bệnh và chuẩn_bị ban_hành trong tháng 5 này .
Tại_Việt_Nam , 8/1994 , TS Nguyễn_Ngọc_Hưng là người phát_hiện ca bệnh teo cơ delta đầu_tiên .
Có chữa khỏi ?
Theo PGS Nguyễn_Ngọc_Hưng , với những trường_hợp xơ_hoá cơ nặng_nề , các bác_sĩ sẽ phải tiến_hành phẫu_thuật tách xơ . Hiện_nay , có 5 kỹ_thuật để mổ xơ_hoá cơ , tuỳ vào từng trường_hợp , tổn_thương cơ nhìn thấy mà bác_sĩ sẽ quyết_định lựa_chọn phương_pháp phẫu_thuật nào tốt nhất cho người_bệnh .
Hầu_hết bệnh_nhân được phẫu_thuật bằng phương_pháp cắt tạo_hình vai chữ Z để không phải cắt bỏ đi đoạn cơ nào , tránh gây lõm trên vai . Sau phẫu_thuật sẽ không để lại hậu_quả sau_này , các chức_năng vận_động được phục_hồi , thậm_chí trẻ vẫn có_thể chơi thể_thao .
Với những trường_hợp mới bắt_đầu có triệu_chứng , bệnh_nhân sẽ không cần phải phẫu_thuật . Tuy_nhiên , nếu_không có chế_độ tập_luyện kịp_thời tình_trạng xơ_hoá cơ sẽ càng phát_triển hơn . Tuỳ tình_trạng bệnh của mỗi người mà các bác_sĩ sẽ có hướng_dẫn vận_động , luyện_tập vật_lý_trị_liệu phù_hợp .
Theo PGS Hưng , kỹ_thuật mổ xơ_hoá cơ delta không có gì phúc tạp . Các bác_sĩ tuyến huyện , tỉnh hoàn_toàn có_thể thực_hiện được các ca phẫu_thuật sau một thời_gian ngắn được tập_huấn . Chi_phí mỗi ca mổ 1,5 - 2,2 triệu đồng . Hồng_Hải

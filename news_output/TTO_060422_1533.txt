﻿ Na_Uy hỗ_trợ Việt_Nam đào_tạo nghề
Thông_tin trên được đưa ra tại cuộc hội_thảo " Hướng tới xây_dựng hệ_thống dạy nghề hiện_đại ở Việt_Nam " do VCCI phối_hợp với Liên_đoàn Giới chủ Nauy tổ_chức ngày 21-4 , tại Hà_Nội .
Phát_biểu tại hội_thảo , ông Hoàng_Văn_Dũng , Phó_Chủ tịch VCCI , cho_biết hiện cơ_sở vật_chất dạy nghề của Việt_Nam còn nghèo_nàn , chưa đủ sức đào_tạo công_nhân kỹ_thuật lành_nghề . Để đạt được mục_tiêu đào_tạo 1,5 triệu lao_động lành_nghề vào 2010 , đáp_ứng yêu_cầu phát_triển và hội_nhập , các doanh_nghiệp Việt_Nam mong_muốn tăng_cường hợp_tác quốc_tế trong lĩnh_vực đào_tạo nghề , trong đó có hợp_tác với doanh_nghiệp Nauy .
Hội_thảo là cơ_hội để các doanh_nghiệp Việt_Nam tiếp_cận thông_tin về các mô_hình dạy nghề tiên_tiến trên thế_giới , về kinh_nghiệm của Nauy trong việc quản_lý hệ_thống dạy nghề ; qua đó giúp nâng cao chất_lượng đào_tạo công_nhân lành_nghề đáp_ứng nhu_cầu yêu_cầu hội_nhập .
Theo TTXVN

﻿ Capello : " Bayern là ứng_cử_viên số 1 "
( Dân_trí ) - " Bayern_Munich là ứng_cử_viên số 1 cho chức vô_địch Champions_League " , đó là lời khẳng_định của HLV Capello của Juventus trong cuộc trả_lời báo_chí gần đây .
Với những thành_tích đạt được tại giải Bundesliga , ông Fabio_Capello cho rằng Bayern_Munich là đối_thủ tương_xứng đối_với bất_kỳ đội bóng nào có ý_định “ nhòm_ngó “ chức vô_địch Champions_League mùa giải này .
Vị HLV này nói : " Theo tôi , Bayern_Munich là ứng_cử_viên hàng_đầu cho chức vô_địch Champions_League " .
Ông Capello cũng khá tự_tin trong trận đấu_loại sắp tới gặp một đội bóng Đức khác là Werder_Bremen và cho_biết " Nhiều khả_năng chúng_tôi sẽ gặp Bayern trong trận chung_kết . Tôi cho rằng Bayern là thử_thách lớn nhất đối_với chúng_tôi trên con đường giành chức vô_địch Champions_League " .
Trong vòng_loại trực_tiếp tới , có_thể khẳng gọi đây là cuộc đối_đầu Đức - Italia , khi Bayern_Munich sẽ đón_tiếp AC Milan trên sân_nhà vào đêm thứ 3 tới , còn ĐKVĐ Serie A_Juventus đến làm_khách trên sân của Werder_Bremen vào đêm thứ 4 .
Ông Capello cho rằng với thời_gian nghỉ đông khá dài , đó chính là cơ_hội tốt và là một thuận_lợi đáng_kể cho những đội bóng đến từ nước Đức .
Trận cầu giữa Bayern_Munich và AC Milan là trận đấu được những fan hâm_mộ hết_sức chú_ý , bởi đây là sự đối_đầu giữa 2 ông lớn của bóng_đá châu Âu với 2 trường_phái hoàn_toàn khác_nhau .
Tuy_vậy đến với trận đấu này , Bayern_Munich có một thiệt_hại rất lớn khi thủ_môn kỳ_cựu Oliver_Kahn khó có_thể ra sân do bị chấn_thương trong trận Bayern hòa Hanover 96 với tỷ_số 1-1 tại vòng đấu trước .
Liệu thủ_môn số 2 của đội bóng Michael_Rensing có_thể chặn được những đợt tấn_công chớp_nhoáng của đội bóng Rossoneri ? Thu_Hương

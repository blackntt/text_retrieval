﻿ Nhóm nghiên_cứu VN nhận giải_thưởng WIPO
Ông Ryan cũng thông_báo giải_thưởng nhà_khoa_học nữ xuất_sắc nhất của WIPO thuộc về tác Võ_Thị_Hạnh , tác_giả công_trình nghiên_cứu sản_xuất các chế_phẩm dùng trong nuôi_trồng thủy_sản và giải_thưởng dành cho tài_năng trẻ xuất_sắc nhất được trao cho Nguyễn_Khánh_Diệu_Hồng , Phạm_Minh_Hảo và Nguyễn_Xuân_Phi , sinh_viên trường Đại_học bách_khoa Hà_Nội .
Công_ty cao_su Sài_Gòn_Kim_Đan nhận cúp WIPO về doanh_nghiệp xuất_sắc nhất vì đã áp_dụng sáng_tạo các quyền_sở_hữu trí_tuệ về thương_hiệu và kiểu_dáng công_nghiệp trong lĩnh_vực sản_xuất và kinh_doanh .
Ông Ryan nói : " Việc khuyến_khích hoạt_động sáng_tạo khoa_học công_nghệ và đảm_bảo cho các công_nghệ mới mang lại lợi_ích cho tất_cả mọi người có_thể giúp Việt_Nam đạt được các Mục_tiêu phát_triển thiên_niên_kỷ và xóa_đói_giảm_nghèo . "
Các giải_thưởng của WIPO sẽ được trao tại lễ trao Giải_thưởng sáng_tạo khoa_học công_nghệ Việt_Nam năm 2003 ( giải VIFOTEC cũ ) , tổ_chức ngày 21/4 tại nhà_hát lớn Hà_Nội . Kể từ chương_trình giải_thưởng WIPO được công_bố năm 1979 , đến nay có hơn 800 giải_thưởng WIPO đã được trao cho các nhà sáng_tạo khoa_học công_nghệ tại 97 quốc_gia .
Theo TTXVN

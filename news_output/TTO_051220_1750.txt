﻿ Giao_lưu “ Một thời và mãi_mãi ”
Đến tham_dự buổi giao_lưu có Trung_tướng Đỗ_Quang_Hưng , chủ_tịch Hội_Cựu chiến_binh TP ; GS.TS Lí_Hoà , nguyên hiệu_trưởng Trường ĐH Tổng_hợp ; Đại_tá Nguyễn_Viết_Tá , tổng_biên_tập báo Cựu_chiến_binh , cùng nhiều thầy_cô và đông_đảo các bạn sinh_viên của nhà_trường .
Trong phần giao_lưu với các thầy_cô_giáo là cựu_chiến_binh các bạn sinh_viên đã được nghe kể lại quá_trình chiến_đấu cũng_như các chiến_công của quân_đội ta trong cuộc kháng_chiến chống Mĩ . Tinh_thần của anh bộ_đội cụ Hồ được các thầy_cô “ hâm_nóng ” bởi những câu_chuyện và những bài ca về “ Một thời và mãi_mãi ” .
Đến với buổi giao_lưu còn có các ca_sĩ như Thanh_Sử ( giải đặt biệt Tiếng hát truyền_hình toàn_quốc năm 1997 ) , Mai_Hậu ( Trung_tâm ca nhạc_nhẹ TP ) … Những bài ca “ đi cùng năm_tháng ” như Cô_gái Sài_Gòn đi tải đạn , Năm anh_em trên một chiếc xe_tăng … đã được cất lên là món quà đầy ý_nghĩa dành tặng cho những người lính nay đang ở một cương_vị mới .
CHÍ_QUỐC

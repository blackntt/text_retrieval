﻿ Thị_trường bất_động_sản " chưa tan băng ”
Nói cách khác , thị_trường bất_động_sản ( BĐS ) vốn đã đóng_băng lại chịu thêm cú sốc về giá vàng tăng sẽ khó_lòng mà “ ấm_áp ” lên trong năm mới .
Giai_đoạn này những năm trước , việc mua_bán nhà_đất diễn ra nhộn_nhịp nhất vì theo tập_quán , người dân_thường thu vốn về vào giáp Tết để đầu_tư mua nhà_đất .
Tuy_nhiên , năm nay , việc mua_bán gần_như ngưng_trệ do giá vàng lên quá cao .
Giá BĐS , giá vàng cao chóng_mặt !
Do giá vàng tăng quá nhanh trong khi việc thanh_toán mua nhà của người_dân hầu_hết đều tính bằng vàng đã khiến người mua “ chùn ” lại . Thậm_chí , không_chỉ các căn nhà trị_giá 200-300 cây vàng bị đóng_băng gần_như hoàn_toàn mà đến cả các căn nhà có_giá dưới 100 cây vàng vốn còn tiêu_thụ được cũng đành chung tình_trạng .
Thậm_chí , những căn nhà nhỏ , trị_giá vài chục cây vàng cũng bị chững lại . Với mỗi căn nhà nhỏ trị_giá từ vài chục tới trăm cây vàng nếu tính ra tiền đồng cũng đã bị tăng_giá vài chục_triệu đồng chỉ trong thời_gian ngắn khoảng hơn tuần_lễ .
Từ khi giá vàng vượt qua ngưỡng 900.000 VNĐ / chỉ thì hầu_hết các giao_dịch đều ngừng_trệ chờ_đợi giá vàng hạ nhiệt trở_lại . Thị_trường mua_bán đất dự_án , căn_hộ mặc_dầu không bị ảnh_hưởng nhiều khi giá vàng lên xuống do việc thanh_toán bằng tiền đồng , nhưng khi giá vàng lên cao cũng khiến cho nhiều chủ đầu_tư gặp khó_khăn trong việc giải_ngân với ngân_hàng .
Với tình_trạng thị_trường như_thế_này , những người vay ngân_hàng để đầu_tư mua nhà_đất lo_lắng nhất bởi vừa_phải chịu áp_lực lãi_suất vừa_phải chịu áp_lực giá vàng lên . Các ngân_hàng cũmg phải đối_diện với “ nợ xấu ” bởi tình_trạng đóng_băng của thị_trường BĐS .
Thị_trường BĐS chết lặng còn khiến cho nhiều doanh_nghiệp khó_khăn vì trả lãi ngân_hàng và nếu cứ kéo_dài tình_trạng này sẽ có ít_nhất hơn 30% DN bị phá_sản . Đó là nhận_định của Hiệp_hội Kinh_doanh BĐS_TP . HCM .
Theo ông Lê_Chí_Hiếu , Tổng_Giám đốc công_ty_cổ_phần Phát_triển nhà Thủ_Đức ( TP.HCM ) , nếu giá vàng cứ tiếp_tục tăng có_thể dẫn đến hiệu_ứng người_dân chuyển sang tích_lũy vàng thay_vì đầu_tư vào BĐS như mọi_khi .
Hiện lợi_nhuận do BĐS mang lại trong tình_trạng thị_trường đóng_băng hiện_nay sẽ không đuổi kịp sự tăng_giá vàng . Nếu giá vàng còn lên thì thị_trường BĐS sẽ còn “ đóng_băng ” mạnh hơn_nữa !
Năm 2006 không sáng_sủa hơn
Hiện_nay , theo đánh_giá của các chuỵên gia , giá chung_cư cao_cấp từ khoảng 10-15 triệu đồng / m 2 sàn đều cao gấp 3-4 lần giá_trị thật . Ở Hà_Nội , ngay cả giá bán cho các đối_tượng chính_sách cũng ở mức 6 triệu đồng / m 2 , mức giá phi thực_tế đối_với khả_năng mua của đối_tượng này .
Theo các chuyên_gia , năm 2006 , tình_trạng thị_trường khó_lòng sáng_sủa hơn bởi tất_cả những biện_pháp để đưa giá BĐS đúng với giá_trị thật hay cân_bằng cung - cầu BĐS đều phải đòi_hỏi giải_quyết tận gốc vấn_đề và có sự kết_hợp của nhiều cấp , nhiều bộ , ngành ...
Những điều này vẫn chưa_thể có được trong thời_gian tới nên giá nhà_đất sẽ vẫn tiếp_tục khiến cho thị_trường chưa_thể tan băng .
Theo ông Đặng_Hùng_Võ - Thứ_trưởng Bộ Tài nguyên - Môi_trường , Bộ đã đề_xuất với Chính_phủ một_số giải_pháp “ phá băng ” cho thị_trường BĐS như : Thứ nhất , Nhà_nước có những giải_pháp kích_cầu để người có thu_nhập thấp và trung_bình có khả_năng thanh_toán khi mua nhà , đất , phát_triển hình_thức trả_góp , trả_chậm rồi thuê_mua ...
Thứ_hai , Nhà_nước trước_mắt giảm cung quỹ đất để thực_hiện các dự_án nhà riêng_lẻ nhằm tránh cho các doanh_nghiệp tiếp_tục đầu_tư .
Thứ_ba , cần có biện tháo_gỡ về vốn cho các nhà_đầu_tư hiện_nay đang bị ngân_hàng đòi thanh_toán nợ , cứu các doanh_nghiệp khỏi phá_sản ...
Đó là những giải_pháp tổng_thể , còn Chính_phủ sẽ phải ra những giải_pháp cụ_thể để giải_quyết tình_trạng đóng_băng trước_mắt . Bình_luận về những giải_pháp này , nhiều ý_kiến cho rằng không_thể tác_dụng vào thị_trường bằng cách giảm cung mà cần kích_cầu hợp_lý .
Theo Cẩm nang mua_sắm

﻿ Thế_giới thu nhỏ tại TP.HCM
" Thế_giới thu nhỏ " bao_gồm nhiều biểu_tượng như bến Nhà_Rồng của VN ( cao 2,5 m , dài 5m , rộng 4,5 m ) , tháp Eiffel của Pháp ( cao 3m , ngang 1,25 m ) , tháp nghiêng Pisa của Ý ( cao 2,2 m , đường_kính 0,62 m ) , đồng_hồ Big_Ben của Anh ( cao 3,5 m , ngang 0,5 m ) , nhà_hát Opera_Sydney của Úc ( cao 2,5 m , ngang 2,2 m ) , kim_tự_tháp Ai_Cập ( dài 2,5 m , cao 2m ) , hay đồi Hollywood của Mỹ ( dài 6m , rộng 1,8 m ) ... Những tác_phẩm này đều được làm từ nhựa composite và khung thép .
Toàn_bộ những mô_hình kiến_trúc thu nhỏ sẽ được trưng_bày sống_động giữa lòng thành_phố để khách tham_quan và chụp ảnh lưu_niệm miễn_phí . Bên_cạnh đó là những đêm giao_lưu văn_hóa ( 7-5 giao_lưu văn_hóa châu Mỹ ; 8-5 giao_lưu văn_hóa châu Á ; 13-5 giao_lưu văn_hóa châu Âu ) với các chương_trình ca_nhạc , giới_thiệu trang_phục và các trò_chơi như đua xe_đạp Tour the France , nhảy kangaroo , xây tháp Pisa , vẽ tranh , bóng_rổ ...
Mô_hình thu nhỏ tháp Eiffel ( Pháp )
Mô_hình thu nhỏ đồng_hồ Big_Ben ( London , Anh )
Mô_hình thu nhỏ bến Nhà_Rồng ( TP.HCM )
Q . N .

﻿ Vụ điện_kế điện_tử : Người_dân được chọn_lựa phương_án bồi_hoàn
Giám_đốc Công_ty Điện_lực TP Nguyễn_Thành_Duy trình_bày hai phương_án tính bồi_hoàn được các đơn_vị liên_quan thống_nhất như sau : đối_với 15.000 khách_hàng đã thay ĐKĐT bằng điện_kế cơ thì áp_dụng phương_án tính bình_quân hai tháng trước và hai tháng sau khi thay ĐKĐT để tính tiền chênh_lệch .
Phương_án hai là gắn điện_kế cơ đối_chứng với ĐKĐT cho hơn 276.000 khách_hàng . Ngành điện sẽ bồi_hoàn lượng điện chênh_lệch trong quá_trình đối_chứng .
Các ý_kiến đồng_ý trên cơ_sở hai phương_án đã chọn . Đối_với khách_hàng đã thay ĐKĐT áp_dụng theo phương_án tính bình_quân . Riêng phương_án đối_chứng áp_dụng cho khách_hàng chưa thay ĐKĐT còn nhiều ý_kiến không đồng_tình .
Cuối_cùng cuộc họp thống_nhất : đối_với khách_hàng chưa thay ĐKĐT nên để người_dân chọn_lựa một trong hai phương_án trên .
PHÚC_HUY

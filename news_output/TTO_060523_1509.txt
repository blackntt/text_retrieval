﻿ Giải_thưởng âm_nhạc của ASCAP 2006
50 Cent nhận giải Sáng_tác ca_khúc của năm vì có tới năm bài hát đoạt giải_thưởng 2005 . Tuy_nhiên ca_khúc của năm lại không do 50 Cent trình_bày , thay vào là Green_Day và Mariah_Carey . Và dĩ_nhiên đó là hai bản rình_rang nhất năm 2005 , Boulevard of Broken_Dreams và We_Belong_Together của các tác_giả Johnta_Austin , Jermaine_Dupri và Manuel_Seal .
Nghe_Gold_Digger_Nghe_Boulevard of Broken_Dreams
Green_Day cũng kiếm thêm giải Giọng hát sáng_tạo ASCAP , trong khi đó , giải nhà_sáng_lập ASCAP trao cho Lennox vì các tác_phẩm đầy cảm_hứng của cô .
Hãng EMI Music được chọn là nhà phát_hành âm_nhạc trong năm vì có 17 ca_khúc đoạt giải .
A.NG . ( Theo AP )

﻿ Hình_dáng người thay_đổi theo mùa
Những thay_đổi thời_tiết khiến mỡ chuyển vùng trong cơ_thể , vì_thế làm biến_đổi hình_dáng toàn_thân tại những thời_điểm nhất_định trong năm .
Các nhà_nghiên_cứu cho_biết chính_sự biến_đổi hàm_lượng testosterone đã thúc_đẩy sự thay_đổi hình_dáng cơ_thể . Hoóc môn này , đi liền với u bắp và sự hung_hăng , biến_đổi theo mùa ở cả đàn_ông và đàn_bà .
Sự thay_đổi rõ_rệt nhất_là ở vùng hông và eo . Khi testosterone tăng , phụ_nữ trở_nên ít eo ót hơn do mỡ chuyển_dịch lên vùng eo . Điều ngược_lại xảy_ra ở đàn_ông , lượng mỡ được giữ lại nhiều hơn ở vùng bụng khi hàm_lượng testosterone giảm .
Các nhà_khoa_học đã kiểm_tra sự thay_đổi testosterone theo mùa trong nước_bọt ở 220 phụ_nữ và 127 đàn_ông . Họ cũng đo vòng eo và hông của phụ_nữ trong từng mùa .
" Chúng_tôi tìm thấy testosterone của cả 2 giới cao nhất vào mùa thu " , Sari van Anders , tại Đại_học Simon_Fraser , Canada , người đứng đầu nghiên_cứu , nói . " Đồng_thời , sự chênh_lệch vòng eo và hông của phụ_nữ thấp nhất vào mùa thu , và lượng mỡ tập_trung ở các vùng ( như mỡ bụng ) cũng cao hơn vào mùa thu ở các chị_em " .
Phụ_nữ cũng có hàm_lượng testosterone cao vào mùa_hè . Đàn_ông có lượng hoóc môn này thấp nhất vào mùa xuân .
Nghiên_cứu chứng_tỏ phụ_nữ sẽ trở_nên eo ót nhất vào mùa_đông và xuân . Trong khi đó , đàn_ông sẽ nam_tính hơn vào mùa xuân . Các nhà_khoa_học vẫn chưa rõ vì_sao có sự biến_đổi trong hình_dáng cơ_thể . Van_Anders cho rằng những thay_đổi đó có_thể chỉ là sản_phẩm phụ của sự thay_đổi testosterone theo mùa , bắt_nguồn từ những yếu_tố như sức_khoẻ , hệ_miễn_dịch , sự sinh_sản và hành_vi .
Theo VNExpress

﻿ Trưng_bày quả bóng cổ nhất thế_giới
Quả bóng có lịch_sử lâu_đời nhất thế_giới hiện_nay đang được cất_giữ tại Bảo_tàng Smith ở thành_phố Stirling , Scotland .
Quả bóng này được phát_hiện tại phòng của Nữ_hoàng Mary của Scotland .
Các nhà_nghiên_cứu cho rằng quả bóng này có_thể là của Nữ_hoàng Mary hoặc có_thể là của cha_mẹ của nữ_hoàng .
Một khám_phá bất_ngờ là phần ruột quả bóng cổ này được làm từ bong_bóng lợn , vỏ bên ngoài của quả bóng rất dày , có_thể được làm từ da hươu , được khâu chặt lại với nhau . Bảo_tàng Smith đã đem quả bóng này đến Bảo_tàng Đức để tham_dự cuộc triển_lãm cùng_với Cúp bóng_đá thế_giới sắp được tổ_chức tại Đức .
Theo SGGP

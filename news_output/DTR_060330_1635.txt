﻿ Gặp nhau cuối tuần : Chuẩn_bị cho “ bữa tiệc ” tuổi lên 6
Xuân_Bắc , Tự_Long , Công_Lý , Quang_Thắng lập ban_nhạc Những cái ê - ke , Vân_Dung chuyển nghề múa , Ca_sỹ Minh_Quân ồm_ồm cãi nhau với Song_Hye_Kyo … Tất_cả sẽ có trong chương_trình tuổi lên 6 của Gặp nhau cuối tuần .
Để trình_bày “ ngon_lành ” ca_khúc Bà tôi ( Nguyễn_Vĩnh_Tiến ) trên sân_khấu , Những cái ê - ke đã phải tập_luyện miệt_mài suốt … 3 tuần ! Nào phân_đoạn , chia bè , khớp nhạc , Xuân_Bắc lo_lắng vì liên_tục trượt nhạc , anh hát “ Bà tôi đưa tôi ra đầu làng … ” , giọng hát thì đến đầu làng rồi mà nhạc vẫn còn tất_tả đuổi theo .
Mất nhiều thời_gian tập nhất_là tiết_mục của ca_sỹ Minh_Quân , ca_khúc Nếu phải xa nhau với phần bè nữ của … Những cái ê - ke ! Khi bốn chàng xuất_hiện trong những chiếc váy hoa xúng_xính đã khiến Minh_Quân cười chảy nước_mắt . Đến phần điệp_khúc , bốn “ cô_gái ” biểu_diễn vũ_đạo thì Minh_Quân chỉ còn biết ôm bụng đứng trên sân_khấu cười bò .
Vân_Dung thì còn_mệt hơn_nữa , vì vừa mất giọng ( không nói được ) vừa tập múa mỏi người . Khi tất_cả các diễn_viên đã nghỉ_ngơi , ăn_uống , Vân_Dung vẫn miệt_mài tập từng động_tác . Và_Vân_Dung đã được bù_đắp xứng_đáng khi tiết_mục múa Chim_Kơtia được khán_giả hoan_nghênh , cổ_vũ nhiệt_tình .
Chương_trình tuổi lên 6 của Gặp nhau cuối tuần sẽ phát_sóng vào 10h sáng thứ_bảy , 1/4 sắp tới trên sóng VTV3 .
Sau đây là một_số hình_ảnh về bữa tiệc mừng Gặp nhau cuối tuần tròn 6 tuổi :
Nhanh_Nhanh lên Thảo_Vân ơi !
Hình_ảnh trên sân_khấu của nhóm “ Những cái ê_ke ” .
Vân_Dung luyện múa .
Công_Lý , Quang_Thắng trong một màn diễn hài
Ca_sĩ Minh_Quân diện_kiến trên sân_khấu cùng MC quen_thuộc Thảo_Vân
Hát múa mừng_tuổi lên 6 của Gặp nhau cuối tuần Hiền_Hương - Nguyễn_Hằng

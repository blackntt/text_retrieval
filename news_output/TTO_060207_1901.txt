﻿ Mùa xuân chưa cạn ngày ...
Những cánh đào , cánh mai đã héo … để bất_chợt đi ngang ngõ nhà ai , thấy đôi ba nhành đứng trơ_vơ dựa thân vào bờ tường , lòng chợt nao_nao . Thương như thương một niềm gì thật gần mà đã thật xa . Một làn hương bay đi , ươm sắc_màu của mùa đổ dài theo lối gió .
Một mùa nghỉ lễ đi qua . Con_người lại trở_về với nhịp sống ngày thường quen_thuộc . Bước chân vào cơ_quan , gặp lại bao gương_mặt thân_quen chỉ vừa_mới xa chừng mấy ngày , thế_mà bỗng xôn_xao , thế_mà lòng cũng chợt náo_nức . Những lời chúc tết rộn_rã , ai cũng mong một năm mới với tất_cả những điều tốt_đẹp , cho mình và cho cả mọi người . “ Đằng_ấy về quê có vui không ? Các cụ ở nhà vẫn khoẻ chứ ? Mình ở đây thì … ” .
Người sống xa quê sau một chuyến đi dài về thăm nhà , trở_lại Sài_thành và mang theo phong_vị tết quê_hương . Văn_phòng nhỏ mấy chục mét_vuông nhưng đọng cả trong mình vị Bắc , hương Nam từ những thức quà Tết . Những miền đất xa hình_như đang gần lại , quyện lẫn bằng yêu_thương .
Cơn gió xuân vẫn đang thổi … ấm đến nồng_nàn . Nắng có chút gì rạo_rực , chừng_như không im_lặng nổi , cứ muốn trỗi lên sắc vàng thắm . Bước chân trên đường , lòng muốn cười với những gương_mặt người không quen . Mùa xuân cho lòng người ấm hơn , nhiều yêu_thương và muốn san_sẻ yêu_thương hơn .
Cánh mai , cánh đào chừng đã rời cành đi nhiều . Còn cánh xuân vẫn nương bay trong gió , trong nắng và trong lòng người . Ấm và nồng đến lạ .
Người ơi , mùa xuân chưa cạn ngày . Và ngày ơi , lòng người chẳng bao_giờ cạn phai thương_nhớ mùa .
ĐÀ_NẴNG

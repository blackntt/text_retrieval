﻿ Beyonce thần_tượng Diana_Ross …
Ngôi_sao ca_nhạc - diễn_viên điện_ảnh nổi_tiếng Hollywood : Beyonce_Knowles đã tuyên_bố , Barbra_Streisand và Diana_Ross là hai diva có ảnh_hưởng lớn nhất tới sự_nghiệp ca_hát của cô .
Trong bộ phim gần đây nhất của mình “ Dreamgirls ” , Beyonce đã vào_vai một ca_sỹ có phong_cách rất giống nữ danh_ca Diana_Ross và sau đó , cô đã không tiếc lời khen_ngợi những nữ nghệ sỹ tiền_bối . Cô tâm_sự với tạp_chí Hello rằng : “ Tôi rất khâm_phục những ca_sỹ có tài thật_sự và họ hát không vì lý_do tiền_bạc . Họ hát chỉ đơn_giản vì họ có tài và yêu ca_hát mà thôi ! ” .
Beyonce cũng bày_tỏ mong_muốn được đóng một bộ phim hoàn_toàn không liên_quan gì tới vấn_đề ca_hát . Cô nói : “ Lâu_nay , những phim tôi đóng không_ít_thì_nhiều cũng có liên_quan tới âm_nhạc . Vào_vai một ca_sỹ cũng tốt nhưng tôi vẫn hoàn_toàn có_thể đóng những dạng phim chẳng “ dính_dáng ” gì tới chuyện hát_hò . Chắc_chắn thế ! ” .
Vĩnh_Ngọc_Theo_Femalefirst

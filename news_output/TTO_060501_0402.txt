﻿ Thi kịch_nói SV_TP . HCM lần 1
Kết_quả , có năm tác_phẩm của ĐH Ngoại_ngữ - tin_học , ĐH_KHXH - NV và ĐH Sư_phạm đã lọt vào vòng chung_kết xếp_hạng tranh giải nhất ( trị_giá 10 triệu đồng ) , nhì ( 7 triệu ) và ba ( 5 triệu ) .
Được biết cuộc_thi thu_hút hơn 30 kịch_bản của chín trường ĐH - CĐ tham_gia dự thi và hàng ngàn SV đến ủng_hộ . Dự_kiến đêm công_diễn , trao giải sẽ diễn ra ngày 6-5-2006 tại nhà_hát Bến_Thành với sự góp_mặt của các ca_sĩ Đàm_Vĩnh_Hưng , Đoan_Trang , Hiền_Thục , nhóm AC& amp ; M , MTV , hài Anh Vũ - Thanh_Thúy , giao_lưu với Thành_Lộc , Bảo_Quốc , Hồng_Vân ... Vào cửa tự_do .
T.NG .

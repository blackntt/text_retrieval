﻿ Trường_Đại học Mỹ_thuật TP.HCM
- Sư_phạm mỹ_thuật ( 4 năm ) - Đại_học chính_quy ( 5 năm ) - Cao_học mỹ_thuật ( 2 năm ) - Đào_tạo theo yêu_cầu của các tỉnh , thành_phố trực_thuộc TW
Các_Khoa :
1 . Khoa hội_họa : sơn_dầu , sơn_mài , lụa , hoành_tráng … 2 . Khoa_Đồ họa tạo_hình : khắc thạch_cao , khắc gỗ , khắc kẽm , khắc đồng , in lụa , in_đá . 3 . Khoa_Điêu khắc : tượng tròn , tượng_đài kỷ_niệm , gò kim_loại , phù_điêu trên các chất_liệu như gỗ , đá , đồng , xi_măng … 4 . Khoa_Sư phạm Mỹ_thuật 5 . Khoa_Lí luận và Lịch_sử Mỹ_thuật : lý_luận chính_trị , lịch_sử mỹ_thuật Việt_Nam , lịch_sử mỹ_thuật Thế_Giới . 6 . Khoa_Kiến thức cơ_bản 7 . Khoa_Mỹ_Thuật ứng_dụng 8 . Khoa_Sau_Đại học : nghệ_thuật tạo_hình , lí_luận mỹ thhuật , lịch_sử mỹ_thuật 9 . Khoa_Tại chức : sơn_dầu , lụa
Kí_túc_xá : 48 chỗ
Yêu_cầu đặc_biệt :
Khối thi : H , R . Môn thi : văn , hình_họa , trang_trí , bố_cục
Khu_vực tuyển_sinh : Thừa_Thiên_Huế đến Cà_Mau
Điều_kiện dự thi : có năng_khiếu về mỹ_thuật , phải qua sơ_tuyển
Học_phí :
- Năm 1 và 2 : 1,5 triệu/năm - Năm 3,4,5 : 1,8 triệu/năm
Mục_Tiêu đào_tạo :
Sinh_viên tốt_nghiệp có_thể làm_việc ở :
- Phòng cổ_động của Sở Văn hóa Thông_tin - Thiết_kế bao_bì , mẫu_mã trong các công_ty - Dạy mỹ_thuật ở trường cấp 2 - Dạy lịch_sử mỹ_thuật ở các trường văn_hóa nghệ_thuật - Tham_gia sáng_tác …
Chỉ_tiêu tuyển_sinh :
Năm 2005 - 2006 : - Đại_học chính_quy : 110 - Đại_học không chính_qui : 110
Năm 2004 - 2005 : - ĐH chính_quy : 105 - ĐH không chính_quy : 110
Ghi_chú : Ngành Hội họa và Đồ_họa tuyển_sinh chung , khi học sẽ phân ra .
Hồ_sơ dự kỳ sơ_tuyển , gồm có :
1 . Thi vào khoa hội_họa , đồ_họa , sư_phạm mỹ_thuật : Thí_sinh nộp 2 bức tranh : 1 bức hình_họa đen_trắng ( vẽ chân_dung hoặc vẽ toàn_thân người thật ) vẽ bằng bút_chì đen , 1 phác_thảo bố_cục màu ( vẽ cảnh sinh_hoạt , lao_động hoặc phong_cảnh đất_nước Việt_Nam ) vẽ bằng màu_bột hoặc màu_nước , khuôn_khổ nhỏ nhất_là 30 - 40cm .
2 . Thi vào khoa Điêu_khắc : Thí_sinh nộp 1 bức tượng ( toàn_thân hoặc chân_dung ) , chiều_cao tối_thiểu của tượng là 30cm , hoặc là 1 bức chạm_nổi ( cảnh sinh_hoạt , lao_động ) khuôn_khổ nhỏ nhất_là 30-40cm
3 . Thi vào khoa Lí_luận lịch_sử mỹ_thuật : Thí_sinh nộp 1 bức hình_họa đen_trắng ( vẽ tĩnh_vật hoặc chân_dung người thật ) ; 1 bài vẽ phong_cảnh hoặc tĩnh_vật màu , khuôn_khổ nhỏ nhất_là 30-40cm .
Ghi_chú : ( Tranh hoặc tượng sơ_tuyển phải do thí_sinh tự sáng_tác . Thí_sinh không qua sơ_tuyển thì không được xét dự thi chính_thức )
Địa_chỉ :
Số 5 Phan_Đăng_Lưu , Quận_Bình_Thạnh , TPHCM Điện_thoại : 8416010 - 8412691 - 8030237 Fax : 84 - 8 - 8412695 . mail : thongtinmythuat@hcm.fpt.vn
HIẾU_HIỀN

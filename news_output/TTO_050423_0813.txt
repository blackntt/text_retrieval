﻿ TP.HCM : Hàng ngàn hộ dân “ đứt ” nước
Việc ngưng bơm nước nhà_máy từ ngày 20 đến chiều 22-4 để đấu nối đường_ống gây khó_khăn về nước sinh_hoạt . Trên đường Tân_Hóa , thuộc quận 6 và 11 , hàng trăm hộ không có nước_dùng phải mua 40.000 đồng / m 3 từ các nơi khác chở đến . Hơn 2.000 hộ trên các tuyến đường Bến_Phú_Lâm , Lò_Gốm , Lê_Quang_Sung ... cũng thiếu nước sinh_hoạt .
Đến chiều 22-4 , theo Công_ty Khai_thác và xử_lý nước_ngầm TP , tuyến ống đã đấu nối xong và đang cho súc rửa đường_ống . Ông Phạm_Văn_Chính , giám_đốc công_ty , nói phải chờ đến khoảng 6-7 giờ sau nước_sạch mới có_thể bơm đến các hộ dân .
PHÚC_HUY

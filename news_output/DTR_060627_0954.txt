﻿ Ukraina - kẻ " sống_sót " sau loạt penalty !
Đêm qua , CĐV Đông_Âu đã vỡ òa trong sung_sướng khi ĐT Ukraina thẳng tiến vào vòng tứ_kết bằng thắng_lợi 3 - 0 trước ĐT Thụy_Sỹ sau loạt penalty " định_mệnh " . Trước đó hai đội đã có 120 phút thi_đấu cân_bằng song không một lần tìm được mành lưới đối_phương .
Một trận đấu rất cân_bằng giữa hai đội bóng cùng là những " hiện_tượng " được dự_đoán trước của VCK năm nay đã buộc phải giải_quyết bằng loạt " đấu súng " trên chấm_phạt_đền
Hai hiệp chính với những diễn_biến hoàn_toàn trái_ngược : một thăng_hoa , một chặt_chẽ đã kết_thúc với không một lần dứt_điểm thành_công , Thụy_Sỹ và Ukraina bước vào 30 phút hiệp phụ khi mà thể_lực của các cầu_thủ của họ không còn được đảm_bảo .
Và hệ_quả tất_yếu của nó chính là loạt penalty " định_mệnh " - nơi chỉ mỉm cười với những người có đủ bản_lĩnh và may_mắn .
Sheva lĩnh " ấn tiên_phong " thực_hiện quả phạt_đền đầu_tiên , song áp_lực nặng_nề đã khiến anh dứt_điểm quá hiền_lành , không thắng được thủ_thành Zuberbuhler . Tuy_nhiên , cơ_hội đã không được người Thụy_Sỹ tận_dụng khi họ sút hỏng ... cả 3 quả penalty sau đó , và dĩ_nhiên đây là sai_lầm không_thể tha_thứ .
Lần_lượt Milevskiy , Rebrov và Gusev sửa_sai cho đội_trưởng của mình , ấn_định chiến_thắng 3 - 0 cho ĐT Ukraina , đồng_thời làm_nên kỳ_tích đưa đội bóng_nước này giành quyền lọt vào vòng tứ_kết ngay trong lần đầu_tiên tham_dự World_Cup .
Xin chúc_mừng bóng_đá Đông_Âu ! Còn với ĐT Thụy_Sỹ , họ vẫn hoàn_toàn có_thể rời nước Đức với những cái đầu ngẩng cao .
Các cầu_thủ Ukraine " vỡ òa " sau quả penalty quyết_định của Gusev ( AP )
Trở_lại trận đấu , HLV Blokhin bất_ngờ để cựu_binh Rebrov ngồi ngoài sân để đặt niềm_tin vào Andriy_Vorobey . Trong khi đó , ĐT Thụy_Sỹ mất trung_vệ Senderos do chấn_thương , chơi thay anh là người đồng_đội tại CLB Arsenal_Johan_Djourou .
Hiệp một diễn ra với tốc_độ rất nhanh cùng những pha ăn_miếng_trả_miếng liên_tiếp khi cả hai bên đều không hề tỏ ra thận_trọng và thăm_dò .
Thụy_Sỹ là đội chiến ưu_thế hơn nhờ khả_năng cầm bóng tốt hơn ở trung_tuyến , trong khi đó Ukraina chủ_động sử_dụng những pha phản_công nhanh chớp_nhoáng .
Hai tình_huống đáng tiếc nhất_là hai lần bóng tìm đến xà_ngang sau cú đánh_đầu trong tư_thế bị kèm chặt bởi 2 hậu_vệ đối_phương của Shevchenko và pha sút phạt tuyệt_vời của Alexander_Frei từ cự_ly 25m trước sự bất_lực của thủ_thành Shovkovskiy .
Một thế_trận rất mở được tạo ra và cả hai đội đều có những khoảng_trống để tổ_chức các đường lên bóng , song các cơ_hội cứ đến rồi đi .
Hiệp 1 kết_thúc với tỷ_số 0 - 0 .
Cú sút phạt tuyệt_vời của Frei , đưa bóng đi trúng xà_ngang ( AP )
Ngay ở tình_huống đầu_tiên của hiệp 2 , ĐT Ukraina đã có cơ_hội rất tốt để mở tỷ_số . Nhận đường tạt bóng chuẩn_xác từ đồng_đội từ bên cánh phải , Voronin tung cú đánh_đầu thoải_mái ở cự_ly gần 10m song tiếc là bóng lại đi chệch cột_dọc trong gang_tấc .
Đó cũng là lúc ĐT Thụy_Sỹ chủ_động giảm nhịp_độ trận đấu và áp_dụng một lối chơi an_toàn và chặt_chẽ .
Thế_trận trở_nên căng_thẳng với nhiều pha phạm lỗi ác_ý đến từ các cầu_thủ Thụy_Sỹ . Song đó chính là cơ_hội để Ukraina lấy lại thế_trận và tổ_chức các đợt tấn_công , dồn_ép đối_phương .
Họ tạo được một loạt cơ_hội nguy_hiểm từ những cú dứt_điểm của Sheva , Gusin , Voronin , ... song vẫn không một lần thành_công . Trong khi đó , Thụy_Sỹ hạ thấp đội_hình và chuyển sang sử_dụng những pha bất_ngờ tăng_tốc trong các tình_huống phản_công .
Song chính thế_trận chặt_chẽ đã khiến cho trận đấu trở_nên " buồn_tẻ " và kém hấp_dẫn , khác hẳn với những diễn_biến trong hiệp 1 .
90 phút thi_đấu chính_thức kết_thúc với tỷ_số 0 - 0 .
ĐT Thụy_Sỹ chủ_động áp_dụng lối chơi chặt_chẽ trong_suốt hiệp 2 với những pha bóng rất quyết_liệt ( AP )
Bước vào hiệp phụ cả hai đội đều thể_hiện rõ sự " xuống sức " với rất nhiều pha chuyền bóng thiếu chính_xác . Thế_trận chặt_chẽ tiếp_tục được tạo ra với một tốc_độ trận đấu khá chậm .
Trong tình_thế đó , HLV Blokhin lần_lượt tung hai " con_bài " chiến_thuật của mình là : Rebrov và Artem_Milevskiy ( tài_năng trẻ 21 tuổi ) vào sân với hy_vọng mở được tỷ_số .
Chính thay_đổi này đã trận đấu trở_nên sôi_nổi hơn với khá nhiều tình_huống đột_phá tra_tấn thể_lực hàng thủ ĐT Thụy_Sỹ .
Tuy_nhiên , hai cơ_hội đáng_kể nhất vẫn thuộc về các cầu_thủ Thụy_Sỹ , trong đó đặc_biệt là pha giật góc tinh_tế loại hậu_vệ đối_phương ngay trong vòng cấm của Streller , song đáng tiếc là cầu_thủ này không kịp tung ra cú dứt_điểm , bỏ lỡ cơ_hội ghi_bàn thắng quyết_định .
Hòa 0 - 0 sau 120 phút thi_đấu , hai đội buộc phải bước vào loạt penalty " định_mệnh " .
Và kết_quả - như đã biết ở trên , ĐT Ukraina với sự xuất_sắc của thủ_môn Shovkovskiy cùng bản_lĩnh của các chân_sút đã vượt qua ĐT Thụy_Sỹ với tỷ_số 3 - 0 một_cách xứng_đáng .
Thụy_Sỹ chia_tay với VCK tại Đức , trong khi ĐT Ukraina thẳng tiến vào vòng tứ_kết , gặp ĐT Italia .
Đội_hình thi_đấu :
Thụy_Sỹ : Zuberbuhler , Djourou , Magnin , Muller , Philipp_Degen , Vogel , Cabanas , Wicky , Barnetta , Yakin , Frei . Ukraine : Shovkovskiy , Vashchuk , Nesmachniy , Tymoschuk , Shelayev , Gusev , Gusin , Kalinichenko , Voronin , Vorobey , Shevchenko . Trọng_tài : Benito_Archundia_Tellez ( Mexico ) . Phan_Trung

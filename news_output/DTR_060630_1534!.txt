﻿ Ghét bóng_đá kiểu Brazil
Dân_tộc khác thì không nói làm_gì , nhưng dân Brazil chính gốc mà gay_gắt với bóng_đá đến mức “ phô ” hết ra ngoài đường thì đúng là chuyện lạ .
Dường_như quá bất_mãn với khí_thế hừng_hực của dân_tình sau trận nước chủ nhà gặp đội Ghana , 3 người đàn_ông trút bỏ quần_áo và bắt_đầu cuộc diễu_hành qua các đường_phố .
“ Cả đất_nước chỉ biết cắm mặt vào bóng_đá và World_Cup , trong khi đó các vấn_đề cấp_thiết như giáo_dục và sức_khỏe thì bị bỏ bẵng … ”
Theo cảnh_sát thành_phố : “ Đã có người gọi điện cho chúng_tôi yêu_cầu xử_lý vụ này , nhưng thật_ra đó không phải là hành_động phạm_pháp nghiêm_trọng gì . Vả_lại , chúng_tôi cũng đang … mải xem bóng_đá ” .
“ Với mục_đích duy_nhất là thu_hút sự chú_ý , họ đã thất_bại hoàn_toàn , bởi tâm_trí dân Brazil bây_giờ chẳng còn để đâu ngoài World_Cup ” .
T.Vân Theo Annanova

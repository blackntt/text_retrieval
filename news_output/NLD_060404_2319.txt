﻿ Thanh_tra vụ bác_sĩ “ trấn_lột ” người nhiễm HIV
( NLĐ ) - Ngày 4-4 , sau khi Báo_Người_Lao_Động phản_ánh việc bác_sĩ Trương_Văn_Hà ( Trung_tâm Truyền_thông - Giáo_dục sức_khỏe Tiền_Giang ) câu móc bệnh_nhân về nhà để bán thuốc với giá cắt_cổ , ông Nguyễn_Hùng_Vĩ , Phó_Giám đốc Sở Y_tế Tiền_Giang , cho_biết sở đã chỉ_đạo cho thanh_tra tiến_hành xác_định những sai_phạm của bác_sĩ Hà.Trước mắt cho bác_sĩ Hà tự viết kiểm_điểm và tường_trình những sự_việc mà báo nêu . Sau khi có kết_luận của thanh_tra , trình Ban Giám đốc để có hình_thức xử_lý kỷ_luật .
Th.An

﻿ Rắn tiến_hóa từ loài thằn_lằn đất ?
Người_ta cho rằng khoảng 365 triệu năm trước , các loài động_vật bốn chân đầu_tiên đã di_chuyển từ nước lên_mặt đất . Một_số loài sau đó quay trở_lại biển , bao_gồm tổ_tiên của cá_heo và cá_voi , khi đó chi của chúng biến thành vây .
Song , các nhà_nghiên_cứu về bò_sát đã không_thể thống_nhất về nguồn_gốc của rắn . Một_số cho rằng rắn tiến_hóa từ những con thằn_lằn đất và chúng tiêu biến các chân để có_thể trườn đi dễ_dàng . Ý_kiến khác cho rằng một loài thằn_lằn dưới nước khác , như mosasaur , đã quay trở_lại bờ và biến thành rắn .
Nhằm kiểm_nghiệm giả_thuyết này , Blair_Hedges và Nicolas_Vidal đã so_sánh ADN từ 17/25 họ_hàng của rắn với ADN của tất_cả 19 họ_hàng của loài thằn_lằn .
Họ phát_hiện rắn có nhiều đặc_điểm ADN giống thằn_lằn đất hơn so với mosasaur . Điều này cung_cấp bằng_chứng xác_đáng về sự tiến_hóa trên cạn của loài rắn ngày_nay .
NGUYỄN_SINH tổng_hợp

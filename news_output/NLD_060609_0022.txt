﻿ Cán_bộ CĐ phải giỏi chuyên_môn , có uy_tín
( NLĐ ) - Gần 80 cán_bộ CĐ quận huyện và cán_bộ CĐ cơ_sở trên địa_bàn quận Tân_Phú - TPHCM đã dự buổi mạn_đàm “ Người cán_bộ CĐ của chúng_tôi ” do LĐLĐ quận Tân_Phú tổ_chức chiều 7-6 .
Đây là buổi mạn_đàm mẫu theo chỉ_đạo của LĐLĐ_TP nhằm giúp LĐLĐ các quận , huyện tham_khảo , đóng_góp ý_kiến nhằm đúc_kết kinh_nghiệm trong việc phát_hiện , lựa_chọn tuyên_dương các cán_bộ CĐ điển_hình , đặc_biệt là trong công_tác xây_dựng CĐ cơ_sở vững_mạnh . Tại buổi mạn_đàm , nhiều đại_biểu tán_thành để xét bình_bầu danh_hiệu “ Người cán_bộ CĐ của chúng_tôi ” , người cán_bộ CĐ phải giỏi chuyên_môn , có uy_tín với doanh_nghiệp và công_nhân .
P.Trang

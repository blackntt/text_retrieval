﻿ Công_chúa đi chơi
Hôm_qua , công_chúa bé_con Aiko đã lần đầu được cha_mẹ dắt đi chơi công_viên giải_trí Disneyland ở Tokyo . Đã 4 tuổi rồi , Aiko mới lần đầu được gặp gần_gũi với những nhân_vật của thế_giới thần_tiên tuổi_thơ như chuột Mickey .
Thái_tử Naruhito cùng công_nương Masako đã tổ_chức buổi đi chơi cho con bình_dị như mọi gia_đình trẻ khác khi mời cả bạn_bè của con cùng cha_mẹ chúng đi cùng . Người_phát_ngôn của hoàng_gia Nhật nhân_sự kiện lẽ_ra quá_đỗi bình_thường này phải giải_thích với công_chúng : " Gia_đình thái_tử muốn công_chúa Aiko có được những niềm_vui như mọi đứa trẻ cùng lứa tuổi trước khi vào trường mẫu_giáo " .
TÚ_ANH ( Theo Reuters )

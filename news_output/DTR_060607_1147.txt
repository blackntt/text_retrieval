﻿ “ Tiềm_năng sex ” và những lời đồn_thổi
Chị_em rỉ_tai nhau cứ nhìn mũi , bàn_chân , bàn_tay của anh_ta thì sẽ hình_dung ra “ độ hùng_dũng ” . Họ liếc nhìn những hiện_vật của đối_tác nam để suy ra cách ứng_xử . Các chàng chân_tay to tự_nhiên được cấp thẻ “ tiềm_năng sex ” .
Trên thực_tế , sức_bền tiềm_năng sex ở nam_giới phụ_thuộc vào việc rèn_luyện đều_đặn . Có cụ ông 75-80 tuổi vẫn chưa chịu nghỉ_ngơi , ngoài_ra vẫn rất tự_hào vì “ máy_móc ” còn trơn_tru lắm .
Song thông_thường , tuổi sung_mãn của nam_giới là 27-35 . Sau 50 nếu nghỉ giải_lao thì_có nguy_cơ không trở_lại phong_độ cũ hoặc nói một_cách cụ_thể là không_thể " cất_nhắc " được nữa .
Không ít đàn_ông trung_niên lại nơm_nớp lo “ mình làm chuyện đó sớm quá không_khéo chưa già đã cạn_kiệt , phải gác kiếm nghỉ_ngơi trước thiên_hạ hàng chục năm ” .
Phái nữ thì khác . Nhiều chị_em ngấp_nghé 40 đã tỏ ra thờ_ơ với chuyện chăn_gối . Các ông chồng hậm_hực : “ Chắc cô sinh_hoạt từ năm 16 tuổi nên bây_giờ phải “ nghỉ hưu ” sớm chứ gì ? ” .
Các bà_mẹ chồng , mẹ vợ tương_lai rất cảnh_giác với con_rể mày_râu rậm_rì và nàng dâu mi đen mắt ướt . Họ sợ các đối_tượng trên có tướng mạnh “ chuyện kia ” , một_mặt vắt kiệt_sức chồng ( vợ ) mình , một_mặt dễ nảy_sinh ham_muốn tội_lỗi gây đổ_vỡ hạnh_phúc .
Trên thực_tế , nếu đúng thế_thì đàn_ông Italy , Ả rập đều là những người_tình xuất_sắc . Viagra ở các quốc_gia này hẳn ế dài_dài . Lông_mi , lông_mày rậm là hình cảnh gợi_cảm dễ hướng người_ta nghĩ về chuyện đó thì đúng hơn .
Các cô_gái da trắng tóc nhạt màu thực_ra cũng có thế mạnh riêng . Đấy là chưa nói vẻ đa_tình chưa chắc đã chứng_tỏ sẽ đa_năng về sex .
Có khối ví_dụ về những người có bề_ngoài lù_khù , lông_mày lợt , lông_mi ngắn nhưng sức_khỏe thì khiến khối chàng “ rậm râu sâu mắt ” phải “ khóc thét ” .
Cũng khổ_thân các đối_tượng có phải_cái mũi không bề_thế , mất bao_nhiêu công tán_tỉnh mà chị_em cứ làm_ngơ . Trên thực_tế , người_ta vẫn nhìn thấy những người đàn_ông mũi to ( hoặc dài ) đến tìm gặp bác_sĩ để giải_quyết bi_kịch chăn_gối .
Kết_luận “ nòng súng ” có độ dài tương_quan với độ dài chân người đàn_ông xem_ra là có cơ_sở khoa_học nhất . Nhưng nó dài là một chuyện , có “ nã pháo ” được như đại_bác hay không lại là chuyện khác .
Theo Người_Đẹp

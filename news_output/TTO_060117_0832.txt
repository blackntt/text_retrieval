﻿ Khai_mạc diễn_đàn nghị_viện châu Á - Thái_Bình_Dương
Tham_dự APPF-14 có 26 đoàn với hơn 400 đại_biểu thuộc 22 nghị_viện thành_viên và quan_sát_viên APPF . APPF-14 tiếp_tục thảo_luận những vấn_đề trọng_tâm lớn về chính_trị - an_ninh , hợp_tác kinh_tế - thương_mại và văn_hóa , môi_trường , luật_pháp và trật_tự xã_hội ... nhằm hỗ_trợ trực_tiếp cho hoạt_động của Diễn_đàn Hợp_tác kinh_tế châu Á - Thái_Bình_Dương ( APEC ) , đóng_góp vào việc duy_trì hòa_bình , ổn_định , phát_triển và thịnh_vượng chung của khu_vực .
Sau phiên khai_mạc , theo chương_trình_nghị_sự , APPF-14 sẽ họp các phiên chuyên_đề , thảo_luận các vấn_đề , thông_qua các nghị_quyết và tuyên_bố_chung .
TTXVN

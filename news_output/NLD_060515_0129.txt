﻿ Ba nơi xét_nghiệm vắc - xin Priorix
( NLĐ ) - Ngày 14 - 5 , PGS-TS Trịnh_Quân_Huấn , Thứ_trưởng Bộ Y_tế , cho_biết bộ này vừa thành_lập một hội_đồng gồm những chuyên_gia trong ngành để tìm_hiểu nguyên_nhân chính_xác gây vụ tai_biến do chích ngừa vắc - xin Priorix ở quận 5 - TPHCM .
Bộ Y_tế cũng gửi mẫu tới Tổ_chức Y_tế Thế_giới ( WHO ) để xét_nghiệm , đồng_thời mời chuyên_gia WHO cùng tham_gia trực_tiếp để tìm_hiểu nguyên_nhân . Đại_diện của nhà_sản_xuất vắc - xin đã đến VN đề_nghị lấy mẫu vắc - xin để kiểm_định và đã được chấp_nhận . Ông Huấn cũng cho_biết , việc tiêm phòng loại vắc - xin Priorix là theo yêu_cầu của các gia_đình , nhưng tất_cả những trường_hợp bị ảnh_hưởng do chất_lượng của vắc - xin này , Bộ Y_tế sẽ có hình_thức hỗ_trợ .
D.Thu

﻿ Mâm ngũ_quả ngày Tết
Quan_niệm về mâm ngũ_quả ở mỗi vùng , mỗi địa_phương và mỗi hoàn_cảnh cũng khác_nhau ; có_khi chỉ gồm hai , ba loại quả , nhưng cũng có_khi tới hàng chục loại khác_nhau .
Mối liên_hệ giữa ngũ_quả và ngũ_hành
Kim , Mộc , Thủy , Hỏa , Thổ là 5 trạng_thái vật_chất cấu_tạo nên vũ_trụ .
Kim màu trắng , mộc màu xanh , thủy màu đen , hỏa màu đỏ , thổ màu vàng . Mâm ngũ_quả cũng lung_linh năm sắc_màu đó .
Các quả thường được bày vào dịp Tết là : Bưởi , chuối , hồng , cam , quýt , quất , xoài , táo , đào , na ( mãng_cầu ) , đu_đủ , dưa_hấu ... Tất_cả đều là sản_phẩm được tạo nên bởi sự giao_hòa của trời_đất ( càn_khôn ) , là kết_quả lao_động của con_người .
Trong những ngày Tết , ai cũng muốn tỏ lòng biết_ơn trời_đất tổ_tiên nên bày mâm ngũ_quả thờ_cúng để " báo " tổ_tiên những thành_quả của năm qua , mong_muốn năm mới sẽ thành_công hơn .
Khi chưng mâm ngũ_quả , người cao_niên thường chọn các quả có ý_nghĩa như : Quả bưởi tròn ý nói sự đầy_đủ , sung_túc ; Quả na nhiều hạt ngụ_ý sự sum_vầy đông con_cháu ; Quả quất tượng_trưng cho người quân_tử ; Quả đu_đủ là sự cầu_mong của những gia_đình nghèo được " đủ ăn " .
Theo các nhà dược_liệu và thầy_thuốc Đông_y thì mâm ngũ_quả cũng là một tập_hợp của nhiều vị thuốc .
Tác_dụng chữa bệnh của một_số loại quả trong mâm ngũ_quả
Quả bưởi ( quả bòng ) : người Thái gọi_là cọ phúc , thuộc họ cam quýt - Rutaceae .
- Lá bưởi có tác_dụng chữa cảm ( dùng cùng một_số lá khác để nấu nồi thuốc xông chữa cảm sốt ) .
- Vỏ bưởi dùng chữa đầy chướng bụng , bí tiểu .
- Vỏ hạt bưởi có chất pectin để cầm máu .
- Múi bưởi giúp giải_khát , dùng tốt cho người tiểu_đường .
- Hoa bưởi có hương thơm đặc_biệt , dùng ướp trà và một_số thực_phẩm .
Đu_đủ : còn có các tên là phiêu mộc , phan qua thụ , mắc hung .
Quả chín có tác_dụng bổ_dưỡng , rất tốt cho trẻ_em và người cao_tuổi , người mới khỏi bệnh .
Đu_đủ xanh chứa_chất papain có tác_dụng phân_giải tế_bào , giúp nấu thịt chóng mềm . Rễ đu_đủ dùng làm_thuốc cầm máu .
Hoa đu_đủ hấp đường_phèn làm_thuốc chữa ho cho trẻ_em khàn tiếng .
Ở châu Phi , dân_gian còn dùng lá đu_đủ để chữa khối_u .
Quả hồng : tên khác là thị đinh .
- Tai quả hồng còn có tên Thị đế dùng chữa khí nghịch - nấc .
- Thị sương là chất đường tiết ra từ quả hồng ; dùng chữa đau rát họng , khô họng .
- Thị tất là nước ép từ quả hồng xanh , dân_gian dùng chữa cao huyết_áp .
Hồng_xiêm : ( hình_như di_thực từ Thái_Lan nên có tên này ) . Tên khác : Tầm lửa , Sacoche , Saboche . Tên khoa_học : Sapota achras .
- Nhựa hồng có tác_dụng kích_thích tiêu_hóa .
- Vỏ quả chữa tiêu_chảy
Chuối : là loại quả giàu dinh_dưỡng , tốt cho người cao_tuổi và trẻ_em bị táo_bón . Không dùng chuối cho người tiểu_đường . Chuối hương phối_hợp bột lòng_đỏ trứng gà dùng chữa trẻ suy_dinh_dưỡng .
Như_vậy , mâm ngũ_quả là tâm_sự gửi_gắm của mỗi gia_đình , nói lên lòng biết_ơn trời_đất , tổ_tiên , ước_muốn đầy_đủ và sung_túc , hòa_hợp như năm sắc_màu của thiên_nhiên trong ngũ_hành .
Theo Sư ́ c kho ̉ e & amp ; Đơ ̀ i sô ́ ng

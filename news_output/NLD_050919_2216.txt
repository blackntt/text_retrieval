﻿ Ra_mắt Trường ĐH Văn_hóa TPHCM
( NLĐ ) - Ông Trần_Văn_Ánh , Hiệu_trưởng Trường ĐH Văn_hóa TPHCM , cho_biết trường vừa được nâng_cấp từ CĐ lên ĐH .
Năm nay , trường sẽ mở_rộng quy_mô đào_tạo ; tập_trung xây_dựng chương_trình mới để tuyển_sinh hệ ĐH ; trường xác_định ưu_tiên cho những đề_tài nghiên_cứu khoa_học về văn_hóa phía Nam . Trong chiến_lược phát_triển , trường sẽ đẩy_mạnh hợp_tác đào_tạo với các trường trong khu_vực tiến tới hình_thành Trung_tâm Đào_tạo Văn_hóa khu_vực Đông_Nam Á . Trường đã và đang đào_tạo trên 3.400 sinh_viên và gần 3.420 học_sinh với các chuyên_ngành thư_viện thông_tin , bảo_tàng , phát_hành xuất_bản_phẩm , quản_lý văn_hóa , văn_hóa du_lịch , văn_hóa học ...
D.Hằng

﻿ Microsoft sẽ mua lại Yahoo - tại_sao không ?
Chỉ cần 1 mũi_tên , Microsoft nhắm liền tới 3 đích : biến thù thành bạn , phát_triển ngành kinh_doanh với công_cụ tìm_kiếm , đồng_thời bành_trướng ảnh_hưởng của tập_đoàn trên thị_trường châu Á .
Đó là lập_luận rất thuyết_phục do chuyên_gia Justin_Post thuộc công_ty tư_vấn và quản_lý tài_chính hàng_đầu thế_giới Merrill_Lynch công_bố ngày hôm_qua ( 23/6 ) .
Theo bài phân_tích , Microsoft hiện đang bước vào thời_điểm nhiều thử_thách nhất trong lịch_sử . Mặc_dù cho_đến nay chưa có sản_phẩm nào thành_công vượt_trội được như hệ_điều_hành Windows - phần_mềm được sử_dụng trong hơn 90% máy PC toàn_cầu , nhưng cũng đã đến lúc tập_đoàn cần tìm một hướng phát_triển vượt ra ngoài phạm_vi kinh_doanh truyền_thống .
Càng khó_khăn hơn khi trụ_cột lớn Bill_Gates vừa chính_thức quyết_định : trong 2 năm tới , ông sẽ thôi chức kĩ_sư phần_mềm cốt_cán và chuẩn_bị từng bước cho kế_hoạch giã_từ vị_trí điều_hành công_ty . Chưa hết , những tín_hiệu dội lại từ thị_trường_chứng_khoán cũng không mấy tốt_lành : giá cổ_phiếu Microsoft trong năm qua giảm đến 10% - một bước lùi ngoài sức tưởng_tượng .
Những thực_tế đó khiến phỏng_đoán mơ_hồ ngày_càng rõ nét : Microsoft sẽ mua lại một trong số những tập_đoàn khổng_lồ của thế_giới , kiểu như Yahoo hay e Bay , trong một ngày không xa ?
“ Giá cổ_phiếu Microsoft chịu ảnh_hưởng lớn từ quyết_định của các nhà_đầu_tư tài_chính , do_đó ban lãnh_đạo mới của tập_đoàn sẽ càng chịu áp_lực nặng_nề trong cuộc cạnh_tranh với các công_ty Internet hàng_đầu thế_giới ” - trích lời Justin_Post trong bài phân_tích .
Khi_Google lấn sâu vào thị_trường phần_mềm trọng_điểm của Microsoft mỗi ngày và phát_triển ngành kinh_doanh công_cụ tìm_kiếm như vũ_bão thì việc Microsoft mua lại Yahoo càng trở_nên cấp_thiết hơn bao_giờ hết .
Theo dự_đoán , vụ mua lại , nếu thành , ngay_lập_tức sẽ đưa tập_đoàn lên vị_trí đứng đầu về doanh_thu trong lĩnh_vực kinh_doanh Internet .
Hải_Minh_Theo_Reuters

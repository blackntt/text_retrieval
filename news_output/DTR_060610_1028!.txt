﻿ Ba cây chụm lại …
Cứ trong mười quảng_cáo được đăng trên mạng VietnamWork.com thì_có tới sáu vị_trí yêu_cầu ứng_viên có khả_năng làm_việc theo nhóm . Ngày_nay , kỹ_năng làm_việc nhóm ( teamwork ) đã trở_thành một trong những tố_chất quan_trọng đối_với những ứng_viên muốn thành_công .
Sự phù_hợp giữa công_ty và cá_nhân sẽ tạo ra các ngôi_sao .
Theo kết_quả nghiên_cứu của một nhóm giáo_sư trường Đại_học Havard trên một_số nhà_phân_tích chứng_khoán tài_năng , những ngôi_sao sáng chói được săn_tìm sau khi nhảy qua nhiều công_ty khác_nhau sẽ dần_dần mất đi sức hấp_dẫn và chỉ sau khoảng năm năm thì biến_mất khỏi danh_sách quan_tâm của các nhà tuyển_dụng .
“ Các công_ty thường tập_trung tuyển_dụng nhân_tài nh ư ng nh ư ng biện_pháp này th ư ờng xuyên thất_bại hay nói cách khác là lại làm vấn_đề nặng_nề h ơ n ” , giáo_sư Boris_Groyberg , trưởng nhóm nghiên_cứu cho_biết .
Chịu áp_lực từ môi_trường kinh_doanh cạnh_tranh ngày_càng gay_gắt , các công_ty ngày_càng coi_trọng tinh_thần teamwork hơn bởi họ muốn đảm_bảo một quá_trình quản_lý suôn_sẻ và thống_nhất . Không có thời_gian cho sự ngắt_quãng , sự bất_đồng , sự trì_hoãn trong công_việc bởi nó khiến cho các nỗ_lực của công_ty trong cuộc chạy_đua với các đối_thủ trở_thành lãng_phí .
Điều gì khiến bạn trở_thành người có tinh_thần đồng_đội và những yếu_tố nào làm_nên một team thành_công ? Sự nỗ_lực của mỗi cá_nhân .
- Không nên e_ngại rằng không_thể chứng_tỏ năng_lực riêng vì mọi quyết_định hay thành_quả đều của cả nhóm hay không được thừa_nhận về sự đóng_góp mang tính cá_nhân .
- Kỹ_năng tương_tác hiệu_quả với các thành_viên khác và đóng_góp vào việc tạo ra quyết_định mà không áp_đặt sự chủ_quan lên những người khác sẽ giúp bạn nâng cao hình_ảnh trong mắt sếp .
- Có_thể bạn lo_lắng rằng bạn sẽ không được chú_ý đến vì không phải là người dẫn_dắt các cuộc thảo_luận và những người khác không tìm đến bạn để xin lời khuyên . Nhưng thực_tế cho thấy một nhóm làm_việc thành_công và có sự đóng_góp của bạn sẽ được các sếp nhận ra và đền_bù xứng_đáng .
- Một thành_viên làm_việc theo nhóm cần có sự nhạy_cảm cần_thiết đối_với nhu_cầu của người khác và không đưa ra quyết_định gì trước khi tham_khảo ý_kiến của cả nhóm .
Nhóm của bạn có mục_tiêu chung nào không , hoặc có một mục_tiêu nào_đó đoàn_kết các bạn lại với nhau ?
- Nếu chưa thì đã đến lúc bắt_đầu xây_dựng cho mọi người ngôi nhà_chung rồi đó . Tìm ra các mục_tiêu riêng của từng cá_nhân cũng_như quan_điểm của từng thành_viên về mục_tiêu của cả nhóm và từng cá_nhân riêng_lẻ . Và một nhóm làm_việc tốt luôn có các mục_tiêu , chính_sách và kế_hoạch làm_việc chặt_chẽ , gắn liền với hướng đi của công_ty .
- Tạo văn_hóa làm_việc coi_trọng sự hợp_tác . Trong một môi_trường làm_việc theo nhóm , ai cũng hiểu và tin rằng mọi ý_tưởng , kế_hoạch và quyết_định bao_giờ cũng tốt_đẹp hơn nếu có sự tham_gia của cả nhóm . Nâng cao trách_nhiệm và quyền_lợi của các thành_viên cũng giúp cho họ có động_lực làm_việc tốt hơn , tập_trung hơn vào các mục_tiêu và chiến_lược cũng_như có những quyết_định táo_bạo hơn trong khi làm_việc nhóm .
Vai_trò thuộc vào sự cam_kết
- Một nhóm làm_việc chỉ có_thể tồn_tại và phát_triển khi giữa họ có mối quan_hệ tương thuộc lẫn nhau . Mỗi thành_viên đều phải hiểu rằng họ sẽ đóng_góp vào sự thành_công chung của toàn nhóm cũng_như khẳng_định giá_trị của chính họ .
- Một trong những yếu_tố giúp nhóm làm_việc hiệu_quả chính là sự cam_kết của các thành_viên . Mỗi người có trách_nhiệm cũng_như quyền_lợi riêng .
- Rất cần sự phối_hợp ăn_ý nhịp_nhàng giữa các thành_viên . ( VietnamWork.com )

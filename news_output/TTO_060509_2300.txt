﻿ Bình_Định : tuyển gấp lao_động đi làm_việc nước_ngoài
Malaysia , tuyển nam , nữ tuổi từ 18-25 , học_vấn 7/12 , công_việc : sản_xuất gạch_men , cơ_khí hàn điện , tiện , lắp_ráp điện_tử , chế_phẩm nhựa ; chi_phí đi 16,8 triệu đồng , thu_nhập bình_quân 3,5 - 4 triệu đồng / tháng ; được hỗ_trợ vốn vay ngân_hàng tối_đa là 20 triệu đồng , được đào_tạo nghề và hỗ_trợ chỗ ở miễn_phí trong thời_gian học_tập .
Nhật_Bản , tuyển nam , nữ tuổi từ 19-32 , học_vấn 9/12 , công_việc : lắp_ráp điện_tử , cơ_khí hàn điện , tiện , dệt , in ; chi_phí đi 27,2 triệu đồng và 160 triệu đồng ( tiền thế_chân để thực_hiện hợp_đồng , về nước sẽ nhận lại ) , thu_nhập 10,4 triệu đồng / tháng , được ngân_hàng cho vay 80% mức tổng_chi phí .
Đài_Loan , tuyển nam , nữ tuổi từ 20-30 , học_vấn 9/12 , nam_cao từ 1,67 m , nặng 50kg ; nữ_cao 1,54 m , nặng 42kg trở_lên ; công_việc : lắp_ráp điện_tử , cơ_khí hàn , tiện ; chi_phí đi 72 triệu đồng , thu_nhập 8 triệu đồng / tháng .
Hàn_Quốc , tuyển nam , nữ tuổi từ 20-35 , học_vấn 9/12 , nam_cao 1,65 m , nặng 50kg ; nữ_cao 1,55 m , nặng 45kg ; đối_tượng là con gia_đình thương_binh và liệt_sĩ , bộ_đội xuất_ngũ , ưu_tiên lao_động có chứng_chỉ nghề cơ_khí , xây_dựng , nông_nghiệp .
Liên_hệ Trung_tâm Dịch_vụ việc_làm Bình_Định , số 72 B_Tây_Sơn , TP Qui_Nhơn , tỉnh Bình_Định . ĐT : 056 . 646509 .
PHẠM_PHƯƠNG

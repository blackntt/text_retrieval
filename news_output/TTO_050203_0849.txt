﻿ Phòng cháy trong tết như_thế_nào ?
- 90% các vụ cháy xảy_ra do nguyên_nhân chập điện . Do_vậy , chúng_tôi lưu_ý người_dân một_số vấn_đề cơ_bản sau đây : với các cơ_sở sản_xuất , kinh_doanh , dịch_vụ trước khi nghỉ tết cần thực_hiện nghiêm_túc những kiến_nghị , yêu_cầu về phòng cháy chữa_cháy mà chúng_tôi đã thông_báo ; tăng_cường tự kiểm_tra để phát_hiện , khắc_phục ngay những sơ_hở dễ gây cháy ; vào những ngày tết khi_không hoạt_động nhất_thiết phải cắt điện nguồn .
Với những nơi tập_trung đông người như chợ , siêu_thị , trung_tâm vui_chơi … ngoài việc tăng_cường kiểm_tra về an_toàn cháy nổ , cần chú_ý đặc_biệt cho các lối_thoát hiểm , thoát nạn ; tổ_chức tập_huấn các phương_án xử_lý khi có sự_cố .
Còn với khu dân_cư ?
- Ở khu dân_cư - nơi thường xảy_ra cháy trong dịp tết - yêu_cầu người_dân hết_sức chú_ý , lưu_tâm trong việc sử_dụng điện , tránh để quá_tải , chạm , chập điện . Trong đun_nấu phải cẩn_thận như_không giao cho trẻ_em , không được bỏ đi khi chưa nấu xong đề_phòng bỏ quên … Chúng_tôi cũng cảnh_báo người_dân là phải thận_trọng , hạn_chế trong việc đốt nhang đèn , vàng_mã trong cúng_quảy , vì đây là một trong những nguyên_nhân dễ gây cháy . Tuyệt_đối không được sang chiết gas trái_phép , đốt pháo , để trẻ_em nghịch lửa … Tất_nhiên phải trang_bị các bình chữa_cháy cá_nhân trong mỗi gia_đình .
Cảm_ơn ông .
LÊ_ANH Đủ thực_hiện

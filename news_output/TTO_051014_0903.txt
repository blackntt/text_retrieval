﻿ Cần khả_năng hay bằng_cấp ?
Số còn lại phải trầy_trật tìm việc với đủ các nghề dạy kèm , bán sách dạo , tiếp_thị , bán hàng ... để kiếm sống , dù không ít người có ít_nhất 1 tấm bằng ĐH chính_quy và 3-4 chứng_chỉ đi kèm .
Hai bằng ĐH ... đi bán sách
Thực_tế , có rất nhiều sinh_viên có bằng ĐH mà vẫn thất_nghiệp . Chẳng_hạn bạn L . H . H , hiện là nhân_viên của nhà_sách Bách_Khoa ( Q.Gò Vấp , TPHCM ) .
Theo lời của cựu sinh_viên Trường ĐH Luật TP.HCM này , ngoài bằng cử_nhân chính quy_luật , anh còn có thêm bằng tại_chức Anh_văn của Trường ĐH Khoa_học_xã_hội và Nhân_văn TP.HCM , chứng_chỉ vi_tính , luật kinh_tế . Nhưng ra trường , đến nay đã 3 năm vẫn chưa có việc_làm ổn_định .
“ Lấy_ngắn_nuôi_dài , mình xin vào làm ca ở nhà_sách này , thu_nhập mỗi tháng 800.000 đồng . Mấy tháng nay , buổi tối mình và mấy đứa bạn cùng cảnh bán sách giảm_giá trên đường Nguyễn_Đình_Chiểu ” , Hùng kể . Vui_miệng , Hùng còn nói cho chúng_tôi biết nhà_sách này chỉ có 10 nhân_viên , thì sáu trong số họ đã tốt_nghiệp ĐH , trong đó có một người hiện làm bảo_vệ và giữ xe .
Nếu biết trước như_thế_này ...
Tìm người có nhiều bằng_cấp nhưng không xin được_việc quả_thật không khó . Tại_Chi cục Thuế Q.Bình Thạnh - TP.HCM , ngày 10-10 , chúng_tôi đã tiếp_xúc với không dưới 30 người báo_cáo thuế cho các công_ty tư_nhân . Họ là những người không nghề_nghiệp cụ_thể nào .
Chị Vũ_Thị_Hiền_Anh , một trong những người đó , cho_biết chị tốt_nghiệp Trường ĐH Kinh_tế TP.HCM năm 2002 , từ đó đến nay chưa đi làm cho một công_ty nào cả , mỗi tháng nhận báo_cáo thuế cho 3 công_ty , mỗi công_ty trả 400.000 đồng/tháng . “ Nếu biết như_thế_này , tôi chẳng phải chạy_đôn_chạy_đáo học cho được chứng_chỉ kế_toán_trưởng làm_gì cho mệt ” , chị than .
Chỉ đủ qua vòng sơ_khảo
Bây_giờ thì những người như Hiền_Anh mới hiểu ra rằng không phải cứ tốt_nghiệp trường “ ngon ” , có bằng_cấp nhiều là sẽ dễ_dàng xin việc . Thế_nhưng , đa_số sinh_viên còn ngồi trên ghế nhà_trường thì chưa nhận ra “ chân_lý ” ấy . Họ chỉ biết học để có tấm bằng mà chưa chú_trọng rèn khả_năng cũng_như bản_lĩnh sống để khi ra trường dễ kiếm việc phù_hợp chuyên_ngành của mình .
Nhận_xét về thực_trạng đáng lo_ngại này , bà Văn_Thị_Ngọc_Lan , Giám_đốc Trung_tâm Nghiên_cứu Xã_hội_học , Viện_Khoa học Xã_hội vùng Nam_Bộ , nói : “ Thị_trường lao_động đòi_hỏi cạnh_tranh cao , sinh_viên cần nhiều bằng_cấp cũng đúng . Đáng tiếc là có bằng_cấp chưa đủ để cạnh_tranh , chỉ đủ để qua vòng sơ_khảo mà thôi ” .
Nhà tuyển_dụng chú_trọng khả_năng
Ông Nguyễn_Sơn , Giám_đốc Công_ty Hoàng_Sơn_Hòa ( Q . 3 , TP.HCM ) : “ Khi đọc phần ứng_viên tự giới_thiệu chúng_tôi vui_mừng lắm . Sinh_viên nào cũng có bằng ĐH chính_quy với đủ những chứng_chỉ Anh_văn , vi_tính . Thậm_chí nhiều người còn có 2 bằng ĐH ... Nhưng thực_tế tuyển_dụng , chúng_tôi quá thất_vọng ” .
Ông Võ_Văn_Trà , Giám_đốc Công_ty Việt_Nhật ( 21A Quang_Trung , Gò_Vấp ) : “ Để tránh tình_trạng tuyển không được người , chúng_tôi nhận sinh_viên thực_tập và giữ lại nếu thấy có khả_năng ” .
Tiến_sĩ Nguyễn_Trọng , Phó_Chủ tịch Hội_đồng_quản_trị Công_ty Vietsoft_Vision : “ Chúng_tôi muốn nhận những người có khả_năng làm_việc thực_sự . Không phải bằng_cấp không quan_trọng , nhưng quan_trọng hơn là khả_năng có tương_xứng với bằng_cấp hay không ! ” .
Theo Người_Lao_Động

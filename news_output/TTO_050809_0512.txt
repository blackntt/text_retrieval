﻿ Hãy làm những bộ phim được đông_đảo người xem chấp_nhận
Cảnh trong phim Ảo_ảnh được chọn chiếu trong giờ vàng nhưng ... quá dở ! Ảnh tư_liệu
Tôi đồng_ý với đạo_diễn Lê_Hoàng rằng các nhà làm phim có_thể định_hướng cho bộ phim của mình chỉ nhằm phục_vụ một tầng_lớp nào_đó .
Tôi cũng đồng_ý là “ ... không_thể nói người đạo_diễn muốn làm phim cho công_nhân xem xấu_xa hơn người đạo_diễn muốn làm phim cho giáo_sư xem ... ” , nhưng chẳng_lẽ vì bộ phim làm cho công_nhân xem mà nó được phép cẩu_thả , vụng_về và sai_sót hay_sao ? Như_thế không phải là có tinh_thần phục_vụ tầng_lớp lao_động mà là xem_thường họ .
Phim đâu cần_thiết lúc_nào cũng quá cao_siêu , quá trí_tuệ đến_nỗi chỉ có giáo_sư mới hiểu được . Khán_giả muốn xem những bộ phim sâu_sắc làm họ phải nghĩ_ngợi , nhưng cũng có lúc họ chỉ muốn xem những bộ phim đơn_thuần chỉ để giải_trí , xả stress .
Các phim Hàn_Quốc thành_công ở VN được nhiều người say_mê theo_dõi cũng đâu có chuyển_tải điều gì quá cao_siêu đâu nhưng cả tầng_lớp lao_động và trí_thức đều có_thể hiểu được , cảm được .
Hơn_nữa có nhà làm phim nào trước khi tung bộ phim của mình ra lại thông_báo trước rằng đây chỉ là phim cho tầng_lớp lao_động chân_tay xem đâu . Như_vậy những_ai không nằm trong diện được phục_vụ nếu có lỡ xem thì : “ A lê hấp ! Miễn bình_luận nhé ! Vì tôi đâu có làm phim cho ông xem đâu . Ông tự đem đến thất_vọng và buồn_bực cho mình đó chứ ! ” .
CẨM_TÚ ( Q.BT , TP.HCM )

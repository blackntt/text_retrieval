﻿ Con_gái Madonna bị nụ_hôn đồng_tính của mẹ ám_ảnh
Hẳn các bạn vẫn còn nhớ nụ_hôn gây shock của Britney và Madonna tại lễ trao giải VMA . Mới_đây , Madonna vừa thú_nhận rằng nụ_hôn tai_tiếng hồi đó vẫn còn để lại cho cô những câu_hỏi dai_dẳng .
Không phải như chúng_ta nghĩ , đó không phải là những câu_hỏi của báo_chí hay các nhà bình_luận , mà là câu_hỏi của con_gái Madge , bé Lourdes .
Madonna vừa kể với Out – tạp_chí dành cho người đồng_tính về việc cô con_gái 9 tuổi của cô đã bị ám_ảnh về các mối quan_hệ đồng_tính sau sự_kiện Madonna – Britney ra_sao . Bé_Lourdes đã từng hỏi mẹ : “ Mẹ có biết người_ta nói mẹ là người đồng_tính không ? ”
Nữ_hoàng nhạc Pop đã giải_thích với cô con_gái bé_nhỏ rằng nụ_hôn trên sân_khấu lúc đó không mang tính_chất giới_tính : “ Mẹ là một ngôi_sao ca_nhạc kiêm vai_trò người_mẹ , Britney lúc đó mới chỉ là một cô_gái mới trưởng_thành . Mẹ hôn Brits để truyền thêm sinh_lực cho cô ấy ” .
Cùng trong bài phỏng_vấn đó , Madge cũng nói_bóng_gió rằng mối bất_hòa giữa cô với Elton_John còn lâu mới kết_thúc . “ Ngay trước khi tổ_chức đám_cưới , Elton_John đã gửi cho tôi một bức thư xin_lỗi về cơn giận_dữ lần trước . Nhưng có_vẻ như ông_ta vẫn rất bực_bội và tôi bị trở_thành bia đỡ . Elton_John thật không hòa_nhã và lịch_sự một_chút nào ” .
Có_vẻ như chúng_ta lại sắp được chứng_kiến một cuộc khẩu_chiến nữa . Hãy chờ xem ! BM Theo Ananova

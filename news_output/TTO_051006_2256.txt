﻿ Lũ_lụt tại Trung_Quốc , 20 người chết và mất_tích
Mưa lớn trong_suốt tuần qua tại tỉnh Thiểm_Tây ( từ 24-9 đến 2-10 ) đã gây ra trận lụt lớn nhất tại tỉnh này trong vòng một thập_kỷ vừa_qua tại sông Vị_Hà và tạo ra lũ_lụt nghiêm_trọng trên sông Hàn_Giang . Toàn_bộ nước của những khúc sông đi qua tỉnh Thiểm_Tây đã buộc 359.000 người sống tại khu_vực này phải di_tản .
Nước sông dâng cao gây lũ_lụt đã gây hư_hại đường_xá , đường_ray , dây_cáp và hệ_thống thủy_lợi của tỉnh , ước_tính thiệt_hại khoảng 239 triệu USD .
Hồ_Bắc , một tỉnh ở trung_tâm Trung_Quốc cũng đang phải hứng_chịu những trận mưa rất lớn và đang phải đối_mặt với nguy_cơ lũ_lụt trên diện rộng . Mực nước của đoạn sông Hàn_Giang đi qua tỉnh Hồ_Bắc đã vượt lên trên mức báo_động .
Trong khi đó cơn bão Long_Vương tại miền tây nam quốc_gia này cũng đã làm 65 người thiệt_mạng trong đó có 50 sĩ_quan cảnh_sát bị chết và 36 đồng_đội khác bị mất_tích trong một vụ trượt đất .
Theo Tân_Hoa_Xã thì_có khoảng 11 người khác cũng đã bị mất_tích trong cơn bão Long_Vương vừa_qua . Sau khi bị rơi vào tình_trạng hỗn_loạn khi bão Long_Vương đổ_bộ vào thì hôm_nay toàn_bộ hệ_thống điện , cáp ... trường_học , đường_sá ... tại tỉnh Phúc_Kiến đã được khôi_phục và mở trở_lại .
KINH_LUÂN ( Theo AFP )

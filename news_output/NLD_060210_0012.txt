﻿ Chọn đơn_vị thử tải , quan_trắc
( NLĐ ) - Phó_Chủ tịch UBND_TP Nguyễn_Văn_Đua vừa có ý_kiến trong việc chọn đơn_vị thử tải cầu Văn_Thánh 2 và thực_hiện quan_trắc theo_dõi chuyển_vị , lún đối_với công_trình đường Nguyễn_Hữu_Cảnh ( quận 1 và Bình_Thạnh ) .
TP chấp_thuận cho Sở GTCC chỉ_định Viện_Khoa học - Công_nghệ ( thuộc Bộ Xây dựng ) thực_hiện công_tác này . Dự_án đường Nguyễn_Hữu_Cảnh bắt_đầu được xây_dựng từ năm 1997 , đưa vào sử_dụng năm 2001 thì đã bị hư_hỏng nhiều hạng_mục . Riêng hạng_mục cầu Văn_Thánh 2 bị hư_hỏng kéo_dài nhiều nhất và mới được sửa_chữa xong phần cơ_bản vào cuối tháng 1 vừa_qua .
Ng . Thạnh

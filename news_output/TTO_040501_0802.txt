﻿ Cục Thuế_Cần_Thơ thành_lập đường_dây_nóng
Như_vậy , từ nay mọi đối_tượng nộp thuế trên địa_bàn TP Cần_Thơ nếu bị cán_bộ , công_chức ngành thuế sách_nhiễu , gây phiền_hà hay gợi_ý đòi bồi_dưỡng , ăn_nhậu ... hãy điện_thoại trực_tiếp theo số : 071.812357 và 0918625555 , bất_cứ lúc_nào .
UBND tỉnh Bà Rịa - Vũng_Tàu vừa ban_hành chỉ_thị số 23/2004/CT-UB về việc tăng_cường thu_hút đầu_tư trực_tiếp nước_ngoài .
Theo đó , giao cho các cơ_quan_chức_năng phải tập_trung xử_lý nghiêm các trường_hợp sách_nhiễu , cửa_quyền , vô_trách_nhiệm , gây phiền_hà đến các nhà_đầu_tư khi tới nghiên_cứu , tìm_hiểu và triển_khai các dự_án trên địa_bàn toàn tỉnh . Đồng_thời tỉnh cũng khuyến_khích các nhà_đầu_tư báo_cáo kịp_thời với lãnh_đạo tỉnh khi gặp phải tiêu_cực của cán_bộ , công_chức có liên_quan đến quá_trình triển_khai các dự_án đầu_tư .
HỒ_VĂN - NGỌC_LUẬN

﻿ Mariah_Carey “ chiến_đấu ” với tuổi già
Nữ danh_ca Mariah_Carey có_thể là người có cặp chân đẹp nhất nước Mỹ , nhưng có một sự_thật mà không ai phủ_nhận được rằng , mặt cô ấy ngày_càng xuất_hiện nhiều các nếp nhăn .
Khi bước qua tuổi 35 , khó ai lẩn_trốn được những vết “ chân_chim ” trên mặt . Thế_nhưng đối_với những ngôi_sao Hollywood thì điều này thật khó chấp_nhận . Vậy_Mariah_Carey đã làm cách nào ?
Với sự giúp_đỡ của chuyên_gia dinh_dưỡng của mình , Mariah_Carey đang bắt_đầu theo một chế_độ ăn_uống vô_cùng nghiêm_ngặt với 3 lần/tuần cô chỉ ăn những loại quả có màu đỏ tía , ví_dụ như nho hay mận chẳng_hạn .
Bác sỹ của Mariah_Carey đã khuyên cô rằng , những loại quả đó có những vitamin và vi_chất khá quan_trọng giúp làm chậm lại quá_trình lão_hóa của da và tất_nhiên , Mariah_Carey thà chịu để_bụng đói chứ không_thể để gương_mặt mình nhăn_nheo được .
Đây không phải là lần đầu_tiên Mariah_Carey ép mình vào “ khuôn_khổ ” . Hồi đầu năm , sau khi tăng cân vòn_vọt và không còn mặc vừa bộ váy nào , nữ danh_ca này đã chỉ ăn súp và một_ít cá_da_trơn đến vài tháng trời .
Hiệu_quả thì ai cũng thấy là Mariah_Carey trở_nên “ eo ót ” hơn hẳn nhưng sau đó chính Mariah_Carey cũng phải thốt lên rằng : “ Tôi quá sợ khi nghĩ đến súp và cá , nó làm tôi kiệt_sức và chẳng bao_giờ muốn bước vào bàn ăn cả ” .
Vĩnh_Ngọc_Theo_Hollywoodrag / skins

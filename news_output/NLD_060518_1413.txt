﻿ Máy_tính Trung_Quốc nhắm đến DN vừa và nhỏ Việt_Nam
Ngày 17-5 , tập_đoàn sản_xuất máy_vi_tính Lenovo của Trung_Quốc đã chính_thức ra_mắt dòng sản_phẩm Lenovo 3000 với nhiều kiểu máy_để_bàn và xách tay có công_nghệ vi_xử_lý hiện_đại với đối tuợng nhắm đến là thị_trường doanh_nghiệp vừa và nhỏ Việt_Nam .
Ông Deepak_Advani , Phó_Chủ tịch Hội_đồng_quản_trị , Giám_đốc Marketing , cho_biết dòng sản_phẩm Lenovo 3000 đã tích_hợp những công_cụ hiện_đại với các tính_năng như đơn_giản_hóa kết_nối mạng và cập nhập tự_động để giúp giảm chi_phí về công_nghệ_thông_tin cho các doanh_nghiệp .
Với bộ công_cụ đảm_bảo hiện suất máy Lenovo_Care_Productivity , việc bảo_trì , kiểm_soát và vận_hành dòng sản_phẩm Lenovo 3000 của người sử_dụng được thay_thế bằng các công_cụ tự_động hiện_đại như tự nhận_dạng và cập_nhật những phần_mềm riêng của Lenovo , dễ_dàng thực_hiện những tác_vụ bảo_dưỡng để đảm_bảo hiệu_suất hoạt_động tốt nhất .
Theo TTXVN

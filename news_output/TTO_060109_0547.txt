﻿ Tôi đi đăng_ký hộ_khẩu
Tôi cũng_như bao người khác phải đi_tới_đi_lui , ký tới ký lui nhiều lần mà vẫn không được Công_an TP.HCM chấp_nhận chỉ vì giấy xác_nhận không đủ bốn nội_dung theo qui_định . Thật_ra việc này đâu có khó_khăn gì nếu chúng_ta có một mẫu thống_nhất ngay từ đầu .
Để có được tờ giấy xác_nhận hoàn_chỉnh , nhiều nơi người_dân phải ký hợp_đồng đo vẽ tại địa_chính phường ( họ có hợp_đồng soạn sẵn của một công_ty TNHH ) với giá 300.000 đồng . Bản_vẽ này không có giá_trị khi làm hợp_thức_hóa nhà_đất , thật lãng_phí !
Hồ_sơ của tôi được Công_an TP chấp_nhận ngày 2-12-2005 và cấp giấy hẹn ngày 16-12 đến lấy hồ_sơ . Chiều 16-12-2005 tôi tới thì được hẹn đến ngày 26 rồi 30-12-2005 ... Thế_là tôi mất gần hai tháng trời mà chưa đâu vào đâu . Không phải anh_em công_an không tích_cực mà cách làm có gì đó tôi thấy không ổn .
Tôi đề_nghị việc trả hồ_sơ nên hẹn dứt_khoát một ngày nào_đó ( tối_đa hai lần ) và trả theo từng đợt nhận hồ_sơ . Tới ngày hẹn , người phụ_trách trả hồ_sơ kêu tên từng người lên nhận thì đâu có cảnh chen_lấn , chầu_chực ngày này qua_ngày khác .
Đợi người_dân nộp phiếu hẹn rồi mới đi lục , có người chờ nửa tiếng đã có , nhưng cũng có nhiều người đợi đến hết giờ thì bị trả phiếu hẹn ngày khác . Thật nản_chí vô_cùng !
Rất mong ban giám_đốc Công_an TP và các ngành chức_năng sớm tìm ra giải_pháp hữu_hiệu vừa giải_quyết tốt công_việc , vừa giúp dân tránh được những phiền_hà rắc_rối và sớm ổn_định cuộc_sống .
dungdangvn2000 @ ... ( TP.HCM )

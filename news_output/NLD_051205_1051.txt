﻿ Tuần_lễ phim Việt_Nam tại Washington
Áp_phích phim Mê_Thảo tại Mỹ_Tuần lễ phim Việt_Nam tại thủ_đô Washington đã bắt_đầu tối 2-12 với bộ phim " Mê_Thảo - Thời xa_vắng " được trình_chiếu tại thính_phòng Mayer nằm trong khu Bảo_tàng Nghệ_thuật Free_Arthur M . Sacker , bảo_tàng quốc_gia về nghệ_thuật châu Á của Mỹ tại Washington .
Cũng với " Mê_Thảo - Thời xa_vắng " , 4 bộ phim_truyện khác cũng sẽ được trình_chiếu trong tuần_lễ phim , kéo_dài từ ngày 2-12 đến 18-12 , gồm " Đời cát " , " Vua bãi rác " , " Của_rơi " và " Thung_lũng hoang_vắng " .
Tuần_lễ phim do Đại_sứ_quán Việt_Nam tại Mỹ và Cục điện_ảnh Việt_Nam đồng tổ_chức với sự trợ_giúp kinh_phí của Quỹ_Ford_Foundation ( Mỹ ) .
Trước mỗi phim chính , khán_giả còn được xem bộ phim_tài_liệu " Những người bán đồ gốm " và phim ngắn " Hành_trình xích_lô đêm " hoặc được mời đi xem gian_hàng trưng_bày đồ gốm Việt_Nam .
Sau đêm chiếu cuối_cùng 18-12 , khán_giả sẽ được trò_chuyện , giao_lưu với nữ đạo_diễn Phạm_Nhuệ_Giang về bộ phim " Thung_lũng hoang_vắng " của bà . Ban tổ_chức bảo_tàng cho_biết các phim Việt_Nam hiếm khi được trình_chiếu tại Mỹ , nhất_là tại trung_tâm văn_hóa châu Á này , vì_vậy đây là một cơ_hội tốt cho khán_giả Mỹ và những_ai quan_tâm đến lịch_sử , văn_hóa và con_người Việt_Nam .
( Theo TTXVN )

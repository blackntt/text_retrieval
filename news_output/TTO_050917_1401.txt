﻿ Xã Thạnh_Tân , huyện Tân_Phước , tỉnh Tiền_Giang : Chia_chác đất công
- Có sự bao_che có hệ_thống từ huyện đến xã nên khó phát_hiện . Trưởng_Phòng_Tài nguyên - môi_trường cùng_với ba cán_bộ dưới quyền làm sai rồi ra_sức che_giấu cho nhau , cho_nên chúng_tôi không phát_hiện được . May là nhờ có sự phản_ánh của dư_luận chúng_tôi mới cho thanh_tra ở Thạnh_Tân . Từ đây mới “ lòi ” ra bốn anh này có tiêu_cực từ lâu . Riêng ông Nguyễn_Văn_Lo đã nhiều lần sai_phạm bị kỷ_luật rồi , nên lần này huyện phải cương_quyết cách_chức thôi .
Dư_luận cũng phản_ánh một_số cán_bộ lãnh_đạo của huyện Tân_Phước có rất nhiều đất , có người sở_hữu hàng chục hecta …
- Chúng_tôi có nghe dư_luận phản_ánh tiêu_cực đất_đai của một_số cán_bộ lãnh_đạo huyện . Nhưng anh_em công_tác ở các cơ_quan khác_nhau , nên phải làm từ_từ . Tới đây chúng_tôi sẽ tiếp_tục cho thanh_tra - kiểm_tra . Bất_kỳ ai sai_phạm cũng đều bị xử_lý nghiêm , không có chuyện bao_che , vị_nể .
VÂN_TRƯỜNG

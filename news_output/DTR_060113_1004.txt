﻿ Amoroso sẽ chơi cho AC Milan ?
Theo báo_chí Italia , tiền_đạo Amoroso mới_đây đã có_mặt tại Milan . Anh sẽ trải qua cuộc kiểm_tra sức_khỏe trước khi đặt bút ký vào những điều_khoản cuối_cùng trong hợp_đồng với Milan . Cầu_thủ này sẽ thay_thế vị_trí của Vieri vừa chuyển sang Monaco .
Theo thông_tin ban_đầu , hợp_đồng sẽ có thời_hạn 18 tháng và trị_giá khoảng 1,4 triệu euro . Trước đó , Amoroso đã chính_thức trở_thành một cầu_thủ tự_do trên thị_trường chuyển_nhượng , khi đã hết hợp_đồng với CLB cũ là Sao_Paulo .
Như_vậy , anh sẽ thay_thế Vieri để cạnh_tranh một vị_trí trong đội_hình chính , cùng_với 3 siêu tiền_đạo khác là Shevchenko , Gilardino và Inzaghi .
Ngay trước khi bay sang Milan , tiền_đạo 31 tuổi này đã từ_chối ký vào bản hợp_đồng mới có thời_hạn 3 năm với Sao_Paulo . " Đây là cơ_hội lớn của cuộc_đời tôi " , anh đã nói như_vậy trong cuộc họp_báo trước khi ra_đi .
Amoroso đã từng có 5 mùa bóng chơi tại Serie A trong các màu áo của Udinese và Parma , với tổng_cộng 49 bàn thắng .
Những thành_tích nổi_bật nhất của anh là : chiếc Cúp VĐ Copa_America 1999 với ĐTQG Brasil , Vua_phá_lưới Serie A mùa bóng 98/99 , Vua_phá_lưới Bundesliga 2002 , Vô_địch Bundesliga với Dortmund cùng năm đó , và mới_đây là chiếc Cúp vô_địch các CLB thế_giới do FIFA tổ_chức .
Maicon sẽ chuyển tới Inter_Milan
Inter_Milan vừa hoàn_tất những thủ_tục chuyển_nhượng hậu_vệ cánh của AS Monaco_Maicon với một hợp_đồng trị_giá 4 triệu bảng .
Hậu_vệ cánh người Brazil này đã có_mặt tại Milan ngày hôm_qua để ký hợp_đồng có thời_hạn bốn năm với Inter_Milan , hợp_đồng này sẽ có hiệu_lực từ tháng Bảy sắp tới .
Maicon đã phải trải qua một cuộc kiểm_tra sức_khoẻ trước khi trở_lại Monaco , Anh là một trong những hậu_vệ cánh tài_năng hàng_đầu thế_giới và hy_vọng sẽ là sự thay_thế thích_đáng cho đội_trưởng Javier_Zanetti trong tương_lai .
Hậu_vệ 24 tuổi này bắt_đầu được biết đến hai năm về trước trong màu áo Cruzeiro . Lối chơi tốc_độ , không ngại va_chạm , khỏe_mạnh , kiểm_soát bóng tốt và khả_năng qua người hiệu_quả đã thu_hút sự chú_ý của Monaco , và đội bóng này đã quyết_định chiêu_mộ cầu_thủ này vào cuối mùa bóng trước .
Juventus biểu_lộ mong_muốn có Caracciolo
Tổng GĐ Juventus_Luciano_Moggi đã thông_báo với CT Palermo_Maurizio_Zamparini rằng hiện họ muốn thảo_luận về Caracciolo về việc chuyển_nhượng cầu_thủ này cũng_như một_số cầu_thủ khác của Palermo .
Cùng_với Caracciolo , Juve cũng đang rất quan_tâm đến Andrea_Barzagli , Simone_Barone và Fabio_Grosso . Về phần mình Palermo cũng tỏ ra chú_ý đến Manuele_Blasi và Fabrizio_Miccoli .
Sao_Paulo phủ_nhận tin_đồn về Lugano
Mặc_dù trong thời_gian vừa_qua có rất nhiều thông_tin cho rằng Milan đã đạt được thoả_thuận với hậu_vệ người Urugoay_Diego_Lugano của CLB Sao_Paulo nhưng mới_đây Chủ_tịch của CLB này ông Marcelo_Portugal_Gouvea đã phát_biểu trước giới truyền_thông rằng Milan vẫn chưa có bất_kỳ động_thái nào liên_quan đến anh .
Ông nói : " Họ vẫn chưa đưa ra lời đề_nghị chính_thức nào về trường_hợp Lugano . Nếu có thì Leonardo ( cựu cầu_thủ của Milan , Sao_Paolo và hiện đang là người đại_diện chuyển_nhượng của Milan tại Nam_Mỹ ) đã tiếp_xúc với chúng_tôi " .
Ngoài_ra , ông cũng cho_biết CLB của TBN Sevilla cũng đang quan_tâm đến cầu_thủ này nhằm củng_cố hàng phòng_ngự yếu_kém của mình . Tuy_nhiên , rất có_thể đây chỉ là động_thái tung hoả_mù nhằm nâng_giá cầu_thủ trên thị_trường chuyển_nhượng mà thôi . Theo ACM . VN / JFC

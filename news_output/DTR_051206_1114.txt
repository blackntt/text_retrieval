﻿ " Cách đây 5 năm , tôi sẽ không để yên cho Hiền_Thục "
Với các ca_khúc Tóc_mây , với tà áo_dài màu hồng duyên_dáng , mái_tóc xõa ngang vai ... Thanh_Thảo giờ_đây đã khác , cô đằm_thắm và nhu_mì hơn . Thảo trầm tính và khép mình lại từ suy_nghĩ đến cách hát , cô hát một_cách nhẹ_nhàng thoải_mái , đôi_lúc cũng nũng_nịu làm_duyên .
Đang được mọi người biết đến và yêu_thích với hình_tượng " búpbê hiphop " , tại_sao chị lại chuyển qua hình_tượng nhu_mì , hiền_dịu với những bản tình_ca vượt thời_gian ?
Khi thực_hiện CD với những tình_khúc vượt thời_gian , bản_thân tôi rất ý_thức việc tôn_trọng ý_nghĩa của từng ca_khúc . Hình_tượng của tôi qua đó cũng chú_trọng nhiều hơn cho phù_hợp với tính_chất và màu_sắc của album . Điều đó thể_hiện văn_hóa của người nghệ_sĩ , nắm_bắt những yếu_tố tưởng_chừng đơn_giản nhưng cũng phải thận_trọng vì nó ảnh_hưởng đến mức_độ thành_công của mình .
Từ một ca_sĩ đầy cá_tính , chị lại đưa ra một hình_tượng mới khá dễ_thương , thực_chất bên trong chị có sự thay_đổi như_vậy hay đó chỉ là hình_thức cần có để tạo sự mới_lạ ?
Khi người_ta trẻ_con , người_ta cho_phép mình có quyền làm những việc khá bản_năng và nông_nổi . Sự chững_chạc , từng_trải sẽ đến theo ngày_tháng . Tôi vẫn là Thanh_Thảo đầy cá_tính , mạnh_mẽ trong công_việc , cuộc_sống nhưng không cho_phép mình có những hành_động thiếu suy_nghĩ chín_chắn . Bản_chất dễ_thương hay đáng ghét không_thể xuất_hiện chỉ bằng hiện_tượng bên ngoài mà xuất_phát từ bên trong con_người mới thật_sự có giá_trị .
Chị có_thể kể vài chuyện vui , vài điều độc_đáo và ấn_tượng nhất trong quá_trình làm album " Bảy ngày đợi_mong " ?
Người đầu_tiên khuyến_khích và chọn cho tôi những ca_khúc trong CD là ca_sĩ Quang_Dũng . Nếu_không có sự khích_lệ này , có_lẽ tôi đã không tự_tin thực_hiện . Một năm trước , CD đã được chuẩn_bị và các nhạc_sĩ hòa_âm đã rất tâm_đắc cho CD này vì tôi hát nghe tốt và lạ . Điều này làm tôi yên_tâm phần_nào .
Thu âm xong , nghe lại lần đầu vẫn chưa " đã " lắm nên tôi quyết_định dời thời_điểm phát_hành muộn mất 4 tháng để thu âm lại . Suốt thời_gian đó , nhạc_sĩ Quốc_An và Phương_Uyên luôn theo sát bên tôi trong phòng thu . Chú Viết_Tân cũng mix khá kỹ cho_đến lúc tôi thực_sự thật ưng_ý . Mọi người ai cũng quyết_tâm giúp tôi vì hiểu rằng với tôi , đây là CD " để đời " .
Phần hình_thức cũng là ý_tưởng ấp_ủ của Công_Trí , tất_cả đạo_cụ và việc may áo_dài đều do anh tận_tình lo_liệu . Rồi việc phát_hiện ra giọng hát của Lê_Hiếu rất hợp cho bản song_ca cũng gây cho tôi rất nhiều thú_vị .
Chị mong_muốn nhận được kết_quả gì trong album này ?
Hiện_nay album nhận được rất nhiều lời khen_tặng , mọi người cũng công_nhận sự cố_gắng và tâm_huyết của tôi qua CD , đó là nguồn động_viên lớn nhất . Hy_vọng lần này tôi sẽ có thêm thật nhiều khá_giả yêu_mến mình .
Chị đã yên_ổn với gia_đình , công_việc , vậy còn điều gì chưa làm hay muốn làm tiếp ?
Album_Búp bê happy cũng sắp hoàn_tất , có_lẽ sẽ phát_hành trong tháng này và tôi sẽ đón năm mới bằng DVD " Anh đã đến bên đời " . Album là lời cảm_ơn của tôi tới ông_xã đó ...
Chị là người bận_rộn , lại còn phải quản_lý Andy_Spa nữa . Ông_xã chị nghĩ sao về sự tất_bật của chị ?
Nếu nói về sự bận_rộn , chắc_chắn anh_ấy còn bận hơn tôi và đi công_tác xa liên_tục . Nhưng hạnh_phúc vẫn còn đó nếu hai người biết thông_cảm và sắp_xếp dành thời_gian cho nhau .
Chị nghĩ sao nếu một ngày chị bị các ca_sĩ trẻ , đẹp " tiếm_ngôi " ?
Đã là quy_luật thì mình cũng phải_biết chấp_nhận . Mọi sự cố_gắng sẽ được đền_bù một_cách xứng_đáng . Nếu mình vẫn còn yêu nghề và hết_lòng với khán_giả thì họ sẽ không_bao_giờ quay lưng lại với mình . Tôi đã hát được 10 năm , một_nửa thời_gian đó là vinh_quang và sự nổi_tiếng . Đến nay , khán_giả vẫn còn yêu_mến , ở bên_cạnh tôi , tôi không còn mong_muốn nào hơn .
Chị từng có xích_mích với Hiền_Thục . Hiện_nay , quan_hệ giữa hai người thế_nào ?
Nếu là Thanh_Thảo cách đây 5 năm , chắc_chắn tôi sẽ không để yên vậy đâu . Hiền_Thục nhỏ_tuổi hơn nên tôi cũng chẳng cần quá nhỏ_mọn với cô ấy . Chuyện cô ấy cư_xử sai hay đúng với tôi thì nhiều người trong giới và cả khán_giả đều hiểu rất rõ , không cần phải khẩu_chiến với nhau trên mặt báo để bị cười_chê . Một ca_sĩ trẻ điều quan_trọng nhất_là phải học bài_học về đạo_đức nghề_nghiệp thì mới thành_công được .
Những lúc mệt_mỏi , bế_tắc , chị thường làm_gì ?
Viết nhật_ký tâm_sự với chính mình , sau đó điện_thoại cho một người bạn_thân đáng tin_cậy nhất để được chia_sẻ .
Kế_hoạch sắp tới của chị sau album này là gì ?
Tôi sẽ tham_gia chương_trình " Làn_sóng xanh " và tiếp_tục lưu_diễn châu Âu .
Theo Màn Ảnh Sân_Khấu

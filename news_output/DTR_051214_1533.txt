﻿ " Không ai hiểu Lê_Văn_Trương hơn chúng_tôi ! "
Ông Ngô_Văn_Trân - Giám_đốc Sở TDTT_TT - Huế cho_biết như_thế xung_quanh chuyện cầu_thủ Lê_Văn_Trương ( H . Huế ) đang có tên trong danh_sách triệu_tập của cơ_quan điều_tra vì bị nghi bán_độ .
Tại SEA Games 23 vừa_rồi , theo nhận_định chung , Văn_Trương thi_đấu rất thất_thường đến khó hiểu . Còn ông thì thấy thế_nào ?
Sau trận Việt_Nam - Myanmar ( tại SEA Games 23 ) , anh Vũ_Hạng có điện_thoại cho tôi hỏi về trường_hợp của Trương . Tôi xin nói thật thế_này , không ai hiểu cậu ta bằng chúng_tôi đâu .
Bởi từ sau trận Việt_Nam - Maldives dự vòng_loại World_Cup 2006 tại sân Mỹ_Đình , Trương đã bị cầu_thủ đội bạn phạm lỗi , gây ra chấn_thương nặng và gây thoát_vị đĩa_đệm .
Mùa bóng trước ( 2004 ) , khi đá tranh lên hạng chuyên_nghiệp ở giai_đoạn 2 , Trương dường_như chỉ đá một hiệp trong trận gặp T . Cần_Thơ , còn sau đó không_thể tham_gia nổi trận nào .
Năm vừa_rồi , trong điều_kiện tập nặng , chúng_tôi phải thường_xuyên cho Trương đi trị_liệu tại khu nghỉ_dưỡng nước_khoáng nóng Mỹ_An .
Theo tôi nghĩ , với chấn_thương như Trương , nếu đá với mật_độ như ở SEA Games 23 sẽ không đủ sức .
Chúng_tôi biết Trương không còn sức như ngày_xưa , mà_lại bị viêm khớp nặng . Không có sức tích_lũy qua tập_luyện thì làm_sao mà đá . Mùa vừa_rồi như_thế cậu ta còn đá tốt đấy , chứ mùa trước Trương đã phải nghỉ mất 6 tháng , mà chẳng có ai cho đồng nào . Cuối_cùng chúng_tôi phải đưa Trương đi chữa khớp bằng vật_lý_trị_liệu tại Bệnh_viện Đường_sắt mất mấy tháng trời .
Các anh không tin thì cứ hỏi anh Thọ , anh Sang ( BHL đội H . Huế ) thì sẽ biết . Trương bị khớp có ai hiểu điều đó đâu , cầu_thủ đá dày như_vậy thì không_tài_nào hồi_phục sức được .
Ông có nghĩ rằng Trương dính_dáng đến tiêu_cực tại SEA Games 23 ?
Trương đá không có sức là một chuyện , nhưng việc cậu ta có dính_dáng tiêu_cực hay không , tôi không dám khẳng_định , tự bản_thân cậu ta biết thôi . Nhưng_mà chắc_chắn một điều là , sức_khỏe của Trương không còn tốt như ngày_xưa .
Đặt kỳ_vọng vào Trương lúc này là không nên . Sau trận U23VN thua U23 Thái_Lan tại_trận chung_kết , tôi có đọc một bài báo của tác_giả Trần_Duy_Long phân_tích vì_sao đội bạn chỉ tập_trung “ đánh ” vào cánh trái của U23 Việt_Nam , mặc_dù mạnh hơn cánh phải .
Sở_dĩ như_vậy là để chia_cắt sự liên_kết phối_hợp giữa Trương và Quốc_Anh . Cuối_cùng cả hai đều đơn_độc , nên Trương vì_thế làm_sao thi_đấu như mong_muốn được .
Xin cảm_ơn ông !
Theo Ngọc_Văn_Tiền phong

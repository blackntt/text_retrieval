﻿ Đà_Nẵng : Bắt_giữ một vụ nhập lậu thuốc_lá lớn nhất với 34.000 gói
Qua kiểm_tra , lực_lượng CSGT đã phát_hiện trên xe chở 34.000 gói thuốc_lá Jet nhập lậu . Ngay sau khi bị phát_hiện , chủ lô hàng đã bỏ trốn khỏi hiện_trường .
Theo lời khai của tài_xế Nguyễn_Văn_Bình thì toàn_bộ số hàng trên được chở từ thị_xã Đông_Hà , tỉnh Quảng_Trị vào tỉnh Quảng_Ngãi để tiêu_thụ .
Hiện toàn_bộ số tang_vật đã được Trạm kiểm_soát giao_thông Kim_Liên chuyển cho phòng cảnh_sát kinh_tế , Công_an TP Đà_Nẵng xử_lý .
Đây là vụ buôn_lậu thứ 3 kể từ đầu năm 2005 và là lớn nhất từ trước đến nay bị Trạm kiểm_soát giao_thông Kim_Liên , TP Đà_Nẵng phát_hiện , bắt_giữ .
Tin , ảnh : ĐĂNG_NAM

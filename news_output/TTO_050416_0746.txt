﻿ Điện_thoại_di_động cho trẻ_em
Công_ty điện_thoại_di_động Mỹ_Firefly_Mobile vừa phát_minh ra một loại điện_thoại giảm nhẹ nỗi lo này cho các bậc cha_mẹ . Điên thoại Firefly ( ảnh ) được chế_tạo cho trẻ_em tuổi từ 8 - 12 , không có bàn_phím mà được điều_khiển bằng giọng nói . Điện_thoại chỉ có năm nút bấm nhưng có tới 12 kiểu chuông , với màn_hình tinh_thể_lỏng .
Sau khi mua và cài mật_mã , phụ_huynh có_thể cài vào máy 22 số mà trẻ có_thể gọi tới ( bằng cách lựa trong danh_sách ) , trong đó có ba số có_thể gọi nhanh là các số của cha , mẹ và cấp_cứu . Điện_thoại sẽ được bán rộng_rãi từ tháng sau với giá 100 USD .
T.LINH ( Theo NY Times )

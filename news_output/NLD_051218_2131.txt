﻿ Trường CĐ cuối_cùng tổ_chức thi_tuyển
Thí_sinh đang làm bài thi ( NLĐ ) – Trong 2 ngày 17 và 18-12 , Trường CĐ tư_thục Kinh_tế Kỹ_thuật Nghiệp_vụ Nguyễn_Tất_Thành đã tổ_chức thi_tuyển cho 1.887 thí_sinh vào các ngành : quản_trị kinh_doanh , tài_chính – ngân_hàng , kế_toán , tin_học , công_nghệ may , công_nghệ kỹ_thuật điện – điện_tử , tiếng Anh , tiếng Trung_Quốc và tiếng Nhật .
Đây là trường CĐ cuối_cùng trong cả nước tổ_chức thi_tuyển do trường vừa được thành_lập tháng 8-2005 và được giao chỉ_tiêu trễ . Trước đó , trường đã xét tuyển được 225 sinh_viên theo điểm_sàn quy_định của Bộ GD - ĐT , vẫn còn thiếu sinh_viên so với chỉ_tiêu cho_phép trong năm_học này là 600 .
D.Hằng

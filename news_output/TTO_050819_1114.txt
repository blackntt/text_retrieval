﻿ Gặp_gỡ lưu_học_sinh Việt_Nam tại Pháp
Buổi gặp_gỡ là nơi để các bạn HSSV đang học_tập tại Việt_Nam giao_lưu với các bạn lưu_học_sinh đang học_tập tại Pháp xoay quanh những thông_tin cập_nhật , đầy_đủ và sinh_động về cuộc_sống và học_tập tại Pháp . Nhân hoạt_động này , Hội SVVN tại Pháp cũng muốn gây quỹ để tổ_chức các hoạt_động ủng_hộ nạn_nhân chất_độc_màu_da_cam .
Chương_trình dành cho tất_cả đối_tượng quan_tâm đến du_học Pháp . Vì số_lượng chỗ có_hạn , để tham_dự chương_trình , xin liên_lạc để đăng_ký chỗ trước theo các số điện_thoại sau : Nguyễn_Bích_Vân : ( 04 ) 9148.616 , Hồ_Thu_Hương : 0989.088.871 và qua email : contact@uevf.asso.fr
BÍCH_DẬU

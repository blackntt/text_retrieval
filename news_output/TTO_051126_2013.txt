﻿ Danh_sách trúng thưởng trận Việt_Nam - Indonesia
Trận đấu hôm_nay giữa Việt_Nam và Indonesia đã thu_hút được 10500 người tham_dự , tăng hơn 2000 người so với trận đấu trước . Tuy_nhiên , kết_quả trận đấu không như các cổ_động_viên mong_đợi , nên chỉ có 1074 bạn_đọc dự_đoán đúng kết_quả trận đấu .
Các giải_thưởng dự_đoán hôm_nay thuộc về :
Giải_Nhất : Nguyen_Huu_Nghiem , CMND : 23540473 , email : n _ babyboy @ , địa_chỉ : P11 , Q . 5 TP . HCM , ĐT : 090823 … Câu_hỏi phụ : 1077
Giải_Nhì : Nguyen_Anh_Tuan , CMND : 182346480 , email : lecheval79 @ , địa_chỉ : Nghi_Loc - Nghe_An , ĐT : 0908658 … Câu_hỏi phụ : 1077
Giải_Ba : Tran_Thanh_Phuc , CMND : 201374960 , email : phuc4y @ , địa_chỉ : 100 Hoang_Dieu , Da_Nang , ĐT : 09140852 … Câu_hỏi phụ : 1078
Bạn_Huu_Nghiêm và Anh Tuan đều cùng có số dự_đoán chênh 3 số so với tổng_số người dự_đoán đúng trận này . Do_đó , theo quy_định , BTC đã quyết_định các giải_thưởng ( trường_hợp trùng câu_hỏi phụ sẽ tính ưu_tiên cho người tham_gia dự_đoán sớm hơn ) theo thời_gian mà các bạn đã dự_đoán .
Giải nhất
Một ĐTDĐ Motorola C390 có Bluetooth trị_giá 2,3 triệu đồng
Giải nhì
Một ĐTDĐ Motorola C139 trị_giá 1,3 triệu đồng
Giải ba
Một ĐTDĐ Motorola C118 trị_giá 1 triệu đồng
Về phần thi đặt_cược :
Đã có tổng_cộng 593 bạn đặt_cược và thắng cược cho tỉ_số Việt_Nam thua Indonesia 0 - 1 , tỉ_lệ thắng cược lần này là 1 thắng 9 .
Đứng đầu_bảng xếp_hạng hiện_nay là anh Nguyễn_Văn_Cảnh ( Q . 2 , TP . HCM ) với 64000 điê ̉ m . Anh Cảnh đã dùng toàn_bộ số điểm của mình tích_lũy ở các trận trước ( 63900 điểm ) để đặt_cược cho tỉ_số 0 - 1 . Và anh đã chiến_thắng , vươn lên dẫn_đầu với số điểm 64000 , cách_biệt khá xa so với hai người chơi ở vị_trí thứ nhì ( anh Pham_Quang_Hung , 45100 điê ̉ m ) và thứ_ba ( anh Nguyen_Duc_Minh , 36200 điê ̉ m ) .
Mời các bạn tiếp_tục theo_dõi dự_đoán kết_quả và đặt_cược cho các trận đấu tiếp_theo . Chúng_tôi sẽ cập_nhật lịch thi_đấu và tỷ_lệ cá_cược sau .
Cuộc_thi " Dự_đoán kết_quả bóng_đá nam SEA Games 23 " do báo Tuổi_Trẻ_Online tổ_chức , Motorola_Việt_Nam tài_trợ và siêu_thị điện_thoại thegioididong ( www.thegioididong.com ) phối_hợp thực_hiện .
TTO

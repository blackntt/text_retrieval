﻿ VOV phát_sóng chương_trình tiếng Chăm hằng ngày
Chương_trình được phát trên sóng trung , tần_số 873KHz và 747KHz , vào các buổi 6g30 , 10g30 , 14g30 và 20g hằng ngày .
Với_Chương trình phát_thanh tiếng Chăm , Hệ phát_thanh tiếng dân_tộc ( VOV4 ) của VOV nhằm phục_vụ đồng_bào các dân_tộc_thiểu_số trong cả nước sẽ có tổng_cộng 9 chương_trình , với tổng thời_lượng phát_sóng hàng ngày là 26 tiếng . Trước đó , hệ VOV4 đã có 8 chương_trình phát_thanh tiếng dân_tộc gồm chương_trình tiếng Mông , Khơme , Ê Ðê , Jơ_Rai , Ba_Na , Xê_Ðăng , Cơ_Ho và Thái .
Đài_Tiếng nói Việt_Nam cũng đang có kế_hoạch xây_dựng một_số chương_trình tiếng dân_tộc như Dao , Tày , M ' Nông , Châu_Ro và Raglai .
Gần 1,4 tỷ đồng xây_dựng bản người Thái tại Điện_Biên
Theo dự_án bảo_tồn và phát_triển văn_hóa dân_tộc Thái , bản Na_Vai ở huyện Điện_Biên sẽ được xây_dựng thành bản văn_hóa - du_lịch điển_hình của tỉnh Điện_Biên , với tổng kinh_phí gần 1,4 tỷ đồng .
Dự_án sẽ đầu_tư xây_dựng nhà_văn_hóa , hệ_thống đường nội bản tại bản Na_Vai , phục_hồi các món ăn ẩm_thực dân_tộc , phát_triển nghề dệt thổ_cẩm truyền_thống và đào_tạo kỹ_năng phục_vụ khách du_lịch cho 60 hộ gia_đình đồng_bào Thái của bản .
Dự_án do Trung_tâm kỹ_thuật công_trình văn_hóa sông Mê_Công thuộc Trường_Đại học Rachaphat_Ubon ( Thái_Lan ) và Trung_tâm Tư_vấn phát_triển văn_hóa bản_địa và đô_thị Việt_Nam triển_khai trong 2 năm .
Ngành văn_hóa thông_tin Điện_Biên đang tổ_chức đào_tạo đội văn_nghệ dân_gian tại bản Na_Vai . Huyện Điện_Biên cũng đã nâng_cấp đường_trục chính vào bản .
Theo TTXVN

﻿ Bi sẽ sẽ biểu_diễn tại Mỹ vào năm tới
New_York_Madison_Square_Garden là nhà_hát nổi_tiếng mà bất_kì nghệ_sĩ nào của Mỹ cũng đều ao_ước được một lần biểu_diễn tại đó .
Năm nay , Bi đã rất thành_công với chuyến lưu_diễn mang tên Rainy_Day , khởi_đầu tại Seoul , sau đó đến Nhật_Bản , Trung_Quốc , Hong_Kong và Đài_Loan . Từ sức hút của bộ phim Ngôi nhà hạnh_phúc đến thành_công của những buổi diễn này đã đẩy tên_tuổi của Bi nổi như cồn tại khu_vực Châu Á .
Kênh MTV của Mỹ đã đặc_biệt chú_ý đến Bi sau khi anh giành được giải Grand_Slam trong lễ trao giải MTV Châu Á được tổ_chức tại Thái_Lan vào tháng 2 . Đến tháng 5 , anh lại tiếp_tục ẵm thêm giải Buzz_Asia tại lễ trao giải MTV Nhật_Bản . Sang tháng 7 , Bi lại được bình_chọn là Nghệ_sĩ Hàn_Quốc của năm tại giải_thưởng âm_nhạc CCTV - MTV ( Trung_Quốc ) .
Tạp_chí Time số tháng 9 cũng đã có một bài viết về Bi và đánh_giá anh là ca_sĩ Hàn_Quốc đầu_tiên đủ lợi_thế để thâm_nhập thị_trường âm_nhạc Mỹ .
Hãng JYP Entertainment - nơi Bi đang đầu_quân cho_biết vé buổi biểu_diễn của Bi tại Mỹ sẽ bắt_đầu được bán trên mạng từ ngày 19-12 .
THẾ_MINH ( Theo KBS )

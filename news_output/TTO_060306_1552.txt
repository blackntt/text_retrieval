﻿ Gạo_Việt_Nam trúng_thầu tại Nhật_Bản
Trong đó , gạo_tẻ hạt dài của Việt_Nam đã trúng_thầu với tổng_số 35.000 tấn . Đây là lần thứ_hai trong năm 2006 gạo Việt_Nam trúng_thầu tại Nhật_Bản .
Như_vậy , từ đầu năm 2006 , gạo Việt_Nam đã 2 lần liên_tiếp trúng_thầu bán cho Chính_phủ Nhật với tổng_số 56.000 tấn , trong khi cả năm 2005 , gạo Việt_Nam đã trúng_thầu 79.024 tấn .
Theo Hiệp hội lương_thực Việt_Nam , từ đầu năm đến nay , các doanh_nghiệp trong nước đã ký hợp_đồng xuất_khẩu 2 triệu tấn gạo . Ngay trong quý 1 , Hiệp_hội lương_thực Việt_Nam đã có_thể xuất khoảng 1 đến 1,1 triệu tấn gạo .
Theo VTV

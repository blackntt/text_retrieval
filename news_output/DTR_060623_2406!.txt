﻿ Gối “ ru ” ngủ
Các nhà_nghiên_cứu Nhật_Bản vừa phát_triển một loại gối chăm_sóc giấc_ngủ cho mọi người . Nó là một chiếc đệm có nhiều nút hướng_dẫn các cách để có một giấc_ngủ ngon_lành .
Chiếc gối này được gọi_là “ Bác sỹ ngủ - Speep_Doctor ” , phân_tích các trạng_thái ngủ của con_người và sau đó đưa ra 40 lời khuyên cho người đó trên một chiếc màn_hình bé tý ở bên_cạnh .
“ Bác sỹ ngủ ” có các bộ cảm_ứng đo những cử_động của đầu và cơ_thể của con_người để tính_toán xem người đó có ngủ sâu không .
“ Nếu mấy ngày không ngủ được , chiếc gối sẽ nói với bạn rằng : Tại_sao tối nay bạn không thư_giãn trước khi đi ngủ nhỉ . Hãy tắm thật lâu rồi lên giường ” , Naomi_Adachi , Giám_đốc công_ty Lofty đang bán sản_phẩm này .
Được thiết_kế thân_thiện , chiếc gối có_thể đưa ra rất nhiều lời khuyên bổ_ích để đảm_bảo giúp mọi người có một giấc_ngủ ngon .
Công_ty Lofty dự_định sẽ bán chiếc gối này vào tháng 12 tới .
N . H_Theo AFP

﻿ Phát_hành quyển tự_truyện Kiatisak
Theo kế_hoạch , lẽ_ra ngay trong ngày khai_mạc 1-6 , Kiatisak sẽ có_mặt để tặng chữ_ký cho người mua sách . Tuy_nhiên , vì lý_do khách_quan , anh đã xin cáo_lỗi vì sự vắng_mặt của mình .
Kiatisak cho_biết : “ Hiện_tại , do tinh_thần toàn đội HAGL đang bị ảnh_hưởng nghiêm_trọng sau cái chết thương_tâm của trung_vệ Bá_Khôi nên tôi không_thể rời xa đồng_nghiệp vào thời_điểm này . Xin được người hâm_mộ bóng_đá đến với Sports_Gala lượng_thứ vì lỗi hẹn .
Tôi không còn lòng_dạ nào để cầm đàn đứng hát trước công_chúng , cũng_như không còn tâm_trí để ký tặng vào sách . Tuy_nhiên , tôi hứa sẽ thu_xếp để có_mặt vào buổi chiều bế_mạc Sports_Gala ngày 5-6 " .
Toàn_bộ tiền nhuận_bút của cuốn sách Cuộc_đời tôi và loạt bài trích_đăng trên Tuổi_Trẻ đã được Kiatisak tặng cho chương_trình " Nâng những cánh_tay " của Tuổi_Trẻ ( hỗ_trợ nạn_nhân bị xơ_hóa cơ Delta ) .
S . H .

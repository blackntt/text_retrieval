﻿ Trận bán_kết bóng_đá nam : Xuất_hiện vé giả
Người hâm_mộ cũng cần lưu_ý đến một loại vé , có tem chống giả đặc_chủng nhưng lại là kiểu vé thực , ghế " ảo " vì vé có đề hàng ghế không hề có trong sân_vận_động . ban tổ_chức đã thừa_nhận lỗi về loại vé này , với lý_do sai_sót trong khâu in_ấn , và cam_kết khắc_phục sự_cố .
Ngoài_ra còn có một loại vé giả nữa , được chính những cơ_sở in vé thật in lậu ra , sử_dụng cả chất_liệu giấy , có tem đặc_chủng và có chất_lượng thật . Theo lực_lượng công_an , vì thời_gian diễn ra SEA Games ngắn nên việc phát_hiện và khắc_phục sự_cố về loại vé này trong các trận bóng_đá và đêm bế_mạc sắp tới sẽ rất khó_khăn .
Lực_lượng công_an cũng vừa bắt được 2 vụ , bắt_giữ hàng chục phe vé và thu_giữ gần 400 vé các trận bóng_đá nam .
Tính đến sáng 8-12 , vé " chợ_đen " trận bán_kết Việt_Nam - Malaysia , diễn ra_chiều 9/12 tại sân_vận_động Mỹ_Đình , đã được chào_bán với giá 2,5 - 3 triệu đồng / đôi , tăng gấp gần 3 lần so với giá được đưa ra trước khi diễn ra các trận vòng_loại .
Theo TTXVN

﻿ Tìm thấy một nhạc_phẩm khác của Đặng_Thế_Phong
Thông_qua Lê_Thương , Thế_Lữ , Lưu_Hữu_Phước , Nguyễn_Đình_Thi , Nguyễn_Cao_Luyện , Nguyễn_Đình_Phúc , Nguyễn_Văn_Thương , v . v … , tôi được biết : Đặng_Thế_Phong còn là tác_giả một_số nhạc_phẩm khác .
Đồng_thời , ông còn có mối quan_hệ về mặt sáng_tác với nhà_văn chuyên viết truyện trinh_thám Phạm_Cao_Củng .
Bộ sưu_tập các bài hát Việt_Nam từ khoảng 1937 tới 1975 của tôi không có nhạc_phẩm nào mà Đặng_Thế_Phong và Phạm_Cao_Củng là đồng_tác_giả .
Cho tới một hôm …
Cuối năm 1993 , tôi lên Đà_Lạt dự kỉ_niệm Đà_Lạt một trăm_tuổi . Một buổi chiều , đang đắm mình trong cảnh_vật trầm_lắng của một ngôi chùa lớn , giữa tiết trời mưa rét , tôi nghe tiếng hát của một người khoảng trên năm_mươi tuổi đứng gần cổng chùa . Người đó hát nhè_nhẹ , cách hát tỏ rõ ông hiểu âm_nhạc .
Tôi làm_quen với ông . Tôi bảo ông có giọng hát khá hay . Ông cười và hát lại cả bài . Tôi ngạc_nhiên vì bài hát_nói về chuyện lên chùa qua những âm_điệu gần_gũi với đặc_trưng nơi cửa Phật .
Tôi nói : Lần đầu_tiên , tôi được biết bài này . Nó làm cho tôi nhớ tới ca_khúc Kinh khổ ( Lời và nhạc : Trầm_Tử_Thiêng ) mặc_dầu hai ca_khúc có nhiều nét khác_nhau .
Ông khen tôi biết thẩm_âm . Rồi ông cho_biết : Bài hát vừa_rồi có tên Gắng bước lên chùa - Nhạc : Đặng_Thế_Phong . Lời : Phạm_Cao_Củng .
Tôi hỏi : Có phải Đặng_Thế_Phong tác_giả Con thuyền không bến và Phạm_Cao_Củng tác_giả nhiều truyện trinh_thám hay không ?
Ông nói : Đúng , đúng ! Rồi ông hẹn tôi : Hôm_sau , khoảng 15 giờ , cũng tại chùa này , đến gặp ông để tiếp_tục câu_chuyện liên_quan đến bài hát_nói trên .
Đúng hẹn , tôi đến . Trời vẫn rét nhưng khô_ráo .
Người đàn_ông mang theo một cây đàn ghita . Ông hát và tự đệm đàn bài hôm trước . Giọng trầm ấm và thiết_tha . Sau đó , ông cho tôi xem một bức ảnh chụp ca_khúc Gắng bước lên chùa do chính tay Đặng_Thế_Phong viết nắn_nót cả nhạc lẫn lời .
Dưới đầu_đề Gắng bước lên chùa , Đặng_Thế_Phong ghi rõ : Nhạc : Đặng_Thế_Phong . Lời : Phạm_Cao_Củng . Cuối ca_khúc , có dòng chữ : Tặng_Hoàng_Quý – Hà_Nội , 1 Mai 1940 ( tức mồng 1 tháng 5 - 1940 ) .
Ông cho_biết thêm : Bức ảnh này do cụ Phúc_Lai , một chủ hiệu ảnh rất nổi_tiếng ở Hà_Nội và Hải_Phòng lúc bấy_giờ , chụp tại Hà_Nội cho bố ông khoảng cuối năm 1940 .
Ảnh chụp từ bản có dòng chữ đề tặng nói trên cho nhạc_sĩ Hoàng_Quý ( tác_giả Cô láng_giềng , Cảm_tử quân ) - anh ruột của Hoàng_Phú tức GS nhạc_sĩ Tô_Vũ .
Nhạc_sĩ Hoàng_Quý quen bố ông và đã cho bố ông mượn bài hát có lời đề tặng ấy để bố ông nhờ cụ Phúc_Lai chụp làm kỉ_niệm . Bố ông qua_đời tại Đà_Lạt năm 1991 . Nhưng khoảng năm 1970 , tại Sài_Gòn , bố ông đã góp_phần vào việc xuất_bản tuyển_tập nhạc Hoàng_Quý .
Tôi hỏi tên ông và tên cụ nhưng ông trả_lời : Vì lí_do riêng , không_thể cho_biết . Tuy_nhiên , ông nhiệt_tình nói rõ : Ca_khúc này , theo bố ông , đã in trên nhật_báo Tin_Mới khoảng đầu năm 1940 .
Hồi giải_phóng Sài_Gòn , cùng cả gia_đình dời Sài_Gòn lên Đà_Lạt , bố ông bỏ lại hết , kể_cả trọn bộ tạp_chí Nam_Phong và trọn bộ báo Phụ_Nữ_Tân_Văn .
Mùa_đông . Chưa đến xế_chiều trời đã trở_nên mờ xám . Gió lạnh . Tôi phải đi xe_ôm về nhà_trọ ở xa ngôi chùa chúng_tôi đang trò_chuyện khoảng 5 cây_số . Vì_thế , không_thể đi phô - tô-cóp-pi bức ảnh chụp bài hát_nói trên được . Thật đáng tiếc !
Từ đó đến nay , tôi không được gặp lại_người bạn quý ấy nữa .
Gần hai tuần sau , trở_về TPHCM , tôi gặp ngay người chủ một tiệm sách_báo cũ mà từ mười_mấy năm nay tôi là khách quen , hỏi anh có báo Tin_Mới hay không . Anh bảo : Khó lắm ! Để em tìm !
Tôi ghi số điện_thoại bàn cho anh chủ tiệm .
Mãi đến … mười năm sau
Một_chiều cuối thu 2003 , anh chủ tiệm sách cũ điện_thoại cho tôi , bảo đến tiệm anh ngay . Tôi vừa tới , anh lập_tức đưa ra hai tập Tin_Mới đóng gáy da từ tháng Giêng đến hết tháng Chạp 1940 , trong đó khoảng bốn chục số bị mất .
Rất mừng : Có một_số in bài hát Gắng bước lên chùa . Đó là số Jeudi 4 Avril 1940 ( tức thứ Năm , mồng 4 tháng 4 năm 1940 ) .
Tôi nghẹn_ngào tưởng như Đặng_Thế_Phong đang đứng trước mặt , và , những trang truyện trinh_thám Phạm_Cao_Củng hồi nhỏ tôi đọc ở Thủ_đô Hà_Nội bỗng hiện ra trước_mắt , chập_chờn ! ! !
Tôi đề_nghị anh chủ tiệm đi cùng tôi đến một cửa_hàng ở đường Hai_Bà_Trưng để photocopy và xin anh nhận một_số tiền nào_đó tương_đối lớn của tôi .
Anh bảo : Mười năm lặn_lội đi tìm một bản_nhạc của một nhạc_sĩ lỗi_lạc . Mười_mấy năm anh mua sách_báo cũ của em và nhiều tiệm khác ... Em chẳng_những không lấy một đồng của anh mà_còn biếu anh tiền photocopy bài hát đó .
22 giờ , trời se lạnh .
Tôi ôm một tập Tin_Mới ngồi sau xe_máy do anh chủ tiệm sách_báo cũ điều_khiển phóng đến một cửa_hàng photocopy trên đường Hai_Bà_Trưng ( Tân_Định , quận 1 ) .
Xong việc , anh đèo tôi về tiệm của anh để tôi lấy xe_đạp .
Tôi nâng_niu hồi lâu ca_khúc Gắng bước lên chùa trước khi bắt_tay cảm_ơn anh bạn , ra về .
Gần 23 giờ . Đường vắng . Trong gió se lạnh phả vào mặt , tôi đạp xe từ_từ trên đường Nam_Kỳ_Khởi_Nghĩa . Khi qua cửa chùa Vĩnh_Nghiêm , tôi chợt nghĩ : Thế_là … mình đã Gắng bước lên chùa …
Đã hơi khuya . Gió_heo_may có_vẻ mạnh hơn .
Đêm nay thu sang cùng heo_may … Bến mơ dù thiết_tha , thuyền ơi đừng chờ_mong …
NHẬT_HOA_KHANH ( Báo_Tiền_Phong )

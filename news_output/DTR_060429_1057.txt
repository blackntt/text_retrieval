﻿ Keira_Knightley người_đẹp gợi_cảm nhất thế_giới
Ngược hẳn với tạp_chí People khi bình_chọn cô đào với cái bụng bầu to_tướng Angelina_Jolie là người phụ_nữ sexy nhất thế_giới , thì FHM - một ấn_phẩm cũng nổi_tiếng không kém , lại cho rằng người_đẹp Keira_Knightley mới là ngôi_sao sexy hơn ai hết .
Nữ diễn_viên người Anh này đã được bình_chọn là người phụ_nữ gợi_cảm nhất thế_giới trên tạp_chí FHM hôm thứ 5 vừa_qua . Người_mẫu Keeley_Hazel và ngôi_sao đang lên của Hollywood_Scarlett_Johansson lần_lượt đứng thứ_hai và ba trong danh_sách này .
FHM - ấ n phẩm đặc_biệt dành cho nam_giới này khẳng_định rằng , đã có 2 triệu người bình_chọn cho siêu_mẫu và kiêm người dẫn_chương_trình bốc_lửa Kelly_Brook - người từ vị_trí thứ nhất trong bảng xếp_hạng năm_ngoái đã tụt xuống vị_trí thứ 5 trong bảng xếp_hạng năm nay . Trong khi đó , ngôi_sao đứng vị_trí thứ nhất do tạp_chí People bầu_chọn - Angelina_Jolie lại chỉ xếp khiêm_tốn ở vị_trí thứ 4 trên FHM .
Ca_sĩ kiêm diễn_viên da_màu Beyonce_Knowles đứng thứ 7 , đáng tiếc ngôi_sao quần_vợt xinh_đẹp của Nga_Maria_Sharapova xếp tận vị_trí thứ ... 56 .
Tạp_chí FMH tuyên_bố , danh_sách này được bình_chọn một_cách công_khai do những người_dân Anh quốc tiến_hành . Hình_ảnh top 5 người phụ_nữ gợi_cảm nhất thế_giới của FHM :
1 . Keira_Knightley đứng thứ nhất trong danh_sách Người phụ_nữ gợi_cảm nhất thế_giới .
2 . Cô người_mẫu " nóng_bỏng " Keeley_Hazel đứng thứ_hai trong danh_sách Người phụ_nữ gợi_cảm nhất thế_giới .
3 . Scarlett_Johansson về thứ_ba trong danh_sách Người phụ_nữ gợi_cảm nhất thế_giới .
4 . Bà bầu quyến_rũ Angelina_Jolie về thứ_tư trong danh_sách Người phụ_nữ gợi_cảm nhất thế_giới .
5 . Người_đẹp bốc_lửa Kelly_Brook về thứ_năm trong danh_sách Người phụ_nữ gợi_cảm nhất thế_giới . Thu_Thy_Theo_China_Daily

﻿ 6 học_sinh thi Olympic toán tại Indonesia
Sáu HS_TP . HCM gồm Võ_Thùy_Minh_Ngọc ( Trường_Trần_Đại_Nghĩa ) , Ngô_Thanh_Bảo_Trâm ( Nguyễn_Du , Q . 1 ) , Nguyễn_Tấn_Sỹ_Nguyên ( Hoa_Lư , Q . 9 ) , Nguyễn_Đoàn_Bảo_Ân ( Lê_Văn_Tám , Bình_Thạnh ) . Nguyễn_An_Hòa ( Nguyễn_Du , Gò_Vấp ) và Lê_Minh_Nhật ( Nguyễn_Văn_Tố , Q.10 )
Đây là những học_sinh đã được tuyển_chọn tham_gia kỳ thi Olympic toán và khoa_học bậc tiểu_học được tổ_chức tại Jakarta ( Indonesia ) vào 28-11 tới với đại_diện HS của 20 nước tham_dự . VN lần thứ_hai tham_gia cuộc_thi này ( lần đầu_tiên đoàn HS Hà_Nội tham_gia và đã đoạt giải nhất toàn đoàn ) .
K.LI ÊN

﻿ Di_chỉ khảo_cổ_học dưới một ngôi chợ ở Cairo
Đó là ngôi đền lớn nhất chưa từng được phát_hiện ở Aim_Shams và Matariya , nơi trước_kia là cung_điện pharaon tại thành_phố Héliopolis , được dành riêng cho việc thờ_cúng Thần_Mặt trời Amon-Râ .
Trong số các hiện_vật được tìm thấy , có một bức tượng bằng đá granite đỏ , nặng từ 4 đến 5 tấn , mà đường_nét " giống với đường_nét bức tượng của Ramsès đệ nhị " - theo lời ông Zahi_Hawass . Trên một bức tượng khác , có ba khung chữ tượng_hình với tên của pharaon Ramsès . Phần nền lát đá xanh của ngôi đền cũng được khai_quật .
Để các nhà khảo_cổ có hiện_trường khai_quật rộng hơn , ngôi chợ phải dời sang vị_trí khác . Ông Zahi_Hawass dự_tính sẽ biến nơi này thành một địa_điểm du_lịch và khảo_cổ trong vòng hai năm .
Ramsès đệ nhị trị_vì Ai_Cập trong 57 năm , từ năm 1270 đến năm 1213 ( trước Thiên_Chúa giáng_sinh ) ( ) . Ông đã cho xây_dựng nhiều công_trình dọc hai bờ sông Nil , trong đó có đền thờ Abou_Simbel ở phía Nam đất_nước . Thành_phố Héliopolis ngày_xưa có rất nhiều đền thờ Thần_Mặt trời . Địa_phận của thành_phố cổ này tương_ứng với rất nhiều khu dân_cư ngày_nay .
TRẦN_LƯƠNG_CÔNG_KHANH ( Theo AP )

﻿ Hiddink : “ Australia quyết không chơi bẩn ”
HLV trưởng Guus_Hiddink tỏ ra giận_dữ khi cho rằng những lời cáo_buộc ĐT Australia chơi_xấu là hoàn_toàn vô_nghĩa và nó chỉ nhằm gây áp_lực lên các trọng_tài điều_khiển trận đấu mà thôi .
Trong buổi họp_báo trước trận đấu Australia - Nhật_Bản ( 20h , giờ VN ) , HLV Guus_Hiddink tự_tin tuyên_bố : “ Chúng_tôi là một đội bóng mạnh và mỗi cầu_thủ trong đội luôn có tinh_thần chiến_đấu mỗi khi xung_trận . Nhưng điều đó không có_nghĩa Australia chỉ nhăm_nhăm “ chặt chém ” đối_phương ” .
Sở_dĩ vị HLV từng đưa Hàn_Quốc vào tới bán_kết World_Cup cách đây 4 năm phải tuyên_bố như_vậy là do trước đó Chủ_tịch LĐBĐ Nhật_Bản_Saburo_Kawabuchi đã cho rằng : “ Australia chỉ bao_gồm toàn những gã chỉ biết triệt_hạ đối_phương . Mục_tiêu mỗi pha vào bóng của họ không phải là phá bóng mà là đốn chân của đội bạn . Thật quá thiếu fair - play ” .
Những lời ông Kawabuchi nhằm ám_chỉ trận đấu gần đây nhất của Australia với Hà_Lan . Trong trận đấu đó , có tới 3 cầu_thủ áo cam đã buộc phải rời sân do chấn_thương còn tiền_vệ Luke_Wilkshire ( Australia ) đã bị đuổi khỏi sân do nhận thẻ_đỏ .
Vị HLV người Hà_Lan này nói tiếp : “ Ai cũng hiểu những tuyên_bố của người Nhật chỉ nhằm gây áp_lực lên các trọng_tài . Điều đó là không chấp_nhận được . Tôi nghĩ chắc_chắn chúng_tôi sẽ thi_đấu tuyệt_vời và chỉ cho họ thấy Australia chơi_đẹp hay không .
Chơi bóng bổng là lợi_thế đáng_kể của Australia trước Nhật_Bản
Nếu nhìn vào thành_tích trong quá_khứ rõ_ràng Nhật_Bản có nhiều thành_tích hơn chúng_tôi . Nhưng điều đó không có nghĩa_lý gì cả trong trận đấu tối nay ” .
Khi được hỏi về cách chế_ngự đội Nhật tối nay , cựu HLV_PSV Eindhoven cho rằng : “ Ảnh_hưởng của Zico lên lối chơi ĐT Nhật là rất rõ . Lối đá kỷ_luật luôn mềm_mại hơn nhờ khả_năng sáng_tạo và cầm bóng của một_vài cầu_thủ .
Để hạn_chế lối đá đó ư ? Khống_chế chặt_chẽ khu_vực giữa sân là có_thể đẩy_lùi Nhật_Bản xuống thế phòng_ngự . Đó là cách tốt nhất để chia_cắt Nhật tối nay .
Ngoài_ra , rõ_ràng Nhật_Bản không thực_sự mạnh trong những pha tranh_chấp bóng bổng và với những tuyển_thủ đang chơi bóng tại Anh như Kewell , Cahill hay Viduka , chúng_tôi hy_vọng sẽ tìm được cho mình một kết_quả có lợi ” .
Trong trận đấu sớm bảng F , Nhật_Bản sẽ đụng_độ Australia tại Kaiserslautern vào tối nay ( 12/6 ) . Ngày_mai hai đội_tuyển mạnh nhất bảng là Brazil và Croatia sẽ gặp nhau tại Berlin . L . T . T

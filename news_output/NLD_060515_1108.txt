﻿ Indonesia : Núi_lửa Merapi phun trào
Núi_lửa Merapi hoạt_động mạnh ( NLĐO ) - Núi_lửa Merapi ở Indonesia bắt_đầu phun trào sáng nay 15-5 . Lửa , nham_thạch , đá và hơi nóng đã bốc lên mạnh_mẽ và trào xuống_dốc núi gần 3 dặm .
Các nhà_khoa_học hôm_qua đã cảnh_báo nham_thạch có_thể làm cho cấu_trúc của ngọn núi bị sụp_đổ .
Hoạt_động của núi_lửa rất mạnh , khói bốc lên ngùn_ngụt và bao_trùm cả một vùng rộng_lớn . Những người_dân trong làng không chịu di_tản khỏi vùng nguy_hiểm đứng thành từng nhóm và nhìn lên dốc núi .
Hơn 4.500 người_dân sống ở ngôi làng gần núi Merapi đã được di_tản khỏi khu_vực nguy_hiểm sau khi các nhà_khoa_học cảnh_báo núi_lửa Merapi sẽ hoạt_động trở_lại . Mặc_dù chính_phủ đã có lệnh bắt_buộc dân_cư trong vùng phải di_tản nhưng một_số người_dân nơi đây vẫn kiên_trì bám_trụ để trông_coi gia_súc và ruộng_đồng .
“ Chúng_tôi không_thể ép_buộc họ ” . Widi_Sutikno , quan_chức điều_phối các chiến_dịch khẩn_cấp của chính_phủ cho_biết . Tuy_nhiên ông Widi_Sutikno cũng cảnh_báo người_dân chuẩn_bị xe_cộ để sẵn_sàng di_chuyển . Lực_lượng cảnh_sát cùng_với xe cứu_hộ cũng được điều_động cách đó 5 dặm .
Núi_lửa Merapi cách thủ_đô Jakarta 250 dặm về phía đông , là một trong 129 ngọn núi_lửa ở Indonesia vẫn còn hoạt_động . Gần đây nhất , hoạt_động núi_lửa đã làm 60 người Indonesia thiệt_mạng trong năm 1994 . Và thảm_kịch núi_lửa đáng nhớ nhất xảy tại Indonesia ra năm 1930 khiến 1.300 người chết .
Một_số hình_ảnh phun trào của núi_lửa Merapi :
T . V ( Theo AP , Reuters )

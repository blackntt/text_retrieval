﻿ Cháy siêu_thị ở Paraguay là do dầu ăn
Ống_khói của siêu_thị không còn được trong tình_trạng tốt , dầu ăn đã kích muội than còn lại trong lò nướng và ngọn lửa đã lan nhanh ra khắp tòa nhà .
Viện công_tố nhà_nước Paraguay cũng cho_biết hiện vẫn còn khoảng 100 xác vẫn chưa được nhận_dạng . Cảnh_sát với sự hỗ_trợ của các chuyên_gia nước_ngoài sẽ sớm tiến_hành lấy mẫu DNA và răng để nhận_dạng các nạn_nhân .
Theo thống_kê chính_thức , vụ cháy siêu_thị ở thủ_đô Asuncion ngày 1-8 vừa_qua đã làm 464 người bị thiệt_mạng , 454 người bị_thương và hơn 100 bị mất_tích .
Tòa_án Paraguay cũng đã chính_thức buộc_tội cố_sát đối_với chủ siêu_thị Juan_Pio_Paiva , con_trai ông_ta - Victor_Daniel_Paiva - và bốn nhân_viên bảo_vệ vì đã đóng_cửa siêu_thị trong thời_điểm xảy_ra hỏa_hoạn .
T . L ( Theo BBC News , AFP )

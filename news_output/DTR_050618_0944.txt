﻿ Argentina , Đức quá nhiều cơ_hội
Chỉ có một chiến_thắng mới giúp Australia tránh khỏi nguy_cơ bị loại khỏi Confederations_Cup , nhưng khó thay khi đối_thủ của họ lại là Argentina . Trong khi đó , Đức quá tự_tin khi chỉ phải tiếp đối_thủ " nhẹ cân " Tunisia .
Đội bóng xứ Tango đã không để lại ấn_tượng đậm_nét bằng Australia sau khi kết_thúc lượt trận mở_màn của bảng A , nhưng đẳng_cấp của họ vẫn được coi là vượt_trội so với Socceroos . Nếu thắng Australia , Argentina nhiều khả_năng sẽ sớm lấy vé vào bán_kết , và như thế_cuộc chạm_trán với chủ nhà Đức ở lượt trận cuối_cùng sẽ gần_như chỉ còn là thủ_tục .
Trong khi đó , Australia không_thể nhận thêm một thất_bại nào nữa nếu_như họ muốn tiếp_tục cuộc hành_trình của mình ở Confederations_Cup , và đó là lý_do khiến cho HLV trưởng ĐT Australia_Frank_Farina phải cân_nhắc khả_năng tung Mark_Viduka ra sân ở trận quyết_chiến với Argentina , dù chân_sút này chưa hoàn_toàn bình_phục chấn_thương .
Confederations_Cup lượt 1
Argentina - Tunisia : 2-1
Đức - Australia : 4-3
Mexico - Nhật_Bản : 2-1
Brazil - Hy_Lạp : 3 - 0
Sơ_đồ thi_đấu với một tiền_đạo cắm duy_nhất John_Aloisi của Australia đã vận_hành tốt ở trận thua Đức , trong đó , riêng Aloisi đóng_góp 2 bàn thắng . Nhưng nếu muốn đánh_bại Argentina , Australia rất cần sự trở_lại của Viduka trên hàng công .
HLV Farina cho_biết : " Mark rất muốn ra sân và cậu ấy có_vẻ như đã hoàn_toàn bình_phục trên sân tập , nhưng chúng_tôi vẫn chưa dám chắc liệu có sử_dụng Mark ở ngay một trận cầu căng_thẳng như_thế_này hay không . Chúng_tôi sẽ để Mark tập thêm , và có_thể sẽ cho cậu ấy vào sân từ băng ghế dự_bị " .
Đã không có trong tay đội_hình mạnh nhất khi tới Đức , nhưng chỉ sau lượt trận đầu_tiên , Australia lại mất thêm một trụ_cột là hậu_vệ Tony_Popovic sau một pha vào bóng mạnh của Bastian_Schweinsteiger . Để lấp vào chỗ trống của Popovic , có_thể HLV Farina sẽ sử_dụng John_Mc Kain , cầu_thủ đã vào sân thay Popovic khi anh bị chấn_thương mắt_cá .
Lịch đấu đêm nay
Đức - Tunisia 23h
Argentina - Australia 1h45
Đánh_giá về đối_thủ , HLV Farina nhận_xét : " Argentina là một trong những đội bóng hay nhất thế_giới , nếu_không nói là số_một . Chắc_chắn đây sẽ là một trận đấu khó_khăn cho Australia , nhưng chúng_tôi vẫn có cơ_hội nếu_như thi_đấu với phong_độ giống trận gặp Đức " .
Cho_dù cửa thắng khá hẹp nhưng Australia vẫn có quyền nuôi hy_vọng , bởi Argentina không phải là không có nhược_điểm . Sau trận thắng Tunisia , chính HLV Jose_Pekerman phải thừa_nhận rằng " nhiều cầu_thủ của tôi trong trận đấu đó còn rất non kinh_nghiệm " . Nếu khai_thác tốt nhược_điểm này của Argentina , Australia sẽ có không ít cơ_hội .
Ở trận đấu thứ 2 của bảng A diễn ra_vào hôm_nay , HLV trưởng ĐT Tunisia_Roger_Lemerre hứa_hẹn sẽ dành cho nước Đức chủ nhà một món quà thú_vị . Tunisia đã thua Argentina trong trận ra_quân mà không có 6 cầu_thủ trụ_cột , và tất_cả trong số họ sẽ đồng_loạt trở_lại ở cuộc chạm_trán này , và đáng chú_ý nhất_là cặp tiền_đạo Ziad_Jaziri - Santos .
Cuộc đối_đầu giữa hai thủ_quân : Ballack và Trabelsi
Về phần mình , ĐT Đức nhiều khả_năng sẽ có ít_nhất 2 thay_đổi trong đội_hình so với trận thắng Australia . Trung_vệ Per_Mertesacker vẫn chưa biết có kịp bình_phục chấn_thương hay không , còn thủ_môn Oliver_Kahn bị bầm tím mắt phải , và chắc_chắn sẽ được HLV trưởng Juergen_Klinsmann thay_thế bằng Jens_Lehmann .
Đức đã không làm hài_lòng giới hâm_mộ nước này khi họ chỉ giành thắng_lợi trước Australia nhờ may_mắn và còn để lọt_lưới tới 3 bàn thua . Bởi_thế , thủ_quân Michael_Ballack đã lên_tiếng kêu_gọi các đồng_đội hãy cống_hiến một màn trình_diễn ấn_tượng hơn để thuyết_phục lòng tin của dư_luận . Ballack tuyên_bố : " Chúng_ta sẽ tiếp_tục gặp rắc_rối nếu cứ để thua những bàn kiểu tự_sát như_thế ' ' .
Cặp tiền_đạo Jaziri và Santos của Tunisia đã có tổng_cộng 24 bàn thắng trong 68 lần ra sân , và lối chơi lắt_léo dựa trên nền_tảng tốc_độ và kỹ_thuật siêu_hạng của họ sẽ là khắc_tinh với hàng thủ của Đức gồm nhiều cầu_thủ cao_to nhưng xoay_trở khá chậm . Chẳng thế_mà HLV Lemerre đã tự_tin khẳng_định : " Người_Đức có ưu_thế về sức_mạnh và thể_hình , nhưng chúng_tôi có những “ con muỗi nhỏ ” có_thể quật_ngã cả những gã khổng_lồ " . Theo Vietnamnet

﻿ Xây_dựng nhà máy_điện chạy bằng rác_thải
Một nhà_máy chế_biến rác thành điện_năng đầu_tiên ở Việt_Nam đã được khởi_công xây_dựng tại khu Gò_Cát , quận Tân_Bình , TPHCM . Nhà_máy này được xây_dựng với số vốn 260 tỷ đồng , trong đó vốn viện_trợ của Chính_phủ Hà_Lan là 65% .
Khu_Gò_Cát rộng 25 ha , có 5 ô chôn rác với diện_tích khoảng 17,5 ha . Công_suất xử_lý bãi rác khoảng 3,750 triệu tấn , nhưng hiện đang được xử_lý để phát_điện với khối_lượng 2,5 triệu tấn .
Khu_Gò_Cát , bãi rác mênh_mông hiện đã được phủ đỉnh ( đóng bãi ) 2 ô số 4 và 5 . Sau khi thực_hiện , lượng khí gas sản_sinh khoảng 420 – 450 m3 gas/giờ ( lượng gas cần để chạy một tổ máy chỉ khoảng 375 m3 gas/giờ ) .
Nhà_máy này có 3 tổ máy_phát_điện , công_suất 2.400 KWh điện . Nhà_máy đã bắt_đầu chạy ổn_định một tổ máy và phát_điện lên lưới_điện quốc_gia 530 KWh . Cuối năm nay , 2 tổ máy còn lại tiếp_tục hoàn_chỉnh và tất_cả sẽ cung_cấp điện ổn_định trong_suốt 15 năm .
Ở_TPHCM hiện có khu liên_hợp chất_thải rắn Tây_Bắc_Củ_Chi , diện_tích 822 ha . Thành_phố đang triển_khai khu quy_hoạch xử_lý rác như : khu đốt rác , chôn lấp rác và chế_biến rác thành phân_hữu_cơ .
Tuy_nhiên , theo các nhà_khoa_học thì việc biến rác thành điện sẽ khả_thi hơn , vì nó không ô_nhiễm môi_trường và còn thu lại lợi_nhuận từ việc bán điện . Theo Tiê ̀ n phong

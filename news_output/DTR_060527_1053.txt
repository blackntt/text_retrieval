﻿ Quả bóng vàng châu Âu và " Lời nguyền " World_Cup !
50 năm kể từ khi ra_đời , danh_hiệu Quả bóng Vàng châu Âu đã làm rạng danh bao tên_tuổi ưu_tú của nền bóng_đá thế_giới . Nhưng trớ_trêu thay , trong lịch_sử , chưa cầu_thủ nào “ đủ sức ” đăng_quang ngôi vô_địch thế_giới khi đang là chủ_nhân của danh_hiệu cao_quý này .
“ Quả bóng Vàng ” châu Âu ( QBV ) được nhật_báo thể_thao hàng_đầu của Pháp , L’ Equipe đứng ra tổ_chức và trao_tặng vào tháng 12 hàng năm cho cầu_thủ xuất_sắc nhất tại các giải vô_địch quốc_gia châu Âu dựa trên kết_quả bầu_chọn của các cây_bút thể_thao nổi_tiếng châu_lục này .
Trước năm 1994 , danh_hiệu này chỉ được trao cho các cầu_thủ có quốc_tịch trong lãnh_thổ châu Âu . Tuy_nhiên , sau đó , cầu_thủ thuộc các châu_lục khác nhưng chơi bóng tại châu Âu cũng được xét chọn và tiền_đạo nổi_tiếng người Liberia , George_Weah là người đầu_tiên có được vinh_dự trên .
Kể từ năm 1956 đến nay , trong số 10 kỳ World_Cup có sự góp_mặt của các QBV , có đến không dưới 5 lần , chủ_nhân của danh_hiệu này cùng đồng_đội gục ngã trong trận chung_kết trước những đối_thủ mạnh hơn .
5 lần “ chết ” trước ngưỡng_cửa thiên_đàng !
Tại_Mexico 1970 , QBV châu Âu lúc đó là huyền_thoại của Milan , Gianni_Rivera đã cùng_với ĐT Italia nổi_tiếng với chiến_thuật Catenaccio ( phòng_ngự đổ bê_tông ) lọt vào tới trận chung_kết .
Rivera , QBV đầu_tiên thất_bại tại một trận chung_kết World_Cup .
Nhưng quả_là ác_mộng khi những chàng_trai Azzurri không_thể đương_cự nổi sức_mạnh “ huỷ_diệt ” của người Brazil với những Pele , Zairzinho ... và chịu thất_bại thảm_hại 1-4 . Rivera trở_thành QBV châu Âu đầu_tiên thử sức trong một trận chung_kết World_Cup và ... thất_bại .
Lịch_sử tiếp_tục lặp lại 4 năm sau đó , tại Tây_Đức 1974 . Khi đó , cả châu Âu đang “ run_rẩy khiếp_sợ ” trước sức_mạnh của những “ cơn lốc màu da_cam ” và huyền_thoại Johan_Cruyff đang ở trên đỉnh_cao của danh_vọng với tư_cách là Cầu_thủ xuất_sắc nhất châu Âu .
Nhưng , lối chơi tấn_công tổng_lực mà Hà_Lan áp_dụng để “ càn_quét ” đối_thủ từ đầu giải đã gặp phải sự thực_dụng đầy hiệu_quả của người Đức trong trận chung_kết và kết_quả là , “ người Hà_Lan bay ” ngẩn_ngơ nhìn những Franz_Beckenbauer , Gerd_Muller bước lên bục chiến_thắng .
Câu_chuyện về sự “ vô_duyên ” của những QBV với ... chiếc Cúp_Vàng thế_giới vẫn tiếp_tục ở Espana ' 82 , nhưng lần này , nó “ vận ” vào chính một người Đức , Karl-Heinze Rummenigge .
Rummenigge không_thể đưa Đức tới ngôi vô_địch tại Espana 82 .
Năm 1981 , người Đức độc_chiếm các vị_trí đầu trong cuộc bình_chọn của báo L’ Equipe với Rummenigge ( quả bóng Vàng ) , Paul_Breitner ( quả bóng Bạc ) và Bernd_Schuster ( quả bóng Đồng ) .
Tuy_nhiên , điều đáng buồn là tại mùa_hè Tây_Ban_Nha năm 1982 , ĐT Đức đã bị Italia của Paolo_Rossi qua_mặt ( 3-1 ) trong trận chung_kết được đánh_giá là một trong những trận cầu hay nhất lịch_sử World_Cup . Thêm một QBV nữa phải bỏ lỡ cơ_hội .
Chưa hết , chương “ bi ” nhất trong câu_chuyện về những QBV xảy_ra tại World_Cup 1994 ở Mỹ .
Một trận chung_kết nghẹt_thở giữa đại_diện của trường_phái tấn_công đẹp_mắt Brazil với đại_diện của phong_cách phòng_ngự hiệu_quả Italia đã diễn ra bất_phân_thắng_bại và phải định_đoạt bằng loạt penalty .
Cuối_cùng đội bóng Nam_Mỹ lên_ngôi vô_địch sau khi cầu_thủ với mái_tóc “ đuôi ngựa thần_thánh ” Roberto_Baggio đá hỏng quả penalty quyết_định .
Cú sút penalty " định_mệnh " của Roberto_Baggio trong trận chung_kết với Brazil .
Giờ thì liệu có cần phải nhắc lại xem ai là QBV châu Âu năm 1993 không nhỉ ? ( ! )
Năm 1998 , ĐT Brazil hùng_mạnh đưa quân đến Pháp khi có trong tay cây săn bàn thượng_thặng , Ronaldo . “ Người_ngoài hành_tinh ” vừa trải qua một mùa bóng tuyệt_vời với đội bóng xứ Catalan khi ghi tới 34 bàn thắng và “ ẵm ” luôn danh_hiệu QBV .
Nhưng điều gì đã xảy_ra trong trận chung_kết tại Stade de France . Brazil gục ngã đau_đớn trước đội chủ nhà Pháp với tỷ_số 0 - 3 trong một loạt nghi_vấn về tình_hình sức_khoẻ của Ronaldo . Thêm một kết_cục buồn cho chủ_nhân của Quả bóng Vàng châu Âu .
3 lần bại_trận dưới tay nhà ... hậu vô_địch
Ngoài những thất_bại trước “ ngưỡng_cửa thiên_đàng ” trên , các QBV còn từng 3 lần khác thất_bại trước những ĐT sau_này lên_ngôi vô_địch , đó là trường_hợp của Michael_Owen ( 2002 ) , Van_Basten ( 1990 ) và Eusebio ( 1966 ) .
Trong khi huyền_thoại Eusebio cùng Bồ_Đào_Nha thất_bại trước nhà VĐTG 1966 , ĐT Anh tại vòng bán_kết thì ĐT Hà_Lan của Van_Basten cũng phải “ ôm hận ” thêm 1 lần nữa trước người Đức của HLV Franz_Beckenbauer tại vòng 2 Italia 1990 .
Gần đây nhất_là thất_bại của “ những chú sư_tử ” Anh trước Brazil tại World_Cup 2002 khi có trong đội_hình một Michael_Owen đang ở đỉnh_cao phong_độ và vừa giành tới ... 5 chiếc Cúp cùng Liverpool năm 2001 .
Còn tại Đức mùa_hè này , liệu Ronaldinho , cầu_thủ xuất_sắc nhất của FIFA 2 năm liên_tiếp và hiện được đánh_giá là tiền_vệ hay nhất thế_giới hiện_nay , có phá_bỏ được “ lời nguyền ” trên ?
Hãy cùng xem và chờ_đợi vậy !
Mạnh_Hải

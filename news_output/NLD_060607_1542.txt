﻿ Hơn 3.000 nhà_đầu_tư tham_gia đấu_giá
( NLĐO ) - Trung_tâm giao_dịch chứng_khoán ( TTGDCK ) Hà_Nội cho_biết , so với cả năm 2005 tổng_số tiền thu thu được qua các phiên đấu_giá trong 5 tháng đầu năm 2006 đã vượt quá 106 tỷ đồng so với mệnh_giá và 220 tỷ đồng so với giá khởi_điểm .
Tổng_số nhà_đầu_tư ( NĐT ) tham_gia đấu_giá đến hết tháng 5-2006 lên tới 3.084 NĐT .
Trong đó , Công_ty_cổ_phần Gas_Petrolimex có số_lượng NĐT đăng_ký đông nhất ( 2.176 NĐT ) , tiếp đến là Đại_lý Hàng_hải Việt_Nam ( 1.608 NĐT ) , Chi_nhánh Cấp_nước Nhà_Bè ( 1.200 NĐT ) và Chi_nhánh Cấp_nước Bến_Thành ( 1.030 NĐT ) . NĐT có tổ_chức chiếm ưu_thế tại các công_ty sau : Công_ty Vận_tải Xây_dựng & amp ; Chế_biến Lương_thực Vĩnh_Hà ( 100% ) , Chi_nhánh Công_ty Xây_dựng & amp ; Phát_triển Đô_thị tại Đà_Lạt & amp ; Cần thơ ( 99% ) , Công_ty Xi_măng Bỉm_Sơn ( 86% ) và CTCP Gas_Petrolimex ( 72% ) .
G.Linh

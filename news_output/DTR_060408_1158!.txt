﻿ Dùng súng dằn_mặt nhau vì … tranh gái
Tuấn đứng_dậy đi thẳng đến bàn của Vũ rồi rút trong người ra một khẩu súng_ngắn , bắn vào giữa bàn Vũ . Hắn lên_đạn định bắn phát thứ_hai nhưng đạn bị kẹt nên cầm súng bỏ chạy .
20h ngày 6/4 , rất đông thực_khách đang ngồi ăn tại quán_ăn Hương_Phố , số 13/7 Lê_Văn_Thọ , phường 12 , quận Gò_Vấp , TPHCM đã chứng_kiến cuộc ẩu_đả giữa hai nhóm thanh_niên . Liền sau đó là một tiếng súng nổ chát_chúa làm một người bị_thương vào chân . Lực_lượng Công_an quận Gò_Vấp đã kịp_thời có_mặt điều_tra vụ_việc .
Qua điều_tra được biết , trước đó Huỳnh_Minh_Tuấn , tức Tuấn “ trắng " , 30 tuổi , trú tại 230/9 / 1 tổ 49 Lê_Văn_Thọ , phường 11 , quận Gò_Vấp cùng đàn_em là Dương_Hải_Nam , Lê_Anh_Tuấn , Đinh_Hữu_Tài đến quán Hương_Phố nhậu .
Khi đến_nơi , nhóm của Tuấn thấy nhóm của Trần_Hoàng_Vũ gồm Nguyễn_Hoàng_Điệp , Nguyễn_Minh_Đức , Nguyễn_Phi_Hùng , Nguyễn_Quốc_Thắng , Đoàn_Dũng_Chinh và Sơn ( chưa rõ địa_chỉ ) đang ngồi nhậu bàn gần bên .
Do_Tuấn và Vũ đã có xích_mích với nhau từ trước về việc cùng giành nhau một cô_gái nên thấy nhau , cả hai đã có thái_độ bực_tức .
Đến khi có hơi men , tên Tuấn đứng_dậy đi thẳng đến bàn của Vũ , sau đó rút trong người ra một khẩu súng_ngắn lên_đạn và bắn một phát vào giữa bàn của Vũ . Viên đạn bay trúng vào chân anh Nguyễn_Quốc_Thắng . Sau khi bắn xong , Huỳnh_Minh_Tuấn tiếp_tục lên_đạn nhưng súng bị kẹt đạn nên đã cầm súng bỏ chạy ra ngoài quán .
Lúc này , tên Dương_Hải_Nam , 22 tuổi , ngụ tại 24A , tổ 172 Thống_Nhất , phường 10 , Gò_Vấp cầm một khẩu súng_ngắn đánh vào đầu anh Nguyễn_Hoàng_Điệp rồi tiếp_tục bắn một phát nữa mới bỏ chạy khỏi hiện_trường .
Qua truy_xét , Công_an quận Gò_Vấp phát_hiện khẩu súng K54 mà tên Huỳnh_Minh_Tuấn sử_dụng là của anh Huỳnh_Công_Tuấn - Cảnh_sát khu_vực Công_an phường 10 , quận Gò_Vấp - cho mượn .
Công_an quận đã mời anh Huỳnh_Công_Tuấn lên làm_việc và Huỳnh_Công_Tuấn đã thừa_nhận có cho Huỳnh_Minh_Tuấn mượn vào ngày 4/4 để lận lưng về quê . Nguồn_gốc khẩu súng này , anh Huỳnh_Công_Tuấn khai đã lấy từ khu khu_vực để vũ_khí quân_dụng hư_hỏng của Nhà_máy Z751 hồi năm 1998 .
Hiện_Cảnh sát khu_vực Huỳnh_Công_Tuấn đã bị tạm đình_chỉ_công_tác 3 tháng để chờ làm rõ hành_vi trên . Cơ_quan điều_tra cũng đã tạm giữ Huỳnh_Minh_Tuấn và đang tổ_chức truy_bắt tên Dương_Hải_Nam
Theo Thanh_Hải_Công_An_Nhân_Dân

﻿ Tưng_bừng míttinh kỷ_niệm 30-4 , thống_nhất đất_nước
& gt ; & gt ; Mời bấm vào đây xem lễ diễu_hành trong chương_trình míttinh kỷ_niệm 30 năm thống_nhất đất_nước .
Đúng 7 giờ , buổi mít - tinh bắt_đầu . Đến dự mít - tinh có Tổng_Bí thư Nông_Đức_Mạnh , Chủ_tịch Quốc_hội Nguyễn_Văn_An , Chủ_tịch nước Trần_Đức_Lương , Thủ_tướng Phan_Văn_Khải , Chủ_tịch UBMTTQ_VN Phạm_Thế_Duyệt ; các đồng_chí Lê_Khả_Phiêu , Võ_Văn_Kiệt , Lê_Đức_Anh … Đặc_biệt , khi Đại_tướng Võ_Nguyên_Giáp xuất_hiện , những tràng pháo tay kéo_dài lẫn tiếng hò_reo chào_đón tưởng_chừng không dứt .
Trong bài diễn_văn của mình , tự_hào về chiến_thắng 30-4-1975 của dân_tộc , bày_tỏ lòng biết_ơn với các nước XHCN anh_em và nhân_dân tiến_bộ trên thế_giới đã giúp_đỡ Việt_Nam , đồng_chí Nguyễn_Minh_Triết , Bí_thư Thành_ủy TP.HCM còn nói : “ Trong niềm_vui vô_hạn ngày miền Nam được giải_phóng , đất_nước thống_nhất , Bắc-Nam sum_họp một nhà , chúng_ta lại phải đương_đầu với những thách_thức hết_sức gay_gắt : kinh_tế kiệt_quệ , hàng vạn người thất_nghiệp , tệ_nạn tràn_lan , đầy_rẫy bom_mìn … Thành_phố bắt_tay ngay vào việc ổn_định an_ninh trật_tự , hàn_gắn vết_thương chiến_tranh , khối phục sản_xuất , chăm_lo đời_sống nhân_dân , bảo_vệ và phát_huy thành_quả cách_mạng .
Từ trong khó_khăn , Đảng_bộ và chính_quyền , nhân_dân thành_phố đã có những bước đột_phá , tháo_gỡ vướng_mắc của cơ_chế cũ , làm sáng_tỏ dần con đường đi và cách làm mới , thúc_đẩy sản_xuất , hình_thành đường_lối đổi_mới của Đảng ta ” . Nhờ đó “ thành_phố đã đạt được những thành_tựu to_lớn ” . Đó không_chỉ là đời_sống người_dân được nâng lên , tích_lũy xã_hội khá mà bộ_mặt cũng thay_đổi nhiều : những dòng kênh đen đang dần biến_mất , nhường chỗ cho nhà cao_tầng ; những vùng_đất hoang_hoá , kém năng_suất giờ_đây là nhà_máy , khu_công_nghiệp … .
Trong bài diễn_văn của mình , Bí_thư Thành_ủy cũng thừa_nhận thành_phố vẫn còn nhiều yếu_kém . Đó là kinh_tế phát_triển chưa vững_chắc , hạ_tầng kỹ_thuật và xã_hội chưa đáp_ứng yêu_cầu phát_triển kinh_tế và cải_thiện dân_sinh , quy_hoạch và quản_lý đô_thị còn yếu_kém , nhiều vấn_đề văn_hoá - xã_hội còn phức_tạp , tệ quan_liêu , lãng_phí vẫn còn nhiều …
Tuy_nhiên Bí_thư Thành_uỷ khẳng_định : TP.HCM sẽ động_viên mọi nguồn_lực , chủ_động nắm_bắt thời_cơ , vượt qua thách_thức và thúc_đẩy kinh_tế phát_triển , chủ_động hội_nhập kinh_tế khu_vực và thế_giới … để “ từng bước trở_thành một trung_tâm công_nghiệp , dịch_vụ , khoa_học - công_nghệ của khu_vực Đông_Nam Á” .
Ông kêu_gọi đồng_bào các giới , bà_con VN ở nước_ngoài … hăng_hái đầu_tư vốn , công_sức và trí_tuệ , đầu_tư nhiều hơn_nữa để thành_phố phát_triển mạnh_mẽ hơn .
Chủ_tịch nước Trần_Đức_Lương cũng phát_biểu và ghi_nhận : TP.HCM là cái nôi của nhiều phong_trào cách_mạng và nhiều phong_trào , chương_trình có sức lan_tỏa , trở_thành mẫu_mực phát_triển trong toàn_quốc như chương_trình xoá_đói_giảm_nghèo , phụng_dưỡng mẹ VN anh_hùng , phẫu_thuật nụ_cười … Dịp này Chủ_tịch trần Đức_Lương cũng đã trao cờ và danh_hiệu Anh_hùng lao_động thời_kỳ đổi_mới cho đồng_bào , chiến_sĩ TP.HCM như một sự ghi_nhận những nỗ_lực đặc_biệt của thành_phố trong 30 năm qua .
Bạn_Phạm_Thị_Thanh_Uyên , Thạc_sĩ Anh_văn , Đảng_viên - sinh năm 1975 - đại_diện cho hàng trăm_ngàn bạn trẻ , phát_biểu : " Thế_hệ trẻ VN hôm_nay sẽ phấn_đấu học_tập và làm_việc , sáng_tạo bằng tinh_thần tiến_công cách_mạng của cha_anh đi trước ; sẽ tiến_công vào lĩnh_vực kinh_tế , khoa_học_kỹ_thuật … để đưa Việt_Nam tiến lên thành quốc_gia hùng_cường " .
Sau lễ mít - tinh , hàng chục_ngàn chiến_sĩ và công_nhân viên_chức thuộc các sở ngành , công_ty , sinh_viên học_sinh … đã tham_gia diễu_hành và trình_bày các màn xếp hình nghệ_thuật .
9 giờ , buổi mít - tinh trọng_thể kết_thúc nhưng các nhà_báo nước_ngoài vẫn còn làm_việc : ở khắp các ngả công_viên , họ háo_hức tìm gặp các tướng_lĩnh , cựu_chiến_binh để phỏng_vấn .
Các đại_biểu đến tham_dự lễ mítting ( từ trái qua ) : Chủ_tịch UBND_TP . HCM Lê_Thanh_Hải , nguyên Tổng_bí_thư Lê_Khả_Phiêu , Thủ_tướng Phan_Văn_Khải , Đại_tướng Võ_Nguyên_Giáp , Chủ_tịch Quốc_hội Nguyễn_Văn_An , nguyên Chủ_tịch nước Lê_Đức_Anh , Bí_thư thứ_hai BCH_TW Đảng CS Cuba_Raul_Castro_Ruz , Chủ_tịch nước Trần_Đức_Lương và Tổng_Bí thư Nông_Đức_Mạnh
Tổng_Bí thư Nông_Đức_Mạnh và Bí_thư thứ_hai BCH_TW Đảng CS Cuba_Raul_Castro_Ruz chào các đại_biểu cựu_chiến_binh và nhân_dân thành_phố
Thủ_tướng Phan_Văn_Khải chào người_dân thành_phố
Chủ_tịch nước Trần_Đức_Lương trao_tặng danh_hiệu Anh_hùng lao_động cho lãnh_đạo TP.HCM
Rợp trời bong_bóng bay thanh_bình .
Tuổi_trẻ TP.HCM tham_gia diễu_hành
Diễu_binh qua lễ_đài
Các dân_tộc anh_em đang diễu_hành qua lễ_đài
Hoạt_cảnh xe tăng_tiến vào giải_phóng Sài_Gòn
Xe_hoa diễu_hành
Diễu_hành của thiếu_nhi thành_phố
Đường_phố Sài_Gòn trước giờ khai lễ
Cô dân_quân Mười_tám thôn vườn trầu Nguyễn_Thị_Thu_Hồng
ĐẶNG_ĐẠI - Ảnh : T . T . D

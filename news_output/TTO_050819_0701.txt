﻿ Trường CĐ bán_công Hoa_Sen xét tuyển hệ kỹ_thuật_viên
Các ngành xét tuyển gồm ngành kỹ_thuật thương_mại ( xét tuyển khối A , D ) , Anh_văn thương_mại ( D ) , đồ_họa kiến_trúc ( A , D , H , V ) , thư_ký y_khoa ( A , B , D ) , kế_toán - tin_học ( A , D ) , thư_ký văn_phòng ( A , C , D ) , công_nghệ web ( A ) , đồ_họa multimedia ( A , D ) và mạng máy_tính ( A ) .
Điều_kiện xét tuyển : TS có điểm thi ( không nhân hệ_số ) các khối A , B , H , V từ 12 điểm trở_lên ; khối C , D từ 10 trở_lên . TS đăng_ký vào ngành thư_ký văn_phòng và kế_toán tin_học phải từ 20 tuổi trở_lên và TS vào ngành đồ_họa kiến_trúc sẽ phải qua kỳ phỏng_vấn . Trường nhận hồ_sơ từ nay đến hết ngày 26-8 .
MINH_GIẢNG

﻿ Lâm_Đồng : đình_chỉ trưng_bày nghệ_thuật sắp_đặt tại Đà_Lạt_Sử_Quán
Theo ông Thể , các hoạt_động cùng những vật_thể trưng_bày quái_dị , nhân_danh “ nghệ_thuật sắp_đặt ” ở Đà_Lạt_Sử_Quán chưa được sự cho_phép của cơ_quan quản_lý_nhà_nước về văn_hóa , gây phản_cảm , ảnh_hưởng đến văn_hóa du_lịch Đà_Lạt .
Nếu_Công ty XQ không xin_phép trình_diễn nghệ_thuật sắp_đặt và kịch_bản cho hoạt_động nghệ_thuật sắp_đặt chưa thông_qua cơ_quan hữu_trách thẩm_định , thì lệnh cấm hoạt_động vĩnh_viễn nghệ_thuật sắp_đặt ở Đà_Lạt_Sử_Quán sẽ được áp_dụng .
Tuổi_Trẻ đã có bài phản_ánh về hoạt_động nghệ_thuật sắp_đặt ở khu du_lịch Đà_Lạt_Sử_Quán , trong đó có nhắc đến những “ chiêu ” tạo ra sự quái_dị , hay dùng chữ_nghĩa để kích_thích ... thị_giác của du_khách .
N . H . T .

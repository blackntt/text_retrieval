﻿ Bước cản khi người_mẫu nam xuất_ngoại
Người_mẫu Hồ_Khánh_Trình - Ảnh : Sài_Gòn_Tiếp_Thị_Mỗi năm cũng chỉ có một cuộc_thi siêu_mẫu , còn an_ủi là một_nửa dành cho nam . Nhưng đó cũng chỉ là cuộc_thi mang tính_chất tìm_kiếm người_mẫu tương_lai nên những người_mẫu đã có_tiếng ngại không tham_dự .
Người_mẫu nam của mình muốn đi ra thế_giới trước_nhất phải_biết tiếng Anh tốt , sau đó mới nói đến những chuyện khác .
Quang_Hòa : Lúng_túng với trang_phục dân_tộc
Trong cuộc_thi Manhunt vừa_qua , anh không chuẩn_bị được phần thi năng_khiếu . Ngoài_ra , còn màn thi nào khiến anh mất tự_tin ?
- Đó là phần trang_phục dân_tộc . Trước lúc đi tôi cũng bàn với chị Thúy_Hạnh chọn trang_phục khố Tây_Nguyên , cho khác với áo_dài khăn_xếp của Bình_Minh dạo trước .
Nhưng sang đó trang_phục của tôi không là gì so với họ . Có những nước ở phía Nam_Mỹ , trang_phục cầu_kỳ vô_cùng . Ngay cả các thí_sinh cũng vậy , họ chuẩn_bị thật kỹ , mỗi bộ quần_áo để trong một cái túi riêng , trong đó có đủ các phụ_kiện đi cùng bộ quần_áo . Tôi quên mất rằng quần_áo trắng phải đi với giầy trắng , nên bộ nào cũng đi giầy đen , đó là thiếu_sót của tôi .
Thí_sinh nào gây cho anh ấn_tượng nhất ?
- Đó là những thí_sinh của châu Âu và châu Mỹ , họ đẹp từ gương_mặt đến hình_thể . Trong các cuộc tiếp_xúc mang tính tập_thể họ rất tự_tin , còn châu Á lép_vế quá .
Qua cuộc_thi đó anh học_hỏi được điều gì ?
- Rất nhiều . Họ cho tôi thấy độ chuyên_nghiệp đến từng cm . Cuộc_thi cho tôi thấy có một nguồn tiềm_năng người_mẫu nam rất phong_phú mà ở nước mình chưa biết khai_thác hết .
Kế_hoạch trong tương_lai của anh ?
- Vẫn làm người_mẫu thôi , nhưng nói thật lòng chẳng ai sống được bằng nghề người_mẫu hết . Tôi đang vào Sài_Gòn lập_nghiệp , tiền kiếm được từ các sô diễn ít_ỏi cũng chỉ đủ chi_tiêu hằng ngày và thuê nhà , nên tôi còn kiêm thêm việc buôn_bán nữa .
Nguyễn_Bình_Minh : Thể_lực ta yếu hơn
Người_mẫu Bình_Minh - Ảnh : Đẹp_Kinh nghiệm " xương_máu " cho anh qua cuộc_thi Manhunt ?
- Không biết ngoại_ngữ là một điều tai_hại , và thể_lực của tôi yếu hơn của họ . Nhiều thí_sinh nhìn khi họ mặc quần_áo thì rất bình_thường , thậm_chí còn hơi gầy nữa , nhưng trong màn thi áo_tắm thì khác hẳn . Cơ_bắp của họ săn chắc và rất đẹp chứ không bị giống với một vận_động_viên thể_hình .
Lúc đó , để kiếm được tài_trợ anh gặp những khó_khăn gì ?
- Đây cũng là một điều khiến cho các thí_sinh VN không_thể thoải_mái thi được . Lúc chuẩn_bị đi , trang_phục dạ_hội tôi cũng được tài_trợ , nhưng áo_dài khăn_xếp lại hơi giống với Trung_Quốc nên người_ta đã nhầm tôi với thí_sinh của Trung_Quốc .
Cuộc_thi đã giúp anh thành ngôi_sao . Anh nghĩ gì về nhận_xét này ?
- Phải cảm_ơn cuộc_thi rất nhiều , vì sau khi tham_dự về tôi đã được rất nhiều công_ty chú_ý , sô diễn cũng nhiều hơn , có cả những lời mời quanh khu_vực nữa . Đi thi không_những chỉ “ tiếp_thị ” cho chính mình mà_còn “ tiếp_thị ” cho vẻ đẹp con_người của đất_nước .
Thanh_Long ( Giám_đốc Công_ty PL ) : Khó kiếm tài_trợ
Theo anh để đưa một người_mẫu nam đi thi quốc_tế thường gặp những khó_khăn gì ?
- Đó là vấn_đề tài_trợ . Kiếm tài_trợ rất khó cho một người_mẫu nữ đi thi nói_gì đến người_mẫu nam . Mà chi_phí cho một chuyến đi rất tốn_kém . Với chức_danh của chúng_tôi thì chỉ biết tổ_chức tập_luyện cho họ đi thi mà thôi .
Chính vì_thế các công_ty trong nước hiện_nay thường chọn những cuộc_thi tổ_chức ở các nước gần với mình , quanh khu_vực , chứ nếu tiến đến các nước Âu , Mỹ thì khó_khăn thật .
Thế còn kinh_nghiệm thì_sao ?
- Cái này chúng_ta chưa đi vào chuyên_nghiệp nên đương_nhiên gặp khó_khăn . Ở các nước khác họ có hẳn một công_ty chuyên tổ_chức đưa các thí_sinh đi thi .
Xuân_Lan : Người_mẫu nam hứa_hẹn hơn nữ
Chị nhận_xét thế_nào về người_mẫu nam của VN ?
- Người_mẫu nam chỉ được xuất_hiện ít_ỏi trong những sô diễn lớn chuyên về thời_trang , mà các chương_trình này thì hiếm lắm . Còn những đại nhạc hội , hoặc hội_nghị khách_hàng đa_phần dùng người_mẫu nữ là chính . Đó cũng là điều thiệt_thòi rất lớn cho các người_mẫu nam , trong khi đó họ lại nhiều triển_vọng hơn nữ nếu tiếp_cận thị_trường thời_trang quốc_tế .
Thời_trang quốc_tế có hẳn những sô diễn cho nam_giới , hoặc có bộ sưu_tập riêng cho nam_giới . Mình chưa làm được điều đó , và tôi nghĩ cũng còn lâu lắm mới thực_hiện được .
Tôi đang có kế_hoạch thực_hiện những sô thời_trang lớn , trong đó chúng_tôi đặt_hàng nhà_thiết_kế , thiết_kế riêng từng bộ sưu_tập cho nam_giới và biểu_diễn từng mảng riêng_biệt .
Chị đánh_giá cao những_ai trong số người_mẫu nam ?
- Vì hoạt_động của công_ty chúng_tôi chỉ bó gọn tại TP.HCM nên ở Hà_Nội tôi không biết nhiều lắm . Vừa_rồi có Quang_Hòa đi thi Manhunt cũng đang là niềm hy_vọng . Ngoài_ra còn có Trần_Doãn_Tuấn , Minh_Diệp , Văn_Liêm , Tô_Bình , Thanh_Phương ...
Để có_thể nổi_bật trong thế_giới thời_trang , người_mẫu nam nên chuẩn_bị sẵn sự tự_tin cho mình , mà điều này rất dễ với nam , vì họ có khả_năng làm ồn_ào một đám đông mà vẫn cuốn_hút và thanh_lịch .
Theo Người_Đẹp

﻿ Huy_động nguồn_lực hỗ_trợ các nước bị sóng_thần
Tại cuộc họp_báo trưa 17-3 , Bộ_trưởng Bộ GD - ĐT Nguyễn_Minh_Hiển - vừa được bầu giữ chức chủ_tịch SEAMEC nhiệm_kỳ 40 - cho_biết : VN đã được các quốc_gia thành_viên đánh_giá cao trong việc khởi_xướng diễn_đàn về chất_lượng và công_bằng giáo_dục cho trẻ_em có hoàn_cảnh khó_khăn , trẻ khuyết_tật .
Đặc_biệt , hội_nghị đã thông_qua một tuyên_bố của Hội_đồng Bộ_trưởng giáo_dục các nước Đông_Nam Á về việc huy_động nguồn_lực thông_qua mạng_lưới của tổ_chức này để hỗ_trợ hệ_thống giáo_dục các nước trong khu_vực vừa bị ảnh_hưởng nặng_nề của sóng_thần , nhất_là hỗ_trợ , giúp_đỡ những trẻ_em có hoàn_cảnh khó_khăn , mồ_côi .
Hội_nghị cũng thống_nhất SEAMEC 41 sẽ được tổ_chức tại Singapore vào tháng 4-2006 .
THANH_HÀ

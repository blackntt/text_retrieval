﻿ Chiêm_ngưỡng " chân dài " nóng_bỏng nhất thế_giới trên sân_cỏ
Vì chiều chồng mà Victoria_Beckham đã cùng cậu con_trai đến tận sân_vận_động Frankfurt của nước Đức để cổ_vũ . Nhưng còn đối_với siêu_mẫu đắt_giá nhất thế_giới - Gisele_Bundchen thì_sao , vì lẽ gì mà cô cũng ra sân_cỏ tung_hứng trái bóng tròn ?
Tất_cả chỉ vì cô chính là người Brazil - một thành_viên nhỏ_bé của một đất_nước mà trong mỗi người_dân đều có niềm đam_mê chung cháy_bỏng đó là bóng_đá . Một đất_nước đã sản_sinh ra rất nhiều ngôi_sao sáng chói trên bầu_trời bóng_đá thế_giới như Roberto_Carlos , Ronaldinho , Ronaldo , Kaka ...
Và đúng là chả có gì phải hỏi khi thấy Gisele_Bundchen ra sân_cỏ với trái bóng tròn cả .
Chụp ảnh kỷ_niệm với đồng_đội trước giờ thi_đấu .
Nhảy lên đánh_đầu cướp bóng ...
Tranh bóng quyết_liệt với các đổi thủ đáng gờm của đội bạn .
Vài giây họp_mặt bàn_bạc thay_đổi chiến_thuật với đồng_đội .
Một khoảnh_khắc đẹp trên sân_cỏ .
Một pha đánh_đầu đỡ bóng ... Manơkanh_Theo_Chinadaily

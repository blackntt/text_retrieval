﻿ Diva nhạc soul Whitney_Houston biểu_diễn tại điện Kremlin
Nữ ca_sĩ 41 tuổi này đang chuẩn_bị cho buổi biểu_diễn trong hai ngày tại điện Kremlin sau buổi trình_diễn mở_màn tại thành_phố Saint_Petersburg trong chuyến lưu_diễn Châu_Âu và Châu Á này của cô .
Các nguồn tin Nga cho_biết Houston sẽ được trả khoảng một_triệu USD cho buổi trình_diễn này và các fan hâm_mộ cô sẽ phải dành ra đến 1300 USD cho vé ngồi hạng nhất tại điện Kremlin - giá vé gần gấp 10 lần lương thu_nhập trung_bình hằng tháng của người Nga ( lương trung_bình hằng tháng ở nước Nga khoảng 150 USD , mặc_dù con_số thu_nhập ở thủ_đô Moscow là gần 1000 USD ) .
Buỗi trình_diễn đầu_tiên của nữ ca_sĩ Carey tại Moscow vào tháng 9 năm_ngoái cũng có_giá vé tương_đương .
Houston đã đoạt 6 giải Grammy trong sự_nghiệp ca_hát của cô và những ca_khúc của cô thường_xuyên được phát_sóng trong chương_trình nhạc pop trên đài_phát_thanh Nga .
L.TH . ( Theo AFP )

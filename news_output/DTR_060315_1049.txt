﻿ Trang_điểm theo " sao " : Từ A đến Z
Bạn có biết năm nay người_ta ưa màu mắt gì , màu son gì không ? Hãy ngắm nhìn các người_đẹp dưới đây để cập_nhật các xu_hướng trang_điểm mới nhất và để không bị lạc_hậu nhé .
Những hàng mi ấn_tượng
Nên : Kết_hợp với màu mắt nhẹ_nhàng
Không nên : Kết_hợp với màu son đậm , hãy trang_điểm các phần còn lại thật đơn_giản .
Lindsay_Lohan - Eva_Longoria
Bầu mắt màu vàng
Nên : Dùng nhũ mắt màu đậm thay cho chì kẻ viền mắt .
Không nên : Dùng bất_cứ thứ gì lấp_lánh trên khuôn_mặt bạn nữa , kẻo trông bạn sẽ giống như một “ nữ_hoàng sàn_nhảy ” .
Mischa_Barton - Beyonce_Knowles
Đánh_phấn nền màu vàng
Nên : Phủ cho làn da của bạn một lớp phấn phủ phát sáng . Trước đấy đừng quên dùng kem giữ ẩm và phấn nền có màu sáng hơn màu_da để tạo cho khuôn_mặt một vẻ đẹp lung_linh huyền_ảo .
Không nên : Dùng phấn màu đồng , nó sẽ làm sạm da bạn .
Jessica_Alba - Ashley_Olsen
Buộc tóc thành lọn cao
Nên : Sau khi buộc tóc thành lọn cao sau gáy , bạn nên dùng thêm một_chút keo giữ nếp tóc .
Không nên : Đừng lấy kiểu tóc này ra làm giải_pháp cho bệnh lười_biếng , hãy nhớ là phải chải và buộc gọn_gàng cẩn_thận các lọn tóc ra sau gáy .
Keira_Knightley - Sheryl_Crow
Son môi bóng và trong_suốt
Nên : Làm nổi_bật màu môi tự_nhiên vốn có_của bạn bằng một lớp son bóng nhẹ màu be sáng hoặc màu đồng . Với màu son này , bạn có_thể thỏai_mái chọn_lựa màu mắt .
Không nên : Dùng son dưỡng môi xỉn màu .
Halle_Berry - Hilary_Swank
Phấn mắt màu xanh lá cây
Nên : Phủ cho mí mắt của bạn bằng màu mắt_xanh lá cây nhẹ_nhàng . Trông bạn sẽ rất “ tinh_khiết và sạch_sẽ ” .
Không nên : Dùng màu xanh lá cây quá đậm .
Scarlett_Johansson - Nia_Long
Son môi màu hồng
Nên : Hãy làm hồng cho đôi môi xinh_xắn của mình bằng một lớp son nước_màu hồng trong_suốt . Đây là cách tốt nhất để tạo vẻ tươi_tắn cho khuôn_mặt .
Không nên : Dùng chì kẻ viền môi .
Eva_Mendes - Kate_Beckinsale
Để tóc xõa không theo trật_tự
Nên : Sấy phồng tóc ngay từ khi tóc bạn đang ướt , hoặc dùng các loại keo phồng tóc để tạo kiểu . Với kiểu tóc này hãy nhớ là bạn phải trang_điểm thật tinh_tế để tạo cho mình dáng_vẻ gợi_cảm nếu_không trông bạn sẽ giống một cô nàng lôi_thôi ngay .
Không nên : Tạo sóng quá nhiều cho kiểu tóc này .
Naomi_Watts - Teri_Hatcher
Búi tóc cao và tạo kiểu hơi lộn_xộn
Nên : Hãy giữ cho tóc mềm và tự_nhiên khi cặp cao lên .
Không nên : Vuốt hết tóc ra đằng sau , hãy để các sợi tóc lòa_xòa trước mặt . Nếu_không bạn sẽ có một búi tóc giống như các cô diễn_viên balê .
Michelle_Williams - Rachel_Weisz_Nguyễn_Minh_Theo_People

﻿ Mở_rộng điều_tra vụ lộ đề tại Trường trung_học BCVT& amp ; CNTT_III
Trong quá_trình điều_tra vụ lộ đề thi môn điện_thoại - fax xảy_ra ngày 23-3 , Công_an Tiền_Giang phát_hiện đề thi có dấu_hiệu bị lộ thường_xuyên , có hệ_thống từ nhiều năm trước với mức_độ nghiêm_trọng .
Theo một_số giáo_viên Trường THBCVT& amp ; CNTT_III , tình_trạng lộ đề thi và đáp_án xảy_ra rất nhiều , đặc_biệt là các kỳ thi hết môn , thi nâng bậc cho công_nhân bưu_điện các tỉnh từ Bình_Thuận đến Cà_Mau . Thậm_chí , học_sinh còn tìm đến tận nhà giáo_viên đặt vấn_đề mua đề thi !
V.TR .

﻿ Baghdad : Thành_phố tồi_tệ nhất thế_giới
Mercer đã tổ_chức thăm_dò trên 215 thành_phố toàn_cầu , khảo_sát tất_cả các mặt của cuộc_sống đo thị nơi hệ_thống y_tế , công_trình công_cộng , trình_độ dân_trí , tình_hình chính_trị ... và lấy New_York là thành_phố chuẩn với số điểm 100 . Tình_hình bất_ổn và sự tàn_phá của chiến_tranh đã đẩy Baghdad xuống chót bảng xếp_hạng năm 2003 - 2004 .
Thụy_Sĩ vẫn đứng đầu_bảng xếp_hạng với 3 thành_phố đứng trong Top_ten là Zurich ( hạng nhất ) , Geneva ( hạng nhì ) và Bern ( hạng 5 ) .
Một khảo_sát riêng về hệ_thống y_tế và công_trình công_cộng cho thấy Canada là nước tốt nhất với thủ_đô Ottawa dẫn_đầu danh_sách và 2 thành_phố Montreal và Vancouver cũng đứng trong Top_ten . Thủ đo Baku của Azerbaijan là thành_phố có hệ_thống hỗ_trợ sức_khỏe người_dân tồi nhất với sự thiếu_hụt thuốc_men nghiêm_trọng và hệ_thống rác_thải cực_kỳ tồi_tệ .
Bóng_ma 11/9 cũng đã xa dần New_York khi thành_phố này nhảy lên 6 bậc trong bảng xếp_hạng ( từ hạng 44 lên hạng 38 ) .
Top 10 thành_phố tốt nhất thế_giới và điểm so với New_York
Top 10 thành_phố tệ nhất thế_giới và điểm so với New_York
ANH_QUÝ

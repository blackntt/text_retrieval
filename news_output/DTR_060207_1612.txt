﻿ Vẫn là cuộc tranh_chấp tốp đầu !
Inter_Milan không_thể san_lấp điểm_số với Juventus khi họ để thua đối_thủ tới 8 điểm . Đối_thủ của họ là Fiorentina cũng chẳng hơn gì , kém Juventus tận 15 điểm . Có_vẻ , thế độc_tôn của Juve khó có_thể bị đe_doạ trong một_vài vòng đấu tới vì đối_thủ của họ chỉ là Parma ...
Inter_Milan - Fiorentina : Căng_thẳng đến nghẹt_thở !
Cả 2 đội đều muốn giành chiến_thắng trong trận đấu này để tạo cơ_hội và sức_ép lên " Bà đầm_già " trên con đường đoạt Scudetto năm nay . Dường_như , kẻ mong_muốn chiến_thắng trong trận đấu này hơn bao_giờ hết là Inter vì khoảng_cách giữa họ với Juve chỉ là con_số 8 .
Nhưng_Fiorentina cũng đang " hồi_sinh " và chân_sút Luca_Toni liệu có để lưới của Inter an_toàn trong_suốt 90 phút thi_đấu ? Trận đấu giữa đội đứng thứ 2 và thứ 4 trong BXH hứa_hẹn nhiều pha bóng hấp_dẫn và căng_thẳng đến nghẹt_thở .
Vòng đấu trước , Inter đã có trận thắng " sít_sao " trước một đối_thủ đã suy_yếu khá nhiều Chievo . Và dù có phản_ứng dữ_dội trước quyết_định thiên_vị cuả trọng_tài Dattilo " vô_tình " mang về chiến thẳng cho đối_thủ Juve , Inter vẫn phải tự cứu mình trong trận đấu với Fio .
Để chuẩn_bị cho chuyến làm_khách ở xứ Toscana trước Fiorentina và đặc_biệt là cuộc tái đấu với Juve , lần đầu_tiên trong mùa giải này HLV Mancini quyết_định tung đội_hình mạnh nhất vào sân để chiếm lợi_thế .
Adriano có tiếp_tục lập_công cho Inter ?
Stankovic được kéo vào trung_tâm đá cặp cùng Veron nhường lại cánh trái cho Kily_Gonzalez , phía cánh phải sẽ là " sân_chơi " của tuyển_thủ BĐN Luis_Figo . Trên hàng công , bộ đôi Adriano - Cruz sẽ rất nguy_hiểm khi có bóng trước khung_thành Fiorentina .
Đội chủ nhà Fio mặc_dù đã gần_như hết cơ_hội theo_đuổi danh_hiệu vô_địch với Juve nhưng chắc_chắn họ sẽ không để Inter qua_mặt . Tuy_nhiên , có một thực_tế của Fiorentina là họ quá phụ_thuộc vào Luca_Toni .
Sau 13 vòng đấu , Toni đã có được 16 bàn , chiếm hơn nửa số bàn thắng của đội bóng áo Tím . Tất_cả các hướng tấn_công chính của Fiorentina đều tập_trung vào một mục_tiêu duy_nhất là Luca_Toni và chỉ cần hàng thủ Inter_Milan tỉnh_táo để bắt chết cầu_thủ này , xem như sức_mạnh của Fiorentina chính_thức bị hủy_diệt .
Ở tuyến giữa của Fio , bộ đôi Donadel - Brocchi sẽ phải đối_mặt với Veron và Stankovic . Fiorentina chắc_chắn sẽ để mình Toni cắm ở trên trong sơ_đồ 4-2 - 3-1 , và như_vậy , nhiệm_vụ của các trung_vệ Inter sẽ không mấy khó_khăn . Inter cần phải thắng để tạo sức_ép lên Juve , đó là điều tất_yếu !
Juventus - Parma : " Ngựa_vằn thành Turin " tiếp_tục sải vó !
Tiếp một đội bóng đang vất_vả trụ hạng như Parma trên sân_nhà , xem như Juventus có quá nhiều cơ_hội bỏ xa đối_thủ Inter trong cuộc đua giành chức vô_địch ở mùa giải năm nay . Chỉ cần không chủ_quan , chỉ cần trọng_tài hơi thiên_vị và chỉ cần ... gặp lại may_mắn như bàn thắng được ghi ở vị_trí việt_vị của Del_Piero ở vòng đấu trước , Juve cầm_chắc 3 điểm .
Các trận đấu khác :
( 8/2/2006 )
Milan - Treviso
Roma - Cagliari
Ascoli - Livorno
Chievo - Sampdoria
Lecce - Empoli
Messina - Siena
Palermo - Lazio
Udinese - Reggina
Tuy_nhiên , phải thừa_nhận đội_hình Juve ở mùa giải năm nay chơi khá ổn_định . Khi các mũi_nhọn tiền_đạo Ibrahimovic - Trezeguet không_thể ghi_bàn thì đã có các cứu_tinh như Del_Piero , Mutu thay_thế .
Tuyến giữa dưới sự chỉ_huy của nhạc_trưởng Pavel_Neved chơi rất chắc_chắn và nhất_là được sự tăng_cường đáng_giá của Vieira , hàng tiền_vệ đã cung_cấp rất nhiều bóng cho hàng công ghi_bàn .
Ngay cả hàng thủ với sự có_mặt của Cannavaro , Thuram , Chiellini và Zambrotta chơi rất ăn_ý và Cannavarro đã từng là cứu_tinh của " Bà đầm_già " khi ghi 2 bàn thắng quyết_định trước Empoli .
Ibrahimovic sẽ tiếp_tục sát_cánh cùng Trezeguet
đem lại thành_công cho Juventus ?
Trấn_giữ khung_thành vẫn là Buffon vừa trở_lại sau chấn_thương và dự_bị cho anh sẽ là Abbiati . Với sơ_đồ chiến_thuật 4-4 - 2 , HLV Capello hoàn_toàn tin_tưởng vào các học_trò sẽ mang lại chiến_thắng ngay tại thánh_địa Delle_Alpi .
Với_Parma , đến làm_khách trên sân Alpi quả_là cực_hình . 1 điểm đối_với họ lúc này cũng là quý lắm rồi . Đoàn quân của HLV Mario_Beretta đang phải vất_vả kiếm từng điểm với nỗi lo trụ hạng .
Hàng_loạt các trụ_cột bị chấn_thương , rời CLB từ đầu mùa giải đã làm Parma suy_yếu . Hiện_Parma đang đứng thứ 14 trong bảng xếp_hạng nhưng chỉ hơn nhóm đội có nguy_cơ xuống hạng có hai điểm , và mỗi trận chiến của họ sẽ là một trận chung_kết để tiếp_tục thoát khỏi nguy_cơ xuống hạng và chắc_chắn một đội bóng ở trong tình_thế như_vậy sẽ vô_cùng nguy_hiểm . Phạm_Quân

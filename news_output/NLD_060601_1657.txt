﻿ Thành_phố New_Orleans lún nhanh hơn dự_đoán
Nước lũ trong trận bão Katrina_Theo kết_quả nghiên_cứu mới công_bố , nhiều khu_vực của thành_phố New_Orleans đã lún nhanh hơn nhiều so với nhận_định trước_đây trước khi cơn bão Katrina tàn_phá thành_phố này vào tháng 8 năm_ngoái .
Nghiên_cứu do nhóm nhà_khoa_học thuộc Đại_học Miami công_bố trên tạp_chí Nature ( Tự_nhiên ) của Anh khẳng_định điều này có_thể giải_thích tại_sao một_số đoạn đê lại dễ_dàng bị nước lũ phá vỡ .
Nghiên_cứu dựa trên các số_liệu thu_thập qua vệ_tinh từ năm 2002 đến 2005 cho_biết thành_phố New_Orleans lún trung_bình mỗi năm 0,5 cm trong giai_đoạn này . Đặc_biệt , một_số khu_vực thấp mỗi năm lún 2,54 cm . Có một đoạn đê của thành_phố đã bị lún hơn 0,91 m so với khi nó được xây_dựng cách đây 30 năm . Điều này gây ra sự lo_ngại về tương_lai của New_Orleans .
Theo nghiên_cứu này , một_số khu_vực nằm rất thấp của thành_phố New_Orleans không_thể xây_dựng lại được , và coi đó như “ những cái bẫy chết người ” . Các khu_vực dự_kiến sẽ gặp rủi_ro cao là Lakeview , Kenner và St_Bernard_Parish .
Các nhà_khoa_học cho rằng việc phát_triển quá mức , sự xuống_cấp của hệ_thống thoát nước và các lớp đất_đá trượt lên nhau là những nguyên_nhân chính gây ra hiện_tượng thành_phố lún dần .
Các nhà_khoa_học cho rằng kết_quả nghiên_cứu mới này cần được xem_xét khi tính_toán việc xây_dựng lại hệ_thống đê phòng_hộ của thành_phố .
Các kỹ_sư Mỹ cho_biết hôm_nay là ngày thành_phố New_Orleans bắt_đầu bước vào mùa lũ năm nay .
Tuy_nhiên , một_số chuyên_gia khí_tượng_thủy_văn cho_biết công_việc xây_dựng lại các đoạn đê bị phá vỡ trong trận bão Katrina chưa được hoàn_thành . Họ dự_đoán trong năm nay có_thể có tới năm trận bão lớn , nhưng sẽ không gây thiệt_hại như các trận bão trong năm 2005 .
Theo Nhân dân / BBC

﻿ Thức_ăn giảm béo
1 . Dưa_chuột . Trong quả dưa_chuột có chất propyl alcohol diacid , có_thể hạn_chế các loại đường thành chất mỡ . Vì_vậy , ăn nhiều dưa_chuột có tác_dụng giảm béo rất tốt .
Dưa_chuột còn chứa Vitamin E giúp thúc_đẩy sự phân_tách các tế_bào , do_đó làm chậm quá_trình lão_hoá da , làm mờ mụn tàn_nhang , mụn sần , nếp nhăn trên da mặt .
2 . Cải_bắp
Có tác_dụng ngăn glucid chuyển thành lipit - một trong những nguyên_nhân gây béo phì . Mặt_khác , thực_phẩm này lại nghèo năng_lượng ( 100g cải_bắp chỉ cung_cấp 502 calo ) . Do_vậy , cải_bắp có tác_dụng chống thừa cân nếu được sử_dụng hàng ngày .
3 . Khoai_tây
Có nhiệt_lượng thấp hơn các loại ngũ_cốc khác nên giúp ta giảm béo hiệu_quả . Ngoài_ra , khoai_tây còn chứa nhiều Vitamin và lycopene , giúp thúc_đẩy quá_trình sinh_sản tế_bào da mới , làm mờ các sắc_tố đen trên da và giúp da bạn ngày_càng láng mịn .
4 . Khoai_lang
Trong khoai_lang chứa nhiều chất mucin ( còn gọi_là mucoprotein ) , có tác_dụng làm giảm thiều chất mỡ dưới da . Khoai_tây còn có tác_dụng duy_trì tính đàn_hồi của thành mạch_máu ở tim , ngăn_chặn xơ_cứng động_mạch .
5 . Sử_dụng gạo làm ngũ_cốc chủ_yếu trong các bữa ăn
Mặc_dù bận đến mấy thì bạn cũng cố_gắng ăn cơm hoặc các sản_phẩm từ gạo , kèm theo đó là cá , rau xanh và các loại quả . Không nên ăn bánh mỳ và các loại bánh ngọt , bánh kem , bánh bơ ... vì các loại bánh này thường chứa nhiều chất_béo , ngọt là một trong những nguyên_nhân gây béo phì . Thanh_Thuỷ ( tổng_hợp )

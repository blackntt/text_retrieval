﻿ Điền_kinh : Vàng_ròng !
Nguyễn_Thị_Tĩnh trên đường đua Ngày_Tĩnh ra_quân thi_đấu đầu_tiên là 8-12 , và đó cũng là ngày kỷ_niệm sinh_nhật lần thứ 21 của cô_gái Hà_Nội này . Đó cũng là ngày sinh_nhật mà cô bảo không_bao_giờ quên trong cuộc_đời của mình bởi hôm ấy cô đã tặng hàng triệu người VN hâm_mộ thể_thao một món quà vô_giá , đó là chiếc HCV cự_ly 400m nữ của SEA Games 22 .
Chưa hết , cô_gái tròn 21 tuổi này như được chắp_cánh để bay trong ngày sinh_nhật khi phá luôn kỷ_lục SEA Games với thành_tích 51 giây 83 ( kỷ_lục cũ của Noodang-Thái Lan lập năm 1993 với 52 giây 60 ) .
Chưa hết , hai ngày_sau ( 10-12 ) , Nguyễn_Thị_Tĩnh lại đoạt HCV thứ_hai tại SEA Games 22 khi thắng quá dễ cự_ly 200m . Tham_gia cự_ly này có bảy VĐV , và Tĩnh đã quá dễ_dàng đoạt HCV khi bỏ xa người về nhì đến 5-6m .
Khi_Tĩnh về đích , trên khán_đài sân Mỹ_Đình đã ầm vang tiếng kêu : “ Phá kỷ_lục SEA Games rồi ” ! Đúng vậy , kỷ_lục SEA Games của cự_ly này là 23 giây 30 của Supavadee ( Thái_Lan ) lập tại Malaysia năm 2001 , trong khi thành_tích của Tĩnh là 23 giây 19 ! Song , thật tiếc khi sau đó thông_báo tốc_độ gió của hôm ấy ở sân Mỹ_Đình lên đến +4,2 m/giây , trong khi qui_định của điền_kinh là chỉ công_nhận kỷ_lục khi tốc_độ gió xuôi_chiều phải từ 2m/giây trở xuống !
Vẫn chưa hết , ở ngày cuối của điền_kinh 11-12 , Tĩnh lại góp công lớn cùng ba đồng_đội Vũ_Thị_Hương , Dương_Thị_Hồng_Hạnh và Nguyễn_Thị_Nụ đoạt luôn cả HCV nội_dung tiếp_sức 4x400m nữ . Ở chiếc HCV này , thành_tích không quan_trọng bằng cách thắng , khi Tĩnh - người chạy cuối_cùng - đã đào_sâu khoảng_cách với đội về nhì là Myanmar đến 20m !
Sửng_sốt tuổi 18
Lan_Anh trên bục vinh_quang Hai tháng trước khi lửa SEA Games 22 bừng cháy trên sân Mỹ_Đình , trong một giải điền_kinh quốc_tế mở_rộng cũng ngay tại Mỹ_Đình , Lan_Anh chỉ xếp_hạng ba sau Khánh_Đoan và Đỗ_Thị_Bông ở cự_ly 1.500m . Cô kém Khánh_Đoan đến gần 4 giây . Gần 4 giây có nghĩa_là khoảng gần chục mét ! Một khoảng_cách diệu_vợi .
Thế_mà tại SEA Games 22 ( ngày 8-12 ) , Nguyễn_Lan_Anh như bay . Khi còn một vòng cuối , đôi chân cô guồng quay_tít bỏ_rơi bà chị Khánh_Đoan lại phía sau ( HCB ) xa tít_tắp đến khoảng 20m ! Ngồi cạnh HLV trưởng đội_tuyển điền_kinh VN , tôi giật bắn người khi ông Hiền_Lương nhảy_dựng : “ Thêm một kỷ_lục SEA Games nữa rồi ” !
Đúng thật , kỷ_lục SEA Games do Sutono của Indonesia lập năm 1997 là 4 phút 21 giây 50 , trong khi thành_tích của Lan_Anh là 4 phút 19 giây 48 ! Sức_mạnh nào đã làm_nên sự tăng_tốc khủng_khiếp chỉ trong vòng hai tháng nếu_không phải là điều kỳ_diệu của tuổi 18 ?
Thế_mà không ai biết suýt chút nữa Lan_Anh không được xuất_trận ở SEA Games 22 . Lý_do vì mỗi quốc_gia chỉ được đăng_ký 2V ĐV tham_gia ở một cự_ly . Vì_vậy , tổ_trưởng tổ huấn_luyện cự_ly trung_bình một_mực căn_cứ vào thành_tích thi_đấu Giải tiền SEA Games nên xếp Đoan và Bông .
Tuy_nhiên , trưởng_đoàn Nguyễn_Hồng_Minh - người đã được phía Hà_nội , đơn_vị gửi Lan_Anh đi tập_huấn Trung_Quốc hai năm nay - khẳng_định cô sẽ có vàng và vượt qua cả Đoan nên quyết_định đưa Lan_Anh vào , để Bông tập_trung vào cự_ly 800m .
Thu_Cúc rửa hận cho Bích_Hường !
Vũ_Bích_Hường là một tài_năng lớn của điền_kinh VN . Tại các kỳ SEA Games , cô thường tham_gia thi_đấu 100m rào và bảy môn thể_thao phối_hợp . Tuy_nhiên , chưa bao_giờ Hường đứng trên bục cao nhất ở bảy môn phối_hợp .
Nỗi hận đó đã được cô_gái đất Tây_Đô giúp đòi lại một_cách vinh_quang . Trong số bảy môn thi , Nguyễn_Thị_Thu_Cúc dẫn_đầu đến năm môn là 100m rào , nhảy_cao , đẩy tạ , chạy 200m , 800m ( chỉ chịu_thua ở hai môn là nhảy_xa và ném lao ) . Với phong_độ ổn_định này , Cúc đã đoạt HCV với 5.274 điểm , hơn người về nhì Masim ( Thái_Lan ) 16 điểm .
Trúc_Vân qua_mặt Sutono
Supriati_Sutono là một tượng_đài của cự_ly trung_bình và dài của điền_kinh Đông_Nam Á . Cô chính là người đang giữ kỷ_lục SEA Games 10.000m ( và cả kỷ_lục 1.500m vừa bị Lan_Anh lật_đổ ) . Đến VN 2003 , cô không dự cự_ly 1.500m và cho_biết mình chỉ tập_trung cho cự_ly 10.000m với mục_tiêu bảo_vệ HCV .
Tuy_nhiên , Đoàn_Nữ_Trúc_Vân - cô_gái quê Khánh_Hòa năm nay 25 tuổi - đã ngăn_cản Sutono thực_hiện điều đó . Chỉ trong ba vòng cuối , Vân trắng_trẻo và nhỏ_nhắn đang luôn ở giữa tốp , bỗng_dưng bứt_phá mãnh_liệt để bỏ tất_cả lại phía sau , băng_băng về đích đoạt HCV_SEA Games đầu đời của mình .
Bông và cuộc lật_đổ ngoạn_mục
Tham_gia cự_ly 800m nữ có ĐKVĐ_SEA Games_Phạm_Đình_Khánh_Đoan , ĐKVĐ châu Á_Yin_Yin_Khine ( Myanmar ) , Đỗ_Thị_Bông - người chưa bao_giờ thắng nổi Đoan ở các giải trong nước , cùng hai tay_đua khác của Myanmar và Thái_Lan .
Trước khi súng nổ , người_ta chú_ý nhiều đến cuộc đua_tranh giữa Đoan với Yin_Yin_Khine chứ chẳng ai để_ý Bông . Nhưng rồi chỉ vài mươi giây sau khi xuất_phát , Yin nhanh_chóng tụt lại phía sau và bước chạy tỏ ra khập_khễnh chứng_tỏ chấn_thương vẫn chưa hồi_phục .
Kết_thúc một vòng đầu , điền_kinh VN đã biết chắc có vàng ở cự_ly này , chỉ có_điều không biết thuộc về ai khi Bông chẳng chịu nhường bà chị một bước nào .
Còn khoảng hơn 100m cuối , Bông bứt_phá mãnh_liệt và về đích trước Đoan đến ba , bốn bước chạy ! Đỗ thị Bông năm 2003 vừa tròn 20 tuổi , quê ở Thừa_Thiên - Huế .
Lê_Văn_Dương “ rửa mặt ” cho đàn_ông !
Người_ta vẫn gọi môn điền_kinh VN là “ âm thịnh dương suy ” , khi từ chiếc HCV đầu_tiên tại SEA Games 18 đến trước SEA Games 22 , duy_nhất chỉ mỗi mình Phan_Văn_Hóa kiếm được HCV 800m vào năm 1999 . Và phải đợi đến SEA Games 22 nam_giới mới có thêm được một HCV nữa do chàng_trai 18 tuổi quê ở Kiên_Giang đoạt được , và cũng ở cự_ly 800m !
Nói trước bước không qua !
Trước khi khai_mạc SEA Games 22 , hai nhân_vật được kỳ_vọng nhiều nhất ở điền_kinh là Bùi_Thị_Nhung và Nguyễn_Duy_Bằng , đều ở môn nhảy_cao .
Với_Nhung , cô là đương_kim vô_địch châu Á với thành_tích 1,88 m ; còn Bằng vừa_mới lập kỷ_lục quốc_gia trước khi diễn ra SEA Games hai tháng với thành_tích 2m20 . Nếu cả hai giữ được phong_độ này tại SEA Games , việc kiếm hai HCV ở nhảy_cao là nằm trong tầm tay .
Chính vì_vậy , một tháng trước khi khai_mạc , tất_cả cứ chuẩn_bị như_là HCV đã nằm trong túi của cả hai VĐV này , và đợi đến SEA Games là họ chỉ việc thò tay vào lấy HCV ra ! Truyền_hình đã lôi Nhung về quê ở Hải_Phòng làm sẵn một phóng_sự ; Duy_Bằng thì bị lôi đi tập_dượt lễ xuất_quân , lễ khai_mạc …
Kết_quả cả hai đều thất_bại , chỉ nhận được HCB khi Bằng chỉ qua 2,10 m và Nhung chỉ đạt 1,83 m ! Đúng là nói trước bước không qua !
Ông chuyên_gia Misa – HLV nước_ngoài duy_nhất được VN tự_động tăng lương vì đã đạt thành_tích xuất_sắc trước đó , đã phải rên lên : “ Biết thế tôi đề_xuất cho đi nước_ngoài tập_huấn và chỉ về khi sát ngày khai_mạc ” !
Âu cũng là bài_học lớn .
Bất_ngờ từ khán_đài !
Chưa bao_giờ môn điền_kinh ở VN lại gây bất_ngờ như tại SEA Games 22 này . Bất_ngờ không_chỉ vì thành_tích mà_cả trên khán_đài . Trong bốn buổi thi_đấu của điền_kinh tại SEA Games 22 luôn_luôn có ít_nhất khoảng 1 vạn khán_giả đến sân Mỹ_Đình . Không_những thế , ở ngày thi_đấu cuối 11-12 đã có khoảng 3 vạn người đến sân .
TT

﻿ Ngôi_sao điện_ảnh Trung_Quốc_Lý_Liên_Kiệt sắp đi_tu
Lý_Liên_Kiệt hiện là diễn_viên hàng_đầu ở Trung_Quốc với thu_nhập năm_ngoái cao nhất trong số các diễn_viên Trung_Quốc , đạt 140 triệu nhân_dân_tệ ( 17 triệu USD ) . Thời_gian gần đây anh đã theo học các lớp Phật học tại Đài_Loan , Hong_Kong .
Từ nhiều năm qua không ít ngôi_sao ca_nhạc , điện_ảnh Hong_Kong và Trung_Quốc đại_lục đã trở_thành Phật tử , thậm_chí vào chùa tu_hành như Huang_Yuan_Shen , Jackie_Chan ( Trần_Thành_Long ) , Eric_Tsang , Anita_Mui và Leslie_Cheung .
Theo SGGP - THX

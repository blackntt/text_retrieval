﻿ Đề thi học_kỳ 2 : Nhiều môn nằm ngoài chương_trình
Cụ_thể , đề kiểm_tra học_kỳ 2 môn_sinh ( chiều 12-5 ) , khối 11 ban A có câu số 14 nội_dung “ Vì_sao trong thức_ăn của các động_vật ăn cỏ chứa hàm_lượng protein rất ít nhưng các động_vật này vẫn phát_triển ? ” . Câu này chiếm 2 điểm trong đề thi nhưng lại nằm trong chương_trình học_kỳ 1 .
Bên_cạnh đó , đề thi môn vật_lý lớp 11 ban C ( thi vào sáng 12-5 ) cũng rơi vào tình_trạng “ râu_ông_nọ_cắm_cằm_bà_kia ” : câu số 12 ( 1,5 điểm ) yêu_cầu HS chứng_minh cảm_ứng từ tổng_hợp tại một điểm bằng 0 nhưng dữ_kiện lại cho “ cường_độ dòng_điện 5 ampe ” .
Cũng chiều 12-5 , với đề thi môn_sinh khối 10 ban A , nhiều HS đã phải bỏ trống câu số 3 vì bài_toán yêu_cầu tính hiệu_ứng Pasteur ( 2 điểm ) nhưng HS chưa được học cách giải . Ngoài_ra , câu số 5 của đề thi còn cho HS tính … logarith ( 1 điểm ) - kiến_thức thuộc về phần toán_học . Tuy_nhiên , theo các giáo_viên và HS , phần kiến_thức toán_học này HS cũng … chưa được học .
Như_vậy , liên_tiếp trong hai ngày kiểm_tra học_kỳ 2 năm_học 2004 - 2005 chương_trình thí_điểm phân_ban , hầu_hết các đề thi của các môn đều có sai_sót về nội_dung .
HOÀNG_HƯƠNG

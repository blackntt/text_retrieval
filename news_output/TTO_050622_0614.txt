﻿ “ Phóng_sự ” của nhỏ bạn tôi
“ Không dám nguy_hiểm đâu - nhỏ bạn bĩu môi và lôi xềnh_xệch tôi theo nó đến một ngôi nhà khá khang_trang , kiến_trúc kiểu biệt_thự , giọng tự_hào - Nhà ca_sĩ X đó ! Tui đang làm phóng_sự về một ngày của X” .
Rồi nhỏ hăm_hở vào_cuộc : đốt phim , chụp ảnh nhân_vật X đủ mọi tư_thế ( mới thức_dậy , đánh răng , ăn sáng , chọn quần_áo để ra phố , đi diễn ... ) . Mướt_mồ_hôi với những cảnh ... đóng_kịch , nhỏ than với tôi ( nhưng có_lẽ cũng để người ca_sĩ kia nghe ) : “ Nghề báo không dễ đâu , nhưng có cực_khổ vậy mới đáp_ứng nhu_cầu của bạn_đọc ... ” .
Tôi lắc_đầu nhìn nhỏ bạn tác_nghiệp , nhớ lại những tâm_sự ngày nào của nhỏ với bạn_bè khi thi vào ngành báo_chí : cố_gắng tìm ra những mảnh đời biết vượt lên chính mình đáng được người trẻ ngưỡng_mộ , thần_tượng ; đứng về phía người nghèo , người oan_ức ...
“ Thôi , bồ làm phóng_sự tiếp nghe , nhà tui có việc ... ” , tôi kiếm cớ ra về khi quá ngán_ngẩm với thiên phóng_sự ... kỳ_cục , đậm_nét quảng_cáo ngôi_sao này .
Thật_ra tôi nghĩ nhỏ bạn tôi không phải là trường_hợp cá_biệt khi hiện_nay người đọc trẻ chúng_tôi được đọc vô_số bài viết như_thế trên nhiều báo , nhất_là những tờ báo chuyên_ngành in đẹp , nhiều màu trên giấy láng lẫy .
Những bài báo , phóng_sự từ nơi chốn riêng_tư nhất của những nhân_vật “ sao ” lẫn chưa là sao : ăn_mặc ra_sao , ngủ thế_nào ... như một kiểu_mẫu về lối sống cho giới trẻ .
Có_thể nói việc nhìn_nhận sai_lệch giá_trị cuộc_sống nơi không ít bạn trẻ hôm_nay có sự góp_phần tích_cực của những “ tác_phẩm báo_chí ” kiểu đó . Lẽ_nào những cây_bút tập_tễnh vào nghề như nhỏ bạn tôi cho rằng những tác_phẩm như_thế vô_tội trong việc tạo ra những thần_tượng ... ảo ( hiện đang có quá nhiều những xìcăngđan tệ_hại ) trong giới trẻ chúng_ta ? !
B . V . ( huongcomay _ 1712 @ ... )

﻿ Tái_định_cư phải đi trước một bước
Làm_sao phải làm tốt công_tác di_dời , đền_bù giải_tỏa ? Làm_sao để người_dân trong vùng được giải_tỏa an_tâm sinh_sống ? Đó không_chỉ là câu_hỏi đặt ra đối_với các cấp chính_quyền mà_còn thu_hút sự quan_tâm đông_đảo của dư_luận công_chúng .
Thực_tế cho thấy người_dân trong vùng được tái_định_cư phải sinh_sống tạm_bợ nơi này nơi khác , phần_lớn là do công_việc tái_định_cư chưa tốt . Thêm vào đó là công_tác di_dời giải_phóng mặt_bằng với tiến_độ rất chậm . Nên_chăng các cấp quản_lý quan_tâm hơn đến việc tái_định_cư cho dân . Khi xác_định khu_vực nào giải_tỏa thì lập_tức xây_dựng chung_cư mới ngay để đáp_ứng nhà_ở cho dân .
Việc còn lại chính_quyền tích_lũy vốn thật đầy_đủ để sau khi người_dân di_dời xong là bắt_tay làm ngay . Tránh tình_trạng dân phải chờ nhà tái_định_cư , tác_động xấu đến tiến_độ thi_công công_trình . Đồng_thời cũng tránh tình_trạng giải_phóng mặt_bằng được rồi lại phải chờ vốn .
ĐỖ_VĂN_ẢNH

﻿ Website cung_cấp thông_tin ngành Mỹ_thuật , Kiến_trúc tại Pháp
Thiết_kế và thực_hiện các tác_phẩm video và truyền_thông đa_phương_tiện , nghiên_cứu và thực_hiện các dự_án tại các công_ty thiết_kế hoặc kiến_trúc , trang_trí , phục_chế các tác_phẩm nghệ_thuật , giảng_dạy ...
Tất_cả các trường đào_tạo ngành Mỹ_thuật cũng_như Kiến_trúc đều mở_rộng cho sinh_viên nước_ngoài theo các tiêu_chí tuyển_chọn như sinh_viên Pháp . Phần_lớn trong số đó là các trường tư_thục không yêu_cầu bằng_cấp đầu_vào và cấp bằng của riêng trường cho sinh_viên tốt_nghiệp .
Xin giới_thiệu những trang_web hữu_ích cung_cấp thông_tin về ngành Mỹ_thuật cũng_như Kiến_trúc tại Pháp do chị Đoàn_Thị_Thái_Hà , Văn_phòng EduFrance , cũng_như thông_tin các đơn_vị du_học Pháp tại Triển_lãm du_học Pháp lần 7 cung_cấp :
Các trường quốc_gia hoặc trường vùng và các chuyên_ngành đào_tạo
Các lĩnh_vực đề_cập đến dưới đây không_chỉ dành riêng cho từng trường vì các trường ngày_càng có khuynh_hướng mở_rộng tất_cả các lĩnh_vực của nghệ_thuật đương_đại : có những trường chuyên về ngành này hoặc ngành khác chẳng_qua là vì những trường đó đã huy_động được số_lượng giảng_viên - nghệ_sĩ cần_thiết cho chuyên_ngành mà thôi . Một_số trường chưa có trang_web ( Annecy , Dunkerque , La_Martinique , Perpugnan , Rouen , Toulon , Tourcoing ) không được đưa vào danh_sách này .
Nghệ_thuật đương_đại , nghệ_thuật thị_giác , múa , âm_nhạc , điện_ảnh , sân_khấu , viết kịch_bản
- Bordeaux : http : / / www.mairie-bordeaux.fr/ebxarts/presentation.htm
- Caen : http : / / www.unicaen.fr/beaux-arts
- Cergy-Pontoise : http : / / www.ensapc.net
- Clermont-Ferarand : http : / / www.ecoledart.ville-clermont-ferrand.fr
- Nice-Villa Arson : http : / / www.villa-arson.org
Đồ gốm
- Limoges / Aubusson : http : / / www.enad.net
- Tarbes : http : / / www.esac-tarbes.com
Truyền_thông
- Angers : http : / / www.angers.fr/page/p-197/art _ id-613
- Besancon : http : / / www.erba.besancon.com
- Caen : http : / / www.unicaen.fr/beaux-arts
- Lyon : http : / / www.enba-lyon.fr
- Marseille : http : / / www.esbam.fr
- Metz : http : / / www.mairie-metz.fr : 8080/METZ/BOZAR/BOZAR _ ECOLE.html
- Nancy : http : / / www.ensa-nancy.fr
- Nantes : http : / / www.erba-nantes.fr
- Orléans : http : / / www.ville-orleans.fr/iav
- Pau : http : / / www.esac-pau.fr
- Strasbourg : http : / / www.esad-stg.org
- Toulouse : http : / / www.mairie-toulouse.fr
Bảo_tồn và phục_chế các tác_phẩm điêu_khắc
- Tours : http : / / www.esbatours.org
Bảo_tồn và phục_chế tranh vẽ
- Avignon : http : / / www.avignon.fr/fr/pratique/etudier/bart.php
Thiết_kế
- Angers : http : / / www.ville-angers.fr
- Bordeaux : http : / / www.mairie-bordeaux.fr/ebxarts/presentation.htm
- Brest : http : / / www.beauxarts-bretagne.asso.fr
- Cambrai : http : / / www.esa-cambrai.fr
- Epinal : http : / / www.ville-epinal.fr/sport _ culture _ loisirs/ecoles _ cult/ecole _ image.shtml
- Le_Mans : http : / / www.esbam.net
- Limoges / Aubusson : http : / / www.enad.net
- Lyon : http : / / www.enba-lyon.fr
- Marseille : http : / / www.esbam.fr
- Mulhouse : Le_Quai : http : / / www.lequai.fr
- Nancy : http : / / www.ensa-nancy.fr
- Nantes : http : / / www.erba-nantes.fr
- Orléans : http : / / www.ville-orleans.fr/iav
- Pau : http : / / www.esac-pau.fr
- Reims : http : / / www.esad-reims.fr
- Rennes : http : / / www.erba-rennes.fr
- Saint - Etienne : http : / / www.institutdesign.com
- Strasbourg : http : / / www.esad-stg.org
- Tarbes : http : / / www.esac-tarbes.com
- Toulouse : http : / / www.mairie-toulouse.fr
- Valence : http : / / www.erba-valence.fr
- Valenciennes : http : / / www.valenciennes.fr
Chạm_khắc , xuất_bản
- Cambrai : http : / / www.esa-cambrai.fr
- Quimper : http : / / www.beauxarts-bretagne.asso.fr
Hình_ảnh tường_thuật
- Epinal : http : / / www.ville-epinal.fr
Phương_tiện đa truyền_thông , công_nghệ mới , số , video , âm_thanh
- Aix-en-Provence : http : / / www.ecole-art-aix.fr
- Angoulème / Poitiers : http : / / www.eesati.fr
- Lorient : http : / / www.beauxarts-bretagne.asso.fr
- Metz : http : / / www.mairie-metz.fr
- Montpellier : http : / / www.esbama.fr.st
- Orléans : http : / / www.ville-orleans.fr/iav
- Pau : http : / / www.esac-pau.fr
- Quimper : http : / / www.beauxarts-bretagne.asso.fr
- Rennes : http : / / www.erba-rennes.fr
- Rueil-Malmaison : http : / / www.earueil.com
- Valence : http : / / www.erba-valence.fr
Nhiếp_ảnh
- Metz : http : / / www.mairie-metz.fr
- Cherbourg-Octeville : http : / / www.ville-cherbourg.fr
Thảm , Dệt
- Angers : http : / / www.angers.fr/page/p-197/art _ id613
- Limoges / Aubusson : http : / / www.enad.net
- Mulhouse : Le_Quai : http : / / www.lequai.fr
Các trường ĐH
- Rất nhiều trường ĐH đào_tạo bằng Thạc_sĩ định_hướng chuyên_môn ( DESS ) về quản_lý văn_hóa . Tham_khảo thêm tại phiếu thông_tin giới_thiệu về ngành quản_lý văn_hóa . Một_số trường như Aix-Marseille 1 , Amiens , Paris 1 , Paris VIII , Strasbourg , Saint-Etienne , Toulouse II , Valenciennes ... cũng giảng_dạy ngành nghệ_thuật tạo_hình , nghệ_thuật ứng_dụng hoặc lịch_sử nghệ_thuật : http : / / www.culture.gouv.fr/culture/infos-pratiques/formations/arts-plast-formation.htm
Địa_chỉ hữu_ích
- http : / / www.ecoles-arts.com : các trường nghệ_thuật trong cộng_đồng nói tiếng Pháp .
- http : / / www.ensba.fr/liens/ecoles.asp # arts : các trường nghệ_thuật tại châu Âu giới_thiệu bởi ENSBA .
- http : / / www.culture.gouv.fr/culture/salonEducation2000/arts-plastiques.htm
- http : / / www.cnap.fr : Trung_tâm Nghệ_thuật tạo_hình quốc_gia .
Các trường chính
Trường quốc_gia cao_cấp về Mỹ_thuật / ENSBA : Điều_kiện tuyển_sinh dành cho sinh_viên Pháp và sinh_viên nước_ngoài như nhau : bằng tú_tài , tối_thiểu là 18 tuổi và tối_đa là 24 tuổi . Nếu đăng_ký vào năm thứ 2 , yêu_cầu tối_đa là 26 tuổi . Tuyển_sinh dựa trên các tác_phẩm đã thực_hiện và thi_tuyển . Thông_tin thêm tại : http : / / www.ensba.fr/pedagoie/admission/htm
Để có_thể tham_gia chương_trình này , thí_sinh bắt_buộc phải có bằng bằng thạc_sĩ hoặc văn_bằng có giá_trị tương_đương trong khoảng thời_gian ít_nhất là một năm trước đó . Thí_sinh nước_ngoài cần phải có trình_độ tiếng Pháp tốt . Thi_tuyển vào chương_trình bao_gồm các phần sau : xét_duyệt hồ_sơ nghệ_thuật của thí_sinh , những thí_sinh trải qua vòng thi này sẽ tham_dự phỏng_vấn với hội_đồng ban giám_khảo bao_gồm các chuyên_gia hoạt_động trong lĩnh_vực nghệ_thuật .
Trường quốc_gia cao_cấp về nghệ_thuật trang_trí / ENSAD : http : / / www.ensad.fr
Trường quốc_gia cao_cấp về sáng_tạo công_nghiệp / ENSCI / các xưởng nghệ_thuật : http : / / www.ensci.com
Trường quốc_gia cao_cấp về Nhiếp_ảnh Arles : Điều_kiện tuyển_sinh : tú_tài + 2 năm , thi_tuyển đầu_vào . Thời_gian đào_tạo 3 năm , văn_bằng tốt_nghiệp của trường quốc_gia cao_cấp về Nhiếp_ảnh . Nhà_trường cũng tổ_chức các lớp_học tối , du_lịch học_tập và trao_đổi quốc_tế : http : / / www.enp-arles.com
Chương_trình dành cho sinh_viên nội_trú : 1 hoặc 2 học_kỳ về nghiên_cứu và nâng cao trình_độ dành cho sinh_viên nước_ngoài đã có bằng tốt_nghiệp ngành nhiếp_ảnh tại quốc_gia sở_tại . ENSP cung_cấp những phương_tiện kỹ_thuật và hướng_dẫn sinh_viên thực_hiện dự_án nghiên_cứu cá_nhân : http : / / www.enp-arles.com/etudes.htm
Le_Fresnoy - Studio quốc_gia về Nghệ_thuật đương_đại : Trung_tâm đào_tạo , nghiên_cứu và sản_xuất trong mọi lĩnh_vực nghệ_thuật hình_ảnh và âm_thanh . Thi_tuyển đầu_vào dành cho tất_cả các thí_sinh từ 35 tuổi trở_lên , đã có 4 năm_học ĐH hoặc 4 năm kinh_nghiệm nghệ_thuật . Sinh_viên sẽ chọn tiếng Pháp hoặc tiếng Anh khi tham_dự thi_tuyển : http : / / www.le-fresnoy.tm.fr
QUỐC_DŨNG

﻿ Kho thành ... nhà
Cùng năm 1992 , UBND quận Bình_Thạnh có quyết_định giao mặt_bằng trên cho Xí_nghiệp Quản_lý và phát_triển nhà ( QL& amp ; PTN ) quận Bình_Thạnh để cải_tạo thành nhà_ở . Sở Xây dựng cấp giấy_phép khởi_công xây_dựng cho Xí_nghiệp QL& amp ; PTN xây_dựng 10 căn một trệt ba lầu . Ngày 15-12-1997 , CTKB báo_cáo Sở Nhà đất và Cục Quản lý vốn xin xuất_toán tài_sản nhưng không được chấp_nhận ( ? ) .
Tám năm sau ( năm 2004 ) , CTKB lại đề_nghị UBND quận Bình_Thạnh xác_minh tình_hình sử_dụng kho_bãi này ( vì trên danh_nghĩa kho này vẫn do CTKB quản_lý ) , nhưng quận Bình_Thạnh có công_văn từ_chối xác_minh . Thực_tế hiện_nay toàn_bộ mặt_bằng này là nhà_ở .
2 . Kho 552 Xô_Viết_Nghệ_Tĩnh , quận Bình_Thạnh
Diện_tích kho : 366m2 , diện_tích bãi : 1.487m2 .
Mặt_bằng này cũng được CTKB quản_lý từ năm 1988 , ký hợp_đồng cho Công_ty Dịch_vụ ăn_uống quận Bình_Thạnh sử_dụng và thuê . Đến năm 1991 Công_ty Dịch_vụ ăn_uống không tái ký hợp_đồng .
Toàn_bộ mặt_bằng này hiện_nay đã được Xí_nghiệp QL& amp ; PTN quận Bình_Thạnh xây_dựng thành 24 căn nhà có kết_cấu kiên_cố bêtông và đã bán chuyển quyền_sở_hữu cho người sử_dụng . Do mặt_bằng này vẫn nằm trong danh_sách tài_sản của CTKB nên công_ty đề_nghị UBND quận Bình_Thạnh xác_minh tình_hình sử_dụng kho_bãi này , nhưng UBND quận BT từ_chối .
Kho 33 Nguyễn_Hữu_Thoại rộng 1.138m2 biến thành sáu căn nhà 3 . Kho 33 Nguyễn_Hữu_Thoại , quận Bình_Thạnh
Diện_tích : 1.138m2 .
Ngày 27-3-1991 , Sở Nhà đất TP có công_văn đồng_ý cho CTKB ( đơn_vị được giao quản_lý ) chuyển công_năng sử_dụng kho này thành nhà_ở . Sở Xây dựng cũng đã cấp giấy_phép xây_dựng cho CTKB xây sáu căn nhà một trệt một lầu ( giấy_phép số 2301/GPXD ngày 9-10-1991 ) . Như_vậy , từ chức_năng quản_lý kho , CTKB đã tìm cách để biến công_sản này thành nhà_ở .
4 . Kho 88 Gò_Công , quận 5
Diện_tích 251m2 , đã được UBND_TP ký quyết_định xác_lập sở_hữu nhà_nước ngày 6-5-1977 . Công_ty Vật_tư tổng_hợp ( VTTH ) TP tiếp_quản sử_dụng và ký hợp_đồng thuê với CTKB từ năm 1985 . Thay_vì chỉ sử_dụng làm kho_bãi , Công_ty VTTH_TP đem bố_trí cho sáu CBCNV làm nhà_ở từ năm 1997 . Trước đó từ năm 1993 , công_ty đã ngưng ký hợp_đồng với CTKB . Qua xác_minh , sáu hộ này không có hợp_đồng thuê nhà với đơn_vị quản_lý_nhà_nước và cũng không có giấy chứng_nhận quyền_sở_hữu nhà_ở và quyền sử_dụng đất ở .
5 . Kho 176/11 Hậu_Giang , quận 6
Diện_tích kho : 312m2 , diện_tích bãi : 30m2 , đã được xác_lập sở_hữu nhà_nước theo quyết_định của UBND_TP ngày 7-11-1994 và được giao cho CTKB quản_lý từ tháng 2-1995 . Công_ty VTTH_TP ký hợp_đồng thuê của CTKB ( đến năm 1993 thì ngưng ký hợp_đồng ) rồi tự_động cải_tạo thành nhà_ở cho sáu hộ gia_đình CBCNV . Những hộ được bố_trí chưa ký hợp_đồng nào với đơn_vị quản_lý_nhà_nước và không có giấy chứng_nhận quyền_sở_hữu nhà_ở và quyền sử_dụng đất ở .
6 . Kho 958 Lò_Gốm , quận 6
- Tổng diện_tích kho_bãi CTKB đang quản_lý : 188.658,5 m2 ( kho : 97.005,9 m2 , bãi : 92.576,8 m2 )
- Kho_bãi chuyển thành nhà_ở ( lập quĩ nhà tái_định_cư , chung_cư cho thuê ... ) : 51.063,7 m2 ( kho là 28.091,7 m2 , bãi là 26.252m2 )
- Kho đang quản_lý dự_kiến giải_tỏa 100% do nằm trong qui_hoạch : 2.782m2
- Xử_lý thu_hồi kho_bãi : 3.298 m2 ( quận 5 , 8 , 11 , Tân_Bình ) Diện_tích : 348m2 , được xác_lập sở_hữu nhà_nước theo quyết_định của UBND_TP ngày 7-11-1994 và giao cho CTKB quản_lý từ tháng 2-1995 . Kho này do UBND quận 6 là đơn_vị tiếp_nhận , sau đó hoán_đổi kho này với kho 135/18 Hùng_Vương do Công_ty VTTH_TP quản_lý . Sau khi hoán_đổi , Công_ty VTTH_TP thuê của CTKB và cải_tạo bố_trí năm hộ CBCNV làm nhà_ở . Trong năm hộ này có bốn hộ ký hợp_đồng thuê nhà với Công_ty Dịch_vụ công_ích quận 6 , một hộ chưa có hợp_đồng thuê .
7 . Kho 26AB Hùng_Vương , quận 11 ( số mới 398-400 Hùng_Vương )
Diện_tích : 264m2 , đã được xác_lập sở_hữu nhà_nước theo quyết_định của UBND_TP ký ngày 7-11-1994 và giao cho CTKB quản_lý từ tháng 2-1995 . Công_ty VTTH_TP sử_dụng và tự_động bố_trí nhà cho hai CBCNV : căn_số 398 Hùng_Vương đã có giấy chứng_nhận quyền_sở_hữu nhà_ở và quyền sử_dụng đất ở do UBND_TP ký cấp cho ông Nguyễn_Chí_Tâm và bà Nguyễn_Thị_Nguyên_Thủy , căn_số 400 Hùng_Vương và phần lửng sau nhà không có giấy_tờ .
Kho ... bị bỏ_hoang
Kho_An_Phú , quận Thủ_Đức ( số mới : 20 Nguyễn_Đăng_Giai , phường Thảo_Điền , quận 2 ) , diện_tích kho 192m2 , diện_tích 1.746m2 , được xác_lập sở_hữu nhà_nước theo quyết_định của UBND_TP ký ngày 7-11-1994 và giao cho CTKB quản_lý tháng 2-1995 . Trước đó , Công_ty Đầu_tư khai_thác nước sông Sài_Gòn sử_dụng . Năm 1995 , CTKB kiểm_tra và phát_hiện kho này do Công_ty Cấp_nước TP quản_lý và đang bỏ trống , không kê_khai đăng_ký sử_dụng đất với địa_phương .
ĐOAN_TRANG

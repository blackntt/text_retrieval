﻿ Những ông bố hàng “ sao ” của Hollywood
Dù luôn bận_rộn với những kế_hoạch đóng phim và thu đĩa nhưng những ông bố hàng “ sao ” dưới đây luôn cố_gắng dành thời_gian chăm_sóc những công_chúa và hoàng_tử của mình . Dưới đây là chân_dung những ông bố đáng yêu nhất Hollywood :
Seal
Dù đã có hai cậu con_trai , Leni ( 2 tuổi ) con_riêng của Heidi và một Henry ( chưa đầy 1 tuổi ) nhưng nam ca_sỹ người Anh Seal không hề giấu_giếm mơ_ước có thêm một nhóc_tì nữa : “ Nếu may_mắn , chúng_tôi sẽ lại có thêm một cục cưng nữa ” . Không_những thế , Heidi_Klum vợ của Seal ca_ngợi về chồng : “ Seal là một ông bố tuyệt_vời . Anh_ấy thay tã cho con còn cừ hơn tôi ấy chứ ” .
Seal và cậu quý_tử Henry
Ben_Affleck
Mặc_dù là một ngôi_sao điện_ảnh lớn của Hollywood , được nhiều người ngưỡng_mộ nhưng Ben vẫn là một ông bố bình_thường với công_việc yêu_thích là được đẩy nôi cho con_gái đi chơi . Một người bạn của Ben cho_biết : " Anh_ấy thích_thú với vai_trò làm cha . Lúc_nào anh_ấy cũng ở bên con_gái " . Hiện_Ben và Jennifer_Garner đang sống những tháng_ngày hạnh_phúc bên cô công_chúa gần một tuổi Violet .
Ben và con_gái Violet
Kevin_Ferderine
Kevin tâm_sự khoảng thời ý_nghĩa nhất trong ngay là được chơi_đùa bên cậu cả Sean_Preston 9 tháng tuổi : " Tôi dậy sớm cùng bé Seal và cùng chuẩn_bị mọi thứ cho con " . Hiện_Kevin là cha của ba nhóc_tì , Kor ( 3 tuổi ) , Kaleb ( 1 tuổi ) với vợ cũ Shar_Jackson , Sean ( 9 tháng tuổi ) và tương_lai anh và Brit sẽ đón_chào một công_chúa hoặc hoàng_tử vào tháng 10 tới .
Kevin và bé Sean
Chris_Martin
Bé_Moses_Martin sinh vào tháng 4 năm nay thật may_mắn khi có một người cha và người_mẹ tuyệt_vời như Gwyneth_Paltrow và Chris_Martin . “ Cả hai người rất thương con . Lúc_nào cũng muốn tự tay chăm_sóc cục cưng của mình ” , vú_em của nhà Martin cho_biết . Hiện cặp đôi này đã có hai nhóc_tì là bé Mose vừa sinh vào tháng 4 và bé gái Apple đã được 2 tuổi . Được biết , Gwyneth đang tìm hợp_đồng đóng phim cho cô con_gái bé_bỏng của mình .
Chris và bé Moses
Drew_Lachey
" Lúc đó tôi có cảm_giác mình bước vào một cuộc phỏng_vấn để nhận một công_việc tốt nhất trong đời vậy . Và thật may_mắn , tôi đã có được công_việc đó ” , đó là những tâm_sự của Drew_Lachey về sự xuất_hiện của cô_gái Isabella vào tháng 3 vừa_qua . Drew cưng con tới mức , vợ anh Lea phải thốt lên rằng : " Anh_ấy là một người tuyệt_vời , hiền_lành và dịu_dàng . Tôi thích nhất_là được nhìn anh_ấy chăm_sóc và chơi_đùa cùng con_gái " .
Drew và con_gái Isabella
Hugh_Jackman
Ngôi_sao của seri phim_nhựa Dị_nhân tâm_sự : " Con_trai tôi thực_sự rất thích những gì tôi đã đạt được giải_thưởng Nickelodeon năm 2004 ( một giải_thưởng do trẻ_em bình_chọn ) ” . Hiện_Hugh_Jackman đã có hai con là bé trai Oscar ( 6 tuổi ) và mới_đây là bé gái Ava gần được 1 tuổi .
Hugh và con_gái Ava
Gavin_Rossdale
Với ông bố bà_mẹ tài_năng là Gwen_Stefani và Gavin_Rossdale , chắc_chắn bé Kingston_James_Mc Gregor mới sinh hôm 26/5 vừa_rồi có khả_năng sẽ trở_thành một tài_năng lớn trong âm_nhạc . " Thật tuyệt_diệu ! Tôi thích cảm_giác này ! " , Gavin đã nói như_vậy khi biết mình vừa có thêm một cậu con_trai .
Gavin và Kingston
Rod_Stewart
Dù đã có 6 con nhưng Rod_Stewart vẫn hạnh_phúc như người lần đầu được làm cha khi biết tin vị_hôn_thê Penny_Lancaster vừa sinh_hạ cho anh cậu con_trai Alastair . Bất_kỳ ai tới thăm cậu quý_tử nhà Stewart cũng được Rod thao_thao_bất_tuyệt về con : “ Các bạn đang được chiêm_ngưỡng thí_sinh của giải American_Idol năm 2026 đấy ” .
Rod_Stewert và con_trai Alastair
Heath_Ledger
Hiện_Heath_Ledger và bạn_gái Michelle_Williams đang sống tại Brooklyn , N . Y ( Mỹ ) cùng cô con_gái 8 tháng tuổi Matilda_Rose . " Tôi càng_ngày_càng yêu cả hai người phụ_nữ của mình ” , đây là những tâm_sự rất thật của ngôi_sao “ Brokeback_Mountain ” về Michelle và Matilda trong chương_trình talkshow của Oprah_Winfrey .
Heath và cô con_gái Matilda .
Mi_Vân_Theo_People

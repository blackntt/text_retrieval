﻿ Hướng tới SEA Games 23 : Đoàn VN và cuộc_chiến tay_ba
Dù bị các quốc_gia khác phàn_nàn về việc chuẩn_bị cơ_sở vật_chất thiếu chu_đáo , nhưng đoàn VĐV chủ nhà Philippines lại được các nhà tài_phiệt lớn ( đứng đầu các liên_đoàn thể_thao ) tài_trợ mạnh_mẽ cho việc tập_huấn tại nước_ngoài .
Vì_vậy , tuy đứng thứ_năm tại SEA Games 22 , nhưng Philippines đã tuyên_bố lấy từ 109 - 171 HCV/441 nội_dung thi để đạt vị_trí số 1 trong bảng_tổng_sắp SEA Games 23 .
Theo trưởng_đoàn thể_thao VN Nguyễn_Hồng_Minh : " Ở các môn karatedo , taekwondo , Philippines đều tuyên_bố lấy hơn 10 HCV trong số chưa đầy 20 bộ huy_chương mỗi môn .
Lấy lý_do khó_khăn kinh_phí , Philippines không mời lực_lượng trọng_tài trung_lập như VN đã làm tại SEA Games 22 . Điều này khiến các đoàn tham_dự đều nhìn lực_lượng trọng_tài với ánh mắt e_ngại , đặc_biệt ở các môn có cách đánh_giá dựa vào cảm_tính " .
Ông Minh cho_biết thêm trong bối_cảnh như_thế , Thái_Lan ( dự_kiến trên 90 HCV ) , VN ( dự_kiến 60 - 80 HCV ) và Indonesia ( dự_kiến trên 60 HCV ) sẽ cùng nhau tranh_chấp vị_trí thứ_hai và ba chung_cuộc .
VN sẽ chạy_đua ra_sao ?
" Chỉ cần đoạt trên 60 HCV , VN sẽ chắc_chắn ở vị_trí thứ_ba " - ông Minh khẳng_định . Hãy điểm qua lực_lượng của đoàn VN :
Nhìn qua bảng đăng_ký chỉ_tiêu thành_tích của các đội_tuyển , điền_kinh đề ra mục_tiêu đạt 5 - 6 HCV , ngang_hàng với chỉ_tiêu của các môn : wushu , bắn súng và chỉ sau mỗi môn vật ( 7-8 HCV ) .
Tự_tin , HLV trưởng đội_tuyển điền_kinh Dương_Đức_Thủy từng mạnh_dạn tuyên_bố sẽ từ_chức nếu điền_kinh không hoàn_thành chỉ_tiêu trên với các niềm hi_vọng : Bùi_Thị_Nhung , Nguyễn_Duy_Bằng , Đỗ_Thị_Phương , Trương_Thanh_Hằng , Lê_Văn_Dương , Đỗ_Thị_Bông và mới_đây là Vũ_Văn_Huyện ( 10 môn phối_hợp ) .
Ông Minh cũng hi_vọng sẽ có HCV ở môn bơi nếu Nguyễn_Hữu_Việt duy_trì được phong_độ ổn_định ở cự_ly 200m ếch ( từng đạt thành_tích dưới 1 ' 04 ) hay những HCV của Phạm_Văn_Mách , Lý_Đức trên sàn đấu thể_hình ...
NGỌC_LAN

﻿ Bắt vụ buôn_lậu thuốc_lá lớn nhất 5 năm qua
10h15 sáng nay , các trinh_sát phòng PC15 , Công_an Hà_Nội đã bắt vụ vận_chuyển và buôn_bán thuốc_lá điếu nhập_ngoại , thu_giữ 1.060 cây thuốc_lá . Đây là vụ buôn_lậu thuốc_lá lớn nhất trong vòng 5 năm qua bị triệt_phá .
Sau một thời_gian theo_dõi , sáng nay 30/3 , các trinh_sát đã bắt quả_tang Trần_Văn_Khải , sinh năm 1977 trú tại Tam_Dương , Vĩnh_Phúc đang vận_chuyển 25 cây thuốc_lá 555 giấu trong các vỏ thùng bánh_mì Scotti . Khải khai nhận số hàng trên là của Võ_Thị_Loan , sinh năm 1960 , trú tại 18 ngõ 61 , Nguyễn_Sơn , Long_Biên . Khải chỉ là người làm_thuê và hàng ngày vận_chuyển hàng cho Loan .
Võ_Thị_Loan tại trụ_sở công_an .
Ngay sau đó , các trinh_sát phòng PC15 đã thực_hiện lệnh khám_xét nơi ở của Võ_Thị_Loan , thu_giữ một số_lượng lớn thuốc_lá điếu ngoại_nhập lậu bao_gồm 710 cây thuốc_lá hiệu 555 , Mild_Seven … trong số đó có 25 cây 555 có dán tem miễn thuế .
Cùng thời_điểm này , các trinh_sát cũng thực_hiện lệnh khám_xét nơi ở của Vũ_Đình_Mười , sinh năm 1954 , trú tại phường Ngọc_Lâm , Quận_Long_Biên , nơi mà Loan nhờ cất_giấu hàng . Tại đây , công_an thu tiếp 325 cây thuốc_lá các loại trong đó có 80 cây_thuốc 555 có dán tem miễn thuế .
Cả ba đối_tượng trên hiện đã bị bắt_giữ để phục_vụ điều_tra . Công_an Hà_Nội cũng đã khởi_tố vụ_án , khởi_tố bị_can với tội_danh “ Buôn_bán , vận_chuyển hàng cấm ” . Nguyên_Đức

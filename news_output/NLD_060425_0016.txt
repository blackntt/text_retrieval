﻿ Ô_tô đâm mô_tô , 9 người chết , 8 bị_thương
( NLĐ ) - Một vụ tai_nạn giao_thông nghiêm_trọng vừa xảy_ra lúc 10 giờ 10 ngày 24-4 trên Quốc_lộ 1A , đoạn thuộc xã Quảng_Đông , huyện Quảng_Trạch - Quảng_Bình làm 9 người chết , 8 bị_thương . Ô_tô biển_số 14LD-0131 chở 50 khách đi hướng Nam - Bắc , đến địa_điểm trên đã đâm vào mô_tô do ông Mai_Văn_Cống ( 56 tuổi , ngụ Quảng_Trạch ) điều_khiển chở 2 cháu nhỏ đi ngược chiều . Vụ tai_nạn làm 3 người đi mô_tô chết tại_chỗ ; ô_tô chạy thêm gần 200 m thì bị lật làm 6 người trên xe chết , 8 người khác bị_thương .
T.Phùng

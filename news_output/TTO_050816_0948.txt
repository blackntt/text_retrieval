﻿ Essien giá 47 triệu USD
Ông Bernard_Lacombe , một quan_chức của Lyon đã xác_nhận thông_tin này bằng cách thông_báo chính_thức trong một cuộc họp_báo rằng Essien đã ký một hợp_đồng kéo_dài 5 năm với Chelsea và ngày_mai tiền_vệ 22 tuổi người Ghana này sẽ sang Anh để khám sức_khỏe .
Ông Lacombe cho_biết : " Vụ chuyển_nhượng này có hiệu_quả và tầm_cỡ như 2 vụ chuyển_nhượng của Zidane và Beckham " .
Essien đã bắt_đầu sự_nghiệp cầu_thủ tại CLB làng Liberty tại Ghana và trở_thành mục_tiêu săn_đuổi của nhiều đại_gia châu Âu sau khi tham_gia giải VĐTG U-17 tổ_chức tại New_Zealand năm 1999 . Năm 2001 , Essien có tên trong đội_hình Ghana tham_gia giải VĐTG trẻ tổ_chức tại Argentina và chính_thức khoác_áo đội_tuyển Ghana năm 2002 tại cúp châu Phi .
Vào tháng 7-2003 , mặc_dù rất thích được khoác_áo M . U nhưng do không nhận được giấy_phép lao_động tại Anh , Essien buộc phải chuyển sang Lyon với giá 13,5 triệu USD .
Ngay_lập_tức , trong vai_trò tiền_vệ phòng_ngự Essien đã cùng_với Lyon đoạt được chức vô_địch Pháp lần thứ 3 và thứ 4 liên_tiếp của CLB . Sắp tới rất nhiều khả_năng Essien sẽ nhận được danh_hiệu cầu_thủ xuất_sắc nhất giải vô_địch Pháp năm 2005 .
KINH_LUÂN ( Theo AFP )

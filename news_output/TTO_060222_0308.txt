﻿ Đài PT-TH Tiền_Giang quảng_cáo “ chùa ” hơn 2.000 phút !
Bà Phan_Thị_Lý , phó_phòng thanh_tra nội_chính - văn xã , Thanh_tra Nhà_nước tỉnh Tiền_Giang , cho_biết như_vậy .
Tháng 9-2004 , giám_đốc Đài PT-TH Tiền_Giang_Trần_Thanh_Nhã ký hợp_đồng với Công_ty Phượng_Tùng với nội_dung : công_ty cung_cấp bản_quyền phim và một xe Ford 15 chỗ ngồi , đổi lại Đài PT-TH sẽ phát phim mỗi buổi chiều trong thời_gian 20 tháng . Trước đó , UBND tỉnh Tiền_Giang đã có ý_kiến thống_nhất cho đài phát quảng_cáo trong các tập phim của công_ty này , nhưng chỉ được 10 phút/buổi .
Tuy_nhiên , thanh_tra phát_hiện từ ngày 1-10-2004 đến 31-12-2005 Đài PT-TH đã phát dư hơn 2.000 phút quảng_cáo trên sóng truyền_hình cho Công_ty Phượng_Tùng ( mỗi ngày phát 15-16 phút thay_vì 10 phút ) . Việc_làm này đã gây thiệt_hại cho Nhà_nước hơn 1,9 tỉ đồng .
TRẦN_ĐỨC

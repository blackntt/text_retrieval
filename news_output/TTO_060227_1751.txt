﻿ Mặt_nạ cho da khô
Không cần tới những loại mỹ_phẩm đắt tiền , bạn hãy bỏ ra chút_ít thời_gian và công_sức để chế_biến mặt_nạ dưỡng da cho chính mình . Những dưỡng_chất từ thiên_nhiên sẽ đem lại cho làn da khô của bạn vẻ đẹp tươi_sáng bất_ngờ .
Sau đây là một_vài công_thức chế_biến mặt_nạ dưỡng da dành cho da khô , bạn có_thể tham_khảo :
Mặt_nạ lê tàu
Thành_phần : 1 lòng_đỏ trứng , 1 quả lê tàu nhỏ , 1/2 quả chuối , 2 thìa nước cà_rốt , 2-3 thìa đất_sét trắng , 1 giọt tinh_dầu hoa_hồng .
Cách làm : Dùng thìa nghiền quả lê tàu và chuối cho tới khi chúng trở_thành hỗn_hợp đặc_sệt như kem . Cho lòng_đỏ trứng vào hỗn_hợp này và khuấy đều .
- Thêm nước cà_rốt . Cho vào 1 thìa đất_sét trắng và trộn đều . Nếu hỗn_hợp vẫn hơi lỏng , có_thể cho thêm đất_sét trắng .
- Khuấy đều cho tới khi hỗn_hợp trở_nên mềm , mượt như hồ . Cho tinh_dầu hoa_hồng vào cuối_cùng .
Sử_dụng
Đắp một lớp hỗn_hợp lên da mặt và cổ sau khi đã rửa sạch . Tránh để dung_dịch dính vào mắt . Để chừng 10 phút ( có_thể lâu hơn nếu muốn ) . Rửa sạch bằng nước ấm . Có_thể dùng kem dưỡng ngay sau khi rửa mặt .
Quả lê tàu có trong thành_phần của loại mặt_nạ này cung_cấp nhiều vitamin , dưỡng_chất và độ_ẩm cho da mặt và vùng da cổ . Bạn cũng có_thể tận_dụng để chăm_sóc da tay bằng cách đắp phần còn thừa lên_tay và bọc lại bằng túi nhựa khi bạn nghỉ_ngơi . Mặt_nạ này có_thể dùng ngay_lập_tức , nhưng không nên để dùng tiếp lần sau .
Mặt_nạ chuối - hoa_hồng
Mặt_nạ chuối thích_hợp với mọi loại da , song đặc_biệt tốt cho da khô và da nhạy_cảm .
Thành_phần : 1/2 quả chuối chín , 1 thìa mật_ong , 1 thìa nước hoa_hồng , 1 thìa bột yến_mạch , 1/8 thìa vitamin E , 1 thìa nhỏ đất_sét đỏ .
Cách làm : Nghiền nhuyễn chuối , thêm mật_ong và nước hoa_hồng trộn đều . Sau đó , mới thêm dầu jojoba và vitamin E , bột yến_mạch và đất_sét đỏ .
Cách dùng : Bôi một lớp mỏng lên da mặt và cổ . Chờ 10 phút cho tinh_chất từ chuối và dầu làm dịu da và đất_sét có_thể hấp_thu những chất bẩn trên da . Rửa sạch bằng nước ấm , vỗ nhẹ nước lên_mặt . Bôi kem dưỡng sau khi hoàn_thành quá_trình trên .
Mặt_nạ 1-2 - 3
Thành_phần : 3 thìa cao_lanh , 2 thìa nước_cất , 1 thìa nước hoa_hồng , 1/2 thìa glycerin , 3 giọt tinh_dầu oải hương ( hoặc có_thể thay bằng tinh_dầu trà , hay cam , chanh .
Cách làm : Hoà nước và glycerin vào với nhau . Khuấy đều tới khi glycerin hoàn_toàn hoà_tan trong nước .
- Cho đất_sét trắng và trộn đều ( bạn cũng có_thể cho vào máy xay , bởi làm_vậy hỗn_hợp sẽ đều và mịn hơn ) . Làm_tới khi hỗn_hợp trở_nên mềm , mượt và sánh .
- Cho tinh_dầu vào . Nếu hỗn_hợp quá khô , có_thể cho thêm nước hoa_hồng hay nước . Còn nếu hỗn_hợp quá nhão và không đủ độ dính , hãy thêm chút đất_sét .
Cách sử_dụng : Dùng chổi lông nhỏ quét hỗn_hợp lên_mặt và cổ . Đợi khoảng 10 phút , khi thấy mặt_nạ khô đi , và những chất bẩn trên bế mặt da bị hút ra .
Rửa sạch mặt_bằng nước ấm . Nếu mặt_nạ quá khô , hãy đắp một chiếc khăn ướt lên_mặt trong giây_lát , việc rửa sạch_sẽ dễ_dàng hơn .
Nên đắp mặt_nạ từ 1-2 lần/tuần . Nếu hỗn_hợp còn nhiều sau mỗi lần làm , bạn có_thể trữ trong tủ_lạnh , có_thể dùng cho 2 tuần tiếp_theo .
Mặt_nạ Ai cập
Thành_phần : 1 quả trứng ( đánh đều ) , 1/2 thìa dầu ô_liu , 1 thìa sữa nguyên_chất ( chưa tách béơ ) , 1/4 thìa muối , 1 thìa bột mì .
Cách làm : Trộn đều trứng , dầu ô_liu và sữa , sau đó mới cho muối và bột mì , lại lắc đều đến khi được một dung_dịch mịn và sánh như hồ .
- Dung_dịch này phải ướt , song không quá lỏng ( nếu_không , nó sẽ chảy thành giọt khi đắp lên_mặt ) . Nếu quá lỏng , hãy thêm vào chút_ít bột mì , còn nếu quá khô , thêm sữa hay nước .
- Bạn cũng có_thể cho thêm một giọt tinh_dầu hoa_hồng hay hoa oải hương , chúng không_chỉ làm sáng da mà_còn tạo mùi hương dễ_chịu .
Cách dùng : Đắp dung_dịch lên_mặt và cổ , nằm thư_giãn trong 15 phút . Sau đó rửa sạch bằng nước ấm . Nguyên_liệu làm loại mặt_nạ này đều tươi , nên bạn chỉ nên làm và dùng trong ngày .
Mặt_nạ xoài
Rất đơn_giản , dễ làm , mặt_nạ loại này có_thể làm sạch và mềm da . Mặt_nạ xoài cung_cấp enzym trái_cây tự_nhiên , và có mùi rất dễ_chịu .
Thành_phần : 1/4 quả xoài cỡ trung_bình , 2 thìa bột yến_mạch , 2 thìa bột hạnh_nhân , 2 thìa kem hoặc sữa_chua ( cho da quá khô , còn nếu da bạn chỉ hơi khô , hãy dùng sữa chưa tách béo ) .
Cách làm : Trộn tất_cả nguyên_liệu trong tô nhỏ , cho vào máy xay đều khoảng 1 phút tới khi hỗn_hợp mượt và sánh .
- Thêm một_chút nước nếu hỗn_hợp quá khô , và thêm bột yến_mạch nếu quá ướt .
Cách sử_dụng : Nhẹ_nhàng quét dung_dịch lên da , nằm thư_giãn trong 15 phút .
Theo Netmode

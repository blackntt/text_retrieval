﻿ Quan_chức tỉnh , thành ở Trung_Quốc sẽ do dân bầu
Người_dân sẽ được bầu quan_chức thành_phố ? Đảng_Cộng sản Trung_Quốc ( CPC ) vừa cho_biết sẽ cho_phép tiến_hành các cuộc bầu_cử ở địa_phương để bầu lãnh_đạo mới ở cấp tỉnh , thành_phố , thị_xã .
Tuy_nhiên , các quan_chức chưa tiết_lộ cụ_thể việc bầu_cử cụ_thể , có một hay nhiều ứng_cử_viên trong cuộc bầu_cử lãnh_đạo địa_phương .
Theo quan_chức CPC , đây là một bước_tiến mạnh_mẽ nhằm đẩy_mạnh dân_chủ và cải_cách ở Trung_Quốc .
Bầu lãnh_đạo địa_phương cũng được CPC xem là mục_tiêu chiến_lược trong việc xây_dựng một xã_hội hài_hòa xã_hội_chủ_nghĩa .
Cùng_với bước_tiến này , CPC cam_kết sẽ tiếp_tục cố_gắng nâng cao năng_lực quản_lý trong đảng . Quan_chức CPC cũng phác_thảo những nét chính để bảo_đảm rằng lãnh_đạo mới của địa_phương sẽ thực_thi một_cách kiên_quyết chính_sách và đường_lối trong việc xây_dựng chủ_nghĩa_xã_hội với đặc_trưng của Trung_Quốc .
Các phương_pháp mới đã được chấp_nhận để kiểm_tra , đánh_giá ứng_cử_viên một_cách khoa_học , với tiêu_chí mới để “ định_giá ” công_việc của họ .
CPC có kế_hoạch phát_huy dân_chủ trong tiến_trình bầu_cử lãnh_đạo địa_phương bằng việc bảo_đảm quyền của các đảng_viên , người_dân cần được biết , được tham_gia bầu_chọn và giám_sát .
Ý_kiến đối_với ứng_cử_viên cũng được thu_thập từ các nhóm khác_nhau ; đồng_thời đảm_bảo người_dân được góp tiếng_nói công_khai của họ với ứng_cử_viên .
Theo quan_chức CPC , những nỗ_lực lớn khác cũng được triển_khai nhằm đề_cử các ứng_cử_viên trẻ vào bộ_máy lãnh_đạo với việc Đảng đang tiếp_tục kêu_gọi giải_phóng hệ_tư_tưởng và xóa_bỏ những nội_dung lạc_hậu .
Quan_chức này cũng cảnh_báo , Trung_ương Đảng sẽ trừng_phạt nghiêm_khắc những kẻ dính vào hành_động bất_chính trong bầu_cử .
Hồi tháng 3/2006 , quan_chức CPC từng cho_biết ý_kiến công_chúng sẽ là tiêu_chuẩn chính cho lần đầu_tiên lựa_chọn gần 100.000 quan_chức địa_phương mới trước cuộc tổng_tuyển_cử Quốc_hội khóa 17 năm 2007 . Trong khi đó , tờ Nhân_dân Nhật_báo , cơ_quan_ngôn_luận của CPC , cũng thông_báo sẽ xuất_bản một tài_liệu đặc_biệt để đảm_bảo các cuộc bầu_cử sẽ thành_công và để quan_chức cũng_như các cơ_quan địa_phương của đảng nhận_thức rõ nhiệm_vụ quan_trọng của tiến_trình cải_cách này .
Theo Tiền_Phong

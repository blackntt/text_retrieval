﻿ Bài giải thi tốt_nghiệp THPT : Môn_Văn
Câu 2 ( 8 điểm ) : Anh hoặc chị hãy phân_tích truyện_ngắn Mùa lạc của Nguyễn_Khải để làm rõ cảm_hứng hồi_sinh trong tác_phẩm .
Đề II
Câu 1 ( 2 điểm ) : Anh hoặc chị hãy trình_bày hoàn_cảnh ra_đời bài_thơ Tây_Tiến của Quang_Dũng .
Câu 2 ( 2 điểm ) : Trình_bày ngắn_gọn quan_điểm sáng_tác văn_học của Hồ_Chí_Minh .
Câu 3 ( 6 điểm ) : Hãy phân_tích đoạn thơ sau trong bài_thơ Tâm_tư trong tù của Tố_Hữu :
Cô_đơn thay là cảnh thân tù ! Tai mở_rộng và lòng sôi rạo_rực Tôi lắng_nghe tiếng đời lăn náo_nức Ở ngoài kia vui_sướng biết_bao_nhiêu ! Nghe chim reo trong gió mạnh lên triều Nghe vội_vã tiếng dơi chiều đập cánh Nghe lạc ngựa rùng chân bên giếng lạnh Dưới đường xa nghe tiếng guốc đi_về ...
( Theo Văn học 12 , tập một , phần VHVN , Nhà_xuất_bản Giáo_Dục 2006 - tr.26 )
Bài giải
Đề I :
Câu 1 : Những nét chính trong cuộc_đời của nhà_thơ Êxênin đã ảnh_hưởng đến sáng_tác của ông , cần nêu được các ý chính sau đây :
- Xecgây Êxênin ( 1895-1925 ) sinh_ra trong một gia_đình nông_dân ở vùng quê tỉnh Riadan nước Nga . Lớn lên giữa ruộng_đồng và thảo_nguyên , giữa làng quê Nga , do_đó những hình_ảnh tuyệt_diệu về thiên_nhiên và cuộc_sống làng quê Nga đã đi vào thơ ông với tình_cảm chân_thành say_đắm , với ngôn_ngữ và lời ca tiếng hát của nhân_dân Nga .
- Từ nhỏ sống với ông bà_ngoại nên chịu ảnh_hưởng lối sống phóng_túng , ham vui_chơi của ông ngoại , và những tình_cảm tôn_giáo trong thơ Êxênin trước Cách_mạng Tháng_Mười là do chịu ảnh_hưởng của bà_ngoại .
- Sau_Cách mạng Tháng_Mười , Êxênin hoàn_toàn đứng về phía cách_mạng , tuy_nhiên có những khía_cạnh của cách_mạng_vô_sản ông không hiểu được , do_đó mà thơ ông cũng băn_khoăn lo_lắng về số_phận của quê_hương , và có những vần thơ đau_buồn , tuyệt_vọng .
Câu 2 : Phân_tích truyện_ngắn Mùa lạc của Nguyễn_Khải để làm rõ cảm_hứng hồi_sinh trong tác_phẩm .
1 . Yêu_cầu về kỹ_năng : Hiểu đúng yêu_cầu của đề_bài là phân_tích truyện_ngắn để làm sáng_tỏ nhận_định ; biết làm một bài văn nghị_luận văn_học có kết_cấu chặt_chẽ , bố_cục rõ_ràng , diễn_đạt tốt , không mắc lỗi chính_tả , lỗi dùng từ , lỗi ngữ_pháp .
2 . Yêu_cầu về kiến_thức : Học_sinh có_thể trình_bày theo nhiều cách khác_nhau , nhưng trên cơ_sở hiểu_biết chung về tác_phẩm mà lựa_chọn , phân_tích những sự_việc , hình_ảnh , chi_tiết tiêu_biểu để làm rõ cảm_hứng hồi_sinh trong truyện_ngắn Mùa lạc của Nguyễn_Khải . Có_thể nêu lên những ý chính sau đây :
2.1 . Sự hồi_sinh ở vùng_đất Điện_Biên : từ vùng_đất chết với thiên_nhiên khắc_nghiệt vì nắng cháy , rét_buốt , gió Lào ... và đầy thương_tích của chiến_tranh , qua một năm lao_động san rừng , đào cây , gỡ mìn ... giờ trở_thành một nông_trường trải dài mênh_mông với đầy_đủ màu_sắc , âm_thanh và sự vận_động mọi mặt của cuộc_sống thật_sự .
2.2 . Sự hồi_sinh qua nhân_vật Đào :
- Sự thay_đổi số_phận : Từ cuộc_đời đầy bất_hạnh góa_bụa , không gia_đình , không tiền_bạc , không nhan_sắc , lang_bạt khắp_nơi để kiếm sống chỉ mong sao ngày được hai bữa , tương_lai mù_mịt nhưng khi đến với nông_trường Điện_Biên , qua quá_trình lao_động cần_mẫn Đào được mọi người tin_yêu , được hạnh_phúc với bức thư_ngỏ lời của thiếu_úy Dịu .
- Sự thay_đổi trong tính_cách : Từ một người chua_ngoa , đanh_đá đến với nông_trường Điện_Biên , sống trong tình_thương mọi người chân_thành giúp_đỡ nhau , Đào đã trở_nên hoàn_thiện hơn , dịu_dàng , đằm_thắm hơn .
- Sự thay_đổi tâm_lý : Từ tâm_lý chán_nản , mỏi_mệt , Đào trở_nên yêu_đời , lạc_quan tin_tưởng ở tương_lai .
2.3 . Sự thay_đổi số_phận của nhân_vật Duệ : Sống với chú dượng từ nhỏ trong nỗi bất_hạnh và lo_âu nhưng đến với nông_trường Điện_Biên , Duệ cũng có được hạnh_phúc với Huân .
2.4 . Qua sự hồi_sinh , tác_phẩm đã ca_ngợi giá_trị của lao_động và mối quan_hệ đạo_đức mới giữa người với người , thể_hiện giá_trị nhân_đạo và khẳng_định một chân_lý của cuộc_sống : “ Sự sống nảy_sinh từ trong cái chết , hạnh_phúc hiện_hình từ trong những hi_sinh , gian_khổ , ở đời này không có con đường cùng , điều cốt_yếu là phải có sức_mạnh để bước qua những ranh_giới ấy ” .
Đề II :
Câu 1 : Hoàn_cảnh ra_đời bài_thơ Tây_Tiến của Quang_Dũng :
Tây_Tiến là tên một đơn_vị quân_đội thành_lập đầu năm 1947 , có nhiệm_vụ phối_hợp với bộ_đội Lào bảo_vệ biên_giới Việt - Lào và đánh_tiêu_hao lực_lượng quân_đội Pháp ở thượng Lào cũng_như ở miền Tây_Bắc bộ VN . Chiến_sĩ Tây_Tiến phần_đông là thanh_niên Hà_Nội , chiến_đấu trong những hoàn_cảnh rất gian_khổ , vô_cùng thiếu_thốn về vật_chất , bệnh sốt_rét hoành_hành dữ_dội , tuy_vậy họ vẫn phơi_phới tinh_thần lãng_mạn anh_hùng .
Đoàn quân Tây_Tiến sau một thời_gian hoạt_động ở Lào , trở_về Hòa_Bình thành_lập trung_đoàn 52 . Quang_Dũng là đại_đội_trưởng ở đó từ đầu năm 1947 đến cuối năm 1948 rồi chuyển sang đơn_vị khác . Nhà_thơ nhớ về đơn_vị cũ nên viết bài_thơ Nhớ_Tây_Tiến , sau đổi thành Tây_Tiến ( năm 1948 ) , bài_thơ được in trong tập Mây đầu ô .
Câu 2 : Quan_điểm sáng_tác văn_học của Hồ_Chí_Minh : Là nhà hoạt_động chính_trị , làm thơ , viết văn là để phục_vụ cách_mạng , Bác có một hệ_thống quan_điểm sáng_tác như sau :
- Hồ_Chí_Minh xem văn_nghệ là một hoạt_động tinh_thần phong_phú và phục_vụ có hiệu_quả cho sự_nghiệp cách_mạng , nhà_văn cũng phải ở giữa cuộc_đời góp_phần vào nhiệm_vụ đấu_tranh và phát_triển xã_hội :
+ “ Nay ở trong thơ nên có thép
Nhà_thơ cũng phải_biết xung_phong ” . ( Cảm_tưởng đọc Thiên gia thi )
+ “ Văn_hóa nghệ_thuật cũng là một mặt_trận . Anh_chị_em là chiến_sĩ trên mặt_trận ấy ” . ( Thư gửi các họa_sĩ , 1951 )
- Văn_chương phải phục_vụ nhân_dân , phải coi quảng_đại quần_chúng là đối_tượng phục_vụ . Bác nêu kinh_nghiệm chung cho hoạt_động báo_chí và văn_chương : Khi viết phải xác_định rõ đối_tượng ( viết cho ai ) , mục_đích ( viết để làm_gì ) , nội_dung ( viết cái gì ) , hình_thức nghệ_thuật ( viết như_thế_nào ) .
- Văn_chương phải có tính chân_thật : Văn_nghệ_sĩ phải “ miêu_tả cho_hay , cho chân_thật , cho hùng_hồn ” ; tránh lối viết xa_lạ , cầu_kỳ , ngôn_ngữ phải trong_sáng ; nội_dung phải sâu_sắc , thể_hiện được tinh_thần dân_tộc .
Câu 3 :
1 . Yêu_cầu về kỹ_năng : Biết cách phân_tích môt đoạn thơ trữ_tình , biết làm một bài văn nghị_luận văn_học có kết_cấu chặt_chẽ , bố_cục rõ_ràng , diễn_đạt tốt , không mắc lỗi chính_tả , lỗi dùng từ , lỗi ngữ_pháp .
2 . Yêu_cầu về kiến_thức : Học_sinh có_thể phân_tích và trình_bày theo nhiều cách khác_nhau , nhưng trên cơ_sở hiểu_biết chung về tác_phẩm mà phân_tích những đặc_sắc nghệ_thuật để làm rõ giá_trị nội_dung của đoạn thơ .
Về nghệ_thuật :
- Với những câu cảm và phép điệp_ngữ , liệt_kê , những từ_láy làm cho lời thơ vừa thiết_tha , chân_thành , vừa có tiết_tấu nhanh , say_sưa , sôi_nổi .
- Hình_ảnh thơ gần_gũi , giàu sức gợi_cảm .
Về nội_dung :
- Thấy được tâm_trạng cô_đơn da_diết của người chiến_sĩ cách_mạng lúc mới vào tù đang ở lứa tuổi căng tràn nhựa_sống , đang say_sưa hoạt_động cách_mạng , giờ bị nhốt trong nhà_lao biệt giam , tối_tăm , lạnh_lẽo , khắc_khổ , sầm u .
- Thấy được niềm khao_khát tự_do của người chiến_sĩ cách_mạng qua việc lắng_nghe những âm_thanh của cuộc_sống bên ngoài , không_chỉ bằng đôi tai mở_rộng mà_còn bằng tấm_lòng sôi rạo_rực .
- Thấy được trái_tim nhạy_cảm , trí_tưởng tượng phong_phú và tấm_lòng thiết_tha yêu_đời , yêu cuộc_sống qua những cảm_nhận của người chiến_sĩ cách_mạng về cuộc_sống náo_nức , rạo_rực khi nghe những âm_thanh quen_thuộc , bình_dị bên ngoài .
- Đánh_giá : Đoạn thơ thể_hiện cái tôi trữ_tình mới mẽ , trẻ_trung , nhạy_bén và tràn_đầy cảm_hứng lãng_mạn . Nỗi_buồn cô_đơn trong đoạn thơ là nỗi_buồn mạnh_mẽ , và cũng chỉ là những khoảnh_khắc nhất_định của tâm_hồn , không dai_dẳng , không bi_lụy như văn_học lãng_mạn đương_thời , bởi nó xuất_phát từ tấm_lòng thiết_tha yêu_đời , yêu cuộc_sống của người chiến_sĩ cách_mạng .
NGUYỄN_THỊ_PHI_HỒNG ( tổ_trưởng tổ văn Trường THPT Nguyễn_Thị_Minh_Khai , TP.HCM )

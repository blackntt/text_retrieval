﻿ Công_bằng
Thật không công_bằng ! “ Em và các bạn có làm_gì để đòi lại công_bằng không ? ” . Cô bé cụp mắt xuống : “ Các bạn nói_nhỏ với nhau vì bạn T . học thêm thầy , còn bạn N . thì không nên mới thế . Nhưng chẳng đứa nào dám hỏi thầy sao bạn T . không bị phạt ” .
Tôi chợt nhớ lớp_học hồi THCS của mình . Trong lớp , tôi ngồi cạnh một bạn là con giáo_viên trong trường . Cô_giáo dạy tiếng Pháp rất quan_tâm đến bạn này .
Giờ kiểm_tra , cô dành nhiều thời_gian xem bạn ấy làm bài , thỉnh_thoảng chỉ tay vào những chỗ bạn ấy làm sai , “ cười ý_nghĩa ” . Sửa xong chỗ sai , bạn ấy cũng nhìn cô “ cười ý_nghĩa ” .
“ Sao cô chỉ bài bạn ấy mà không_chỉ em ? ” - tôi muốn nói câu ấy lắm nhưng phần không dám , phần hiểu được lý_do nên chỉ biết thầm ghen_tị .
Lại nhớ có lần cô em_gái tôi kể về những bài kiểm_tra tập_làm_văn . Giờ kiểm_tra , cô_giáo đọc báo , nhiều bạn mừng_húm , sách văn mẫu lật rào_rào trong hộc bàn .
Trả_bài , cô bảo những bài_làm nào chép sách , chép sách nào cô biết hết , bài phát ra , những “ thợ chép - thợ lật ” mỉm cười hài_lòng với điểm 6 , điểm 7 , thậm_chí có cả 8 . Em làm bài trung_thực cũng ngần ấy điểm , có_khi còn thấp hơn . Thật không công_bằng !
Không dám nói đến ý_nghĩa lớn_lao của hai tiếng “ công_bằng ” trong cụm_từ “ xã_hội công_bằng , dân_chủ văn_minh ” mà chỉ dám nói đến hai tiếng “ công_bằng ” trong học_đường .
Bất_cứ biểu_hiện dù lớn hay nhỏ của sự công_bằng - không công_bằng trong học_đường đều để lại dấu_ấn trong tâm_hồn con_trẻ . Chấp_nhận , cho_qua , hùa theo một sự không công_bằng ngay trong lớp_học cũng có_nghĩa rất có_thể em sẽ chấp_nhận sự không công_bằng của xã_hội ngoài cổng trường .
Và ngược_lại , nếu dám dũng_cảm đấu_tranh vì những sự công_bằng tưởng_chừng như vụn_vặt ngay trong lớp_học để tự bênh_vực mình , bênh_vực bạn , cũng có_nghĩa rất có_thể đến lúc_nào đó em sẽ trở_thành người_hùng trong cuộc_sống rộng_lớn .
Và cũng thật mong những người dìu_dắt em trên con đường học_vấn sẽ mỗi ngày cho em tròn nghĩa ban_sơ hai tiếng “ công_bằng ” .
ĐINH_PHỤNG_ANH

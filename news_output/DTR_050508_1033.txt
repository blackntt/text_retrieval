﻿ VNPT tăng 113 tỷ đồng cho dịch_vụ tiết_kiệm bưu_điện
Tổng_công_ty Bưu_chính Viễn_thông Việt_Nam ( VNPT ) vừa quyết_định bổ_sung thêm 113 tỷ đồng_vốn điều_lệ cho Công_ty Dịch_vụ Tiết_kiệm Bưu_điện ( VPSC ) , nâng tổng_số vốn của công_ty lên 163 tỷ đồng .
Đây là một bước_tiến quan_trọng trong kế_hoạch phát_triển bền_vững và lâu_dài đối_với các dịch_vụ tài_chính bưu_chính của VNPT .
Trong năm nay , VPSC dự_kiến sẽ huy_động thêm 13.000 tỷ đồng_vốn thông_qua mạng tiết_kiệm bưu_điện và đạt trên 18.000 tỷ đồng được chuyển qua mạng_lưới chuyển_tiền .
VPSC cung_cấp ác dịch_vụ tiết_kiệm có kỳ_hạn các loại , tiết_kiệm gửi góp và tài_khoản tiết_kiệm cá_nhân , các dịch_vụ chuyển_tiền bưu_chính như thư_chuyển_tiền - điện chuyển_tiền , chuyển_tiền nhanh , chuyển_tiền quốc_tế .
Năm 2004 , VPSC đã huy_động được gần 33.000 tỷ đồng_vốn thông_qua mạng tiết_kiệm bưu_điện ( chuyển_giao cho Quỹ hỗ_trợ phát_triển được trên 9.400 tỷ đồng ) . Theo TTXVN

﻿ Biểu_diễn trích_đoạn ballet kinh_điển
Đặc_biệt cặp Tố_Như và Đạt_Minh đã xuất_hiện trong duo trích vũ_kịch Hồ thiên_nga , Thằng gù Nhà_thờ Đức_Bà và grand pas - de-deux trích ballet Don_Quixote . Ngoài_ra , còn có các nghệ_sĩ Hà_Thế_Dũng - Trúc_Giang ( trong duo trích ballet Spartacus ) , Hồng_Châu - Phúc_Hùng ( trong duo Thanh_xuân ) ...
Ngày 26-3 tới , Nhà_hát Giao_hưởng và vũ_kịch TP.HCM cũng sẽ công_diễn ra_mắt tác_phẩm Fabliau của Sven_Erik_Werner , một trong mười truyện_cổ_tích giao_hưởng sáng tác_nhân kỷ_niệm 200 năm ngày_sinh của Hans_Christian_Andersen tại Nhà_hát TP.HCM .
Tin , ảnh : T . T . D .

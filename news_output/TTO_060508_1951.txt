﻿ Việt_kiều có quyền_sở_hữu nhà_ở trước năm 1991
Nghị_quyết được áp_dụng để giải_quyết những giao_dịch như cho mượn , cho thuê , thừa_kế , tặng , cho , mua_bán , đổi nhà_ở , hiện những trường_hợp này chưa có văn_bản pháp_luật nào quy_định .
Chiều 6-5 , các đại_biểu đã thảo_luận dự_thảo này và cùng thống_nhất với việc thừa_nhận quyền_sở_hữu của Việt_kiều đối_với những loại nhà_ở trước 1-7-1991 .
Theo dự_thảo nghị_quyết , hồ_sơ để xác_lập quyền_sở_hữu nhà_ở của Việt_kiều gồm đơn đề_nghị cấp giấy chứng_nhận quyền_sở_hữu nhà_ở , giấy_tờ chứng_minh là người VN định_cư ở nước_ngoài , các hợp_đồng cho thuê , mượn , giấy_tờ thừa_kế ...
Sau khi thảo_luận , Ủy_ban Thường_vụ Quốc_hội đã thống_nhất sẽ tiếp_tục hoàn_thiện các nội_dung khác của nghị_quyết để phiên họp sau thông_qua , nhằm tạo thuận_lợi , đảm_bảo quyền_lợi hợp_pháp của người VN định_cư ở nước_ngoài đối_với nhà_ở .
Theo Vn Express

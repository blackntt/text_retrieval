﻿ TP.HCM : tổ_chức lại việc cấp , đổi chứng_minh nhân_dân
Phòng cảnh_sát quản_lý hành_chính về trật_tự xã_hội ( PC13 ) chuyên tiếp_nhận , làm thủ_tục CMND cho những trường_hợp bị mất , đổi CMND cho những người trước_đây ở tỉnh nay nhập hộ_khẩu vào TP .
Để tạo thuận_lợi cho người_dân , Công_an TP qui_định những trường_hợp thuộc trách_nhiệm của PC13 có_thể thực_hiện tại công_an huyện nếu người_dân có hộ_khẩu thường_trú ở huyện . Công_an TP cũng qui_định rút ngắn thời_gian hẹn trả CMND cho người_dân . Cụ_thể : PC13 còn không quá bảy ngày ; công_an các quận không quá 11 ngày ; công_an các huyện không quá 15 .
L . A . Đ .

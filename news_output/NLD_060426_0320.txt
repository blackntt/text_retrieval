﻿ Miễn ... công_nhận KLQG chạy ngắn
( NLĐ ) - Sự_kiện hy_hữu này có_thể xảy_ra tại giải điền_kinh quốc_tế Hà_Nội mở_rộng 2006 diễn ra từ ngày 27 đến 29-4 tại SVĐ Hàng_Đẫy .
Theo Trưởng_Bộ môn Điền_kinh Ủy_ban TDTT Dương_Đức_Thủy , do đường chạy của SVĐ Hàng_Đẫy xuống_cấp , không bảo_đảm tiêu_chuẩn nên toàn_bộ các nội_dung chạy ngắn ( 100 m , 200 m và 400 m ) sẽ không được công_nhận kỷ_lục quốc_gia ( KLQG ) . Ông Thủy cho_biết , Ủy_ban TDTT chỉ công_nhận KLQG ở những nội_dung chạy trung_bình , dài và ném - đẩy . Giải điền_kinh quốc_tế Hà_Nội mở_rộng 2006 quy_tụ 33 đội trong nước và 3 đội quốc_tế là Malaysia , Quảng_Tây và Bắc_Kinh ( Trung_Quốc ) . Đáng chú_ý là lần đầu_tiên nội_dung chạy 2.000 m vượt chướng_ngại_vật nữ được đưa vào chương_trình thi_đấu .
N.Ngọc

﻿ # # D : \ Corpus \ HTML\ NLD\ www.nld.com.vn \ tools \ print.asp-news _ id = 141743.htm
Thành_lập Trường cao_đẳng y_tế Khánh_Hòa
Tại cuộc họp giao_ban chiều 7-2 , UBND tỉnh Khánh_Hòa cho_biết Bộ Giáo dục - Đào_tạo vừa ký quyết_định thành_lập Trường cao_đẳng y_tế Khánh_Hòa ...
với chức_năng đào_tạo hệ cao_đẳng , trung_cấp và dạy nghề các ngành điều_dưỡng , hộ_sinh , kỹ_thuật y_học , dược ... cho Khánh_Hòa và các tỉnh lân_cận . Trường sẽ được ra_mắt trong tháng 2-2006 và dự_kiến tuyển_sinh ngay trong năm_học này .
Theo SGGP

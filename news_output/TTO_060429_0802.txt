﻿ Chuyện thường_ngày : Riết rồi quen !
- Bút_Bi ơi , đúng là riết rồi quen . “ Hồ_Cây_Gõ ” lại xuất_hiện , khiến cả biển người và xe được “ ngâm ” nước thỏa_thích suốt nhiều tiếng đồng_hồ .
- Nghe_nói tất_cả đều tại ... ông trời , vừa mưa lớn lại vừa triều_cường .
- Thì đó , người_dân cứ nghe đi nghe lại mấy nguyên_nhân này , riết rồi quen . Có người hỏi vậy_sao thành_phố không xin trung_ương cho hưởng chính_sách ưu_đãi “ vùng sống chung với lũ ” .
- Không biết_mấy ông giao_thông công_chính có rát tai vì bị dân kêu không nhỉ ?
- Trời_ơi , cả chục năm nay rồi . Bọn tui ngâm nước hoài , riết rồi quen , thì mấy ổng nghe dân nói hoài , cũng riết rồi quen !
TRẦN_VƯƠNG_THẢO

﻿ Những điều cần biết về tuyển_sinh ĐH , CĐ 2006
Ngoài_ra , trong năm 2006 , các trường cũng sẽ tuyển mới 99.010 CT_ĐH và 37.210 CT_CĐ hệ vừa học vừa làm .
Bộ GD - ĐT cũng chính_thức thông_báo năm 2006 sẽ có 1.810 CT cử_tuyển ĐH và 750 CT cử_tuyển CĐ dành cho HS con_em các dân_tộc . Đồng_thời sẽ có 2.710 CT dự_bị ĐH , tăng 8% so với năm 2005 .
Có điểm gì mới ?
Thi_trắc_nghiệm đối_với môn ngoại_ngữ ở cả bốn thứ tiếng Anh , Nga , Pháp , Trung .
Bổ_sung đối_tượng ưu_tiên : đối_tượng 03 thuộc nhóm ưu_tiên 1 đối_với con của người hoạt_động cách_mạng trước tổng_khởi_nghĩa 19-8-1945 , bổ_sung vào đối_tượng 06 thuộc nhóm ưu_tiên 2 thí_sinh ( TS ) tàn_tật , con_đẻ của người hoạt_động kháng_chiến bị nhiễm chất_độc hóa_học , có giấy chứng_nhận của cấp tỉnh bị dị_dạng , dị_tật , suy_giảm khả_năng tự_lực trong sinh_hoạt hoặc lao_động .
Bỏ qui_định cộng điểm thưởng đối_với HS tốt_nghiệp THPT loại giỏi .
Qui_trình nộp hồ_sơ , thi và xét tuyển
Từ 10-3 đến 17-4 : TS nộp hồ_sơ ĐKDT , cụ_thể :
- Từ 10-3 đến 10-4 nộp qua sở GD - ĐT : TS đang là HS lớp 12 nộp hồ_sơ ĐKDT tại các trường THPT , TS tự_do nộp tại các địa_điểm được sở GD - ĐT qui_định dành riêng cho TS tự_do .
- Từ 10-4 đến 17-4 : TS nộp hồ_sơ ĐKDT trực_tiếp tại các trường ĐH , CĐ .
Từ 30-5 đến 5-6 : TS nhận giấy_báo dự thi qua hệ_thống tuyển_sinh của các sở GD - ĐT .
Trước ngày 20-6 : những TS diện tuyển thẳng nộp hồ_sơ đăng_ký nguyện_vọng tuyển thẳng cho sở GD - ĐT để chuyển về Bộ GD - ĐT .
Ngày 30-6 : công_bố kết_quả xét tuyển thẳng . Trước 10-8 : các trường ĐH có tổ_chức thi_công bố kết_quả thi của TS . Trước 15-8 : các trường gửi giấy chứng_nhận kết_quả thi số 1 và số 2 , phiếu báo điểm của TS . Trước 20-8 : Vụ ĐH - SĐH công_bố điểm_sàn . Từ 20 đến 25-8 : các trường ĐH công_bố điểm trúng_tuyển đợt 1 ( NV1 ) và gửi giấy_báo trúng_tuyển cho TS . Từ 25-8 đến 10-9 : các trường ĐH còn CT xét tuyển NV2 , các trường ĐH , CĐ không tổ_chức thi nhận hồ_sơ xét tuyển NV2 . Từ 11-9 đến 15-9 : các trường ĐH , CĐ công_bố điểm trúng_tuyển đợt 2 . Từ 15-9 đến 30-9 : các trường còn CT nhận hồ_sơ xét tuyển NV3 . Trước 5-10 : công_bố điểm trúng_tuyển đợt 3 .
Những điều TS cần lưu_ý
- Nộp hồ_sơ và lệ_phí ĐKDT theo tuyến sở GD - ĐT hoặc nộp trực_tiếp tại trường sẽ dự thi , không nộp qua bưu_điện . Nhưng hồ_sơ đăng_ký xét tuyển ( NV2 , NV3 ) nộp qua đường bưu_điện .
- Mức lệ_phí đăng_ký dự thi : 40.000 đồng/hồ sơ ( nếu thi ngành năng_khiếu thì nộp 80.000 đồng/hồ sơ ) . Đối_với các ngành có yêu_cầu sơ_tuyển thì lệ_phí sơ_tuyển là 40.000 đồng/hồ sơ . Lệ_phí thi là 20.000 đồng/TS .
- Nhận giấy_báo dự thi tại nơi đã nộp hồ_sơ và lệ_phí ĐKDT . Đọc kỹ nội_dung giấy_báo dự thi , nếu phát_hiện có sai_sót cần thông_báo cho hội_đồng tuyển_sinh trường điều_chỉnh trước ngày thi mới có giá_trị .
- Đến làm thủ_tục dự thi : 3-7-2006 ( đối_với TS thi khối A và khối V ) và 8-7-2006 ( đối_với TS thi khối B , C , D , T , N , H , M , R ) , nhận thẻ dự thi ( nếu giấy_báo dự thi không kiêm thẻ dự thi ) , nghe phổ_biến qui_chế .
- Khi đến dự thi phải mang theo : giấy_báo dự thi ; bằng tốt_nghiệp THPT hoặc tương_đương ( đối_với TS tốt_nghiệp từ năm 2005 về trước ) hoặc giấy chứng_nhận tốt_nghiệp tạm_thời ( đối_với TS tốt_nghiệp năm 2006 ) ; chứng_minh_thư ; giấy chứng_nhận sơ_tuyển ( nếu thi vào các ngành có yêu_cầu sơ_tuyển ) .
- Dự thi môn đầu_tiên : TS phải có_mặt tại phòng thi trước 6g30 ngày 4-7-2006 ( đối_với TS thi khối A và khối V ) , trước 6g30 ngày 9-7-2006 ( đối_với TS thi khối B , C , D , T , N , H , M , R ) . Đến chậm 15 phút sau khi bóc đề thi sẽ không được dự thi .
- Chỉ được mang vào phòng thi : bút viết , bút_chì , compa , tẩy , thước_kẻ , thước_tính , máy_tính_điện_tử không có thẻ nhớ và không soạn được văn_bản , giấy_thấm chưa dùng , giấy nháp ( giấy nháp phải xin chữ_ký của cán_bộ coi thi ) . Ngoài các vật_dụng trên , không được mang bất_kỳ tài_liệu , vật_dụng nào khác vào khu_vực thi và phòng thi .
- Trước 20-6-2006 , HS đoạt giải quốc_tế , quốc_gia nộp hồ_sơ đăng_ký tuyển thẳng vào ĐH , CĐ ( theo mẫu của Bộ GD - ĐT ) và lệ_phí 15.000 đồng/HS cho sở GD - ĐT . Bộ GD - ĐT sẽ công_bố kết_quả tuyển thẳng trên mạng Internet và gửi cho các sở GD - ĐT trước 30-6-2006 .
- HS thuộc diện tuyển thẳng nhưng không sử_dụng quyền được tuyển thẳng được cộng thêm điểm thưởng vào kết_quả thi ĐH , CĐ . Mức điểm thưởng theo qui_chế .
- HS đoạt giải quốc_tế , quốc_gia nhưng chưa tốt_nghiệp THPT sẽ được tuyển thẳng ĐH , CĐ vào đúng năm tốt_nghiệp THPT .
- HS có nhiều chế_độ ưu_tiên , chỉ được hưởng một ưu_tiên cao nhất .
PV

﻿ Vì_sao Putin hôn bụng một cậu_bé ?
Hôm_qua Tổng_thống Nga_Putin đã chính_thức lên_tiếng “ giải_mã ” nụ_hôn bất_ngờ và gây xôn_xao của mình vào bụng một cậu_bé hồi tuần trước . Ông cho_biết đó là một cử_chỉ yêu_mến tự_phát như vuốt_ve một chú mèo con vậy .
Ngày 28/6 khi đi ngang qua sân của điện Kremlin , Tổng_thống Putin đã dừng lại nói_chuyện với cậu_bé 5 tuổi có tên Nikita , rồi vén áo của cậu_bé lên và bất_ngờ hôn vào bụng cậu trong sự ngỡ_ngàng của những người xung_quanh . Nụ_hôn đó đã được trình_chiếu trên đài_truyền_hình Nga và được người_dân trong nước rất quan_tâm .
“ Cậu_bé có_vẻ như rất độc_lập và nghiêm_nghị … Tôi muốn dỗ_dành cậu_bé như dỗ_dành một chú mèo con vậy . Và cử_chỉ đó đã đến một_cách bột_phát . Cậu_bé thật đáng yêu . ” – Putin nói .
Nụ_hôn đã khiến báo_chí Nga cũng_như báo_chí nước_ngoài không khỏi tò_mò . Họ đặt ra hàng_loạt câu_hỏi về động_cơ của người đứng đầu_nước Nga . “ Không có ẩn_ý gì đằng sau nụ_hôn đó cả ” , Putin phát_biểu trên BBC ngày hôm_qua . Và ông giải_thích đó chỉ là một cử_chỉ yêu_mến bột_phát .
Theo tờ Izvestia của Nga , sau khi được hôn , cậu_bé Nikita đã nhất_quyết không chịu tắm . “ Cháu rất yêu ngài Putin , và ngài cũng rất yêu_quý cháu . Cháu cũng muốn trở_thành tổng_thống . ” - Cậu_bé nói .
Trang_Thu_Theo BBC

﻿ Hà_Nội : 3 gia_đình ngộ_độc nghi do ăn kem
Một_số người trong các gia_đình này đã phải nhập_viện Xanh_Pôn và Bạch_Mai điều_trị . Lý_do gây ngộ_độc được “ để_ý ” đến nhiều nhất_là kem đậu_xanh Tràng_Tiền , được các gia_đình nói trên mua về ăn vào tối 16-5 .
Rạng sáng 18-5 đã có bốn người ( trong gia_đình 13 người_ở phố Đại_La , Hà_Nội , mua kem Tràng_Tiền ăn tối 16-5 ) phải vào Trung_tâm chống độc ( Bệnh_viện Bạch_Mai ) điều_trị do ngộ_độc thức_ăn .
Theo anh N . Đ . H . , đại_diện gia_đình , trước khi ăn kem những người nói trên ăn cơm tại gia_đình , những người cùng ăn cơm nhưng không ăn kem không bị ngộ_độc . Bốn người phải nhập_viện là bốn người bị nặng nhất .
Sáng_qua 20-5 , Thanh_tra Sở Y_tế Hà_Nội đã thanh_tra đột_xuất hai cơ_sở sản_xuất kem lớn trên địa_bàn là Công_ty Thủy_Tạ và Công_ty_cổ_phần Tràng_Tiền . Đoàn thanh_tra đã phạt Công_ty_cổ_phần Tràng_Tiền 2 triệu đồng vì lý_do không đảm_bảo vệ_sinh .
Hôm 18-5 , Trung_tâm Y_tế dự_phòng Hà_Nội đã lấy 20 mẫu kem Tràng_Tiền về xét_nghiệm . Kết_quả xét_nghiệm sẽ có vào ngày 23-5 .
L.ANH

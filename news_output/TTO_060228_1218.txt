﻿ Beckham gặp khó_khăn với những bài_tập toán của con_trai
Cầu_thủ này đã than_phiền với tuần_báo Mail on Sunday : " Ngày_nay , bài_tập của bọn trẻ khó như_thế đấy " . Anh giải_thích thêm : " Hôm trước , tôi đang ở cạnh Brooklyn và tôi đã nói với Victoria : Có_lẽ tối nay em phải làm bài_tập giúp con . Tôi tin rằng đó là các bài_tính . Người_ta đã dạy cái đó cho bọn trẻ bằng một_cách hoàn_toàn khác với thời tôi đi học và tôi tự_nhủ : Ôi , lạy Chúa , con không làm nổi đâu " .
Từ khi Beckham chuyển sang chơi cho Real_Madrid , Brooklyn theo học ở Runnymede_College tại thủ_đô của Tây_Ban_Nha . Đây là một trường_học dạy theo chương_trình chính_thức của Anh quốc .
Tuần_báo Mail on Sunday cũng đưa ra hai ví_dụ về các bài_toán trong chương_trình dành cho học_sinh 7 tuổi : " Lúc 11g45 , Bet đi đến cửa_hàng . Nửa giờ sau , cô ấy quay về . Hỏi lúc ấy_là mấy giờ ? " và " Mười_hai chia ba bằng bao_nhiêu ? " .
Beckham , năm nay 30 tuổi , phải dẫn_dắt đội_tuyển Anh trong trận giao_hữu với Uruguay vào thứ_tư 1-3-2006 ( giờ_địa_phương ) tại Liverpool ( Tây_Bắc nước Anh ) .
TRẦN_LƯƠNG_CÔNG_KHANH ( Theo AFP )

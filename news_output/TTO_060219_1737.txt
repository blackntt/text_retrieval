﻿ Isabelle_Durin : " Tôi biết_ơn nhạc_sĩ Nguyễn_Văn_Quỳ "
Isabelle_Durin vừa trở_lại Hà_Nội và chị lại có cuộc biểu_diễn trình_tấu bản Sonate số 9 của Nguyễn_Văn_Quỳ với phần đệm piano của nghệ_sĩ Trần_Ngọc_Bích .
Được biết , mối quan_hệ của chị với nhạc_sĩ Nguyễn_Văn_Quỳ khởi_xướng từ cha chị . Vậy chị chơi nhạc của Nguyễn_Văn_Quỳ trước_tiên là vì yêu_thích nó , hay_là vì mối quan_hệ con_người thân_tình đó nhiều hơn ?
- Cha tôi là hiệu_trưởng một trường trung_học_phổ_thông , nơi có một_số học_sinh VN theo học . Ông còn sáng_lập một hiệp_hội liên_kết Pháp ngữ , và kết_giao với VN nên có nhiều cơ_hội sang thăm đất_nước các bạn .
Tôi không biết ông quen nhạc_sĩ Nguyễn_Văn_Quỳ trong hoàn_cảnh nào , nhưng khi ông nhận được những bản tổng_phổ sáng_tác của nhạc_sĩ kèm đĩa ghi_âm , ông đã đưa chúng cho tôi . Tôi nghe và thấy thực_sự rung_cảm .
Tôi nhận ra được những xúc_động của tác_giả trong âm_nhạc , những sự khác_biệt trong cấu_trúc và giai_điệu so với các sáng_tác mà tôi từng biểu_diễn . Nó có một chất_lượng nghệ_thuật nhất_định nên tôi thực_sự muốn chơi . Lý_do thứ_hai song_song là vì nhạc_sĩ Quỳ , vì mối quan_hệ rất con_người giữa ông và chúng_tôi . Hai lý_do này quan_trọng như nhau ( cười ) .
Về nhạc_sĩ Nguyễn_Văn_Quỳ :
Ông sinh năm 1925 tại Hà_Nội , tốt_nghiệp hòa_âm hệ cao_đẳng hàm_thụ tại Paris năm 1954 ( Trường_Cao đẳng tổng_hợp hàm_thụ Paris ) .
Về nước , ông giảng_dạy về hòa_âm tại Trường cao_đẳng Sư_phạm Hà_Nội , từ năm 1956 đến 1978 . Cho_đến nay , ông đã hoàn_thành được 9 bản Sonate và nhiều hợp_xướng , dạ_khúc ...
Ông được trao giải nhì ( không có giải nhất ) của Hội_Nhạc sĩ VN cho bản Sonate số 4 , năm 1995 và bản Sonate số 8 , năm 2005 . Thử hình_dung nếu nhạc_sĩ Nguyễn_Văn_Quỳ không phải ở VN mà ở Paris chẳng_hạn , các sáng_tác của ông liệu có_thể đạt được một danh_tiếng nghệ_thuật ở tầm_mức quốc_tế nhất_định ?
- Đây là một hình_dung khó_khăn ... nhưng tôi cho rằng rất có_thể . Âm_nhạc của ông được sáng_tác từ những năm cuối thế_kỷ XX nhưng lại theo trường_phái ấn_tượng vốn thịnh_hành ở châu Âu từ đầu thế_kỷ XX . Đó là một điểm đáng chú_ý khi đặt nó trong bối_cảnh xã_hội đương_đại quá tốc_độ và pha_tạp .
Bên_cạnh đó , ông đã tạo được phong_cách - một sự độc_đáo và hết_sức cá_nhân - ý tôi muốn nói âm_nhạc là chính con_người ông , khác_biệt và không bị lẫn giữa đám đông .
Riêng về chuyện danh_tiếng , tôi nghĩ rằng không nhất_thiết phải là người_ở Paris hay Berlin hay Moskva , ... mới có nhiều hơn cơ_hội .
Tôi thấy nhạc_sĩ Nguyễn_Văn_Quỳ đã tự thoát ra được_cái ràng_buộc mình là người nước nào ... và ông đã tự_tin mình là một nhạc_sĩ . Ông là người chủ_động gửi các sáng_tác của mình sang Pháp , nếu_không , làm_sao chúng_tôi có được một chương_trình như_thế_này ?
Việc trình_diễn các bản Sonate của nhạc_sĩ Nguyễn_Văn_Quỳ chỉ dừng lại ở một_hai buổi ở Hà_Nội có_lẽ hơi uổng_phí ?
- Cha tôi đang có một dự_án đưa âm_nhạc của ông sang Pháp tổ_chức một đêm trình_diễn công_phu tại Paris . Nhưng đây là một kế_hoạch lớn , có lẽ_phải đến năm 2008 mới thực_hiện được .
... Và vẫn bằng tiền_túi của cha_con chị ?
- Dự_án lớn thế_thì phải kiếm_tìm tài_trợ chứ . Nhưng bạn biết không , khi bỏ tiền_túi ra làm một công_việc mà mình yêu_thích , ta mới thấy hết được giá_trị của nó . Và thực_sự chúng_tôi làm_việc này vì nhạc_sĩ Nguyễn_Văn_Quỳ , vì một mối quan_hệ con_người mà chúng_tôi lấy_làm hãnh_diện .
Theo Thể thao và Văn_hóa

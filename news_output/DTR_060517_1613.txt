﻿ Mai_Hoa : “ Tôi may_mắn vì có được Trọng_Đài ”
“ Chồng tôi là người lớn_tuổi nhưng trong cuộc_sống cũng là người khá nhí_nhảnh , hóm_hỉnh . Giữa chúng_tôi nói_chuyện với nhau theo kiểu người_lớn cũng được , trẻ_con cũng được ” , ca_sĩ Mai_Hoa tâm_sự về phu_quân của mình - nhạc_sĩ Trọng_Đài như_vậy .
Sau CD " Hương đất " , vợ_chồng Mai_Hoa - Trọng_Đài đã lên kế_hoạch gì chưa ?
Thời_gian này tôi có khá nhiều cơ_hội , lời mời , chương_trình biểu_diễn , làm đĩa và nếu_không có gì thay_đổi vợ_chồng tôi dự_định ra tiếp CD nữa vào cuối năm nay . Ngoài những bài hát của chồng tôi ít được công_chúng biết đến , album sẽ chọn khoảng 8 - 9 bài mà tôi nghĩ là rất hay và công_chúng sẽ thích .
Chị có quan_niệm về sự “ độc_quyền ” kiểu như Trọng_Đài sáng_tác , Mai_Hoa hát như nhiều cặp đôi nhạc_sĩ - ca_sĩ khác trong âm_nhạc ?
Tôi không quan_niệm về sự độc_quyền và nghĩ rằng sáng_tác của chồng mình hay của bất_cứ một nhạc_sĩ nào khác được nhiều người hát càng tốt . Tác_phẩm nghệ_thuật giống như một bông hoa , nếu_không có nhiều người chiêm_ngưỡng , khen chê thì chưa hẳn là cuộc_sống . Tôi chẳng cảm_thấy tủi_thân ở điều ấy . Tôi coi công_việc là nghề và mình làm công_việc để sinh_nhai .
Vì coi là nghề nên chưa bao_giờ anh Trọng_Đài ngồi ngẫu_hứng sáng_tác một bài hát ?
Anh làm hoàn_toàn như một công_chức nhà_nước , cứ đặt bài là anh làm . Tất_cả những bài nhạc phim bắt_buộc , anh phải có . Hà_Nội đêm trở gió hay Chị tôi ra_đời đều thế , và tất_cả những bài hát sau_này cũng vậy .
Khoảng_cách 17 tuổi giữa hai người liệu có gây_sự trở_ngại ?
Tôi vẫn trả_lời là gần_như không có khoảng_cách , nếu_như có thì chúng_tôi đã không lấy nhau . Anh là người lớn_tuổi nhưng trong cuộc_sống cũng là người khá nhí_nhảnh , hóm_hỉnh . Giữa chúng_tôi nói_chuyện với nhau theo kiểu người_lớn cũng được , trẻ_con cũng được , kiểu gì cũng chiều được .
Chỉ có một điều khác là anh lớn_tuổi hơn tôi , mọi thứ anh đã đi qua hết rồi còn tôi thì chưa . Ví_như , đi sàn_nhảy , gần đây tôi mới biết , khi chính anh dẫn cho đi hết sàn này đến sàn khác đấy .
Nếu có sự lựa_chọn lại , liệu chị có “ lung_lay ” ?
Trong nhà , tôi là người ít tuổi hơn nhưng lên_giọng cũng … hoành_tráng lắm , sẵn_sàng kiêu_căng . Khi chúng_tôi có chuyện gì giận_dỗi , mẹ tôi nói rằng : “ Thôi , tha cho người lớn_tuổi . Có đi tìm khắp cũng chẳng được đứa nào như nó đâu ” . Thật_ra thì tôi hiểu rằng mình khá may_mắn khi có được người chồng như_thế !
Xin cảm_ơn và chúc anh_chị hạnh_phúc ! Nguyễn_Hằng

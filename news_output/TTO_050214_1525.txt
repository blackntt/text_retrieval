﻿ Hà_Nội : Tăng thêm ít_nhất 500ha đất cho công_nghiệp
Trong các vấn_đề mà nhà_đầu_tư quan_tâm nhiều nhất chính là mặt_bằng sản_xuất , vì_vậy công_việc mà TP Hà_Nội sẽ tập_trung trong năm nay là đầu_tư tăng mạnh diện_tích các khu_công_nghiệp .
Dự_kiến , trong năm nay TP Hà_Nội sẽ phải đầu_tư để có thêm ít_nhất 500ha mặt_bằng cho sản_xuất công_nghiệp . Con_số này chưa thấm_tháp so với nhu_cầu của DN và các nhà_đầu_tư tại Hà_Nội , tuy_nhiên đó mới chỉ là bước khởi_đầu , các năm tiếp_theo sẽ phải làm mạnh hơn_nữa .
Trong thời_gian qua , các khu_công_nghiệp mới chỉ được tập_trung phát_triển ở các huyện ngoại_thành phía Nam , trong khi tại phía bắc và đông_bắc thì cơ_sở_hạ_tầng cho sản_xuất công_nghiệp còn rất yếu . Do_vậy từ năm nay , việc đầu_tư xây_dựng thêm các khu_công_nghiệp ở khu_vực này sẽ được tập_trung nhiều hơn .
NHẬT_LINH ghi

﻿ Mỹ “ phô ” sức_mạnh quân_sự chưa từng có
Dư_luận Mỹ cho rằng , bắt_đầu_từ ngày 19/6 ở Thái_Bình_Dương gần đảo Guam , là cuộc biểu_dương lực_lượng qui_mô lớn chưa từng có_của Mỹ kể từ thời chiến_tranh Việt_Nam .
Cuộc diễn_tập kéo_dài một tuần với sự tham_gia của 22.000 binh_lính , gần 280 máy_bay và ba tàu_sân_bay , được tổ_chức trong khu_vực mà Mỹ xác_định là " tiềm_ẩn nhiều bất_ổn không_thể đoán biết trước được " , trong đó có vấn_đề CHDCND Triều_Tiên .
Các nhà_phân_tích cho rằng cuộc diễn_tập chính là một bước_đi quan_trọng trong điều_chỉnh chiến_lược_quân_sự nghiêng về châu Á của Mỹ trong thời_gian tới và là dấu_hiệu cho thấy Guam sẽ là “ mũi giáo ” của Mỹ hướng về châu Á . Nỗ_lực của Mỹ gia_tăng sức chiến_đấu tại Guam không_chỉ giảm bớt chi_phí đóng quân , tăng_cường phối_hợp trong nội_bộ quân Mỹ mà_còn giảm bớt tính nhạy_cảm chính_trị so với đóng quân ở nước_ngoài .
Theo PV Tuổi_trẻ

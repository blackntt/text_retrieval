﻿ Sạt_lở trên đường_sắt đèo Hải_Vân gây ách_tắc giao_thông
Tuổi_Trẻ đã có_mặt tại hiện_trường và nhận thấy một khối đất_đá hàng trăm mét_khối từ vách núi đã đổ ập xuống đường_sắt , ngay trong khi công_nhân Công_ty công_trình đường_sắt đang thi_công chống sạt_lở taluy dương của vách núi này .
Sự_cố xảy_ra khiến các đoàn tàu S8 , S2 từ TPHCM đi Hà_Nội phải nằm chờ ở ga Kim_Liên , đoàn tàu E2 nằm chờ trên ga Hải_Vân_Nam hơn ba giờ . Trong khi đó , tàu E1 phải nằm chờ trên ga Hải_Vân_Bắc , tàu S3 phải nằm chờ ở ga Lăng_Cô từ 12h30 trưa .
Công_ty công_trình đường_sắt đã huy_động hai xe xúc , một xe ủi và công_nhân cùng_với toàn_bộ nhân_viên đoàn tàu S8 tham_gia giải_phóng đất_đá , nhưng phải đến 15h30 giờ thì đường_sắt mới thông trở_lại .
LAM_GIANG

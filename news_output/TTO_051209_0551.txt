﻿ Năm 2015 không còn chung_cư cũ nát
Sẽ lựa_chọn các dự_án lớn , có tính_chất điển_hình như khu Kim_Liên , Giảng_Võ , Nguyễn_Công_Trứ , Văn_Chương , nhằm đảm_bảo tính khả_thi . Các chung_cư cao_tầng không bố_trí tầng 1 để ở mà dành cho thuê kinh_doanh dịch_vụ công_cộng , tạo kinh_phí bù_đắp chi_phí dịch_vụ quản_lý và bảo_dưỡng .
Theo sở này , hiện toàn thành_phố có 23 khu_tập_thể cũ đã xuống_cấp , với gần 450 nhà cao 4-5 tầng , 200 nhà lắp_ghép tấm lớn , tổng diện_tích khoảng 1,5 triệu m 2 . Các hộ dân đang ở nhà chung_cư cũ được cải_thiện về diện_tích ở , được tái_định_cư ngay tại khu_vực sau khi xây_dựng lại và được góp vốn , mua thêm diện_tích .
XUÂN_LONG

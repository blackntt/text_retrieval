﻿ Những người trẻ mắc “ bệnh than ”
H.Ngân bạn tôi làm nhân_viên kinh_doanh của một Cty nước_ngoài tại TPHCM . Công_việc của Ngân ổn_định , lại có thu_nhập thuộc loại “ chiếu trên ” so với đám bạn_bè cùng lớp .
Lại nghe_nói cô nàng có một tình_yêu đẹp với một anh_chàng cùng cơ_quan . Ấy vậy_mà mỗi lần cả đám gặp nhau là thế_nào H . Ngân cũng ngồi than_vãn với thái_độ chán_nản .
Thôi_thì đủ chuyện , từ chuyện Cty mấy tháng nay không chịu tăng lương , nào_là doanh_thu tháng này giảm_sút , nào_là chàng không hiểu mình ... Hết than chuyện riêng , nàng lại than sang chuyện thành_phố dạo này kẹt xe thường_xuyên , nước_máy ô_nhiễm đến chuyện … tham_nhũng .
Thêm cái khổ nữa_là sau khi H.Ngân than_vãn xong thì “ con virút than ” ấy lại lan nhanh hết người này đến người khác . Và sau mỗi cuộc “ đốt lò than ” như_vậy , thì đứa nào đứa nấy vẫn tiếp_tục công_việc “ khổ đủ thứ ” ấy và chẳng ai muốn thay_đổi những thứ mình vừa than .
Còn T.Phương - SV_ĐH Kinh_tế TP.HCM thì câu cửa_miệng mà cô bắt_đầu với bạn_bè luôn là “ Chán quá mày ơi ” và sau đấy là một lô , một lốc cái sự chán của cô nàng .
Cả lớp thì hơn phân_nửa đều biết những cái chán của T.Phương và đặt cho cô nàng cái biệt_danh ngộ_nghĩnh “ Phương than ” và gọi vui cái sự than của cô là “ nỗi_buồn thế_thái_nhân_tình ” .
Một_cách giải_tỏa “ xì trét ” ?
Đó là ý_kiến của nhiều người trẻ là SV , lập_trình_viên , kế_toán , giáo_viên , phóng_viên … mà chúng_tôi tiến_hành phỏng_vấn trong bài viết nhỏ này .
Trả_lời câu_hỏi : “ Khi than_thở bạn cảm_thấy thế_nào , và đó có phải là cách tốt để thay_đổi vấn_đề ? ” - Thanh_Bình ( 24 tuổi , lập_trình_viên của một Cty tại Hà_Nội ) cho_biết : “ Than_thở chỉ là một phản_xạ tức_thời để giải_tỏa áp_lực công_việc và xả “ xì trét ” chứ không phải là biện_pháp để giải_quyết những va_chạm , những điều không tốt trong công_việc và cuộc_sống ” .
Thế_nhưng , phần_lớn những người được hỏi cũng cho_biết , tuy than_vãn không giải_quyết được vấn_đề , nhưng vẫn thích than vì nói ra sẽ thấy nhẹ người hơn , thấy có_thể được chia_sẻ hơn . Cũng có nhiều người cho rằng , than là một_cách giải_tỏa tâm_lý rất tốt vì sau khi than xong , cảm_thấy tinh_thần sảng_khoái và phấn_chấn hơn .
Về “ nội_dung ” than thì 60% cho rằng hay than_thở về công_việc , về chỗ làm , về mức lương , 25% cho rằng thường than_thở về tình_yêu và chỉ trên 5% cho rằng thường than_thở về cuộc_sống cũng_như các vấn_đề khác .
Huyền_Hân ( SV Báo_chí , ĐH_KHXH và NV_TPHCM ) thì khẳng_định : “ Tôi dị_ứng với những người hay than_thở vì nếu cảm_thấy không hài_lòng thì_phải tự mình giải_quyết vấn_đề còn hay hơn là ngồi than_thở dông_dài ” .
Còn_Thanh_Nhã ( phóng_viên ) thì lại nhận_định “ Than_thở là biện_pháp tốt về tâm_lý , và một việc khi than_thở với bạn_bè thì có_thể sẽ có nhiều cách xử_lý hơn ” .
Tuy_nhiên , cũng có người bộc_lộ quan_điểm : “ Không thích những người than_thở quá nhiều hoặc chỉ biết ngồi than mà không tìm cách giải_quyết ” . Trong các đối_tượng mà chúng_tôi phỏng_vấn thì SV là những người hay than_thở nhất , tiếp đến là nhân_viên văn_phòng , giáo_viên …
Phóng_viên có_lẽ là đối_tượng ít than_thở nhất vì theo Ngọc_Thúy ( báo Mực_Tím ) thì “ không có thời_gian để mà than với thở ” hay như Quỳnh_Hương ( phóng_viên ) thì “ cứ than_thở thoải_mái đi nhưng_mà than_thở xong thì_phải dấn bước giải_quyết mọi việc ” .
Các chuyên_gia tâm_lý cho rằng than_thở cũng là một_cách để giải_tỏa tâm_lý rất tốt , vì khi nói ra những bức_xúc thì bản_thân sẽ cảm_thấy nhẹ_nhõm hơn .
Nếu có chuyện buồn_bực , không hài_lòng thì đừng nên để trong lòng mà phải nói ra vì nếu cứ để sẽ gây nên trạng_thái ức_chế tâm_lý , sẽ không tốt cho sức_khỏe cũng_như công_việc .
Tuy_nhiên , các chuyên_gia tâm_lý cũng khuyên rằng : “ Than thì cứ than , nhưng_mà đừng thái_quá ” . Bởi nếu than quá nhiều thì sẽ gây ra tâm_lý chán_nản , tạo một trạng_thái không tốt đối_với thần_kinh , sẽ ảnh_hưởng lớn đến công_việc và cuộc_sống .
Theo Tiền_Phong

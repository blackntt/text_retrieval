﻿ Máy_tính_cá_nhân nào được mang vào phòng thi ĐH ?
TS_QUÁCH_TUẤN_NGỌC - giám_đốc Trung_tâm Tin_học - cho_biết :
- So với qui_định áp_dụng trong kỳ thi_tuyển sinh năm 2005 , qui_định về các loại máy_tính cầm tay cá_nhân được phép sử_dụng trong phòng thi có sự điều_chỉnh .
Danh_mục máy_tính_cá_nhân được phép sử_dụng trong phòng thi ĐH , CĐ sẽ mở_rộng hơn năm 2005 vì những loại máy_tính_cá_nhân đã được phép sử_dụng trong kỳ thi tốt_nghiệp THPT sẽ được phép dùng trong kỳ thi_tuyển sinh ĐH , CĐ .
Như_vậy hai loại máy_tính Casio FX 570MS và Casio FX 570ES cũng sẽ được phép sử_dụng , thưa ông ?
- Đúng vậy , trong kỳ thi_tuyển sinh năm nay , TS được phép sử_dụng hai loại máy_tính Casio FX 570MS và Casio FX 570ES . Chúng_tôi sẽ công_bố một danh_mục cụ_thể các loại máy_tính_cá_nhân để TS có_thể lựa_chọn , không_chỉ có các loại máy của Casio mà_còn của các nhãn_hiệu khác .
Các máy_tính này phải đảm_bảo đúng theo qui_chế tuyển_sinh và các qui_định hiện_hành của Bộ GD - ĐT : TS chỉ được mang vào phòng thi các máy_tính cầm tay thông_dụng , làm được các phép_tính số_học đơn_giản ( cộng , trừ , nhân , chia , khai_căn ... ) , các phép_tính lượng_giác và các phép_tính siêu_việt ( ln , exp ... ) , không có thẻ nhớ và không soạn_thảo được văn_bản .
Cụ_thể những loại máy_tính cầm tay thông_dụng mà trung_tâm đã kiểm_tra và đề_nghị Bộ GD - ĐT cho TS được phép sử_dụng như sau : gồm Casio FX 95 , FX 220 , FX 500A , FX 500 MS , FX 570 MS , Casio FX 570ES . Đối_với nhãn_hiệu Sharp có các loại sau : Sharp EL 124A , EL 250S , EL 506W , EL 509WM .
Đối_với máy_tính hiệu Canon , TS được sử_dụng các loại sau : Canon FC 45S , LS153TS , F710 , F720 . Ngoài_ra , TS có_thể sử_dụng các máy_tính cầm tay khác có tính_năng tương_đương các loại máy kể trên .
THANH_HÀ thực_hiện

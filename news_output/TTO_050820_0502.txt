﻿ Điểm_chuẩn Học_viện Kỹ_thuật quân_sự , Quân_y , Khoa_học_quân_sự
- Hệ quân_sự ( miền Bắc / miền Nam ) : 26.5 - 22 - Đối_với nữ ( miền Bắc / miền Nam ) : 27 - 25.5 - Hệ dân_sự ( NV1 ) : 21
Học_viện Quân_y
Hệ quân_sự - Đào_tạo bác_sĩ : Khối A ( miền Bắc / miền Nam ) : 26.5 - 21 Đối_với nữ ( miền Bắc / miền Nam ) : 27 - 23 Khối B ( miền Bắc / miền Nam ) : 26.5 - 22 Đối_với nữ ( miền Bắc / miền Nam ) : 27 - 23
- Đào_tạo dược_sĩ Khối A ( miền Bắc/miền Nam ) : 25 - 21.5 Đối_với nữ ( miền Bắc / miền Nam ) : 27.5 - 23 Khối B ( miền Bắc / miền Nam ) : 25 - 21.5 Đối_với nữ ( miền Bắc / miền Nam ) : 27.5 - 24.5
Hệ dân_sự : cả khối A và B : 25
Học_viện Khoa_học_quân_sự
Hệ quân_sự Ngành 101 ( miền Bắc/miền Nam ) : 22.5 - 19 Ngành 701 : 27 - 25 Ngành 702 ( Anh - Nga ) : 25.5 - 22.5 Ngành 702 ( Nga - Nga , chỉ tuyển miền Bắc ) : 31 Ngành 702 ( Pháp - Nga , chỉ tuyển miền Bắc ) : 29.5 Ngành 703 ( Anh - Pháp , chỉ tuyển miền Bắc ) : 26 Ngành 703 ( Pháp - Pháp ) : 28.5 - 25.5 Ngành 704 ( Anh - Trung ) : 25.5 - 22.5 Ngành 704 ( Nga - Trung , chỉ tuyển miền Bắc ) : 31 Ngành 704 ( Pháp - Trung , chỉ tuyển miền Bắc ) : 29
Hệ dân_sự - Ngành 751 : 23.5 - Ngành 752 : 20 ( cả ba khối D1 , 2 , 3 ) - Ngành 753 : 22 điểm ( cả D1 và D3 ) - Ngành 754 : D1 : 23 ; D2 : 28,5 ; D3 : 28 điểm
Thứ_trưởng Bộ GD - ĐT kiêm trưởng_ban chỉ_đạo tuyển_sinh Bành_Tiến_Long cho_biết 150.320 thí_sinh đã trúng_tuyển vào các trường ĐH , CĐ theo NV1 , trong đó 107.045 thí_sinh trúng_tuyển vào ĐH , 43.275 trúng_tuyển vào CĐ . So với chỉ_tiêu tuyển mới ĐH , CĐ năm 2005 là 232.288 sinh_viên , hiện còn 81.968 chỉ_tiêu xét tuyển NV2 , NV3 .
Tại 55 trường ĐH , CĐ không tổ_chức thi ( gồm 14 trường ĐH và 41 trường CĐ ) sẽ xét tuyển 52.540 chỉ_tiêu , gồm 15.400 chỉ_tiêu ĐH và 37.140 chỉ_tiêu CĐ . Gần 29.500 chỉ_tiêu còn lại xét tuyển vào 72 trường ĐH và hệ CĐ trong trường ĐH có tổ_chức thi . Trong đó chỉ_tiêu NV2 ĐH là 23.816 , CĐ là 5.612 .
Trong số này có 50 ngành lấy từ 20 điểm trở_lên . Ngành có mức điểm xét tuyển NV2 cao nhất hiện_nay là toán - tin ( Trường ĐH Khoa_học_tự_nhiên thuộc ĐH Quốc_gia Hà_Nội ) , sư_phạm toán ( ĐH Sư_phạm 2 ) với mức điểm nhận hồ_sơ là 24,5 , ngành toán cơ ( Trường ĐH Khoa_học_tự_nhiên thuộc ĐH Quốc_gia Hà_Nội ) và hệ dân_sự của Học_viện Kỹ_thuật quân_sự nhận đăng_ký xét tuyển NV2 với các thí_sinh đạt từ 23,5 điểm trở_lên . Mức điểm xét tuyển NV2 thấp nhất_là bằng điểm_sàn .
THANH_HÀ

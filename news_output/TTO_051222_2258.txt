﻿ Rộn_ràng " Ngày hội tuổi_thơ "
Lễ_hội sẽ tiếp_tục được diễn ra từ nay đến Tết dương_lịch với những hoạt_động rất phong_phú dành cho thiếu_nhi nhân mùa Noel .
Trò_chơi cá_cảnh đã thu_hút rất đông trẻ_em về chơi tại khu trò_chơi dân_gian trong đêm khai_mạc " Ngày hội tuổi_thơ " - Ảnh : Th . Đạm
Các bạn nhỏ thích_thú vui_chơi tại các bàn nặn đất_sét trong khu trò_chơi dân_gian - Ảnh : Thanh_Đạm
Do tổ_chức gần ngày Noel nên trong khu vui_chơi xuất_hiện hình_ảnh những ông_già Noel tí_hon rất ngộ_nghĩnh - Ảnh : Đình_Long
Các em nhỏ thích_thú với gian_hàng bán quà Noel , nhất_là mũ và áo_quần của ông_già Noel - Ảnh : Đình_Long
Tin , ảnh : THANH_ĐẠM - ĐÌNH_LONG

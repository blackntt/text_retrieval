﻿ Hơn 1.000 m 3 gỗ khai_thác lậu
( NLĐ ) - Chiều 31-5 , Phó_Giám đốc Công_an Quảng_Nam_Phan_Như_Thạch ký quyết_định khởi_tố vụ_án “ Vi_phạm về các quy_định về khai_thác bảo_vệ rừng ” xảy_ra tại huyện Nam_Giang , Quảng_Nam . Trong thời_gian gần đây tại khu_vực rừng đầu nguồn tại địa_bàn 3 xã Tà_Bing , Chà_Vàl và xã Zuôi thuộc khu bảo_tồn thiên_nhiên sông Thanh , huyện Nam_Giang lực_lượng cảnh_sát điều_tra Công_an Quảng_Nam đã phát_hiện hơn 1.000 m 3 gỗ khai_thác trái_phép . Việc khởi_tố vụ_án này nhằm truy_tìm , đưa số lâm_tặc ra xử_lý theo pháp_luật .
H.Dũng

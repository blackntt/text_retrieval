﻿ Sự_thật về dị_ứng thực_phẩm
Cơ_thể bạn thường có những phản_ứng tiêu_cực với một loại thực_phẩm nào_đó ? Bạn thực_sự bị dị_ứng , cơ_thể không chịu dung_nạp hay đơn_giản hơn đó chỉ là vấn_đề tâm_lý .
Theo các nhà_nghiên_cứu , ước_tính khoảng trên 20% dân_số thế_giới có những phản_ứng tiêu_cực đối_với một_số loại thực_phẩm và người_ta gọi đó là dị_ứng thực_phẩm . Tuy_nhiên , qua các thực_nghiệm , sự_thật là chỉ có 1% người_lớn và 4% trẻ nhỏ là dị_ứng với thực_phẩm .
Các loại “ dị_ứng ” thực_phẩm
1 . Dị_ứng thực_sự
Tác_nhân : Ở trẻ nhỏ , những thực_phẩm kích_thích dị_ứng bao_gồm : protein trong sữa bò , trứng gà , bột mỳ , đỗ tương , cá tuyết và hạt lạc .
Ở người_lớn , các loại hạt , quả ( lạc , đào , táo … ) và các loại rau ( cần_tây , khoai_tây , hành_tây , tỏi và mùi tây ) được xem là các tác_nhân chính .
Riêng đồ hải_sản ( cá , ngao , cua , tôm , mực ) , tỉ_lệ dị_ứng luôn ở mức cao nhất .
Triệu_chứng : Biểu_hiện đặc_trưng của các trường_hợp dị_ứng thực_phẩm cấp_tính gồm phát_ban , ngứa_ngáy , cơ_thể sưng tấy , thở khó và thậm_chí cả đột_quỵ . Đây là hệ_quả của việc giải_phóng nhanh các histamine từ các tế_bào và phản_ứng của cơ_thể trước những " vật lạ " .
Tính quá mẫn_cảm với lạc được xem là một trong những ví_dụ điển_hình về các triệu_chứng dị_ứng thực_phẩm khi biểu_hiện được tập_trung ở miệng và ruột .
Ở một_số người khác , miệng và họng sẽ bị sưng đỏ khi ăn một_số loại quả , rau và hạt .
Những trường_hợp dị_ứng từ_từ sẽ có các triệu_chứng cơ_bản như của người bị eczema ( nổi mẩn , lấm_tấm các hạt mụn nước ) .
Dị_ứng ảnh_hưởng đến nội_tạng thường xảy_ra ở những người dị_ứng với chất gluten trong bột mỳ . Khi đó , màng ruột sẽ bị tổn_thương và kết_quả là dẫn tới bệnh tiêu_chảy , trướng bụng và về lâu_dài sẽ gây bệnh thiếu máu .
2 . Không dung_nạp thực_phẩm
Nguyên_nhân : Thực_phẩm không dung_nạp – ví_dụ như_không dung_nạp đường lactose – có_thể bắt_nguồn từ nguyên_nhân thiếu một loại enzyme tiêu_hóa đặc_trưng .
Khi thiếu enzyme này , cơ_thể sẽ xây_dựng một cơ_chế : coi những sản_phẩm không_thể dung_nạp đó như một loại sản_phẩm độc_hại . Đây là một trong những nguyên_nhân phổ_biến nhất .
Chất histamine tự_nhiên trong cơ_thể ngay_lập_tức sẽ được tiết ra khi cơ_thể tiếp_nhận " vật lạ " không_thể tiêu_hóa và gây ra các triệu_chứng giống như người bị dị_ứng thực_phẩm .
Triệu_chứng : Những thực_phẩm không dung_nạp thường có các biểu_hiện ban_đầu chậm hơn , không liên_quan tới hệ_miễn_dịch và cũng không đe_dọa tính_mạng như dị_ứng thực_phẩm .
Những biểu_hiện của nó giống như người uống thuốc quá liều . Với một lượng nhỏ thực_phẩm thì sẽ không có vấn_đề gì xảy_ra nhưng nếu một lượng lớn hơn , phản_ứng sẽ là phát_ban , mẩn đỏ , đau bụng , nôn_mửa , tiêu_chảy và đập trống_ngực .
3 . Thực_phẩm độc và vấn_đề tâm_lý
Các chất_độc có_thể tích_tụ tự_nhiên trong thực_phẩm như nấm và khoai_tây . Các vi_khuẩn sống ký_sinh trong cá cũng là nguyên_nhân gây ngộ_độc thực_phẩm .
Một_số người thì tỏ ra ác_cảm với một_số thực_phẩm và ám_thị mình rằng chúng thật đáng ghét mà chẳng có bất_cứ một lý_do cơ_bản nào . Kết_quả là khi tiếp_xúc với những sản_phẩm đó , họ cũng có các triệu_chứng giống như người dị_ứng thực_phẩm .
Tuy_nhiên , nếu thực_phẩm đó được giấu lẫn vào trong những loại thực_phẩm khác thì cơ_thể họ sẽ chẳng có bất_cứ phản_ứng nào . Những phản_ứng giống dị_ứng chính là do tâm_lý và việc điều_trị là rất nan_giải vì không_thể thuyết_phục họ thay_đổi suy_nghĩ đã ăn vào tiềm_thức .
Chẩn_đoán dị_ứng thực_phẩm
Có_thể chẩn_đoán dị_ứng thực_phẩm bằng các test trên da .
Kiểm_tra trên da với các thực_phẩm tươi_sống luôn mang lại kết_quả chính_xác hơn các loại test khác đồng_thời biết chính_xác được loại thực_phẩm gây dị_ứng .
Ngăn_ngừa dị_ứng thực_phẩm
Đối_với những gia_đình có nguy_cơ cao ( những gia_đình có cha_mẹ hay trong họ_hàng có người bị dị_ứng thực_phẩm ) cần chú_ý :
- Muốn các thế_hệ sau không bị dị_ứng thực_phẩm , người phụ_nữ khi mang thai cần tuyệt_đối tránh hút thuốc , ăn các loại thực_phẩm có nguy_cơ bị dị_ứng cao , đặc_biệt là giai_đoạn nửa cuối của thời_kỳ mang thai .
- Sau khi sinh con , trong quá_trình cho con bú , người_mẹ tuyệt_đối tránh ăn tất_cả các loại thực_phẩm có khả_năng gây dị_ứng . Nếu người_mẹ không cho con bú thì các sản_phẩm sữa phải được lựa_chọn hết_sức cẩn_thận , không chứa các thành_phần gây dị_ứng .
- Không cho trẻ ăn dặm trước 6 tháng tuổi và trong những tháng ăn dặm đầu_tiên , không cho trẻ ăn thịt cừu , thịt gà , gạo , khoai_tây ngọt , cà_rốt và lê . Tránh sữa bò , trứng , bột mỳ , cá , đậu_nành trong năm đầu_tiên và chỉ cho trẻ ăn các loại hạt khi được 3 tuổi .
- Cần đọc kỹ bảng thành_phần và quy_trình sản_xuất sản_phẩm . Cố_gắng tránh những thực_phẩm có chứa các chất phụ_gia và chất bảo_quản .
Nếu bị dị_ứng một loại thực_phẩm nào_đó thì tốt nhất_là hãy tránh xa nó . Nếu không_thể tránh được hoàn_toàn thì nên cho thêm hoạt_chất sodium chromoglycate để giảm_thiểu các biểu_hiện của dị_ứng .
Thu_Phương_Theo BBC Healths

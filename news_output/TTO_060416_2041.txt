﻿ Người_thân ở Mỹ có_thể chứng_minh tài_chính , nhưng ...
Những_ai bảo_trợ hoặc bảo_lãnh cho em đi học ở Hoa_Kỳ đều phải chứng_minh tài_chính . Em cần phải chứng_minh mình có đủ tiền để chi_trả cho ít_nhất một năm đầu_tiên ( phải có giấy_tờ đầy_đủ về nguồn tài_chính và thu_nhập trong một thời_gian nhất_định ) và có nguồn thu_nhập để trả cho những năm_học tiếp_theo cho_đến khi em tốt_nghiệp .
Chú của em có_thể bảo_lãnh cho em được nhưng cũng cần phải chứng_minh tài_chính . Cô của em ở Hoa_Kỳ cũng có_thể bảo_lãnh cho em được .
Nhưng có_lẽ em không nên chọn phương_án này vì em sẽ gặp khó_khăn khi xin visa . Em sẽ khó chứng_minh được những quan_hệ ràng_buộc xã_hội để em quay về Việt_Nam sau khi học xong .
Q . DŨNG thực_hiện

﻿ Valencia khiến Barca phải đợi !
( Dân_trí ) - Valencia khiến các culé Barca phải “ om ” rượu và pháo thêm ít_nhất 3 ngày nữa khi đánh_bại Alaves 3 - 0 tại Mestalla . Cũng trong đêm qua , dù chỉ còn chơi với 10 người nhưng Real_Madrid vẫn có được 3 điểm trước chủ nhà Osasuna nhờ bàn thắng duy_nhất của Baptista .
Chiến_thắng trước Alaves giúp Valencia rút ngắn cách_biệt với Barca xuống còn 8 điểm khi Liga còn 3 vòng đấu nữa_là hạ_màn .
Tuy_nhiên , thày trò HLV Frank_Rijkaard sẽ chính_thức đăng_quang ngôi vô_địch lần thứ 2 liên_tiếp nếu họ vượt qua đội bóng đang có phong_độ rất cao , Celta_Vigo vào thứ 4 này .
Trở_lại với diễn_biến trận đấu đêm qua , Valencia nhanh_chóng khẳng_định sức_mạnh trước đội bóng đang vật_lộn ở nhóm cuối bảng Alaves bằng bàn thắng của Ruben_Baraja ở phút 24 .
Nhận đường chuyền của Pablo_Aimar , Baraja khống_chế bằng ngực gọn_gàng rồi tung cú sút quyết_đoán , hạ gục thủ_thành Costanzo , đưa Valencia vượt lên dẫn trước .
Thế_trận tiếp_tục hoàn_toàn nằm trong quyền kiểm_soát của các cầu_thủ chủ nhà . Chỉ 9 phút sau , Aimar , cầu_thủ vừa trở_lại sau 2 tuần phải nhập_viện để điều_trị chứng viêm màng não virus , nhân đôi cách_biệt cho Valencia bằng một pha dứt_điểm khéo_léo từ_đường tạt của Moretti .
Với 1 đường chuyền kiến_tạo và 1 bàn thắng , tiền_vệ người Argentina đã có màn tái_xuất ấn_tượng trong đội_hình của Los_Che . Chiến_thắng đậm_đà của Valencia được hoàn_tất bằng bàn thắng từ chấm 11m của David_Villa .
Đây cũng là bàn thứ 23 của chân_sút này tại Liga mùa giải 2006 và hiện anh chỉ còn kém Eto ’ o vẻn_vẹn 1 bàn . Cuộc đua tới danh_hiệu Pichichi đang đến hồi hấp_dẫn .
Trong khi đó , dù chỉ thi_đấu với 10 người trên sân_sau khi thủ_môn Casillas bị đuổi , Real_Madrid vẫn nỗ_lực có được chiến_thắng quý_giá 1 - 0 trước đối_thủ xếp ngay sau Osasuna .
Baptista ăn_mừng bàn thắng quyết_định .
Trận đấu diễn ra rất “ nóng ” và trọng_tài đã phải rút ra 5 thẻ_vàng cùng 1 thẻ_đỏ để “ hạ nhiệt ” cho những cái đầu “ bốc_lửa ” .
Julio_Baptista đưa đội khách vượt lên dẫn trước bằng pha sút phạt 11m thành_công ở phút 51 . Đội chủ nhà tràn lên tấn_công sau bàn thua và cơ_hội đến với họ 12 phút sau đó khi được hưởng quả penalty do Pablo_Garcia phạm lỗi với Pierre_Webo trong vòng cấm .
Tuy_nhiên , Francisco_Punal đã không thắng được thủ_thành Casillas từ khoảng_cách 11m , bỏ lỡ cơ_hội san bằng cách_biệt .
“ Người_hùng ” Casillas suýt chút nữa đã trở_thành “ tội_đồ ” nếu Real không bảo_vệ được thành_quả cho_đến hết trận đấu bởi thủ_thành số 1 Tây_Ban_Nha này phải nhận thẻ_vàng thứ 2 và buộc phải rời sân vì một lỗi “ lãng_nhách ” ở phút 81 .
Với chiến_thắng này , Real gần_như đã “ cắt đuôi ” Osasuna khi khoảng_cách giữa 2 đội đã là 5 điểm . “ Kền_kền trắng ” tiếp_tục kiên_trì bám đuổi Valencia trong cuộc đua giành ngôi á_quân , hòng “ vớt_vát ” lại chút_ít danh_dự cho một mùa bóng tệ_hại .
Cũng trong đêm qua , Malaga đã chính_thức trở_thành đội bóng đầu_tiên chia_tay Liga sau khi thúc_thủ 2-3 trên sân_nhà trước Racing_Santander . Khoảng_cách giữa họ với nhóm an_toàn đã là 10 điểm và mọi chuyện đã “ vô_phương cứu_chữa ” với Malaga .
Trận chiến nhằm “ trốn ” 2 suất xuống hạng còn lại vẫn diễn ra rất gay_cấn khi khoảng_cách giữa đội_xếp thứ 19 ( Cadiz ) và đội_xếp thứ 17 , vị_trí an_toàn trụ hạng , chỉ là 4 điểm . Cuộc đua có_lẽ sẽ chỉ ngã_ngũ ở vòng đấu cuối_cùng .
M.Hải

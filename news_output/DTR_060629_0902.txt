﻿ Ngoại_trưởng Anh chửi_thề
Magaret_Beckett - nữ ngoại_trưởng đầu_tiên của Anh - tiết_lộ rằng bà lỡ thốt ra một từ không đứng_đắn cho lắm khi Thủ_tướng Tony_Blair đề_nghị giao cho bà công_việc này hồi tháng trước .
" Tôi quá choáng_váng " , Beckett - cựu bộ_trưởng môi_trường - nhớ lại lúc Blair hỏi bà có chịu nhận một trong những vị_trí quan_trọng nhất trong chính_phủ hay không .
Nữ ngoại_trưởng 63 tuổi nói bà đã đáp lại ông bằng " một từ có bốn chữ_cái " . Khi được hỏi từ đó bắt_đầu bằng chữ f hay s , bà thừa_nhận nó bắt_đầu bằng chữ f .
May_mắn là Blair có_vẻ như thích_thú trước phản_ứng đó và bà vẫn được bổ_nhiệm vào chiếc ghế đầu ngành ngoại_giao Anh .
Beckett cho_biết trong vai_trò mới bà " như bị ném xuống vực sâu " vì ngay_lập_tức bà phải đối_mặt với cuộc tranh_luận nảy_lửa về chương_trình hạt_nhân của Iran .
Theo H . N . Vnexpress/AFP

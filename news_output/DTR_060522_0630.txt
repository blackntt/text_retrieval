﻿ Người phụ_nữ chàng yêu
Ai đó nói tình_yêu là tất_cả . Nhưng để nuôi_dưỡng một mối quan_hệ lâu_dài , bạn không_thể chỉ đem tặng tình_yêu là đủ . Người phụ_nữ chiếm được trái_tim , tâm_hồn và cả thể_xác chàng phải là người …
1 . Có cuộc_sống của riêng mình
Có_nghĩa là bạn có khả_năng tự chăm_sóc mình , chú_ý đến phong_cách cá_nhân , dành thời_gian cho gia_đình và những buổi đi chơi cùng bè_bạn . Bạn tìm_kiếm các cuộc phiêu_lưu qua những chuyến du_lịch , biết tận_hưởng cuộc_sống , từ tự nuông_chiều mình bằng bữa tráng_miệng đến thư_giãn dạo chơi trong công_viên sáng cuối tuần .
Nói cách khác bạn không cần có chàng kè_kè bên_cạnh suốt cả ngày , vắng chàng bạn vẫn biết cách làm cho cuộc_sống luôn vận_động .
2 . Không bắt_đầu trước
Đây vẫn là vấn_đề đang tranh_cãi chưa đi đến hồi kết . Nhưng có ý_kiến chuyên_gia cho rằng phụ_nữ không_bao_giờ nên theo_đuổi đàn_ông . Bởi “ nếu bạn luôn là người gọi điện trước bạn sẽ không_thể biết được anh_ấy có thực_sự muốn nói_chuyện với bạn không ” .
Phụ_nữ khi chủ_động hay phải băn_khoăn về sự tiến_triển của mối quan_hệ , mình làm_vậy có đúng không , trong khi đàn_ông chẳng bị “ lập_trình ” cách nghĩ đó nên thích_hợp trong vai_trò kẻ theo_đuổi hơn phụ_nữ nhiều .
3 . Không “ mồi_chài ” vẫn sexy
Các chàng_trai thích sự khác_biệt trong cách cư_xử của bạn_gái ở mỗi giai_đoạn tình_cảm . Lúc mới chớm , bạn nên tránh đưa ra những nhận_định quá cởi_mở về sex . Bạn vẫn có_thể “ ra tín_hiệu ” bằng những động_chạm khẽ_khàng không mang tính_dục như đặt tay mình lên cẳng_tay chàng , thậm_chí lên đầu_gối nhưng chỉ trong thoáng chốc .
“ Va_chạm giới_tính ” hay công_khai âu_yếm trước mọi người chỉ thích_hợp khi quan_hệ của hai người đã tiến xa hơn , tình_cảm gần_gũi hơn .
4 . Biết chờ_đợi
Nhân_loại đã có cả một cuộc cách_mạng về sex và giờ_đây không mấy_ai còn khăng_khăng cô_dâu của mình phải “ còn nguyên ” . Mặc_dù vậy chuyện “ quan_hệ ” vẫn là một bước lớn trong tình_cảm nam_nữ .
Các bạn_gái có khi_không nhận_thức được hết những thay_đổi về động_cơ tình_cảm do sex mang lại . Song nghiên_cứu khoa_học cho thấy phụ_nữ “ vào đời ” quá sớm dễ đối_mặt với thứ tình_cảm không hề tồn_tại bên ngoài phòng ngủ , tình_yêu khi đó chỉ là sex . Khi bạn thấy không_thể thiếu được mối quan_hệ đó cũng là lúc chàng “ bỏ của chạy lấy người ” .
5 . Cử_chỉ nhỏ tỏ rõ sự quan_tâm
Chìa_khóa của vấn_đề nằm ở chỗ bạn để_ý làm cho chàng những điều nho_nhỏ nhưng chứa_đựng trong đó đầy yêu_thương chăm_sóc .
Anh_ấy không có thời_gian giặt áo sơ_mi , hãy mang đồ đến tiệm giặt cho anh hàng tuần , quan_tâm đến cái dạ_dày của anh_ấy . Cứ chú_ý đến nhu_cầu cá_nhân của chàng , rồi bạn sẽ được đền_đáp .
6 . Luôn yểm_trợ
Giúp chàng trông thật phong_độ khi đứng trước mặt sếp . Cười trước câu nói đùa của chàng và để chàng “ tỏa sáng ” khi cần . Tất_nhiên với bạn , chàng cũng sẽ “ đền_đáp ” như_thế .
7 . Không_bao_giờ gây áp_lực
Đây là điều tối quan_trọng vì bẩm_sinh các chàng “ dị_ứng ” với mọi loại áp_lực . Đừng nên gọi điện hay email cho chàng suốt cả ngày hoặc bày_tỏ ý_kiến về “ chuyện tương_lai ” .
“ Kết_hôn ” là từ không nên có trong từ_điển của bạn vì đàn_ông rất sợ sự ràng_buộc trong các mối quan_hệ . C ứ để mọi cái đến tự_nhiên thôi .
8 . Có quy_tắc riêng
Người phụ n ữ tốt không chấp_nhận những lối cư_xử không ra_gì . Nếu biết ở bạn có những nguyên_tắc không_bao_giờ được vượt qua ( như_không lừa_dối ) , chàng sẽ tôn_trọng bạn hơn và hiếm khi “ xâm_phạm ” .
Bạn cũng_nên tránh xa những người đàn_ông đã có vợ hay bạn_gái , những người có_thể gây hại hay lợi_dụng mình .
9 . Phụ_nữ tốt chọn đàn_ông tốt
Có_nghĩa là nên tìm_kiếm người chân_thành và đáng tin_cậy , một người biết đối_xử tốt với bạn . Cũng_nên cân_nhắc đến cả tình_hình tài_chính của người đàn_ông mình chọn . Mặc_dù điều này bị nhiều cặp đôi phản_đối nhưng bạn sẽ thấy nó không thừa .
10 . Hiểu tình_yêu là điều quan_trọng nhất
Bạn sẽ làm được điều này khi có trong mình thứ cảm_giác độc_đáo : biết chấp_nhận và luôn thoải_mái , hài_lòng . Cảm_giác coi niềm_vui , hạnh_phúc của người kia cũng là niềm_vui , hạnh_phúc của chính mình .
Huyền_Trang_Theo_Askmen

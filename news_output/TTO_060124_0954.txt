﻿ Serbia-Montenegro : tàu_hỏa trật bánh , 41 người chết
Chiếc tàu_hỏa bị trật bánh vào_khoảng 16 giờ giờ_địa_phương ( 14 giờ giờ_quốc_tế ) tại khu_vực Bioce , cách Podgorica khoảng 10 km về phía bắc . Đây là một trong những tai_nạn tàu_hỏa tồi_tệ nhất trong lịch_sử Montenegro .
Bộ_trưởng Bộ Nội vụ Montenegro , Jusuf_Kalamperovic , cho_biết nguyên_nhân dẫn đến tai_nạn là do một lỗi hư_hỏng trong hệ_thống phanh .
Chiếc tàu_hỏa nói trên chở theo khoảng 300 người từ thành_phố miền bắc Bijelo_Polje đến thành_phố cảng Bar , đã lao xuống một vực thẳm sâu 100 m đầy những đá .
Tin cho_biết có 90 trẻ_em trong số 180 người bị_thương . Nhiều người đang trở_về từ chuyến đi trượt_tuyết .
Chính_phủ đã tuyên_bố 3 ngày quốc_tang và Bộ_trưởng Bộ Vận tải Andrija_Lompar đã từ_chức sau vụ tai_nạn .
Vị_trí xảy_ra tai_nạn
Hiện_trường vụ tai_nạn Nhiều chiếc trực_thăng cứu_hộ đã bay_lượn phía trên nơi xảy_ra tai_nạn trong khi các nhân_viên cứu_hộ đang vật_lộn với thời_tiết xấu để đến với những người còn bị mắc_kẹt trên tàu .
Tổng_thống Montenegro , Filip_Vujanovic , và thủ_tướng Milo_Djukanovic đã đến hiện_trường xảy_ra tai_nạn .
“ Mọi nỗ_lực đang được thực_hiện để giảm thiếu thương_vong ” , tổng_thống nói .
Montenegro thuộc CH Montenegro và CH Serbia , nằm ở bán_đảo Balkan của Đông_Âu , với dân_số khoảng 620.000 người . Đây là hai nước cộng_hòa duy_nhất thuộc Nam_Tư cũ vẫn gắn_kết với nhau .
T.VY ( Theo BBC )

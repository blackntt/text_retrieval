﻿ Bố của Lê_Văn_Trương : " Tôi cũng không biết nói_gì ... "
Chiều 29/12 , chúng_tôi tìm đến nhà Lê_Văn_Trương tại thôn Bồn_Phổ , xã Hương_An ( huyện Hương_Trà , Thừa_Thiên - Huế ) , khi Văn_Trương đang ở nhà cùng các anh_chị và bố_mẹ .
Tiếp chúng_tôi , Trương có_vẻ bình_tĩnh nhưng vẻ mặt rất buồn và lo_lắng . Trương cho_biết đã về tập cùng đội Huda_Huế từ nhiều ngày_nay .
Riêng trong chiều_qua ( 29/12 ) , do đội nghỉ tập nên Trương được về với gia_đình . Anh cũng khẳng_định rằng chưa hề nhận thêm bất_cứ một giấy triệu_tập của CQĐT hay những thông_tin gì như báo_chí đã nói .
Mẹ của Trương đang ngồi bên_cạnh chỉ nói ngắn_gọn : " Tới đâu hay đó thôi ! " . Còn bố của Trương - ông Lê_Văn_Tuấn thì : " Hắn ( Trương ) cũng đi_đi về_về giữa nhà và đội bóng . Tui cũng có nghe đài , xem tivi nhưng cũng không biết gì .
Chừ tui cũng không biết nói_gì nữa , bởi đã có chính_quyền , đã có pháp_luật lo " .
Theo Th . Lộc_Tuổi trẻ

﻿ Mì ống sốt kem - rau
Cho bơ hành vào chảo đảo thơm , tiếp_theo là rau_muống , sữa , kem , nêm vừa ăn . Khi hỗn_hợp sôi đều cho mì ống và 1/2 phó_mát vào nấu hơi sệt lại .
Nguyên_liệu
500g Pene ( mì ống )
200g lá rau_muống thái sợi
300ml kem sữa_tươi không đường ( Whipping_Cream )
10g bơ thực_vật
150g phó_mát ( cheddar cheese )
1 muỗng cà_phê hành_tây băm nhuyễn
Thực_hiện Luộc mì theo hướng_dẫn ngoài bao hoặc luộc từ 6 ' đến 8 ' , vớt ra để ráo ( trộn 1 ít dầu để mì không dính vào nhau ) . Cho bơ hành vào chảo đảo thơm , tiếp_theo là rau_muống , sữa , kem , nêm vừa ăn . Khi hỗn_hợp sôi đều cho mì ống và 1/2 phó_mát vào nấu hơi sệt lại . Cho ra đĩa , cho 1/2 phó_mát còn lại lên trên mặt , ăn nóng .
Theo Món ngon

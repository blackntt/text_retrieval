﻿ Có chồng : làm_việc gấp đôi
Tôi đang kiệt_sức và sắp ngã quị . Có gia_đình , có con rồi tôi thấy mình làm_việc như gấp đôi đàn_ông . Ở công_sở thì bị áp_lực công_việc . Về nhà là tất_bật với công_việc nội_trợ , chăm_sóc chồng_con . Đêm chỉ ngủ được vài tiếng . Thú_thật , từ lúc lên xe_hoa về nhà chồng đến giờ tôi chẳng có lấy một buổi xem phim , thư_giãn ở quán cà_phê , từ_chối mọi cuộc vui với bạn_bè ... Lúc_nào cũng thấy thiếu thời_gian cho việc cơ_quan và gia_đình .
Sống trong guồng quay này tôi thấy choáng_váng , không biết sẽ cầm_cự được bao_lâu . Thân_hình mơn_mởn thời con_gái , giờ đã dần khô_cằn , xơ_xác , lúc_nào cũng chỉ muốn cáu_gắt .
Để được là phụ_nữ hai giỏi thời hiện_đại thật_sự , tôi thấy quá khó . Phải_chăng đây cũng là một trong những nguyên_nhân làm tăng tình_trạng ly_hôn của gia_đình trẻ hiện_nay ? Tôi có vài người bạn đã ly_hôn , họ bảo : thấy mình như được “ giải_thoát ” .
THANH_HẰNG

﻿ Làm thế_nào để xác_định mục_tiêu ?
Đó là tên của cuộc_thi thuyết_trình tiếng Anh do Hội_đồng Anh phối_hợp với Tập_đoàn Giáo_dục EMG tổ_chức . Tất_cả những bạn học_sinh , sinh_viên Việt_Nam trong độ tuổi từ 16 - 20 đều có_thể tham_dự .
Giải_thưởng bao_gồm :
- 1 khóa học đặc_biệt về kỹ_năng thuyết_trình “ Academic_Presentation_Skill ” trị_giá 300 USD / suất kèm theo 1 bộ giáo_trình Kỹ_năng thuyết_trình .
- Thẻ hội_viên của British_Council .
- Thẻ_Sinh viên Quốc_tế ISIC của Ngân_hàng ANZ .
Cuộc_thi sẽ được bắt_đầu_từ 17 giờ ngày thứ_bảy 17/6 tại Hội_đồng Anh .
Thông_tin chi_tiết mời xem trang_web : http : / / www.educationuk-vietnam.org
Để tham_gia cuộc_thi , các bạn học_sinh , sinh_viên có_thể lấy phiếu đăng_ký trước ngày 13/6 tại :
Hội_đồng Anh , 40 Cát_Linh , Hà_Nội
ĐT : ( 04 ) 8436780
Email : educationuk@britishcouncil.org.vn
hoặc EMG Education , tầng 3 , Khách_sạn Horison , 40 Cát_Linh , Hà_Nội
ĐT : ( 04 ) 7366087 Trí_Kiên

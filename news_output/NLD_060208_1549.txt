﻿ Truyền_thống đón lễ Valentine trên thế_giới
Giới trẻ Hàn_Quốc với Lễ tình_yêu Ngày lễ Tình_yêu , hay con gọi_là lễ Thánh_Valentine , đã tồn_tại trên khắp thế_giới hơn 1.000 năm qua – không_chỉ vì Thánh_Valentine mà vì tính huyền_diệu của ngày lễ_hội .
Tình_yêu , sự lãng_mạn và lòng chia_sẻ đã hoà_quyện vào nhau tạo nên một lễ Valentine đẹp hơn mọi ngày lễ trong năm .
Ý
Lễ_Valentine còn được gọi với một cái tên khác , đó là Lễ_hội chó_sói . Trong quá_khứ , vào ngày này , nam_giới khắp nước sẽ chuẩn_bị các cỗ xe_ngựa đẹp nhất để đón người mình yêu đi tham_dự các hoạt_động văn_hoá ngoài_trời . Hiện_nay , các cặp uyên_ương luôn ngồi lại bên nhau và cùng nhau ngâm_nga những bài_thơ tình , hoặc nghe nhạc tình_yêu .
Đức
Những đoá hoa đại_diện cho tình_yêu là món mà giới trẻ nước Đức ưa_chuộng nhất trong ngày lễ Tình_yêu . Nhiều người sợ rằng vào ngày này , các cửa_hàng sẽ không đủ hoa để cung_cấp nên họ phải đặt mua từ một tháng trước . Nam_giới nước này không_chỉ mua hoa tặng riêng cho người_tình của mình mà_còn tặng cả những người nữ thân_yêu trong gia_đình .
Anh
Thanh_niên nam_nữ nước Anh thuờng làm những món quà tình_yêu bằng những gì có_thể và gói món quà này trong một tờ giấy có viết một bài_thơ tình để_dành tặng người mình yêu ! Những bài_thơ tình này không phải hay như những nhà_thơ nổi_tiếng , nhưng miễn_là lời thơ xuất_phát từ trái tìm của mỗi người nhằm bày_tỏ tình_yêu thật_sự với người bạn_đời tương_lai .
Bộ thương_mại & amp ; công_nghiệp Anh dự_đoán người_dân sẽ gửi 68 triệu thông_điệp lãng_mạn qua ĐTDĐ trong lễ Tình_yêu , tăng 19,3% so với năm trước . Năm_ngoái , 45 triệu người dùng ĐTDĐ gửi đi tổng_cộng hơn 16 tỷ tin nhắn . " Tin nhắn chữ là phương_tiện liên_lạc tức_thời và dễ_dàng . Với sự ra_đời của công_nghệ và dịch_vụ mới như 3G , nhắn_tin hình ... , năm nay chúng_tôi không_chỉ bày_tỏ tình_cảm của mình qua con chữ " , Bộ_trưởng Thương_mại Stephen_Timms nói .
Ngoài tin nhắn và thiệp điện_tử , trong dịp lễ Valentine năm nay , người Anh có_thể gửi đi 13 triệu tấm thiệp truyền_thống ( làm_bằng giấy với hình trái_tim , hoa_lá ... ) .
Ireland
Ngày lễ Valentine là một dịp lễ_hội vui_chơi dành cho các đôi uyên_ương tại đất_nước này . Nhiều hoạt_động văn_hoá trong nhà , cũng_như ngoài_trời được tổ_chức dành cho giới trẻ , đặc_biệt là những buổi hoà_nhạc tình_yêu rất đặc_sắc . Các bạn trẻ có_thể đến các câu_lạc_bộ để yêu_cầu tặng những bản_nhạc tình_yêu cho người_yêu của mình .
Pháp_Pháp là đất_nước có truyền_thống tổ_chức lễ_hội Tình_yêu mang màu_sắc lãng_mạn nhất thế_giới . Vào ngày này , các đôi uyên_ương thường dành cho nhau những khoảnh_khắc thời_gian tuyệt_vời bên nhau , trao_tặng những lời yêu_thương ngọt_ngào nhất . Món quà mà giới trẻ Pháp yêu_thích để_dành tặng cho nhau là những chiếc bánh sôcôla với hình trái_tim lớn_nhỏ khác_nhau .
Theo SGTT

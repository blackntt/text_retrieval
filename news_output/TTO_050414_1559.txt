﻿ Xây_dựng nhà triển_lãm , thông_tin bảo_vệ môi_trường biển trên đảo Hòn_Mun
Theo ông Trương_Kỉnh , phó_giám_đốc ban quản_lý cho_biết , trung_tâm sẽ là nơi triển_lãm hình_ảnh , nội_dung tuyên_truyền , phổ_biến các thông_tin , kiến_thức , bố_trí phòng chiếu_phim v . v … giới_thiệu về khu bảo_tồn với mọi du_khách trong và ngoài nước đến đảo Hòn_Mun .
Qua đó , giúp cho mọi người có thêm nhận_thức về những giá_trị vô_giá của môi_trường_sinh_thái biển và đa_dạng_sinh_học trong khu bảo_tồn cũng_như của cả di_tích danh_thắng quốc_gia vịnh Nha_Trang … để cùng có những hành_động tích_cực trong việc tham_gia bảo_vệ môi_trường biển .
Tin , ảnh : P . S . N

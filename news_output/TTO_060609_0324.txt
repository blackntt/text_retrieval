﻿ Sống chung với khói bụi
Một chị bán hàng ăn trên đường Hàm_Tử ( quận 5 , TP.HCM ) cho_biết từ khi xuất_hiện khói bụi quán chị ế hẳn , ngày nào cũng bù_lỗ nên chắc phải đóng_cửa .
Không_những hàng ăn mà những hộ kinh_doanh khác cũng chịu cảnh tương_tự . Không ai nghĩ một công_trình có tầm_cỡ như đại_lộ đông - tây lại không được che_chắn triệt_để . Nhiều đoạn chỉ được ngăn bằng hàng_rào lưới nên mỗi khi xe chở cát đá cho công_trình chạy qua là bụi khói bay mù_mịt . Khói bụi còn làm cản_trở tầm nhìn nên rất dễ xảy_ra tai_nạn .
Dự_án đại_lộ đông - tây còn mất nhiều thời_gian để hoàn_thành , không biết người_dân phải hứng_chịu khói bụi đến bao_giờ ?
BẢO_MINH

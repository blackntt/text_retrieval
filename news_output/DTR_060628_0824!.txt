﻿ Khởi_tố kẻ môi_giới côn_đồ đập phá nhà dân ở hồ Ba_Mẫu
Hôm_qua 27/6 , Cơ_quan điều_tra Công_an quận Đống_Đa ( Hà_Nội ) đã ra quyết_định khởi_tố bị_can đối_với Bùi_Văn_Giang , cán_bộ một công_ty du_lịch , về hành_vi cố_ý hủy_hoại tài_sản công_dân .
Ông Giang chính là người môi_giới cho Trần_Việt_Sơn , Giám_đốc Công_ty TNHH Thương_mại và sản_xuất hàng may_mặc Việt_Huy , thuê côn_đồ đập nát nhà bà Nguyễn_Thị_Thanh_Bình tại khu_vực hồ Ba_Mẫu ( Đống_Đa , Hà_Nội ) .
Sau khi xảy_ra vụ đập nhà vào đêm 23/5 , ông Giang đã bỏ trốn cho_đến 18/6 mới ra đầu_thú .
Theo K . T . L_Thanh_Niên

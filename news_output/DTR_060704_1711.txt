﻿ Hai trường_hợp gian_lận tinh_vi ở Học_viện Ngân_hàng
Một nữ_sinh bỏ của chạy người , một nam_sinh đội tóc giả để che tai_nghe - đó là hai trường_hợp gian_lận ngoạn_mục và tinh_vi diễn ra tại Hội_đồng thi của Học_viện Ngân_hàng , Hà_Nội trong buổi thi ngày hôm_nay , 4/7 .
Tại điểm thi trường THPT Trung_Tự của Học_viện Ngân_hàng , trong buổi thi sáng nay , một thí_sinh nữ đã cắm_đầu_cắm_cổ chạy vụt ra từ phòng thi số 307 bỏ lại toàn_bộ “ hành_trang ” đi thi .
Thí_sinh đó là Bùi_Huyền_Trang có số_báo_danh 11288 . Thí_sinh này bị công_an PA25 phát_hiện đã sử_dụng điện_thoại di dộng có tai_nghe trong phòng thi và sự_việc này đã diễn ra trót_lọt suốt 45 phút mà giám_thị của phòng thi đó không hề hay_biết !
Đà_Nẵng : 5 thí_sinh bị đình_chỉ và khiển_trách
Tại_Đà_Nẵng , trong buổi đầu thi môn Toán số thí_sinh có_mặt đạt 78,73% trong tổng_số 29.139 thí_sinh đăng_ký dự thi ( vắng 6.195 ) . Đến buổi chiều thi môn Lý , cụm thi này lại vắng tiếp 412 thí_sinh .
Trong buổi_sáng , có 2 thí_sinh bị khiển_trách , 3 thí_sinh bị đình_chỉ thi vì vi_phạm quy_chế thi . Nhiều thí_sinh kêu đề Toán khó , chỉ làm được vài câu .
Tình_hình an_ninh thi_cử ở cụm thi Đà_Nẵng nói_chung khá yên_ắng , thời_tiết thuận_lợi , ngoại_trừ cảnh nhốn_nháo mua_bán bài giải môn Toán ( 2 nghìn đồng / bộ ) trước cổng trường ĐH Kinh_tế - ĐH Đà_Nẵng .
Một thí_sinh do quá căng_thẳng đã bị ngất_xỉu ở Hội_đồng thi số 2 của ĐH Sư_phạm Đà_Nẵng . - Phúc_Hưng
Sau khi bị phát_hiện , Bùi_Huyền_Trang đã bỏ của chạy lấy người , vứt lại hiện_trường chiếc điện_thoại_di_động vẫn đang trong chế_độ nghe với thời_gian sử_dụng lên tới hơn 120 phút . Được biết , thí_sinh Bùi_Huyền_Trang là học_sinh của trường THPT Việt_Đức , Hà_Nội .
Còn trong buổi thi môn Lý chiều nay , cũng một thí_sinh thi vào Học_viện Ngân_hàng tại điểm thi trường THPT Kim_Liên , thí_sinh Vũ_Việt_Đức có số_báo_danh 12840 tại phòng thi 352 đã sử_dụng di_động để nghe nhắc bài từ bên ngoài vào trong_suốt thời_gian 1h45 phút mà giám_thị cũng vẫn không hề hay_biết .
Sự_việc cũng chỉ được phát_hiện khi có công_an PA25 vào_cuộc . Dưới sự chứng_kiến của nhiều người , thí_sinh Đức đã phải bỏ ... bộ tóc giả đang ngự trên đầu mình cùng_với đôi tai_nghe di_động !
Hiện , công_an đang điều_tra để làm rõ vụ_việc và Hội_đồng tuyển_sinh của Học_viện Ngân_hàng cũng đã họp và nhắc_nhở những giám_thị của hai phòng thi có hai thí_sinh gian_lận kiểu cực_kỳ tinh_vi này .
Trong ngày thi đầu_tiên , ĐH Bách_Khoa cũng phát_hiện có một thí_sinh sử_dụng điện_thoại_di_động với mục_đích gian_lận . Tại ĐH Mở_Bán công TPHCM , một thí_sinh sử_dụng điện_thoại_di_động trong nhà_vệ_sinh đã giám_thị bắt quả_tang .
Chiều nay , tại nhiều hội_đồng thi đã diễn ra hiện_tượng thí_sinh bỏ thi với số_lượng khá lớn vì không làm được bài như ĐH Xây_dựng bỏ thi 84 thí_sinh , Học_viện Ngân_hàng 135 thí_sinh bỏ thi ... . Minh_Hạnh

﻿ Phóng_sự ảnh : Lễ_hội đường_phố
Đến con phố đi bộ ngắn_ngủn này cả du_khách địa_phương và phương xa được tận_hưởng , hòa_nhập trực_tiếp vào một không_khí rất đúng chất , tinh_thần festival .
Mấy anh_chàng cà_kheo đi lênh_khênh ngoài đường . Nhiều người đàn hát , vẽ tranh , biểu_diễn ngay trên phố . Là nghệ_sĩ dân_gian nên chuyện nghệ_thuật không quan_trọng lắm , tinh_thần văn_hóa lễ_hội mới là chính .
Một lối vào khu nghệ_thuật đường_phố khá ấn_tượng với bên trước là một bãi giữ xe , khách leo qua dây_xích ngăn đường và vén hai tấm vải phông để lọt vào . - Ảnh : Hòa_Bình
Giữa trưa nắng , phố đi bộ chưa đến giờ hoạt_động nhưng hai nghệ_sĩ đến từ Nhà_văn_hóa Huế này cũng ngẫu_hứng những bản_nhạc du_dương đầy cảm_xúc , bắt_đầu lôi_kéo sự chú_ý của người qua_lại . - Ảnh : Hòa_Bình
Ai biết gì , thích gì cứ bày ra mà làm ; người khác thích cứ vô_tư tham_gia , chẳng kể đến chuyện quen lạ hay phải có tiền mua vé ...
Hòa đến từ Câu_lạc_bộ trẻ_em đường_phố Huế đang ký_họa chân_dung cho du_khách - Hoạt_động này hoàn_toàn miễn_phí - Ảnh : Hòa_Bình
Nguyễn_Hoài_Linh đang thực_hiện một bức trang đường_phố - Ảnh : Hòa_Bình
Rất lạ mà rất quen , hình_thức nghệ_thuật đường_phố phổ_biến ở châu Âu đã hòa_nhịp vào Festival_Huế 2006 một_cách nhẹ_nhàng , duyên_dáng !
HÒA_BÌNH

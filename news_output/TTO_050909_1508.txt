﻿ Bố_mẹ già , có_thể bảo_lãnh con định_cư Canada ?
- Trả_lời của ngài Sanjeev_Chowdhury - Tổng_Lãnh sự Canada tại TP.HCM :
Vợ/chồng và cha_mẹ có_thể bảo_lãnh người_thân sang Canađa . Vợ/chồng có_thể bảo_lãnh cho nhau ; cha_mẹ có_thể bảo_lãnh con dưới 22 tuổi hoặc lớn hơn tuổi này nếu người con đó hiện đang là học_sinh chính_quy tại trường cao_đẳng , đại_học . Thường_trú dân hoặc công_dân Canađa có_thể bảo_lãnh cha_mẹ sum_họp với gia_đình . Gia_đình này gồm các con_nhỏ . Trường_hợp bảo_lãnh này đòi_hỏi về tài_chính , vì_vậy nếu cha_mẹ không làm_việc , thì người bảo_lãnh sẽ gặp nhiều khó_khăn hơn trong việc bảo_lãnh .
Bạn không cho_biết rõ trong câu_hỏi của bạn là bạn dưới 22 tuổi hoặc là sinh_viên chính_quy . Nếu bạn không đủ tiêu_chuẩn trong diện bảo_lãnh gia_đình , bạn có_thể định_cư theo diện tự_túc .
Để định_cư sang Canađa , bạn phải nộp đơn tại Đại_sứ_quán Canađa tại Singapore . Chúng_tôi không giải_quyết cấp visa định_cư tại Tp_Hồ_Chí_Minh ( chỉ cấp visa du_học , làm_việc , thăm thân tại Tp_Hồ_Chí_Minh ) .
Trước khi nộp đơn định_cư , bạn có_thể tự đánh_giá để xem có đủ điểm xin đi hay không . Để làm_việc này , hãy truy_cập trang_web www . cic.gc.ca và đánh_giá bản_thân cũng_như đoán thử số điểm nhân_viên định_cư dành cho mình . Nếu đủ số điểm , hãy nộp đơn . Nếu_không , bạn nên suy_nghĩ lại việc nộp đơn .
Lưy ý : Bạn sẽ được thưởng điểm , vì cha_mẹ của bạn đang sinh_sống tại Canađa . Nhớ rằng anh_em không_thể bảo_lãnh cho nhau .
Mọi thắc_mắc liên_quan đến vấn_đề hộ_chiếu , thị_thực ... bạn_đọc có_thể gửi về chương_trình " Hỏi & amp ; Đáp_Xuất_Nhập cảnh " tại địa_chỉ : tto@tuoitre.com.vn
TTO

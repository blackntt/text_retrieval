﻿ Chung nỗi sợ
- Sợ không bắt được khách .
- Không , khách nhiều tới mức họ phải nhét vào cốp xe đấy thôi . Họ sợ công_an . Với nạn mãi_lộ hoành_hành , cánh lái_xe phải vừa nhìn đường vừa nhìn công_an đấy ông ạ .
- Vậy tôi đố ông hành_khách đi xe sợ nhất điều gì ?
- Sợ cánh lái_xe nhét họ vào cốp xe .
- Không , họ cũng sợ một_số công_an “ ăn giữa đường ” . Thông_thường một_số công_an bắt được chiếc xe nào chở quá_tải thì sau khi cánh tài_xế “ làm_luật ” xong , xe lại được bon_bon . Nay có nhiều xe nhét hành_khách vào cốp xe quá , một_số công_an “ làm gắt ” hơn , thế nên lái_xe thu giá vé đắt hơn với lý_do phải “ làm_luật ” nhiều hơn .
- Thế_là cả cánh lái_xe lẫn khách đi xe đều có chung nỗi sợ , ông nhỉ ?
NGUYỄN_THỊ_THU_GIANG

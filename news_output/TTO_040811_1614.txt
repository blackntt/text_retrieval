﻿ Đà_Nẵng : 200 triệu đồng cho cuộc_thi sáng_tác ca_khúc về TP
Theo đó tất_cả các nhạc_sĩ chuyên , không chuyên , cũng_như mọi công_dân Việt_Nam , kể_cả người Việt_Nam định_cư ở nước_ngoài đều có_thể tham_gia cuộc_thi . Theo Ban tổ_chức sẽ có 5 giải_thưởng được trao cho 5 ca_khúc hay nhất ( mỗi ca_khúc 20 triệu đồng ) vào dịp 30 năm ngày giải_phóng Đà_Nẵng ( 29-3-2005 ) .
Đồng_thời Ban tổ_chức cũng sẽ trao giải_thưởng cho ca_khúc hay nhất trong số 5 ca_khúc nói trên vào dịp 2-9-2005 với tiền thưởng 100 triệu đồng , thời_gian nhận bài thi sẽ kết_thúc vào ngày 31-12-2004 .
Được biết đây là lần đầu_tiên TP Đà_Nẵng kết_hợp với Hội_Nhạc sĩ Việt_Nam tổ_chức cuộc_thi , trước đó tuy có nhiều cuộc_thi sáng_tác về ca_khúc nhưng vẫn chưa có tác_phẩm nào thật_sự “ làm tổ ” trong lòng người nghe .
ĐĂNG_NAM

﻿ Cuộc_chiến của các thế_giới " tàn_phá " các rạp phim !
Ở trên toàn thế_giới , War of the worlds đạt doanh_thu 13,3 triệu đô trong ngày đầu . Đây là bộ phim doanh_thu kỷ_lục nhất từ trước đến nay ở ngày đầu phát_hành ( vào thứ_tư trong tuần ) của hãng Paramount_Pictures .
Người_ta nói bộ phim không_chỉ ăn_khách nhờ nội_dung ... xưa như trái_đất ( sinh_vật ngoài hành_tinh tấn_công trái_đất ) mà_còn vì sự_kiện siêu_sao trong phim Tom_Cruise đang gây chú_ý thời_gian gần đây khi đính_hôn với ngôi_sao Katie_Holmes . Dĩ_nhiên tên_tuổi Spielberg cũng đóng_góp không nhỏ vào sự tò_mò của khán_giả .
Như_vậy War of the Worlds đã phá kỷ_lục doanh_thu ngày đầu trình_chiếu của bộ phim Batman_Begins ( 15 triệu đô ) , nhưng vẫn thua xa Spider-Man 2 ( 40 triệu đô trong ngày đầu chiếu_phim vào năm 2004 ) . Spider-Man 2 năm_ngoái đồng_loạt trình_chiếu ở 4.100 rạp ở Bắc_Mỹ trong khi năm nay War of the Worlds chỉ chiếu đồng_loạt ở 3.900 rạp .
Theo nhận_xét của giới am_tường , War of the Worlds cũng không cách chi đoạt doanh_thu tuần đầu_tiên sánh bằng Spider-Man 2 được ( 180 triệu đô ) . Điều an_ủi duy_nhất là War of the Worlds sẽ trở_thành sản_phẩm lớn nhất của đợt phim hè năm nay .
TR . N

﻿ “ Hắn chịu_khó và nhạy lắm ”
Hàng_loạt sáng_kiến nhưng với anh có_lẽ nhớ nhất vẫn là lần thiết_kế bộ giá đỡ truyền động ( cầu_dao ) CD 03 pha ; 245kV ; 1250A ; hiệu : Gec-Alsthom ; kiểu : S2 Dat - trạm Hóc_Môn - TP.HCM nhằm phục_vụ đợt cao_điểm tổ_chức SEA Games tại VN .
Đang thi_công thì gặp trục_trặc ( phần giá đỡ và bộ truyền động CD đã thiết_kế không phù_hợp về vị_trí , kích_thước lắp_ghép với bộ giá đỡ CD mới ) , Tuấn đã đưa ra giải_pháp bỏ các ống truyền động ngang , bệ đỡ trung_gian , khớp quay trung_gian , đồng_thời dời ống truyền động đứng , tủ điều_khiển vào vị_trí pha giữa ( pha B ) CD và nối trực_tiếp với các ổ quay ở vị_trí ấy .
Với giải_pháp này , CD-239 - 1 Hóc_Môn đóng mở khá tốt , đóng điện đúng tiến_độ . “ Sáng_kiến được công_ty thưởng đủ bao anh_em chầu chè , nhưng điều làm tôi vui là đảm_bảo công_việc thi_công đúng tiến_độ ” - anh cho_biết .
“ Nhiệt_tình lắm , là kỹ_sư nhưng rất chịu_khó tham_khảo những công_nhân lâu năm nhiều kinh_nghiệm ” - anh Phạm_Văn_Bậu , đội_trưởng đội bảo_trì - thí_nghiệm điện , nói về Tuấn như_thế .
Trong đợt lắp CD 572 - 7 Phú_Lâm anh và đồng_nghiệp đã làm_việc từ ngày hôm trước cho_đến tận trưa hôm_sau . “ Chỉ kịp tranh_thủ đôi phút ăn cơm rồi lại vào việc . Nhanh chừng nào thì trả điện sớm chừng đó để giảm bớt thiệt_hại , bực_bội trong sản_xuất , sinh_hoạt của bà_con ” - anh chia_sẻ .
Dường_như mỗi lần khắc_phục sự_cố lại thấy Tuấn có sáng_kiến . Một đồng_nghiệp với Tuấn nhiều năm qua , anh Nguyễn_Hữu_Minh , bảo : “ Hắn chịu_khó và nhạy công_việc lắm ” .
K . A .

﻿ Phát_hiện bức hoạ mới của Leonardo da Vinci
Bức vẽ khuôn_mặt của một bà lão do nhà sưu_tập Italy mua được ở Venice từ những năm 1970 đã được xác_định là của danh_hoạ Leonardo da Vinci . Nếu đúng như_vậy , tác_phẩm sẽ trị_giá 6,64 triệu USD .
Bức hoạ sẽ được công_bố vào ngày 15/6 tại Cung_điện Doges ở Venice .
Hình phác_họa chiếc đầu với những đặc_điểm méo_mó , kỳ_quái có phong_cách rất giống da Vinci đã được bán cho nhà công_nghiệp Giancarlo_Ligabue . " Tôi mua nó từ một nhà_buôn đồ cổ ở Venice mà ông này khẳng_định rằng đừng mơ_tưởng đó là một tác_phẩm của Leonardo " .
" Một năm trước tôi đã quyết_định xuất_bản bộ sưu_tập của mình . Với bức hoạ có phong_cách Leonardo , tôi đã tham_khảo ý_kiến giáo_sư Luisa_Cogliati_Arano và bà khuyên tôi nên đưa nó đi kiểm_tra " .
Cuộc phân_tích bằng tia_hồng_ngoại cho thấy còn một bức hoạ khác nằm phía dưới bức tranh .
" Cuộc kiểm_tra khẳng_định đó thực_sự là tác_phẩm của Leonardo da Vinci và về sau được bổ_sung thêm một_số đường_nét . Các nhà sưu_tập thường chỉnh_sửa những đặc_điểm mà họ cho là chưa đủ chi_tiết " , Cogliati nói .
Theo M . T . Vnexpress/AFP

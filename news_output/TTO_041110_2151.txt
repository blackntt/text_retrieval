﻿ Lại thêm hàng_loạt “ nhạc copy ” ? !
Mới_đây , trên thị_trường lại xuất_hiện CD nhạc copy - cover gồm 10 đĩa , sưu_tập những " cặp " bài hát mà người sản_xuất gọi_là đạo nhạc . Một sản_phẩm không hợp_pháp nhưng ...
CD copy - cover cung_cấp cho chúng_ta một danh_sách tương_đối dài hơn với bản_nhạc gọi_là gốc và bản xem như copy dù chưa được bất_cứ cơ_quan_chức_năng nào thẩm_định và xác_định . Nghe qua , dù không là người trong nghề cũng có_thể cảm_nhận sự giống nhau ít_nhiều của những bản_nhạc này .
Nghe các ca_khúc
Mẹ yêu
Anh yêu em
Tuổi 16
Renaissance faire
Ngồi hát ca bềnh_bồng
The clock ticks on Chiếm tỉ_lệ cao trong danh_sách này là các bài hát của nhạc_sĩ Quốc_Bảo . Ngoài ca_khúc Tuổi 16 ( copy từ Renaissance fair ) đã được Hội_Âm nhạc TP.HCM thẩm_định , còn có Ngồi hát ca bồng_bềnh ( giống_hệt nhạc_khúc The clock ticks on của Blackmore ' s Night ) , Để anh cháy cùng em ( giống_hệt Dance with me của Debelah_Morgan ) , Ánh trăng ( giống_hệt Can ' t wait của Yuki_Hsu - Yoo_Seung_Jun ) .
Đây cũng là những nhạc_khúc nghi là đạo ( do chưa có bằng_chứng cụ_thể ) đã được báo_giới đặt câu_hỏi trước_đây nhưng chưa được cơ_quan chức_trách thẩm_định .
Tiếp_theo trong danh_sách còn có hai bài hát của Phương_Uyên : Anh tôi ( giống ca_khúc Biết không còn của Nhậm_Hiền_Tề ) và Mẹ yêu ( giống Anh yêu em của Vương_Kiệt ) .
Tay guitar trẻ nổi_tiếng thời_gian gần đây là Vĩnh_Tâm lâu_nay cũng có vài ca_khúc tham_gia thị_trường . Và trong danh_sách này có hai ca_khúc của anh : Tình_yêu tìm thấy ( giống Seven days của Craig_David ) và Hãy cho em ngày_mai ( giống Let me be the one của SwiT ) .
Thái_Thịnh , một cái tên rất lạ , cũng có hai tác_phẩm nằm trong danh_sách là Niềm thương_nhớ ( giống Generous của Moon_Hee_Jun ) và Anh sorry ( giống Out of the d ark của Falco ) . Cả hai ca_khúc này đều viết cho Triệu_Hoàng hát .
Danh_sách còn có Quang_Huy với bài Em thích anh ( HAT hát , rất giống Em yêu anh của Jewelry ) , Hàn_Vy với Tình em ngọn nến ( giống Sự_thật của tình_yêu của Trịnh_Tú_Văn ) , Minh_Kha với Cỗ máy tình_yêu ( giống Ka-ching của Shania_Twain ) , Đỗ_Quang với Giấc_mơ xinh ( giống Định_mệnh của Harlem_Yu ) ...
Tính ra CD này có bài hát của 20 nhạc_sĩ ( có một_số ca_khúc giống_hệt ca_khúc nước_ngoài nhưng chỉ có người hát mà không có tên tác_giả ) . Dù không rõ ngoài chuyện kinh_doanh những người sản_xuất còn nhằm vào mục_đích gì , nhưng rõ_ràng điều này đã tạo một ấn_tượng xấu cho hoạt_động âm_nhạc . Chí_ít là sự hoài_nghi , lớn hơn là sự thất_vọng của người_yêu nhạc đối_với những người viết nhạc trẻ hiện_nay .
Giới buôn_bán băng đĩa cho_biết 10 đĩa đang bán hiện_nay chưa chắc là những đĩa cuối_cùng . Với tình_hình hiện_tại chắc_chắn sẽ còn đĩa thứ 11 rồi 12 , 13 ra_đời . Liệu những đĩa ra_đời sau_này có là những cái tát thật_sự vào hoạt_động âm_nhạc của giới trẻ không ? Và chẳng_lẽ những bộ_phận quản_lý có liên_quan lại dửng_dưng trước hiện_tượng này ?
TRẦN_NHẬT_VY

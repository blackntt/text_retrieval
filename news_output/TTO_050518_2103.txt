﻿ Kinh_tế Mỹ thiệt_hại vì ... Star_Wars
Những bài phê_bình tốt_đẹp về bộ phim này sẽ đẩy người hâm_mộ đến rạp_hát . Nhưng hàng người xếp_hàng chực_chờ trước các rạp_hát ắt_hẳn khiến người_ta sẵn_sàng bỏ việc để được trong nhóm những người đầu_tiên đi xem tập ba và cũng là tập cuối của bộ phim khoa_học_viễn_tưởng hoành_tráng của đạo_diễn George_Lucas .
Tính_toán của văn_phòng Challenger , Gray & amp ; Christmas dựa trên số khán_giả của hai tập phim đầu . Họ dự_báo sẽ có đến 4,8 triệu người Mỹ đi xem phim trong hai ngày 19 và 20-5 . Thậm_chí họ còn cho rằng có người có_thể bỏ việc từ 18-5 để đi kiếm vé xem cho được buổi chiếu đầu_tiên vào lúc 0g ngày 19-5 .
N . QUÂN ( Theo AFP )

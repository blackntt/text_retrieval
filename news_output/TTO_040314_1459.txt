﻿ Phát_động cuộc_thi ảnh đẹp đô_thị mới Phú_Mỹ_Hưng lần 2-2004
Ảnh dự thi là ảnh màu ( khổ 25x35cm , kèm một ảnh 9x12cm , không được ghép ) được chụp tại Phú_Mỹ_Hưng năm 2004 xoay quanh các mặt như : môi_trường , cơ sơ hạ_tầng , kiến_trúc , các tiện_ích , sinh_hoạt của cư_dân …
Đăng_ký làm thẻ chụp ảnh và nhận ảnh dự thi ( đến hết ngày 15-5-2004 ) tại Phòng kinh_doanh nhà Công_ty LD Phú_Mỹ_Hưng , Q7 , TP.HCM hoặc Hội_Nhiếp ảnh TP.HCM số 122 Sương_Nguyệt_Ánh , Q1 .
BTC sẽ chọn ra 60 ảnh để triển_lãm ; trong đó có một giải nhất ( trị_giá 6 triệu đồng ) , hai giải II , ba giải III và bốn giải khuyến_khích .
Tin , ảnh : T . T . D

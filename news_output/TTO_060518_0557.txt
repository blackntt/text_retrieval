﻿ Cổ_vũ kiểu Hàn_Quốc !
Ngày 27-5 tới đây , cộng_đồng cư_dân châu Âu đang sinh_sống và làm_việc tại TP.HCM sẽ khởi_động " tiền World_Cup " bằng giải bóng_đá nội_bộ thu_hút tám đội bóng Pháp , Đức , Bắc_Ireland , Hà_Lan , Scandinavia , Tây_Ban_Nha , Thụy_Sĩ và Anh tham_dự . Giải diễn ra_vào lúc 10g30 tại Trường đại_học RMIT , Q . 7 , TP.HCM .
Ronaldinho được chơi tự_do
Đó là lời khẳng_định của HLV tuyển Brazil_Carlos_Alberto_Parreira . Ông cho rằng cầu_thủ số 10 của tuyển Brazil này không phải là mẫu cầu_thủ chơi ở một vị_trí nhất_định nào . Ronaldinho sẽ rất nguy_hiểm nếu anh_ta được tự_do di_chuyển bởi anh luôn biết chọn những vị_trí là " điểm_nóng " của trận đấu .
Tuyển_Đức có_thể bị mất quân
Hậu_vệ của CLB Bayern_Munich_Philipp_Lahm đã bị chấn_thương khuỷu tay_trong trận đấu tập với CLB_FSV Luckenwalde cùng tuyển Đức tại Mannheim hôm thứ_ba . Hậu_vệ 22 tuổi này được đưa tới bệnh_viện để kiểm_tra xem có bị gãy xương hay không trong khi đội_tuyển Đức đã di_chuyển tới Ý để tiếp_tục đợt tập_huấn .
Saudi_Arabia công_bố danh_sách
Danh_sách 23 tuyển_thủ mà HLV Marcos_Paqueta của Saudi_Arabia vừa công_bố là sự pha_trộn giữa những cựu_binh nhiều kinh_nghiệm như đội_trưởng Sami al - Jaber và thủ_môn Mohammed al - Deayea ( hai trong số những cầu_thủ có số lần khoác_áo tuyển quốc_gia nhiều nhất thế_giới ) cùng những tài_năng trẻ như Reda_Takur và Yasser al - Qahtani .
Bị tước niềm_vui sống
Tiền_vệ Martin_Demichelis than_thở với giới truyền_thông Đức rằng việc bị HLV Jose_Pekerman loại khỏi danh_sách 23 tuyển_thủ Argentina không_những cướp đi niềm say_mê đá bóng mà_còn cướp mất niềm_vui sống của anh . Đến với Bayern_Munich từ năm 2003 , Demichelis đã có hai chức VĐQG , hai chiếc cúp QG Đức .
Zidane muốn có " vô_địch tập hai "
Từng được tận_hưởng những giây_phút đăng_quang ngọt_ngào tại VCK World_Cup 1998 , ngôi_sao người Pháp này lại muốn có được điều đó thêm một lần nữa trước khi anh chia_tay với sân_cỏ . Ở " tuổi xế_chiều " , Zidane tuyên_bố : " Tôi muốn vô_địch World_Cup thêm một lần nữa và tôi sẽ làm mọi thứ để đạt được điều đó " .
Tổng_thống Ba_Lan đến Đức cổ_vũ đội nhà
Tổng_thống Ba_Lan_Lech_Kaczynski sẽ tới Dortmund để dự_khán trận đấu giữa Ba_Lan và đội chủ nhà Đức vào ngày
14-6 tới theo lời mời của Tổng_thống Đức_Horst_Kohler . Ba_Lan là nước láng_giềng đã phối_hợp với Đức tích_cực nhất để chống lại nạn hooligan tại VCK World_Cup 2006 .
T . P . - H . Y . - M.TR

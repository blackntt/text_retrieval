﻿ Báo_chí quốc_tế đưa tin về kết_quả đại_hội Đảng
Các hãng thông_tấn quốc_tế hôm_nay đã nhanh_chóng đưa tin kết_quả nhân_sự đại_hội X_Đảng_Cộng sản Việt_Nam , tập_trung vào việc Tổng_bí_thư Nông_Đức_Mạnh tái đắc_cử .
Hãng tin AP và đài CNN loan_báo tổng_bí_thư được bầu lại một nhiệm_kỳ nữa đồng_thời giới_thiệu gương_mặt nhà_lãnh_đạo của Việt_Nam cùng một_số ủy_viên Bộ Chính trị . Trang_web của CNN đặt tin này vào hàng nổi_bật ở châu Á , đăng kèm ảnh Tổng_bí_thư bỏ_phiếu bầu trong đại_hội .
" Đảng_Cộng sản Việt_Nam hôm_nay đã bầu lại Tổng_bí_thư Nông_Đức_Mạnh , trong bối_cảnh các nhà_lãnh_đạo của đảng này chiến_đấu với nạn tham_nhũng , đồng_thời đẩy nhanh tốc_độ cải_cách và mở_rộng quan_hệ với thế_giới " , Reuters đưa tin .
Đài_truyền_hình TVNZ của New_Zealand cũng ngay_lập_tức thông_báo nhà_lãnh_đạo đảng Cộng_sản Việt_Nam tái_cử , với nhiệm_vụ trước_mắt là đẩy_mạnh chống tham_nhũng và mở_cửa hơn với thế_giới .
Hãng tin Pháp AFP cũng đồng_tình với nhận_định của TVNZ , cho rằng nhiệm_vụ quan_trọng nhất của ban lãnh_đạo mới của đảng cầm_quyền ở Việt_Nam là đẩy_mạnh cải_cách kinh_tế và chiến_đấu với nạn tham_nhũng .
Hãng dẫn lời các nhà_phân_tích chính_trị quốc_tế cho rằng ban lãnh_đạo mới thể_hiện sự kế_thừa trong tổ_chức đảng và dự_đoán sẽ không có những thay_đổi đột_biến về chính_sách trong thời_gian trước_mắt .
" Có một yếu_tố đáng chú_ý là sự kế_tiếp và đây cũng là điều mà chúng_tôi dự_đoán ở Việt_Nam . Đây là một quá_trình dần_dần " , nhà_phân_tích Martin_Gainsborough thuộc đại_học Bristol_University của Anh nhận_xét .
Việt_Nam với mức tăng_trưởng kinh_tế hàng_đầu thế_giới 8,4% trong năm_ngoái , hy_vọng sẽ gia_nhập Tổ_chức thương_mại Thế_giới vào năm nay , trước khi làm_chủ nhà của Hội_nghị thượng_đỉnh Diễn_đàn kinh_tế châu Á_Thái_Bình_Dương vào tháng 11 .
" Họ sẽ có một năm bận_rộn ở phía trước " , AFP dự_báo . Theo T . H . VnExpress

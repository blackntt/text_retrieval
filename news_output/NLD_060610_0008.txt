﻿ Núi lở
Anh rời trạm khi trời vẫn mưa . Cố đạp lên những vạt cỏ , anh né đôi chân khỏi những luống bùn nhão . Nếu cứ đi vài bước lại phải ngồi xuống cạy bùn bám vào giày , chẳng biết bao_giờ mới đến_nơi . Bầu_trời sũng nặng , mây quết qua những tán cây lướt_thướt chỉ cách anh vài bước chân .
Đã mấy ngày_nay chưa có một giọt nắng . Một cành củi mục gãy dưới chân anh . Đám mộc_nhĩ đã mọc dài từ cành củi ấy , loại sinh_vật này ưa ẩm . Nhưng mưa quá , ngay cả đám mộc_nhĩ cũng đã mủn ra . Ở trạm đầu_tiên anh đã tranh_thủ hái một đám mộc_nhĩ , định đem xuống núi , nhưng rồi chẳng biết bỏ chúng vào đâu . Chiếc gùi trên vai đã quá chật . Lượng thức_ăn mà anh mang theo chỉ đủ cho một người dùng trong mươi ngày . Mà trong khoảng thời_gian từ chân núi lên đến đây , anh đã dùng lạm vào đến một ngày khẩu_phần ấy .
Nước bắt_đầu ngấm qua gáy anh , từ cái cổ áo_mưa không thật khít . Gió lại quá mạnh , thốc bạt cây xung_quanh . Bước chậm cũng lạnh , mà bước quá nhanh , để mồ_hôi túa ra trong lưng lại càng lạnh hơn . Rất may anh đã mua được ở trạm một bao diêm mới . Bao diêm hôm_qua đã bị ngấm nước , khiến anh khổ_sở đến nửa ngày vì không sao mồi được thuốc hút . Nghĩ thế , anh đốt một điếu thuốc mới , bàn_tay khum lại che_chắn đầu lửa . Anh rít từng hơi dài . Điếu thuốc ngấm nước nổ lách_tách .
Mưa khiến tầm_mắt anh không_thể nhìn xa hơn hai ngọn đồi . Bầu_trời chưa hề có dấu_hiệu hửng nắng . Anh thấy những triền đất đỏ_ối vừa lở xuống phía bên kia thung_lũng . Rừng loang_lổ . Những mảnh đồi được khoanh thành từng viền nhỏ , xếp chồng lên nhau . Rời trạm nghĩa_là anh không còn hy_vọng gặp người . Nếu_không bị trượt ngã hay xảy_ra một tai_nạn nào_đó , cây cầu cũng không gãy , chắc đến trưa anh sẽ gặp ba . Nghĩ đến đấy , anh xốc mạnh cái gùi trên vai , bước rảo .
Qua hết ngọn đồi , lại một thung_lũng nữa . Đường trơn , bước xuống còn cực hơn bước lên . Anh bẻ được một đoạn cây dùng làm gậy chống . Đã nghe tiếng suối đổ ào_ạt dưới chân . Con suối này vào mùa_khô nắng trơ ra những tảng đá bạc_phếch , thế_mà hôm_nay ngay cả những tảng đá ấy cũng có cơ bị nước cuốn phăng , lăn xuống tận đáy lũng , phía con suối đổ ra sông lớn . Anh che mắt , nhìn xuyên qua màn mưa , thở ra một_hơi dài . Chiếc cầu bắc ngang suối vẫn ánh nhẫy lên dưới tầm nhìn của anh . Nếu nó đã bị nước cuốn_trôi anh sẽ không_thể_nào qua suối , nếu_không lần ngược lên tận thượng_nguồn , chỗ dòng_chảy thắt lại . Mà như_thế có_thể anh sẽ mất đứt nửa ngày .
Hai thanh gỗ lớn bắc song_song , phía trên có thành vịn . Tuy_nhiên cái thành vịn đã bị gãy mất một khúc ở giữa . Anh mọp sát xuống , kẹp hai chân qua hai thanh gỗ trơn_tuột , đẩy mình lên từng nhịp . Anh không_thể cúi lưng xuống mà bò , vì như_thế số thức_ăn trong gùi sẽ đổ hết xuống dòng nước . Qua hết suối , mình_mẩy anh dù mặc áo_mưa vẫn ướt_sũng .
Bóng chiếc lán nhỏ đã hiện ra trong tầm_mắt . Nó đậm đen , như một cái mộc_nhĩ đã nở hết cỡ đang bắt_đầu mủn rữa . Trong lán không thấy ánh lửa . Anh dấn bước . Cửa lán khép hờ . Có_lẽ ba anh chỉ làm cửa để phòng thú_dữ vào ban_đêm , chứ giữa chốn hoang quạnh này , còn ai để mà trộm_cắp .
Ba anh không có trong lều . Mưa thế_này ông đi đâu nhỉ ? Anh ra cửa lán , bụm tay gọi lớn . Tiếng gọi đập qua mấy ngọn đồi hoang . Ngạt trong màn mưa mù_mịt .
Gọi mấy tiếng , anh nghe tiếng cành gãy sau lưng :
- Ba đi đâu mưa_gió thế_này ?
- Vạt đồi bên kia hồi hôm vừa lở mất một mảng . Gần hai sào đậu của ba theo xuống vực rồi con ạ .
Ba anh nói tin xấu , nhưng trong giọng không thấy nhuốm một_chút phiền_muộn . Anh thở_dài . Giũ áo_mưa bước vào lều . Căn lều ẩm_thấp , tối_đen . Anh phải căng mắt một lúc mới thấy được_cái ổ nằm của ba . Chiếc mùng chưa cuốn lại . Tấm mền rách loang_lổ . Đã mấy lần anh mang lên mền mới nhưng ba bắt phải mang về . Ngay dưới ổ nằm là bếp . Lửa đã tắt , nhưng tro còn khô .
- Con mang đồ mới lên cho ba .
- Rượu được mấy lít ?
- Hai thôi ba ạ . Ba_không nên uống nhiều . Đêm_hôm gió_máy một_mình .
- Ta chẳng chết được đâu mà lo ! Tình_hình dưới đó sao rồi ?
- Ba nên về ba ạ ...
- Ta không hỏi chuyện ấy ...
Ba anh lục cái gùi , tìm hũ rượu , mở nút lá , nhấm thử một_chút .
- Vẫn lấy rượu chỗ ấy phải không ?
- Vâng , vẫn chỗ ấy . Ba đã dặn mà .
- Con nhóm lửa lên đi . Hong cho đồ khô , đứng đó ngấm lạnh .
Anh loay_hoay tìm củi . Ba anh hất cằm :
- Dưới giường ấy . Ta đã gom đủ dùng cả mùa mưa .
Thực_chất cái giường mà ba anh nằm chính là củi . Ông xếp lên từng_lớp , bên trên chải một cái chiếu . Củi gồ_ghề , phá thủng cả chiếu chui lên .
Ngọn lửa bén lên từ những mẩu gỗ . Căn lều ấm hẳn . Nhưng khói cuộn lên từng đụn khiến anh cay_xè mắt .
- Bay ở lại đêm nay chứ ?
- Con định ngồi một_chút rồi xuống ngay ba ạ . Dưới nhà còn nhiều việc lắm . Phần nữa sợ cái cầu dưới kia bị nước cuốn mất . Tranh_thủ về , mưa thế_này chẳng biết nó trụ được đến lúc_nào .
- Tùy con ...
Ba anh mở hũ rượu , rót ra một cái chén nhỏ . Ông nhấp nháp một_chút rồi chuyền qua anh :
- Uống một_hơi cho ấm bụng mà về .
Anh nhấp luôn hai ngụm . Mong là rượu mau ngấm để đủ can_đảm nói_chuyện với ba . Rượu ngấm mau thật , loáng đã nghe bừng_bừng nơi vành tai :
- Ba ạ , có người bảo ...
- Ta biết rồi , mọi người đều bảo chứ gì ? !
- Sao ba_không làm thế ?
Ông_già im_lặng . Đôi mắt nhăn_nheo dõi thấu màn mưa . Ngọn lửa lay đều trên bếp , hắt bóng ông lên vách liếp . Ông tợp một_hơi rượu mới :
- Rồi con sẽ hiểu ...
- Dạ , ba ...
- Con cứ về đi . Con chưa hiểu ...
- Con hiểu rồi mà ba ...
- Hiểu gì ?
Anh mồi một điếu thuốc . Rít được hai hơi lại gí cái tàn xuống đất . Chợt có_tiếng nổ lớn xa đâu_đó trong tiếng mưa ràn_rạt .
- Gì vậy ba ?
- Đất lở đó . Cứ lở dần từng ngày_một ...
Anh thở_dài :
- Năm nay ba làm bao_nhiêu đậu ba nhỉ ?
- Ta chẳng đong đếm bao_giờ . Làm là làm_vậy thôi . Làm cho đã rồi đất cũng lở hết . Ta cũng tính trồng cây mà giữ đất . Nhưng cây chưa kịp lớn cũng đã tuột xuống lũng hết cả .
- Vậy ba nên về ...
- Về làm_gì ?
- Vậy ba ở đây làm_gì ? - Lần đầu_tiên anh dám đưa ra một câu hỏi_vặn lại ý ba .
- Chẳng làm_gì cả . Cũng_như là về dưới đó vậy . Chẳng làm_gì cả !
Ông_già cúi xuống , bẻ thêm mấy cành khô quăng vào ngọn lửa :
- Con có_ăn gì với ba_không ?
- Thôi , con đem lên để ba dùng , chứ ăn vào đó thì còn nói_gì nữa !
- Hết mùa này , con không phải đem lên cho ba nữa ...
- Vậy làm_sao con gặp được ba ? !
- Thì mày lên không . Mà không gặp hay gặp cũng chẳng khác_gì ...
Anh ứa nước_mắt :
- Con lấy gạo nấu cơm cho ba nhé .
- Thôi , nếu_không ăn thì nấu làm_gì . Để đó chút chiều ta tính . Con cứ về đi . Từ đây ra đến trạm là vừa tối đấy . Con nghỉ lại trạm hay về luôn ?
- Con có gửi ít đồ_ăn lại trạm . Có_lẽ sẽ nghỉ lại . Mưa thế_này , đêm_hôm làm_sao xuống núi .
- Vậy con về đi .
Anh giũ cái áo_mưa mặc vào . Bộ_đồ trên người hong một lúc vẫn còn âm_ẩm :
- Ba ở lại giữ_gìn sức_khỏe ba nhé ! Nhớ đừng uống rượu ban_đêm .
- Ừ ...
Anh nhặt lại đoạn cây gác ngoài cửa lều . Bặm chân xuống lớp bùn nhão :
- Ba cứ ở trong này , đừng tiễn con !
- Ừ ...
Gió thốc một vạt mưa tung hổng tà áo anh lên . Mặt_trời có_lẽ đã ngả đâu_đó sau màn mây sũng nặng kia . Anh ngoảnh lại , thấy bóng ba , tay_vịn vách liếp nhìn theo . Mồi một điếu thuốc , anh rít liền mấy hơi dài , ngực bỏng rát . Qua hết con suối , anh nghe một tiếng nổ khủng_khiếp phía sau . Có_lẽ một vạt núi mới vừa lở xuống đâu_đó .
Truyện_ngắn của Nguyễn_Danh_Lam

﻿ Cơ_hội tiết_kiệm chi_phí học ACCA
Những người có chứng_chỉ ACCA có_thể hành_nghề ở tất_cả các nước trên thế_giới , được quyền chọn phương_thức thi kiểm_toán_viên ( CPA ) VN ( thi_trắc_nghiệm 100 câu thay_vì thi_viết tám môn như thông_thường ) .
Ngoài_ra , tham_dự học ACCA , bạn không_những được học các môn liên_quan tới lĩnh_vực tài_chính , kế_toán , kiểm_toán , mà_còn được học các môn về quản_trị doanh_nghiệp , quản_trị nhân_sự , hệ_thống thông_tin quản_lý , luật thương_mại , luật thuế ...
Chương_trình học ACCA bao_gồm 14 môn_học , được chia thành ba phần . Một quyền_lợi khác khi tham_gia học ACCA là bạn sẽ có cơ_hội nhận bằng đại_học danh_dự về kế_toán ứng_dụng của ĐH Oxford_Brookes ( Anh ) sau khi hoàn_thành phần 1 và phần 2 .
Đăng_ký học ACCA với Apollo trước 14-1-2006 sẽ giúp bạn được giảm học_phí và tiết_kiệm tới 185 USD , được tài_trợ học_phí cho môn kế_tiếp nếu bạn đạt điểm thi cao nhất so với các học_viên khác của Apollo trong kỳ thi ACCA vào tháng 6-2006 .
H.TH .

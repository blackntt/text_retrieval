﻿ 15.000 lượt người dự triển_lãm IECE lần IV-2006
( NLĐ ) - Từ ngày 7 đến 9-4 tại Nhà_thi_đấu Quân_khu 7 - TPHCM , Triển_lãm Quốc_tế Giáo_dục và Nghề_nghiệp ( IECE ) lần IV-2006 đã thu_hút 15.000 lượt người tham_gia .
Triển_lãm do Công_ty VINEXAD kết_hợp với Công_ty Triển_lãm Quốc_tế BINET Singapore tổ_chức , được sự bảo_trợ của Bộ GD - ĐT , Sở GD - ĐT_TPHCM . Báo_Người_Lao_Động là một trong những đơn_vị bảo_trợ thông_tin . Triển_lãm thu_hút 140 gian_hàng của 150 trường ĐH , CĐ và các học_viện quốc_tế đến từ 15 quốc_gia . Đặc_biệt , IECE đã tổ_chức “ Khu gian_hàng Nghề_nghiệp và Tuyển_dụng 2006 ” giúp hơn 1.000 lao_động tìm được cơ_hội việc_làm , tập_trung vào các ngành như kinh_doanh , nhân_sự , nhân_viên văn_phòng ...
H . Nga

﻿ Mạnh_Dũng ' ' mách_nước ' ' hàng phòng_ngự ĐTVN
Không có dịp ra sân , nhưng Mạnh_Dũng vẫn để lại dấu_ấn của mình trong hàng phòng_ngự ĐTVN ở trận đấu với U23 Uzbekistan .
Số_là sau giờ giải_lao , Mạnh_Dũng cùng các cầu_thủ dự_bị khác của ĐTVN bắt_đầu khởi_động phía sau cầu_môn của Quang_Huy . Vì_thế , trung_vệ này chứng_kiến rõ hơn ai hết pha nâng tỷ_số lên 2-1 của tiền_đạo đối_phương , bàn thua mà anh cho là ... lãng_xẹt .
Đó thực_sự là một tình_huống thủng lưới rất đáng tiếc . Không có hậu_vệ nào tranh_chấp với Erkinov đã_đành , ngay cả thủ_thành Quang_Huy cũng lưỡng_lự giữa hai giải_pháp băng ra đấm bóng hay lùi lại .
Kết_quả là anh đành đứng chôn_chân nhìn bóng_bay vào chính góc mà mình phải khép , dù pha đánh_đầu lần này của Erkinov không quá hiểm_hóc .
Thua quá sớm ngay đầu hiệp 2 , hàng thủ ĐTVN trở_nên lúng lúng và bối_rối . Điều đó khiến Mạnh_Dũng , với tư_cách " đàn_anh " cả về tuổi_tác lẫn chuyên_môn , cảm_thấy " nóng_mắt " . Vậy_là Dũng " Giáp " điềm_nhiên kiêm luôn vai_trò " cố_vấn kỹ_thuật " cho riêng khâu phòng_ngự .
Bàn nâng tỷ_số đầu hiệp hai của các cầu_thủ U23 Uzbekistan
Dĩ_nhiên , thủ_môn Quang_Huy ở gần nhất nên nhận được nhiều lời mách_nước hơn cả : " Huy ơi , phải to cái mồm lên chứ . Gọi anh_em nhiều vào , cho nó tỉnh ra . Nhắc anh_em đá trách_nhiệm lên , hỗ_trợ nhau lên , cứ ai biết chỗ người ấy_là thế_nào ? "
Văn_Nhiên cũng nằm trong vùng " phủ_sóng " : " Nhiên đá để_ý vào . Khi mình có bóng thì banh ra biên mà leo lên chứ chúi vào giữa làm_gì ? Nhắc thằng Thương ( Hoàng_Thương ) chú_ý phía sau , tiền_đạo của " nó " lẻn nhanh lắm đấy " ...
Có nhiều pha bóng nguy_hiểm trước cầu_môn đội nhà làm Mạnh_Dũng sốt_ruột đến mức góp_ý mà như gắt . Tính anh là thế , hễ cứ nói ra là bỗ_bã , nhiều khi đến mức sỗ_sàng .
Tuy_nhiên , rõ_ràng sự tận_tình của cầu_thủ nổi_tiếng là " ngựa_chứng " này đã giúp_ích khá nhiều cho đồng_đội . Kể từ khi có thêm một chỉ_đạo viên ở phía sau , những Minh_Đức , Huy_Hoàng hay Văn_Nhiên đã vững_vàng hẳn lên . Những pha xử_lý bóng cũng chuẩn_xác hơn , và đặc_biệt là khả_năng bọc_lót cho nhau được cải_thiện trông thấy .
Không_những không thua thêm bàn nào mà_còn phản_công gỡ hoà 2-2 , đó có_thể coi là nỗ_lực rất đáng ghi_nhận nơi hàng thủ .
Và khi cầu_thủ dự_bị cuối_cùng vào sân ( Hữu_Thắng ) , cũng là lúc đồng_hồ đã chỉ sang thời_gian bù giờ , người_ta thấy Mạnh_Dũng quay lại khu kỹ_thuật vẫn với vẻ mặt lạnh_lùng thường_ngày , nhưng không giấu nổi niềm_vui .
Anh không đá một phút nào , nhưng đã " chơi " gần trọn hiệp 2 trong tư_thế của một kẻ " vẽ đường " .
Dù không được_lòng nhiều người do cách sống khá lập_dị , nhưng kinh_nghiệm và khả_năng chuyên_môn của anh là điều ai cũng phải vị_nể . Thế nên vào sân thi_đấu hay chỉ đứng " nhắc_vở " phía sau , anh cũng đều có_ích cho trạng_thái thăng_bằng của toàn đội .
Trong những trận đấu còn lại của T& amp ; T_Cup , nhiều khả_năng Mạnh_Dũng sẽ rời bỏ vai_trò chỉ_đạo bất_đắc_dĩ này , bởi ông Riedl luôn muốn có anh ở trung_tâm hàng hậu_vệ . Hãy chờ xem , khi ấy tác_dụng của Dũng " giáp " có phát_huy như ở " hậu_trường " không ...
Theo A . Đ_Vietnamnet

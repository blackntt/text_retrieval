﻿ Hơn 860 nguồn tin tội_phạm do nông_dân cung_cấp
( NLĐ ) - Ngày 14-3 , ông Dương_Minh_Quang , Phó_Chủ tịch Hội_Nông dân TPHCM , cho_biết 3 năm qua ( 2002-2005 ) , Hội_Nông dân và Công_an TP đã xây_dựng được nhiều mô_hình phòng_chống tội_phạm và tệ_nạn xã_hội đạt hiệu_quả cao .
Hội_Nông dân đã cùng các đoàn_thể và công_an phường - xã kết_hợp trong việc rà_soát phân_loại đối_tượng , thông_báo các thủ_đoạn của tội_phạm , quản_lý tốt nhân hộ_khẩu , phân_công hội_viên giám_sát đối_tượng trên địa_bàn dân_cư .
Ngoài_ra , nông_dân và nhân_dân tại 13 quận - huyện trên địa_bàn TPHCM đã cung_cấp cho cơ_quan_chức_năng 861 nguồn tin có giá_trị liên_quan đến tội_phạm và tệ_nạn xã_hội . Nhiều nông_dân ở các quận 12 , huyện Hóc_Môn , Nhà_Bè ... còn dũng_cảm , trực_tiếp truy_bắt tội_phạm .
M . Nam
